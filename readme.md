<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://bisto.vn/"><img src="https://cdn4.iconfinder.com/data/icons/software-line/32/software-line-02-512.png" alt="Build Status" width="40" height="40"></a>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<a href="https://play.google.com/store/apps/details?id=com.bisto.geer_fe"><img src="https://w7.pngwing.com/pngs/52/715/png-transparent-google-play-logo-google-play-computer-icons-app-store-google-text-logo-sign.png" width="100" height="40"></a></p>


## Install Project Bisto

    1, clone project from gitlab
    2, copy project in to "C:\xampp\htdocs" (WINDOW) or " /opt/lampp/" (LINUX)
    3, start xampp (WINDOW) or lampp (LINUX)
    4, import file bisto.sql in to PHPMyAdmin
    5, go into project localhost/bisto_web
