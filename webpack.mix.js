// myproject/webpack.mix.js
const mix = require('laravel-mix');

mix.scripts([
    'public/backend/js2/jquery.min.js',
    'public/backend/js2/popper.min.js',
    'public/backend/js2/bootstrap.min.js',
    'public/backend/js2/jquery-ui.min.js',
    'public/backend/js2/jquery.appear.js',
    'public/backend/js2/countdown.min.js',
    'public/backend/js2/waypoints.min.js',
    'public/backend/js2/jquery.counterup.min.js',
    'public/backend/js2/wow.min.js',
    'public/backend/js2/apexcharts.js',
    'public/backend/js2/slick.min.js',
    'public/backend/js2/select2.min.js',
    'public/backend/js2/owl.carousel.min.js',
    'public/backend/js2/jquery.magnific-popup.min.js',
    'public/backend/js2/smooth-scrollbar.js',
    'public/backend/js2/lottie.js',
    'public/backend/js2/core.js',
    'public/backend/js2/charts.js',
    'public/backend/js2/animated.js',
    'public/backend/js2/kelly.js',

    'public/backend/js2/chart-custom.js',
    'public/backend/js2/jquery-confirm.min.js',
    'public/backend/js2/toastr.min.js',
    'public/backend/js2/photoviewer.min.js',
    'public/backend/js2/moment.js',
    'public/backend/js2/custom.js',
    'public/backend/js2/cropper.min.js',
    'public/backend/js2/jquery.simplePagination.js',
    'public/backend/js/jquery.scrollTo.js',
    'public/backend/js/monthly.js',
    'public/backend/js/flatpickr.js',
], 'public/backend/dist/vendor.js')

// mix.scripts([
//     'public/backend/js2/constant.js',
//     'public/backend/js2/helper.js',
//     'public/backend/js2/validation.js',
//     'public/backend/js2/util.js',
//     'public/backend/js2/uploadImage.js',
//     'public/backend/js2/message.js',
//     'public/backend/js2/layout.js',
// ], 'public/backend/dist/bisto.js')

mix.styles([
    'public/backend/css2/bootstrap.min.css',
    'public/backend/css2/jquery-ui.min.css',
    'public/backend/css2/style.css',
    'public/backend/css2/responsive.css',
    'public/backend/css2/cropper.min.css',
    'public/backend/css2/jquery-confirm.min.css',
    'public/backend/css2/simplePagination.css',
    'public/backend/css2/photoviewer.min.css',
    'public/backend/css2/toastr.min.css',
    'public/backend/css2/flatpickr.min.css',
    'public/backend/css2/css.css',
    'public/backend/css2/variable.css',
    'public/backend/css2/owl.carousel.min.css',
    'public/backend/css2/dripicons.css',
    'public/backend/css2/remixicon.css',
    'public/backend/css2/fontawesome.css',
    'public/backend/css2/line-awesome.min.css',
    'public/backend/css2/ionicons.min.css',
    'public/backend/css2/slick.css',
    'public/backend/css2/slick-theme.css',
    'public/backend/css2/Chart.min.css',
    'public/backend/css2/select2.min.css',
    'public/backend/css2/magnific-popup.css',
    'public/backend/css2/animate.css',
    'public/backend/css2/developer.css',
    'public/backend/css2/Ecommerce.css',
    'public/backend/css2/EcommerceResponsive.css',
    'public/backend/css2/typography1.css',
], 'public/backend/dist/vendor.css')

// mix.styles([
//     'public/backend/css2/custom.css',
// ], 'public/backend/dist/custom.css')

mix.sass('public/backend/scss/app.scss', 'public/backend/dist');
mix.sass('public/backend/scss/custom.scss', 'public/backend/dist');
