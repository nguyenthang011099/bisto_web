importScripts("https://www.gstatic.com/firebasejs/8.2.9/firebase-app.js");
importScripts(
    "https://www.gstatic.com/firebasejs/8.6.2/firebase-messaging.js",
);
// For an optimal experience using Cloud Messaging, also add the Firebase SDK for Analytics.
importScripts(
    "https://www.gstatic.com/firebasejs/8.2.9/firebase-analytics.js",
);
// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
const firebaseConfig = {
    apiKey: "AIzaSyDWblzGuvKRrbqwNoRv_3cSxpY6uGUd_BA",
    authDomain: "bistovn-vn.firebaseapp.com",
    projectId: "bistovn-vn",
    storageBucket: "bistovn-vn.appspot.com",
    messagingSenderId: "512116742705",
    appId: "1:512116742705:web:722bedd03aa9762e11d310",
    measurementId: "G-X9SQVZQW4T"
};
firebase.initializeApp(firebaseConfig);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message', payload);

    self.clients.matchAll().then(clients => {
        clients.forEach(client => client.postMessage(payload));
    });

    const audio = new Audio(`${PREFIX_API}/backend/sounds/notification.wav`);
    audio.play().then();
    // Customize notification here
    // const notificationTitle = 'Background Message Title';
    // const notificationOptions = {
    //     body: 'Background Message body.',
    //     icon: '/firebase-logo.png'
    // };
    //
    // self.registration.showNotification(notificationTitle,
    //     notificationOptions);
});

