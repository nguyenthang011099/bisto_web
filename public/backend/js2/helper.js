(function ($) {
    $.fn.serializeAllArray = function () {
        let obj = {};

        $('input', this).each(function () {
            obj[this.name] = $(this).val();
        });
        return $.param(obj);
    }
})(jQuery);

var isPrivateMode = () => {
    let fs = window.RequestFileSystem || window.webkitRequestFileSystem;
    if (!fs) {
        console.log("check failed?");
    } else {
        fs(window.TEMPORARY,
            100,
            console.log.bind(console, "not in incognito mode"),
            console.log.bind(console, "incognito mode"));
    }
}

var urlSuccess = (url) => {
    const http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status === 200;
}

/*
    Hàm thực hiện định dạng kiểu tiền khi người dùng nhập ngoài màn hình (100.111.222)
*/
formatCurrency = function (number) {
    let n = number.split('').reverse().join('');
    let n2 = n.replace(/\d\d\d(?!$)/g, '$&.');
    return n2.split('').reverse().join('');
};


// convert datetime to string such as 1 day ago
var getDateTimeAgoString = (date) => {
    let miliSeconds = Date.now() - date.getTime();
    let seconds = miliSeconds / 1000;
    const minAgo = 60, hourAgo = 3600, dayAgo = 3600 * 24, monthAgo = 3600 * 24 * 30, yearAgo = 3600 * 24 * 365;
    if (seconds < minAgo) {
        return 'vừa xong';
    } else if (seconds < hourAgo) {
        return Math.floor(seconds / 60) + ' phút trước';
    } else if (seconds < dayAgo) {
        return Math.floor(seconds / 3600) + ' giờ trước';
    } else if (seconds < monthAgo) {
        return Math.floor(seconds / (3600 * 24)) + ' ngày trước';
    } else if (seconds < yearAgo) {
        return Math.floor(seconds / (3600 * 24 * 30)) + ' tháng trước';
    } else if (seconds >= yearAgo) {
        return Math.floor(seconds / (3600 * 24 * 365)) + ' năm trước';
    } else {
        return 'vừa xong';
    }
}
/*
Chuyển cộng chuỗi sang kiểu truyền parameter
*/
if (!String.prototype.format) {
    String.prototype.format = function () {
        let args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] !== 'undefined' ? args[number] : match;
        });
    };
}

/*
Thực hiện định dạng kiểu money format 100,111,222
*/
Number.prototype.formatComma = function () {
    return this.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
};

Number.prototype.formatFullStop = function () {
    return this.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
};

Number.prototype.formatVND = function () {
    return this.toLocaleString('it-IT', { style: 'currency', currency: 'VND' });
};

String.prototype.spaceTo_ = function () {
    return this.toString().replace(/ /g, '_');
}

String.prototype.formatDate = function () {
    return moment(this).format('DD-MM-yyyy HH:mm');
}

String.prototype.summary = function (maxLength = 100) {
    if (this.length <= maxLength) return this;
    const newString = this.substring(0, maxLength);
    const lastWord = newString.substring(newString.lastIndexOf(' '));
    return `${newString.replace(lastWord, '')}...`;
}

String.prototype.hashCode = function () {
    let hash = 0, i, chr;
    if (this.length === 0) return hash;
    for (i = 0; i < this.length; i++) {
        chr = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};
/*
Thực hiện định dạng dữ liệu kiểu thời gian theo định dạng dd/MM/yyyy
*/
Date.prototype.formatddMMyyyy = function () {
    let day = this.getDate() + '';
    if (day.length === 1) {
        day = '0' + day;
    }
    let month = this.getMonth() + 1 + '';
    if (month.length === 1) {
        month = '0' + month;
    }
    let year = this.getFullYear();
    return day + '/' + month + '/' + year;
};

var imageIsExists = (image_url) => {
    const http = new XMLHttpRequest();
    http.open('HEAD', image_url, false);
    http.send();
    return http.status !== 404;
}

var createElementByText = (textHTML = '') => {
    let template = document.createElement('template');
    textHTML = textHTML.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = textHTML;
    return template.content.firstChild;
};

var createMultiElementByText = (textHTML = '') => {
    let template = document.createElement('template');
    textHTML = textHTML.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = textHTML;
    return template.content.childNodes;
};

var isFirstElementInParent = (selector = '') => {
    let $selector = selector;
    if (typeof (selector) === 'string') {
        $selector = $(selector);
    }
    if ($selector.length === 0) return undefined;
    else return $selector.prev().length === 0;
}

var isExistElement = (selector = '') => {
    return $(selector).length !== 0;
}

var moveChildElementToFirstOrder = (childSelector = '') => {
    let $child = childSelector;
    if (typeof (childSelector) === 'string') {
        $child = $(childSelector);
    }
    const $parent = $child.parent();
    const $newE = $child.clone();
    $child.remove();
    $parent.prepend($newE);
}

var cloneElement = (element) => createElementByText(element.outerHTML);

//check if string is json format
var isJsonString = (str) => {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
};

var hashCode = (value) => {
    return isEmptyValue(value) ? 0 : value.toString().hashCode();
}

//check if string is json format
var parseJSON = (str) => {
    try {
        return JSON.parse(str);
    } catch (e) {
        return undefined;
    }
};

var parseUrlParams = (query = window.location.search.substring(1)) => {
    let vars = query.split("&");
    let query_string = {};
    for (let i = 0; i < vars.length; i++) {
        let pair = vars[i].split("=");
        let key = decodeURIComponent(pair[0]);
        let value = decodeURIComponent(pair[1]);
        // If first entry with this name
        if (typeof query_string[key] === "undefined") {
            query_string[key] = decodeURIComponent(value);
            // If second entry with this name
        } else if (typeof query_string[key] === "string") {
            query_string[key] = [query_string[key], decodeURIComponent(value)];
            // If third or later entry with this name
        } else {
            query_string[key].push(decodeURIComponent(value));
        }
    }
    return query_string;
}

var isEmptyValue = (value) => {
    return value === '' ||
        value === null ||
        value === undefined ||
        (Object.keys(value).length === 0 && value.constructor === Object) ||
        (Array.isArray(value) && value.length === 0);
};
var _set_ = (value) => {
    return !isEmptyValue(value) ? value : undefined;
}
//check if a variable is HTML object
var isElement = (obj) => {
    try {
        //Using W3 DOM2 (works for FF, Opera and Chrome)
        return obj instanceof HTMLElement;
    } catch (e) {
        //Browsers not supporting W3 DOM2 don't have HTMLElement and
        //an exception is thrown and we end up here. Testing some
        //properties that all elements have (works on IE7)
        return (
            typeof obj === 'object' &&
            obj.nodeType === 1 &&
            typeof obj.style === 'object' &&
            typeof obj.ownerDocument === 'object'
        );
    }
};

var getLastParamUrl = () => window.location.href.split('/').pop().split('#')[0];

var setInputDateTime = (inputName, dateTime = new Date(), parentSelector = document) => {
    dateTime.setMinutes(dateTime.getMinutes() - dateTime.getTimezoneOffset());
    $(`input[name=${inputName}]`, parentSelector)[0].value = dateTime.toISOString().slice(0, 16);
}

var setInputDate = (inputName, date = new Date(), parentSelector = document) => {
    $(`input[name=${inputName}]`, parentSelector)[0].value = date.toISOString().substring(0, 10);
}

var setInputTime = (inputName, time = '00:00:00', parentSelector = document) => {
    $(`input[name=${inputName}]`, parentSelector).val(time);
}

var toastSuccessMessage = (message) => {
    const successMes = isEmptyValue(message) ? 'Thao tác thành công!' : message;
    toastr.success(successMes, '', {
        positionClass: 'toast-bottom-right',
    })
}

var toastSuccess = (res, defaultMessage) => {
    if (res.responseJSON) toastSuccessMessage(res.responseJSON.message);
    else toastSuccessMessage(defaultMessage);
}

var toastErrorMessage = (message) => {
    const errMes = isEmptyValue(message) ? 'Có lỗi xảy ra!' : message;
    toastr.error(errMes, '', {
        positionClass: 'toast-bottom-right',
    })
}

var toastError = (err, defaultMessage) => {
    if (err.responseJSON) toastErrorMessage(err.responseJSON.message);
    else toastErrorMessage(defaultMessage);
}

var toastInfoMessage = (message) => {
    const infoMes = isEmptyValue(message) ? 'Có lỗi xảy ra!' : message;
    toastr.info(infoMes, '', {
        positionClass: 'toast-bottom-right',
    })
}

var toastWarningMessage = (message) => {
    const warningMes = isEmptyValue(message) ? 'Cảnh báo!' : message;
    toastr.warning(warningMes, '', {
        positionClass: 'toast-bottom-right',
    })
}

var isUnexpired = (startAt, endAt) => {
    const now = new Date().getTime();
    return new Date(startAt).getTime() < now && new Date(endAt).getTime() > now;
}

var toVND = (value) => {
    return isEmptyValue(value) ? Number(0).formatVND() : value.formatVND();
}

var toDate = (value) => {
    return isEmptyValue(value) ? '' : value.formatDate();
}

var confirmDelete = (content = 'Bạn có chắc muốn xóa?', callback, title = 'Xác nhận') => {
    $.confirm({
        icon: 'fa text-warning fa-warning',
        title: title,
        content: content,
        backgroundDismiss: true,
        buttons: {
            confirm: {
                text: 'Xóa',
                btnClass: 'btn-red',
                action: function () {
                    callback();
                }
            },
            cancel: {
                text: 'Thoát'
            },
        }
    });
}

var confirmCancel = ({
    content = 'Bạn có chắc muốn hủy?',
    callback,
    title = 'Xác nhận',
    confirmText = 'Hủy đơn',
    confirmClass = 'btn-red',
    cancelText = 'Thoát'
}) => {
    $.confirm({
        icon: 'fa text-warning fa-warning',
        title: title,
        content: content,
        backgroundDismiss: true,
        buttons: {
            confirm: {
                text: confirmText,
                btnClass: confirmClass,
                action: function () {
                    callback();
                }
            },
            cancel: {
                text: cancelText
            },
        }
    });
}

var clearValidate = ({ formSelector = "#content-page", cssClassMes = 'display' }) => {
    $(`${formSelector} :input`).removeClass('is-invalid').removeClass('is-valid');
    $(`.mes-iv-display`, formSelector).addClass(`mes-v-display`).removeClass(`mes-iv-display`);
    $(`.mes-iv-visibility`, formSelector).addClass(`mes-v-visibility`).removeClass(`mes-iv-visibility`);
}
/*
* fieldFiles = [{
                          uploaderInstance: {},
                          listFiles: [{name: '', url: '', hash: '', size: 0, type: ''}]
                      }],
       fieldInputs =
                          [{selector: 'input', value: ''}, {selector: 'select', value: ''}, {selector: '', value: ''}]

* */
var dataToForm = ({
    fieldInputs = [],
    fieldFiles = [],
    parentSelector = document, action = 'clear-validate', cssClassMes = 'display'
}) => {
    for (let i = 0; i < fieldInputs.length; i++) {
        const $i = $(fieldInputs[i].selector, parentSelector);
        if ($i.attr('type') === 'datetime-local') {
            const dateTime = fieldInputs[i].value ? new Date(fieldInputs[i].value) : new Date();
            dateTime.setMinutes(dateTime.getMinutes() - dateTime.getTimezoneOffset());
            $i.val(dateTime.toISOString().slice(0, 16));
        } else if ($i.attr('type') === 'time') {
            const time = fieldInputs[i].value ?? '00:00';
            $i.val(time);
        } else if ($i.attr('type') === 'date') {
            const date = fieldInputs[i].value ? new Date(fieldInputs[i].value) : new Date();
            $i.val(date.toISOString().substring(0, 10));
        } else {
            $i.val(fieldInputs[i].value ?? '')
        }
        if (action === 'clear-validate') {
            $i.removeClass('is-invalid').removeClass('is-valid');
            //$i.css('border', '1px solid #d7dbda');
            $(`.mes-iv-${cssClassMes}`, parentSelector).addClass(`mes-v-${cssClassMes}`).removeClass(`mes-iv-${cssClassMes}`);
        }
        if (action === 'set-disable') {
            $i.attr('disabled', true)
        }
    }
    for (let i = 0; i < fieldFiles.length; i++) {
        let _totalFiles = 0;
        let _fileUrls = [];
        fieldFiles[i].uploaderInstance.setIsNewUpdateImg(false);
        fieldFiles[i].uploaderInstance.getContainer().empty();
        for (const file of fieldFiles[i].listFiles) {
            const { fileElement } = fieldFiles[i].uploaderInstance.createFileElement(file?.url ?? 'bisto_url', file?.name ?? 'bisto');
            _fileUrls.push({
                id: file?.id ?? null,
                name: file?.name ?? 'bisto',
                url: file?.url ?? 'bisto_url',
                hash: file?.hash ?? 'none',
                type: file?.type ?? 'none',
                size: file?.size ?? 0
            })
            _totalFiles++;
            fieldFiles[i].uploaderInstance.getContainer().append(fileElement);
        }
        fieldFiles[i].uploaderInstance.setTotalFile(_totalFiles);
        fieldFiles[i].uploaderInstance.setFileUrls(_fileUrls);
    }
}

var getDataFromForm = (formSelector = 'form', parentSelector = document) => {
    const array = $(`${formSelector} :input`, parentSelector).serializeArray() || [];
    let rs = {};

    for (let i = 0; i < array.length; i++) {
        rs[array[i].name] = array[i].value;
    }
    return rs;
}

var formToData = (formSelector = 'form', parentSelector = document, fieldsTimeStamp = []) => {
    const _inputs = $(`${formSelector} :input`, parentSelector);
    let inputs = [];
    for (let i = 0; i < _inputs.length; i++) {
        if (!isEmptyValue(_inputs[i].name)) inputs.push(_inputs[i]);
    }
    let rs = {};
    for (let i = 0; i < inputs.length; i++) {
        if (Array.isArray(rs[inputs[i].name])) {
            rs[inputs[i].name].push(inputs[i].value);
        } else if (rs[inputs[i].name]) {
            const old = rs[inputs[i].name];
            rs[inputs[i].name] = [old, inputs[i].value];
        } else rs[inputs[i].name] = inputs[i].value;
    }

    for (let i = 0; i < fieldsTimeStamp.length; i++) {
        if (!isEmptyValue(rs[fieldsTimeStamp[i]]))
            rs[fieldsTimeStamp[i]] = moment(rs[fieldsTimeStamp[i]]).format();
    }
    return rs;
}

var playSoundNotification = () => {
    const audio = new Audio(`${PREFIX_API}/backend/sounds/notification.wav`);
    audio.play().then();
}

var playSoundMessage = () => {
    const audio = new Audio(`${PREFIX_API}/backend/sounds/message.wav`);
    audio.play().then();
}


const animateTitle = () => {
    const defaultTitle = document.title;
    let instanceInterval;
    let beforeTitle = defaultTitle;
    let status = 'normal';
    const flashTitle = (mainTitle = defaultTitle, extraTitle, interval = 2200) => {
        clearInterval(instanceInterval);
        let i = true;
        beforeTitle = mainTitle;
        instanceInterval = setInterval(function () {
            document.title = i ? beforeTitle : extraTitle;
            i = !i;
        }, interval);
        status = 'flash';
    }

    const updateTitle = (mainTitle) => {
        beforeTitle = mainTitle;
        if (status !== 'flash') {
            setTitle(mainTitle);
        }
    }

    const movingTitle = (writeText, interval, visibleLetters) => {
        let _instance = {};
        let _currId = 0;
        let _numberOfLetters = writeText.length;

        function updateTitle() {
            _currId += 1;
            if (_currId > _numberOfLetters - 1) {
                _currId = 0;
            }
            let startId = _currId;
            let endId = startId + visibleLetters;
            let finalText;
            if (endId < _numberOfLetters - 1) {
                finalText = writeText.substring(startId, endId);
            } else {
                let cappedEndId = _numberOfLetters;
                endId = endId - cappedEndId;

                finalText = writeText.substring(startId, cappedEndId) + writeText.substring(0, endId);
            }

            document.title = finalText;
        }

        instanceInterval = setInterval(updateTitle, interval);
        return _instance;
    }

    const stop = () => {
        clearInterval(instanceInterval);
    }

    const setTitle = (title = '') => {
        if (instanceInterval)
            clearInterval(instanceInterval);
        document.title = title;
        status = 'normal';
    }
    const getInterval = () => instanceInterval;
    return {
        movingTitle,
        flashTitle,
        stop,
        setTitle,
        getInterval,
        updateTitle
    }
}

var AnimateTitle = animateTitle();


const from = "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆĞÍÌÎÏİŇÑÓÖÒÔÕØŘŔŠŞŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇğíìîïıňñóöòôõøðřŕšşťúůüùûýÿžþÞĐđßÆa";
const to = "AAAAAACCCDEEEEEEEEGIIIIINNOOOOOORRSSTUUUUUYYZaaaaaacccdeeeeeeeegiiiiinnooooooorrsstuuuuuyyzbBDdBAa------";

const UNICODE = [
    {
        from: '·|/|_|,|:|;',
        to: '-'
    },
    {
        from: 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
        to: 'a',
    },
    {
        from: 'đ',
        to: 'd',
    },
    {
        from: 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
        to: 'e',
    },
    {
        from: 'í|ì|ỉ|ĩ|ị',
        to: 'i',
    },
    {
        from: 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
        to: 'o',
    },
    {
        from: 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
        to: 'u',
    },
    {
        from: 'ý|ỳ|ỷ|ỹ|ỵ',
        to: 'y',
    },
    {
        from: 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
        to: 'A',
    }, {
        from: 'Đ',
        to: 'D',
    },
    {
        from: 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
        to: 'E',
    },
    {
        from: 'Í|Ì|Ỉ|Ĩ|Ị',
        to: 'I',
    },
    {
        from: 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
        to: 'O',
    },
    {
        from: 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
        to: 'U',
    },
    {
        from: 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
        to: 'Y',
    }
]

const MAX_SLUG_LENGTH = 200;
var convertToSlug = function (str, addString = '') {
    str += addString;
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    for (let i = 0; i < UNICODE.length; i++) {
        str = str.replace(new RegExp(UNICODE[i].from, 'g'), UNICODE[i].to);
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes
    if (str.length > MAX_SLUG_LENGTH)
        str = str.substr(0, MAX_SLUG_LENGTH);

    return str;
};

var randomString = function (length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}
