var handleLayout = ({id = 'shop'}) => {
    let dt = {title: ''}
    if (id === 'shop') {
        dt.title = 'Trang quản lý shop';
    } else if (id === 'admin-shop') {
        dt.title = 'Trang quản lý admin shop';
    } else if (id === 'admin') {
        dt.title = 'Trang quản lý admin';
    }
    if (localStorage.getItem('isHideMenu') === '1') {
        $('body').removeClass('sidebar-main');
        $('.wrapper-menu').removeClass('open');
    } else {
        $('body').addClass('sidebar-main');
        $('.wrapper-menu').addClass('open');
    }
    let countNotif = 0;
    let countMess = 0;
    const $notifContainer = $('#notification-container');
    const $messContainer = $('#notif-message-container');
    const $countNote = $('#countNote');
    const $countMes = $('#count-message');
    const $title = $('title');
    let currentPage = 1, isSeenNotification = false;
    let updateTimeAgoInterval;

    window.addEventListener('load', function () {
        loadNotification(1);
    });

    function updateTimeAgo() {
        clearInterval(updateTimeAgoInterval);
        updateTimeAgoInterval = setInterval(() => {
            const createdAts = $('.created_at_notif');
            const agoTimes = $('.ago-time');
            for (let i = 0; i < createdAts.length; i++) {
                const old = moment(createdAts[i].value).valueOf();
                agoTimes[i].textContent = getDateTimeAgoString(new Date(old));
            }
        }, 1000 * 60);
    }

    FCM.onComingMessage((payload) => {
        console.log(payload);
        updateTimeAgo();
        if (payload?.data?.isMes === undefined) {
            isSeenNotification = false;
            countNotif += 1;
            AnimateTitle.updateTitle(`(${countMess + countNotif}) ${dt.title}`);
            const notif = payload.notification;
            $notifContainer.prepend(
                ` <a href="#" class="iq-sub-card">
                                        <div class="media align-items-center">
                                            <div class="media-body ml-3">
                                                <input class='id-notification' hidden value=${payload?.data?.notif_id ?? 0}>
                                                <input class="created_at_notif" type="datetime-local" hidden value="${payload?.data?.created_at ? moment(payload.data.created_at).format('YYYY-MM-DDTHH:mm:ss') : moment().format('YYYY-MM-DDTHH:mm:ss')}">
                                                <div class="d-flex justify-content-between align-items-center">
                                                   <h6 class="mb-0 ">${notif?.title ?? ''}</h6>
                                                    <span class="float-right ago-time font-size-12">${getDateTimeAgoString(new Date(payload?.data?.created_at))}</span>
                                                </div>
                                                <p class="mb-0">${notif?.body ?? ''}</p>
                                            </div>
                                        </div>
                                    </a>`
            );
            playSoundNotification();
        } else if (payload?.data?.isMes === '1') {
            const message = {
                content: "",
                conversation_id: 0,
                id: 0,
                full_name: '',
                receiver_avatar: DEFAULT_AVATAR,
                source_urls: [],
                type: 0,
                user_id: 0,
                ...payload.data
            }
            // updateTimeAgo();
            // isSeenNotification = false;
            const content = parseJSON(message.source_urls)?.length === 0 ? message.content : 'Đã gửi ảnh';
            if ($(`.notif-mes-${message.user_id}`).length > 0) {
                $(`.notif-mes-${message.user_id}`).find('small').text(`${getDateTimeAgoString(new Date())}`);
                $(`.notif-mes-${message.user_id}`).find('.created_at_notif').val(`${moment().format('YYYY-MM-DDTHH:mm:ss')}`);
                $(`.notif-mes-${message.user_id}`).find('h6').text(content);
            } else {
                countMess += 1;
                $messContainer.prepend(
                    `       <a data-receiver_id="${message.user_id}" href="${PREFIX_API}/${id}/chats#${message.user_id}" class="iq-sub-card notif-mes-class notif-mes-${message.user_id}">
                                            <div class="media align-items-center">
                                                <div class="">
                                                    <img class="avatar-40 rounded"
                                                         src="${message.receiver_avatar}" alt="${message.full_name}">
                                                </div>
                                                <input class="created_at_notif" type="datetime-local" hidden value="${moment().format('YYYY-MM-DDTHH:mm:ss')}">
                                                <div class="media-body ml-3">
                                                    <h6 class="mb-0 ">${content}</h6>
                                                    <small class="ago-time float-left font-size-12">${getDateTimeAgoString(new Date())}</small>
                                                </div>
                                            </div>
                                        </a>`
                );
            }
            AnimateTitle.flashTitle(`(${countMess + countNotif}) ${dt.title}`, `${message.full_name} đã nhắn`);
            $countMes.text(countMess);
            playSoundMessage();
        }
    });

    // $(document).on('click', '.notif-mes-class', function (e) {
    //     debugger
    //     _GLOBAL_.message.receiver_id = e.currentTarget.dataset.receiver_id;
    // })

    $('#btn-show-notification').on('click', function (e) {
        const listInput = $('.id-notification');
        let listId = [];
        for (let i = 0; i < listInput.length; i++) {
            listId.push(listInput[i].value)
        }
        if (listId.length > 0 && !isSeenNotification)
            $.ajax({
                url:
                    `${PREFIX_API}/${id}/notification-status`,
                method: 'put',
                data: {
                    _token: _TOKEN,
                    listId: listId
                },
                success: function (obj) {
                    countNotif = obj.count;
                    $countNote.text(countNotif);
                    AnimateTitle.updateTitle(`(${countMess + countNotif}) ${dt.title}`);
                    isSeenNotification = true;
                }
            })
    })
    ;

    $(document).on('click', '#btn-show-more span', function (e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        loadNotification(currentPage + 1);
    })

// when shop click show more
    function loadNotification(page = 1) {
        currentPage = page;
        $.ajax({
            url: `${PREFIX_API}/${id}/get-notifications/${page}`,
            method: "GET",
        }).done(res => {
            // $('.notification-big').addClass('iq-show');
            const notifications = res?.results ?? [];
            let notificationHTML = '';
            countNotif = res?.countSeenNotification ?? 0;
            $countNote.text(countNotif);
            for (const notif of notifications) {
                notificationHTML += ` <a href="#" class="iq-sub-card">
                                        <div class="media align-items-center">
                                            <div class="media-body ml-3">
                                                <input class='id-notification' hidden value=${notif.id}>
                                                <input type="datetime-local" class="created_at_notif" hidden value="${moment(notif.created_at).format('YYYY-MM-DDTHH:mm:ss')}">
                                                <div class="d-flex justify-content-between align-items-center">
                                                   <h6 class="mb-0 ">${notif?.title ?? ''}</h6>
                                                    <span class="float-right ago-time font-size-12">${getDateTimeAgoString(new Date(notif.created_at))}</span>
                                                </div>
                                                <p class="mb-0">${notif?.content ?? ''}</p>
                                            </div>
                                        </div>
                                    </a>`;
            }
            $notifContainer.append(notificationHTML);
            if (notifications.length === 0) {
                notificationHTML = ` <a href="#" class="iq-sub-card">
                                            <div class="media align-items-center">
                                                <div class="media-body ml-3">
                                                    <h6 class="mb-0 ">Không có thông báo nào</h6>
                                                </div>
                                            </div>
                                        </a>`;
                $notifContainer.empty().append(notificationHTML);
            }

            if (res.haveMore === 1) {
                $('#btn-show-more').show();
            } else $('#btn-show-more').hide();
            updateTimeAgo();
        }).fail(err => {
        })
    }

}
