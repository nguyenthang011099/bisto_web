var defineMessage = ({oneFcmOneUser = true}) => {
    const firebaseConfig = {
        apiKey: "AIzaSyDWblzGuvKRrbqwNoRv_3cSxpY6uGUd_BA",
        authDomain: "bistovn-vn.firebaseapp.com",
        projectId: "bistovn-vn",
        storageBucket: "bistovn-vn.appspot.com",
        messagingSenderId: "512116742705",
        appId: "1:512116742705:web:722bedd03aa9762e11d310",
        measurementId: "G-X9SQVZQW4T"
    };

    firebase.initializeApp(firebaseConfig);
    const messaging = firebase.messaging();

    function handleFcmEvent(sentToServer) {
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register('/firebase-messaging-sw.js').then((registration) => {
                navigator.serviceWorker.addEventListener('message', function (event) {
                    // console.log('background');
                    if (document.visibilityState === 'hidden') {
                        const e = new CustomEvent('on-coming-message', {detail: event.data});
                        document.dispatchEvent(e);
                    }
                });
                const keyPair = 'BJT5V4kZIr82rdzxhaRm_4gp2i8fTcfZ1wMO6kH2AlGNpJqwZCCbXNSq7NweL6FCq1_lMBBNPh73fIHPOYDlaA8';
                messaging.getToken({
                    serviceWorkerRegistration: registration,
                    vapidKey: keyPair
                }).then((currentToken) => {
                    if (currentToken) {
                        const user_device_id = sentToServer.user_device_id;
                        $.ajax({
                            url: `${PREFIX_API}/fcm-token/${user_device_id}`,
                            method: 'PUT',
                            data: {
                                _token: _TOKEN,
                                fcm_token: currentToken
                            }
                        }).done(res => {
                            sentToServer.user_id = USER_ID;
                            sentToServer.hash = hashCode(currentToken);
                            window.localStorage.setItem('sentToServer', JSON.stringify(sentToServer));
                        }).fail(err => {
                            setTokenSentToServer(false);
                        })
                    } else {
                        // Show permission request UI
                        console.log('No registration token available. Request permission to generate one.');
                        setTokenSentToServer(false);
                    }
                }).catch((err) => {
                    console.log('An error occurred while retrieving token. ', err);
                    setTokenSentToServer(false);
                });
            });
        }
    }

    function resetUI() {
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register('/firebase-messaging-sw.js').then((registration) => {
                navigator.serviceWorker.addEventListener('message', function (event) {
                    // console.log('background');
                    if (document.visibilityState === 'hidden') {
                        const e = new CustomEvent('on-coming-message', {detail: event.data});
                        document.dispatchEvent(e);
                    }
                });

                const keyPair = 'BJT5V4kZIr82rdzxhaRm_4gp2i8fTcfZ1wMO6kH2AlGNpJqwZCCbXNSq7NweL6FCq1_lMBBNPh73fIHPOYDlaA8';
                messaging.getToken({
                    serviceWorkerRegistration: registration,
                    vapidKey: keyPair
                }).then((currentToken) => {
                    if (currentToken) {
                        // console.log(currentToken)
                        sendTokenToServer(currentToken);
                    } else {
                        // Show permission request UI
                        console.log('No registration token available. Request permission to generate one.');
                        setTokenSentToServer(false);
                    }
                }).catch((err) => {
                    console.log('An error occurred while retrieving token. ', err);
                    setTokenSentToServer(false);
                });
            });
        }
    }


    function sendTokenToServer(currentToken) {
        if (!isTokenSentToServer(currentToken)) {
            $.ajax({
                url: `${PREFIX_API}/fcm-token`,
                method: 'POST',
                data: {
                    _token: _TOKEN,
                    fcm_token: currentToken,
                    message_notification_type: 3,
                }
            }).done(res => {
                console.log('fcm added')
                setTokenSentToServer(true, res?.user_device?.id ?? 0, currentToken);
            }).fail(err => {
                setTokenSentToServer(false);
            });
        }
    }

    //{user_id,is_sent}
    //1 fcm per 1 user
    let isTokenSentToServer = () => {
    };
    let setTokenSentToServer = (sent, user_device_id) => {
    };

    if (oneFcmOneUser) {
        isTokenSentToServer = (currentToken = '') => {
            const sentToServer = parseJSON(localStorage.getItem('sentToServer'));
            if (sentToServer === undefined || sentToServer === null) return false;
            return sentToServer.user_id === USER_ID && sentToServer.is_sent === '1' && hashCode(currentToken) === sentToServer.hash;
        }

        setTokenSentToServer = (sent, user_device_id = '0', currentToken = '') => {
            const _is_sent = sent ? '1' : '0';
            let sentToServer = parseJSON(localStorage.getItem('sentToServer'));
            if (sentToServer === undefined || sentToServer === null || sentToServer.user_id !== USER_ID) {
                sentToServer = {user_id: USER_ID, is_sent: _is_sent, user_device_id, hash: hashCode(currentToken)};
            } else {
                sentToServer.is_sent = _is_sent;
                sentToServer.user_device_id = user_device_id;
                sentToServer.hash = hashCode(currentToken);
            }
            window.localStorage.setItem('sentToServer', JSON.stringify(sentToServer));
        }
    }

    //[{user_id, is_sent}]
    //1 fcm per many user
    // function isTokenSentToServer() {
    //     const sentToServers = parseJSON(localStorage.getItem('sentToServers'));
    //     if (sentToServers === undefined || sentToServers === null) return false;
    //     return sentToServers.find(item => item.user_id === USER_ID) !== undefined && sentToServers.find(item => item.user_id === USER_ID)['is_sent'] === '1';
    // }
    //
    // function setTokenSentToServer(sent) {
    //     const _is_sent = sent ? '1' : '0';
    //     let sentToServers = parseJSON(localStorage.getItem('sentToServers'));
    //     if (sentToServers === undefined || sentToServers === null) {
    //         sentToServers = [{user_id: USER_ID, is_sent: _is_sent}];
    //     } else {
    //         const index = sentToServers.findIndex(item => item.user_id === USER_ID);
    //         if (index === -1) sentToServers.push({user_id: USER_ID, is_sent: _is_sent});
    //         else sentToServers[index]['is_sent'] = _is_sent;
    //     }
    //     window.localStorage.setItem('sentToServers', JSON.stringify(sentToServers));
    // }

    function requestPermission() {
        Notification.requestPermission().then((permission) => {
            if (permission === 'granted') {
                console.log('Notification permission granted.');
                resetUI();
            } else if (permission === 'denied') {
                if (isAlertShowAgain())
                    alertNotification();
                console.log('Unable to get permission to notify.');
            }
        });
    }

    function deleteToken() {
        // Delete registration token.
        messaging.getToken().then((currentToken) => {
            messaging.deleteToken(currentToken).then(() => {
                console.log('Token deleted.');
                setTokenSentToServer(false);
                resetUI();
            }).catch((err) => {
                console.log('Unable to delete token. ', err);
            });
        }).catch((err) => {
            console.log('Error retrieving registration token. ', err);
        });
    }

    const onComingMessage = (callback) => {
        document.addEventListener('on-coming-message', function (e) {
            callback(e.detail);
        })
    }

    messaging.onMessage(function (payload) {
        // console.log('foreground');
        if (document.visibilityState === 'visible') {
            const event = new CustomEvent('on-coming-message', {detail: payload});
            document.dispatchEvent(event);
        }
        //let enableForegroundNotification = true;
        // if (enableForegroundNotification) {
        //     let notification = payload.notification;
        //     navigator.serviceWorker
        //         .getRegistrations()
        //         .then((registration) => {
        //             registration[0].showNotification(notification.title);
        //         });
        // }
    });

    const setAlertShowAgain = (show) => {
        const _is_show_again = show ? '1' : '0';
        let alertShowAgain = parseJSON(localStorage.getItem('alertShowAgain'));
        if (alertShowAgain === undefined || alertShowAgain === null || alertShowAgain.user_id !== USER_ID) {
            alertShowAgain = {user_id: USER_ID, is_show_again: _is_show_again};
        } else {
            alertShowAgain.is_show_again = _is_show_again;
        }
        window.localStorage.setItem('alertShowAgain', JSON.stringify(alertShowAgain));
    }

    const isAlertShowAgain = () => {
        const alertShowAgain = parseJSON(localStorage.getItem('alertShowAgain'));
        if (alertShowAgain === undefined || alertShowAgain === null) return false;
        return alertShowAgain.user_id === USER_ID && alertShowAgain.is_show_again === '1';
    }

    const alertNotification = () => {
        $.alert({
            icon: 'fa fa-warning',
            content: `<div>
                <p>
                    Để nhận thông báo và tin nhắn hãy "cho phép thông báo trong cửa sổ trang web",
                <p>
                <div><a class="btn-link" target="_blank" href="https://support.google.com/chrome/answer/3220216?hl=vi">Hướng dẫn bật thông báo</a></div>
                <div class="form-check">
                <input type="checkbox" class="form-check-input" id="popup-show-again">
                    <label class="form-check-label" for="popup-show-again">
                        Không hiển thị lại
                    </label>
                </div>
            </div>`,
            title: 'Thông báo bị chặn',
            container: '#confirm-container',
            buttons: {
                close: {
                    text: 'Đóng',
                    action: function () {
                        setAlertShowAgain(!$('#popup-show-again').prop('checked'));
                    }
                },
            }
        });
    }

    $(document).ready(function () {
        const alertShowAgain = parseJSON(localStorage.getItem('alertShowAgain'));
        if (alertShowAgain === undefined || alertShowAgain === null || alertShowAgain?.user_id !== USER_ID) setAlertShowAgain(true);
        const sentToServer = parseJSON(localStorage.getItem('sentToServer'));
        if (sentToServer === undefined || sentToServer === null) return requestPermission();
        if (sentToServer?.user_id !== USER_ID && sentToServer?.is_sent === '1' && sentToServer?.user_device_id !== '0') {
            handleFcmEvent(sentToServer);
        } else requestPermission();
    });

    return {
        messaging,
        deleteToken,
        requestPermission,
        onComingMessage,
    }
}

var FCM = defineMessage({});


