//get random name from file
var getRandomFileName = (imageFile = {name: ''}) => {
    let u = Date.now().toString(16) + Math.random().toString(16) + '0'.repeat(16);
    let guid = [u.substr(0, 8), u.substr(8, 4), '4000-8' + u.substr(13, 3), u.substr(16, 12)]
        .join('-');
    return (guid + '.' + imageFile.name.split('.').pop());
}

var definePreviewImage = ({
                              id = 'img1', getFileUrls = () => [{name: '', url: ''}]
                          }) => {
    //modal preview image
    const modalPreviewImageHTML =
        `<div id="modal-preview-img-${id}" class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title modalLabel" id="modalLabel-${id}">Xem ảnh</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
               <div class="col-1 prev-preview super-center">
                    <i class="fa fa-chevron-left fa-3x" aria-hidden="true"></i>
               </div>
               <div class="img-cover col-10" style="min-height: 33rem">
                    <img name="" class="${id}-img-preview" src="" alt="bisto"/>
                </div>
                <div class="col-1 next-preview super-center">
                     <i class="fa fa-chevron-right fa-3x" aria-hidden="true"></i>
                </div>
            </div>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>`

    const modalPreviewImageElement = createElementByText(modalPreviewImageHTML);
    let imgPreview = modalPreviewImageElement.querySelector(`img.${id}-img-preview`);
    const $modalPreviewImageElement = $(modalPreviewImageElement);
    modalPreviewImageElement.querySelector('.next-preview').addEventListener('click', function () {
        const index = getFileUrls().findIndex(item => item.name === imgPreview.getAttribute('name'));
        if (index === getFileUrls().length - 1) {
            return;
        }
        imgPreview.src = getFileUrls()[index + 1].url;
        imgPreview.setAttribute('name', getFileUrls()[index + 1].name);
    })

    modalPreviewImageElement.querySelector('.prev-preview').addEventListener('click', function () {
        const index = getFileUrls().findIndex(item => item.name === imgPreview.getAttribute('name'));
        if (index === 0) {
            return;
        }
        imgPreview.src = getFileUrls()[index - 1].url;
        imgPreview.setAttribute('name', getFileUrls()[index - 1].name);
    })

    return {
        modalPreviewImageElement,
        $modalPreviewImageElement,
        imgPreview
    }
}

var openPhotoViewer = ({fileUrls = [], currentName = '', options = {}}) => {
    const index = fileUrls.findIndex(item => item.name === currentName);
    const photos = fileUrls.map(item => {
        return {src: item.url, title: item.name}
    })
    const viewer = new PhotoViewer(photos, {
        index, ...options
    });
}

var loadImage = (src, img, nullSrc) => {
    return new Promise((resolve, reject) => {
        const tmp = new Image();
        tmp.onload = () => {
            img.src = src;
            resolve({src, img});
        };

        tmp.onerror = () => {
            img.src = nullSrc;
            reject({src, img});
        };
        tmp.src = src;
    });
}


var defineImage = ({mode = 'html', optionsPhotoviewers = {}, id = 'default'}) => {
    let photos = [];
    const toElement = (src, title) => {
        return createElementByText(toHTML(src, title));
    }

    const me = this;
    const resetPhotos = () => {
        photos = [];
        $(document).off('click', `.preview-img-${id}`);
        $(document).on('click', `.preview-img-${id}`, view);
        return me;
    }

    const resetEvent = () => {
        $(document).off('click', `.preview-img-${id}`);
        $(document).on('click', `.preview-img-${id}`, view);
        return me;
    }

    const resetIndexPhotos = ({parentSelector = document}) => {
        resetPhotos();
        const imgElements = $('.image-bisto', parentSelector);
        const length = imgElements.length;
        for (let i = 0; i < length; i++) {
            imgElements[i].id = `img-container-bisto-${i}`;
            imgElements[i].querySelector('img').dataset.index = `${i}`;
            imgElements[i].querySelector('.preview-image-css').dataset.index = `${i}`;
            photos.push({
                src: imgElements[i].querySelector('img').src,
                title: imgElements[i].dataset.title,
            })
        }
    }

    resetPhotos();

    let ratio_class, optionsPhotoviewer = {};
    switch (id) {
        case 'banner':
        case 'flashsale':
            ratio_class = '13';
            break;
        case 'default':
        case 'size':
        case 'order-return':
        case 'product':
        case 'license':
        case 'message':
            ratio_class = 'normal';
            break;
        case 'category':
            ratio_class = 'small';
            optionsPhotoviewer = {
                callbacks: {
                    opened: function (context) {
                        $('.photoviewer-stage').addClass('photoviewer-stage-svg');
                    },
                }
            };
            break;
        default :
            ratio_class = 'normal';
    }

    const toHTML = (src, title) => {
        photos.push({src, title});
        const index = photos.length - 1;
        return `
            <div class="img-container-hover-bisto image-bisto" id="img-container-bisto-${index}" data-title="${title}">
               <div class="position-relative">
                    <img data-index="${index}" class="bisto-img-upload bisto-img-onload img-fluid img-ratio-${ratio_class} " src=${src} alt="bisto-${title}"/>
                     <div class="d-none maybe-super-center loading-image lds-ellipsis-container-${ratio_class}">
                        <div class="lds-ellipsis super-center">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                        </div>
                      </div>
                    <div class="middle">
                        <div class="preview-img-${id} preview-image-css" data-index=${index}>
                        <i class="fas fa-eye fa-lg" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }

    const handleOnload = ({callback, parentSelector = document}) => {
        const loadingImg = new Image()
        loadingImg.onload = function () {
            callback();
            const imgElements = $('.bisto-img-onload', parentSelector);
            for (let i = 0; i < imgElements.length; i++) {
                imgElements[i].src = `${PREFIX_API}/backend/images/loading-${ratio_class}.gif`;
                loadImage(photos[i].src, imgElements[i]).then(result => {
                }).catch(err => {
                });
            }
        }
        loadingImg.src = `${PREFIX_API}/backend/images/loading-${ratio_class}.gif`;
    }

    const afterOnload = ({
                             parentSelector = document,
                             callbackAppend,
                             tbodyHtml = '',
                             tbodySelector = '.table-striped>tbody',
                             colspan = 0
                         }) => {
        const tbodyElements = createMultiElementByText(tbodyHtml);
        const length = tbodyElements.length;
        const ar = [...tbodyElements];
        let imgElements = [];
        for (let i = 0; i < length; i++) {
            imgElements.push(ar[i].querySelector('.bisto-img-onload'));
        }
        let promises = [];
        for (let i = 0; i < length; i++) {
            promises.push(loadImage(photos[i].src, imgElements[i], `${PREFIX_API}/backend/images/null-${ratio_class}.jpg`));
            Promise.allSettled(promises).then((results) => {
                if (results.length === length) {
                    const $container = $(tbodySelector, parentSelector).empty();
                    $container.append(ar);
                }
            })
        }
    }

    const animateOnloadImage = ({beginIndex = 0, parentSelector = document}) => {
        const length = photos?.length ?? 0;
        for (let i = beginIndex; i < length; i++) {
            const imgElementContainer = $(`#img-container-bisto-${i}`, parentSelector);
            const img = imgElementContainer[0]?.querySelector('img')
            if (isEmptyValue(img)) return;
            img.classList.add('d-none');
            const loading = imgElementContainer.find(`.loading-image`);
            loading.removeClass('d-none');
            loadImage(photos[i].src, img, `${PREFIX_API}/backend/images/null-${ratio_class}.jpg`).then(result => {
                loading.removeClass('maybe-super-center').hide();
                img.classList.remove('d-none');
            }).catch(err => {
                loading.removeClass('maybe-super-center').hide();
                img.classList.remove('d-none');
            });
        }
    }

    const getPhotos = () => photos;

    function view(e) {
        const viewer = new PhotoViewer(getPhotos(), {
            index: parseInt(e.currentTarget.dataset.index), ...optionsPhotoviewer
        });
    }

    const toMultiHTML = (_photos, options = {numberImageInRow: 2, padding: 3}) => {
        let container = `
              <div class="container px-lg-${options.padding}">
                <div class="container-photos row mx-lg-n${options.padding} row-cols-${options.numberImageInRow}">
        `;
        for (const photo of _photos) {
            container += `<div class="col super-center">${toHTML(photo.src, photo.title)}</div>`
        }
        container += `</div></div>`
        return container;
    }

    const toMultiElement = (_photos, numberImageInRow = 2) => {
        let elements = []
        for (const photo of _photos) {
            elements.push(toElement(photo.src, photo.title));
        }
        return elements;
    }

    return {
        toElement,
        toMultiElement,
        toHTML,
        toMultiHTML, getPhotos, resetPhotos, handleOnload, afterOnload, animateOnloadImage, resetEvent, resetIndexPhotos
    }
}


const installFirebase = ({}) => {
    const firebaseConfig = {
        apiKey: "AIzaSyBK9eScBvaK3GH0CQzAZfozawSkVwe-_eA",
        authDomain: "fir-web-c6c09.firebaseapp.com",
        projectId: "fir-web-c6c09",
        storageBucket: "fir-web-c6c09.appspot.com",
        messagingSenderId: "231147196503",
        appId: "1:231147196503:web:1ec701425caa4c695e9312",
        measurementId: "G-Y376EFELYG"
    };
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }

    const storage = firebase.storage;

    const getStorageRef = (url) => {
        return firebase.storage().ref(url);
    }

    const getUploadTask = (url, file, metadata = {
        cacheControl: 'max-age=7200',
    }) => {
        return getStorageRef(url).put(file, metadata)
    }

    const getRefFromUrl = (url) => firebase.storage().refFromURL(url);

    return {
        firebase,
        getStorageRef,
        getUploadTask,
        storage,
        getRefFromUrl
    }
}

//module upload file, image, preview image,delete image
var defineUploadFile = ({
                            type = 'image',
                            id = 'product',
                            imageName = 'imageUrl',
                            inputSelector = null,
                            containerSelector = '#img-container', maxFiles = 10, cbWhenAddedFile = function () {
    }, optionsPhotoViewer = {}, maxFileSize = 2000000, fileExtensions = ['gif', 'png', 'jpg', 'jpeg']
                            , cropperInstance = undefined
                            , cropperOption = undefined
                        }) => {

    let totalFiles = 0;
    let fileUrls = [];
    const getFileUrls = () => fileUrls;
    const container = document.querySelector(containerSelector);
    const $container = $(containerSelector);
    if (inputSelector) {
        const input = document.querySelector(inputSelector);
        //when user choose file to upload
        input.addEventListener('change', function (e) {
            if (totalFiles + e.target.files.length > maxFiles) {
                return alert(`Bạn không thể chọn quá ${maxFiles} ảnh`);
            } else {
                for (let i = 0; i < e.target.files.length; i++) {
                    const file = e.target.files[i];
                    const extension = file.name.split('.').pop().toLowerCase();
                    const fsize = file.size || file.fileSize;
                    if (fsize > maxFileSize)
                        return alert(`Ảnh thứ ${i + 1} kích thước quá lớn! ,tối đa ${maxFileSize / 1000000} MB`);
                    if (jQuery.inArray(extension, fileExtensions) === -1)
                        return alert(`File thứ ${i + 1} không hợp lệ!, cho phép các định dạng ${fileExtensions.toString()}`);
                    uploadSingleFile(file);
                }
            }
        })
    }

    function uploadSuccessProgress(_progress) {
        _progress.show().removeClass('progress-bar-striped progress-bar-animated').addClass('bg-success').css('width', '100%').attr('aria-valuenow', 100);
    }

    const createFileElement = (imgUrl, imgName = '') => {
        let html;
        html = `
        <div class="${id}-wrapper-upload col super-center wrapper-upload-bisto-img">
            <div class="img-cover img-container-hover-${id} img-container-hover-bisto-modal">
                <img name=${imgName} class="${id}-img-upload shadow bisto-img-upload-modal" src=${imgUrl} alt="bisto"/>
                <div class="middle">
                    <div class="mr-4 delete-file-${id}">
                        <i class="fas fa-trash fa-lg" aria-hidden="true"></i>
                    </div>
                    <div class="preview-upload-${id}">
                        <i class="fas fa-eye fa-lg" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="progress" style="margin: auto;width: 200px;">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                    aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">0%</div>
            </div>
            <input type="hidden" name="${imageName}" value=${imgUrl} />
        </div>`
        if (id === "message") {
            html = `
            <div class="${id}-wrapper-upload super-center col">
                <div class="img-container-hover-bisto ${id}-wrapper-upload">
                        <div class="position-relative">
                            <img name=${imgName} class="${id}-img-upload bisto-img-upload img-fluid" src=${imgUrl} alt="${id}"/>
                        <div class="middle">
                            <div class="mr-4 delete-file-${id}">
                                <i class="fas fa-trash fa-lg" aria-hidden="true"></i>
                            </div>
                            <div class="preview preview-upload-${id}">
                                <i class="fas fa-eye fa-lg" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="progress progress-bisto">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                    aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">0%</div>
                </div>
                <input type="hidden" name="${imageName}" value=${imgUrl} />
            </div>
        `;
        }
        let fileElement = createElementByText(html);
        const img = fileElement.querySelector(`img.${id}-img-upload`);
        const deleteButton = fileElement.querySelector(`.delete-file-${id}`);
        const previewButton = fileElement.querySelector(`.preview-upload-${id}`);
        const inputHidden = fileElement.querySelector(`input[name=${imageName}]`);
        const $progress = $(fileElement).find('.progress');
        const $progressBar = $(fileElement).find('.progress-bar');

        if (imgUrl !== '') {
            deleteButton.addEventListener('click', function () {
                fileElement.remove();
                fileUrls.splice(fileUrls.findIndex(v => v.name === imgName), 1);
                totalFiles -= 1;
                // const urlRefer = firebase.storage().refFromURL(imgUrl);
                // urlRefer.delete().then(function () {
                //     console.log('xóa ảnh thành công')
                // }).catch(function (error) {
                //     console.log(error)
                // });
            })
            previewButton.addEventListener('click', function () {
                // imgPreview.src = imgUrl;
                // imgPreview.setAttribute('name', imgName);
                // $modalPreviewImageElement.modal('show')
                openPhotoViewer({fileUrls, currentName: imgName, options: optionsPhotoViewer});
            })
        }
        return {
            fileElement, img, deleteButton, previewButton, inputHidden, $progress, $progressBar
        }
    }

    $(document).on('hidden.bs.modal', function (event) {
        if ($('.modal:visible').length) {
            $('body').addClass('modal-open');
        }
    });
    //create DOM Element with image, preview button, delete button, progress bar,

    //upload single file to firebase
    function uploadSingleFile(file) {
        try {
            const rdFileName = getRandomFileName(file);
            const {
                fileElement,
                img,
                deleteButton,
                previewButton,
                inputHidden,
                $progress,
                $progressBar
            } = createFileElement('', rdFileName);
            deleteButton.style.opacity = '0.1';
            container.append(fileElement);
            const length = fileUrls.push({
                name: rdFileName,
                url: '',
                type: file.type,
                size: file.size,
                hash: '',
                isUploading: false
            });
            const tempFile = fileUrls[length - 1];

            let reader = new FileReader();
            reader.onload = function () {
                img.src = reader.result;
                cbWhenAddedFile();
            }
            reader.readAsDataURL(file);
            const {firebase, getStorageRef, getUploadTask, storage, getRefFromUrl} = installFirebase({});
            let uploadTask = getUploadTask(`${moment().format('YYYY/MM/DD')}/` + rdFileName, file);
            // Listen for state changes, errors, and completion of the upload.
            uploadTask.on(storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
                (snapshot) => {
                    tempFile.isUploading = true;
                    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                    const percent = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    const percentage = percent + '%';
                    $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);
                    $progress.show();
                },
                (error) => {
                    tempFile.isUploading = false;
                    $progress.hide();
                    fileUrls.splice(fileUrls.findIndex(v => v.name === rdFileName), 1);
                },
                () => {
                    tempFile.isUploading = false;
                    // Upload completed successfully, now we can get the download URL
                    uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
                        tempFile.url = downloadURL;
                        // fileUrls.find(item => item.name === rdFileName).url = downloadURL;
                        totalFiles++;
                        inputHidden.setAttribute('value', downloadURL);
                        uploadSuccessProgress($progressBar);
                        deleteButton.style.opacity = '1';
                        previewButton.addEventListener('click', function () {
                            openPhotoViewer({fileUrls, currentName: rdFileName, options: optionsPhotoViewer});
                        })
                        deleteButton.addEventListener('click', function () {
                            fileElement.remove();
                            fileUrls.splice(fileUrls.findIndex(v => v.name === rdFileName), 1);
                            totalFiles -= 1;
                            const urlRefer = getRefFromUrl(downloadURL);
                            urlRefer.delete().then(function () {
                                console.log('xóa ảnh thành công')
                            }).catch(function (error) {
                                console.log(error)
                            });
                        })
                    });
                }
            );
        } catch (e) {
            console.log(e)
            toastErrorMessage('Có Lỗi xảy ra khi upload ảnh')
        }
    }

    let defaultFileUrls = [];

    const setTotalFile = (p) => {
        totalFiles = p;
    }
    const getTotalFile = () => totalFiles;
    const setFileUrls = (p) => {
        fileUrls = p;
        totalFiles = fileUrls.length;
        defaultFileUrls = [...p];
    }
    const getDeleteImageIds = () => {
        const updatedNames = fileUrls.map(item => item.name);
        return defaultFileUrls.filter(item => !updatedNames.includes(item.name)).map(item => item.name);
    }

    const getNewImgs = () => {
        const defaultNames = defaultFileUrls.map(item => item.name);
        return fileUrls.filter(item => !defaultNames.includes(item.name)).map(item => {
            return {
                filename: item.name,
                url: item.url,
                hash: item.hash,
                type: item.type,
                size: item.size,
            }
        })
    }
    const getContainer = () => $container;
    const getOneImg = () => {
        if (fileUrls.length > 0) {
            return {
                filename: fileUrls[0].name,
                url: fileUrls[0].url,
                type: fileUrls[0].type,
                size: fileUrls[0].size,
                hash: fileUrls[0].hash,
            }
        } else return null;
    }
    let isNewUpdateImg = false;
    const _isNewUpdateImg = () => isNewUpdateImg;
    const setIsNewUpdateImg = (p) => isNewUpdateImg = p;
    const getOneImgUpdate = () => {
        if (!_isNewUpdateImg()) return null;
        if (fileUrls.length > 0) {
            return {
                filename: fileUrls[0].name,
                url: fileUrls[0].url,
                type: fileUrls[0].type,
                size: fileUrls[0].size,
                hash: fileUrls[0].hash,
            }
        } else return null;
    }

    const getMultiImg = () => {
        let rs = [];
        const length = fileUrls.length;
        if (length > 0) {
            for (let i = 0; i < length; i++) {
                rs.push({
                    filename: fileUrls[i].name,
                    url: fileUrls[i].url,
                    type: fileUrls[i].type,
                    size: parseInt(fileUrls[i].size),
                    hash: fileUrls[i].hash,
                })
            }
        }
        return rs;
    }

    const setUpdateFileUrls = (_fileUrls = [{filename: '', url: '', type: '', size: 0, hash: '', id: ''}]) => {
        const length = _fileUrls.length;
        $container.empty();
        for (let i = 0; i < length; i++) {
            const {fileElement} = createFileElement(_fileUrls[i].url, _fileUrls[i].filename);
            $container.append(fileElement);
        }
        fileUrls = _fileUrls.map(item => {
            return {
                name: item.filename,
                url: item.url,
                type: item.type,
                size: item.size,
                hash: item.hash,
            }
        })
        totalFiles = _fileUrls.length;
    }

    const validateUploading = (mesSelector = '.mes', parentSelector = document) => {
        const $mes = $(mesSelector, parentSelector);
        for (let i = 0; i < fileUrls.length; i++) {
            if (fileUrls[i]?.isUploading === true) {
                $mes.text('Vui lòng chờ ảnh đang được tải lên').removeClass('mes-v-display').addClass('mes-iv-display');
                return false;
            }
        }
        $mes.removeClass('mes-iv-display').addClass('mes-v-display');
        return true;
    }

    //handle cropper
    // if (cropperInstance) {
    //     cropperInstance.setCbWhenAddFile(function () {
    //         if (totalFiles > maxFiles) {
    //             toastInfoMessage(`Cho phép chọn tối đa ${maxFiles} ảnh`);
    //             cropperInstance.stopUpload();
    //         }
    //     })
    //
    //     cropperInstance.setType(id);
    //
    //     cropperInstance.setFileExtensions(fileExtensions);
    //
    //     cropperInstance.saveButton.addEventListener('click', function () {
    //         const {fileElement} = createFileElement(cropperInstance?.getImageUrl(), cropperInstance?.getImgName());
    //         fileUrls.push(cropperInstance.getImg());
    //         totalFiles++;
    //         getContainer().append(fileElement);
    //         cropperInstance?.$modal?.modal('hide');
    //     });
    // }

    if (cropperOption) {
        const cropper = defineCropAndUploadImage({
            ...cropperOption, type: id, fileExtensions: fileExtensions,
            cbWhenAddFile: function () {
                if (totalFiles >= maxFiles) {
                    toastInfoMessage(`Cho phép chọn tối đa ${maxFiles} ảnh`);
                    cropper.stopUpload();
                }
            }
        });

        cropperInstance = cropper;

        cropper.saveButton.addEventListener('click', function () {
            const {fileElement} = createFileElement(cropper?.getImageUrl(), cropper?.getImgName());
            setIsNewUpdateImg(true);
            fileUrls.push(cropper.getImg());
            totalFiles++;
            getContainer().append(fileElement);
            cropper?.$modal?.modal('hide');
        });
    }

    return {
        totalFiles,
        getFileUrls,
        setFileUrls,
        createFileElement,
        getTotalFile,
        setTotalFile,
        getContainer,
        getDeleteImageIds,
        getMultiImg,
        getOneImg,
        getNewImgs,
        setUpdateFileUrls,
        isNewUpdateImg,
        _isNewUpdateImg,
        getOneImgUpdate,
        setIsNewUpdateImg,
        validateUploading,
        cropperInstance
    }
}

//module crop image by type avatar or cover
var defineCropAndUploadImage = ({
                                    type = 'avatar',
                                    inputSelector,
                                    cbWhenAddFile,
                                    fileExtensions = ['gif', 'png', 'jpg', 'jpeg'],
                                    maxFileSize = 2000000
                                }) => {
    let totalFiles = 0;
    let options = {
        title: 'Thay avatar',
        aspectRatio: 1,
        viewMode: 3,
        borderRadius: '100%',
        defaultCropImage: '../backend/images/none-avatar.jpg',
        btnSaveText: 'Lưu thay đổi'
    };

    const initOptions = function () {
        switch (type) {
            case 'cover':
                options.aspectRatio = 3;
                options.title = 'Thay ảnh bìa';
                options.borderRadius = '0%';
                options.defaultCropImage = '../backend/images/none-cover.jpg'
            case 'banner':
                options.aspectRatio = 3;
                options.title = 'Cắt và thêm banner';
                options.borderRadius = '0%';
                options.defaultCropImage = '../backend/images/none-cover.jpg';
                options.btnSaveText = 'Thêm banner';
            case'flashsale':
                options.aspectRatio = 3;
                options.title = 'Cắt và thêm ảnh quảng bá';
                options.borderRadius = '0%';
                options.defaultCropImage = '../backend/images/none-cover.jpg';
                options.btnSaveText = 'Thêm ảnh quảng bá';
            case 'brand':
                options.aspectRatio = 1;
                options.title = 'Cắt và thêm ảnh thương hiệu';
                options.borderRadius = '0%';
                options.defaultCropImage = '../backend/images/none-cover.jpg';
                options.btnSaveText = 'Thêm ảnh thương hiệu';
            case 'category':
            case 'category-update':
                options.viewMode = 0;
                options.aspectRatio = 1;
                options.title = 'Cắt và thêm ảnh danh mục';
                options.borderRadius = '0%';
                options.defaultCropImage = '../backend/images/none-cover.jpg';
                options.btnSaveText = 'Thêm ảnh danh mục';
            case 'icon':
                options.aspectRatio = 1;
                options.title = 'Cắt và thêm ảnh phong cách';
                options.borderRadius = '0%';
                options.defaultCropImage = '../backend/images/none-cover.jpg';
                options.btnSaveText = 'Thêm ảnh phong cách';
            case 'blog':
                options.aspectRatio = 4 / 3;
                options.title = 'Cắt và thêm ảnh thumbnail';
                options.borderRadius = '0%';
                options.defaultCropImage = '../backend/images/none-cover.jpg';
                options.btnSaveText = 'Thêm ảnh thumbnail';
            case 'advertisement':
                options.aspectRatio = 2 / 3;
                options.title = 'Cắt và thêm ảnh quảng cáo';
                options.borderRadius = '0%';
                options.defaultCropImage = '../backend/images/none-cover.jpg';
                options.btnSaveText = 'Thêm ảnh quảng cáo';
        }
    }

    initOptions();

    const modalElementString = `<div id="modal-crop-${type}" class="modal modal-${type} fade"  tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title modalLabel" id="modalLabel-${type}">${options.title}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-6">
                <div class="img-container">
                  <img class="image-${type}">
                </div>
                <div class="feature-cropper row" style="justify-content: space-around">
                    <button class="btn btn-primary rotate-l"><i class="mr-0 fa fa-undo"></i></button>
                    <button class="btn btn-primary rotate-r"><i class="fa mr-0 fa-repeat"></i></button>

                    <button class="btn btn-primary reverse-h"><i class="fa mr-0 fa-arrows-h"></i></button>
                    <button class="btn btn-primary reverse-v"><i class="fa mr-0 fa-arrows-v"></i></button>
                </div>
              </div>
              <div class="col-sm-6 super-center">
                <div style="border-radius: ${options.borderRadius} !important;" class="img-preview img-preview-${type} preview-${type}-lg">
                    <img class="rounded avatar-${type}" src=${options.defaultCropImage}  alt=${type}>
                </div>
                <div class="progress" style="width: 100%">
                  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
                </div>
                <div class="alert alert-u-image" role="alert-u-image"></div>
                <div class="super-center" style="margin-top: 0.4rem">
                  <button class="btn-upload btn btn-primary">Tải lên</button>
                </div>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
            <button type="button" class="btn btn-save-image btn-primary">${options.btnSaveText}</button>
          </div>
        </div>
      </div>
    </div>`
//    <button type="button" class="btn btn-primary crop">Cắt ảnh</button>
    let modalCropElement = createElementByText(modalElementString);
    let avatar = modalCropElement.querySelector(`.avatar-${type}`);
    let image = modalCropElement.querySelector(`.image-${type}`);
    //let input = modalCropElement.querySelector('#input');
    let $progress = $(modalCropElement).find('.progress');
    let $progressBar = $(modalCropElement).find('.progress-bar');
    let $alert = $(modalCropElement).find('.alert-u-image');
    let $modal = $(modalCropElement);
    let cropButton = modalCropElement.querySelector(`.crop`);
    let uploadButton = modalCropElement.querySelector(`.btn-upload`);
    let saveButton = modalCropElement.querySelector(`.btn-save-image`);
    saveButton.disabled = true;
    uploadButton.disabled = true;

    const firebaseConfig = {
        apiKey: "AIzaSyBK9eScBvaK3GH0CQzAZfozawSkVwe-_eA",
        authDomain: "fir-web-c6c09.firebaseapp.com",
        projectId: "fir-web-c6c09",
        storageBucket: "fir-web-c6c09.appspot.com",
        messagingSenderId: "231147196503",
        appId: "1:231147196503:web:1ec701425caa4c695e9312",
        measurementId: "G-Y376EFELYG"
    };
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }
    let cropper;
    let file;
    const input = document.querySelector(inputSelector);
    //when user choose file
    const setTotalFiles = (total) => {
        totalFiles = total
    }

    let stop = false;
    const stopUpload = () => {
        stop = true
    };

    const setType = (t) => {
        type = t;
        initOptions();
    }

    const setCbWhenAddFile = (func) => {
        cbWhenAddFile = func;
    };

    const setFileExtensions = (p) => {
        fileExtensions = p;
    }

    input.addEventListener('change', function (e) {
        let files = e.target.files;
        if (cbWhenAddFile)
            cbWhenAddFile(e);
        if (stop) {
            stop = false;
            return;
        }
        const done = function (url) {
            input.value = '';
            image.src = url;
            $alert.hide();
            $modal.modal('show');
        };
        let reader;
        if (files && files.length > 0) {
            file = files[0];
            let extension = file.name.split('.').pop().toLowerCase();
            const fsize = file.size ?? file.fileSize;
            if (fsize > maxFileSize)
                return alert(`Kích thước quá lớn! ,tối đa ${maxFileSize / 1000000} MB`);
            if (jQuery.inArray(extension, fileExtensions) === -1)
                return alert(`Định dạng chỉ bao gồm ${fileExtensions.toString()}`);
            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                reader.onload = function (e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    });
    let reverseX = 1;
    let reverseY = 1;
    let imageUrl;
    //create new cropper instance when modal showing
    $modal.on('shown.bs.modal', function () {
        imageUrl = null;
        cropper = new Cropper(image, {
            preview: `.img-preview-${type}`,
            aspectRatio: options.aspectRatio,
            viewMode: options.viewMode,
            crop: function () {
                uploadButton.disabled = false;
                saveButton.disabled = true;
                $alert.hide();
                if (imageUrl) {
                    const urlRefer = firebase.storage().refFromURL(imageUrl);
                    imageUrl = null;
                    urlRefer.delete().then(function () {
                        console.log('Xóa ảnh')
                    }).catch(function (error) {
                        console.log(error)
                    });
                }
            }
        });

        modalCropElement.querySelector(`.rotate-l`).addEventListener('click', function () {
            cropper.rotate(-90);
        });
        modalCropElement.querySelector(`.rotate-r`).addEventListener('click', function () {
            cropper.rotate(90);
        });
        modalCropElement.querySelector(`.reverse-h`).addEventListener('click', function () {
            reverseX = reverseX * -1;
            cropper.scaleX(reverseX);
        });
        modalCropElement.querySelector(`.reverse-v`).addEventListener('click', function () {
            reverseY = reverseY * -1;
            cropper.scaleY(reverseY);
        });
    }).on('hidden.bs.modal', function () {
        cropper.destroy();
        cropper = null;
    });

    let canvas;
    let initialAvatarURL;

    //when user press crop button
    // cropButton.addEventListener('click', function () {
    //     if (cropper) {
    //         canvas = cropper.getCroppedCanvas({
    //             minWidth: 160,
    //             minHeight: 160,
    //         });
    //         initialAvatarURL = avatar.src;
    //         avatar.src = canvas.toDataURL();
    //         uploadButton.disabled = false;
    //         saveButton.disabled = true;
    //         $progress.hide();
    //         $alert.removeClass('alert-success alert-warning');
    //         $alert.hide();
    //     }
    // });
    let imgName = '';
    let imageSize = 0, imageType = 'jpeg', imageHash = '';
    let isUploading = false;
    const metadata = {
        cacheControl: 'max-age=7200',
    };
    //when user press upload button
    uploadButton.addEventListener('click', function () {
        if (cropper) {
            canvas = cropper.getCroppedCanvas({
                minWidth: 160,
                minHeight: 160,
            });
            initialAvatarURL = avatar.src;
            avatar.src = canvas.toDataURL();
            uploadButton.disabled = true;
            saveButton.disabled = true;
            $alert.removeClass('alert-success alert-warning');
            $alert.hide();
        }
        imgName = getRandomFileName(file);
        let storageRef = firebase.storage().ref(`${moment().format('YYYY/MM/DD')}/` + imgName);
        let uploadTask;

        canvas.toBlob(function (blob) {
            imageSize = blob.size;
            imageType = blob.type;
            uploadTask = storageRef.put(blob, metadata);
            // Listen for state changes, errors, and completion of the upload.
            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
                (snapshot) => {
                    isUploading = true;
                    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                    const percent = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    const percentage = percent + '%';
                    $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);
                    $progress.show();
                },
                (error) => {
                    isUploading = false;
                    // A full list of error codes is available at
                    // https://firebase.google.com/docs/storage/web/handle-errors
                    //avatar.src = initialAvatarURL;
                    $progress.hide();
                    $alert.show().addClass('alert-warning').text('Tải lên thất bại');
                },
                () => {
                    isUploading = false;
                    // Upload completed successfully, now we can get the download URL
                    uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
                        imageUrl = downloadURL;
                        $progress.hide();
                        uploadButton.disabled = true;
                        saveButton.disabled = false;
                        $alert.show().addClass('alert-success').text('Tải lên thành công');
                    });
                }
            );
        })
        //uploadTask = storageRef.putString(canvas.toDataURL('image/jpeg', 1.0), 'data_url');

    })

    const validateUploading = (mesSelector = '.mes', parentSelector = document) => {
        const $mes = $(mesSelector, parentSelector);
        if (isUploading) {
            $mes.text('Vui lòng chờ ảnh đang được tải lên').removeClass('mes-v-display').addClass('mes-iv-display');
            return false;
        } else {
            $mes.removeClass('mes-iv-display').addClass('mes-v-display');
            return true;
        }
    }
    const getImageUrl = () => imageUrl;
    const getImgName = () => imgName;
    const getImgType = () => imageType;
    const getImgSize = () => imageSize;
    const getImgHash = () => imageHash;
    const getImg = () => {
        return {name: imgName, url: imageUrl, type: imageType, size: imageSize, hash: imageHash}
    };
    return {
        modalCropElement,
        $modal,
        getImageUrl,
        saveButton,
        getImgName,
        input,
        stopUpload,
        getImg,
        getImgSize,
        getImgType,
        getImgHash,
        validateUploading,
        setCbWhenAddFile,
        setFileExtensions,
        setType
    }
}

//module upload file, image, preview image,delete image
var defineAsyncUploadFile = ({
                                 type = 'image',
                                 id = 'product',
                                 imageName = 'imageUrl',
                                 inputSelector = null,
                                 containerSelector = '#img-container', maxFiles = 10, cbWhenAddedFile = function () {

    }, optionsPhotoViewer = {}, maxFileSize = 2000000, fileExtensions = ['gif', 'png', 'jpg', 'jpeg']
                             }) => {

    let totalFiles = 0;
    let fileUrls = [];
    let localFileUrls = [];
    let promises = [];
    const getFileUrls = () => fileUrls;
    const container = document.querySelector(containerSelector);
    const $container = $(containerSelector);
    if (inputSelector) {
        const input = document.querySelector(inputSelector);
        //when user choose file to upload
        input.addEventListener('change', function (e) {
            const fileList = e?.detail?.fileList ?? e.target.files;
            if (totalFiles + fileList.length > maxFiles) {
                return alert(`Bạn không thể chọn quá ${maxFiles} ảnh`);
            } else {
                for (let i = 0; i < fileList.length; i++) {
                    const file = fileList[i];
                    const extension = file.name.split('.').pop().toLowerCase();
                    const fsize = file.size || file.fileSize;
                    if (fsize > maxFileSize)
                        return alert(`Ảnh thứ ${i + 1} kích thước quá lớn! ,tối đa ${maxFileSize / 1000000} MB`);
                    if (jQuery.inArray(extension, fileExtensions) === -1)
                        return alert(`File thứ ${i + 1} không hợp lệ!, cho phép các định dạng ${fileExtensions.toString()}`);
                    promises.push(createPromiseUploadImage(file));
                }
            }
        })
    }

    const createFileElement = (imgUrl, imgName = '') => {
        let html = `
            <div class="${id}-wrapper-upload super-center col">
                <div class="img-container-hover-bisto ${id}-wrapper-upload">
                        <div class="position-relative">
                            <img name=${imgName} class="${id}-img-upload bisto-img-upload img-fluid" src=${imgUrl} alt="${id}"/>
                        <div class="middle">
                            <div class="mr-4 delete-file-${id}">
                                <i class="fas fa-trash fa-lg" aria-hidden="true"></i>
                            </div>
                            <div class="preview preview-upload-${id}">
                                <i class="fas fa-eye fa-lg" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="progress progress-bisto">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                    aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">0%</div>
                </div>
                <input type="hidden" name="${imageName}" value=${imgUrl} />
            </div>
        `;

        let fileElement = createElementByText(html);
        const img = fileElement.querySelector(`img.${id}-img-upload`);
        const deleteButton = fileElement.querySelector(`.delete-file-${id}`);
        const previewButton = fileElement.querySelector(`.preview-upload-${id}`);
        const inputHidden = fileElement.querySelector(`input[name=${imageName}]`);
        const $progress = $(fileElement).find('.progress');
        const $progressBar = $(fileElement).find('.progress-bar');

        previewButton.addEventListener('click', function () {
            openPhotoViewer({fileUrls: localFileUrls, currentName: imgName, options: optionsPhotoViewer});
        })

        deleteButton.addEventListener('click', function () {
            fileElement.remove();
            const index = fileUrls.findIndex(v => v.name === imgName);
            fileUrls.splice(index, 1);
            localFileUrls.splice(index, 1);
            promises.splice(index, 1);
            totalFiles -= 1;
        })

        return {
            fileElement, img, deleteButton, previewButton, inputHidden, $progress, $progressBar
        }
    }

    const createPromiseUploadImage = (file) => {
        return new Promise((resolve, reject) => {
            try {
                const rdFileName = getRandomFileName(file);
                const {
                    fileElement,
                    img,
                } = createFileElement('', rdFileName);
                container.append(fileElement);
                const length = fileUrls.push({
                    name: rdFileName,
                    url: '',
                    type: file.type,
                    size: file.size,
                    hash: '',
                    isUploading: false
                });
                localFileUrls.push({
                    name: rdFileName,
                    url: '',
                });
                const tempFile = fileUrls[length - 1];
                const localFile = localFileUrls[length - 1];

                if (URL) {
                    img.src = URL.createObjectURL(file);
                    localFile.url = URL.createObjectURL(file);
                    cbWhenAddedFile();
                } else if (FileReader) {
                    const reader = new FileReader();
                    reader.onload = function (e) {
                        img.src = reader.result;
                        localFile.url = reader.result;
                        cbWhenAddedFile();
                    };
                    reader.readAsDataURL(file);
                }
                const {firebase, getStorageRef, getUploadTask, storage, getRefFromUrl} = installFirebase({});
                let uploadTask = getUploadTask(`${moment().format('YYYY/MM/DD')}/` + rdFileName, file);
                uploadTask.on(storage.TaskEvent.STATE_CHANGED,
                    (snapshot) => {
                        tempFile.isUploading = true;
                    },
                    (error) => {
                        tempFile.isUploading = false;
                        tempFile.url = `${PREFIX_API}/backend/images/null-normal.jpg`;
                        fileUrls.splice(fileUrls.findIndex(v => v.name === rdFileName), 1);
                        reject({file: tempFile, success: 0, error});
                    },
                    () => {
                        tempFile.isUploading = false;
                        // Upload completed successfully, now we can get the download URL
                        uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
                            tempFile.url = downloadURL;
                            totalFiles++;
                            resolve({file: tempFile, success: 1});
                        });
                    }
                );
            } catch (e) {
                console.log(e)
                reject(e);
            }
        });
    }

    let defaultFileUrls = [];

    const asyncUploadImage = (callbackWhenUploadDone) => {
        Promise.allSettled(promises).then((results) => {
            if (results.length === totalFiles) {
                callbackWhenUploadDone();
            }
        })
    }

    const setTotalFile = (p) => {
        totalFiles = p;
    }
    const getTotalFile = () => totalFiles;
    const setFileUrls = (p) => {
        fileUrls = p;
        totalFiles = fileUrls.length;
        defaultFileUrls = [...p];
    }
    const getDeleteImageIds = () => {
        const updatedNames = fileUrls.map(item => item.name);
        return defaultFileUrls.filter(item => !updatedNames.includes(item.name)).map(item => item.name);
    }

    const getNewImgs = () => {
        const defaultNames = defaultFileUrls.map(item => item.name);
        return fileUrls.filter(item => !defaultNames.includes(item.name)).map(item => {
            return {
                filename: item.name,
                url: item.url,
                hash: item.hash,
                type: item.type,
                size: item.size,
            }
        })
    }
    const getContainer = () => $container;
    const getOneImg = () => {
        if (fileUrls.length > 0) {
            return {
                filename: fileUrls[0].name,
                url: fileUrls[0].url,
                type: fileUrls[0].type,
                size: fileUrls[0].size,
                hash: fileUrls[0].hash,
            }
        } else return null;
    }
    let isNewUpdateImg = false;
    const _isNewUpdateImg = () => isNewUpdateImg;
    const setIsNewUpdateImg = (p) => isNewUpdateImg = p;
    const getOneImgUpdate = () => {
        if (!_isNewUpdateImg()) return null;
        if (fileUrls.length > 0) {
            return {
                filename: fileUrls[0].name,
                url: fileUrls[0].url,
                type: fileUrls[0].type,
                size: fileUrls[0].size,
                hash: fileUrls[0].hash,
            }
        } else return null;
    }

    const getMultiImg = () => {
        let rs = [];
        const length = fileUrls.length;
        if (length > 0) {
            for (let i = 0; i < length; i++) {
                rs.push({
                    filename: fileUrls[i].name,
                    url: fileUrls[i].url,
                    type: fileUrls[i].type,
                    size: parseInt(fileUrls[i].size),
                    hash: fileUrls[i].hash,
                })
            }
        }
        return rs;
    }

    const setUpdateFileUrls = (_fileUrls = [{filename: '', url: '', type: '', size: 0, hash: '', id: ''}]) => {
        const length = _fileUrls.length;
        $container.empty();
        for (let i = 0; i < length; i++) {
            const {fileElement} = createFileElement(_fileUrls[i].url, _fileUrls[i].filename);
            $container.append(fileElement);
        }
        fileUrls = _fileUrls.map(item => {
            return {
                name: item.filename,
                url: item.url,
                type: item.type,
                size: item.size,
                hash: item.hash,
            }
        })
        totalFiles = _fileUrls.length;
    }

    const validateUploading = (mesSelector = '.mes', parentSelector = document) => {
        const $mes = $(mesSelector, parentSelector);
        for (let i = 0; i < fileUrls.length; i++) {
            if (fileUrls[i]?.isUploading === true) {
                $mes.text('Vui lòng chờ ảnh đang được tải lên').removeClass('mes-v-display').addClass('mes-iv-display');
                return false;
            }
        }
        $mes.removeClass('mes-iv-display').addClass('mes-v-display');
        return true;
    }

    const resetAllFiles = () => {
        $container.empty();
        promises = [];
        localFileUrls = [];
        setTotalFile(0);
        setFileUrls([]);
    }

    const resetPromises = () => {
    }

    const getLocalFileUrls = () => localFileUrls;

    return {
        totalFiles,
        getFileUrls,
        setFileUrls,
        createFileElement,
        getTotalFile,
        setTotalFile,
        getContainer,
        getDeleteImageIds,
        getMultiImg,
        getOneImg,
        getNewImgs,
        setUpdateFileUrls,
        isNewUpdateImg,
        getOneImgUpdate,
        setIsNewUpdateImg,
        validateUploading,
        asyncUploadImage,
        resetAllFiles,
        getLocalFileUrls,
    }
}

var uploadSingleFile = (file) => {
    return new Promise((resolve, reject) => {
        try {
            const rdFileName = getRandomFileName(file);
            const {firebase, getStorageRef, getUploadTask, storage, getRefFromUrl} = installFirebase({});
            let uploadTask = getUploadTask(`${moment().format('YYYY/MM/DD')}/` + rdFileName, file);
            // Listen for state changes, errors, and completion of the upload.
            uploadTask.on(storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
                (snapshot) => {
                },
                (error) => {
                    reject(error)
                },
                () => {
                    // Upload completed successfully, now we can get the download URL
                    uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
                        resolve(downloadURL)
                        // deleteButton.addEventListener('click', function () {
                        //     const urlRefer = getRefFromUrl(downloadURL);
                        //     urlRefer.delete().then(function () {
                        //         console.log('xóa ảnh thành công')
                        //     }).catch(function (error) {
                        //         console.log(error)
                        //     });
                        // })
                    });
                }
            );
        } catch (e) {
            console.log(e)
            toastErrorMessage('Có Lỗi xảy ra khi upload ảnh')
        }
    })
}
