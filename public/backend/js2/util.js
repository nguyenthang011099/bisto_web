var definePagination = ({
    paginationSelector = '#pagination-container',
    type = 'default',
    onPageClick = (pageNumber, event) => {
    }, loadData = () => {
    }
}) => {
    const me = this;
    let options = {
        currentPage: 1,
        itemsOnPage: 10,
        pages: 1,//totalpage
        prevText: '<',
        nextText: '>',
        cssStyle: 'paging-primary-theme',
        onInit: loadData,
    }
    const $pagination = $(paginationSelector);
    let _totalItems = 0;

    $pagination.pagination({
        ...options,
        onPageClick: (pageNumber, event) => {
            onPageClick(pageNumber, event);
        }
    });

    const setTotalItems = (totalItems) => {
        $pagination.pagination('updateItems', totalItems);
        _totalItems = totalItems;
        return me;
    }

    const setCurrentPage = (totalPages) => {
        $pagination.pagination('drawPage', totalPages);
        return me;
    }

    const setTotalPages = (page) => {
        $pagination.pagination('updateItems', Math.ceil(page * 10));
        return me;
    }

    const getLastPageOnAdd = () => {
        if (_totalItems % options.itemsOnPage === 0) {
            return _totalItems / options.itemsOnPage + 1;
        } else return _totalItems / options.itemsOnPage;
    }

    const setItemsPerPage = (itemsNumber) => {
        $pagination.pagination('updateItemsOnPage', itemsNumber);
        return me;
    }

    const handleLoadData = (loadData) => {
        if ($pagination.pagination('getCurrentPage') === 1) {

        }
    }
    return {
        $pagination,
        me,
        getLastPageOnAdd,
        setCurrentPage,
        setTotalItems,
        setItemsPerPage,
        setTotalPages
    }
}

var multiSelect = ({
    listSelect = [{
        selectSelector: 'select', url: '', valueName: 'id', showName: 'name', defaultName: ''
    }], parentSelector = document, cbWhenSelectLastOption, loading = { type: 'content' }
}) => {
    const length = listSelect.length;
    let currentValueSelected = null;
    let multiValueSelected = new Array(length);
    let isSelectLastOption = false;
    let $selects = [];

    for (const s of listSelect) {
        $selects.push($(s.selectSelector, parentSelector));
    }

    const loadRootSelect = (callback) => {
        $.ajax({
            url: listSelect[0].url,
            method: 'GET',
            loading,
        }).done(res => {
            const rs = res.results || [];
            let options = `<option selected disabled hidden value="">-- Chọn ${listSelect[0].defaultName || ''}--</option>`;
            //currentValueSelected = rs[0] ? rs[0][listSelect[0].valueName] : null;
            for (const r of rs) {
                options += `<option value=${r[listSelect[0].valueName]}>${r[listSelect[0].showName]}</option>`
            }
            $selects[0].empty().append(options);
            if (callback) callback();
        }).fail(err => {
            console.log(err)
        })
    }

    for (let i = 0; i < length - 1; i++) {
        $selects[i].on('change', function () {
            currentValueSelected = { value: $(this).val(), text: $(this).find('option:selected').text() };
            isSelectLastOption = false;
            multiValueSelected[i] = currentValueSelected;
            $.ajax({
                url: `${listSelect[i + 1].url}${$(this).val()}`,
                method: 'GET',
                loading,
            }).done(res => {
                const rs = res.results || [];
                let options = `<option selected disabled hidden value="">-- Chọn ${listSelect[i + 1].defaultName || ''}--</option>`;
                //currentValueSelected = rs[0] ? rs[0][valueName] : currentValueSelected;
                for (const r of rs) {
                    options += `<option value=${r[listSelect[i + 1].valueName]}>${r[listSelect[i + 1].showName]}</option>`
                }
                $selects[i + 1].empty().append(options);
                if (rs.length === 0) $selects[i + 1].empty().append(`<option selected disabled hidden value="">-- Chọn ${listSelect[i + 1].defaultName || ''}--</option>`);

                for (let j = i + 2; j < length; j++) {
                    $selects[j].empty().append(`<option selected disabled hidden value="">-- Chọn ${listSelect[j].defaultName || ''}--</option>`);
                }
            }).fail(err => {
                console.log(err)
            })
        })
    }

    $selects[length - 1].on('change', function () {
        isSelectLastOption = true;
        currentValueSelected = { value: $(this).val(), text: $(this).find('option:selected').text() };
        multiValueSelected[length - 1] = currentValueSelected;
        if (cbWhenSelectLastOption) cbWhenSelectLastOption();
    })

    //load selects
    const loadSelects = (listValues = [], callback) => {
        let currentLength = listValues.length;
        currentValueSelected = { value: listValues[currentLength - 1] };
        if (currentLength === 0) return;
        loadRootSelect(function () {
            multiValueSelected[0] = { value: listValues[0], text: '' };
            $selects[0].find(`option[value=${listValues[0]}]`).attr('selected', 'selected');
        })
        for (let i = 1; i < currentLength; i++) {
            multiValueSelected[i] = { value: listValues[i], text: '' };
            $.ajax({
                url: `${listSelect[i].url}${listValues[i - 1]}`,
                method: 'GET',
                loading,
            }).done(res => {
                const rs = res.results || [];
                let options = `<option selected disabled hidden value="">-- Chọn ${listSelect[i].defaultName || ''}--</option>`;
                for (const r of rs) {
                    options += `<option value=${r[listSelect[i].valueName]}>${r[listSelect[i].showName]}</option>`
                }
                $selects[i].empty().append(options).find(`option[value=${listValues[i]}]`).attr('selected', 'selected');
            }).fail(err => {
                console.log(err)
            })
        }

        if (currentLength < length) {
            isSelectLastOption = false;
            $.ajax({
                url: `${listSelect[currentLength].url}${listValues[currentLength - 1]}`,
                method: 'GET',
                loading,
            }).done(res => {
                const rs = res.results || [];
                let options = `<option selected disabled hidden value="">-- Chọn ${listSelect[currentLength].defaultName || ''}--</option>`;
                for (const r of rs) {
                    options += `<option value=${r[listSelect[currentLength].valueName]}>${r[listSelect[currentLength].showName]}</option>`
                }
                $selects[currentLength].empty().append(options);
            }).fail(err => {
                console.log(err)
            })
            for (let i = currentLength + 1; i < length; i++) {
                $selects[i].empty().append(`<option selected disabled hidden value="">-- Chọn ${listSelect[i].defaultName || ''}--</option>`);
            }
        } else isSelectLastOption = true;
        if (callback) callback();
    }
    const isSelectedLastOption = () => isSelectLastOption;
    const getCurrentValueSelected = () => currentValueSelected;
    const getMultiValueSelected = () => multiValueSelected;
    return {
        getCurrentValueSelected,
        getMultiValueSelected,
        isSelectedLastOption,
        loadSelects,
        loadRootSelect
    }
}

var genLoadingTable = (colspan) => `<tr class="tr-loading-table"><td colspan="${colspan}" class="text-center"><div class="loading-table">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div></td></tr>`;

var genNoContentTable = (colspan, mes) => `<tr class="tr-no-content"><td colspan=${colspan} class="text-center">${mes}</td></tr>`;
/*
<div id="loading-content">
    <div class="loading-content-css">
        <div class="spinner-border text-primary" role="status">
        </div>
    </div>
</div>

<div class="loading-selector" style="display:none;">
    <div class="loading-center">
        <div class="spinner-border text-primary" role="status">
        </div>
    </div>
</div>
*/

$(document).ajaxSend(function (event, request, settings) {
    if (settings && settings.loading && settings.loading.type === 'table') {
        const $tbody = settings.loading.selector ? $(settings.loading.selector).first() : $('#content-page tbody').first();
        const colspan = $tbody.first().parent().find('thead').find('th').length ?? settings.loading.colspan;
        $tbody.first().empty().append(genLoadingTable(colspan));
    } else if (settings && settings.loading && settings.loading.type === 'content') {
        const $loader = settings.loading.selector ? $(settings.loading.selector) : $('#loading-content');
        if ($loader.length !== 0) {
            $loader.show();
            $loader.parent().css('opacity', '0.5');
        }
    }
});

$(document).ajaxComplete(function (event, request, settings) {
    const isError = parseInt(request.status / 100) !== 2;

    if (isError && request.responseJSON) {
        toastErrorMessage(request.responseJSON?.message);
    }
    if (settings && settings.loading && settings.loading.type === 'content') {
        const $loader = settings.loading.selector ? $(settings.loading.selector) : $('#loading-content');
        if ($loader.length !== 0) {
            $loader.hide();
            $loader.parent().css('opacity', '1');
        }
    } else if (settings && settings.loading && settings.loading.type === 'table' && isError) {
        const $tbody = settings.loading.selector ? $(settings.loading.selector).first() : $('#content-page tbody').first();
        const colspan = $tbody.first().parent().find('thead').find('th').length ?? settings.loading.colspan;
        $tbody.first().empty().append(genNoContentTable(colspan, 'Có lỗi xảy ra !Vui lòng reload lại trang'));
    }
});

const DEFAULT_IMAGE = {
    cover: '../backend/images/cover-default.jpg',
    avatar: DEFAULT_AVATAR,
    profile_cover_not_found: '../backend/images/cover-not-found.jpg',
    profile_avatar_not_found: '../backend/images/none-avatar.jpg'
}

var setImage = (imageSelector = 'img', imageUrl, type = 'default', parentSelector = document) => {
    if (isEmptyValue(imageUrl)) {
        return $(imageSelector, parentSelector).attr('src', DEFAULT_IMAGE[type]);
    }
    let img = new Image();
    img.onload = () => {
        $(imageSelector, parentSelector).attr('src', imageUrl);
    };
    img.onerror = () => {
        $(imageSelector, parentSelector).attr('src', DEFAULT_IMAGE[type]);
    };
    img.src = imageUrl;
}

// $(document).ajaxSuccess(function (event, request, settings) {
//     debugger
// });
//
// $(document).ajaxError(function (event, request, settings, thrownError) {
//     debugger
// });

var defineLazyLoadingSelect = ({
    selectSelector = 'select',
    url = '',
    urlParams = {},
    valueName = 'id',
    textName = 'name',
    parentSelector = document,
    name = 'results',
    width = 'auto',
    loading,
    className = ''
}) => {
    let dt = { currentPage: 1, isMax: false, loading: { type: 'content' } };
    let isLoading = false;
    let paramsString = '';
    const $select = $(selectSelector, parentSelector);
    $select.selectmenu({
        "width": width,
    });
    for (const paramName in urlParams) {
        paramsString += `&${paramName}=${urlParams[paramName]}`
    }
    $(`${selectSelector} + span`, parentSelector).addClass(`form-control select-lazy ${className}`);
    $select.selectmenu("menuWidget").addClass("make-scrolling overflow-select");
    $select.selectmenu("menuWidget").scroll(function (e) {
        if (e.currentTarget.scrollHeight - 10 < e.currentTarget.scrollTop + $(e.currentTarget).height() && !isLoading) {
            let curTar = e.currentTarget;
            let lastTop = curTar.scrollTop;
            if (!dt.isMax) {
                isLoading = true;
                $.ajax({
                    url: `${url}?page=${dt.currentPage}${paramsString}`,
                    method: 'GET',
                }).done(res => {
                    isLoading = false;
                    const items = res[name]?.data ?? [];
                    dt.currentPage++;
                    console.log(items)
                    if (items.length === 0) dt.isMax = true;
                    let options = '';
                    for (const item of items) {
                        options += `<option value=${item[valueName]}">${item[textName]}</option>`
                    }
                    $select.append(options).selectmenu("refresh");
                    curTar.scrollTop = lastTop;
                }).fail(err => {
                })
            }
        }
    });

    const loadFirstTime = () => {
        $.ajax({
            url: `${url}?page=1${paramsString}`,
            method: 'GET',
            loading
        }).done(res => {
            const items = res[name]?.data ?? [];
            dt.currentPage = 2;
            if (items.length === 0) dt.isMax = true;
            let options = '';
            for (const item of items) {
                options += `<option value=${item[valueName]}>${item[textName]}</option>`
            }
            $select.append(options).selectmenu("refresh");
        }).fail(err => {
        })
    }

    const onSelect = (callbackOnSelect) => {
        $select.on('selectmenuselect', function (event, ui) {
            callbackOnSelect(ui.item.value, event, ui)
        });
    }
    loadFirstTime();

    const isMaximum = () => dt.isMax

    return {
        $select,
        onSelect,
        isMaximum
    }
}


var defineBisto = ({
    btnSearchSelector = '.btn-search-bisto',
    btnShowAddSelector = '.btn-show-add-bisto',
    btnShowUpdateSelector = '.btn-show-update-bisto',
    btnAddSelector = '.btn-add-bisto',
    btnUpdateSelector = '.btn-update-bisto',
    btnDeleteSelector = '.btn-delete-bisto',
    btnAcceptSelector = '.btn-accept-bisto',
    btnRejectSelector = '.btn-reject-bisto',
    modalAddSelector = '.modal-add-bisto',
    modalUpdateSelector = '.modal-update-bisto',
    modalSelector = '.modal-bisto',
    tbodySelector = '.tbody-bisto',
    formSelector = '.form-bisto',
    bodySelector = '.body-bisto',
    btnSelector = '.btn-bisto',
    btnQuerySelector = '.btn-query-bisto',
    loadingSelector = '#loading-content',
    btnShowDetailSelector = '.btn-show-detail-bisto',
    parentSelector = document,
    btnKeySelector = '.btn-key-bisto',
    type = 'default',
    name = null,
    v = [],
    cssClassMes = 'display'
}) => {
    const $btnSearch = $(btnSearchSelector, parentSelector)
    const $btnQuery = $(btnQuerySelector, parentSelector)
    const $tbody = $(tbodySelector, parentSelector)
    const $btnAdd = $(btnAddSelector, parentSelector)
    const $btnShowAdd = $(btnShowAddSelector, parentSelector)
    const $btnDelete = $(btnDeleteSelector, parentSelector)
    const $btnUpdate = $(btnUpdateSelector, parentSelector)
    const $btnShowUpdate = $(btnShowUpdateSelector, parentSelector)
    const $body = $(bodySelector, parentSelector);
    const $modal = $(modalSelector, parentSelector);
    let $modalAdd = $(modalAddSelector, parentSelector);
    let $btn = $(btnSelector, parentSelector);
    let $form = $(formSelector, parentSelector);
    const $btnAccept = $(btnAcceptSelector, parentSelector)
    const $btnReject = $(btnRejectSelector, parentSelector)

    let $modalUpdate = $(modalUpdateSelector, parentSelector);
    let currentId = null;


    if (type === 'default') {
        $modalAdd = $modal;
        $modalUpdate = $modal;
    }

    const onLockOrUnlock = (cbAjax) => {
        $(parentSelector).on('click', btnKeySelector, function () {
            currentId = $(this).attr('data-id');
            const status = $(this).attr('data-status');
            let changeStatus = 0;
            if (status == 1) {
                changeStatus = -1;
                $.confirm({
                    icon: 'fa text-warning fa-warning',
                    title: "Xác nhận",
                    content: `Bạn có chắc muốn khóa ${name}`,
                    backgroundDismiss: true,
                    buttons: {
                        confirm: {
                            text: "Khóa",
                            btnClass: 'btn-red',
                            action: function () {
                                cbAjax(currentId, changeStatus);
                            }
                        },
                        cancel: {
                            text: "Thoát"
                        },
                    }
                });
            } else if (status == -1) {
                changeStatus = 1;
                $.confirm({
                    icon: 'fa text-warning fa-warning',
                    title: "Xác nhận",
                    content: `Bạn có chắc muốn mở khóa ${name}`,
                    backgroundDismiss: true,
                    buttons: {
                        confirm: {
                            text: "Mở khóa",
                            btnClass: 'btn-success',
                            action: function () {
                                cbAjax(currentId, changeStatus);
                            }
                        },
                        cancel: {
                            text: "Thoát"
                        },
                    }
                });
            }
        })
    }

    const onShowDetail = (cbAjax) => {
        $(parentSelector).on('click', btnShowDetailSelector, function () {
            currentId = $(this).attr('data-id');
            cbAjax(currentId);
        })
    }

    const onSearch = (cb) => {
        $(parentSelector).on('keydown', btnSearchSelector, function (e) {
            const code = e.keyCode ?? e.which;
            if (code === 13) { //Enter keycode
                cb(e.currentTarget.value);
            }
        })
    }

    const onShowAdd = (cbClearData) => {
        $(parentSelector).on('click', btnShowAddSelector, function () {
            cbClearData();
            $btnAdd.show();
            $btnUpdate.hide();
            if (name)
                $modalAdd.find('.title-modal-bisto').text(`Thêm mới ${name}`)
            $modalAdd.modal('show');
        })
    }

    const onShowUpdate = (cbAjax) => {
        $(parentSelector).on('click', btnShowUpdateSelector, function () {
            currentId = $(this).attr('data-id');
            const cbShowModalUpdate = function () {
                $btnAdd.hide();
                $btnUpdate.show();
                if (name)
                    $modalAdd.find('.title-modal-bisto').text(`Cập nhật ${name}`)
                $modalUpdate.modal('show');
            }
            cbAjax(currentId, cbShowModalUpdate);
            // $btnAdd.hide();
            // $btnUpdate.show();
            // if (name)
            //     $modalAdd.find('.title-modal-bisto').text(`Cập nhật ${name}`)
            // $modalUpdate.modal('show');
        })
    }

    const onDelete = (cbAjax) => {
        $(parentSelector).on('click', btnDeleteSelector, function () {
            currentId = $(this).attr('data-id');
            confirmDelete(`Bạn có chắc chắn muốn xóa ${name} này?`, function () {
                cbAjax(currentId);
            })
        })
    }

    const onUpdate = (cbAjax) => {
        $(parentSelector).on('click', btnUpdateSelector, function () {
            const dt = v.length === 0 ? getData() : validate();
            if (dt) cbAjax(currentId, dt);
        })
    }

    const onAdd = (cbAjax) => {
        $(parentSelector).on('click', btnAddSelector, function () {
            const dt = v.length === 0 ? getData() : validate();
            if (dt) cbAjax(dt);
        })
    }
    const onAccept = (cbAjax) => {
        $(parentSelector).on('click', btnAcceptSelector, function (e) {
            currentId = e.target.dataset.id;
            cbAjax(currentId);
        })
    }
    const onReject = (cbAjax) => {
        $(parentSelector).on('click', btnRejectSelector, function (e) {
            currentId = e.target.dataset.id;
            cbAjax(currentId);
        })
    }

    const onQuery = (cbAjax) => {
        $(parentSelector).on('click', btnQuerySelector, function () {
            cbAjax();
        })
    }
    const getData = () => {
        return formToData(formSelector, parentSelector);
    }
    // const v = [{
    //     listValidateFunction: [{
    //         func: requiredValidation,
    //         mes: 'Tên giấy phép là bắt buộc'
    //     }, {
    //         func: requiredValidation,
    //         mes: 'Tên giấy phép là bắt buộc'
    //     }],
    //     propertyName: 'name', parentSelector
    // }, {
    //     inputSelector: 'input[name=source_url]',
    //     mesSelector: '.mes-image_urls',
    //     message: 'Bạn phải thêm ít nhất một ảnh', parentSelector
    // }]
    const validate = () => {
        const dt = getData();
        for (let i = 0; i < v.length; i++) {
            const _name = v[i].propertyName;
            for (let j = 0; j < v[i].listValidateFunction.length; j++) {
                if (!v[i].listValidateFunction[j].func(dt[_name], v[i].listValidateFunction[j].params)) {
                    $(`:input[name=${_name}]`, parentSelector).addClass(`is-invalid`);
                    $(`.mes-${_name}`, parentSelector).text(v[i].listValidateFunction[j].mes).removeClass(`mes-v-${cssClassMes}`).addClass(`mes-iv-${cssClassMes}`);
                    return false;
                } else {
                    $(`:input[name=${_name}]`, parentSelector).removeClass(`is-invalid`);
                    $(`.mes-${_name}`, parentSelector).removeClass(`mes-iv-${cssClassMes}`).addClass(`mes-v-${cssClassMes}`);
                }
            }
        }
        return dt;
    }

    const showLoading = function () {
        const $loader = $(loadingSelector);
        if ($loader.length !== 0) {
            $loader.show();
            $loader.parent().css('opacity', '0.5');
        }
    }

    const hideLoading = function () {
        const $loader = $(loadingSelector);
        if ($loader.length !== 0) {
            $loader.hide();
            $loader.parent().css('opacity', '1');
        }
    }

    return {
        onDelete,
        onSearch,
        onShowUpdate,
        onShowAdd,
        onAdd,
        onUpdate,
        getData,
        onReject,
        onAccept,
        onQuery,
        showLoading,
        hideLoading,
        onShowDetail,
        onLockOrUnlock,
        $btnSearch,
        $tbody,
        $btnAdd,
        $btnShowAdd,
        $btnDelete,
        $btnUpdate,
        $btnShowUpdate,
        $modal,
        $modalAdd,
        $modalUpdate,
        $form,
        $body,
        $btn,
        $btnAccept,
        $btnReject,
        $btnQuery
    }
}

var defineSearchSelect = ({ id = 'default', selectSelector = 'select', parentSelector = document }) => {
    let $select = $(selectSelector, parentSelector);
    let dataSelect = [];
    if (id === 'admin_shop_and_shop') {
        dataSelect.push({ id: 0, text: 'Tất cả cửa hàng' });
        $.ajax({
            url: 'lists/admin-shops-and-shops?page=1&limit=&keyword=',
            method: 'GET',
        }).done(res => {
            const listAdminShop = res?.results?.data ?? [];
            const listShopWithoutBranch = res?.shops_without_branch ?? [];
            for (const a of listAdminShop) {
                let temp = {
                    id: a.user_id,
                    text: a.admin_shop_name,
                    children: []
                }
                for (const shop of a.shops) {
                    temp.children.push({
                        id: shop.user_id,
                        text: shop.shop_name,
                    })
                }
                dataSelect.push(temp);
            }
            if (listShopWithoutBranch.length > 0) {
                let temp = {
                    text: 'Cửa hàng ngoài chuỗi',
                    children: []
                }
                for (const shopWithoutBranch of listShopWithoutBranch) {
                    temp.children.push({
                        id: shopWithoutBranch.user_id,
                        text: shopWithoutBranch.shop_name,
                    })
                }
                dataSelect.push(temp);
            }
            $select.select2({
                data: dataSelect,
                selectionCssClass: "search-select-bisto",
                dropdownCssClass: "dropdown-search-select-bisto"
            })
        }).fail(err => {
        })
    } else if (id === 'shops_of_admin_shop') {
        dataSelect.push({ id: 0, text: 'Tất cả cửa hàng' });
        $.ajax({
            url: 'shops',
            method: 'GET',
        }).done(res => {
            const shops = res?.results?.data ?? [];
            for (const shop of shops) {
                dataSelect.push({
                    id: shop.id,
                    text: shop.name,
                })
            }
            $select.select2({
                data: dataSelect,
                selectionCssClass: "search-select-bisto",
                dropdownCssClass: "dropdown-search-select-bisto"
            })
        }).fail(err => {
        });
    }

    const onSelect = (cb) => {
        $select.on('select2:select', function (e) {
            cb(e.params.data.id, e.params.data);
        });
    }

    return {
        $select,
        onSelect
    }
}

/*
*      * @param string $order_code Mã order (Lấy từ bisto)
     * @param int $amount Số tiền thanh toán
     * @param string $order_type Mã danh mục hàng hóa.
     * @param string $order_description Mô tả thanh toán
     * @param string $bank_code Mã Ngân hàng thanh toán
     * @param string $return_url Url chuyển hướng khi thanh toán xong
* */

var definePayment = ({
    passData = {},
    type = 'shop',
}) => {
    let formData = {
        orderCode: '',
        amount: 0,
        orderType: '',
        orderDescription: '',
        bankCode: '',
        returnUrl: '',
        ...passData
    }
    let prefix_url = '';
    if (type === 'shop') {
        prefix_url = PREFIX_API_SHOP;
    } else if (type === 'admin-shop') {
        prefix_url = PREFIX_API_ADMIN_SHOP;
    }

    const modalPaymentHTML = `
             <div id="modal-payment-bisto" class="modal fade modal-payment" tabindex="-1" role="dialog"
                 aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content" style="border-radius: 20px">
                    <div class="loading-selector" style="display: none">
                        <div class="loading-center">
                            <div class="spinner-border text-primary" role="status">
                            </div>
                        </div>
                    </div>
                        <div class="iq-card">
                            <header class="panel-heading title-modal-bisto">
                                Thông tin thanh toán
                            </header>
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="iq-card-body">
                                        <div class="form-payment" id="form-payment" role="form">
                                            <!--<div class="form-group">
                                                <label class="required-input">Mã đơn hàng</label>
                                                <input type="number" name="order_code" class="form-control">
                                                <div class="mes-order_code mes-v-display">
                                                </div>
                                            </div>-->
                                            <div class="form-group">
                                                <label class="required-input">Số tiền thanh toán</label>
                                                <input type="number" name="amount" class="form-control">
                                                <div class="mes-amount mes-v-display">
                                                </div>
                                            </div>
                                           <!-- <div class="form-group">
                                                <label class="required-input">Danh mục hàng hóa</label>
                                                  <select name="order_type" class="form-control">
                                                        <option selected value="billpayment">Thanh toán hóa đơn</option>
                                                        <option value="fashion">Thời trang</option>
                                                        <option value="other">Khác - Xem thêm tại VNPAY</option>
                                                  </select>
                                                <div class="mes-order_type mes-v-display">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required-input">Nội dung thanh toán</label>
                                                <textarea name="order_description" class="textarea-bisto-css form-control"></textarea>
                                                <div class="mes-order_description mes-v-display">
                                                </div>
                                            </div>-->
                                            <div class="form-group">
                                                <label class="required-input">Cổng thanh toán</label>
                                                  <select name="provider" class="form-control" id="payment_provider">
                                                        <option selected value="vnpay">Vnpay</option>
                                                        <option value="momo">Momo</option>
                                                        <option value="zalopay">Zalopay</option>
                                                  </select>
                                                <div class="mes-provider mes-v-display">
                                                </div>
                                            </div>

                                        </div>
                                        <div>
                                            <button id="btn-redirect-payment" class="btn btn-primary">
                                                Đi đến thanh toán
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
    `;
    const modalPayment = createElementByText(modalPaymentHTML);
    const $modalPayment = $(modalPayment);
    const $btnRedirectPayment = $modalPayment.find('#btn-redirect-payment');

    const validateFormPayment = () => {
        return validateInput({
            listValidateFunction: [{
                func: requiredValidation,
                mes: 'Mã đơn hàng là bắt buộc'
            }],
            propertyName: 'order_code', parentSelector: '#modal-payment-bisto'
        }) && validateInput({
            listValidateFunction: [{
                func: requiredValidation,
                mes: 'Số tiền thanh toán là bắt buộc'
            }, {
                func: rangeValidation,
                params: { min: MIN_MONEY, max: MAX_MONEY },
                mes: `Số tiền thanh toán tối thiểu là ${MIN_MONEY} VND`
            }],
            propertyName: 'amount', parentSelector: '#modal-payment-bisto'
        }) && validateInput({
            listValidateFunction: [{
                func: requiredValidation,
                mes: 'Nội dung thanh toán là bắt buộc'
            }],
            propertyName: 'order_description', parentSelector: '#modal-payment-bisto', type: 'textarea'
        })
    }

    $btnRedirectPayment.on('click', function () {
        if (validateFormPayment()) {
            const paymentInfo = formToData('#form-payment');
            $.ajax({
                url: `${prefix_url}/payment-redirect`,
                method: 'POST',
                data: {
                    _token: _TOKEN,
                    ...paymentInfo
                },
                loading: { type: 'content', selector: '.loading-selector' }
            }).done(res => {
                //chuyển hướng thanh toán
                window.open(res?.payment_url, '_blank');
                // window.location.href = res?.payment_url;
            }).fail(err => {

            })
        }
    })
    return {
        $modalPayment
    }
}

var defineTag = ({ containerSelector = '#tag-container' }) => {
    // const containerSelector = '#tag-container';
    const $tagContainer = $(containerSelector);
    const $tagItemContainer = $(`${containerSelector} .tag-item-container`);
    const $input = $(`${containerSelector} #input-tag`);
    let tagList = []
    let currentTagIdList = [];
    let tagAvaiIds = []
    let tagItemIds = []

    function loadTagList({
        callback = function () {
        }
    }) {
        $.ajax({
            url: `${PREFIX_API_ADMIN}/tags?page=1&keyword=`,
            method: 'GET',
        }).done(res => {
            tagList = res?.results?.data ?? []
            callback()
        }).fail(err => {
        })
    }

    function postTag(tagName) {
        $.ajax({
            url: `${PREFIX_API_ADMIN}/tags`,
            method: 'POST',
            data: {
                "_token": `${_TOKEN}`,
                "name": tagName
            }
        }).done(res => {

        }).fail(err => {

        })
    }

    function createTagAvaiHTML(tag) {
        return `
                    <div class="tag-avai" id="tagAvai-${tag?.id}">${tag?.name}</div>
                    `
    }

    function createTagItemHTML(tag) {
        return `
                    <div class="tag-item">
                        <div class="text-too-long">${tag?.name}</div>
                        <div class="tag-action" data-id="${tag?.id}">
                            <i class="fa fa-times" ></i>
                        </div>
                    </div>
                    `
    }

    function renderTagItemContainer() {
        let tagItemsHTML = '';
        let tagItems = [];

        for (const tagItemId of tagItemIds) {
            tagItems.push(tagList.find(item => item.id === tagItemId));
        }

        for (const tagItem of tagItems) {
            tagItemsHTML += createTagItemHTML(tagItem);
        }
        $tagItemContainer.empty().append(tagItemsHTML);
    }

    function renderTagAvai(searchKey = '') {
        let temp = [];
        temp = tagList.filter(item => !tagItemIds.includes(item.id))

        if (searchKey !== '') {
            temp = temp.filter(item => item.name.toLowerCase().search(searchKey.toLowerCase()) !== -1);
        }

        tagAvaiIds = temp.map(item => item.id);
        console.log(tagAvaiIds);
        $tagContainer.popover('show')
    }

    $(document).on('keyup', `${containerSelector} #input-tag`, function (e) {
        const inputValue = e.currentTarget.value;
        console.log(inputValue);
        renderTagAvai(inputValue);
    })

    $(document).on('click', `${containerSelector} .tag-avai`, function (e) {
        const tagId = parseInt(e.currentTarget.id.split('-')[1]);
        tagAvaiIds.splice(tagAvaiIds.indexOf(tagId), 1);
        tagItemIds.push(tagId);
        renderTagItemContainer();
        $input.focus()

        // $tagContainer.popover('hide')
        $tagContainer.popover('show')

        if ($input.val() !== '') {
            $input.val('');
            renderTagAvai('');
        }
    })

    $(document).on('click', `${containerSelector} .tag-action`, function (e) {
        const tagId = parseInt(e.currentTarget.dataset.id);
        tagItemIds.splice(tagItemIds.indexOf(tagId), 1);
        renderTagItemContainer();
        renderTagAvai();
        $input.focus()
        // $tagContainer.popover('hide')
        $tagContainer.popover('show')

    })


    $tagContainer.popover({
        container: containerSelector,
        // trigger: 'focus',
        placement: 'bottom',
        html: true,
        content: function () {
            let tagAvaisHTML = '';
            const tagAvais = tagList.filter(item => tagAvaiIds.includes(item.id));
            if (tagAvais.length === 0) {
                tagAvaisHTML = ' <div class="tag-notavai">Không có dữ liệu</div>'
            }
            for (const tagAvai of tagAvais) {
                tagAvaisHTML += createTagAvaiHTML(tagAvai);
            }

            let contentTagHTML = `
                            <div class="tag-overlay-container" >
                                <div class="tag-avai-container">
                                ${tagAvaisHTML}
                                </div>
                            </div>
                        `
            return contentTagHTML;
        }
    })

    renderTagItemContainer();
    loadTagList({
        callback: function () {
            tagAvaiIds = tagList.map(item => item?.id)
        }
    });

    function setTagItemIds(params = []) {
        currentTagIdList = [...params];
        tagItemIds = [...params];
        // tagAvaiIds = tagAvaiIds.filter(id => !tagItemIds.includes(id));
        renderTagItemContainer()
        renderTagAvai()
    }

    function getTagIdsDelete() {
        return currentTagIdList.filter(item => !tagItemIds.includes(item)) ?? [];
    }

    function getTagIdsCreate() {
        return tagItemIds.filter(item => !currentTagIdList.includes(item)) ?? []
    }

    function getTagList() {
        return tagList
    }

    function getTagAvaiIds() {
        return tagAvaiIds
    }

    function getTagItemIds() {
        return tagItemIds
    }


    return {
        getTagList,
        getTagItemIds,
        getTagAvaiIds,
        getTagIdsCreate,
        getTagIdsDelete,
        setTagItemIds
    }
}

var defineComment = function ({ selector = '#comment-container', showMode = 'collapse' }) {
    const $commentContainer = $(`<div class="comment-container"></div>`);
    $(selector).append($commentContainer);
    const $commentListContainer = $(`<div class="comment-list"><ul></ul></div>`);
    const $commentList = $commentListContainer.find('ul');
    const $commentViewMore = $(`
        <div class="cmt__view__more">Hiển thị thêm bình luận</div>
    `);
    $commentContainer.append($commentListContainer);
    $commentContainer.append($commentViewMore);
    let commentModules = [];
    let currentPage = 1;
    let currentPostId = '';

    function createCommentManager(commentData) {
        const {
            id = '',
            content = '',
            created_at = '',
            user = { fullname: '', avatar_url: DEFAULT_AVATAR, id: '' },
            images = [],
        } = commentData;
        console.log(commentData);

        const $comment = $(`
        <li class="cmt__container cmt-${id}">
        <div class="cmt__main">
            <div class="cmt__avatar">
                <a href="#">
                    <img src="${user?.avatar_url ?? DEFAULT_AVATAR}" class="img-fluid cmt__avatar__img" alt="">
                </a>
            </div>
            <div class="cmt__content">
                <div class="cmt__content__main">
                    <div class="cmt__username">
                        <a href="#">${user?.fullname ?? ''}</a>
                    </div>
                    <div class="cmt__content__inner">
                        <p class="cmt__content__text">
                           ${content ?? ''}
                        </p>

                    </div>

                </div>
                ${images?.length > 0 ? `<div class="cmt__content__img"></div>` : ''}
                <!--<div class="cmt__content__emoji">
                    <span class="cmt__content__emoji__icon__heart">
                        <img src="${PREFIX_API}/backend/images/heart-emoji.svg" class="img-fluid" alt="">
                    </span>
                    <span class="cmt__content__emoji__count">1</span>
                </div> -->
            </div>
            <div class="cmt__util">

            </div>
        </div>
        <div class="cmt__reply"></div>
    </li>
        `);

        const $commentAvatar = $comment.find('.cmt__avatar');
        const $commentContentText = $comment.find('.cmt__content__text');
        const $commentContentUtil = $comment.find('.cmt__content__util');
        const $commentReply = $comment.find('.cmt__reply');
        const $commentImgContainer = $comment.find('.cmt__content__img');
        const $commentViewMoreContent = $(`<span class="cmt__content__view__more"> Xem thêm</span>`);


        for (const image of images) {
            $commentImgContainer.append(`<div class="cmt__content__img__item cmt__content__img__item-${image?.id}">
                                <img src="${image?.url}" class="img-fluid" alt="">
                            </div>`);
        }

        function handleShowContentText() {
            if (content.length > 100) {
                $commentContentText.empty();
                $commentContentText.append(content?.summary(100));
                $commentContentText.append($commentViewMoreContent);
            }

            $commentViewMoreContent.off('click').on('click', function () {
                $commentContentText.empty();
                $commentContentText.append(content ?? '');
                $commentViewMoreContent.hide();
            });
        }

        handleShowContentText();

        return {
            $comment,
        }
    }

    const pushCommentModules = function (commentData) {
        const commentModule = {
            data: commentData,
            manager: createCommentManager(commentData)
        }
        commentModules.push(commentModule);
        $commentList.append(commentModule.manager.$comment);
    }

    const init = function (commentDatas) {
        commentDatas.forEach(data => pushCommentModules(data));
    }

    if (showMode === 'collapse') {
        $commentContainer.collapse();
    }

    function loadComments(postId = '', page = 1) {
        $.ajax({
            url: `${PREFIX_API}/shop/comments?page=${page}&post_id=${postId}`,
            method: "GET",
            loading: { type: 'content' },
        }).done(res => {
            const comments = res?.results?.data ?? [];
            if (comments?.length === 0) {
                $commentViewMore.hide();
            } else {
                for (const comment of comments) {
                    pushCommentModules(comment);
                }
            }
        }).fail(err => {
        });
    }

    function initCommentsForPost(postId) {
        currentPostId = postId;
        currentPage = 1;
        $commentViewMore.show();
        $commentList.empty();
        commentModules = [];
        loadComments(postId, currentPage);
    }

    $commentViewMore.on('click', function () {
        currentPage++;
        loadComments(currentPostId, currentPage);
    })

    return {
        $commentContainer,
        commentModules,
        pushCommentModules,
        init,
        initCommentsForPost
    }
}
