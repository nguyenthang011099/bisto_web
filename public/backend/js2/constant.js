var MAX_INT32 = 2147483647;
var MIN_MONEY = 1000;
var MAX_MONEY = 2147483647;
var PREFIX_API = 'http://127.0.0.1:8000/';
var PREFIX_API_SHOP = 'http://127.0.0.1:8000/shop';
var PREFIX_API_ADMIN_SHOP = 'http://127.0.0.1:8000/admin-shop';
var PREFIX_API_ADMIN = 'http://127.0.0.1:8000/admin';
var MES_PERCENT = 'Phần trăm >= 0 và <= 100'
var DEFAULT_AVATAR = 'https://i.pinimg.com/originals/51/f6/fb/51f6fb256629fc755b8870c801092942.png';
var _TOKEN = '';
var USER_ID = 0;
var CONTEXT = 'layout';
var _GLOBAL_ = {
    message: {
        receiver_id: 0,
    }
}
