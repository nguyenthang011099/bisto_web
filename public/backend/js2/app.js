var BASE_URL;
var DEFAULT_THUMB;
var PER_PAGE;
var LOGO_URL;

var initBlogPage = function ({containerSelector = '#blog-cate-container', mode = 'normal'}) {

    function createLogo() {

    }

    function testImage(url) {
        return new Promise((resolve, reject) => {
            const tester = new Image();
            tester.src = url;
            tester.onload = resolve;
            tester.onerror = reject
        });
    }

    const $container = $(containerSelector);

    function createBlogCard(blog) {
        const tagElement = blog?.tags[0]?.name !== undefined ? `<span href="#" class="blog-card__tag__item">${blog?.tags[0]?.name}</span>` : ''
        const html = `
                      <div class="blog-card col-sm-6 col-md-4 col-lg-3 col-xl-2 col-xxl-2">
                    <a class="blog-card__content" href="${BASE_URL}/blogs/${blog?.slug}">
                        <div>
                            <div class="blog-card__thumb">
                                <img
                                src="${blog?.image?.url}"
                                class="img-fluid" alt="${blog?.title}">
                            </div>
                            <div class="blog-card__detail">
                                    <span class="blog-card__title">${blog?.title ?? ''}</span>
                                <p class="blog-card__des">${blog?.description ?? ''}</p>
                                <div class="blog-card__tag">
                                    ${tagElement}
                                </div>
                                <div class="blog-card__tail">
                                    <div class="blog-card__tail__left">
                                         <div class="blog-card__logo">
                                             <img
                                                src="${LOGO_URL}"
                                                class="img-fluid" alt="${blog?.title}">
                                         </div>
                                         <span>Bisto</span>
                                    </div>
                                    <div class="blog-card__createdAt">
                                        <i class="fa fa-clock-o"></i>
                                        ${moment(blog?.created_at).format("D/MM/YYYY")}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a>
                </div>
            `
        return $(html);
    }

    function createBlogCate(blogCategory) {
        const $self = $(
            `
        <div class="blog-cate__item blog-card-container">
            <div class="blog-cate__title super-center">
                <h3>${blogCategory?.name?.toUpperCase() ?? ''}</h3>
            </div>
            <div class="blog-cate__content blog-cate__${blogCategory?.id ?? ""}">
            </div>
            <div class="blog-cate__footer super-center">
                <button class="btn btn-more">Xem thêm</button>
            </div>
        </div>
`
        )
        const $blogCardContainer = $self.find('.blog-cate__content')
        const $title = $self.find('h3');
        const $btnMore = $self.find('.btn-more');

        $btnMore.on('click', function () {
            if (mode == 'slide') {
                window.open(`${BASE_URL}/blog-categories/${blogCategory.slug}`, '_blank')
            } else {
                blogCategory.currentPage = blogCategory.currentPage + 1;
                loadBlogs({
                    page: blogCategory.currentPage,
                    blog_category_id: blogCategory?.id,
                    callback: function (blogs, total) {
                        if (blogs?.length === 0 || total <= blogCategory.currentPage * PER_PAGE) {
                            $btnMore.remove();
                        }
                        appendBlogCards(blogs);
                    }
                })
            }

        })

        function appendBlogCards(blogs = []) {
            for (const blog of blogs) {
                const $blogCard = createBlogCard(blog);
                testImage(blog?.image?.url).then().catch(() => {
                    $blogCard.find('.blog-card__thumb img').attr('src', DEFAULT_THUMB)
                })
                $blogCardContainer.append($blogCard);
            }
        }


        return {
            $self,
            $blogCardContainer,
            $title,
            $btnMore,
            appendBlogCards
        }
    }

    function loadBlogs({
                           page = 1,
                           blog_category_id = '',
                           limit = 10,
                           type = "normal",
                           blogCateInstance,
                           callback = function () {
                           }
                       }) {
        $.ajax({
            url: `${BASE_URL}/_blogs?page=${page}&blog_category_id=${blog_category_id}&limit=${limit}&type=${type}`,
            method: 'GET',
        }).done(res => {
            callback(res?.results?.data ?? [], res?.results?.total ?? 0);
            const maxItems = res?.results?.data.length >= 6 ? 6 : res?.results?.data.length;
            if (mode === 'slide') {
                const $slider = blogCateInstance?.$blogCardContainer ?? $(`${containerSelector} .blog-cate__content`);
                $slider.css('flex-wrap', 'nowrap')
                $slider.slick({
                    dots: false,
                    infinite: true,
                    speed: 300,
                    autoplay: true,
                    autoplaySpeed: 5000,
                    slidesToShow: maxItems,
                    slidesToScroll: 1,
                    prevArrow: `<button type="button" data-role="none" class="slick-prev">
                                       <i class="fa fa-chevron-left"></i>
                                </button>`,
                    nextArrow: `<button type="button" data-role="none" class="slick-next">
                                    <i class="fa fa-chevron-right"></i>
                                </button>`,
                    responsive: [
                        {
                            breakpoint: 1368,
                            settings: {
                                slidesToShow: 4,
                            }
                        },
                        {
                            breakpoint: 1200,
                            settings: {
                                dots: false,
                                slidesToShow: 4,
                            }
                        },
                        {
                            breakpoint: 992,
                            settings: {
                                slidesToShow: 3,
                            }
                        },
                        {
                            breakpoint: 720,
                            settings: {
                                slidesToShow: 2,
                            }
                        },
                        {
                            breakpoint: 540,
                            settings: {
                                slidesToShow: 1,
                            }
                        }
                    ]
                })
            }
        }).fail(err => {
        })
    }

    return {
        createBlogCate,
        createLogo,
        createBlogCard,
        $container,
        loadBlogs
    }
}
