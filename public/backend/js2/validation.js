/*
* Validate input by multi validateFunction
* EX:
*    <input type="text" name="name" class="need-validate form-control">
    <div class="mes-name mes-invalid">
       Tên sản phẩm là bắt buộc
    </div>
* */
let validate = (listValidateFunction = [{
    func: rangeValidation,
    params: {min: 0, max: 100}, mes: 'Trường không hợp lệ'
}], inputElement = 'input', mesElement = '.mes-invalid', cssClassMes = 'display', parentSelector = document, _value = NaN) => {
    const $inputs = $(inputElement, parentSelector);
    const $mess = $(mesElement, parentSelector);
    for (let i = 0; i < $inputs.length; i++) {
        let value = isNaN(_value) ? $inputs[i].value : _value;
        let mesE = $mess[i] ? $mess[i] : $mess[0];
        for (let j = 0; j < listValidateFunction.length; j++) {
            if (listValidateFunction[j].func(value, listValidateFunction[j].params)) {
                mesE.classList.remove(`mes-iv-${cssClassMes}`);
                mesE.classList.add(`mes-v-${cssClassMes}`)
                $inputs[i].classList.remove(`is-invalid`);
                //$inputs[i].classList.add(`is-valid`);
                //$inputs[i].style.border = '1px solid #d7dbda';
            } else {
                mesE.textContent = listValidateFunction[j].mes;
                mesE.classList.remove(`mes-v-${cssClassMes}`);
                mesE.classList.add(`mes-iv-${cssClassMes}`)
                //$inputs[i].classList.remove(`is-valid`);
                $inputs[i].classList.add(`is-invalid`);
                //$inputs[i].style.border = '1px solid red';
                return false;
            }
        }
    }
    return true;
}

let customValidate = (listValidateFunction = [{
    func: rangeValidation,
    params: {min: 0, max: 100}, mes: 'Trường không hợp lệ'
}], inputElement = 'input', mesElement = '.mes-invalid', value = '') => {
    for (let i = 0; i < listValidateFunction.length; i++) {
        if (listValidateFunction[i].func(listValidateFunction[i].params)) {
            $(mesElement).hide();
            //$(inputElement).css('border', '1px solid #d7dbda');
        } else {
            $(mesElement).text(listValidateFunction[i].mes);
            $(mesElement).show();
            // $(inputElement).css('border', '1px solid red');
            return false;
        }
    }
    return true;
}


/*
* validate input when user enter or submit
* */
var validateInput = ({
                         listValidateFunction = [{
                             func: rangeValidation,
                             params: {min: -Infinity, max: Infinity},
                             mes: 'Trường không hợp lệ'
                         }],
                         propertyName = 'xxx',
                         type = 'onsubmit',
                         requiredElement = false,
                         customInput = '',
                         customMes = '',
                         cssClassMes = 'display',
                         parentSelector = document,
                         formData = [{name: '', value: ''}],
                         value = NaN
                     }) => {
    switch (type) {
        case "onsubmit":
            return validate(listValidateFunction, `input[name=${propertyName}]`, `.mes-${propertyName}`, cssClassMes, parentSelector);
        case 'onkeyup':
            $(`input[name=${propertyName}]`).keyup(function () {
                validate(listValidateFunction, `input[name=${propertyName}]`, `.mes-${propertyName}`, cssClassMes, parentSelector);
            })
            break;
        case 'form':
            return validate(listValidateFunction, `input[name=${propertyName}]`, `.mes-${propertyName}`, cssClassMes, parentSelector, formData.find(item => item.name === propertyName));
        case 'select':
            return validate(listValidateFunction, `select[name=${propertyName}]`, `.mes-${propertyName}`, cssClassMes, parentSelector);
        case 'custom':
            return customValidate(listValidateFunction, customInput, customMes);
        case 'textarea':
            return validate(listValidateFunction, `textarea[name=${propertyName}]`, `.mes-${propertyName}`, cssClassMes, parentSelector);
        default:
            return false;
    }
}

var emailValidation = (email) => {
    const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(String(email).toLowerCase()) || isEmptyValue(email);
}
var requiredValidation = (value) => !isEmptyValue(value);
var requiredElementValidation = (params = {classNameElement: ''}) => !($(params.classNameElement).length === 0);
//only validate number
var rangeValidation = (value, params = {
    min: 0,
    max: 100
}) => {
    params.max = params.max > MAX_INT32 ? MAX_INT32 : params.max;
    return (parseFloat(value) >= params.min && parseFloat(value) <= params.max) || isEmptyValue(value);
}
//var passwordValidation = (value) => !/[^\x00-\x7F]+/.test(value) && /(?=.{6,})/.test(value);
var passwordValidation = (value) => (!/[^\x00-\x7F]+/.test(value) && /^[0-9]{6}$/.test(value)) || isEmptyValue(value);
var phoneValidation = (value) => /^[0-9]{1,10}$/.test(value) || isEmptyValue(value);
var urlValidation = (value) => /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/.test(value) || isEmptyValue(value);
var validateRequiredElement = ({
                                   inputSelector,
                                   mesSelector,
                                   message,
                                   parentSelector = document,
                                   cssClassMes = 'display'
                               }) => {
    const $mes = $(mesSelector, parentSelector);
    if ($(inputSelector, parentSelector).length === 0) {
        $mes.text(message);
        $mes.addClass(`mes-iv-${cssClassMes}`).removeClass(`mes-v-${cssClassMes}`);
        return false;
    } else {
        $mes.addClass('mes-v-display').removeClass(`mes-iv-${cssClassMes}`);
        return true;
    }
}

/*
* prevent user enter wrong value in range
* */
var validateRangeNumber = (inputElementNumber, min = 0, max = 100, isInt = false) => {
    $(inputElementNumber).data('prevValue', '');
    $(inputElementNumber).keyup(function (event) {
        let value = $(this).val();
        if ($(this).data('prevValue') != "" && (event.key == '-' || event.key == '+')) {
            $(this).val($(this).data('prevValue'));
        } else if (event.key != '.') {
            $(this).data('prevValue', value);
        }

        if (isInt && event.key == '.') {
            $(this).val($(this).data('prevValue'));
        } else if (parseFloat(value) > max) {
            $(this).val(max);
        }
        // else if (parseFloat(value) < min) {
        //     $(this).val(min);
        // }
    })
    $(inputElementNumber).change(function () {
        let value = $(this).val();
        if (parseFloat(value) > max) {
            $(this).val(max);
        }
        //  else if (parseFloat(value) < min) {
        //     $(this).val(min);
        // }
    })
}

/*
* validate duplicate sise color id
* */
var validateDuplicateSizeColorOnSubmit = (classElement) => {
    const sizeColorSelected = [];
    for (const element of $(classElement)) {
        const v = {
            size_id: element.querySelector('select[name=size_id]').value,
            color_id: element.querySelector('select[name=color_id]').value
        }
        if (sizeColorSelected.filter(item => item.size_id == v.size_id && item.color_id == v.color_id).length > 0) {
            $('.mes-dup-sc').show();
            return false;
        } else
            sizeColorSelected.push(v)
    }
    $('.mes-dup-sc').hide();
    return true;
}

/*
* validate date1 < date2 when user submit
* */
var validateRangeDate = (inputDate1, inputDate2, mesElement, parentSelector = document) => {
    const time1 = new Date($(inputDate1, parentSelector).val()).getTime();
    const time2 = new Date($(inputDate2, parentSelector).val()).getTime();
    if (time1 > time2) {
        $(mesElement).text('Ngày bắt đầu phải nhỏ hơn ngày kết thúc').show();
        return false;
    } else if (isNaN(time1) || isNaN(time2)) {
        $(mesElement).text('Ngày giờ là bắt buộc').show();
        return false;
    } else $(mesElement).hide();
    return true;
}

/*
* validate time1 < time2 when user submit
* */
var validateRangeTime = (inputTime1, inputTime2, mesElement, parentSelector = document) => {
    const ar1 = $(inputTime1, parentSelector).val().split(':');
    const ar2 = $(inputTime2, parentSelector).val().split(':');
    const time1 = parseInt(ar1[0]) * 60 + parseInt(ar1[1]);
    const time2 = parseInt(ar2[0]) * 60 + parseInt(ar2[1]);
    if (time1 > time2) {
        $(mesElement).text('Giờ bắt đầu phải nhỏ hơn giờ kết thúc').show();
        return false;
    } else if (isNaN(time1) || isNaN(time2)) {
        $(mesElement).text('Khung giờ là bắt buộc').show();
        return false;
    } else $(mesElement).hide();
    return true;
}

/*
* restrict user typing uppercase
* */
var validateOnKeyupUppercase = (inputElement) => {
    $(inputElement).on('input', function () {
        $(this).val($(this).val().replace(/[^a-z0-9]/gi, '').toUpperCase());
    });
}

var validateChangePassword = (currentPass = "", newPass = "", confirmPass = "") => {
    let regex = '^\\d{6,6}$';
    if (newPass == currentPass) {
        $('input[name=confirm_password]').css('border', 'solid, 1px, red');
        $('.not-equal-newpass').hide();
        $('.wrong-type-pass').hide();
        $('.equal-pass').show();
        return false;
    } else if (!new RegExp(regex).test(newPass)) {
        $('input[name=new_password]').css('border', 'solid, 1px, red');
        $('.wrong-type-pass').show();
        $('.not-equal-newpass').hide();
        $('.equal-pass').hide();
        return false;
    } else if (newPass !== confirmPass) {
        $('input[name=new_password]').css('border', 'solid, 1px, red');
        $('.equal-pass').hide();
        $('.wrong-type-pass').hide();
        $('.not-equal-newpass').show();
        return false;
    } else {
        $('.wrong-type-pass').hide();
        $('.not-equal-newpass').hide();
        $('.equal-pass').hide();
        return true;
    }
}
