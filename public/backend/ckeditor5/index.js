var createCkeditor5 = ({
                           selector = '#editor', cb = function () {
    }
                       }) => {
    let _editor;
    let fileList = [];

    class MyUploadAdapter {
        constructor(loader) {
            // CKEditor 5's FileLoader instance.
            this.loader = loader;
            // URL where to send files.
        }

        // Starts the upload process.
        upload() {
            return new Promise((resolve, reject) => {
                // this._initRequest();
                // this._initListeners(resolve, reject);
                // this._sendRequest();
                this.loader.file.then(async (file) => {
                    fileList.push(file);
                    const url = await uploadSingleFile(file);
                    // data.append('upload', result);
                    // // console.log(data.data.data)
                    resolve({
                        default: url,
                    });
                });
            });
        }

        // // Aborts the upload process.
        // abort() {
        // 	if (this.xhr) {
        // 		this.xhr.abort();
        // 	}
        // }

        // // Example implementation using XMLHttpRequest.
        // _initRequest() {
        // 	// const xhr = this.xhr = new XMLHttpRequest();
        // 	// xhr.open('POST', this.url, true);
        // 	// xhr.responseType = 'json';
        // 	// xhr.setRequestHeader('Access-Control-Allow-Origin', '*')
        // 	// XMLHttpRequest.setRequestHeader('', value)
        // 	//   xhr.setRequestHeader('Authorization', getToken())
        // }

        // // Initializes XMLHttpRequest listeners.
        // _initListeners(resolve, reject) {
        // 	const xhr = this.xhr;
        // 	const loader = this.loader;
        // 	const genericErrorText = "Couldn't upload file:" + ` ${loader.file.name}.`;

        // 	xhr.addEventListener('error', () => reject(genericErrorText));
        // 	xhr.addEventListener('abort', () => reject());
        // 	xhr.addEventListener('load', () => {
        // 		const response = xhr.response;
        // 		if (!response || response.error) {
        // 			return reject(
        // 				response && response.error
        // 					? response.error.message
        // 					: genericErrorText,
        // 			);
        // 		}

        // 		// If the upload is successful, resolve the upload promise with an object containing
        // 		// at least the "default" URL, pointing to the image on the server.
        // 		resolve({
        // 			default: response.s3Url,
        // 		});
        // 	});

        // 	if (xhr.upload) {
        // 		xhr.upload.addEventListener('progress', (evt) => {
        // 			if (evt.lengthComputable) {
        // 				loader.uploadTotal = evt.total;
        // 				loader.uploaded = evt.loaded;
        // 			}
        // 		});
        // 	}
        // }

        // // Prepares the data and sends the request.
        // _sendRequest() {
        // 	const data = new FormData();

        // 	this.loader.file.then((result) => {
        // 		data.append('upload', result);
        // 	});
        // }
    }

    const MyCustomUploadAdapterPlugin = function (editor) {
        editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
            return new MyUploadAdapter(loader);
        };
    }

    ClassicEditor
        .create(document.querySelector(selector), {
            // removePlugins: ['Base64UploadAdapter', 'Markdown'],
            extraPlugins: [MyCustomUploadAdapterPlugin],
            licenseKey: '',
            toolbar: [
                'heading',
                '|',
                'fontFamily',
                'fontColor',
                'fontBackgroundColor',
                '|',
                'bold',
                'italic',
                'link',
                'bulletedList',
                'numberedList',
                '|',
                'outdent',
                'indent',
                '|',
                'imageUpload',
                'blockQuote',
                'insertTable',
                'mediaEmbed',
                'undo',
                'redo',
            ],
            htmlEmbed: {
                showPreviews: true,
                // sanitizeHtml: (inputHtml) => {
                //     // Strip unsafe elements and attributes, e.g.:
                //     // the `<script>` elements and `on*` attributes.
                //     const outputHtml = sanitize(inputHtml);
                //
                //     return {
                //         html: outputHtml,
                //         // true or false depending on whether the sanitizer stripped anything.
                //         hasChanged: true
                //     };
                // }
            },
            fontColor: {
                colors: [
                    {
                        color: 'hsl(0, 0%, 0%)',
                        label: 'Default'
                    },
                    {
                        color: '#e76808',
                        label: 'Orange'
                    },
                    {
                        color: 'hsl(0, 0%, 30%)',
                        label: 'Dim grey'
                    },
                    {
                        color: 'hsl(0, 0%, 60%)',
                        label: 'Grey'
                    },
                    {
                        color: 'hsl(0, 0%, 90%)',
                        label: 'Light grey'
                    },
                    {
                        color: 'hsl(0, 0%, 100%)',
                        label: 'White',
                        hasBorder: true
                    },

                    // ...
                ]
            },
        })
        .then(editor => {
            window.editor = editor;
            _editor = editor;
            _editor.execute('fontColor', {value: 'hsl(0, 0%, 0%)'});
            initEvent();
            cb();
        })
        .catch(error => {
            console.error('Oops, something went wrong!');
            console.error('Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:');
            // console.warn('Build id: x9zid630yq03-nohdljl880ze');
            console.error(error);
        });

    function initEvent() {
        const toolbarElement = _editor.ui.view.toolbar.element;
        _editor.on('change:isReadOnly', (evt, propertyName, isReadOnly) => {
            if (isReadOnly) {
                toolbarElement.style.display = 'none';
            } else {
                toolbarElement.style.display = 'flex';
            }
        });
    }

    function getFileList() {
        return fileList
    }

    function getEditor() {
        return _editor
    }

    function setDisable() {
        _editor.isReadOnly = true
    }

    return {
        _editor,
        getEditor,
        getFileList,
        setDisable
    }

}
