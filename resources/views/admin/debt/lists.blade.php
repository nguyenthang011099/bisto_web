@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page debt-admin">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Xuất Excel công nợ
                </div>
                <div style="padding: 20px" class="row btn-bisto row-action-bisto-css">
                    <div class="col-sm-4">
                        <select class="form-control" id="btn-range-date" name="btn-range-date">
                            <option value="0" selected>Trong cả tháng</option>
                            <option value="1">Nửa đầu tháng</option>
                            <option value="2">Nửa cuối tháng</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <select class="form-control" id="btn-month" name="btn-month">
                            <option selected value="0">Tất cả 12 tháng</option>
                            <option value="1">Tháng 1</option>
                            <option value="2">Tháng 2</option>
                            <option value="3">Tháng 3</option>
                            <option value="4">Tháng 4</option>
                            <option value="5">Tháng 5</option>
                            <option value="6">Tháng 6</option>
                            <option value="7">Tháng 7</option>
                            <option value="8">Tháng 8</option>
                            <option value="9">Tháng 9</option>
                            <option value="10">Tháng 10</option>
                            <option value="11">Tháng 11</option>
                            <option value="12">Tháng 12</option>
                        </select>
                    </div>
                    {{--                    <div class="col-sm-3">--}}
                    {{--                        <select class="form-control" id="btn-precious" name="btn-precious">--}}
                    {{--                            <option selected value="1">Quý 1</option>--}}
                    {{--                            <option value="2">Quý 2</option>--}}
                    {{--                            <option value="3">Quý 3</option>--}}
                    {{--                            <option value="4">Quý 4</option>--}}
                    {{--                        </select>--}}
                    {{--                    </div>--}}
                    <div class="col-sm-4">
                        <select class="form-control" id="btn-year" name="btn-year">
                            @foreach($_years_ as $year)
                                <option value="{{$year}}">Năm {{$year}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="iq-card-body body-bisto">
                    <div class="row">
                        <div class="input-group col-sm-6">
                            <div type="button" id="export-revenue-subsidy"
                                 class="export-excel-debt btn input-group-prepend btn-outline-success d-flex align-items-center justify-content-between"
                                {{--                               href="{{url('/admin')}}/revenue-bisto-one?option=0&month=&precious=3&year=2021"--}}
                            >
                                <span class="fas mr-1 fa-file-excel fa-lg"></span>
                                Tải Excel Admin
                            </div>
                            <select class="form-select form-control" name="revenue-subsidy" id="revenue-subsidy">
                                <option selected value="revenue-bisto-one">Doanh thu bisto 1</option>
                                <option value="revenue-bisto-two">Doanh thu bisto 2</option>
                                <option value="subsidy-bisto">Bisto trợ giá</option>
                                <option value="payment-debts">Thanh toán</option>
                            </select>
                            <button data-table="revenue-subsidy" class="btn-query-debt btn btn-append btn-primary">Truy
                                vấn
                            </button>
                        </div>
                        <div class="input-group col-sm-6">
                            <div type="button" id="export-unit"
                                 class="export-excel-debt btn input-group-prepend btn-outline-success d-flex align-items-center justify-content-between">
                                <span class="fas mr-1 fa-file-excel fa-lg"></span>
                                Tải Excel Vận Chuyển
                            </div>
                            <select class="form-select form-control" id="unit" name="unit">
                                <option selected value="{{\App\ShippingOrder::GIAO_HANG_NHANH}}">Giao hàng nhanh
                                </option>
                                <option value="{{\App\ShippingOrder::AHAMOVE}}">Ahamove</option>
                                <option value="{{\App\ShippingOrder::GHTK}}">Giao hàng tiết kiệm</option>
                            </select>
                            <button data-table="unit" class="btn-query-debt btn btn-append btn-primary">Truy vấn
                            </button>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="input-group col-sm-6">
                            <div type="button" id="export-admin-shop"
                                 class="export-excel-debt btn input-group-prepend btn-outline-success d-flex align-items-center justify-content-between">
                                <span class="fas mr-1 fa-file-excel fa-lg"></span>
                                Tải Excel Chuỗi
                            </div>
                            <select class="form-select form-control" id="admin-shop" name="admin-shop">
                            </select>
                            <button data-table="admin-shop" class="btn-query-debt btn btn-append btn-primary">Truy vấn
                            </button>
                        </div>
                        <div class="input-group col-sm-6">
                            <div type="button" id="export-shop"
                                 class="export-excel-debt btn input-group-prepend btn-outline-success d-flex align-items-center justify-content-between">
                                <span class="fas mr-1 fa-file-excel fa-lg"></span>
                                Tải Excel Cửa Hàng
                            </div>
                            <select name="shop">
                            </select>
                            <button data-table="shop" class="btn-query-debt btn btn-append btn-primary">Truy vấn
                            </button>
                        </div>
                    </div>
                </div>

                <div class="iq-card-body body-bisto debt-container">
                    <div class="panel-heading">
                    </div>

                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>

                        </tr>
                        </thead>

                        <tbody class="tbody-bisto">
                        </tbody>
                    </table>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <!-- ADD PRODUCT START -->
    </div>
    <script>
        let dt = {
            currentPage: 1,
            keyword: '',
            option: '0',
            month: '0',
            year: '2021',
            loading: {type: 'content'},
            shopId: '',
            currentTable: 'revenue-bisto-one',
            adminShopId: null,
            unit: '',
            shopName: '',
            title: 'Doanh thu bisto 1'
        };
        let $selectShop, pagination;
        $(document).ready(function () {
            loadListAdminShop();
            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadDebts(dt);
                }
            });
            loadDebts(dt);
            $selectShop = defineLazyLoadingSelect({
                selectSelector: 'select[name=shop]',
                url: 'lists/shops',
                valueName: 'user_id',
                textName: 'shop_name',
                name: 'users',
                className: 'debt-select-shop',
                urlParams: {is_without_branch: 1},
                loading: {type: 'content', selector: '.loading-selector'}
            });
            $selectShop.onSelect(function (value, event, ui) {
                dt.shopId = value;
                dt.shopName = ui.item.label;
            });
        });

        $('select[name=btn-range-date]').on('change', function (e) {
            dt.option = e.currentTarget.value;
        });

        $('select[name=btn-month]').on('change', function (e) {
            dt.month = e.currentTarget.value;
        });

        $('select[name=btn-year]').on('change', function (e) {
            dt.year = e.currentTarget.value;
        });

        // $('select[name=unit]').on('change', function (e) {
        //     dt.currentTable = 'delivery-unit';
        //     dt.unit = e.currentTarget.value;
        // });
        //
        // $('select[name=shop]').on('change', function (e) {
        //     dt.currentTable = 'shop/debts';
        //     dt.shopId = e.currentTarget.value;
        // });
        //
        $('select[name=admin-shop]').on('change', function (e) {
            dt.adminShopId = e.currentTarget.value;
        });
        //
        // $('select[name=revenue-subsidy]').on('change', function (e) {
        //     dt.currentTable = e.currentTarget.value;
        // });

        $('button.btn-query-debt').on('click', function (e) {
            switch (e.currentTarget.dataset.table) {
                case 'revenue-subsidy':
                    dt.currentTable = $('select[name=revenue-subsidy]').val();
                    dt.title = $('select[name=revenue-subsidy] option:selected').text();
                    break;
                case 'unit':
                    dt.currentTable = 'delivery-unit';
                    dt.unit = $('select[name=unit]').val();
                    dt.title = $('select[name=unit] option:selected').text();
                    break;
                case 'admin-shop':
                    dt.currentTable = 'admin-shop/debts';
                    dt.adminShopId = $('select[name=admin-shop]').val();
                    dt.title = $('select[name=admin-shop] option:selected').text();
                    break;
                case 'shop':
                    dt.currentTable = 'shop/debts';
                    dt.title = dt.shopName;
                    break;
            }
            loadDebts(dt);
        })

        function loadListAdminShop() {
            $.ajax({
                url: 'lists/admin-shops?paginate=false',
                method: 'GET'
            }).done(res => {
                const users = res.users;
                let options = '';
                for (const user of users) {
                    options += `<option value=${user.user_id}>${user.shop_name}</option>`
                }
                $('select[name=admin-shop]').append(options);
            }).fail(err => {
                toastErrorMessage('Load danh sách admin-shop thất bại')
            })
        }

        function getStringParams() {
            const option = document.getElementById('btn-range-date').value;
            const month = document.getElementById('btn-month').value;
            const year = document.getElementById('btn-year').value;
            return `option=${option}&month=${month}&year=${year}`;
        }

        $('#export-revenue-subsidy').on('click', function () {
            const url = `{{url('/admin')}}/${$('#revenue-subsidy').val()}?${getStringParams()}`;
            $.ajax({
                url: url,
                method: 'GET',
                loading: {type: 'content'}
            }).done(res => {
                if (res) window.open(url, '_blank');
                else toastInfoMessage('Không có dữ liệu');
            }).fail(err => {
            })
        });

        $('#export-unit').on('click', function () {
            const url = `{{url('/admin/delivery-unit')}}?unit=${$('#unit').val()}&${getStringParams()}`;
            $.ajax({
                url: url,
                method: 'GET',
                loading: {type: 'content'}
            }).done(res => {
                if (res) window.open(url, '_blank');
                else toastInfoMessage('Không có dữ liệu');
            }).fail(err => {
            })
        });

        $('#export-shop').on('click', function () {
            const url = `{{url('/admin/shop/debts')}}?shopId=${dt.shopId}&${getStringParams()}`;
            $.ajax({
                url: url,
                method: 'GET',
                loading: {type: 'content'}
            }).done(res => {
                if (res) window.open(url, '_blank');
                else toastInfoMessage('Không có dữ liệu');
            }).fail(err => {
            })
        });

        $('#export-admin-shop').on('click', function () {
            const url = `{{url('/admin/admin-shop/debts')}}?shopId=${$('select[name=admin-shop]').val()}&${getStringParams()}`;
            $.ajax({
                url: url,
                method: 'GET',
                loading: {type: 'content'}
            }).done(res => {
                if (res) window.open(url, '_blank');
                else toastInfoMessage('Không có dữ liệu');
            }).fail(err => {
            })
        });

        function loadDebts({
                               currentPage = 1,
                               keyword = '',
                               option = '0',
                               month = '0',
                               year = '2021',
                               loading = {type: 'content'},
                               currentTable = 'revenue-bisto-one',
                               shopName = '',
                               title = '',
                           }) {
            let url = `{{url('/admin')}}/${currentTable}?page=${currentPage}`;
            let extraData = {};
            switch (currentTable) {
                case 'revenue-bisto-one':
                    break;
                case 'revenue-bisto-two':
                    break;
                case 'subsidy-bisto':
                    break;
                case 'admin-shop/debts':
                    extraData = {shopId: dt.adminShopId};
                    break;
                case 'shop/debts':
                    extraData = {shopId: dt.shopId};
                    break;
                case 'delivery-unit':
                    extraData = {unit: dt.unit};
                    break;
            }
            $.ajax({
                url: url,
                method: "POST",
                data: {
                    _token: '{{csrf_token()}}',
                    option: option,
                    month: month,
                    year: year,
                    ...extraData
                },
                loading,
            }).done(res => {
                const items = res?.result?.data ?? [];
                if (items?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadDebts(dt);
                } else {
                    generateDebts(currentTable, items);
                    pagination.setTotalItems(res?.result?.total ?? 0);
                }
            }).fail(err => {
            });
        }

        function generateDebts(type = 'revenue-bisto-one', items = []) {
            let tableElement = createElementByText(`<div>
                    <div class="panel-heading">
                    </div>
                    <table class="table table-striped table-bordered">
                        <thead class="thead-debt">

                        </thead>
                        <tbody class="tbody-debt">
                        </tbody>
                    </table></div>`);
            const $panelHeading = $(tableElement).find('.panel-heading');
            const $thead = $(tableElement).find('.thead-debt')[0];
            const $tbody = $(tableElement).find('.tbody-debt')[0];
            $panelHeading.text(dt.title);
            switch (type) {
                case 'revenue-bisto-one':
                    $thead.append(createElementByText(
                        `<tr>
                            <th class="text-center">Mã đơn</th>
                            <th class="text-center">Ngày tháng</th>
                            <th class="text-center">Shop</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Giá gốc</th>
                            <th class="text-center">Phí dịch vụ thu hộ</th>
                            <th class="text-center">Phí cố định</th>
                            <th class="text-center">Phí thanh toán</th>
                            <th class="text-center">Hoàn tiền</th>
                        </tr>
                        `)
                    );
                    if (items.length === 0) $tbody.append(createElementByText(genNoContentTable('9', 'Không có dữ liệu')));
                    for (const item of items) {
                        $tbody.append(createElementByText(
                            `<tr>
                            <td class="text-center">${item.code}</td>
                            <td class="text-center">${item.created_at}</td>
                            <td class="text-center">${item.shop_name}</td>
                            <td class="text-center">${item.status}</td>
                            <td class="text-center">${item.offer_total.formatVND()}</td>
                            <td class="text-center">${item.fee_service.formatVND()}</td>
                            <td class="text-center">${item.fixed_charge.formatVND()}</td>
                            <td class="text-center">${item.pay.formatVND()}</td>
                            <td class="text-center">${item.refund.formatVND()}</td>
                        </tr>`));
                    }
                    break;
                case 'revenue-bisto-two':
                    $thead.append(createElementByText(
                        `<tr>
                            <th class="text-center">Thời gian</th>
                            <th class="text-center">Shop</th>
                            <th class="text-center">Loại quảng cáo</th>
                            <th class="text-center">Bắt đầu</th>
                            <th class="text-center">Kết thúc</th>
                            <th class="text-center">Số tiền</th>
                        </tr>
                        `)
                    );
                    if (items.length === 0) $tbody.append(createElementByText(genNoContentTable('6', 'Không có dữ liệu')));
                    for (const item of items) {
                        $tbody.append(createElementByText(
                            `<tr>
                            <td class="text-center">${item.created_at}</td>
                            <td class="text-center">${item.shop}</td>
                            <td class="text-center">${item.type_advertise}</td>
                            <td class="text-center">${item.start_at}</td>
                            <td class="text-center">${item.end_at}</td>
                            <td class="text-center">${item.money.formatVND()}</td>
                        </tr>`));
                    }
                    break;
                case 'subsidy-bisto':
                    $thead.append(createElementByText(
                        `<tr>
                            <th class="text-center">Mã trợ giá</th>
                            <th class="text-center">Mã đơn hàng</th>
                            <th class="text-center">Thời gian</th>
                            <th class="text-center">Shop</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Trợ giá</th>
                        </tr>
                        `)
                    );
                    if (items.length === 0) $tbody.append(createElementByText(genNoContentTable('6', 'Không có dữ liệu')));
                    for (const item of items) {
                        $tbody.append(createElementByText(
                            `<tr>
                            <td class="text-center">${item.coupon_code}</td>
                            <td class="text-center">${item.code}</td>
                            <td class="text-center">${item.created_at}</td>
                            <td class="text-center">${item.shop_name}</td>
                            <td class="text-center">${item.status}</td>
                            <td class="text-center">${item.bisto_voucher.formatVND()}</td>
                        </tr>`));
                    }
                    break;
                case 'admin-shop/debts':
                    $thead.append(createElementByText(
                        `<tr>
                            <th class="text-center">Mã đơn</th>
                            <th class="text-center">Thời gian</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Thực thu</th>
                            <th class="text-center">Phí cố định</th>
                            <th class="text-center">Hoàn tiền</th>
                            <th class="text-center">Phí v/c đổi trả</th>
                        </tr>
                        `)
                    );
                    if (items.length === 0) $tbody.append(createElementByText(genNoContentTable('7', 'Không có dữ liệu')));
                    for (const item of items) {
                        $tbody.append(createElementByText(
                            `<tr>
                            <td class="text-center">${item.code}</td>
                            <td class="text-center">${item.created_at}</td>
                            <td class="text-center">${item.status}</td>
                            <td class="text-center">${item.real_money.formatVND()}</td>
                            <td class="text-center">${item.fixed_charge.formatVND()}</td>
                            <td class="text-center">${item.refund.formatVND()}</td>
                            <td class="text-center">${item.shipping_fee.formatVND()}</td>
                        </tr>`));
                    }
                    break;
                case 'shop/debts':
                    $thead.append(createElementByText(
                        `<tr>
                            <th class="text-center">Mã đơn</th>
                            <th class="text-center">Thời gian</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Thực thu</th>
                            <th class="text-center">Phí cố định</th>
                            <th class="text-center">Hoàn tiền</th>
                            <th class="text-center">Phí v/c đổi trả</th>
                        </tr>
                        `)
                    );
                    if (items.length === 0) $tbody.append(createElementByText(genNoContentTable('7', 'Không có dữ liệu')));
                    for (const item of items) {
                        $tbody.append(createElementByText(
                            `<tr>
                            <td class="text-center">${item.code}</td>
                            <td class="text-center">${item.created_at}</td>
                            <td class="text-center">${item.status}</td>
                            <td class="text-center">${item.real_money.formatVND()}</td>
                            <td class="text-center">${item.fixed_charge.formatVND()}</td>
                            <td class="text-center">${item.refund.formatVND()}</td>
                            <td class="text-center">${item.shipping_fee.formatVND()}</td>
                        </tr>`));
                    }
                    break;
                case 'delivery-unit':
                    $thead.append(createElementByText(
                        `<tr>
                            <th class="text-center">Thời gian</th>
                            <th class="text-center">Mã đơn</th>
                            <th class="text-center">Shop</th>
                            <th class="text-center">Người nhận</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Giá trị đơn hàng</th>
                            <th class="text-center">Phí v/c</th>
                            <th class="text-center">Ghi chú</th>
                        </tr>
                        `)
                    );
                    if (items.length === 0) $tbody.append(createElementByText(genNoContentTable('8', 'Không có dữ liệu')));
                    for (const item of items) {
                        $tbody.append(createElementByText(
                            `<tr>
                            <td class="text-center">${item.created_at}</td>
                            <td class="text-center">${item.code}</td>
                            <td class="text-center">${item.shop_name}</td>
                            <td class="text-center">${item.buyer_name}</td>
                            <td class="text-center">${item.status}</td>
                            <td class="text-center">${item.total_money.formatVND()}</td>
                            <td class="text-center">${item.fee.formatVND()}</td>
                            <td class="text-center">${item.note ?? ''}</td>
                        </tr>`));
                    }
                    break;
                case 'payment-debts':
                    $thead.append(createElementByText(
                        `<tr>
                            <th class="text-center">Thời gian</th>
                            <th class="text-center">Mã đơn</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Phương thức TT</th>
                            <th class="text-center">Giá trị đơn hàng</th>
                            <th class="text-center">Phí thanh toán</th>
                            <th class="text-center">Ghi chú</th>
                        </tr>
                        `)
                    );
                    if (items.length === 0) $tbody.append(createElementByText(genNoContentTable('7', 'Không có dữ liệu')));
                    for (const item of items) {
                        $tbody.append(createElementByText(
                            `<tr>
                            <td class="text-center">${item.created_at}</td>
                            <td class="text-center">${item.code}</td>
                            <td class="text-center">${item.status}</td>
                            <td class="text-center">${item.payment_method_name}</td>
                            <td class="text-center">${item.total_money.formatVND()}</td>
                            <td class="text-center">${item.fee_service.formatVND()}</td>
                            <td class="text-center">${item.note ?? ''}</td>
                        </tr>`));
                    }
                    break;
                default:
                    return 0;
            }
            $('.debt-container').empty().append(tableElement);
        }

    </script>
@endsection
