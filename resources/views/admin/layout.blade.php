<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Trang quản lý admin</title>
    <!-- Favicon -->
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('backend/images/Logo.png')}}"/>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link type="text/css" rel="stylesheet" href="{{asset('backend/dist/vendor.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('backend/dist/custom.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('backend/dist/app.css')}}"/>
    <link rel="manifest" href="{{asset('manifest.json')}}"/>
</head>

<body>
<div id="confirm-container">

</div>
<!-- loader Start -->
<div id="loading">
    <div id="loading-center">

    </div>
</div>
<!-- loader END -->
<!-- Wrapper Start -->
<div class="wrapper">
    <!-- Sidebar  -->
    <div class="iq-sidebar">
        <div class="iq-sidebar-logo d-flex justify-content-between">
            <a href="">
                <img src="{{asset('backend/images/Logo.png')}}" class="img-fluid" alt="">
                <span>Admin</span>
            </a>
            <div class="iq-menu-bt align-self-center">
                <div class="wrapper-menu bisto-bar">
                    <div class="main-circle">
                        <i class="fa fa-chevron-left"></i>
                    </div>
                    <div class="hover-circle"><i class="fa fa-chevron-right"></i></div>
                    <span class="fa fa-bars" aria-hidden="true" style="display: none"></span>
                </div>
            </div>
        </div>
        <div id="sidebar-scrollbar">
            <nav class="iq-sidebar-menu">
                <ul id="iq-sidebar-toggle" class="iq-menu">
                    <li>
                        <a href="{{URL::to('admin/dashboard')}}" class="iq-waves-effect"><i
                                class="fas fa-chart-bar"></i><span style="font-weight: bold;font-size: 20px">Tổng
                                    quan</span></a>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="true"><i
                                class="fas fa-blog"></i><span>Blog</span></a>
                        <ul id="menu-design1" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">
                            <li><a href="{{URL::to('admin/marketing-page')}}"><i class="fas fa-list-ol"></i>Quản lý
                                    ảnh quảng cáo</a></li>
                            <li><a href="{{URL::to('admin/blog-page')}}"><i class="fas fa-list-ol"></i>Quản lý
                                    blog</a></li>
                            <li><a href="{{URL::to('admin/blog-category-page')}}"><i class="fas fa-list-ol"></i>Quản lý
                                    danh mục blog</a></li>
                            <li><a href="{{URL::to('admin/tag-page')}}"><i class="fas fa-list-ol"></i>Quản lý
                                    tag</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="true"><i
                                class="fa fa-newspaper-o"></i><span>Bisto Feed</span></a>
                        <ul id="menu-design" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">
                            <li><a href="{{URL::to('admin/post-page')}}"><i class="fas fa-list-ol"></i>Danh sách bài
                                    đăng</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="true"><i
                                class="fa fa-book"></i><span>Đơn hàng</span></a>
                        <ul id="menu-design1" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">
                            <li><a href="{{URL::to('admin/manage-order')}}"><i class="fas fa-list-ol"></i>Quản lý
                                    đơn hàng</a></li>
                            <li><a href="{{URL::to('admin/debt-page')}}"><i class="fas fa-list-ol"></i>Đối soát công nợ</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" aria-expanded="true"><i
                                class="far fa-tshirt"></i><span>Sản phẩm</span></a>
                        <ul id="menu-design2" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">
                            <li><a href="{{URL::to('admin/product-page')}}"><i class="fas fa-list-ol"></i>Danh sách sản
                                    phẩm</a></li>
                            <li><a href="{{URL::to('admin/all-category')}}"><i class="fas fa-list-ol"></i>Danh mục sản
                                    phẩm</a></li>
                            <li><a href="{{URL::to('admin/stylish-page')}}"><i
                                        class="fas fa-list-ol"></i>Phong cách</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="true"><i
                                class="fa fa-book"></i><span>Quản lý Marketing</span></a>
                        <ul id="menu-design" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">
                            <li><a href="{{URL::to('admin/manage-banner')}}"><i class="fas fa-list-ol"></i>Banner admin</a>
                            </li>
                            <li><a href="{{URL::to('admin/manage-banner-admin-shop')}}"><i class="fas fa-list-ol"></i>Banner
                                    chuỗi cửa hàng</a></li>
                            <li><a href="{{URL::to('admin/manage-banner-shop')}}"><i class="fas fa-list-ol"></i>Banner
                                    cửa hàng</a></li>
                            <li><a href="{{URL::to('admin/service-banner')}}"><i class="fas fa-list-ol"></i>Dịch vụ
                                    banner</a></li>
                            <li><a href="{{URL::to('admin/service-brand')}}"><i class="fas fa-list-ol"></i>Dịch vụ brand</a>
                            </li>
                            <li><a href="{{URL::to('admin/product-service-page')}}"><i
                                        class="fas fa-list-ol"></i>Dịch vụ đẩy sản phẩm lên top</a>
                        </ul>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="true"><i
                                class="fa fa-tag"></i><span>Mã giảm giá</span></a>
                        <ul id="menu-design3" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">
                            <li><a href="{{URL::to('admin/list-coupon')}}"><i class="fas fa-list-ol"></i>Mã giảm giá hệ
                                    thống</a></li>
                            <li><a href="{{URL::to('admin/list-promotion')}}"><i class="fas fa-list-ol"></i>Mã giảm giá
                                    cửa hàng</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="true"><i
                                class="far fa-bolt"></i><span>Flashsale</span></a>
                        <ul id="menu-design3" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">
                            <li><a href="{{URL::to('admin/list-flashsale')}}"><i class="fas fa-list-ol"></i>Danh sách
                                    Flashsale</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="true"><i
                                class="fas fa-store"></i><span>Cửa hàng</span></a>
                        <ul id="menu-design4" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">
                            <li><a href="{{URL::to('admin/list-shop-without-branch')}}"><i class="fas fa-list-ol"></i>Cửa
                                    hàng bán lẻ</a></li>
                            <li><a href="{{URL::to('admin/list-shop')}}"><i class="fas fa-list-ol"></i>Cửa
                                    hàng thuộc chuỗi</a></li>
                            <li><a href="{{URL::to('admin/list-admin-shop')}}"><i class="fas fa-list-ol"></i>Chuỗi cửa
                                    hàng</a></li>
                            <li><a href="{{URL::to('admin/list-users')}}"><i class="fas fa-list-ol"></i>Khách hàng</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" aria-expanded="true"><i
                                class="fab fa-facebook-messenger"></i><span>Tin nhắn</span></a>
                        <ul id="menu-design3" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">
                            <li><a href="{{URL::to('admin/chats')}}"><i class="fas fa-list-ol"></i>Quản lý tin
                                    nhắn</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="true"><i
                                class="fas fa-user-cog"></i><span>Quản lý ADMIN</span></a>
                        <ul id="menu-design6" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">
                            <li><a href="{{URL::to('admin/list-admin')}}"><i class="fas fa-list-ol"></i>Danh sách
                                    Admin</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="true"><i
                                class="fas fa-shipping-fast"></i><span>Vận chuyển hàng</span></a>
                        <ul id="menu-design7" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">
                            <li><a href="{{URL::to('admin/list-shipping-order')}}"><i
                                        class="fas fa-list-ol"></i>Danh sách vận chuyển</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="true"><i
                                class="fas fa-id-card"></i><span>Giấy phép kinh doanh</span></a>
                        <ul id="menu-design7" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">
                            <li><a href="{{URL::to('admin/license-shop-page')}}"><i
                                        class="fas fa-list-ol"></i>Liêt kê giấy phép kinh doanh</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="true"><i
                                class="fas fa-list"></i><span>Dịch vụ</span></a>
                        <ul id="menu-design7" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">

                            <li><a href="{{URL::to('admin/fee-payment-page')}}"><i
                                        class="fas fa-list-ol"></i>Phí thanh toán</a>
                            <li><a href="{{URL::to('admin/fee-app-page')}}"><i
                                        class="fas fa-list-ol"></i>Phí người bán</a></li>
                            <li><a href="{{URL::to('admin/service-system-page')}}"><i
                                        class="fas fa-list-ol"></i>Phí người mua</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" aria-expanded="true"><i
                                class="fas fa-money-bill-alt"></i><span>Thanh toán</span></a>
                        <ul id="menu-design5" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">
                            <li><a href="{{URL::to('admin/payment')}}"><i class="fas fa-list-ol"></i>Thông tin
                                    thanh toán</a>
                            </li>
                            <li><a href="{{URL::to('admin/payment-history-page')}}"><i class="fas fa-list-ol"></i>Lịch
                                    sử thanh toán</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="true"><i
                                class="fas fa-bell"></i><span>Thông báo hệ thống</span></a>
                        <ul id="menu-design7" class="iq-submenu collapse show" data-parent="#iq-sidebar-toggle">
                            <li><a href="{{URL::to('admin/send-notification-page')}}"><i
                                        class="fas fa-list-ol"></i>Gửi thông báo</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <div class="p-3"></div>
        </div>
    </div>
    <!-- TOP Nav Bar -->
    <div class="iq-top-navbar">
        <div class="iq-navbar-custom">
            <div class="iq-sidebar-logo">
                <div class="top-logo">
                    <a href="{{URL::to('admin/dashboard')}}" class="logo">
                        <img src="" class="img-fluid" alt="">
                        <span>BISTO</span>
                    </a>
                </div>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light p-0">
                <div class="navbar-left">
                    <ul id="topbar-data-icon" class="d-flex p-0 topbar-menu-icon">
                        <li class="nav-item">
                            <a href="{{URL::to('admin/dashboard')}}"
                               class="nav-link font-weight-bold search-box-toggle" title="Tổng quan"><i
                                    class="fas fa-chart-bar fa-lg"></i></a>
                        </li>
                        <li><a href="{{URL::to('admin/manage-order')}}" class="nav-link" title="Quản lý đơn hàng"><i
                                    class="fa fa-book fa-lg"></i></a></li>
                        <li><a href="{{URL::to('admin/product-page')}}" class="nav-link" title="Quản lý sản phẩm"><i
                                    class="far fa-tshirt fa-lg"></i></a></li>
                        <li><a href="{{URL::to('admin/manage-banner')}}" class="nav-link"
                               title="Quản lý banner"><i class="fa fa-list-ul fa-lg"></i></a></li>
                        <li><a href="{{URL::to('admin/list-coupon')}}" class="nav-link"
                               title="Quản lý mã giảm giá"><i class="fa fa-tag fa-lg"></i></a></li>
                        <li><a href="{{URL::to('admin/chats')}}" class="nav-link" title="Quản lý tin nhắn"><i
                                    class="fab fa-facebook-messenger fa-lg"></i></a></li>
                        <li><a href="{{URL::to('admin/list-shop-without-branch')}}" class="nav-link"
                               title="Quản lý shop"><i
                                    class="fas fa-user-cog fa-lg"></i></a></li>
                        <li><a href="{{URL::to('admin/list-shipping-order')}}" class="nav-link"
                               title="Quản lý vận chuyển"><i class="fas fa-shipping-fast fa-lg"></i></a></li>
                    </ul>
                    <div class="iq-search-bar">
                        <form action="#" class="searchbox">
                            <input type="text" class="text search-input" placeholder="Tìm kiếm...">
                            <a class="search-link" href="#"><i class="fas fa-search"></i></a>
                            <div class="searchbox-datalink">
                                <h6 class="pl-3 pt-3">Pages</h6>
                                <ul class="m-0 pl-3 pr-3 pb-3">
                                    <li class="iq-bg-primary-hover rounded"><a href="{{URL::to('admin/dashboard')}}"
                                                                               class="nav-link router-link-exact-active router-link-active pr-2"><i
                                                class="fas fa-chart-bar"></i> Tổng quan</a></li>
                                    <li class="iq-bg-primary-hover rounded"><a
                                            href="{{URL::to('admin/manage-order')}}" class="nav-link"><i
                                                class="fa fa-book"></i> Quản lý đơn hàng</a></li>
                                    <li class="iq-bg-primary-hover rounded"><a
                                            href="{{URL::to('admin/product-page')}}" class="nav-link"><i
                                                class="far fa-tshirt fa-lg"></i> Quản lý sản phẩm</a></li>
                                    <li class="iq-bg-primary-hover rounded"><a
                                            href="{{URL::to('admin/all-category')}}" class="nav-link"><i
                                                class="fa fa-list-ul"></i> Quản lý danh mục sản phẩm</a></li>
                                    <li class="iq-bg-primary-hover rounded"><a
                                            href="{{URL::to('admin/list-coupon')}}" class="nav-link"><i
                                                class="fa fa-tag"></i> Quản lý mã giảm giá</a></li>
                                    <li class="iq-bg-primary-hover rounded"><a
                                            href="{{URL::to('admin/list-flashsale')}}" class="nav-link"><i
                                                class="far fa-bolt"></i> Quản lý Flash Sale</a></li>
                                    <li class="iq-bg-primary-hover rounded"><a href="{{URL::to('admin/list-shop')}}"
                                                                               class="nav-link"><i
                                                class="fas fa-store"></i> Quản lý cửa hàng</a></li>
                                    <li class="iq-bg-primary-hover rounded"><a
                                            href="{{URL::to('admin/list-admin-shop')}}" class="nav-link"><i
                                                class="fas fa-store"></i> Quản lý chuỗi cửa hàng</a></li>
                                    <li class="iq-bg-primary-hover rounded"><a href="{{URL::to('admin/chats')}}"
                                                                               class="nav-link router-link-exact-active router-link-active pr-2"><i
                                                class="fab fa-facebook-messenger"></i> Quản lý tin nhắn</a></li>
                                    <li class="iq-bg-primary-hover rounded"><a
                                            href="{{URL::to('admin/list-admin')}}" class="nav-link"><i
                                                class="fas fa-user-cog"></i> Danh sách Admin</a></li>
                                    <li class="iq-bg-primary-hover rounded"><a
                                            href="{{URL::to('admin/list-shipping-order')}}" class="nav-link"><i
                                                class="fas fa-shipping-fast"></i> Quản lý vận chuyển</a></li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-label="Toggle navigation">
                    <i class="ri-menu-3-line"></i>
                </button>
                <div class="iq-menu-bt align-self-center">
                    <div class="wrapper-menu">
                        <div class="hover-circle"><i class="ri-arrow-left-s-line"></i></div>
                        <div class="main-circle"><i class="ri-arrow-right-s-line"></i></div>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-list">
                        <li class="nav-item">
                            <a href="#" class="search-toggle iq-waves-effect">
                                <div id="lottie-beil"></div>
                                <span class="bg-danger dots"></span>
                            </a>
                            <div class="iq-sub-dropdown">
                                <div class="iq-card shadow-none m-0">
                                    <div class="iq-card-body p-0 ">
                                        <div class="bg-primary p-3">
                                            <h5 class="mb-0 text-white">Tất cả thông báo<small
                                                    class="badge  badge-light float-right pt-1">0</small></h5>
                                        </div>
                                    <!-- <a href="#" class="iq-sub-card">
                                            <div class="media align-items-center">
                                                <div class="">
                                                    <img class="avatar-40 rounded"
                                                         src="{{asset('backend/images/user/01.jpg')}}" alt="">
                                                </div>
                                                <div class="media-body ml-3">
                                                    <h6 class="mb-0 ">Có bình luận mới</h6>
                                                    <small class="float-right font-size-12">Hiện tại</small>
                                                    <p class="mb-0">Nguyễn Văn A</p>
                                                </div>
                                            </div>
                                        </a> -->
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="search-toggle iq-waves-effect">
                                <div id="lottie-mail"></div>
                                <span class="count-mess-css" id="count-message"></span>
                            </a>
                            <div class="iq-sub-dropdown">
                                <div class="iq-card shadow-none m-0">
                                    <div class="iq-card-body p-0 ">
                                        <div class="bg-primary p-3">
                                            <h5 class="mb-0 text-white">Tất cả tin nhắn
                                                <!-- <small class="badge count-message badge-light float-right pt-1">0</small>--></h5>
                                        </div>
                                        <div id="notif-message-container">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="navbar-list">
                    <li>
                        <a href="#"
                           class="search-toggle iq-waves-effect d-flex align-items-center bg-primary rounded">
                            <img src="{{$source_avatar}}" id="icon-ava" class="img-fluid rounded mr-3" alt="user">
                            <div class="caption">
                                <h6 class="mb-0 line-height text-white">
                                    <?php
                                    $name = Session()->get('admin_name');
                                    if ($name) {
                                        echo $name;
                                    }
                                    ?>
                                </h6>
                                <span class="font-size-12 text-white">Đang trực tuyến</span>
                            </div>
                        </a>
                        <div class="iq-sub-dropdown iq-user-dropdown">
                            <div class="iq-card shadow-none m-0">
                                <div class="iq-card-body p-0 ">
                                    <div class="bg-primary p-3">
                                        <h5 class="mb-0 text-white line-height">
                                            <?php
                                            $name = Session()->get('admin_name');
                                            if ($name) {
                                                echo $name;
                                            }
                                            ?>
                                        </h5>
                                        <span class="text-white font-size-12">Đang trực tuyến</span>
                                    </div>
                                    <a href="{{URL::to('admin/profile-page')}}" class="iq-sub-card iq-bg-primary-hover">
                                        <div class="media align-items-center">
                                            <div class="rounded iq-card-icon iq-bg-primary">
                                                <i class="fas fa-id-badge"></i>
                                            </div>
                                            <div class="media-body ml-3">
                                                <h6 class="mb-0 ">Hồ sơ</h6>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="{{URL::to('admin/password-page')}}"
                                       class="iq-sub-card iq-bg-primary-hover">
                                        <div class="media align-items-center">
                                            <div class="rounded iq-card-icon iq-bg-primary">
                                                <i class="fas fa-lock"></i>
                                            </div>
                                            <div class="media-body ml-3">
                                                <h6 class="mb-0 ">Đổi mật khẩu</h6>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="{{URL::to('admin/address-page')}}"
                                       class="iq-sub-card iq-bg-primary-hover">
                                        <div class="media align-items-center">
                                            <div class="rounded iq-card-icon iq-bg-primary">
                                                <i class="fas fa-map-marker-alt"></i>
                                            </div>
                                            <div class="media-body ml-3">
                                                <h6 class="mb-0 ">Địa chỉ</h6>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="d-inline-block w-100 text-center p-3">
                                        <a class="bg-primary iq-sign-btn" href="{{URL::to('/logout')}}"
                                           role="button">Đăng xuất <i class="far fa-sign-out-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <!-- Page Content  -->
</div>
<!-- TOP Nav Bar END -->

<!-- Wrapper END -->
<!-- Footer -->
<!-- Footer END -->
<script src="{{asset('backend/dist/vendor.js')}}"></script>
<!-- constant JavaScript -->
<script src="{{asset('backend/js2/constant.js')}}"></script>
<!-- helper JavaScript -->
<script src="{{asset('backend/js2/helper.js')}}"></script>
<script src="{{asset('backend/js2/validation.js')}}"></script>
<!-- UTIL JavaScript -->
<script src="{{asset('backend/js2/util.js')}}"></script>
<!-- upload/delete image product -->
<script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-storage.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.2.9/firebase-analytics.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.6.2/firebase-messaging.js"></script>
<script type="text/javascript" src="{{asset('backend/js2/uploadImage.js')}}"></script>
<script type="text/javascript" src="{{asset('backend/js2/message.js')}}"></script>
<script type="text/javascript" src="{{asset('backend/ckeditor5/build/ckeditor.js')}}"></script>
<script type="text/javascript" src="{{asset('backend/ckeditor5/index.js')}}"></script>
{{--<script type="text/javascript" src="{{asset('backend/js2/layout.js')}}"></script>--}}
@yield('admin_content')
<script>
    const p = '{{url('/')}}'
    PREFIX_API = p;
    PREFIX_API_ADMIN = `${p}/admin`;
    PREFIX_API_ADMIN_SHOP = `${p}/admin-shop`;
    PREFIX_API_SHOP = `${p}/shop`;
    USER_ID = '{{$_user_id_}}';
    _TOKEN = '{{csrf_token()}}';
</script>
</body>

</html>
