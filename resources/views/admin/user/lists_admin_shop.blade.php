@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách chuỗi cửa hàng
                </div>

                <div class="row row-action-bisto-css">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-3 d-flex justify-content-between">
                        <div class="col-sm-6">
                            <a type="button" class="w-100 btn btn-outline-primary btn-bisto-height-css"
                               href="{{url('/admin/lists/admin-shops/export-pdf')}}">
                                <span class="fas fa-file-pdf fa-lg"></span>
                                Xuất PDF
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a type="button" class="btn btn-outline-success w-100 btn-bisto-height-css"
                               href="{{url('/admin/lists/admin-shops/export-excel')}}">
                                <span class="fas fa-file-excel fa-lg"></span>
                                Xuất excel
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search" placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm form-control btn-search-bisto-css"
                                   name="search-user">
                        </div>
                    </div>
                </div>

                <div class="iq-card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Tên chuỗi cửa hàng</th>
                            <th class="text-center">Số Bcoin</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Số điện thoại</th>
                            <th class="text-center">Địa chỉ</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
    </div>
    <script type="text/javascript">
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: 6}}, pagination;
        $(document).ready(function () {
            loadListAdminShops(dt);
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadListAdminShops(dt);
                }
            });
        });


        function generateAdminShops(users) {
            let tbody = ''
            for (let i = 0; i < users.length; i++) {
                const amount = users[i]?.amount ?? 0;
                tbody += `<tr>
                        <td class="text-center">${i + 1}</td>
                        <td class="text-center">${users[i].shop_name}</td>
                        <td class="text-center">
                            <span>${amount.formatFullStop()}
                                <span class="fa fa-btc" aria-hidden="true"></span>
                            </span>
                        </td>
                        <td class="text-center">${users[i].email}</td>
                        <td class="text-center">${users[i].phone}</td>
                        <td class="text-center">${users[i].address || ''}</td>
                    </tr>`;
            }
            $('.table-striped>tbody').empty().append(tbody);
        }

        function loadListAdminShops({currentPage = 1, keyword = '', loading = {type: 'table', colspan: 6}}) {
            const _url = `lists/admin-shops?page=${currentPage}&keyword=${keyword}`;
            $.ajax({
                url: _url,
                method: 'GET',
                loading,
            })
                .done(res => {
                    const users = res?.users?.data ?? [];
                    if (users.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadListAdminShops(dt);
                    } else if (users.length === 0) {
                        $('.table-striped>tbody').empty().append(genNoContentTable(6, 'Không có chuỗi cửa hàng nào'));
                    } else {
                        generateAdminShops(users);
                        pagination.setTotalItems(res.users.total);
                    }
                })
                .fail(err => {
                    toastError(err, 'Load danh sách admin-shop thất bại');
                });
        }

        $(document).on('keydown', 'input[name=search-user]', function () {
            if (event.key === 'Enter') {
                dt.keyword = $(this).val();
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadListAdminShops(dt);
            }
        })
    </script>
@endsection
