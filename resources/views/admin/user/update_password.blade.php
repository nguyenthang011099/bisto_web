@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title">
                        <h4 class="card-title">Đổi mật khẩu</h4>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="form-group">
                        <label for="current_password" class="required-input">Mật khẩu hiện tại</label>
                        <a href="javascripe:void();" class="float-right">Forgot Password</a>
                        <input required placeholder="Nhập mật khẩu" type="password" name="current_password"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="new_password" class="required-input">Mật khẩu mới (6 chữ số)</label>
                        <input required type="password" name="new_password" class="form-control"
                               placeholder="Mật khẩu chỉ chứa 6 chữ số">
                    </div>
                    <div class="form-group">
                        <label for="confirm_password" class="required-input">Xác nhận mật khẩu</label>
                        <input required type="password" name="confirm_password" class="form-control"
                               placeholder="Mật khẩu chỉ chứa 6 chữ số">
                    </div>
                    <button onclick="check_submit_form()" type="button" class="btn btn-primary mr-2">Lưu thay
                        đổi
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script>
        function check_submit_form() {
            let current_password = $("[name='current_password']");
            let new_password = $("[name='new_password']");
            let confirm_password = $("[name='confirm_password']");
            let regex = '^\\d{6,6}$';

            if (confirm_password.val() !== new_password.val()) {
                toastErrorMessage('Mật khẩu chưa trùng nhau');
            } else if (!new RegExp(regex).test(new_password.val())) {
                toastErrorMessage('Mật khẩu chỉ chứa 6 chữ số');
            } else if (current_password.val() === new_password.val()) {
                toastErrorMessage('Mật khẩu mới không được trùng mật khẩu cũ');
            } else {
                submit();
            }
        }

        function submit() {
            $.ajax({
                url: "{{url('admin/passwords')}}",
                method: "post",
                data: {
                    _token: "{{csrf_token()}}",
                    current_password: $("input[name=current_password]").val(),
                    new_password: $("input[name=new_password]").val(),
                    confirm_password: $("input[name=confirm_password]").val()
                },
                success: function (obj, textStatus, xhr) {
                    toastSuccessMessage(obj.message);
                    setTimeout(() => {
                        window.location.replace('{{url("/admin/dashboard")}}');
                    }, 500);

                },
                fail: function (obj, err) {
                    toastErrorMessage("thay đổi mật khẩu thất bại, hãy thử lại");
                }
            })
        }

    </script>
    <?php
    if (session('change_password')) {
        $message = session('change_password');
        session()->remove('change_password');
        echo "<script type='text/javascript'>alert('$message');</script>";
    }?>
@endsection
