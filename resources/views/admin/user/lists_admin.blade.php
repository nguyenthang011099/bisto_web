@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách người quản lí Bisto
                </div>
                <div class="row w3-res-tb row-action-bisto-css">
                    <div class="col-sm-5">
                        <button class="btn btn-show-add-bisto-css btn-primary btn-add-bisto" data-toggle="modal"
                                data-target=".add-admin">+
                            Thêm ADMIN
                        </button>
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search" placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm form-control btn-search-bisto-css"
                                   name="keywords_submit">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Tên quản lí</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Số điện thoại</th>
                            <th class="text-center">Địa chỉ</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <footer class="panel-footer">
                    <!-- PAGINATION START-->
                    <div class="pagination-custom">
                        <div id="pagination-container"></div>
                    </div>
                    <!-- PAGINATION END-->
                </footer>
            </div>
        </div>
    </div>
    <!-- ADD ADMIN -->
    <div class="modal fade add-admin" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content" style="border-radius: 20px">
                <div class="iq-card">
                    <section class="panel">
                        <header class="panel-heading">
                            Thêm người quản lí
                        </header>
                        <div class="iq-card-body">
                            <div class="position-center">
                                <div id="form-admin">
                                    <div class="form-group">
                                        <label for="admin_username" class="required-input">Tên tài khoản</label>
                                        <input required type="text" name="username" class="form-control"
                                               placeholder="Tên tài khoản">
                                        <div class="mes-username mes-v-display"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="admin_name" class="required-input">Tên người quản lí</label>
                                        <input required type="text" name="full_name" class="form-control" id="name"
                                               placeholder="Tên người quản lí">
                                        <div class="mes-full_name mes-v-display"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="admin_email" class="required-input">Email</label>
                                        <input required type="text" name="email" class="form-control" id="email"
                                               placeholder="Email">
                                        <div class="mes-email mes-v-display"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="admin_phone" class="required-input">Số điện thoại</label>
                                        <input required type="text" name="phone_number" class="form-control" id="phone"
                                               placeholder="Số điện thoại">
                                        <div class="mes-phone_number mes-v-display"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="required-input">Mật khẩu</label>
                                        <input required type="password" name="password" class="form-control"
                                               id="password" placeholder="Mật khẩu">
                                        <div class="mes-password mes-v-display"></div>
                                    </div>
                                    <button
                                        class="btn-save-admin btn btn-primary">Thêm admin
                                    </button>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <!-- END ADD ADMIN -->
    <script>
        let dt = {currentPage: 1, keyword: '', loading: {colspan: 5, type: 'table'}}
        let pagination;
        $(document).ready(function () {
            loadListAdmins(dt)
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadListAdmins(dt);
                },
            });
        })

        function generateAdmins(users) {
            let tbody = ''
            for (let i = 0; i < users.length; i++) {
                tbody += `<tr>
                        <td class="text-center">${i + 1}</td>
                        <td class="text-center">${users[i].fullname}</td>
                        <td class="text-center">${users[i].email}</td>
                        <td class="text-center">${users[i].phone}</td>
                        <td class="text-center">${users[i].address}</td>
                    </tr>`;
            }
            $('.table-striped>tbody').empty().append(tbody);
        }

        function loadListAdmins({currentPage = 1, keyword = '', loading = {colspan: 5, type: 'table'}}) {
            const _url = `lists/admins?page=${currentPage}&keyword=${keyword}`;
            $.ajax({
                url: _url,
                method: 'GET',
                loading
            })
                .done(res => {
                    const users = res?.users?.data ?? [];
                    if (users.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadListAdmins(dt);
                    } else if (users.length === 0) {
                        $('.table-striped>tbody').empty().append(genNoContentTable(5, `Không có quản lý nào`));
                    } else {
                        generateAdmins(users);
                        pagination.setTotalItems(res.users.total);
                    }
                })
                .fail(err => {
                });
        }

        $(document).on('keydown', 'input[name=keywords_submit]', function () {
            if (event.key === 'Enter') {
                dt.keyword = $(this).val();
                loadListAdmins(dt);
            }
        })

        function validateFormAdmin() {
            return validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên tài khoản là bắt buộc'
                    }],
                    propertyName: 'username'
                }) && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên người quản lý là bắt buộc'
                    }],
                    propertyName: 'full_name'
                }) && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Email là bắt buộc'
                    }, {
                        func: emailValidation,
                        mes: 'Email không hợp lệ'
                    }],
                    propertyName: 'email'
                }) && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Số điện thoại là bắt buộc'
                    }, {
                        func: phoneValidation,
                        mes: 'Số điện thoại không hợp lệ'
                    }],
                    propertyName: 'phone_number'
                })
                && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Mật khẩu là bắt buộc'
                    }, {
                        func: passwordValidation,
                        mes: 'Mật khẩu chỉ bao gồm đúng 6 chữ số'
                    }],
                    propertyName: 'password'
                })
        }

        $(document).on('click', '.btn-save-admin', function () {
            if (validateFormAdmin()) {
                const admin = formToData('#form-admin');
                $.ajax({
                    url: 'save-admin',
                    method: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        ...admin
                    }
                }).done(res => {
                    $('.modal').modal('hide');
                    loadListAdmins({...dt, loading: {type: 'content'}});
                    toastSuccess(res, 'Thêm quản lý mới thành công')
                }).fail(err => {
                    $('.modal').modal('hide');
                })
            }
        })
    </script>

@endsection
