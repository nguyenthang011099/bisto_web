@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title">
                        <h4 class="card-title">Thông tin địa chỉ ADMIN</h4>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="row align-items-center">
                        <div class="form-group col-sm-12">
                            <label for="province">Tỉnh/Thành phố<span class="text-danger">*</span></label>
                            <select required class="form-control" id="province" name="province"
                                    data-live-search="true">
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="district">Quận/huyện<span class="text-danger">*</span></label>
                            <select required class="form-control"
                                    id="district" name="district" data-live-search="true">
                                <option selected disabled hidden value="">-- Chọn --</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="ward">Phường/xã<span class="text-danger">*</span></label>
                            <select required class="form-control" id="ward"
                                    name="ward" data-live-search="true">
                                <option selected disabled hidden value="">-- Chọn --</option>
                            </select>
                        </div>

                        <div class="form-group col-sm-12">
                            <div class="validate-pdw mes-v-display">
                                Bạn cần chọn đầy đủ địa chỉ bao gồm Tỉnh/Thành phố - Quận/Huyện - Phường/Xã
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="detail-address" class="required-input">Địa chỉ chi tiết</label>
                            <textarea class="form-control" id="detail-address"
                                      name="detail-address" required></textarea>
                            <div class="mes-v-display mes-detail-address">

                            </div>
                        </div>
                    </div>
                    <button id="btn-save-address" class="btn btn-primary mr-2">Lưu địa chỉ
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script>
        let selecterAddress;

        $(document).ready(function () {
            //define module multiple select
            selecterAddress = multiSelect({
                listSelect: [{
                    selectSelector: 'select[name=province]',
                    url: '/provinces', valueName: 'id', showName: 'name'
                }, {
                    selectSelector: 'select[name=district]',
                    url: '/districts?provinceId=', valueName: 'id', showName: 'name'
                }, {
                    selectSelector: 'select[name=ward]',
                    url: '/wards?districtId=',
                    valueName: 'id',
                    showName: 'name'
                }], cbWhenSelectLastOption: () => {
                    $('#btn-save-address').prop('disabled', false);
                }
            })
            //selecterAddress.loadRootSelect()
            loadAddress();
        })

        //load address
        function loadAddress() {
            $.ajax({
                url: 'addresses',
                loading: {type: 'content'},
                method: 'GET'
            }).done(res => {
                const address = res?.address;
                if (isEmptyValue(address)) {
                    return selecterAddress.loadRootSelect();
                }
                $('textarea[name=detail-address]').val(address.address || '')
                selecterAddress.loadSelects([address.city_id, address.district_id, address.ward_id], function () {
                    $('#btn-save-address').prop('disabled', true);
                });
            }).fail(err => {
            })
        }

        //when user typing in address detail, btn save is enable
        $('textarea[name=detail-address]').change(function () {
            $('#btn-save-address').prop('disabled', false);
        })

        //when user press btn save
        $('#btn-save-address').on('click', function () {
            //validate user need select all select
            if (!selecterAddress.isSelectedLastOption()) {
                $('.validate-pdw').show();
                return;
            }
            let obj = {};

            obj.address = $('textarea[name=detail-address]').val() ?? '';
            obj.province_id = selecterAddress.getMultiValueSelected()[0].value;
            obj.district_id = selecterAddress.getMultiValueSelected()[1].value;
            obj.ward_id = selecterAddress.getMultiValueSelected()[2].value;
            if (validateInput({
                listValidateFunction: [
                    {
                        func: requiredValidation,
                        mes: 'Địa chỉ chi tiết là bắt buộc',
                    }
                ], propertyName: 'detail-address', type: 'textarea'
            })) {
                $.ajax({
                    url: "{{url("/admin/addresses")}}",
                    method: 'POST',
                    loading: {type: 'content'},
                    data: {
                        _token: "{{csrf_token()}}",
                        ...obj
                    },
                }).done(res => {
                    toastSuccess(res, 'Cập nhật địa chỉ thành công');
                    loadAddress();
                    $('.validate-pdw').hide();
                }).fail(err => {
                });
            }
        })
    </script>

@endsection
