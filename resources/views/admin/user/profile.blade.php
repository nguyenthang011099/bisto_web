@extends('admin.layout')
@section('admin_content')
    <?php
    $role_text = ['Tài khoản chưa được kích hoạt', 'Người dùng', 'Quản trị shop', 'Quản trị tổng shop', 'Quản trị viên'];
    ?>
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="iq-card-body">
                    <div class="iq-card-body profile-page p-0">
                        <div class="profile-header">
                            <div class="row" style="margin-bottom: 3%;">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-6">
                                    <!--left col-->
                                    {{--cover--}}
                                    <div class="text-center" style="position: relative;">
                                        <img class="img-fluid cover-show" src="" alt="bisto-cover"
                                             id="image-cover">
                                        <label
                                            style="cursor: pointer; color: orange; position: absolute; right: 5%; transform: translateY(-50%)  translateX(-50%); bottom: 0%"
                                            for="cover-img-input"><i class="fas fa-camera"></i></label>
                                        <input id="cover-img-input" type="file"
                                               class="text-center center-block file-upload"
                                               style="cursor: pointer; display: none;">
                                    </div>

                                    {{--avatar--}}
                                    <div class="text-center" id="avatar-admin"
                                         style="z-index: 10; position: absolute;width: 25%;left: 50%;transform: translateY(-85%)  translateX(-50%)">
                                        <img id="image-ava" class="avatar-show img-circle img-thumbnail" src=""
                                             alt="avatar">
                                        <label
                                            style="cursor: pointer; color: orange; position: absolute; left: 50%; transform: translateY(-50%)  translateX(-50%); bottom: 0%"
                                            for="avatar-img-input"><i class="fas fa-camera"></i></label>
                                        <input id="avatar-img-input" type="file"
                                               class="text-center center-block file-upload"
                                               style="cursor: pointer; display: none;">
                                    </div>
                                    <div class="row mt-4 mb-2">
                                        <div
                                            class="col-2"></div>
                                        <div class="col-8 d-flex justify-content-center">
                                            <h4 class="super-center">Bcoins hệ thống:&nbsp;</h4>
                                            <h4
                                                class="bcoins super-center">0</h4>&nbsp;<span
                                                class="fa fa-btc fa-lg super-center"
                                                aria-hidden="true"></span>
                                        </div>
                                        <div class="col-2">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="admin_phone">Số điện thoại</label>
                                                <input readonly type="text" class="form-control" name="admin_phone"
                                                       id="admin_phone"
                                                       placeholder="enter phone"
                                                       title="enter your phone number if any.">
                                                <div class="mes-admin_phone mes-v-visibility">
                                                    Số điện thoại là bắt buộc
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="full_name">Họ và tên</label>
                                                <input type="text" class="form-control" name="admin_name"
                                                       id="admin_name"
                                                       placeholder="enter your full name"
                                                       title="enter your name if any.">
                                                <div class="mes-admin_name mes-v-visibility">
                                                    Tên quản trị viên là bắt buộc
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="admin_role">Quyền</label>
                                                <input type="text" class="form-control" readonly
                                                       id="admin_role" name="admin_role"/>
                                                <div class="mes-v-visibility">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" class="form-control" name="admin_email"
                                                       id="admin_email"
                                                       title="enter your email if any."
                                                       placeholder="enter your email">
                                                <div class="mes-admin_email mes-v-visibility">
                                                    Email là bắt buộc
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <button onclick="onClickButtonSave()" id="btn-save"
                                                    class="btn btn-primary mr-2">Lưu
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-container"></div>
    <!--/row-->

    <script>
        let cropperAvatar;
        let cropperCover, _i;
        $(document).ready(function () {
            _i = defineImage({id: 'avatar'});

            loadProfile();
            cropperAvatar = defineCropAndUploadImage({inputSelector: '#avatar-img-input'})
            cropperCover = defineCropAndUploadImage({inputSelector: '#cover-img-input', type: 'cover'})

            cropperAvatar.saveButton.addEventListener('click', function () {
                $.ajax({
                    url: '{{url('/admin/upload-avatar')}}',
                    method: 'POST',
                    data: {
                        imageUrl: cropperAvatar.getImg(),
                        _token: '{{csrf_token()}}'
                    }
                }).done(res => {
                    cropperAvatar.$modal.modal('hide');
                    toastSuccess(res, 'Thay đổi avatar thành công');
                    loadProfile();
                }).fail(err => {
                    cropperAvatar.$modal.modal('hide');
                    toastError(err, 'Thay đổi avatar thất bại');
                })
            })

            cropperCover.saveButton.addEventListener('click', function () {
                $.ajax({
                    url: '{{url('/admin/upload-cover')}}',
                    method: 'POST',
                    data: {
                        imageUrl: cropperCover.getImg(),
                        _token: '{{csrf_token()}}'
                    }
                }).done(res => {
                    cropperCover.$modal.modal('hide');
                    toastSuccess(res, 'Thay đổi ảnh bìa thành công');
                    loadProfile();
                }).fail(err => {
                    cropperCover.$modal.modal('hide');
                    toastError(err, 'Thay đổi ảnh bìa thất bại')
                })
            })
        });


        $("select").each(function () {
            $(this).find('option[value="' + $(this).attr("value") + '"]').prop('selected', true);
        });

        function validateProfileForm() {
            return validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên quản trị viên là bắt buộc'
                    }],
                    propertyName: 'admin_name',
                    type: 'onsubmit', cssClassMes: 'visibility'
                }) &&
                validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Số điện thoại là bắt buộc'
                    }],
                    propertyName: 'admin_phone',
                    type: 'onsubmit', cssClassMes: 'visibility'
                }) &&
                validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Email là bắt buộc'
                    }, {
                        func: emailValidation,
                        mes: 'Email Không đúng định dạng'
                    }],
                    propertyName: 'admin_email',
                    type: 'onsubmit', cssClassMes: 'visibility'
                });


        }

        function getProfileObject() {
            let obj = {};
            obj.admin_id = $('input[name=admin_id]').val();
            obj.full_name = $('input[name=admin_name]').val();
            obj.email = $('input[name=admin_email]').val();
            obj.phone_number = $('input[name=admin_phone]').val();
            console.log(obj);
            return obj;
        }

        function loadProfile() {
            let role_text = ['Tài khoản chưa được kích hoạt', 'Người dùng', 'Quản trị shop', 'Quản trị chuỗi shop', 'Quản trị viên'];
            $('#btn-save').prop('disabled', true);
            $.ajax({
                url: "{{url('/admin/profiles')}}",
                method: "get",
                loading: {type: 'content'},
                success: function (obj, textStatus, xhr) {
                    // console.log(obj);
                    $('.bcoins').text(parseInt(obj.profile.total_bcoins).formatComma()).counterUp({
                        delay: 10,
                        time: 1000
                    });
                    if (obj.profile) {
                        $("#btn-save").prop('disabled', false);
                        $('input[name=admin_phone]').val(obj.profile.phone);
                        $('input[name=admin_email]').val(obj.profile.email);
                        $('input[name=admin_role]').val(role_text[obj.profile.role]);
                        $('input[name=admin_id]').val(obj.profile.id);
                        $('input[name=admin_name]').val(obj.profile.fullname);
                        setImage('#image-cover', obj?.profile?.cover_image, 'cover');
                        setImage('#image-ava', obj?.profile?.avatar_image, 'avatar');
                    }
                },
                fail: function (err) {
                    toastErrorMessage('có lỗi xảy ra');
                }
            });

        }

        function onClickButtonSave() {
            if (validateProfileForm()) {
                $.ajax({
                    url: "{{url('/admin/profiles')}}",
                    method: "POST",
                    data: {
                        _token: "{{csrf_token()}}",
                        ...getProfileObject(),
                    },
                    success: function (obj, textStatus, xhr) {
                        loadProfile();
                        toastSuccessMessage(obj.message);
                    }
                })
            }

        }

    </script>
@endsection
