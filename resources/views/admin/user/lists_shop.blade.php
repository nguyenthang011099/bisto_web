@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách cửa hàng trong chuỗi
                </div>
                <div class="row row-action-bisto-css">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-3 d-flex justify-content-between">
                        <div class="col-sm-6">
                            <a type="button" class="btn btn-outline-primary btn-bisto-height-css w-100"
                               href="{{url('/admin/lists/shops/export-pdf?is_without_branch=0')}}">
                                <span class="fas fa-file-pdf fa-lg"></span>
                                Xuất PDF
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a type="button" class="btn btn-outline-success btn-bisto-height-css w-100"
                               href="{{url('/admin/lists/shops/export-excel?is_without_branch=0')}}">
                                <span class="fas fa-file-excel fa-lg"></span>
                                Xuất excel
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search" placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm form-control btn-search-bisto-css"
                                   name="search-user">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Tên cửa hàng</th>
                            <th class="text-center">Số Bcoin</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Số điện thoại</th>
                            <th class="text-center">Địa chỉ</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <div class="modal-container">
            <div class="modal-bcoin modal fade bd-example-modal-sm" tabindex="-1" role="dialog"
                 aria-labelledby="mySmallModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Nhập số bcoin thay đổi<span class="text-danger">*</span></label>
                                <input type="number" name="amount" class="form-control"
                                       placeholder="Nhập số âm để giảm bcoin">
                                <div class="mes-amount mes-v-display">
                                    Hãy nhập số bcoin thay đổi
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button name="save-bcoin" class="btn btn-primary">
                                Lưu thay đổi
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: 8}}
        let $modalUpdateBcoin, pagination;
        $(document).ready(function () {
            $modalUpdateBcoin = $('.modal-bcoin');
            loadListShops(dt);
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadListShops(dt);
                }
            });
        });


        $(document).on('click', '.on-show-modal-bcoin', function () {
            $modalUpdateBcoin.find('input[name=amount]').val(0).css('border', '1px solid #d7dbda');
            $modalUpdateBcoin.find('.mes-amount').removeClass('mes-iv-display mes-v-display').addClass('mes-v-display');
            $modalUpdateBcoin.data('userid', $(this).attr('userid'));
            $modalUpdateBcoin.data('amount', $(this).attr('amount'));
            $modalUpdateBcoin.find('.modal-title').text(`${$(this).attr('username')} (${$(this).attr('amount')} bcoin)`);
            $modalUpdateBcoin.modal('show');
        })
        $(document).on('click', 'button[name=save-bcoin]', function () {
            const userId = $modalUpdateBcoin.data('userid');
            const amount = $modalUpdateBcoin.data('amount');
            const changeAmount = $modalUpdateBcoin.find('input[name=amount]').val();
            if (validateInput({
                listValidateFunction: [{
                    func: rangeValidation,
                    params: {min: -1 * amount, max: MAX_INT32},
                    mes: `Số bcoin giảm tối đa là ${amount}`
                }], propertyName: 'amount'
            })) {
                $.ajax({
                    url: '{{url('/admin/bcoins')}}',
                    method: 'POST',
                    data: {
                        _token: '{{csrf_token()}}',
                        amount: changeAmount,
                        user_id: userId,
                    }
                }).done(res => {
                    $modalUpdateBcoin.modal('hide');
                    toastSuccess(res, 'Thay đổi bcoin thành công');
                    loadListShops({...dt, loading: {type: 'content'}});
                }).fail(err => {

                })
            }
        })

        function generateShops(users) {
            let tbody = ''
            for (let i = 0; i < users.length; i++) {
                const amount = users[i]?.amount ?? 0;
                let buttonBLock = '', isBlock = '';

                if (users[i].status === 1) {
                    buttonBLock = `<span id="lock-action-${users[i].user_id}" type="button" onclick="blockShop(${users[i].user_id})" class="btn-delete fa-lg fa fa-lock text-danger"></span>`;
                    isBlock = `<span id="lock-status-${users[i].user_id}" class="badge badge-success">Hoạt động</span>`;
                } else {
                    buttonBLock = `<span id="lock-action-${users[i].user_id}" type="button" onclick="unBlockShop(${users[i].user_id})" class="fa fa-lg fa-unlock text-success"></span>`;
                    isBlock = `<span id="lock-status-${users[i].user_id}" class="badge badge-danger">Bị khóa</span>`;
                }

                tbody += `<tr>
                        <td class="text-center">${i + 1}</td>
                        <td class="text-center">${users[i].shop_name}</td>
                        <td class="d-flex justify-content-between align-items-center">
                            <span>${amount.formatFullStop()}
                                <span class="fa fa-btc" aria-hidden="true"></span>
                            </span>
                            <span amount=${users[i].amount || 0} username="${users[i].shop_name}" userid=${users[i].user_id} type="button"
                                class="on-show-modal-bcoin fa mr-0 active fa-pencil-square-o fa-lg text-success show-modal-update-voucher">
                            </span>
                        </td>
                        <td class="text-center">${users[i].email}</td>
                        <td class="text-center">${users[i].phone}</td>
                        <td class="text-center">${users[i].address || ''}</td>
                        <td class="text-center">` + isBlock + `</td>
                        <td class="text-center">` + buttonBLock + `</td>
                    </tr>`;
            }
            $('.table-striped>tbody').empty().append(tbody);
        }

        function blockShop(id) {
            $.ajax({
                url: "{{url('/admin/block/shop')}}" + `/${id}`,
                method: "put",
                data: {
                    "_token": "{{csrf_token()}}"
                },
                success: function (obj, text, xhr) {
                    toastSuccessMessage(obj.message);
                    loadListShops({...dt, loading: {type: 'content'}})
                },
                fail: function (err) {
                    toastErrorMessage('có lỗi xảy ra');
                }
            })
        }

        function unBlockShop(id) {
            $.ajax({
                url: "{{url('/admin/un-block/shop')}}" + `/${id}`,
                method: "put",
                data: {
                    "_token": "{{csrf_token()}}"
                },
                success: function (obj, text, xhr) {
                    toastSuccessMessage(obj.message);
                    loadListShops({...dt, loading: {type: 'content'}})
                },
                fail: function (err) {
                    toastErrorMessage('có lỗi xảy ra');
                }
            })
        }

        function loadListShops({currentPage = 1, keyword = '', loading = {type: 'table', colspan: 8}}) {
            const _url = `lists/shops?page=${currentPage}&keyword=${keyword}&is_without_branch=0`;
            $.ajax({
                url: _url,
                method: 'GET',
                loading,
            })
                .done(res => {
                    const users = res?.users?.data ?? [];
                    if (users.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadListShops(dt);
                    } else if (users.length === 0) {
                        $('.table-striped>tbody').empty().append(genNoContentTable(8, 'Không có cửa hàng nào'));
                    } else {
                        generateShops(users);
                        pagination.setTotalItems(res.users.total);
                    }
                })
                .fail(err => {
                });
        }

        $(document).on('keydown', 'input[name=search-user]', function () {
            if (event.key === 'Enter') {
                dt.keyword = $(this).val();
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadListShops(dt);
            }
        })
    </script>
@endsection
