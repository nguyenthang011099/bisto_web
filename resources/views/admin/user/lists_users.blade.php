@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách người mua
                </div>
                <div class="row row-action-bisto-css">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-3 d-flex justify-content-between">
                        <div class="col-sm-6">
                            <a type="button" class="btn btn-outline-primary w-100 btn-bisto-height-css"
                               href="{{url('/admin/users/export-pdf')}}">
                                <span class="fas fa-file-pdf fa-lg"></span>
                                Xuất PDF
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a type="button" class="btn w-100 btn-outline-success btn-bisto-height-css"
                               href="{{url('/admin/users/export-excel')}}">
                                <span class="fas fa-file-excel fa-lg"></span>
                                Xuất excel
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search" placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm form-control btn-search-bisto-css"
                                   name="search-user">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Tên người mua</th>
                            <th class="text-center">Số Bcoin</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Số điện thoại</th>
                            <th class="text-center">Địa chỉ</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <div class="modal-container">
            <div class="modal-bcoin modal fade bd-example-modal-sm" tabindex="-1" role="dialog"
                 aria-labelledby="mySmallModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Nhập số bcoin thay đổi<span class="text-danger">*</span></label>
                                <input type="number" name="amount" class="form-control"
                                       placeholder="Nhập số âm để giảm bcoin">
                                <div class="mes-amount mes-v-display">
                                    Hãy nhập số bcoin thay đổi
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button name="save-bcoin" class="btn btn-primary">
                                Lưu thay đổi
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: 6}};
        let $modalUpdateBcoin, pagination;
        $(document).ready(function () {
            $modalUpdateBcoin = $('.modal-bcoin');
            loadListUsers(dt);
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadListUsers(dt);
                }
            });
        });


        $(document).on('click', '.on-show-modal-bcoin', function () {
            $modalUpdateBcoin.find('input[name=amount]').val(0).css('border', '1px solid #d7dbda');
            $modalUpdateBcoin.find('.mes-amount').removeClass('mes-iv-display mes-v-display').addClass('mes-v-display');
            $modalUpdateBcoin.data('userid', $(this).attr('userid'));
            $modalUpdateBcoin.data('amount', $(this).attr('amount'));
            $modalUpdateBcoin.find('.modal-title').text(`${$(this).attr('username')} (${$(this).attr('amount')} bcoins)`);
            $modalUpdateBcoin.modal('show');
        })
        $(document).on('click', 'button[name=save-bcoin]', function () {
            const userId = $modalUpdateBcoin.data('userid');
            const amount = $modalUpdateBcoin.data('amount');
            const changeAmount = $modalUpdateBcoin.find('input[name=amount]').val();
            if (validateInput({
                listValidateFunction: [{
                    func: rangeValidation,
                    params: {min: -1 * amount, max: Infinity},
                    mes: `Số bcoin giảm tối đa là ${amount}`
                }], propertyName: 'amount'
            })) {
                $.ajax({
                    url: '{{url('/admin/bcoins')}}',
                    method: 'POST',
                    data: {
                        _token: '{{csrf_token()}}',
                        amount: changeAmount,
                        user_id: userId,
                    }
                }).done(res => {
                    $modalUpdateBcoin.modal('hide');
                    toastSuccessMessage('Thay đổi bcoin thành công');
                    loadListUsers({...dt, loading: {type: 'content'}});
                }).fail(err => {
                    toastErrorMessage('Thay đổi thất bại');
                })
            }
        })

        function generateUsers(users) {
            let tbody = ''
            for (let i = 0; i < users.length; i++) {
                const amount = users[i]?.amount ?? 0;
                tbody += `<tr>
                        <td class="text-center">${i + 1}</td>
                        <td class="text-center">${users[i].fullname}</td>
                        <td class="d-flex justify-content-between align-items-center"><span>${amount.formatFullStop()} <span class="fa fa-btc" aria-hidden="true"></span></span><span amount=${users[i].amount || 0} username="${users[i].fullname}" userid=${users[i].user_id} class="on-show-modal-bcoin fa mr-0 active fa-pencil-square-o fa-lg text-success show-modal-update-voucher" type="button"></span></td>
                        <td class="text-center">${users[i].email}</td>
                        <td class="text-center">${users[i].phone}</td>
                        <td class="text-center">${users[i].address || ''}</td>
                    </tr>`;
            }
            $('.table-striped>tbody').empty().append(tbody);
        }

        function loadListUsers({currentPage = 1, keyword = '', loading = {type: 'table', colspan: 6}}) {
            const _url = `users?page=${currentPage}&keyword=${keyword}`;
            $.ajax({
                url: _url,
                method: 'GET',
                loading
            })
                .done(res => {
                    const users = res?.users?.data ?? [];
                    if (users.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadListUsers(dt);
                    } else if (users.length === 0) {
                        $('.table-striped>tbody').empty().append(genNoContentTable(6, `Không có người dùng nào`));
                    } else {
                        generateUsers(users);
                        pagination.setTotalItems(res.users.total);
                    }
                })
                .fail(err => {
                    toastErrorMessage('Load danh sách user thất bại');
                });
        }

        $(document).on('keydown', 'input[name=search-user]', function () {
            if (event.key === 'Enter') {
                dt.keyword = $(this).val();
                loadListUsers(dt);
            }
        })
    </script>
@endsection
