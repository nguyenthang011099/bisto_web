@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách danh mục sản phẩm
                </div>
                <div class="row w3-res-tb row-action-bisto-css">
                    <div class="col-sm-7">
                        <button class="btn btn-primary btn-show-add-bisto-css btn-show-add-category"
                                data-toggle="modal"
                                data-target=".add-category">+
                            Thêm danh mục sản phẩm
                        </button>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control" name="option-category-level">
                            <option value="1" selected>Cấp độ 1</option>
                            <option value="2">Cấp độ 2</option>
                            <option value="3">Cấp độ 3</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input onkeydown="searchCategory()" type="search" placeholder="&#xf002 Tìm kiếm"
                                   class="form-control btn-search-bisto-css"
                                   name="keywords_submit">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th class="text-center">Tên danh mục</th>
                                <th class="text-center">Thuộc danh mục</th>
                                <th class="text-center">Hình ảnh</th>
                                <th class="text-center">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody id="body-table-cate">
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div style="" id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <div class="modal modal-category-bisto" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style="border-radius: 20px">
                    <div class="iq-card">
                        <header class="panel-heading">
                            Thêm danh mục sản phẩm
                        </header>
                        <div class="iq-card-body">
                            <div class="loading-category">
                                <div class="loading-center">
                                    <div class="spinner-border text-primary" role="status">
                                    </div>
                                </div>
                            </div>
                            <div class="position-center category">
                                <div id="form-category">
                                    <div class="form-group">
                                        <label>Cấp danh mục</label>
                                        <select name="category_level"
                                                class="form-control">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group bisto-none">
                                            <label>Danh mục gốc</label>
                                            <select name="category_id" class="form-control input-sm m-bot15">
                                            </select>
                                            <div class="mes-v-display mes-category_id"></div>
                                        </div>
                                        <div class="form-group bisto-none">
                                            <label>Danh mục con cấp 2</label>
                                            <select name="category_id_level2"
                                                    class="form-control input-sm m-bot15">
                                            </select>
                                            <div class="mes-v-display mes-category_id_level2"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="name-category">Tên danh mục</label><span
                                                class="text-danger">*</span>
                                            <input type="text" class="form-control" name="name"/>
                                            <div class="mes-v-display mes-name"></div>
                                        </div>
                                        <div class="form-group" style="overflow: auto">
                                            <label class="required-input">Hình ảnh</label><span
                                                class="note-img note-img-1-11"></span>
                                            <div class="custom-file">
                                                <input type="file"
                                                       class="form-control-file custom-file-input inputfile"
                                                       id="img-category" multiple>
                                                <div id="img-container" class="row align-items-start">
                                                </div>
                                                <label class="custom-file-label" for="customFile">Chọn
                                                    ảnh</label>
                                            </div>
                                            <div class="mes-image_url mes-v-display">
                                                Bạn phải thêm ít nhất 1 ảnh
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-add-category">
                                        Thêm danh mục
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal modal-update-category" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style="border-radius: 20px">
                    <div class="iq-card">
                        <section class="panel">
                            <header class="panel-heading">
                                Cập nhật danh mục sản phẩm
                            </header>
                            <div class="iq-card-body">
                                <div class="position-center category">
                                    <div id="form-update-category">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label class="name-category">Tên danh mục</label><span
                                                    class="text-danger">*</span>
                                                <input type="text" class="form-control" name="name"/>
                                                <div class="mes-v-display mes-name"></div>
                                            </div>
                                            <div class="form-group" style="overflow: auto">
                                                <label class="required-input">Hình ảnh</label><span
                                                    class="note-img note-img-1-11"></span>
                                                <div class="custom-file">
                                                    <input type="file"
                                                           class="form-control-file custom-file-input inputfile"
                                                           id="img-category-update" multiple>
                                                    <div id="img-container-update" class="row align-items-start">
                                                    </div>
                                                    <label class="custom-file-label" for="customFile">Chọn
                                                        ảnh</label>
                                                </div>
                                                <div class="mes-image_url mes-v-display">
                                                    Bạn phải thêm ít nhất 1 ảnh
                                                </div>
                                            </div>
                                            <button class="btn btn-primary btn-update-category">
                                                Cập nhật danh mục
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>
        let dt = {currentPage: 1, level: 1, searchKey: '', loading: {colspan: 5, type: 'table'}}, _;
        let pagination, _i;
        let selecterCategory;
        let cropperCategory;
        let uploaderCategory;
        let cropperCategoryUpdate;
        let uploaderCategoryUpdate;
        $(document).ready(function () {
            _i = defineImage({id: 'category'});
            loadCategory(dt);
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadCategory(dt);
                }
            });
            let _totalFiles = 0;
            uploaderCategory = defineUploadFile({
                containerSelector: '#img-container', imageName: 'image_url', optionsPhotoViewer: {
                    callbacks: {
                        opened: function (context) {
                            $('.photoviewer-stage').addClass('photoviewer-stage-svg');
                        },
                    }
                }
            })
            cropperCategory = defineCropAndUploadImage({
                type: 'category', inputSelector: '#img-category',
                cbWhenAddFile: () => {
                    if (uploaderCategory.getTotalFile() > 0) {
                        toastInfoMessage('Cho phép chọn tối đa 1 ảnh danh mục');
                        cropperCategory.stopUpload();
                    }
                }, fileExtensions: ['gif', 'png', 'jpg', 'jpeg', 'svg'],
            });
            cropperCategory.saveButton.addEventListener('click', function () {
                _totalFiles = uploaderCategory.getTotalFile() + 1;
                const {fileElement} = uploaderCategory.createFileElement(cropperCategory.getImageUrl(), cropperCategory.getImgName());
                uploaderCategory.getFileUrls().push(cropperCategory.getImg());
                uploaderCategory.setTotalFile(_totalFiles);
                uploaderCategory.getContainer().append(fileElement);
                cropperCategory.$modal.modal('hide');
            });

            let _totalFilesUpdate = 0;
            uploaderCategoryUpdate = defineUploadFile({
                containerSelector: '#img-container-update',
                imageName: 'image_url', optionsPhotoViewer: {
                    callbacks: {
                        opened: function (context) {
                            $('.photoviewer-stage').addClass('photoviewer-stage-svg');
                        },
                    }
                }
            })
            cropperCategoryUpdate = defineCropAndUploadImage({
                type: 'category-update', inputSelector: '#img-category-update',
                cbWhenAddFile: () => {
                    if (uploaderCategoryUpdate.getTotalFile() > 0) {
                        toastInfoMessage('Cho phép chọn tối đa 1 ảnh danh mục');
                        cropperCategoryUpdate.stopUpload();
                    }
                }, fileExtensions: ['gif', 'png', 'jpg', 'jpeg', 'svg'],
            });
            cropperCategoryUpdate.saveButton.addEventListener('click', function () {
                _totalFilesUpdate = uploaderCategoryUpdate.getTotalFile() + 1;
                const {fileElement} = uploaderCategoryUpdate.createFileElement(cropperCategoryUpdate.getImageUrl(), cropperCategoryUpdate.getImgName());
                uploaderCategoryUpdate.getFileUrls().push(cropperCategoryUpdate.getImg())
                uploaderCategoryUpdate.setTotalFile(_totalFilesUpdate);
                uploaderCategoryUpdate.getContainer().append(fileElement);
                uploaderCategoryUpdate.isNewUpdateImg = true;
                cropperCategoryUpdate.$modal.modal('hide');
            });

            selecterCategory = multiSelect({
                listSelect: [{
                    selectSelector: 'select[name=category_id]',
                    url: 'root-category', valueName: 'id', showName: 'name'
                }, {
                    selectSelector: 'select[name=category_id_level2]',
                    url: 'category-level-two/', valueName: 'id', showName: 'name'
                }], loading: {type: 'content', selector: '.loading-category'}
            });
            selecterCategory.loadRootSelect();
        });

        function generateCategories(categories) {
            _i.resetPhotos();
            let tbody = '';
            for (let i = 0; i < categories.length; i++) {
                const id = categories[i].id
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${categories[i].name}</td>
                            <td class="text-center">${categories[i].parent_name}</td>
                            <td class="text-center">
                                 ${_i.toHTML(categories[i].image_url, categories[i].name)}
                            </td>
                            <td class="text-center">
                                <span type="button" categoryid=${id} class="fas fa-edit fa-lg text-success btn-show-update-category"></span>
                            </td>
                          </tr>`;
            }
            $('.table-striped>tbody').empty().append(tbody);
            _i.animateOnloadImage({});
        }

        function loadCategory({currentPage = 1, level = 1, searchKey = '', loading = {colspan: 5, type: 'table'}}) {
            $.ajax({
                url: `categories/query?keyword=${searchKey}&level=${level}&page=${currentPage}`,
                method: "GET",
                loading
            }).done(res => {
                const categories = res?.categories?.data ?? [];
                if (categories.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadCategory(dt);
                } else if (categories.length === 0) {
                    $('.table-striped>tbody').empty().append(genNoContentTable(5, 'Không có danh mục nào'));
                } else {
                    generateCategories(categories);
                    pagination.setTotalItems(res.categories.total);
                }
            }).fail(err => {
            })
        }

        $(document).on('click', '.btn-show-update-category', function () {
            const id = $(this).attr('categoryid');
            $('.btn-update-category').data('categoryid', id);
            $.ajax({
                url: `categories/${id}`,
                method: 'GET',
                loading: {type: 'content'},
            })
                .done(res => {
                    dataToForm({
                        fieldInputs: [{selector: 'input[name=name]', value: res.category.name}],
                        fieldFiles: [{
                            uploaderInstance: uploaderCategoryUpdate,
                            listFiles: [{name: res.category.id, url: res.category.image_url}]
                        }],
                        parentSelector: '.modal-update-category', action: 'clear-validate'
                    })
                    $('.modal-update-category').modal('show');
                })
                .fail(err => {
                    console.log(err);
                })
        })

        $(document).on('click', '.btn-delete', function () {
            const id = $(this).attr('categoryid');
            confirmDelete('Bạn có chắc muốn xóa danh mục này?', function () {
                $.ajax({
                    url: `categories/${id}`,
                    method: 'DELETE',
                    loading: {type: 'content'},
                    data: {
                        _token: "{{csrf_token()}}",
                    }
                })
                    .done(res => {
                        toastSuccess(res, 'Xóa thành công');
                        loadCategory({...dt, loading: {type: 'content'}});
                    })
                    .fail(err => {
                        console.log(err);
                    })
            })
        })

        $('.btn-update-category').on('click', function () {
            const id = $('.btn-update-category').data('categoryid');
            if (validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Tên danh mục là bắt buộc'
                }],
                propertyName: 'name', parentSelector: '#form-update-category'
            }) && validateRequiredElement({
                inputSelector: 'input[name=image_url]',
                mesSelector: '.mes-image_url',
                message: 'Bạn phải thêm ít nhất một ảnh', parentSelector: '#form-update-category'
            })) {
                const dataObj = getDataFromForm('#form-update-category');
                $.ajax({
                    url: `categories/${id}`,
                    method: 'PUT',
                    data: {
                        _token: "{{csrf_token()}}",
                        nameCateEdit: dataObj.name,
                        image_url: uploaderCategoryUpdate.isNewUpdateImg ? uploaderCategoryUpdate.getOneImg() : null,
                    },
                })
                    .done(res => {
                        toastSuccess(res, 'Cập nhật danh mục thành công');
                        $('.modal-update-category').modal('hide');
                        loadCategory({...dt, loading: {type: 'content'}});
                    })
                    .fail(err => {
                        console.log(err);
                    })
            }
        })

        $('select[name=option-category-level]').on('change', function () {
            dt.level = $(this).val();
            loadCategory(dt);
        })

        function searchCategory() {
            if (event.key === 'Enter') {
                dt.searchKey = $('input[name=keywords_submit]').val();
                loadCategory(dt);
            }
        }

        $('.btn-show-add-category').on('click', function () {
            $('.modal-category-bisto').modal('show');
            $('input[name=name]').val('');
        })

        $(document).on('click', '.btn-add-category', function () {
            if (validateFormCategory()) {
                const dataObj = getDataFromForm('#form-category');
                $.ajax({
                    url: "{{url('/admin/save-category')}}",
                    method: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        category_level: dataObj.category_level,
                        name: dataObj.name,
                        parent_id: selecterCategory.getCurrentValueSelected() ? selecterCategory.getCurrentValueSelected().value : null,
                        image_url: uploaderCategory.getOneImg()
                    },
                })
                    .done(res => {
                        toastSuccess(res, 'Thêm danh mục thành công');
                        $('.modal-category-bisto').modal('hide');
                        dt.level = dataObj.category_level;
                        if (dt.level === "1") selecterCategory.loadRootSelect();
                        dt.currentPage = 1;
                        pagination.setCurrentPage(dt.currentPage);
                        loadCategory({...dt, loading: {type: 'content'}});
                        dataToForm({
                            fieldInputs: [{selector: 'input[name=name]'}],
                            fieldFiles: [{
                                uploaderInstance: uploaderCategory,
                                listFiles: []
                            }],
                            parentSelector: '#form-category', action: 'clear-validate'
                        })
                    })
                    .fail(err => {
                        console.log(err);
                    })
            }
        })

        function validateFormCategory(parentSelector = '#form-category') {
            const level = $('select[name=category_level]').val();
            let validateLevel = true;
            if (level === '2') {
                validateLevel = validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Danh mục gốc là bắt buộc'
                    }], type: 'select',
                    propertyName: 'category_id', parentSelector
                })
            } else if (level === '3') {
                validateLevel = validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Danh mục cấp 2 là bắt buộc'
                    }],
                    propertyName: 'category_id_level2',
                    parentSelector, type: 'select',
                })
            }
            return validateLevel && validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Tên danh mục là bắt buộc'
                }],
                propertyName: 'name', parentSelector
            }) && validateRequiredElement({
                inputSelector: 'input[name=image_url]',
                mesSelector: '.mes-image_url',
                message: 'Bạn phải thêm ít nhất một ảnh', parentSelector
            });
        }


        $("select[name=category_level]").on('change', function () {
            const level = $(this).val();
            const $c2 = $('select[name=category_id_level2]').parent();
            const $c1 = $('select[name=category_id]').parent();
            const $cName = $('.name-category');
            if (level === '1') {
                $c1.hide();
                $c2.hide();
                $cName.text('Tên danh mục gốc');
            } else if (level === '2') {
                $c2.hide();
                $c1.show();
                $cName.text('Tên danh mục cấp 2');
            } else if (level === '3') {
                $c2.show();
                $c1.show();
                $cName.text('Tên danh mục cấp 3');
            }
        })
    </script>
@endsection


