@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page stylish-admin">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách phong cách
                </div>
                <div style="margin-top: 20px" class="row btn-bisto">
                    <div class="col-sm-6">
                        <button class="btn-show-add-bisto btn-show-add-bisto-css btn btn-primary">+ Thêm phong cách
                        </button>
                    </div>
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search"
                                   placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm btn-search-bisto btn-search-bisto-css form-control">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body body-bisto">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Tên phong cách</th>
                            <th class="text-center">Thuộc</th>
                            <th class="text-center">Hình ảnh</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody class="tbody-bisto">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <!-- ADD PRODUCT START -->
        <div id="modal-container">
            <div class="modal fade modal-bisto" tabindex="-1" role="dialog"
                 aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content" style="border-radius: 20px">
                        <div class="iq-card">
                            <header class="panel-heading title-modal-bisto">
                                Thêm phong cách
                            </header>
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="iq-card-body">
                                        <div class="form-bisto" role="form">
                                            @csrf
                                            <div class="form-group">
                                                <label class="required-input">Tên phong cách</label>
                                                <input type="text" name="name" class="form-control">
                                                <div class="mes-name mes-v-display">
                                                </div>
                                            </div>
                                            <div class="form-group type-checkbox">
                                                <label class="required-input">
                                                    Giới tính
                                                </label>
                                                <div class="d-flex">
                                                    <div class="form-check">
                                                        <input class="form-check-input" value="1" id="male" checked
                                                               type="radio"
                                                               name="type">
                                                        <label class="form-check-label" for="male">
                                                            Nam
                                                        </label>
                                                    </div>
                                                    <div class="form-check ml-3">
                                                        <input class="form-check-input" value="0" id="female"
                                                               type="radio"
                                                               name="type">
                                                        <label class="form-check-label" for="female">
                                                            Nữ
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="overflow: auto">
                                                <label class="required-input">Hình ảnh</label><span
                                                    class="note-img-1-11 note-img"></span>
                                                <div class="custom-file">
                                                    <input type="file"
                                                           class="form-control-file custom-file-input input-file-bisto">
                                                    <div id="img-container" class="row align-items-start">
                                                    </div>
                                                    <label class="custom-file-label" for="customFile">Chọn
                                                        ảnh</label>
                                                </div>
                                                <div class="mes-icon mes-v-display">
                                                    Bạn phải thêm ít nhất 1 ảnh
                                                </div>
                                            </div>

                                        </div>
                                        <div>
                                            <button class="btn btn-primary btn-add-bisto btn-add-bisto-css">
                                                Thêm phong cách
                                            </button>
                                            <button class="btn btn-primary btn-update-bisto btn-update-bisto-css">
                                                Cập nhật phong cách
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-container">
        </div>
    </div>
    <script>
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: 5}};
        let _i;
        let _, pagination, cropperStylish, uploaderStylish;

        $(document).ready(function () {
            _i = defineImage({id: 'stylish'});
            _ = defineBisto({
                parentSelector: '.stylish-admin', name: 'phong cách', v: [
                    {
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Tên phong cách là bắt buộc'
                        }],
                        propertyName: 'name'
                    },
                    {
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Bạn phải thêm ít nhất 1 ảnh'
                        }],
                        propertyName: 'icon'
                    }]
            });
            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadStylish(dt);
                }
            });
            loadStylish(dt);
            uploaderStylish = defineUploadFile({
                containerSelector: '#img-container', imageName: 'icon', id: 'icon',
            })
            cropperStylish = defineCropAndUploadImage({
                type: 'icon', inputSelector: '.input-file-bisto',
                cbWhenAddFile: () => {
                    if (uploaderStylish.getTotalFile() > 0) {
                        toastInfoMessage('Cho phép chọn tối đa 1 ảnh danh mục');
                        cropperStylish.stopUpload();
                    }
                }, fileExtensions: ['gif', 'png', 'jpg', 'jpeg', 'svg'],
            });
            cropperStylish.saveButton.addEventListener('click', function () {
                const {fileElement} = uploaderStylish.createFileElement(cropperStylish.getImageUrl(), cropperStylish.getImgName());
                uploaderStylish.getFileUrls().push(cropperStylish.getImg());
                uploaderStylish.setTotalFile(uploaderStylish.getTotalFile() + 1);
                uploaderStylish.getContainer().append(fileElement);
                cropperStylish.$modal.modal('hide');
            });

            _.onSearch((value) => {
                dt.keyword = value;
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadStylish(dt);
            });

            function clearForm() {
                dataToForm({
                    fieldInputs: [
                        {selector: 'input[name=name]'}
                    ],
                    fieldFiles: [{
                        uploaderInstance: uploaderStylish,
                        listFiles: []
                    }],
                    parentSelector: '.form-bisto', action: 'clear-validate'
                })
            }

            _.onShowAdd(() => {
                clearForm();
                $('#male').prop('checked', true);
            })

            _.onAdd((data) => {
                const type = $('input[name=type]:checked').val();
                $.ajax({
                    url: `{{url('/admin/stylishes')}}`,
                    method: 'POST',
                    data: {
                        ...data,
                        type,
                        icon: uploaderStylish.getOneImg()
                    }
                }).done(res => {
                    toastSuccess(res, 'Thêm phong cách thành công');
                    loadStylish(dt);
                    _.$modalAdd.modal('hide');
                }).fail(err => {
                    _.$modalAdd.modal('hide');
                })
            })

            _.onShowUpdate((id, cbShowModalUpdate) => {
                $.ajax({
                    url: `{{url('/admin/stylishes')}}/${id}`,
                    method: 'GET',
                }).done(res => {
                    const stylish = res?.result ?? {name: '', icon: {}};
                    if (stylish.type === 0) {
                        $('#female').prop('checked', true);
                    } else $('#male').prop('checked', true);
                    dataToForm({
                        fieldInputs: [
                            {selector: 'input[name=name]', value: stylish.name},
                        ],
                        fieldFiles: [{
                            uploaderInstance: uploaderStylish,
                            listFiles: [stylish.icon]
                        }],
                        parentSelector: '.form-bisto', action: 'clear-validate'
                    });
                    cbShowModalUpdate();
                }).fail(err => {
                })
            })

            _.onUpdate((id, data) => {
                $.ajax({
                    url: `{{url('/admin/stylishes')}}/${id}`,
                    method: 'PUT',
                    loading: {type: 'content'},
                    data: {
                        ...data,
                        type: $('input[name=type]:checked').val(),
                        icon: uploaderStylish.getOneImg()
                    }
                }).done(res => {
                    toastSuccess(res, 'Cập nhật phong cách thành công');
                    loadStylish({...dt, loading: {type: 'content'}});
                    _.$modalUpdate.modal('hide');
                }).fail(err => {
                    _.$modalUpdate.modal('hide');
                })
            })
        });


        function loadStylish({currentPage = 1, keyword = '', loading = {type: 'table', colspan: 5}}) {
            $.ajax({
                url: `{{url('/admin/stylishes')}}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
                loading,
            }).done(res => {
                const stylishes = res?.results?.data ?? [];
                if (stylishes?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadStylish(dt);
                } else if (stylishes?.length === 0) {
                    _.$tbody.empty().append(genNoContentTable(5, `Không có phong cách nào`));
                } else {
                    generateStylish(stylishes);
                    pagination.setTotalItems(res.results.total);
                }
            }).fail(err => {
            });
        }

        function generateStylish(stylishes) {
            let tbody = '';
            _i.resetPhotos();
            for (let i = 0; i < stylishes.length; i++) {
                const id = stylishes[i].id;
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${stylishes[i].name}</td>
                            <td class="text-center">${stylishes[i].type === 0 ? 'Nữ' : 'Nam'}</td>
                            <td>${_i.toHTML(stylishes[i].icon?.url ?? null, stylishes[i].name)}</td>
                            <td class="text-center">
                                    <span type="button" data-id=${id} class="fas fa-edit fa-lg text-success btn-show-update-bisto btn-show-update-bisto-css"></span>
                            </td>
                        </tr>`;
            }
            _.$tbody.empty().append(tbody);
            _i.animateOnloadImage({});
        }
    </script>
@endsection
