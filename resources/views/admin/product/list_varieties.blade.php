@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách sản phẩm
                </div>
                <div style="margin-top: 20px" class="row row-action-bisto-css">
                    <div class="col-sm-6">
                        <h5 style="margin-left: 20px">Tổng số sản phẩm: <span
                                id="total-product"></span></h5>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="admin-shop">
                            <option selected value="0">Tất cả chuỗi</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input onkeydown="searchProduct()" id="input-text-product" type="search"
                                   placeholder="&#xf002 Tìm kiếm" class="input-sm btn-search-bisto-css form-control"
                                   name="keywords_submit">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Tên sản phẩm</th>
                            <th class="text-center">Giá</th>
                            <th class="text-center">Hình sản phẩm</th>
                            <th class="text-center">Danh mục</th>
                            <th class="text-center">Màu sắc</th>
                            <th class="text-center">Size</th>
                            <th class="text-center">Giá biến thể</th>
                            <th class="text-center">Đã bán</th>
                            <th class="text-center">Số lượng</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody id="tbl-product">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->

                <!-- footer and pagination -->
            </div>
        </div>
        <!-- ADD PRODUCT START -->
        <div id="modal-add-update-product-container">
            <div id="modal-add-update-product" class="modal fade add-product" tabindex="-1" role="dialog"
                 aria-labelledby="myExtraLargeModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content" style="border-radius: 20px">
                        <div class="iq-card">
                            <header class="panel-heading title-product">
                                Thêm sản phẩm
                            </header>
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="iq-card-body">
                                        <div id="form-add-product" role="form"
                                             enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label>Tên sản phẩm</label><span class="text-danger">*</span>
                                                <input readonly type="text" name="name"
                                                       class="need-validate form-control">
                                                <div class="mes-name mes-invalid">
                                                    Tên sản phẩm là bắt buộc
                                                </div>
                                            </div>

                                            <div class="form-group" style="overflow: auto">
                                                <label>Hình ảnh sản phẩm</label><span class="text-danger">*</span>
                                                <div class="custom-file">
                                                    <input readonly disabled type="file"
                                                           class="form-control-file custom-file-input inputfile"
                                                           id="productImg" multiple>
                                                    <div id="img-container" class="row align-items-start"
                                                         style="overflow: auto">
                                                    </div>
                                                    <label class="custom-file-label" for="customFile">Chọn
                                                        ảnh</label>
                                                </div>
                                                <div class="mes-imageUrl mes-invalid">
                                                    Bạn phải thêm ít nhất 1 ảnh
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Mô tả sản phẩm</label>
                                                <textarea readonly style="resize: none;height: 250px;line-height: 1.5rem;" rows="8"
                                                          class="form-control"
                                                          name="description"
                                                          placeholder="Mô tả sản phẩm"></textarea>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-4">
                                                    <label>Danh mục gốc</label>
                                                    <select readonly name="category_id"
                                                            class="form-control input-sm m-bot15">
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Danh mục con cấp 2</label>
                                                    <select readonly name="category_id_level2"
                                                            class="form-control input-sm m-bot15">
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Danh mục con cấp 3</label>
                                                    <select readonly name="category_id_level3"
                                                            class="form-control input-sm m-bot15">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="mes-category_id_level3 mes-invalid">
                                                Bạn phải chọn danh mục cho sản phẩm
                                            </div>
                                            <div class="form-group">
                                                <label>Giá sản phẩm</label><span class="text-danger">*</span>
                                                <input readonly type="number" data-validation="number"
                                                       name="price" class="need-validate form-control"/>
                                                <div class="mes-price mes-invalid">
                                                    Giá sản phẩm là bắt buộc
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="product-size-color-container">
                                                    <div class="product-size-color-1 product-size-color flex-row-wrap">
                                                        <div class="col-sm form-group">
                                                            <label for="exampleInputPassword1">Kích cỡ</label>
                                                            <select name="size_id" class="form-control">
                                                                <option value="1">XL</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm form-group">
                                                            <label for="exampleInputPassword1">Màu</label>
                                                            <select name="color_id" class="form-control">
                                                                <option value="1">Đỏ</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm form-group">
                                                            <label>Số lượng</label><span class="text-danger">*</span>
                                                            <input type="number" readonly data-validation="number"
                                                                   data-validation-error-msg="Làm ơn điền số lượng"
                                                                   name="quantity"
                                                                   class="need-validate form-control">
                                                            <div class="mes-quantity mes-invalid">
                                                                Số lượng là bắt buộc
                                                            </div>
                                                        </div>
                                                        {{--                                <button type="button" id="myBtn" class="btn btn-warning mb-3">Tham khảo size</button>--}}
                                                        <div class="col-sm form-group">
                                                            <label>Giá</label><span class="text-danger">*</span>
                                                            <input readonly type="number" data-validation="number"
                                                                   name="price-size-color"
                                                                   class="need-validate form-control"/>
                                                            <div class="mes-price-size-color mes-invalid">
                                                                Giá là bắt buộc
                                                            </div>
                                                        </div>
                                                        <div onclick="removeParent(this)"
                                                             class="col-sm super-center minus-product-size-color">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                 height="24"
                                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                                 stroke-width="2" stroke-linecap="round"
                                                                 stroke-linejoin="round"
                                                                 class="feather feather-minus-circle">
                                                                <circle cx="12" cy="12" r="10"></circle>
                                                                <line x1="8" y1="12" x2="16" y2="12"></line>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mes-dup-sc mes-invalid">
                                                    Trùng lựa chọn size và màu
                                                </div>
                                                <div class=" mes-required-psc mes-invalid">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Phần trăm giảm giá</label>
                                                <input type="number"
                                                       name="discount"
                                                       class="form-control"/>
                                                <div class="mes-discount mes-v-display">
                                                </div>
                                            </div>
                                            <div class="iq-card">
                                                <div class="form-row">
                                                    <div class="col">
                                                        <label>Ngày bắt đầu</label>
                                                        <input readonly type="datetime-local"
                                                               class="form-control" id="start_at" name="start_at">
                                                    </div>
                                                    <div class="col">
                                                        <label>Ngày kết thúc</label>
                                                        <input readonly type="datetime-local" class="form-control"
                                                               id="end_at" name="end_at">
                                                    </div>
                                                </div>
                                                <div class="mes-rangedate mes-invalid">
                                                    Ngày bắt đầu phải nhỏ hơn ngày kết thúc
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Trạng thái</label>
                                                <select readonly name="active" class="form-control input-sm m-bot15">
                                                    <option value="1">Đang hoạt động</option>
                                                    <option value="-1">Khóa</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-container">

            </div>
        </div>
        <!-- ADD PRODUCT END -->

        <script type="text/javascript">
            let dt = {
                currentPage: 1,
                searchKey: '',
                currentProductId: null,
                loading: {type: 'table', colspan: 9},
                adminShopId: 0,
                quantity: 0
            }
            let elementProductSizeColor = document.getElementsByClassName('product-size-color')[0].outerHTML || '';
            let modalAddUpdateProductElement = $('#modal-add-update-product')[0].outerHTML || '';
            let optionsMale = ''
            let optionsFemale = ''
            let selecterCategory, pagination, img = defineImage({id: 'product'});

            let uploaderProductImage;
            $(document).ready(function () {
                const params = parseUrlParams();
                if (params?.quantity === '0') {
                    loadProducts({...dt, quantity: '0'});
                } else loadProducts(dt);
                loadSizeColorCategory();
                loadListAdminShop();
                $('select[name=admin-shop]').on('change', function () {
                    dt.adminShopId = $('select[name=admin-shop] option:selected').val();
                    loadProducts(dt);
                })
                uploaderProductImage = defineUploadFile({
                    inputSelector: '#productImg',
                    containerSelector: '#img-container',
                })
                pagination = definePagination({
                    onPageClick: (pageNumber, event) => {
                        dt.currentPage = pageNumber;
                        loadProducts(dt);
                    }
                });
            });


            //event delete product-size-color element
            function removeParent(e) {
                e.parentElement.remove();
            }

            function onShowModalUpdateProduct() {
                $('.title-product').empty().text('Chi tiết sản phẩm');
                $('select[name=active]').attr('disabled', false);
                const id = event.target.getAttribute('productid');
                $('#modal-add-update-product').data('productid', id);
                const _url = `detail-product/${id}`
                $.ajax({
                    url: _url,
                    method: 'GET',
                }).done(res => {
                    let product = res.results || {};
                    let listValuesSelectCategory = [];
                    listValuesSelectCategory.push(product.category.id)
                    let _parent = product.category.parent;
                    while (_parent) {
                        listValuesSelectCategory.push(_parent.id);
                        _parent = _parent.parent;
                    }
                    selecterCategory.loadSelects(listValuesSelectCategory.reverse());
                    $('input[name=name]').val(product.name);
                    $('#img-container').empty();
                    let _totalFiles = 0;
                    let _fileUrls = [];
                    for (const img_url of product.image_urls) {
                        const {fileElement} = uploaderProductImage.createFileElement(img_url, _totalFiles.toString());
                        _fileUrls.push({name: _totalFiles.toString(), url: img_url})
                        _totalFiles++;
                        $('#img-container').append(fileElement);
                    }
                    uploaderProductImage.setTotalFile(_totalFiles);
                    uploaderProductImage.setFileUrls(_fileUrls);
                    if (product.description)
                        $('textarea[name=description]').val(product.description);

                    if (product.category_parent_id == 1) {
                        $('select[name=category_id]').empty().append(optionsMale);
                        $('input.male').attr('checked', true);
                    } else {
                        $('select[name=category_id]').empty().append(optionsFemale);
                        $('input.female').attr('checked', true);
                    }
                    $('select[name=category_id]').val(product.category_id);
                    $('input[name=discount]').val(product.discount);
                    $('input[name=active]').val(product.active);
                    $('input[name=price]').val(product.price);
                    let start_at = new Date(product.start_at);
                    start_at.setMinutes(start_at.getMinutes() - start_at.getTimezoneOffset());
                    $('input[name=start_at]')[0].value = start_at.toISOString().slice(0, 16);
                    let end_at = new Date(product.end_at);
                    end_at.setMinutes(end_at.getMinutes() - end_at.getTimezoneOffset());
                    $('input[name=end_at]')[0].value = end_at.toISOString().slice(0, 16);

                    $('div.product-size-color-container').empty();
                    for (const productSizeColor of product.product_size_color) {
                        let newElementProductSizeColor = createElementByText(elementProductSizeColor);
                        newElementProductSizeColor.querySelector('select[name=size_id]').value = productSizeColor.size_id;
                        newElementProductSizeColor.querySelector('select[name=color_id]').value = productSizeColor.color_id;
                        newElementProductSizeColor.querySelector('input[name=price-size-color]').value = productSizeColor.price;
                        newElementProductSizeColor.querySelector('input[name=quantity]').value = productSizeColor.quantity;
                        $('div.product-size-color-container').append(newElementProductSizeColor);
                    }
                    $('#modal-add-update-product').modal('show');
                })
                    .fail(err => {
                    })
            }

            //get product object from form
            function getProductObject() {
                let product = {
                    name: $('input[name=name]').val() || '',
                    price: parseFloat($('input[name=price]').val()) || 0,
                    discount: parseFloat($('input[name=discount]').val()) || 0,
                    active: parseInt($('select[name=active]').val()),
                    start_at: new Date($('input[name=start_at]').val()).getTime() / 1000 || null,
                    end_at: new Date($('input[name=end_at]').val()).getTime() / 1000 || null,
                    category_id: selecterCategory.getCurrentValueSelected().value || 0,
                    description: $('textarea[name=description]').val() || ''
                }
                let product_size_color = [];
                for (const productSizeColorElement of $('.product-size-color')) {
                    let p = {};
                    p.color_id = parseInt(productSizeColorElement.querySelector('select[name=color_id]').value) || 0;
                    p.size_id = parseInt(productSizeColorElement.querySelector('select[name=size_id]').value) || 0;
                    p.quantity = parseInt(productSizeColorElement.querySelector('input[name=quantity]').value) || 0;
                    p.price = parseInt(productSizeColorElement.querySelector('input[name=price-size-color]').value) || 0;
                    product_size_color.push(p);
                }
                let image_urls = [];
                for (const imageUrlElement of $('input[name=imageUrl]')) {
                    image_urls.push(imageUrlElement.value);
                }
                product.product_size_color = product_size_color;
                product.image_urls = image_urls;
                console.log('getproductObject', product);
                return product;
            }

            //products to interface
            function generateProducts(products, search = false) {
                let tbody = '';
                console.log(products)
                img.resetPhotos();

                for (let i = 0; i < products.length; i++) {
                    const varieties = products[i].varieties;
                    const rowspan = varieties.length ?? 1;
                    const id = products[i].id;
                    let isActive = null;
                    if (products[i].self_status === 1)
                        isActive = `<span class="badge badge-success">Hiển thị</span>`;
                    else isActive = `<span class="badge badge-danger">Ẩn</span>`;
                    let isBlock;
                    if (products[i].admin_status === 1)
                        isBlock = `<span id="lock-status-${id}" class="badge badge-success">Không khóa</span>`;
                    else isBlock = `<span id="lock-status-${id}" class="badge badge-danger">Bị khóa</span>`;
                    let trr = `<tr>
                                    <td rowspan="${rowspan}" class="text-center">${i + 1}</td>
                                    <td rowspan="${rowspan}" class="text-center">${products[i].name}</td>
                                    <td rowspan="${rowspan}" class="text-center">${products[i].price.formatVND()}</td>
                                    <td rowspan="${rowspan}" class="text-center">
                                        ${img.toHTML(products[i].image, products[i].name)}
                                    </td>
                                    <td rowspan="${rowspan}" class="text-center">${products[i].category_name}</td>
                                    <td class="text-center">${varieties[0].color_name}</td>
                                    <td class="text-center">${varieties[0].size_name}</td>
                                    <td class="text-center">${varieties[0].price.formatVND()}</td>
                                    <td class="text-center">${varieties[0].sold}</td>
                                    <td class="text-center">${varieties[0].quantity}</td>
                                    <td class="text-center" rowspan="${rowspan}">
                                        <span class="text-ellipsis">${isActive}</span>
                                        <span class="text-ellipsis">${isBlock}</span>
                                    </td>
                                    <td class="text-center" rowspan="${rowspan}">
                                        <span type="button" onclick="onShowModalUpdateProduct()" productid=${id} class="fa fa-eye btn-see-bisto-css fa-lg btn-show-modal-edit-product text-success"></span>
                                       ${products[i].admin_status === 1 ? `<span id="lock-action-${id}" type="button" onclick="blockProduct(${id})" class="btn-delete btn-block-bisto-css fa fa-lg fa-lock text-danger"></span>` : `<span id="lock-action-${id}" type="button" onclick="unBlockProduct(${id})" class="btn-delete btn-unblock-bisto-css fa fa-lg fa-unlock text-success"></span>`}
                                    </td>
                                </tr>`;
                    for (let j = 1; j < varieties.length; j++) {
                        trr += `<tr>
                                    <td class="text-center">${varieties[j].color_name}</td>
                                    <td class="text-center">${varieties[j].size_name}</td>
                                    <td class="text-center">${varieties[j].price.formatVND()}</td>
                                    <td class="text-center">${varieties[j].sold}</td>
                                    <td class="text-center">${varieties[j].quantity}</td>
                                </tr>`;
                    }
                    tbody += trr;
                }
                $('.table-striped>tbody').empty().append(tbody);
                img.animateOnloadImage({});
            }

            //call api get product by paging and generate on interface
            function loadProducts({
                                      currentPage = 1,
                                      searchKey = '',
                                      loading = {type: 'table', colspan: 9},
                                      adminShopId = 0, quantity = 0
                                  }) {
                $.ajax({
                    url: `{{url('/admin/product-varieties')}}?keyword=${searchKey}&page=${currentPage}&adminShopId=${adminShopId}&quantity=${quantity}`,
                    loading,
                    method: 'GET',
                })
                    .done(res => {
                        const products = res?.products?.data ?? [];
                        if (products.length === 0 && dt.currentPage > 1) {
                            dt.currentPage -= 1;
                            pagination.setCurrentPage(1);
                            loadProducts(dt);
                        } else if (products.length === 0) {
                            $('.table-striped>tbody').empty().append(genNoContentTable(12, `Không có sản phẩm nào`));
                            pagination.setTotalItems(res.products.total);
                            $('#total-product').text(res.products.total);
                        } else {
                            $('#total-product').text(res.products.total);
                            generateProducts(products);
                            pagination.setTotalItems(res.products.total);
                        }
                    })
                    .fail(err => {
                    });
            }

            function blockProduct(id) {
                $.ajax({
                    url: '{{url('/admin/product/block')}}' + `/${id}`,
                    method: 'GET'
                }).done(res => {
                    //isBlock = `<span id="lock-status-${id}" class="badge badge-success">Không khóa</span>`;
                    // else isBlock = `<span id="lock-status-${id}" class="badge badge-danger">Bị khóa</span>`;
                    $(`#lock-status-${id}`).text('Bị khóa');
                    $(`#lock-status-${id}`).attr('class', 'badge badge-danger');
                    $(`#lock-action-${id}`).attr("onclick", `unBlockProduct(${id})`);
                    $(`#lock-action-${id}`).attr("class", "btn-delete fa fa-lg btn-unblock-bisto-css fa-unlock text-success");
                    toastSuccessMessage(res.message);
                }).fail(err => {
                    toastErrorMessage('Load size thất bại');
                });
            }

            function unBlockProduct(id) {
                $.ajax({
                    url: '{{url('/admin/product/un-block')}}' + `/${id}`,
                    method: 'GET'
                }).done(res => {
                    $(`#lock-status-${id}`).text('Không khóa');
                    $(`#lock-status-${id}`).attr('class', 'badge badge-success');
                    $(`#lock-action-${id}`).attr("onclick", `blockProduct(${id})`);
                    $(`#lock-action-${id}`).attr("class", "btn-delete fa-lg fa btn-block-bisto-css fa-lock text-danger");
                    toastSuccessMessage(res.message);
                }).fail(err => {
                    toastErrorMessage('Load size thất bại');
                });
            }

            //call api get size color category and generate on interface
            function loadSizeColorCategory() {
                $.ajax({
                    url: '{{url('/admin/all-size')}}',
                    method: 'GET'
                }).done(res => {
                    let options = ''
                    res.results.forEach(item => {
                        options += `<option value="${item.id}">${item.name}</option>`
                    })
                    $('select[name=size_id]').empty().append(options);
                    elementProductSizeColor = $('div.product-size-color')[0].outerHTML;
                }).fail(err => {
                    // alert('Load size thất bại');
                });

                $.ajax({
                    url: '{{url('/admin/all-color')}}',
                    method: 'GET'
                }).done(res => {
                    let options = ''
                    res.results.forEach(item => {
                        options += `<option value="${item.id}">${item.name}</option>`
                    })
                    $('select[name=color_id]').empty().append(options);
                    elementProductSizeColor = $('div.product-size-color')[0].outerHTML;
                }).fail(err => {
                    // alert('Load color thất bại');
                });

                selecterCategory = multiSelect({
                    listSelect: [{
                        selectSelector: 'select[name=category_id]',
                        url: 'root-category', valueName: 'id', showName: 'name'
                    }, {
                        selectSelector: 'select[name=category_id_level2]',
                        url: 'category-level-two/', valueName: 'id', showName: 'name'
                    }, {
                        selectSelector: 'select[name=category_id_level3]',
                        url: 'category-level-three/',
                        valueName: 'id',
                        showName: 'name'
                    }], loading: null
                });
                selecterCategory.loadRootSelect();
            }

            //handle event when user typing enter on input search
            function searchProduct(e) {
                // user press enter search
                if (event.key === 'Enter') {
                    dt.searchKey = $('#input-text-product').val();
                    loadProducts(dt);
                }
            }

            function loadListAdminShop() {
                $.ajax({
                    url: 'lists/admin-shops?paginate=false',
                    method: 'GET'
                }).done(res => {
                    const users = res.users;
                    let options = '';
                    for (const user of users) {
                        options += `<option value=${user.user_id}>${user.shop_name}</option>`
                    }
                    $('select[name=admin-shop]').append(options);
                }).fail(err => {
                    toastErrorMessage('Load danh sách admin-shop thất bại')
                })
            }
        </script>
@endsection
