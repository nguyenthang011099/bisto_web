<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Xuất file PDF</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
        body {
            font-family: DejaVu Sans, serif;
        }

        .table td,
        .table th {
            padding: 0.4rem !important;
            font-size: 12px;
        }
    </style>
</head>

<body>
<div class="container-fluid p-md-1 p-sm-1 p-xl-1 p-lg-1">
    <h5 class="text-center mb-3">{{ $title }}</h5>

    <table class="table table-bordered mb-5">
        <thead>
        <tr class="table-warning">
            <th class="text-center">STT</th>
            <th class="text-center">Họ tên</th>
            <th class="text-center">Email</th>
            <th class="text-center">SĐT</th>
            <th class="text-center">Địa chỉ</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $key => $user)
            <tr>
                <td class="text-center text-wrap" style="width: 10px">{{ $key + 1 }}</td>
                <td class="text-center text-nowrap" style="width: 30px">{{ $user->fullname }}</td>
                <td class="text-center text-nowrap" style="width: 30px">{{ $user->email }}</td>
                <td class="text-center text-nowrap" style="width: 30px">{{ $user->phone }}</td>
                <td class="text-wrap" style="width: 200px">{{ $user->address }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>

</html>
