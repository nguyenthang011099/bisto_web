@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page advertisement-admin">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách ảnh quảng cáo
                </div>
                <div style="margin-top: 20px" class="row btn-bisto row-action-bisto-css">
                    <div class="col-sm-6">
                        <button class="btn-show-add-bisto btn-show-add-bisto-css btn btn-primary">+ Thêm ảnh quảng cáo
                        </button>
                    </div>
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                        {{--                        <div class="input-group">--}}
                        {{--                            <input type="search"--}}
                        {{--                                   placeholder="&#xf002 Tìm kiếm"--}}
                        {{--                                   class="input-sm btn-search-bisto btn-search-bisto-css form-control">--}}
                        {{--                        </div>--}}
                    </div>
                </div>
                <div class="iq-card-body body-bisto">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Ảnh</th>
                            <th class="text-center">Đường dẫn</th>
                            <th class="text-center">Ngày tạo</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody class="tbody-bisto">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <!-- ADD START -->
        <div id="modal-container">
            <div class="modal fade modal-bisto" tabindex="-1" role="dialog"
                 aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content" style="border-radius: 20px">
                        <div class="iq-card">
                            <header class="panel-heading title-modal-bisto">
                                Thêm ảnh quảng cáo
                            </header>
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="iq-card-body">
                                        <div class="form-bisto" role="form">
                                            @csrf
                                            <div class="form-group" style="overflow: auto">
                                                <label class="required-input">Hình ảnh quảng cáo</label><span
                                                    class="note-img note-img-1-normal"></span>
                                                <div class="custom-file">
                                                    <input type="file"
                                                           class="form-control-file custom-file-input inputfile"
                                                           id="img-advertisement" multiple>
                                                    <div id="img-container-advertisement" class="row align-items-start">
                                                    </div>
                                                    <label class="custom-file-label" for="customFile">Chọn
                                                        ảnh</label>
                                                </div>
                                                <div class="mes-image_url mes-v-display">
                                                    Bạn phải thêm ít nhất 1 ảnh
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required-input">Đường dẫn</label>
                                                <input type="text" name="url" class="form-control">
                                                <div class="mes-url mes-v-display">
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <button class="btn btn-primary btn-add-bisto btn-add-bisto-css">
                                                Thêm ảnh quảng cáo
                                            </button>
                                            <button class="btn btn-primary btn-update-bisto btn-update-bisto-css">
                                                Cập nhật ảnh quảng cáo
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-container">
        </div>
    </div>
    <script>
        const COLSPAN = 6;
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: COLSPAN}};
        let _, pagination, _i;
        let uploaderImage;

        $(document).ready(function () {
            _ = defineBisto({
                parentSelector: '.advertisement-admin', name: 'ảnh quảng cáo',
            });

            _i = defineImage({id: 'advertisement'});

            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadAdvertisement(dt);
                }
            });

            uploaderImage = defineUploadFile({
                id: 'advertisement',
                containerSelector: '#img-container-advertisement', imageName: 'image_url', maxFiles: 1,
                cropperOption: {inputSelector: '#img-advertisement'}
            })

            loadAdvertisement(dt);

            // _.onSearch((value) => {
            //     dt.keyword = value;
            //     dt.currentPage = 1;
            //     pagination.setCurrentPage(1);
            //     loadAdvertisement(dt);
            // });

            _.onShowAdd(() => {
                dataToForm({
                    fieldInputs: [
                        {selector: 'input[name=url]'},
                    ],
                    fieldFiles: [{
                        uploaderInstance: uploaderImage,
                        listFiles: [],
                    }],
                    parentSelector: '.form-bisto', action: 'clear-validate'
                })
            })

            _.onAdd((data) => {
                if (validateFormAdvertisement())
                    $.ajax({
                        url: `{{url('/admin/marketings')}}`,
                        method: 'POST',
                        data: {
                            ...data,
                            url: genUrl(data?.url),
                            image: uploaderImage.getOneImg(),
                        }
                    }).done(res => {
                        toastSuccess(res, 'Thêm ảnh quảng cáo thành công');
                        loadAdvertisement({...dt, loading: {type: 'content'}});
                        _.$modalAdd.modal('hide');
                    }).fail(err => {
                        _.$modalAdd.modal('hide');
                    })
            })

            _.onShowUpdate((id, cbShowModalUpdate) => {
                $.ajax({
                    url: `{{url('/admin/marketings')}}/${id}`,
                    method: 'GET',
                    loading: {type: 'content'}
                }).done(res => {
                    const advertisement = res?.result ?? {url: '', created_at: null};
                    dataToForm({
                        fieldInputs: [
                            {selector: 'input[name=url]', value: advertisement.url},
                        ],
                        fieldFiles: [{
                            uploaderInstance: uploaderImage,
                            listFiles: [{name: advertisement?.image?.filename, url: advertisement?.image?.url}],
                        }],
                        parentSelector: '.form-bisto', action: 'clear-validate'
                    });
                    cbShowModalUpdate();
                }).fail(err => {
                })
            })

            _.onUpdate((id, data) => {
                if (validateFormAdvertisement())
                    $.ajax({
                        url: `{{url('/admin/marketings')}}/${id}`,
                        method: 'PUT',
                        loading: {type: 'content'},
                        data: {
                            ...data,
                            url: genUrl(data?.url),
                            image: uploaderImage._isNewUpdateImg() ? uploaderImage.getOneImg() : null,
                        }
                    }).done(res => {
                        toastSuccess(res, 'Cập nhật ảnh quảng cáo thành công');
                        loadAdvertisement({...dt, loading: {type: 'content'}});
                        _.$modalUpdate.modal('hide');
                    }).fail(err => {
                        _.$modalUpdate.modal('hide');
                    })
            })

            _.onDelete((id) => {
                $.ajax({
                    url: `{{url('/admin/marketings')}}/${id}`,
                    method: 'DELETE',
                    data: {
                        _token: '{{csrf_token()}}',
                    }
                }).done(res => {
                    toastSuccess(res, 'Xóa ảnh quảng cáo thành công');
                    loadAdvertisement({...dt, loading: {type: 'content'}});
                    _.$modalUpdate.modal('hide');
                }).fail(err => {
                    _.$modalUpdate.modal('hide');
                })
            })
        });

        function validateFormAdvertisement() {
            return validateRequiredElement({
                inputSelector: 'input[name=image_url]',
                mesSelector: '.mes-image_url',
                message: 'Bạn phải thêm ít nhất một ảnh', parentSelector: '.form-bisto'
            }) && validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Đường dẫn là bắt buộc'
                }, {
                    func: urlValidation,
                    mes: 'Đường dẫn không hợp lệ'
                }],
                propertyName: 'url'
            })
        }

        function loadAdvertisement({currentPage = 1, keyword = '', loading = {type: 'table', colspan: 4}}) {
            $.ajax({
                url: `{{url('/admin/marketings')}}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
                loading,
            }).done(res => {
                const advertisements = res?.results?.data ?? [];
                if (advertisements?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadAdvertisement(dt);
                } else if (advertisements?.length === 0) {
                    _.$tbody.empty().append(genNoContentTable(COLSPAN, `Không có dữ liệu`));
                } else {
                    generateAdvertisements(advertisements);
                    pagination.setTotalItems(res.results.total);
                }
            }).fail(err => {
            });
        }

        function genUrl(url) {
            return url.match(/^http[s]?:\/\//) ? url : 'https://' + url;
        }

        $(document).on('click', '.ads-url', function (e) {
            e.preventDefault();
            const url = e.currentTarget.dataset?.url;
            window.open(genUrl(url), '_blank');
        })

        function generateAdvertisements(advertisements) {
            let tbody = '';
            _i.resetPhotos();
            for (let i = 0; i < advertisements.length; i++) {
                const id = advertisements[i].id;
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${_i.toHTML(advertisements[i]?.image?.url, advertisements[i]?.url)}</td>
                            <td class="text-center"><a href="/#" class="ads-url" data-url="${advertisements[i]?.url ?? ''}" target="_blank">${advertisements[i]?.url ?? ''}</a></td>
                            <td class="text-center">${advertisements[i]?.created_at?.formatDate() ?? ''}</td>
                            <td class="text-center">
                                <span type="button" data-id=${id} class="fas fa-edit fa-lg text-success btn-show-update-bisto btn-show-update-bisto-css"></span>
                                <span type="button" data-id=${id} class="fas fa-times fa-lg text-danger btn-delete-bisto-css btn-delete-bisto"></span>
                            </td>
                        </tr>`;
            }
            _.$tbody.empty().append(tbody);
        }
    </script>
@endsection
