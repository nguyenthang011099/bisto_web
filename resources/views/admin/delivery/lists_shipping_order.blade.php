@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách đơn hàng vận chuyển
                </div>
                <div class="row row-action-bisto-css">
                    <div class="col-sm-3">
                        <select class="form-control" style="margin-left: 20px" id="btn-provider" name="btn-provider">
                            <option selected value="0">Tất cả đơn vị vận chuyển</option>
                            <option value="{{\App\ShippingOrder::GIAO_HANG_NHANH}}">Giao hàng nhanh</option>
                            <option value="{{\App\ShippingOrder::AHAMOVE}}">Ahamove</option>
                            <option value="{{\App\ShippingOrder::GHTK}}">Giao hàng tiết kiệm</option>
                        </select>
                    </div>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-3">
                                <select class="form-control" id="btn-range-date" name="btn-range-date">
                                    <option value="0" selected>Cả tháng</option>
                                    <option value="1">Nửa đầu tháng</option>
                                    <option value="2">Nửa cuối tháng</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" id="btn-month" name="btn-month">
                                    <option selected value="0">Tất cả 12 tháng</option>
                                    <option value="1">Tháng 1</option>
                                    <option value="2">Tháng 2</option>
                                    <option value="3">Tháng 3</option>
                                    <option value="4">Tháng 4</option>
                                    <option value="5">Tháng 5</option>
                                    <option value="6">Tháng 6</option>
                                    <option value="7">Tháng 7</option>
                                    <option value="8">Tháng 8</option>
                                    <option value="9">Tháng 9</option>
                                    <option value="10">Tháng 10</option>
                                    <option value="11">Tháng 11</option>
                                    <option value="12">Tháng 12</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" id="btn-year" name="btn-year">
                                    <option value="2021">Năm 2021</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <button id="btn-query" name="btn-query"
                                        class="btn btn-bisto-height-css btn-primary">Truy Vấn
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Mã vận chuyển</th>
                            <th class="text-center">Tên đơn vị vận chuyển</th>
                            <th class="text-center">Thời gian tạo</th>
                            <th class="text-center">Tổng tiền vận chuyển</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div style="" id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
                {{--                <div class="d-flex justify-content-end">--}}
                {{--                    <div>--}}
                {{--                        <p class="total-shipping-fee">Tổng số phí vận chuyển: 0 VND</p>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
        </div>
    </div>

    <script>
        let dt = {
            currentPage: 1,
            text_search: '',
            loading: {type: 'table', colspan: 5},
            provider: 0,
            option: 0,
            month: 0,
            year: 2021
        }, pagination;
        $(document).ready(function () {
            loadShippingOrders(dt);
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadShippingOrders(dt.currentPage);
                }
            });
        });

        //event when user press query order by option
        $(document).on('click', '#btn-query', function () {
            dt.option = document.getElementById('btn-range-date').value;
            dt.month = document.getElementById('btn-month').value;
            dt.year = document.getElementById('btn-year').value;
            dt.provider = document.getElementById('btn-provider').value;
            loadShippingOrders(dt);
        })

        function generateShippingOrders(shippingOrders) {
            let tbody = '', total_fee = 0;
            for (let i = 0; i < shippingOrders.length; i++) {
                tbody += `<tr>
                        <td class="text-center">${i + 1}</td>
                        <td class="text-center">${shippingOrders[i]?.code ?? ''}</td>
                        <td class="text-center">${shippingOrders[i].provider_name}</td>
                        <td class="text-center">${shippingOrders[i].created_at.formatDate()}</td>
                        <td class="text-center">${toVND(shippingOrders[i]?.shipping_fee ?? 0)}</td>
            </tr>`
                total_fee += shippingOrders[i]?.shipping_fee ?? 0;
            }
            $('.table-striped>tbody').empty().append(tbody);
            // $('.total-shipping-fee').text(`Tổng số phí vận chuyển: ${toVND(total_fee)}`);
        }

        function loadShippingOrders({
                                        currentPage = 1,
                                        text_search = '',
                                        loading = {type: 'table', colspan: 5},
                                        provider = 0,
                                        option = 0,
                                        month = 0,
                                        year = 2021
                                    }) {
            $.ajax({
                url: `{{url('/admin')}}/shipping-orders?page=${currentPage}&keyword=${text_search}`,
                method: 'POST',
                data: {
                    _token: "{{csrf_token()}}",
                    option: option,
                    month: month,
                    year: year,
                    provider: provider,
                },
                loading
            })
                .done(res => {
                    const shippingOrders = res?.results?.data ?? [];
                    if (shippingOrders.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadShippingOrders(dt);
                    } else if (shippingOrders.length === 0) {
                        $('.table-striped>tbody').empty().append(genNoContentTable(5, 'Không có vận chuyển nào'));
                    } else {
                        generateShippingOrders(shippingOrders);
                        pagination.setTotalItems(res.results.total);
                    }
                })
                .fail(err => {
                });
        }

        // $('input[name=keywords_submit]').on('keydown', function (e) {
        //     if (e.key === 'Enter') {
        //         dt.text_search = $(this).val();
        //         loadShippingOrders(dt);
        //     }
        // })
    </script>
@endsection
