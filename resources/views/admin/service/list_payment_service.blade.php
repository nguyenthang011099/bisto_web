@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page payment-service-admin">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách phí thanh toán
                </div>
                <div style="margin-top: 20px" class="row btn-bisto row-action-bisto-css">
                    <div class="col-sm-6">
                        <button class="btn-show-add-bisto btn-show-add-bisto-css btn btn-primary">+ Thêm phí thanh toán
                        </button>
                    </div>
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search"
                                   placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm btn-search-bisto btn-search-bisto-css form-control">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body body-bisto">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Đơn vị thanh toán</th>
                            <th class="text-center">Phần trăm thêm</th>
                            <th class="text-center">Tiền thêm</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody class="tbody-bisto">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <!-- ADD PRODUCT START -->
        <div id="modal-container">
            <div class="modal fade modal-bisto" tabindex="-1" role="dialog"
                 aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content" style="border-radius: 20px">
                        <div class="iq-card">
                            <header class="panel-heading title-modal-bisto">
                                Thêm phí thanh toán
                            </header>
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="iq-card-body">
                                        <div class="form-bisto" role="form">
                                            @csrf
                                            <div class="form-group">
                                                <label class="required-input">Đơn vị thanh toán</label>
                                                <input type="text" name="unit_payment" class="form-control">
                                                <div class="mes-unit_payment mes-v-display">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required-input">Phần trăm thêm</label>
                                                <input type="number" name="plus_percent" class="form-control">
                                                <div class="mes-plus_percent mes-v-display">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required-input">Tiền thêm</label>
                                                <input type="number" name="plus_amount" class="form-control">
                                                <div class="mes-plus_amount mes-v-display">
                                                </div>
                                            </div>
                                            <div class="mes-required-plus mes-v-display">
                                                Phần trăm thêm hay tiền thêm là bắt buộc
                                            </div>
                                        </div>
                                        <div>
                                            <button class="btn btn-primary btn-add-bisto btn-add-bisto-css">
                                                Thêm phí thanh toán
                                            </button>
                                            <button class="btn btn-primary btn-update-bisto btn-update-bisto-css">
                                                Cập nhật phí thanh toán
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-container">
        </div>
    </div>
    <script>
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: 5}};
        let _, pagination;

        $(document).ready(function () {
            _ = defineBisto({
                parentSelector: '.payment-service-admin', name: 'phí',
            });

            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadPaymentService(dt);
                }
            });
            loadPaymentService(dt);

            _.onSearch((value) => {
                dt.keyword = value;
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadPaymentService(dt);
            });

            _.onShowAdd(() => {
                dataToForm({
                    fieldInputs: [
                        {selector: 'input[name=unit_payment]'},
                        {selector: 'input[name=plus_percent]'},
                        {selector: 'input[name=plus_amount]'}
                    ],
                    parentSelector: '.form-bisto', action: 'clear-validate'
                })
            })

            _.onAdd((data) => {
                if (validateFormFee())
                    $.ajax({
                        url: `{{url('/admin/payment-services')}}`,
                        method: 'POST',
                        data: {
                            ...data,
                        }
                    }).done(res => {
                        toastSuccess(res, 'Thêm phí thanh toán thành công');
                        loadPaymentService({...dt, loading: {type: 'content'}});
                        _.$modalAdd.modal('hide');
                    }).fail(err => {
                        _.$modalAdd.modal('hide');
                    })
            })

            _.onShowUpdate((id, cbShowModalUpdate) => {
                $.ajax({
                    url: `{{url('/admin/payment-services')}}/${id}`,
                    method: 'GET',
                }).done(res => {
                    const fee = res?.result ?? {plus_percent: 0, plus_amount: 0, unit_payment: ''};
                    dataToForm({
                        fieldInputs: [
                            {selector: 'input[name=unit_payment]', value: fee.unit_payment},
                            {selector: 'input[name=plus_percent]', value: fee.plus_percent},
                            {selector: 'input[name=plus_amount]', value: fee.plus_amount}
                        ],
                        parentSelector: '.form-bisto', action: 'clear-validate'
                    });
                    cbShowModalUpdate();
                }).fail(err => {
                })
            })

            _.onUpdate((id, data) => {
                if (validateFormFee())
                    $.ajax({
                        url: `{{url('/admin/payment-services')}}/${id}`,
                        method: 'PUT',
                        loading: {type: 'content'},
                        data: {
                            ...data,
                        }
                    }).done(res => {
                        toastSuccess(res, 'Cập nhật phí thanh toán thành công');
                        loadPaymentService({...dt, loading: {type: 'content'}});
                        _.$modalUpdate.modal('hide');
                    }).fail(err => {
                        _.$modalUpdate.modal('hide');
                    })
            })

            _.onDelete((id) => {
                $.ajax({
                    url: `{{url('/admin/payment-services')}}/${id}`,
                    method: 'DELETE',
                    data: {
                        _token: '{{csrf_token()}}',
                    }
                }).done(res => {
                    toastSuccess(res, 'Xóa phí thanh toán thành công');
                    loadPaymentService({...dt, loading: {type: 'content'}});
                    _.$modalUpdate.modal('hide');
                }).fail(err => {
                    _.$modalUpdate.modal('hide');
                })
            })
        });

        function validateFormFee() {
            const validatePlus = () => {
                if ($('input[name=plus_percent]').val() === '' && $('input[name=plus_amount]').val() === '') {
                    $('.mes-required-plus').show();
                    return false;
                } else {
                    $('.mes-required-plus').hide()
                    return true;
                }
            }
            return validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Đơn vị thanh toán là bắt buộc'
                }],
                propertyName: 'unit_payment'
            }) && validatePlus() && validateInput({
                listValidateFunction: [{
                    func: rangeValidation,
                    params: {min: 0, max: 100},
                    mes: MES_PERCENT
                }],
                propertyName: 'plus_percent'
            }) && validateInput({
                listValidateFunction: [{
                    func: rangeValidation,
                    params: {min: 0, max: MAX_MONEY},
                    mes: 'Tiền thêm lớn hơn 0'
                }],
                propertyName: 'plus_amount'
            })
        }

        function loadPaymentService({currentPage = 1, keyword = '', loading = {type: 'table', colspan: 5}}) {
            $.ajax({
                url: `{{url('/admin/payment-services')}}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
                loading,
            }).done(res => {
                const fees = res?.results?.data ?? [];
                if (fees?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadPaymentService(dt);
                } else if (fees?.length === 0) {
                    _.$tbody.empty().append(genNoContentTable(5, `Không có phí thanh toán nào`));
                } else {
                    generateFees(fees);
                    pagination.setTotalItems(res.results.total);
                }
            }).fail(err => {
            });
        }

        function generateFees(fees) {
            let tbody = '';
            for (let i = 0; i < fees.length; i++) {
                const id = fees[i].id;
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${fees[i].unit_payment ?? ''}</td>
                            <td class="text-center">${fees[i]?.plus_percent ?? 0} %</td>
                            <td class="text-center">${toVND(fees[i].plus_amount)}</td>
                            <td class="text-center">
                                <span type="button" data-id=${id} class="fas fa-edit fa-lg text-success btn-show-update-bisto btn-show-update-bisto-css"></span>
                                <span type="button" data-id=${id} class="fas fa-times fa-lg text-danger btn-delete-bisto-css btn-delete-bisto"></span>
                            </td>
                        </tr>`;
            }
            _.$tbody.empty().append(tbody);
        }
    </script>
@endsection
