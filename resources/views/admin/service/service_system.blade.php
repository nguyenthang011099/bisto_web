@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page service-system-admin">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Phí người mua
                </div>
                <div style="margin-top: 20px" class="row btn-bisto row-action-bisto-css">
                    <div id="btn-add-container" class="col-sm-6">
                        {{--                        <button class="btn-show-add-bisto btn-show-add-bisto-css btn btn-primary">+ Thêm phí người mua--}}
                        {{--                        </button>--}}
                    </div>
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>
                <div class="iq-card-body body-bisto">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">Phí người mua</th>
                            <th class="text-center">Ngày cập nhật gần nhất</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody class="tbody-bisto">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
            {{--                <div class="pagination-custom">--}}
            {{--                    <div id="pagination-container"></div>--}}
            {{--                </div>--}}
            <!-- PAGINATION END-->
            </div>
        </div>
        <!-- ADD PRODUCT START -->
        <div id="modal-container">
            <div class="modal fade modal-bisto" tabindex="-1" role="dialog"
                 aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content" style="border-radius: 20px">
                        <div class="iq-card">
                            <header class="panel-heading title-modal-bisto">
                                Thêm phí người mua
                            </header>
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="iq-card-body">
                                        <div class="form-bisto" role="form">
                                            @csrf
                                            <div class="form-group">
                                                <label class="required-input">Phí áp dụng</label>
                                                <input type="number" name="amount" class="form-control">
                                                <div class="mes-amount mes-v-display">
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <button class="btn btn-primary btn-add-bisto btn-add-bisto-css">
                                                Thêm phí người mua
                                            </button>
                                            <button class="btn btn-primary btn-update-bisto btn-update-bisto-css">
                                                Cập nhật phí người mua
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-container">
        </div>
    </div>
    <script>
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: 3}};
        let _, pagination;
        $(document).ready(function () {
            _ = defineBisto({
                parentSelector: '.service-system-admin', name: 'phí người mua',
            });
            // pagination = definePagination({
            //     onPageClick: (pageNumber) => {
            //         dt.currentPage = pageNumber;
            //         loadServiceSystem(dt);
            //     }
            // });
            loadServiceSystem(dt);
            _.onShowAdd(() => {
                dataToForm({
                    fieldInputs: [
                        {selector: 'input[name=amount]'}
                    ],
                    parentSelector: '.form-bisto', action: 'clear-validate'
                })
            })

            _.onAdd((data) => {
                if (validateFormService())
                    $.ajax({
                        url: `{{url('/admin/service-system')}}`,
                        method: 'POST',
                        data: {
                            ...data,
                        }
                    }).done(res => {
                        toastSuccess(res, 'Thêm phí người mua thành công');
                        loadServiceSystem({...dt, loading: {type: 'content'}});
                        $('#btn-add-container').empty();
                        _.$modalAdd.modal('hide');
                    }).fail(err => {
                        _.$modalAdd.modal('hide');
                    })
            })

            _.onShowUpdate((id, cbShowModalUpdate) => {
                $.ajax({
                    url: `{{url('/admin/service-system')}}`,
                    method: 'GET',
                }).done(res => {
                    const service = res?.result ?? {amount: 0};
                    dataToForm({
                        fieldInputs: [
                            {selector: 'input[name=amount]', value: service.amount}
                        ],
                        parentSelector: '.form-bisto', action: 'clear-validate'
                    });
                    cbShowModalUpdate();
                }).fail(err => {
                })
            })

            _.onUpdate((id, data) => {
                if (validateFormService())
                    $.ajax({
                        url: `{{url('/admin/service-system')}}`,
                        method: 'PUT',
                        loading: {type: 'content'},
                        data: {
                            ...data,
                        }
                    }).done(res => {
                        toastSuccess(res, 'Cập nhật phí người mua thành công');
                        loadServiceSystem({...dt, loading: {type: 'content'}});
                        _.$modalUpdate.modal('hide');
                    }).fail(err => {
                        _.$modalUpdate.modal('hide');
                    })
            })
        });

        function validateFormService() {
            return validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Phí người mua là bắt buộc'
                }, {
                    func: rangeValidation,
                    params: {min: 0, max: MAX_MONEY},
                    mes: 'Phí người mua >= 0 VND'
                }],
                propertyName: 'amount'
            })
        }

        function loadServiceSystem({currentPage = 1, keyword = '', loading = {type: 'table', colspan: 3}}) {
            $.ajax({
                url: `{{url('/admin/service-system')}}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
                loading,
            }).done(res => {
                const service = res?.result ?? null;
                if (service)
                    generateService(service);
                else {
                    _.$tbody.empty().append(genNoContentTable(3, 'Chưa có phí người mua'));
                    $('#btn-add-container').empty().append(`
<button class="btn-show-add-bisto btn-show-add-bisto-css btn btn-primary">+ Thêm phí người mua</button>`);
                }
            }).fail(err => {
            });
        }

        function generateService(service) {
            let tbody = '';
            tbody += `<tr>
                            <td class="text-center">${toVND(service.amount)}</td>
                            <td class="text-center">${service.updated_at.formatDate()}</td>
                            <td class="text-center">
                                <span type="button" class="fas fa-edit fa-lg text-success btn-show-update-bisto btn-show-update-bisto-css"></span>
                            </td>
                        </tr>`;
            _.$tbody.empty().append(tbody);
        }
    </script>
@endsection
