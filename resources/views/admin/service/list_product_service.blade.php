@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page service-product-admin">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách dịch vụ đẩy sản phẩm lên top
                </div>
                <div style="margin-top: 20px" class="row btn-bisto row-action-bisto-css">
                    <div class="col-sm-6">
                        <button class="btn-show-add-bisto btn-show-add-bisto-css btn btn-primary">+ Thêm gói dịch vụ
                        </button>
                    </div>
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                        {{--                        <div class="input-group">--}}
                        {{--                            <input type="search"--}}
                        {{--                                   placeholder="&#xf002 Tìm kiếm"--}}
                        {{--                                   class="input-sm btn-search-bisto btn-search-bisto-css form-control">--}}
                        {{--                        </div>--}}
                    </div>
                </div>
                <div class="iq-card-body body-bisto">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Số ngày áp dụng</th>
                            <th class="text-center">Số sản phẩm tối đa</th>
                            <th class="text-center">Phí áp dụng</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody class="tbody-bisto">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <!-- ADD PRODUCT START -->
        <div id="modal-container">
            <div class="modal fade modal-bisto" tabindex="-1" role="dialog"
                 aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content" style="border-radius: 20px">
                        <div class="iq-card">
                            <header class="panel-heading title-modal-bisto">
                                Thêm gói dịch vụ
                            </header>
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="iq-card-body">
                                        <div class="form-bisto" role="form">
                                            @csrf
                                            <div class="form-group">
                                                <label class="required-input">Số ngày áp dụng gói dịch vụ</label>
                                                <input type="number" name="date" class="form-control">
                                                <div class="mes-date mes-v-display">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required-input">Số sản phẩm tối đa</label>
                                                <input type="number" name="max_product_apply" class="form-control">
                                                <div class="mes-max_product_apply mes-v-display">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required-input">Phí áp dụng</label>
                                                <input type="number" name="price" class="form-control">
                                                <div class="mes-price mes-v-display">
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <button class="btn btn-primary btn-add-bisto btn-add-bisto-css">
                                                Thêm gói dịch vụ
                                            </button>
                                            <button class="btn btn-primary btn-update-bisto btn-update-bisto-css">
                                                Cập nhật gói dịch vụ
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-container">
        </div>
    </div>
    <script>
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: 5}};
        let _, pagination;

        $(document).ready(function () {
            _ = defineBisto({
                parentSelector: '.service-product-admin', name: 'gói dịch vụ',
            });

            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadProductService(dt);
                }
            });
            loadProductService(dt);

            _.onSearch((value) => {
                dt.keyword = value;
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadProductService(dt);
            });

            _.onShowAdd(() => {
                dataToForm({
                    fieldInputs: [
                        {selector: 'input[name=date]'},
                        {selector: 'input[name=max_product_apply]'},
                        {selector: 'input[name=price]'}
                    ],
                    parentSelector: '.form-bisto', action: 'clear-validate'
                })
            })

            _.onAdd((data) => {
                if (validateFormService())
                    $.ajax({
                        url: `{{url('/admin/product-services')}}`,
                        method: 'POST',
                        data: {
                            ...data,
                        }
                    }).done(res => {
                        toastSuccess(res, 'Thêm gói dịch vụ thành công');
                        loadProductService({...dt, loading: {type: 'content'}});
                        _.$modalAdd.modal('hide');
                    }).fail(err => {
                        _.$modalAdd.modal('hide');
                    })
            })

            _.onShowUpdate((id, cbShowModalUpdate) => {
                $.ajax({
                    url: `{{url('/admin/product-services')}}/${id}`,
                    method: 'GET',
                }).done(res => {
                    const service = res?.result ?? {date: 0, max_product_apply: 0, price: 0};
                    dataToForm({
                        fieldInputs: [
                            {selector: 'input[name=date]', value: service.date},
                            {selector: 'input[name=max_product_apply]', value: service.max_product_apply},
                            {selector: 'input[name=price]', value: service.price}
                        ],
                        parentSelector: '.form-bisto', action: 'clear-validate'
                    });
                    cbShowModalUpdate();
                }).fail(err => {
                })
            })

            _.onUpdate((id, data) => {
                if (validateFormService())
                    $.ajax({
                        url: `{{url('/admin/product-services')}}/${id}`,
                        method: 'PUT',
                        loading: {type: 'content'},
                        data: {
                            ...data,
                        }
                    }).done(res => {
                        toastSuccess(res, 'Cập nhật gói dịch vụ thành công');
                        loadProductService({...dt, loading: {type: 'content'}});
                        _.$modalUpdate.modal('hide');
                    }).fail(err => {
                        _.$modalUpdate.modal('hide');
                    })
            })

            _.onDelete((id) => {
                $.ajax({
                    url: `{{url('/admin/product-services')}}/${id}`,
                    method: 'DELETE',
                    data: {
                        _token: '{{csrf_token()}}',
                    }
                }).done(res => {
                    toastSuccess(res, 'Xóa gói dịch vụ thành công');
                    loadProductService({...dt, loading: {type: 'content'}});
                    _.$modalUpdate.modal('hide');
                }).fail(err => {
                    _.$modalUpdate.modal('hide');
                })
            })
        });

        function validateFormService() {
            return validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Số ngày là bắt buộc'
                }, {
                    func: rangeValidation,
                    params: {min: 1, max: MAX_INT32},
                    mes: 'Số ngày lớn hơn 0'
                }],
                propertyName: 'date'
            }) && validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Số sản phẩm áp dụng là bắt buộc'
                }, {
                    func: rangeValidation,
                    params: {min: 1, max: MAX_INT32},
                    mes: 'Số sản phẩm áp dụng lớn hơn 0'
                }],
                propertyName: 'max_product_apply'
            }) && validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Phí áp dụng là bắt buộc'
                }, {
                    func: rangeValidation,
                    params: {min: 1000, max: MAX_MONEY},
                    mes: 'Phí áp dụng >= 1000 VND'
                }],
                propertyName: 'price'
            })
        }

        function loadProductService({currentPage = 1, keyword = '', loading = {type: 'table', colspan: 5}}) {
            $.ajax({
                url: `{{url('/admin/product-services')}}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
                loading,
            }).done(res => {
                const services = res?.results?.data ?? [];
                if (services?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadProductService(dt);
                } else if (services?.length === 0) {
                    _.$tbody.empty().append(genNoContentTable(5, `Không có gói dịch vụ nào`));
                } else {
                    generateServices(services);
                    pagination.setTotalItems(res.results.total);
                }
            }).fail(err => {
            });
        }

        function generateServices(services) {
            let tbody = '';
            for (let i = 0; i < services.length; i++) {
                const id = services[i].id;
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${services[i].date ?? 0} ngày</td>
                            <td class="text-center">${services[i]?.max_product_apply ?? 0} sản phẩm</td>
                            <td class="text-center">${toVND(services[i].price)}</td>
                            <td class="text-center">
                                <span type="button" data-id=${id} class="fas fa-edit fa-lg text-success btn-show-update-bisto btn-show-update-bisto-css"></span>
                                <span type="button" data-id=${id} class="fas fa-times fa-lg text-danger btn-delete-bisto-css btn-delete-bisto"></span>
                            </td>
                        </tr>`;
            }
            _.$tbody.empty().append(tbody);
        }
    </script>
@endsection
