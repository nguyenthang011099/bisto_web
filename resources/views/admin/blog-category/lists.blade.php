@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page blog-category-admin">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách danh mục
                </div>
                <div style="margin-top: 20px" class="row btn-bisto row-action-bisto-css">
                    <div class="col-sm-6">
                        <button class="btn-show-add-bisto btn-show-add-bisto-css btn btn-primary">+ Thêm danh mục
                        </button>
                    </div>
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search"
                                   placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm btn-search-bisto btn-search-bisto-css form-control">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body body-bisto">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Tên</th>
                            <th class="text-center">Số blog</th>
                            <th class="text-center">Ngày tạo</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody class="tbody-bisto">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <!-- ADD START -->
        <div id="modal-container">
            <div class="modal fade modal-bisto" tabindex="-1" role="dialog"
                 aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content" style="border-radius: 20px">
                        <div class="iq-card">
                            <header class="panel-heading title-modal-bisto">
                                Thêm danh mục
                            </header>
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="iq-card-body">
                                        <div class="form-bisto" role="form">
                                            @csrf
                                            <div class="form-group">
                                                <label class="required-input">Tên danh mục</label>
                                                <input type="text" name="name" class="form-control">
                                                <div class="mes-name mes-v-display">
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <button class="btn btn-primary btn-add-bisto btn-add-bisto-css">
                                                Thêm danh mục
                                            </button>
                                            <button class="btn btn-primary btn-update-bisto btn-update-bisto-css">
                                                Cập nhật danh mục
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-container">
        </div>
    </div>
    <script>
        const COLSPAN = 6;
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: COLSPAN}};
        let _, pagination;

        $(document).ready(function () {
            _ = defineBisto({
                parentSelector: '.blog-category-admin', name: 'danh mục',
            });

            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadBlogCategory(dt);
                }
            });
            loadBlogCategory(dt);

            _.onSearch((value) => {
                dt.keyword = value;
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadBlogCategory(dt);
            });

            _.onShowAdd(() => {
                dataToForm({
                    fieldInputs: [
                        {selector: 'input[name=name]'},
                    ],
                    parentSelector: '.form-bisto', action: 'clear-validate'
                })
            })

            _.onAdd((data) => {
                if (validateFormBlogCategory())
                    $.ajax({
                        url: `{{url('/admin/blog-categories')}}`,
                        method: 'POST',
                        data: {
                            ...data,
                            slug: convertToSlug(data?.name, `-${randomString(5)}`)
                        }
                    }).done(res => {
                        toastSuccess(res, 'Thêm danh mục thành công');
                        loadBlogCategory({...dt, loading: {type: 'content'}});
                        _.$modalAdd.modal('hide');
                    }).fail(err => {
                        _.$modalAdd.modal('hide');
                    })
            })

            _.onShowUpdate((id, cbShowModalUpdate) => {
                $.ajax({
                    url: `{{url('/admin/blog-categories')}}/${id}`,
                    method: 'GET',
                    loading: {type: 'content'}
                }).done(res => {
                    const blog = res?.result ?? {name: 0, created_at: null};
                    dataToForm({
                        fieldInputs: [
                            {selector: 'input[name=name]', value: blog.name},
                        ],
                        parentSelector: '.form-bisto', action: 'clear-validate'
                    });
                    cbShowModalUpdate();
                }).fail(err => {
                })
            })

            _.onUpdate((id, data) => {
                if (validateFormBlogCategory())
                    $.ajax({
                        url: `{{url('/admin/blog-categories')}}/${id}`,
                        method: 'PUT',
                        loading: {type: 'content'},
                        data: {
                            ...data,
                        }
                    }).done(res => {
                        toastSuccess(res, 'Cập nhật danh mục thành công');
                        loadBlogCategory({...dt, loading: {type: 'content'}});
                        _.$modalUpdate.modal('hide');
                    }).fail(err => {
                        _.$modalUpdate.modal('hide');
                    })
            })

            _.onDelete((id) => {
                $.ajax({
                    url: `{{url('/admin/blog-categories')}}/${id}`,
                    method: 'DELETE',
                    data: {
                        _token: '{{csrf_token()}}',
                    }
                }).done(res => {
                    toastSuccess(res, 'Xóa danh mục thành công');
                    loadBlogCategory({...dt, loading: {type: 'content'}});
                    _.$modalUpdate.modal('hide');
                }).fail(err => {
                    _.$modalUpdate.modal('hide');
                })
            })
        });

        function validateFormBlogCategory() {
            return validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Tên danh mục là bắt buộc'
                }],
                propertyName: 'name'
            })
        }

        function loadBlogCategory({currentPage = 1, keyword = '', loading = {type: 'table', colspan: 4}}) {
            $.ajax({
                url: `{{url('/admin/blog-categories')}}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
                loading,
            }).done(res => {
                const blogCategories = res?.results?.data ?? [];
                if (blogCategories?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadBlogCategory(dt);
                } else if (blogCategories?.length === 0) {
                    _.$tbody.empty().append(genNoContentTable(COLSPAN, `Không có dữ liệu`));
                } else {
                    generateBlogCategories(blogCategories);
                    pagination.setTotalItems(res.results.total);
                }
            }).fail(err => {
            });
        }

        function generateBlogCategories(blogCategories) {
            let tbody = '';
            for (let i = 0; i < blogCategories.length; i++) {
                const id = blogCategories[i].id;
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${blogCategories[i]?.name ?? ''}</td>
                            <td class="text-center">${blogCategories[i]?.count_blog ?? 0}</td>
                            <td class="text-center">${blogCategories[i]?.created_at?.formatDate() ?? ''}</td>
                            <td class="text-center">
                                <span type="button" data-id=${id} class="fas fa-edit fa-lg text-success btn-show-update-bisto btn-show-update-bisto-css"></span>
                                <span type="button" data-id=${id} class="fas fa-times fa-lg text-danger btn-delete-bisto-css btn-delete-bisto"></span>
                            </td>
                        </tr>`;
            }
            _.$tbody.empty().append(tbody);
        }
    </script>
@endsection
