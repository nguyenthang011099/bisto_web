@extends('admin.layout')
@section('admin_content')
    <?php
    $ID = 'post';
    ?>
    <div id="content-page" class="content-page post-admin">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách post
                </div>
                <div style="margin-top: 20px" class="row btn-bisto row-action-bisto-css">
                                        <div class="col-sm-6">
{{--                                            <button class="btn-show-add-bisto btn-show-add-bisto-css btn btn-primary">+ Thêm post--}}
                                            {{--                                            </button>--}}
                                        </div>
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search"
                                   placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm btn-search-bisto btn-search-bisto-css form-control">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body body-bisto">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Shop</th>
                            <th class="text-center">Tiêu đề</th>
                            <th class="text-center">Ngày tạo</th>
                            <th class="text-center">Mô tả</th>
                            <th class="text-center">Số like</th>
                            <th class="text-center">Số comment</th>
                            <th class="text-center">Hiển thị</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody class="tbody-bisto">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <div id="test-ck">

                </div>
                <!-- PAGINATION END-->
            </div>

            <div class="iq-card">
                <div class="panel-heading" id="form-post-title">
                    Chi tiết bài đăng
                </div>

                <div class="iq-card-body">
                    <section class="panel">
                        <div class="panel-body">
                            <div class="iq-card-body">
                                <div class="form-bisto" role="form" id="form-{{$ID}}">
                                    @csrf
                                    <div class="form-group">
                                        <label class="required-input">Tiêu đề</label>
                                        <input type="text" name="title" disabled class="form-control">
                                        <div class="mes-title mes-v-display">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="required-input">Mô tả</label>
                                        <textarea style="height: 80px" rows="8"
                                                  disabled
                                                  class="form-control"
                                                  name="description"
                                                  placeholder=""></textarea>
                                        <div class="mes-description mes-v-display"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="required-input">Nội dung</label>
                                        <div class="" id="">

                                        </div>
                                        <div class="" id="post-content-editor">
                                        </div>
                                        <div class="mes-content mes-v-display">
                                        </div>
                                    </div>
                                </div>
                                {{--                                <div class="btn-show-cmt">Bình luận</div>--}}
                                {{--                                <div id="cmt-post-admin">--}}

                                {{--                                </div>--}}
                                {{--                                <div>--}}
                                {{--                                    <button class="btn btn-primary btn-add-bisto btn-add-bisto-css">--}}
                                {{--                                        Thêm post--}}
                                {{--                                    </button>--}}
                                {{--                                    <button class="btn btn-primary btn-update-bisto btn-update-bisto-css">--}}
                                {{--                                        Cập nhật post--}}
                                {{--                                    </button>--}}
                                {{--                                </div>--}}
                            </div>
                        </div>
                    </section>
                </div>
            </div>

        </div>

    </div>
    <script>
        const COLSPAN = 10;
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: COLSPAN}};
        let pagination, CMT;
        let ckeditor;
        const $formTitle = $('#form-post-title');

        $(document).ready(function () {
            _ = defineBisto({
                parentSelector: '.post-admin', name: 'post',
            });
            _.$btnUpdate.hide();
            ckeditor = createCkeditor5({
                selector: '#post-content-editor', cb: function () {
                    ckeditor.setDisable();
                }
            })
            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadPost(dt);
                }
            });
            // CMT = defineComment({ selector: '#cmt-post-admin', showMode : 'collapse' });
            loadPost(dt);

            _.onSearch((value) => {
                dt.keyword = value;
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadPost(dt);
            });


            _.onAdd((data) => {
                if (validateFormPost())
                    $.ajax({
                        url: `{{url('/admin/posts')}}`,
                        method: 'POST',
                        data: {
                            ...data,
                            admin_status: 1,
                            content: ckeditor.getEditor().getData(),
                        }
                    }).done(res => {
                        toastSuccess(res, 'Thêm post thành công');
                        loadPost({...dt, loading: {type: 'content'}});
                        _.$modalAdd.modal('hide');
                    }).fail(err => {
                        _.$modalAdd.modal('hide');
                    })
            })

            _.onShowDetail((id) => {
                $.ajax({
                    url: `{{url('/admin/posts')}}/${id}`,
                    method: 'GET',
                    loading: {type: 'content'}
                }).done(res => {
                    const post = res?.result ?? {title: '', created_at: null};
                    dataToForm({
                        fieldInputs: [
                            {selector: 'input[name=title]', value: post?.title},
                            {selector: 'textarea[name=description]', value: post?.description},
                            {selector: 'select[name=self_status]', value: post?.self_status},
                        ],
                        parentSelector: '.form-bisto', action: 'set-disable'
                    })
                    ckeditor.getEditor().setData(post?.content)
                    $formTitle.text('Chi tiết bài đăng');
                    // CMT.initCommentsForPost(post?.id);
                }).fail(err => {
                })
            })

            _.onLockOrUnlock((id, status) => {
                $.ajax({
                    url: `{{url('/admin/posts/admin-status')}}/${id}`,
                    method: 'PUT',
                    data: {
                        _token: "{{csrf_token()}}",
                        admin_status: status,
                    },
                    loading: {type: 'content'}
                }).done(res => {
                    loadPost({...dt, loading: {type: 'content'}});
                }).fail(err => {
                })
            })

        });

        function loadPost({currentPage = 1, keyword = '', loading = {type: 'table', colspan: COLSPAN}}) {
            $.ajax({
                url: `{{url('/admin/posts')}}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
                loading,
            }).done(res => {
                const posts = res?.results?.data ?? [];
                if (posts?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadPost(dt);
                } else if (posts?.length === 0) {
                    _.$tbody.empty().append(genNoContentTable(COLSPAN, `Không có dữ liệu`));
                } else {
                    generatePosts(posts);
                    pagination.setTotalItems(res.results.total);
                }
            }).fail(err => {
            });
        }

        function generatePosts(posts) {
            let tbody = '';

            for (let i = 0; i < posts.length; i++) {
                const id = posts[i].id;

                let selfStatus = '';
                if (posts[i]?.self_status === 1)
                    selfStatus = `<span class="badge badge-success">Hiển thị</span>`;
                else selfStatus = `<span class="badge badge-danger">Ẩn</span>`;

                let adminStatus = '';
                if (posts[i]?.admin_status == `{{\App\Post::STATUS_ACCEPT}}`)
                    adminStatus = `<span class="badge badge-success">Không khóa</span>`;
                else adminStatus = `<span class="badge badge-danger">Bị khóa</span>`;

                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${posts[i]?.shop?.name ?? ''}</td>
                            <td class="text-center btn-show-detail-bisto btn-title-onshow" data-id=${id}>${posts[i]?.title ?? ''}</td>
                            <td class="text-center">${posts[i]?.created_at?.formatDate() ?? ''}</td>
                            <td class="text-center">${posts[i]?.description?.summary() ?? ''}</td>
                            <td class="text-center">${posts[i]?.count_like ?? 0}</td>
                            <td class="text-center">${posts[i]?.count_comment ?? 0}</td>
                            <td class="text-center">${selfStatus}</td>
                            <td class="text-center">${adminStatus}</td>
                            <td class="text-center">
                                <span type="button" data-id=${id} data-status="${posts[i]?.admin_status}" class="fas fa-key fa-lg text-warning btn-key-bisto-css btn-key-bisto"></span>
                            </td>
                        </tr>`;
            }
            _.$tbody.empty().append(tbody);
        }

        // $('.btn-show-cmt').on('click',function () {
        //     CMT.$commentContainer.collapse('toggle');
        // })
    </script>
@endsection
