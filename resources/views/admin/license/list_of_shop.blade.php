@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page license-admin">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách giấy phép kinh doanh
                </div>
                <div style="margin-top: 20px" class="row btn-bisto row-action-bisto-css">
                    <div class="col-sm-3" style="margin-left: 20px">
                        <div class="">
                            <select name="shop_id" class="form-control select-search-bisto-css">
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
                <div class="iq-card-body body-bisto">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Chuỗi cửa hàng / Cửa hàng</th>
                            <th class="text-center">Tên giấy phép</th>
                            <th class="text-center">Mô tả</th>
                            <th class="text-center">Hình ảnh</th>
                            <th class="text-center">Phê duyệt</th>
                        </tr>
                        </thead>

                        <tbody class="tbody-bisto">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <!-- ADD PRODUCT START -->
        <div id="modal-container">
            <div class="modal fade modal-bisto" tabindex="-1" role="dialog"
                 aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content" style="border-radius: 20px">
                        <div class="iq-card">
                            <header class="panel-heading title-modal-bisto">
                                Thêm giấy phép kinh doanh
                            </header>
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="iq-card-body">
                                        <div class="form-bisto" role="form">
                                            @csrf
                                            <div class="form-group">
                                                <label class="required-input">Tên giấy phép kinh doanh</label>
                                                <input type="text" name="name" class="form-control">
                                                <div class="mes-name mes-v-display">
                                                    Tên giấy phép là bắt buộc
                                                </div>
                                            </div>

                                            <div class="form-group" style="overflow: auto">
                                                <label class="required-input">Hình ảnh giấy phép</label><span
                                                    class="note-img-2-normal note-img"></span>
                                                <div class="custom-file">
                                                    <input type="file"
                                                           class="form-control-file custom-file-input input-file-bisto"
                                                           multiple>
                                                    <div id="img-container" class="row align-items-start">
                                                    </div>
                                                    <label class="custom-file-label" for="customFile">Chọn
                                                        ảnh</label>
                                                </div>
                                                <div class="mes-image_urls mes-v-display">
                                                    Bạn phải thêm ít nhất 1 ảnh
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Mô tả</label>
                                                <textarea class="form-control textarea-bisto" name="description"
                                                          placeholder="Mô tả giấy phép kinh doanh"></textarea>
                                            </div>
                                        </div>
                                        <div>
                                            <button class="btn btn-primary btn-add-bisto">
                                                Lưu
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-container">
        </div>
    </div>
    <script>
        let dt = {currentPage: 1, keyword: '', userId: 0, loading: {type: 'table', colspan: 6}};
        let _i;
        let _, $selectShop, pagination;

        $(document).ready(function () {
            _i = defineImage({id: 'license'});
            _ = defineBisto({
                parentSelector: '.license-admin', name: 'giấy phép kinh doanh',
            });
            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadLicense(dt);
                }
            });
            $selectShop = defineSearchSelect({id: 'admin_shop_and_shop', selectSelector: 'select[name=shop_id]'})
            $selectShop.onSelect((userId => {
                dt.userId = userId;
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadLicense(dt);
            }))
            loadLicense(dt);
            _.onDelete((id) => {
                $.ajax({
                    url: `{{url('/admin-shop/license')}}`,
                    method: 'DELETE',
                    data: {
                        _token: '{{csrf_token()}}'
                    }
                }).done(res => {
                    toastSuccess(res, 'Xóa thành công');
                    loadLicense(dt);
                }).fail(err => {
                })
            })

            _.onAccept((id) => {
                $.ajax({
                    url: `{{url('/admin/license/approve')}}/${id}`,
                    method: "PUT",
                    data: {
                        _token: '{{csrf_token()}}',
                    }
                }).done(res => {
                    toastSuccess(res, 'Chấp nhận thành công');
                    loadLicense({...dt, loading: {type: 'content'}});
                }).fail(err => {
                });
            })

            _.onReject((id) => {
                $.ajax({
                    url: `{{url('/admin/license/reject')}}/${id}`,
                    method: "PUT",
                    data: {
                        _token: '{{csrf_token()}}',
                    }
                }).done(res => {
                    toastSuccess(res, 'Từ chối thành công');
                    loadLicense({...dt, loading: {type: 'content'}});
                }).fail(err => {
                });
            })

            _.onQuery(() => {
                dt.userId = 0;
                loadLicense(dt);
            })
        });


        function loadLicense({currentPage = 1, keyword = '', userId = 0, loading = {type: 'table', colspan: 6}}) {
            $.ajax({
                url: `{{url('/admin/license/query')}}/${userId}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
                loading,
            }).done(res => {
                const lincenses = res?.results?.data;
                if (lincenses?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadLicense(dt);
                } else if (lincenses?.length === 0 || !lincenses) {
                    $('.table-striped>tbody').empty().append(genNoContentTable(6, `Không có giấy phép nào`));
                } else {
                    generateLicense(lincenses);
                    pagination.setTotalItems(res.results.total);
                }
            }).fail(err => {
            });
        }

        function generateLicense(licenses) {
            let tbody = '';
            _i.resetPhotos();
            for (let i = 0; i < licenses.length; i++) {
                const photos = licenses[i].image_urls.map(item => {
                    return {
                        src: item,
                        title: licenses[i].name
                    }
                })
                const id = licenses[i].id;
                const wait = `<div class="super-center"><span class="badge badge-warning">Đang chờ duyệt</span></div>
                <div class="d-flex justify-content-between">
                    <button type="button" class="text-danger btn btn-link btn-sm btn-reject-bisto" data-id=${id}>Từ chối</button>
                    <button type="button" class="text-success btn btn-link btn-sm btn-accept-bisto" data-id=${id}>Chấp nhận</button>
                </div>`

                let adminStatus = null;
                if (licenses[i].admin_status === 1) {
                    adminStatus = `<span class="badge badge-success">Chấp nhận</span>`
                } else if (licenses[i].admin_status === 0) {
                    adminStatus = wait;
                } else if (licenses[i].admin_status === -1) {
                    adminStatus = `<span class="badge badge-danger">Từ chối</span>`
                }

                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${licenses[i].shop_name}</td>
                            <td class="text-center">${licenses[i].name}</td>
                            <td class="text-center">${licenses[i].description}</td>
                            <td class="text-center">${_i.toMultiHTML(photos)}</td>
                            <td class="text-center">${adminStatus}</td>
                        </tr>`;
            }
            _.$tbody.empty().append(tbody);
            _i.animateOnloadImage({});
        }
    </script>
@endsection
