@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách Banners chuỗi cửa hàng
                </div>

                <div class="row row-action-bisto-css">
                    <div class="col-sm-5">

                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search"
                                   onkeydown="searchBanner()"
                                   placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm btn-search-bisto-css form-control"
                                   name="input-text-banner">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Chuỗi cửa hàng</th>
                            <th class="text-center">Tên Banner</th>
                            <th class="text-center">Hình ảnh</th>
                            <th class="text-center">Vị trí banner</th>
                            <th class="text-center">Ngày bắt đầu</th>
                            <th class="text-center">Ngày kết thúc</th>
                            <th class="text-center">Hiển thị</th>
                            <th class="text-center">Phê duyệt</th>
                        </tr>
                        </thead>

                        <tbody id="tbl-slider">
                        </tbody>
                    </table>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div style="" id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
                <!-- footer and pagination -->
            </div>
        </div>
    </div>
    <script type="text/javascript">
        let dt = {currentPage: 1, searchKey: '', loading: {colspan: 9, type: 'table'}}, pagination, _i;
        $(document).ready(function () {
            _i = defineImage({id: 'banner'});
            loadBanners(dt);
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadBanners(dt);
                }
            });
        });


        //call api get product by paging and generate on interface
        function loadBanners({currentPage = 1, searchKey = '', loading = {colspan: 9, type: 'table'}}) {
            let _url = `admin-shop/banners?page=${currentPage}&keyword=${searchKey}`;
            $.ajax({
                url: _url,
                method: 'GET',
                loading
            })
                .done(res => {
                    let banners = res?.banners?.data ?? [];
                    if (banners.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadBanners(dt);
                    } else if (banners.length === 0) {
                        $('.table-striped>tbody').empty().append(`<tr><td class="text-center" colspan="9">Không có banner nào</td></tr>`);
                    } else {
                        generateBanners(banners);
                        pagination.setTotalItems(res.banners.total);
                    }
                })
                .fail(err => {
                    toastErrorMessage(err, 'Không thể load banner');
                });
        }

        function generateBanners(banners) {
            let tbody = '';
            _i.resetPhotos();
            for (let i = 0; i < banners.length; i++) {
                const id = banners[i].banner_id;
                const wait = `<div class="super-center"><span class="badge badge-warning">Đang chờ duyệt</span></div>
                <div class="d-flex justify-content-between">
                    <button type="button" class="text-danger btn btn-link btn-sm btn-reject btn-reject-bisto-css" bannerid=${id}>Từ chối</button>
                    <button type="button" class="text-success btn btn-link btn-sm btn-accept btn-accept-bisto-css" bannerid=${id}>Chấp nhận</button>
                </div>`

                let adminStatus = null;
                if (banners[i].admin_status === 1) {
                    adminStatus = `<span class="badge badge-success">Chấp nhận</span>`
                } else if (banners[i].admin_status === 0) {
                    adminStatus = wait;
                } else if (banners[i].admin_status === -1) {
                    adminStatus = `<span class="badge badge-danger">Từ chối</span>`
                }
                let locationString = '';
                if (banners[i].position === 1) {
                    locationString = 'Giữa trang chủ'
                } else if (banners[i].position === 2) {
                    locationString = 'Dưới trang chủ'
                } else if (banners[i].position === 3) {
                    locationString = 'Đầu tab khám phá'
                }
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${banners[i].shop_name}</td>
                            <td class="text-center">${banners[i].banner_name}</td>
                            <td class="text-center">${_i.toHTML(banners[i].source_urls[0], banners[i].banner_name)}</td>
                            <td class="text-center">${locationString}</td>
                            <td class="text-center">${banners[i].start_at.formatDate()}</td>
                            <td class="text-center">${banners[i].end_at.formatDate()}</td>
                            <td class="text-center">
                            ${banners[i].self_status == 1 ? `<span bannerid=${id} class="btn-unactive badge badge-success">Kích hoạt</span>` : `<span bannerid=${id} class="btn-active badge badge-danger">Ẩn</span>`}
                            </td>
                            <td class="text-center">${adminStatus}</td>
                        </tr>`;
            }
            $('.table-striped>tbody').empty().append(tbody);
            _i.animateOnloadImage({});
        }

        //accept
        $(document).on('click', '.btn-accept', function (e) {
                const bannerId = e.target.getAttribute('bannerid');
                const _url = 'banners/accept/' + bannerId;
                $.ajax(
                    {
                        url: _url,
                        method: 'PUT',
                        data: {
                            _token: "{{csrf_token()}}",
                        }
                    }
                ).done(res => {
                    toastSuccessMessage('Thay đổi trạng thái chấp nhận banner thành công');
                    loadBanners({...dt, loading: {type: 'content'}});
                }).fail(err => {
                    toastErrorMessage('Thay đổi banner thất bại')
                })
            }
        )

        //reject
        $(document).on('click', '.btn-reject', function (e) {
                const bannerId = e.target.getAttribute('bannerid');
                $.ajax(
                    {
                        url: `{{url('/admin')}}/banners/reject/${bannerId}`,
                        method: 'PUT',
                        data: {
                            _token: "{{csrf_token()}}",
                        }
                    }
                ).done(res => {
                    toastSuccessMessage('Thay đổi trạng thái từ chối banner thành công');
                    loadBanners({...dt, loading: {type: 'content'}});
                }).fail(err => {
                    toastErrorMessage('Thay đổi banner thất bại')
                })
            }
        )

        function searchBanner() {
            // user press enter search
            if (event.key === 'Enter') {
                dt.searchKey = $('input[name=input-text-banner]').val();
                loadBanners(dt);
            }
        }
    </script>

@endsection
