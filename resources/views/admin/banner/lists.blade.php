@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách Banners Admin
                </div>

                <div class="row row-action-bisto-css">
                    <div class="col-sm-5">
                        <span
                            class="btn btn-show-add-bisto-css btn-show-modal-banner btn-primary">+ Thêm
                            banner</span>
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search"
                                   onkeydown="searchBanner()"
                                   placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm btn-search-bisto-css form-control"
                                   name="input-text-banner">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Tên Banner</th>
                            <th class="text-center">Hình ảnh</th>
                            <th class="text-center">Vị trí banner</th>
                            <th class="text-center">Ngày bắt đầu</th>
                            <th class="text-center">Ngày kết thúc</th>
                            <th class="text-center">Hiển thị</th>
                            <th class="text-center">Phê duyệt</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody id="tbl-slider">
                        </tbody>
                    </table>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div style="" id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
                <!-- footer and pagination -->
            </div>
        </div>
    </div>
    <!-- ADD BANNERS -->
    <div class="modal fade add-banner" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content" style="border-radius: 20px">
                <div class="iq-card">
                    <div class="title-banner panel-heading">
                        Đăng ký banner
                    </div>
                    <div class="iq-card-body">
                        <div id="banners-form">
                            <div class="form-group">
                                <label>Tên banners<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="name_banner" placeholder="Tên banners"
                                       name="name">
                                <div class="mes-invalid mes-name">
                                </div>
                            </div>
                            <div class="form-group" style="overflow: auto">
                                <label class="required-input">Hình ảnh cho banner</label><span
                                    class="note-img note-img-10-13"></span>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input"
                                           name="banner-img">
                                    <div id="img-container" class="row align-items-start"></div>
                                    <label class="custom-file-label">Chọn ảnh</label>
                                </div>
                                <div class="mes-imageUrl mes-invalid">
                                    Bạn phải thêm ít nhất 1 ảnh
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Vị trí banner</label>
                                <select class="form-control" name="position">
                                    <option value="1" selected>Giữa trang chủ</option>
                                    <option value="2">Dưới trang chủ</option>
                                    <option value="3">Đầu tab khám phá</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label>Ngày bắt đầu</label>
                                    <input type="datetime-local" class="form-control" id="start_at"
                                           placeholder="Ngày bắt đầu"
                                           name="start_at">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label>Ngày kết thúc</label>
                                    <input type="datetime-local" class="form-control" id="end_at"
                                           placeholder="Ngày kết thúc"
                                           name="end_at">
                                </div>
                            </div>
                            <div class="mes-invalid mes-rangedate">
                                Ngày bắt đầu phải nhỏ hơn ngày kết thúc
                            </div>
                            <div class="form-group">
                                <label>Trạng thái hiển thị</label>
                                <select class="form-control" name="self_status">
                                    <option value="1" selected>Kích hoạt</option>
                                    <option value="-1">Ẩn</option>
                                </select>
                            </div>
                            <div class="form-group info-admin-status">
                                <label>Trạng thái phê duyệt</label>
                                <select class="form-control" name="admin_status">
                                    <option value="1">Chấp nhận</option>
                                    <option value="-1">Từ chối</option>
                                </select>
                            </div>
                            <input hidden id="source-url" type="text"/>
                            <button id="btn-save" type="submit"
                                    class="btn btn-primary" name="add_banner">Đăng ký banner
                            </button>
                            <button id="btn-update" type="submit"
                                    class="btn btn-primary" name="update_banner">Cập nhật banner
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        let dt = {currentPage: 1, searchKey: '', loading: {type: 'table', colspan: 9}}
        let cropperBanner;
        let uploaderBanner, pagination, _i;
        $(document).ready(function () {
            loadBanners(dt);
            setInputDateTime('start_at');
            _i = defineImage({id: 'banner'});
            setInputDateTime('end_at');
            $('#img-container').empty();
            let _totalFiles = 0;
            uploaderBanner = defineUploadFile({id: 'banner', containerSelector: '#img-container'})
            cropperBanner = defineCropAndUploadImage({type: 'banner', inputSelector: 'input[name=banner-img]'});
            cropperBanner.saveButton.addEventListener('click', function () {
                _totalFiles = uploaderBanner.getTotalFile() + 1;
                const {fileElement} = uploaderBanner.createFileElement(cropperBanner.getImageUrl(), cropperBanner.getImgName());
                uploaderBanner.getFileUrls().push(cropperBanner.getImg())
                uploaderBanner.setTotalFile(_totalFiles);
                uploaderBanner.getContainer().append(fileElement);
                cropperBanner.$modal.modal('hide');
            });
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadBanners(dt);
                }
            });
        });

        //Clear form data input
        function clearFormData() {
            setInputDateTime('start_at');
            setInputDateTime('end_at');
            $('input[name=name]').val('');
            $('select[name=position]').val('1');
            $('select[name=self_status]').val('1').prop('disabled', true);
            $('#img-container').empty();
            uploaderBanner.setTotalFile(0);
            uploaderBanner.setFileUrls([]);
            $('.title-banner').empty().text('Đăng ký banner');
            $('select[name=admin_status]').val('1').attr('disabled', true);
        }

        $(document).on('click', '.btn-show-modal-banner', function () {
            clearFormData();
            $('.modal.add-banner').modal('show');
            $('button[name=add_banner]').show();
            $('button[name=update_banner]').hide();
        })

        //call api get product by paging and generate on interface
        function loadBanners({currentPage = 1, searchKey = '', loading = {type: 'table', colspan: 9}}) {
            $.ajax({
                url: `{{url('/admin')}}/banners?page=${currentPage}&keyword=${searchKey}`,
                method: 'GET',
                loading
            })
                .done(res => {
                    let banners = res?.banners?.data ?? [];
                    if (banners.length === 0 && dt.currentPage > 1) {
                        dt.currentPage = 1;
                        pagination.setCurrentPage(1);
                        loadBanners(dt);
                    } else if (banners.length === 0) {
                        $('.table-striped>tbody').empty().append(`<tr><td class="text-center" colspan="9">Không có banner nào</td></tr>`);
                    } else {
                        generateBanners(banners);
                        pagination.setTotalItems(res.banners.total);
                    }
                })
                .fail(err => {
                    toastError(err, 'Không thể load banner');
                });
        }

        $('#btn-save').on('click', function (e) {
            let dataObj = formToData('#banners-form');
            if (validateFormBanner()) {
                $.ajax({
                    url: "{{url('/admin/banners')}}",
                    method: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        ...dataObj,
                        source_urls: uploaderBanner.getMultiImg(),
                        admin_status: 1,
                    },
                })
                    .done(res => {
                        toastSuccess(res, "Thêm mới banner thành công");
                        $('.add-banner').modal('hide');
                        loadBanners({...dt, loading: {type: 'content'}});
                    })
                    .fail(err => {
                        toastError(err, 'Không thể đăng ký banner')
                    })
            }
        });

        $('#btn-update').click(function (e) {
            let dataObj = formToData('#banners-form');
            const id = $('.modal.add-banner').data('id');

            if (validateFormBanner()) {
                $.ajax({
                    url: `banners/${id}`,
                    method: 'PUT',
                    data: {
                        _token: "{{csrf_token()}}",
                        ...dataObj,
                        source_urls: uploaderBanner.getNewImgs(),
                        deleted_imageids: uploaderBanner.getDeleteImageIds(),
                        admin_status: $('select[name=admin_status]').val(),
                    },
                    dataType: 'json',
                })
                    .done(res => {
                        $(".modal.add-banner").modal('hide');
                        toastSuccessMessage("Cập nhật banner thành công");
                        loadBanners({...dt, loading: {type: 'content'}});
                    })
                    .fail(err => {
                        toastError(err, 'Cập nhật thất bại');
                    })
            }
        });

        $(document).on('click', '.btn-show-modal-update', function (e) {
                $('button[name=add_banner]').hide();
                $('button[name=update_banner]').show();
                $('.title-banner').empty().text('Cập nhật banner');
                const bannerId = e.target.getAttribute('bannerid');
                let _url = `banners/${bannerId}`;
                $.ajax({
                    url: _url,
                    method: 'GET',
                }).done(res => {
                    const banner = res.banner || {};
                    $('.modal.add-banner').data('id', bannerId);
                    $('input[name=name]').val(banner.name);
                    $('select[name=position]').val(banner.position);
                    let _fileUrls = [];
                    const _totalFiles = banner.source_urls.length;
                    const image_ids = banner.image_ids.split(',');
                    $('.modal.add-banner').data('imageids', image_ids);
                    $('#img-container').empty();
                    for (let i = 0; i < _totalFiles; i++) {
                        const {fileElement} = uploaderBanner.createFileElement(banner.source_urls[i], image_ids[i]);
                        _fileUrls.push({name: image_ids[i], url: banner.source_urls[i]});
                        $('#img-container').append(fileElement);
                    }
                    uploaderBanner.setTotalFile(_totalFiles);
                    uploaderBanner.setFileUrls(_fileUrls);
                    setInputDateTime('start_at', new Date(banner.start_at));
                    setInputDateTime('end_at', new Date(banner.end_at));
                    $('select[name=admin_status]').val(banner.admin_status).attr('disabled', false);
                    $('select[name=self_status]').val(banner.self_status).prop('disabled', false);

                    $('.modal.add-banner').modal('show');
                }).fail(err => {
                    toastErrorMessage('Load banner thất bại');
                });
            }
        )

        function generateBanners(banners) {
            let tbody = '';
            _i.resetPhotos();
            for (let i = 0; i < banners.length; i++) {
                const id = banners[i].id;
                let adminStatus = null;
                if (banners[i].admin_status === 1) {
                    adminStatus = `<span class="badge badge-success btn-reject btn-badge" type="button" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Nhấn để ẩn từ chối banner" bannerid=${id}>Chấp nhận</span>`
                } else if (banners[i].admin_status === 0) {
                    adminStatus = `<span class="badge badge-warning btn-accept" type="button" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Nhấn để chấp nhận banner" bannerid=${id}>Đang chờ duyệt</span>`
                } else if (banners[i].admin_status === -1) {
                    adminStatus = `<span class="badge badge-danger btn-accept btn-badge" type="button" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Nhấn để chấp nhận banner" bannerid=${id}>Từ chối</span>`
                }
                let locationString = '';
                if (banners[i].position == 1) {
                    locationString = 'Giữa trang chủ'
                } else if (banners[i].position == 2) {
                    locationString = 'Dưới trang chủ'
                } else if (banners[i].position == 3) {
                    locationString = 'Đầu tab khám phá'
                }
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${banners[i].name}</td>
                            <td class="text-center">${_i.toHTML(banners[i].source_urls[0], banners[i].name)}</td>
                            <td class="text-center">${locationString}</td>
                            <td class="text-center">${banners[i].start_at.formatDate()}</td>
                            <td class="text-center">${banners[i].end_at.formatDate()}</td>
                            <td class="text-center">
                            ${banners[i].self_status == 1 ? `<span bannerid=${id} type="button" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Nhấn để ẩn banner" class="btn-badge btn-unactive badge badge-success">Kích hoạt</span>` : `<span type="button" bannerid=${id} data-bs-toggle="tooltip" data-bs-placement="bottom" title="Nhấn để kích hoạt banner" class="btn-badge btn-active badge badge-danger">Ẩn</span>`}
                            </td>
                            <td class="text-center">${adminStatus}</td>
                            <td class="text-center">
                                <span bannerid=${id} class="fas fa-edit fa-lg text-success styling-edit btn-show-modal-update btn-show-update-bisto-css" data-toggle="modal" data-target=".edit-banner"></span>
                                <span type="button" bannerid=${id} class="fa-lg styling-edit btn-delete fa fa-times text-danger btn-delete-bisto-css" ></span>
                            </td>
                        </tr>`;
            }
            $('.table-striped>tbody').empty().append(tbody);
            _i.animateOnloadImage({});
        }

        function validateFormBanner() {
            return (validateInput({
                listValidateFunction: [
                    {
                        func: requiredValidation,
                        mes: 'Tên banner là bắt buộc',
                    }
                ], propertyName: 'name'
            }) && validateInput({
                listValidateFunction: [{
                    func: requiredElementValidation,
                    mes: 'Bạn phải thêm ít nhất một ảnh',
                    params: {classNameElement: 'input[name=imageUrl]'}
                }], type: 'custom', customInput: 'input[name=imageUrl]', customMes: '.mes-imageUrl'
            }) && validateRangeDate('input[name=start_at]', 'input[name=end_at]', '.mes-rangedate'))
        }


        $(document).on('click', '.btn-delete', function (e) {
            let bannerId = e.target.getAttribute('bannerid');
            confirmDelete("Bạn có chắc muốn xóa banner này không?", function () {
                let _url = `banners/` + bannerId
                $.ajax(
                    {
                        url: _url,
                        method: 'DELETE',
                        data: {
                            _token: "{{csrf_token()}}"
                        }
                    }
                ).done(res => {
                    toastSuccessMessage('Xóa banner thành công');
                    loadBanners({...dt, loading: {type: 'content'}});
                }).fail(err => {
                    toastErrorMessage('Xóa thất bại');
                })
            })
        });

        //unactive
        $(document).on('click', '.btn-unactive', function (e) {
                const bannerId = e.target.getAttribute('bannerid');
                const _url = 'banners/un-active/' + bannerId;
                $.ajax(
                    {
                        url: _url,
                        method: 'PUT',
                        data: {
                            _token: "{{csrf_token()}}",
                        }
                    }
                ).done(res => {
                    toastSuccess(res, 'Ẩn banner thành công');
                    loadBanners({...dt, loading: {type: 'content'}});
                }).fail(err => {
                    toastErrorMessage('Ẩn banner thất bại')
                })
            }
        )
        //active
        $(document).on('click', '.btn-active', function (e) {
                const bannerId = e.target.getAttribute('bannerid');
                const _url = 'banners/active/' + bannerId;
                $.ajax(
                    {
                        url: _url,
                        method: 'PUT',
                        data: {
                            _token: "{{csrf_token()}}",
                        }
                    }
                ).done(res => {
                    toastSuccessMessage('Kích hoạt banner thành công');
                    loadBanners({...dt, loading: {type: 'content'}});
                }).fail(err => {
                    toastErrorMessage('Kích hoạt banner thất bại')
                })
            }
        )

        //accept
        $(document).on('click', '.btn-accept', function (e) {
                const bannerId = e.target.getAttribute('bannerid');
                const _url = 'banners/accept/' + bannerId;
                $.ajax(
                    {
                        url: _url,
                        method: 'PUT',
                        data: {
                            _token: "{{csrf_token()}}",
                        }
                    }
                ).done(res => {
                    toastSuccessMessage('Thay đổi trạng thái chấp nhận banner thành công');
                    loadBanners({...dt, loading: {type: 'content'}});
                }).fail(err => {
                    toastErrorMessage('Thay đổi banner thất bại')
                })
            }
        )

        //reject
        $(document).on('click', '.btn-reject', function (e) {
                const bannerId = e.target.getAttribute('bannerid');
                const _url = 'banners/reject/' + bannerId;
                $.ajax(
                    {
                        url: _url,
                        method: 'PUT',
                        data: {
                            _token: "{{csrf_token()}}",
                        }
                    }
                ).done(res => {
                    toastSuccessMessage('Thay đổi trạng thái từ chối banner thành công');
                    loadBanners({...dt, loading: {type: 'content'}});
                }).fail(err => {
                    toastErrorMessage('Thay đổi banner thất bại')
                })
            }
        )

        function searchBanner() {
            // user press enter search
            if (event.key === 'Enter') {
                dt.searchKey = $('input[name=input-text-banner]').val();
                pagination.setCurrentPage(1);
                dt.currentPage = 1;
                loadBanners(dt);
            }
        }
    </script>

@endsection
