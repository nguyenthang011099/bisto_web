@extends('admin.layout')
@section('admin_content')
    <?php
    $ID = 'blog';
    ?>
    <div id="content-page" class="content-page blog-admin">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách blog
                </div>
                <div style="margin-top: 20px" class="row btn-bisto row-action-bisto-css">
                    <div class="col-sm-6">
                        <button class="btn-show-add-bisto btn-show-add-bisto-css btn btn-primary">+ Thêm blog
                        </button>
                    </div>
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search"
                                   placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm btn-search-bisto btn-search-bisto-css form-control">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body body-bisto">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Tiêu đề</th>
                            <th class="text-center">Ngày tạo</th>
                            <th class="text-center">Danh mục</th>
                            <th class="text-center">Thể loại</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody class="tbody-bisto">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <div id="test-ck">

                </div>
                <!-- PAGINATION END-->
            </div>

            <div class="iq-card">
                <div class="panel-heading" id="form-blog-title">
                    Thêm blog
                </div>

                <div class="iq-card-body">
                    <section class="panel">
                        <div class="panel-body">
                            <div class="iq-card-body">
                                <div class="form-bisto" role="form" id="form-{{$ID}}">
                                    @csrf
                                    <div class="form-group">
                                        <label class="required-input">Tiêu đề</label>
                                        <input type="text" name="title" class="form-control">
                                        <div class="mes-title mes-v-display">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="required-input">Mô tả</label>
                                        <textarea style="resize: none;height: 150px" rows="8"
                                                  class="form-control"
                                                  name="description"
                                                  placeholder=""></textarea>
                                        <div class="mes-description mes-v-display"></div>
                                    </div>
                                    <div class="form-group" style="overflow: auto">
                                        <label class="required-input">Hình ảnh</label><span
                                            class="note-img note-img-1-normal"></span>
                                        <div class="custom-file">
                                            <input type="file"
                                                   class="form-control-file custom-file-input inputfile"
                                                   id="img-{{$ID}}" multiple>
                                            <div id="img-container-{{$ID}}" class="row align-items-start">
                                            </div>
                                            <label class="custom-file-label" for="customFile">Chọn
                                                ảnh</label>
                                        </div>
                                        <div class="mes-image_url mes-v-display">
                                            Bạn phải thêm ít nhất 1 ảnh
                                        </div>
                                    </div>

                                    {{--                                    <div class="form-group">--}}
                                    {{--                                        <label class="required-input">Loại</label>--}}
                                    {{--                                        <select name="type"--}}
                                    {{--                                                class="form-control input-sm m-bot15">--}}
                                    {{--                                            <option value="{{\App\Blog::TYPE_NORMAL }}" selected>Tin bình thường--}}
                                    {{--                                            </option>--}}
                                    {{--                                            <option value="{{\App\Blog::TYPE_HOT }}">Tin hot</option>--}}
                                    {{--                                        </select>--}}
                                    {{--                                        <div class="mes-type mes-v-display">--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}

                                    <div class="form-group">
                                        <label class="required-input">Loại</label>
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input" name="type"
                                                   id="type-switch">
                                            <label class="custom-control-label type-label" for="type-switch">Tin
                                                thường</label>
                                        </div>
                                        <div class="mes-type mes-v-display">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="required-input">Danh mục</label>
                                        <select name="blog_category_id"
                                                class="form-control input-sm m-bot15">

                                        </select>
                                        <div class="mes-blog_category_id mes-v-display">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="required-input">Tag</label>
                                        <div class="tag-container" id="tag-container">
                                            <div class="tag-item-container">

                                                <div class="tag-item">
                                                    <div class="text-too-long">halolo</div>
                                                    <div class="tag-action">
                                                        <i class="fa fa-times"></i>
                                                    </div>
                                                </div>

                                            </div>
                                            <input placeholder="Nhập tag" value="" id="input-tag"/>

                                        </div>
                                        <div class="mes-tag_ids mes-v-display">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="required-input">Nội dung</label>
                                        <div class="" id="">

                                        </div>
                                        <div class="" id="blog-content-editor">

                                        </div>
                                        <div class="mes-content mes-v-display">
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <button class="btn btn-primary btn-add-bisto btn-add-bisto-css">
                                        Thêm blog
                                    </button>
                                    <button class="btn btn-primary btn-update-bisto btn-update-bisto-css">
                                        Cập nhật blog
                                    </button>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

        </div>
        <!-- ADD START -->
        {{--        <div id="modal-container">--}}
        {{--            <div class="modal fade modal-bisto" id="form-{{$ID}}" tabindex="-1" role="dialog"--}}
        {{--                 aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">--}}
        {{--                <div class="modal-dialog modal-xl">--}}
        {{--                    <div class="modal-content" style="border-radius: 20px">--}}
        {{--                        <div class="iq-card">--}}
        {{--                            <header class="panel-heading title-modal-bisto">--}}
        {{--                                Thêm blog--}}
        {{--                            </header>--}}
        {{--                            <section class="panel">--}}
        {{--                                <div class="panel-body">--}}
        {{--                                    <div class="iq-card-body">--}}
        {{--                                        <div class="form-bisto" role="form">--}}
        {{--                                            @csrf--}}
        {{--                                            <div class="form-group">--}}
        {{--                                                <label class="required-input">Tiêu đề</label>--}}
        {{--                                                <input type="text" name="title" class="form-control">--}}
        {{--                                                <div class="mes-title mes-v-display">--}}
        {{--                                                </div>--}}
        {{--                                            </div>--}}
        {{--                                            <div class="form-group" style="overflow: auto">--}}
        {{--                                                <label class="required-input">Hình ảnh</label><span--}}
        {{--                                                    class="note-img note-img-1-11"></span>--}}
        {{--                                                <div class="custom-file">--}}
        {{--                                                    <input type="file"--}}
        {{--                                                           class="form-control-file custom-file-input inputfile"--}}
        {{--                                                           id="img-{{$ID}}" multiple>--}}
        {{--                                                    <div id="img-container-{{$ID}}" class="row align-items-start">--}}
        {{--                                                    </div>--}}
        {{--                                                    <label class="custom-file-label" for="customFile">Chọn--}}
        {{--                                                        ảnh</label>--}}
        {{--                                                </div>--}}
        {{--                                                <div class="mes-image_url mes-v-display">--}}
        {{--                                                    Bạn phải thêm ít nhất 1 ảnh--}}
        {{--                                                </div>--}}
        {{--                                            </div>--}}
        {{--                                            <div class="form-group">--}}
        {{--                                                <label class="required-input">Danh mục</label>--}}
        {{--                                                <select name="blog_category_id"--}}
        {{--                                                        class="form-control input-sm m-bot15">--}}

        {{--                                                </select>--}}
        {{--                                                <div class="mes-blog_category_id mes-v-display">--}}
        {{--                                                </div>--}}
        {{--                                            </div>--}}
        {{--                                            <div class="form-group">--}}
        {{--                                                <label class="required-input">Tag</label>--}}
        {{--                                                <div class="tag-container" id="tag-container">--}}
        {{--                                                    <div class="tag-item-container">--}}

        {{--                                                        <div class="tag-item">--}}
        {{--                                                            <div class="text-too-long">halolo</div>--}}
        {{--                                                            <div class="tag-action">--}}
        {{--                                                                <i class="fa fa-times"></i>--}}
        {{--                                                            </div>--}}
        {{--                                                        </div>--}}

        {{--                                                    </div>--}}
        {{--                                                    <input placeholder="Nhập tag" value="" id="input-tag"/>--}}

        {{--                                                </div>--}}
        {{--                                                <div class="mes-tag_ids mes-v-display">--}}
        {{--                                                </div>--}}
        {{--                                            </div>--}}
        {{--                                            <div class="form-group">--}}
        {{--                                                <label class="required-input">Nội dung</label>--}}
        {{--                                                <div class="" id="">--}}

        {{--                                                </div>--}}
        {{--                                                <div class="" id="content-editor">--}}

        {{--                                                </div>--}}
        {{--                                                <div class="mes-content mes-v-display">--}}
        {{--                                                </div>--}}
        {{--                                            </div>--}}
        {{--                                        </div>--}}
        {{--                                        <div>--}}
        {{--                                            <button class="btn btn-primary btn-add-bisto btn-add-bisto-css">--}}
        {{--                                                Thêm blog--}}
        {{--                                            </button>--}}
        {{--                                            <button class="btn btn-primary btn-update-bisto btn-update-bisto-css">--}}
        {{--                                                Cập nhật blog--}}
        {{--                                            </button>--}}
        {{--                                        </div>--}}
        {{--                                    </div>--}}
        {{--                                </div>--}}
        {{--                            </section>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
        {{--        --}}
    </div>
    <script>
        const COLSPAN = 6;
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: COLSPAN}};
        let _, pagination, _tag;
        let uploaderImage;
        let ckeditor;
        const $formTitle = $('#form-blog-title');
        let oldImageId = null;
        let $typeInput = $('input[name=type]');

        const TYPE = {
            normal: '{{\App\Blog::TYPE_NORMAL}}',
            hot: '{{\App\Blog::TYPE_HOT}}',
        }

        $typeInput.on('change', function (e) {
            const checked = e.currentTarget.checked;
            setType(checked)
        })

        function getType() {
            if ($typeInput.attr('checked')) {
                return TYPE.hot
            } else {
                return TYPE.normal
            }
        }

        function setType(checked) {
            $typeInput.attr('checked', checked);
            if (checked) {
                $('.type-label').text('Tin hot')
            } else {
                $('.type-label').text('Tin thường')
            }
        }

        $(document).ready(function () {
            _ = defineBisto({
                parentSelector: '.blog-admin', name: 'blog',
            });
            _.$btnUpdate.hide();
            _i = defineImage({id: '{{$ID}}'});
            _tag = defineTag({containerSelector: '#tag-container'});
            ckeditor = createCkeditor5({selector: '#blog-content-editor'})

            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadBlog(dt);
                }
            });

            uploaderImage = defineUploadFile({
                id: '{{$ID}}',
                containerSelector: '#img-container-{{$ID}}', imageName: 'image_url', maxFiles: 1,
                cropperOption: {inputSelector: '#img-{{$ID}}'}
            })

            loadBlog(dt);
            loadBlogCategory({});

            _.onSearch((value) => {
                dt.keyword = value;
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadBlog(dt);
            });

            _.onShowAdd(() => {
                _tag.setTagItemIds([]);
                ckeditor.getEditor().setData('');
                $formTitle.text('Thêm blog');

                dataToForm({
                    fieldInputs: [
                        {selector: 'input[name=title]'},
                        {selector: 'textarea[name=description]'},
                        // {selector: 'select[name=blog_category_id]'},
                    ],
                    fieldFiles: [{
                        uploaderInstance: uploaderImage,
                        listFiles: [],
                    }],
                    parentSelector: '.form-bisto', action: 'clear-validate'
                })
            })

            _.onAdd((data) => {
                if (validateFormBlog())
                    $.ajax({
                        url: `{{url('/admin/blogs')}}`,
                        method: 'POST',
                        data: {
                            ...data,
                            type: getType(),
                            blog_category_id: parseInt(data?.blog_category_id),
                            content: ckeditor.getEditor().getData(),
                            slug: convertToSlug(data?.title, `-${randomString(5)}`),
                            image: uploaderImage.getOneImg(),
                            tag_ids: _tag.getTagItemIds() ?? []
                        }
                    }).done(res => {
                        toastSuccess(res, 'Thêm blog thành công');
                        loadBlog({...dt, loading: {type: 'content'}});
                        _.$modalAdd.modal('hide');
                    }).fail(err => {
                        _.$modalAdd.modal('hide');
                    })
            })

            _.onShowUpdate((id, cbShowModalUpdate) => {
                $.ajax({
                    url: `{{url('/admin/blogs')}}/${id}`,
                    method: 'GET',
                    loading: {type: 'content'}
                }).done(res => {
                    const blog = res?.result ?? {title: '', created_at: null};
                    oldImageId = blog?.image?.id;
                    if (blog?.type == TYPE.hot) {
                        setType(true)
                    } else {
                        setType(false)
                    }
                    dataToForm({
                        fieldInputs: [
                            {selector: 'input[name=title]', value: blog?.title},
                            // {selector: 'select[name=type]', value: blog?.type},
                            {selector: 'textarea[name=description]', value: blog?.description},
                            {selector: 'select[name=blog_category_id]', value: blog?.blog_category_id},
                        ],
                        fieldFiles: [{
                            uploaderInstance: uploaderImage,
                            listFiles: [{name: blog?.image?.filename, url: blog?.image?.url}],
                        }],
                        parentSelector: '.form-bisto', action: 'clear-validate'
                    })

                    const tagItemIds = blog?.tags?.map(item => item?.id);
                    _tag.setTagItemIds(tagItemIds);
                    ckeditor.getEditor().setData(blog?.content)
                    $formTitle.text('Chỉnh sửa blog');
                    cbShowModalUpdate();
                }).fail(err => {
                })
            })

            _.onUpdate((id, data) => {
                if (validateFormBlog())
                    $.ajax({
                        url: `{{url('/admin/blogs')}}/${id}`,
                        method: 'PUT',
                        loading: {type: 'content'},
                        data: {
                            ...data,
                            type: getType(),
                            blog_category_id: parseInt(data?.blog_category_id),
                            content: ckeditor.getEditor().getData(),
                            slug: null,//convertToSlug(data?.title),
                            image_id: oldImageId,
                            image: uploaderImage._isNewUpdateImg() ? uploaderImage.getOneImg() : null,
                            tag_ids_delete: _tag.getTagIdsDelete(),
                            tag_ids_create: _tag.getTagIdsCreate()
                        }
                    }).done(res => {
                        toastSuccess(res, 'Cập nhật blog thành công');
                        loadBlog({...dt, loading: {type: 'content'}});
                        _.$modalUpdate.modal('hide');
                    }).fail(err => {
                        _.$modalUpdate.modal('hide');
                    })
            })

            _.onDelete((id) => {
                $.ajax({
                    url: `{{url('/admin/blogs')}}/${id}`,
                    method: 'DELETE',
                    data: {
                        _token: '{{csrf_token()}}',
                    }
                }).done(res => {
                    toastSuccess(res, 'Xóa blog thành công');
                    loadBlog({...dt, loading: {type: 'content'}});
                    _.$modalUpdate.modal('hide');
                }).fail(err => {
                    _.$modalUpdate.modal('hide');
                })
            })
        });

        function validateFormBlog() {
            function validateTags() {
                if (_tag.getTagItemIds()?.length == 0) {
                    toastInfoMessage('Bạn phải thêm ít nhất 1 tag')
                    return false
                } else return true
            }

            function validateContent() {
                if (ckeditor.getEditor().getData().length == 0) {
                    toastInfoMessage('Bạn phải thêm nội dung')
                    return false
                } else return true
            }

            return validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên blog là bắt buộc'
                    }],
                    propertyName: 'title'
                }) && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Mô tả là bắt buộc',
                    }], type: 'textarea', propertyName: 'description'
                })
                && validateRequiredElement({
                    inputSelector: 'input[name=image_url]',
                    mesSelector: '.mes-image_url',
                    message: 'Bạn phải thêm ít nhất một ảnh', parentSelector: '#form-{{$ID}}'
                }) && validateTags() && validateContent()
        }

        function loadBlog({currentPage = 1, keyword = '', loading = {type: 'table', colspan: COLSPAN}}) {
            $.ajax({
                url: `{{url('/admin/blogs')}}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
                loading,
            }).done(res => {
                const blogs = res?.results?.data ?? [];
                if (blogs?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadBlog(dt);
                } else if (blogs?.length === 0) {
                    _.$tbody.empty().append(genNoContentTable(COLSPAN, `Không có dữ liệu`));
                } else {
                    generateBlogs(blogs);
                    pagination.setTotalItems(res.results.total);
                }
            }).fail(err => {
            });
        }

        function getTypeString(type) {
            switch (type) {
                case '{{\App\Blog::TYPE_NORMAL}}':
                    return 'Tin bình thường'
                case '{{\App\Blog::TYPE_HOT}}':
                    return 'Tin hot'
                default:
                    return ''
            }
        }

        function generateBlogs(blogs) {
            let tbody = '';

            for (let i = 0; i < blogs.length; i++) {
                const id = blogs[i].id;
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${blogs[i]?.title ?? ''}</td>
                            <td class="text-center">${blogs[i]?.created_at?.formatDate() ?? ''}</td>
                            <td class="text-center">${blogs[i]?.blog_category?.name ?? ''}</td>
                            <td class="text-center">${getTypeString(blogs[i]?.type)}</td>
                            <td class="text-center">
                                <span type="button" data-id=${id} class="fas fa-edit fa-lg text-success btn-show-update-bisto btn-show-update-bisto-css"></span>
                                <span type="button" data-id=${id} class="fas fa-times fa-lg text-danger btn-delete-bisto-css btn-delete-bisto"></span>
                            </td>
                        </tr>`;
            }
            _.$tbody.empty().append(tbody);
        }

        function loadBlogCategory({currentPage = 1, keyword = '', loading = {type: 'table', colspan: 4}}) {
            $.ajax({
                url: `{{url('/admin/blog-categories')}}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
            }).done(res => {
                const blogCategories = res?.results?.data ?? [];
                let options = '';
                for (const blogCategory of blogCategories) {
                    options += `<option value="${blogCategory.id}">${blogCategory.name}</option>`
                }
                $('select[name=blog_category_id]').empty().append(options);
            }).fail(err => {
            });
        }
    </script>
@endsection
