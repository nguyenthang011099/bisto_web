@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div class="iq-card" style="background: white">
                <div style="padding: 15px">
                    <div class="form-group">
                        <label for="title">Tiêu đề thông báo</label>
                        <input type="text" class="form-control" id="title"
                               placeholder="nhập tiêu đề">
                        <small id="title-alert" style="color:red; display: none">Tiêu đề không được để trống</small>
                    </div>
                    <div class="form-group">
                        <label for="content">Nội dung thông báo</label>
                        <textarea type="content" class="form-control" id="content"
                                  placeholder="nhập nội dung"></textarea>
                        <small id="content-alert" style="color:red; display: none">Nội dung không được để trống</small>
                    </div>
                    <button type="button" id="btn-post-notification" class="btn btn-primary">Gửi</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        window.addEventListener('load', function () {
            $('#btn-post-notification').on('click', postNotification);
        })

        function postNotification() {
            $('#btn-post-notification').prop('disabled', true);
            let title = $('#title').val();
            let content = $('#content').val();
            console.log(title);
            if(!title){
                $('#title-alert').css('display', 'block');
                if(content){
                    return
                }
            }
            if(!content){
                $('#content-alert').css('display', 'block');
                return
            }
            $('#title-alert').css('display', 'none');
            $('#content-alert').css('display', 'none');

            $.ajax({
                url: "{{url('/admin/notification')}}",
                method: 'post',
                data: {
                    _token: "{{csrf_token()}}",
                    title: title,
                    content: content,
                },
                success: function(obj, textStatus, xhr){
                    if(xhr.code == 400){
                        toastErrorMessage(obj.message)
                    }else{
                        toastSuccessMessage(obj.message)
                    }

                    setTimeout(() => location.reload(), 2000)
                }
            })
        }
    </script>
@endsection
