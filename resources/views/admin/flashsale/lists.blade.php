@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách chương trình Flashsale
                </div>
                <div class="row w3-res-tb row-action-bisto-css">
                    <div class="col-sm-5">
                        <div class="btn-show-add-bisto-css btn btn-primary add-flash-sale" data-toggle="modal"
                             data-target=".add-flashsale">
                            +
                            Thêm chương trình flashsale
                        </div>
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search" placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm form-control btn-search-bisto-css"
                                   name="keywords_submit">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th class="text-center">Tên chương trình</th>
                                <th class="text-center">Mô tả</th>
                                <th class="text-center">Giá trị</th>
                                <th class="text-center">Hình ảnh quảng bá</th>
                                <th class="text-center">Từ ngày - Đến ngày</th>
                                <th class="text-center">Khung giờ</th>
                                <th class="text-center">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <!-- ADD FLASHSALE -->
        <div class="modal fade add-flashsale" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style="border-radius: 20px">
                    <div class="col-sm-12 col-lg-12">
                        <div class="iq-card">
                            <div class="panel-heading">
                                Thêm chương trình flashsale
                            </div>
                            <div class="iq-card-body">
                                <div id="sale-form">
                                    <div class="form-group">
                                        <label class="required-input">Tên chương trình Flashsale</label>
                                        <input type="text" class="form-control" name="name"
                                               placeholder="Tên flashsale"/>
                                        <div class="mes-v-display mes-name"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="required-input">Hình ảnh quảng bá</label><span
                                            class="note-img note-img-1-13"></span>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input"
                                                   name="flashsale-img">
                                            <div class="img-container-f row align-items-start"></div>
                                            <label class="custom-file-label">Chọn ảnh</label>
                                        </div>
                                        <div class="mes-imageUrl mes-invalid">
                                            Bạn phải thêm ít nhất 1 ảnh
                                        </div>
                                    </div>
                                    <div class="form-group d-none">
                                        <label>Loại flashsale</label>
                                        <select name="type" class="form-control">
                                            <option selected value="1">Giảm theo tiền</option>
                                            <option value="2">Giảm theo %</option>
                                        </select>
                                        <div class="mes-v-display mes-type"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="required-input">Giá trị flashsale</label>
                                        <input type="number" class="form-control" name="amount"
                                               placeholder="Ví dụ : 100000"/>
                                        <div class="mes-v-display mes-amount"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="required-input">Mô tả</label>
                                        <textarea class="form-control textarea-bisto" name="description"></textarea>
                                        <div class="mes-v-display mes-description">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label>Từ ngày</label>
                                            <input type="date" class="form-control" name="start_date">

                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label>Đến ngày</label>
                                            <input type="date" class="form-control" name="end_date">
                                        </div>
                                        <div class="mes-v-display mes-rangedate">
                                            Ngày bắt đầu phải nhỏ hơn ngày kết thúc
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label>Khung giờ từ</label>
                                            <input type="time" class="form-control" name="start_time"/>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label>Đến</label>
                                            <input type="time" class="form-control" name="end_time"/>
                                        </div>
                                        <div class="mes-v-display mes-rangetime">
                                            Giờ bắt đầy phải nhỏ hơn giờ kết thúc
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Hiển thị</label>
                                        <select name="status" class="form-control input-sm m-bot15">
                                            <option value="1" selected>Hiển thị</option>
                                            <option value="0">Ẩn</option>
                                        </select>
                                    </div>
                                    <button id="btn-save" class="btn btn-primary">Bắt đầu chương trình
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END ADD FLASHSALE -->

        <!-- EDIT FLASHSALE -->
        <div class="modal fade edit-flashsale" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style="border-radius: 20px">
                    <div class="col-sm-12 col-lg-12">
                        <div class="iq-card">
                            <div class="panel-heading">
                                Cập nhật chương trình flashsale
                            </div>
                            <div class="iq-card-body">
                                <div class="edit-sale-form">
                                    <div class="form-group">
                                        <label class="required-input">Tên chương trình Flashsale</label>
                                        <input type="text" class="form-control" name="name"
                                               placeholder="Tên flashsale"/>
                                        <div class="mes-v-display mes-name"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="required-input">Hình ảnh quảng bá</label><span
                                            class="note-img note-img-1-13"></span>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input"
                                                   name="flashsale-edit-img">
                                            <div class="img-container-f-edit row align-items-start"></div>
                                            <label class="custom-file-label">Chọn ảnh</label>
                                        </div>
                                        <div class="mes-imageUrl mes-invalid">
                                            Bạn phải thêm ít nhất 1 ảnh
                                        </div>
                                    </div>
                                    <div class="form-group d-none">
                                        <label>Loại flashsale</label>
                                        <select name="type" class="form-control">
                                            <option selected value="1">Giảm theo tiền</option>
                                            <option value="2">Giảm theo %</option>
                                        </select>
                                        <div class="mes-v-display mes-type"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="required-input">Giá trị flashsale</label>
                                        <input type="number" class="form-control" name="amount"
                                               placeholder="Ví dụ : 100000"/>
                                        <div class="mes-v-display mes-amount"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="required-input">Mô tả</label>
                                        <textarea class="form-control textarea-bisto" name="description"></textarea>
                                        <div class="mes-v-display mes-description">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label>Từ ngày</label>
                                            <input type="date" class="form-control" name="start_date">
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label>Đến ngày</label>
                                            <input type="date" class="form-control" name="end_date">
                                        </div>
                                        <div class="mes-v-display mes-rangedate">
                                            Ngày bắt đầu phải nhỏ hơn ngày kết thúc
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label>Khung giờ từ</label>
                                            <input type="time" class="form-control" name="start_time"/>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label>Đến</label>
                                            <input type="time" class="form-control" name="end_time"/>
                                        </div>
                                        <div class="mes-v-display mes-rangetime">
                                            Giờ bắt đầy phải nhỏ hơn giờ kết thúc
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Hiển thị</label>
                                        <select name="status" class="form-control input-sm m-bot15">
                                            <option value="1" selected>Hiển thị</option>
                                            <option value="0">Ẩn</option>
                                        </select>
                                    </div>
                                    <button class="btn-update-flashsale btn btn-primary">Cập nhật chương trình</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-container">

        </div>
    </div>
    <!-- END EDIT FLASHSALE -->

    <script type="text/javascript">
        let dt = {
            currentPage: 1, searchKey: '', loading: {colspan: 9, type: 'table'}, isEdit: false
        };
        let pagination, _i;
        let cropperFlashsale, cropperFlashsaleEdit;
        let uploaderFlashsale, uploaderFlashsaleEdit;

        $(document).ready(function () {
            _i = defineImage({id: 'flashsale'});
            setInputDate('start_date');
            setInputDate('end_date');
            setInputTime('start_time');
            setInputTime('end_time');
            loadFlashSales(dt);
            let _totalFiles = 0;
            let _totalFilesEdit = 0;
            uploaderFlashsale = defineUploadFile({id: 'flashsale', containerSelector: '.img-container-f'})
            uploaderFlashsaleEdit = defineUploadFile({id: 'flashsale', containerSelector: '.img-container-f-edit'});

            cropperFlashsale = defineCropAndUploadImage({
                type: 'flashsale',
                inputSelector: 'input[name=flashsale-img]', cbWhenAddFile: () => {
                    if (uploaderFlashsale.getTotalFile() > 0) {
                        toastInfoMessage('Cho phép chọn tối đa 1 ảnh quảng bá');
                        cropperFlashsale.stopUpload();
                    }
                }
            });
            cropperFlashsaleEdit = defineCropAndUploadImage({
                type: 'flashsale',
                inputSelector: 'input[name=flashsale-edit-img]', cbWhenAddFile: () => {
                    if (uploaderFlashsaleEdit.getTotalFile() > 0) {
                        toastInfoMessage('Cho phép chọn tối đa 1 ảnh quảng bá');
                        cropperFlashsaleEdit.stopUpload();
                    }
                }
            });
            cropperFlashsale.saveButton.addEventListener('click', function () {
                let u = uploaderFlashsale;
                if (u.getTotalFile() > 0) return toastInfoMessage('Cho phép chọn tối đa 1 ảnh quảng bá')
                _totalFiles = u.getTotalFile() + 1;
                const {fileElement} = u.createFileElement(cropperFlashsale.getImageUrl(), cropperFlashsale.getImgName());
                u.getFileUrls().push(cropperFlashsale.getImg());
                u.setTotalFile(_totalFiles);
                u.getContainer().append(fileElement);
                cropperFlashsale.$modal.modal('hide');
            });
            cropperFlashsaleEdit.saveButton.addEventListener('click', function () {
                let u = uploaderFlashsaleEdit;
                if (u.getTotalFile() > 0) return toastInfoMessage('Cho phép chọn tối đa 1 ảnh quảng bá')
                _totalFilesEdit = u.getTotalFile() + 1;
                const {fileElement} = u.createFileElement(cropperFlashsaleEdit.getImageUrl(), cropperFlashsaleEdit.getImgName());
                u.getFileUrls().push(cropperFlashsaleEdit.getImg())
                u.setTotalFile(_totalFilesEdit);
                u.getContainer().append(fileElement);
                cropperFlashsaleEdit.$modal.modal('hide');
            });
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadFlashSales(dt);
                }
            });
        });

        function generateFlashsale(flashSales) {
            let tbody = '';
            _i.resetPhotos();
            for (let index = 0; index < flashSales.length; index++) {
                const id = flashSales[index].id;

                const amount = flashSales[index].type == 1 ? flashSales[index]?.amount?.formatVND() : `${flashSales[index]?.amount} %`
                tbody += `<tr>
                    <td class="text-center">${index + 1}</td>
                    <td class="text-center">${flashSales[index].name}</td>
                    <td class="text-center">${flashSales[index].description}</td>
                    <td class="text-center">
                        <span class="text-ellipsis">${amount}</span>
                    </td>
                    <td>
                           ${_i.toHTML(flashSales[index].image_url, flashSales[index].name)}
                    </td>
                    <td class="text-nowrap text-center">${moment(flashSales[index].start_date).format('DD-MM-YYYY')} đến ${moment(flashSales[index].end_date).format('DD-MM-YYYY')}</td>
                    <td class="text-nowrap text-center">${flashSales[index].start_time ? flashSales[index].start_time.substring(0, 5) : '00:00'} - ${flashSales[index].end_time ? flashSales[index].end_time.substring(0, 5) : '00:00'}</td>
                    <td class="text-center">
                    <span flid=${id} class="btn-show-update-bisto-css styling-edit-flashsale" data-toggle="modal" data-target=".edit-flashsale">
                                <i class="fas fa-edit fa-lg btn-show-update-bisto-css text-success"></i>
                    </span>
                    <span class="btn-delete btn-delete-bisto-css" flid=${id}>
                        <i class="fa fa-times fa-lg text-danger"></i>
                    </span>
                    </td>
                </tr>`;
            }
            $('.table-striped>tbody').empty().append(tbody);
            _i.animateOnloadImage({});
        }

        function loadFlashSales({currentPage = 1, searchKey = '', loading = {colspan: 9, type: 'table'}}) {
            const _url = `flashsales?page=${currentPage}&keyword=${searchKey}`
            $.ajax({
                url: _url,
                loading,
                method: 'GET',
            })
                .done(res => {
                    let flashSales = res?.FlashSales?.data ?? [];
                    if (flashSales?.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadFlashSales(dt);
                    } else if (flashSales.length === 0 || res === undefined) {
                        $('.table-striped>tbody').empty().append(genNoContentTable(9, 'Không có chương trình nào'));
                    } else {
                        generateFlashsale(flashSales);
                        pagination.setTotalItems(res.FlashSales.total);
                    }
                })
                .fail(err => {
                    toastError(err, 'Không thể load ');
                });
        }

        function validateFormFlashsale(parentSelector = '#sale-form') {
            let validateType = true;
            if ($('select[name=type]', parentSelector).val() === '1') {
                validateType = () => validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Giá trị flashsale là bắt buộc'
                    }, {
                        func: rangeValidation,
                        mes: 'Giá trị flashsale tối thiểu 1000 VND',
                        params: {
                            min: 1000,
                            max: MAX_MONEY,
                        }
                    }], propertyName: 'amount', parentSelector
                })
            } else {
                validateType = () => validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Giá trị flashsale là bắt buộc'
                    }, {
                        func: rangeValidation,
                        mes: 'Phần trăm lớn hơn 0 và nhỏ hơn 100',
                        params: {
                            min: 0,
                            max: 100,
                        }
                    }], propertyName: 'amount', parentSelector
                })
            }

            return validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên chương trình là bắt buộc'
                    }],
                    propertyName: 'name', parentSelector
                }) && validateRequiredElement({
                    inputSelector: 'input[name=imageUrl]',
                    mesSelector: '.mes-imageUrl',
                    message: 'Bạn phải thêm ít nhất một ảnh', parentSelector
                }) && validateType() && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Mô tả là bắt buộc'
                    }], propertyName: 'description', type: 'textarea', parentSelector
                })
                && validateRangeDate('input[name=start_date]', 'input[name=end_date]', '.mes-rangedate', parentSelector) &&
                validateRangeTime('input[name=start_time]', 'input[name=end_time]', '.mes-rangetime', parentSelector);
        }


        $(document).on('click', '.btn-delete', function () {
            const id = $(this).attr('flid');
            confirmDelete('Bạn có chắc muốn xóa chương trình này?', function () {
                $.ajax({
                    url: `flashsales/${id}`,
                    method: 'DELETE',
                    data: {
                        _token: "{{csrf_token()}}",
                    },
                })
                    .done(res => {
                        toastSuccess(res, 'Xóa thành công');
                        loadFlashSales({...dt, loading: {type: 'content'}});
                    })
                    .fail(err => {
                        console.log(err);
                    })
            });
        })

        function clearFormAdd() {
            dataToForm({
                fieldInputs: [{selector: 'input[name=name]'},
                    {selector: 'input[name=code]'},
                    {selector: 'select[name=end_date]'},
                    {selector: 'select[name=start_date]'},
                    {selector: 'input[name=amount]'},
                    {selector: 'textarea[name=description]'},
                    {selector: 'input[name=start_time]'},
                    {selector: 'input[name=end_time]'},
                    {selector: 'select[name=status]', value: '1'}],
                fieldFiles: [{
                    uploaderInstance: uploaderFlashsale,
                    listFiles: []
                }],
                parentSelector: '#sale-form', action: 'clear-validate'
            })
        }

        $(document).on('click', '.add-flash-sale', function () {
            $('select[name=status]').prop('disabled', true);
        })

        $('#btn-save').on('click', function (e) {
            if (validateFormFlashsale()) {
                const dataObj = formToData('#sale-form');
                $.ajax({
                    url: "{{url('/admin/flashsales')}}",
                    method: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        ...dataObj,
                        type: "1",
                        source_urls: [uploaderFlashsale.getOneImg()]
                    },
                })
                    .done(res => {
                        clearFormAdd();
                        toastSuccess(res, 'Thêm mới thành công');
                        $('.modal.add-flashsale').modal('hide');
                        loadFlashSales(dt);
                    })
                    .fail(err => {
                        $('.modal.add-flashsale').modal('hide');
                        console.log(err);
                    })
            }
        });

        $(document).on('click', '.btn-update-flashsale', function () {
            const id = $('.btn-update-flashsale').data('id');
            if (validateFormFlashsale('.edit-sale-form')) {
                const dataObj = formToData('.edit-sale-form');
                //console.log(dataObj)
                $.ajax({
                    url: `flashsales/${id}`,
                    method: 'PUT',
                    data: {
                        _token: "{{csrf_token()}}",
                        ...dataObj,
                        type: "1",
                        deleted_image_ids: uploaderFlashsaleEdit.getDeleteImageIds(),
                        source_urls: [uploaderFlashsaleEdit.getOneImg()]
                    },
                })
                    .done(res => {
                        toastSuccess(res, 'Cập nhật thành công');
                        $('.modal.edit-flashsale').modal('hide');
                        loadFlashSales({...dt, loading: {type: 'content'}});
                    })
                    .fail(err => {
                        $('.modal.edit-flashsale').modal('hide');
                        console.log(err);
                    })
            }
        });
        $(document).on("click", ".styling-edit-flashsale", function () {
            const id = $(this).attr('flid');
            $('select[name=status]').prop('disabled', false);
            $.ajax({
                url: `flashsales/${id}`,
                method: 'GET'
            })
                .done(res => {
                    const flashSale = res.FlashSale[0];
                    const $edit = $('.edit-sale-form');
                    $edit.find('input[name=name]').val(flashSale.name);
                    $edit.find('select[name=type]').val(flashSale?.type ?? '1');
                    $edit.find('input[name=amount]').val(flashSale.amount);
                    $edit.find('textarea[name=description]').val(flashSale.description);
                    setInputTime('start_time', flashSale.start_time, '.edit-sale-form');
                    setInputTime('end_time', flashSale.end_time, '.edit-sale-form');
                    setInputDate('start_date', new Date(flashSale.start_date), '.edit-sale-form');
                    setInputDate('end_date', new Date(flashSale.end_date), '.edit-sale-form');
                    $edit.find('select[name=status]').val(flashSale.status);
                    let _fileUrls = [];
                    const _totalFiles = 1;
                    uploaderFlashsaleEdit.getContainer().empty();
                    for (let i = 0; i < _totalFiles; i++) {
                        const {fileElement} = uploaderFlashsaleEdit.createFileElement(flashSale.image, flashSale.image_id);
                        _fileUrls.push({name: flashSale.image_id, url: flashSale.image});
                        uploaderFlashsaleEdit.getContainer().append(fileElement);
                    }
                    uploaderFlashsaleEdit.setTotalFile(_totalFiles);
                    uploaderFlashsaleEdit.setFileUrls(_fileUrls);
                    $('.btn-update-flashsale').data('id', flashSale.id)
                })
                .fail(err => {
                    console.log(err);
                })
        });

        document.querySelector('input[name=keywords_submit]').addEventListener('keydown', function (e) {
            if (e.key === 'Enter') {
                dt.searchKey = e.target.value;
                loadFlashSales(dt);
            }
        })
    </script>
@endsection
