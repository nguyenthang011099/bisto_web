@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách đơn hàng
                </div>
                <div class="row row-action-bisto-css">
                    <div class="col-sm-9 d-flex justify-content-between">
                        <div style="margin-left: 20px">
                            <select class="form-control" name="order-admin-shop">
                                <option selected value="0">Tất cả chuỗi</option>
                            </select>
                        </div>
                        <div class="">
                            <select class="form-control" id="btn-order-status">
                                <option selected value="0">Tất cả đơn hàng</option>
                                <option value="{{\App\Order::NGUOI_MUA_DA_TAO_DON}}">Người mua đã tạo đơn</option>
                                <option value="{{\App\Order::NGUOI_BAN_XAC_NHAN_DON}}">Người bán xác nhận đơn</option>
                                <option value="{{\App\Order::NGUOI_BAN_HUY_DON}}">Người bán hủy đơn</option>
                                <option value="{{\App\Order::DON_HANG_DANG_GIAO}}">Đơn đang được giao</option>
                                <option value="{{\App\Order::NGUOI_MUA_DA_NHAN_HANG}}">Người mua đã nhận hàng</option>
                                <option value="{{\App\Order::NGUOI_MUA_HUY_DON}}">Người mua hủy đơn</option>
                                <option value="{{\App\Order::NGUOI_MUA_TRA_HANG}}">Người mua trả hàng</option>
                                <option value="{{\App\Order::NGUOI_MUA_DOI_HANG}}">Người mua đổi hàng</option>
                                <option value="{{\App\Order::DON_THANH_CONG}}">Đơn thành công</option>
                            </select>
                        </div>
                        <div class="">
                            <select class="form-control" id="btn-range-date" name="btn-range-date">
                                <option value="0" selected>Trong cả tháng</option>
                                <option value="1">Nửa đầu tháng</option>
                                <option value="2">Nửa cuối tháng</option>
                            </select>
                        </div>
                        <div class="">
                            <select class="form-control" id="btn-month" name="btn-month">
                                <option selected value="0">Tất cả 12 tháng</option>
                                <option value="1">Tháng 1</option>
                                <option value="2">Tháng 2</option>
                                <option value="3">Tháng 3</option>
                                <option value="4">Tháng 4</option>
                                <option value="5">Tháng 5</option>
                                <option value="6">Tháng 6</option>
                                <option value="7">Tháng 7</option>
                                <option value="8">Tháng 8</option>
                                <option value="9">Tháng 9</option>
                                <option value="10">Tháng 10</option>
                                <option value="11">Tháng 11</option>
                                <option value="12">Tháng 12</option>
                            </select>
                        </div>
                        <div class="">
                            <select class="form-control" id="btn-year" name="btn-year">
                                @foreach($_years_ as $year)
                                    <option value="{{$year}}">Năm {{$year}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="">
                            <button style="height: 100%;" id="btn-query"
                                    name="btn-query"
                                    class="btn btn-primary">Truy Vấn
                            </button>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search"
                                   placeholder="&#xf002 Tìm kiếm theo mã đơn hàng"
                                   class="input-sm form-control btn-search-bisto-css"
                                   name="keywords_submit">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Mã đơn hàng</th>
                            <th class="text-center">Ngày tháng đặt hàng</th>
                            <th class="text-center">Tình trạng đơn hàng</th>
                            <th class="text-center">Tiền hàng</th>
                            <th class="text-center">Phí vận chuyển</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody id="tbl-order">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div style="" id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->

                <div class="d-flex justify-content-end">
                    <div class="" style="padding: 20px">
                        <p id="total-sales" class=""></p>
                        <p id="total-order"></p>
                        <p id="total-order-success"></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- total price and footer -->
    </div>

    <script>
        let dt = {currentPage: 1, text_search: '', loading: {type: 'table', colspan: 7}}, pagination,
            currentPageQuery = 1;
        $(document).ready(function () {
            // loadOrders(dt);
            const params = parseUrlParams();
            if (params.status) {
                document.getElementById('btn-order-status').value = params.status;
                query(params);
            } else query();
            loadListAdminShop();
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    currentPageQuery = pageNumber;
                    query();
                }
            });
        });

        $(document).on('change', '#btn-year', function () {
            document.getElementById('btn-query').disabled = $('#btn-year').val() === 'null';
        })

        function query(params) {
            const option = params?.option ?? document.getElementById('btn-range-date').value;
            const month = params?.month ?? document.getElementById('btn-month').value;
            const year = params?.year ?? document.getElementById('btn-year').value;
            const status = params?.status ?? document.getElementById('btn-order-status').value;
            const adminShopId = params?.adminShopId ?? $('select[name=order-admin-shop]').val();
            queryOrder(option, month, year, status, adminShopId);
        }

        //event when user press query order by option
        $(document).on('click', '#btn-query', function () {
            document.getElementById('btn-query').disabled = true;
            query();
        })

        //query order by option
        function queryOrder(option, month, year, status, adminShopId) {
            $.ajax({
                url: `{{url('/admin/orders/query')}}?page=${currentPageQuery}`,
                method: "POST",
                loading: {type: 'table', colspan: 7},
                data: {
                    _token: "{{csrf_token()}}",
                    option: option,
                    month: month,
                    year: year,
                    status: status,
                    admin_shop_id: adminShopId
                }
            }).done(res => {
                const orders = res?.result?.data ?? [];
                if (orders.length === 0 && currentPageQuery > 1) {
                    currentPageQuery -= 1;
                    pagination.setCurrentPage(1);
                    query();
                } else if (orders.length === 0) {
                    $('.table-striped>tbody').empty().append(genNoContentTable(7, 'Không có đơn hàng nào'));
                } else {
                    generateOrders(orders);
                    document.getElementById('btn-query').disabled = false;
                    pagination.setTotalItems(res.result.total);
                }
            }).fail(err => {
                document.getElementById('btn-query').disabled = false;
            })

        }

        function generateOrders(orders) {
            let tbody = '';
            orders = orders.filter(item => item.status_code !== {{\App\Order::NGUOI_MUA_DANG_MUA_HANG}});
            for (let i = 0; i < orders.length; i++) {
                let id = orders[i].id;
                let orderDetailUrl = `{{url('/admin')}}/order-detail/${id}`;
                tbody += `<tr>
                        <td class="text-center">${i + 1}</td>
                        <td class="text-center">${orders[i].code}</td>
                        <td class="text-center">${orders[i].created_at}</td>
                        <td class="text-center" style="color: ${orders[i].color}">${orders[i].status}</td>
                        <td class="text-center">${orders[i].total_money ? orders[i].total_money.formatVND() : Number(0).formatVND()}</td>
                        <td class="text-center">${orders[i].shipping_fee ? orders[i].shipping_fee.formatVND() : Number(0).formatVND()}</td>
                        <td class="text-center">
                            <a href=${orderDetailUrl} class="active styling-edit">
                                <i class="fa fa-eye fa-lg text-success text-active"></i></a>
                        </td>
            </tr>`;
            }
            $('.table-striped>tbody').empty().append(tbody);
            // caculate sales, order, order successs
            let total_money = 0;
            let total_order = 0;
            let total_order_success = 0;
            for (const order of orders) {
                total_order++;
                if (order.status === "Đơn thành công") {
                    total_money += order.total_money;
                    total_order_success++;
                }
            }
            document.getElementById('total-sales').innerHTML = "Tổng doanh thu: " + total_money.formatVND();
            document.getElementById('total-order').innerHTML = "Tổng đơn hàng: " + total_order + " đơn";
            document.getElementById('total-order-success').innerHTML = "Tổng đơn hàng thành công: " +
                total_order_success + " đơn";
        }


        function loadOrders({currentPage = 1, text_search = '', loading = {type: 'table', colspan: 7}}) {
            $.ajax({
                url: `{{url('/admin')}}/orders?page=${currentPage}&keyword=${text_search}`,
                method: 'GET',
                loading
            })
                .done(res => {
                    const orders = res?.orders?.data ?? [];
                    if (orders.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadOrders(dt);
                    } else if (orders.length === 0) {
                        $('.table-striped>tbody').empty().append(genNoContentTable(7, 'Không có đơn hàng nào'));
                    } else {
                        generateOrders(orders);
                        pagination.setTotalItems(res.orders.total);
                    }
                })
                .fail(err => {
                });
        }

        $('input[name=keywords_submit]').on('keydown', function (e) {
            if (e.key === 'Enter') {
                dt.text_search = $(this).val();
                loadOrders(dt);
            }
        })

        function loadListAdminShop() {
            $.ajax({
                url: 'lists/admin-shops',
                method: 'GET'
            }).done(res => {
                const users = res?.users?.data ?? [];
                let options = '';
                for (const user of users) {
                    options += `<option value=${user.user_id}>${user.shop_name}</option>`
                }
                $('select[name=order-admin-shop]').append(options);
            }).fail(err => {
                toastError(err, 'Load danh sách admin-shop thất bại')
            })
        }
    </script>
@endsection
