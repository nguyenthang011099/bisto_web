@extends('admin.layout')
@section('admin_content')
    <div id="content-page" class="content-page tag-admin">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách tag
                </div>
                <div style="margin-top: 20px" class="row btn-bisto row-action-bisto-css">
                    <div class="col-sm-6">
                        <button class="btn-show-add-bisto btn-show-add-bisto-css btn btn-primary">+ Thêm tag
                        </button>
                    </div>
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search"
                                   placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm btn-search-bisto btn-search-bisto-css form-control">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body body-bisto">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Tên</th>
                            <th class="text-center">Ngày tạo</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody class="tbody-bisto">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <!-- ADD START -->
        <div id="modal-container">
            <div class="modal fade modal-bisto" tabindex="-1" role="dialog"
                 aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content" style="border-radius: 20px">
                        <div class="iq-card">
                            <header class="panel-heading title-modal-bisto">
                                Thêm tag
                            </header>
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="iq-card-body">
                                        <div class="form-bisto" role="form">
                                            @csrf
                                            <div class="form-group">
                                                <label class="required-input">Tên tag</label>
                                                <input type="text" name="name" class="form-control">
                                                <div class="mes-name mes-v-display">
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <button class="btn btn-primary btn-add-bisto btn-add-bisto-css">
                                                Thêm tag
                                            </button>
                                            <button class="btn btn-primary btn-update-bisto btn-update-bisto-css">
                                                Cập nhật tag
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-container">
        </div>
    </div>
    <script>
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: 5}};
        let _, pagination;

        $(document).ready(function () {
            _ = defineBisto({
                parentSelector: '.tag-admin', name: 'tag',
            });

            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadTag(dt);
                }
            });
            loadTag(dt);

            _.onSearch((value) => {
                dt.keyword = value;
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadTag(dt);
            });

            _.onShowAdd(() => {
                dataToForm({
                    fieldInputs: [
                        {selector: 'input[name=name]'},
                    ],
                    parentSelector: '.form-bisto', action: 'clear-validate'
                })
            })

            _.onAdd((data) => {
                if (validateFormTag())
                    $.ajax({
                        url: `{{url('/admin/tags')}}`,
                        method: 'POST',
                        data: {
                            ...data,
                        }
                    }).done(res => {
                        toastSuccess(res, 'Thêm tag thành công');
                        loadTag({...dt, loading: {type: 'content'}});
                        _.$modalAdd.modal('hide');
                    }).fail(err => {
                        _.$modalAdd.modal('hide');
                    })
            })

            _.onShowUpdate((id, cbShowModalUpdate) => {
                $.ajax({
                    url: `{{url('/admin/tags')}}/${id}`,
                    method: 'GET',
                    loading: {type: 'content'}
                }).done(res => {
                    const tag = res?.result ?? {name: 0, created_at: null};
                    dataToForm({
                        fieldInputs: [
                            {selector: 'input[name=name]', value: tag.name},
                        ],
                        parentSelector: '.form-bisto', action: 'clear-validate'
                    });
                    cbShowModalUpdate();
                }).fail(err => {
                })
            })

            _.onUpdate((id, data) => {
                if (validateFormTag())
                    $.ajax({
                        url: `{{url('/admin/tags')}}/${id}`,
                        method: 'PUT',
                        loading: {type: 'content'},
                        data: {
                            ...data,
                        }
                    }).done(res => {
                        toastSuccess(res, 'Cập nhật tag thành công');
                        loadTag({...dt, loading: {type: 'content'}});
                        _.$modalUpdate.modal('hide');
                    }).fail(err => {
                        _.$modalUpdate.modal('hide');
                    })
            })

            _.onDelete((id) => {
                $.ajax({
                    url: `{{url('/admin/tags')}}/${id}`,
                    method: 'DELETE',
                    data: {
                        _token: '{{csrf_token()}}',
                    }
                }).done(res => {
                    toastSuccess(res, 'Xóa tag thành công');
                    loadTag({...dt, loading: {type: 'content'}});
                    _.$modalUpdate.modal('hide');
                }).fail(err => {
                    _.$modalUpdate.modal('hide');
                })
            })
        });

        function validateFormTag() {
            return validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Tên tag là bắt buộc'
                }],
                propertyName: 'name'
            })
        }

        function loadTag({currentPage = 1, keyword = '', loading = {type: 'table', colspan: 4}}) {
            $.ajax({
                url: `{{url('/admin/tags')}}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
                loading,
            }).done(res => {
                const tags = res?.results?.data ?? [];
                if (tags?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadTag(dt);
                } else if (tags?.length === 0) {
                    _.$tbody.empty().append(genNoContentTable(4, `Không có dữ liệu`));
                } else {
                    generateTags(tags);
                    pagination.setTotalItems(res.results.total);
                }
            }).fail(err => {
            });
        }

        function generateTags(tags) {
            let tbody = '';
            for (let i = 0; i < tags.length; i++) {
                const id = tags[i].id;
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${tags[i]?.name ?? ''}</td>
                            <td class="text-center">${tags[i]?.created_at?.formatDate() ?? ''}</td>
                            <td class="text-center">
                                <span type="button" data-id=${id} class="fas fa-edit fa-lg text-success btn-show-update-bisto btn-show-update-bisto-css"></span>
                                <span type="button" data-id=${id} class="fas fa-times fa-lg text-danger btn-delete-bisto-css btn-delete-bisto"></span>
                            </td>
                        </tr>`;
            }
            _.$tbody.empty().append(tbody);
        }
    </script>
@endsection
