<!DOCTYPE html>
<html lang="en">

<head>
    <title>BISTO-Tiện ích Thời Trang Online</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{asset('frontend_landingpage/images/icons/title_1.svg')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('frontend_landingpage/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('frontend_landingpage/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('frontend_landingpage/fonts/iconic/css/material-design-iconic-font.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('frontend_landingpage/fonts/linearicons-v1.0.0/icon-font.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend_landingpage/vendor/animate/animate.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('frontend_landingpage/vendor/css-hamburgers/hamburgers.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('frontend_landingpage/vendor/animsition/css/animsition.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend_landingpage/vendor/select2/select2.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('frontend_landingpage/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend_landingpage/vendor/slick/slick.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('frontend_landingpage/vendor/MagnificPopup/magnific-popup.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('frontend_landingpage/vendor/perfect-scrollbar/perfect-scrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend_landingpage/css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend_landingpage/css/main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/dist/app.css')}}">

</head>

<body class="animsition">
<header>
    <div class="container-menu-desktop">
        <div class="wrap-menu-desktop">
            <nav class="limiter-menu-desktop container">
                <a href="{{URL::to('/')}}" class="logo">
                    <img src="{{asset('frontend_landingpage/images/icons/SVG/app.svg')}}" alt="IMG-LOGO" width="52"
                         height="52">
                </a>
                <div class="menu-desktop">
                    <ul class="main-menu">
                        <li>
                            <a href="{{URL::to('/')}}">Trang chủ</a>
                        </li>
                        <li>
                            <a href="{{URL::to('/blogs')}}">Tin tức</a>
                            <ul class="sub-menu" id="blog-cate-list">
                                <li><a href="{{URL::to("/blog-categories/".\App\Blog::SLUG_HOT_PAGE)}}">Tin tức nổi
                                        bật</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#Stand2">Dành cho người bán</a>
                        </li>
                        <li>
                            <a href="{{URL::to('login')}}">Đăng nhập bán hàng</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <div class="wrap-header-mobile">
        <div class="logo-mobile">
            <a href="{{URL::to('/')}}" class="logo">
                <img src="{{asset('frontend_landingpage/images/icons/SVG/app.svg')}}" alt="IMG-LOGO" width="32"
                     height="32">
            </a>
        </div>
        <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
        </div>
    </div>
    <div class="menu-mobile">
        <ul class="main-menu-m">
            <li>
                <a href="{{URL::to('/')}}">Trang chủ</a>
            </li>
            <li>
                <a href="{{URL::to('/blogs')}}">Tin tức</a>
                <span class="arrow-main-menu-m">
                    <i class="fa fa-angle-down"></i>
                </span>
                <ul class="sub-menu-m" id="blog-cate-list-m">
                    <li><a href="{{URL::to("/blog-categories/".\App\Blog::SLUG_HOT_PAGE)}}">Tin tức nổi bật</a></li>
                </ul>
            </li>
            <li>
                <a href="{{URL::to('/htbh')}}">Dành cho người bán</a>
            </li>
            <li>
                <a href="{{URL::to('login')}}">Đăng nhập bán hàng</a>
            </li>
        </ul>
    </div>
</header>
<section class="bg0 p-t-70 p-b-120">
    @yield('app_content')
</section>
<footer class="bg3 p-t-35 p-b-32">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-6 ">
                <h3 class="haha stext-301 cl0" style="font-size:40px">BISTO</h3>
                <h3 class="haha stext-301 cl0" style="font-size:20px"> THẾ GIỚI THỜI TRANG CỦA BẠN !</h3>
                <h4 class="stext-301 cl0 p-b-30 p-t-50">
                    Download App
                </h4>
                <div class="">
                    <a href="https://play.google.com/store/apps/details?id=com.bisto.geer_fe&fbclid=IwAR2ilvXgPBbtehNZ_7acDmRXLKwO_vliQbpqR3guW6EhkcYYdFwkmZcaX3g"
                       class="m-all-1">
                        <img src="{{asset('frontend_landingpage/images/icons/icon-play-0.png')}}" alt="ICON-PAY"
                             width="150">
                    </a>
                    <a href="https://play.google.com/store/apps/details?id=com.bisto.geer_fe&fbclid=IwAR2ilvXgPBbtehNZ_7acDmRXLKwO_vliQbpqR3guW6EhkcYYdFwkmZcaX3g"
                       class="m-all-1">
                        <img src="{{asset('frontend_landingpage/images/icons/icon-play-1.png')}}" alt="ICON-PAY"
                             width="150">
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-2 p-b-50 p-t-30">
                <h4 class="stext-301 cl0 p-b-30">
                    Menu
                </h4>
                <ul>
                    <li>
                        <a href="{{URL::to('/')}}" class="stext-107 cl7 hov-cl1 trans-04">Trang chủ</a>
                    </li>
                    <li>
                        <a href="{{URL::to('htbh')}}" class="stext-107 cl7 hov-cl1 trans-04">Dành cho người bán</a>
                    </li>
                    <li>
                        <a href="{{URL::to('/login')}}" class="stext-107 cl7 hov-cl1 trans-04">Đăng nhập bán
                            hàng</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-6 col-lg-2 p-b-50 p-t-30">
                <h4 class="stext-301 cl0 p-b-30">
                    Về chúng tôi
                </h4>
                <ul>
                    <li class="p-b-10">
                        <a href="{{URL::to('/gtbt')}}" class="stext-107 cl7 hov-cl1 trans-04"
                           alt="tiêu chuẩn cộng đồng">
                            Giới thiệu về BISTO
                        </a>
                    </li>
                    <li class="p-b-10">
                        <a href="{{URL::to('/tccd')}}" class="stext-107 cl7 hov-cl1 trans-04"
                           alt="tiêu chuẩn cộng đồng">
                            Tiêu chuẩn cộng đồng
                        </a>
                    </li>
                    <li class="p-b-10">
                        <a href="{{URL::to('/qchd')}}" class="stext-107 cl7 hov-cl1 trans-04">
                            Quy chế hoạt động
                        </a>
                    </li>
                    <li class="p-b-10">
                        <a href="{{URL::to('/dkdv')}}" class="stext-107 cl7 hov-cl1 trans-04">
                            Điều khoản dịch vụ
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-6 col-lg-2 p-b-50 p-t-30">
                <h4 class="stext-301 cl0 p-b-30">
                    CSKH
                </h4>
                <ul>
                    <li class="p-b-10">
                        <a href="{{URL::to('/htbh')}}" class="stext-107 cl7 hov-cl1 trans-04">
                            Hợp tác bán hàng
                        </a>
                    </li>
                    <li class="p-b-10">
                        <a href="{{URL::to('/gqtc')}}" class="stext-107 cl7 hov-cl1 trans-04">
                            Giải quyết tranh chấp
                        </a>
                    </li>
                    <li class="p-b-10">
                        <a href="{{URL::to('/csdt')}}" class="stext-107 cl7 hov-cl1 trans-04">
                            Chính sách đổi trả
                        </a>
                    </li>
                    <li class="p-b-10">
                        <a href="{{URL::to('/vcdg')}}" class="stext-107 cl7 hov-cl1 trans-04">
                            Vận chuyển đóng gói và khiếu nại bồi thường
                        </a>
                    </li>
                    <li class="p-b-10">
                        <a href="{{URL::to('/bmtt')}}" class="stext-107 cl7 hov-cl1 trans-04">
                            Bảo mật
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-6">
                <h4 class="stext-301 cl0 p-t-30 p-b-30">
                    Đóng góp
                </h4>
                <form action="https://mail.google.com/mail/u/0/#inbox">
                    <div class="wrap-input1 w-full p-b-4" style="max-width: 500px;">
                        <input class="input1 bg-none plh1 stext-107 cl7" type="text" name="email"
                               placeholder="Anberry.vn@gmail.com">
                        <div class="focus-input1 trans-04"></div>
                    </div>
                    <div class="p-t-18">
                        <button class="flex-c-m stext-101 cl0 size-103 bg1 bor1 hov-btn2 p-lr-15 trans-04">
                            Gửi
                        </button>
                    </div>
                </form>
            </div>
            <div class="col-sm-6 col-lg-6 " style="padding-left: 80px;">
                <img src="{{asset('frontend_landingpage/images/icons/SVG/company.svg')}}" alt="ICON-PAY" width="100">
                <h4 class="stext-301 cl0 p-t-30">
                    Công ty TNHH ANBERRY Việt Nam
                </h4>
                <p class="stext-107 cl7 size-301">
                    <i class="fa fa-phone"></i> 0963228283 | Mã số thuế: 0109366605
                </p>
                <p class="stext-107 cl7 size-401">
                    <i class="fa fa-map-marker" aria-hidden="true"></i> Số 82 Vương Thừa Vũ, phường Khương Trung, quận
                    Thanh Xuân, thành phố Hà Nội.
                </p>
                <p class="stext-107 cl7 size-401">
                    Giấy phép ĐKKD số 0109366605 do Sở Kế Hoạch và Đầu tư TP Hà Nội cấp ngày 07/10/2020
                </p>
            </div>
        </div>
        <div class="p-t-30">
            <h4 class="stext-301 cl0 p-b-30 txt-center">
                Thanh toán
            </h4>
            <div class="flex-c-m flex-w p-b-18">
                <a href="#" class="m-all-1 pay-bor">
                    <img src="{{asset('frontend_landingpage/images/icons/atm.png')}}" alt="ICON-PAY" width="60px"
                         height="38.8px;">
                </a>
                <a href="#" class="m-all-1 pay-bor">
                    <img src="{{asset('frontend_landingpage/images/icons/vnpt.jpg')}}" alt="ICON-PAY" width="60px"
                         height="38.8px;">
                </a>
                <a href="#" class="m-all-1 pay-bor">
                    <img src="{{asset('frontend_landingpage/images/icons/momo.png')}}" alt="ICON-PAY" width="60px"
                         height="38.8px;">
                </a>
                <a href="#" class="m-all-1 pay-bor">
                    <img src="{{asset('frontend_landingpage/images/icons/zalo.png')}}" alt="ICON-PAY" width="60px"
                         height="38.8px;">
                </a>
                <a href="#" class="m-all-1 pay-bor">
                    <img src="{{asset('frontend_landingpage/images/icons/icon-pay-02.png')}}" alt="ICON-PAY"
                         width="60px" height="38.8px;">
                </a>
            </div>
            <h4 class="stext-301 cl0 p-t-30 p-b-30 txt-center">
                Đơn vị vận chuyển
            </h4>
            <div class="flex-c-m flex-w">
                <a href="#" class="m-all-1">
                    <img src="{{asset('frontend_landingpage/images/icons/ghn.png')}}" alt="ICON-PAY" height="100px">
                </a><a href="#" class="m-all-1 pay-img">
                    <img src="{{asset('frontend_landingpage/images/icons/vnpay.png')}}" alt="ICON-PAY" height="100px">
                </a>
                <a href="#" class="m-all-1 pay-img">
                    <img src="{{asset('frontend_landingpage/images/icons/ahamove.png')}}" alt="ICON-PAY" height="40px">
                </a>
            </div>
        </div>
    </div>
</footer>
<div class="btn-back-to-top" id="myBtn">
        <span class="symbol-btn-back-to-top">
            <i class="zmdi zmdi-chevron-up"></i>
        </span>
</div>
<script src="{{asset('frontend_landingpage/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('frontend_landingpage/vendor/animsition/js/animsition.min.js')}}"></script>
<script src="{{asset('frontend_landingpage/vendor/bootstrap/js/popper.js')}}"></script>
<script src="{{asset('frontend_landingpage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontend_landingpage/vendor/select2/select2.min.js')}}"></script>
<script>
    $(".js-select2").each(function () {
        $(this).select2({
            minimumResultsForSearch: 20,
            dropdownParent: $(this).next('.dropDownSelect2')
        });
    })
</script>
<script src="{{asset('frontend_landingpage/vendor/daterangepicker/moment.min.js')}}"></script>
<script src="{{asset('frontend_landingpage/vendor/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('frontend_landingpage/vendor/slick/slick.min.js')}}"></script>
<script src="{{asset('frontend_landingpage/js/slick-custom.js')}}"></script>
<script src="{{asset('frontend_landingpage/vendor/parallax100/parallax100.js')}}"></script>
<script>
    $('.parallax100').parallax100();
</script>
<script src="{{asset('frontend_landingpage/vendor/MagnificPopup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('frontend_landingpage/vendor/isotope/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('frontend_landingpage/vendor/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('frontend_landingpage/vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script>
    $('.js-pscroll').each(function () {
        $(this).css('position', 'relative');
        $(this).css('overflow', 'hidden');
        var ps = new PerfectScrollbar(this, {
            wheelSpeed: 1,
            scrollingThreshold: 1000,
            wheelPropagation: false,
        });
        $(window).on('resize', function () {
            ps.update();
        })
    });
</script>
<script src="{{asset('frontend_landingpage/js/main.js')}}"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script defer src="../../../static.cloudflareinsights.com/beacon.min.js"
        data-cf-beacon='{"rayId":"60d2e1b72f7e21be","version":"2020.12.2","si":10}'></script>
<script type="text/javascript">
    $(document).ready(function () {
        const $blogDropdown = $("#blog-cate-list");
        const $blogDropdownM = $("#blog-cate-list-m");

        $.ajax({
            url: `{{url('/_blog-categories')}}?page=1&limit=20&keyword=`,
            method: 'GET'
        }).done(res => {
            const blogCategories = res?.results?.data ?? [];
            for (const blogCategory of blogCategories) {
                $blogDropdown.append(`<li><a href="{{URL::to('/blog-categories')}}/${blogCategory?.slug ?? "#"}">${blogCategory?.name ?? ""}</a></li>`)
                $blogDropdownM.append(`<li><a href="{{URL::to('/blog-categories')}}/${blogCategory?.slug ?? "#"}">${blogCategory?.name ?? ""}</a></li>`)
            }
        }).fail(err => {
        })
    })

</script>
</body>

</html>
