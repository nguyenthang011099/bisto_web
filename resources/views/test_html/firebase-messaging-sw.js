importScripts("https://www.gstatic.com/firebasejs/8.2.9/firebase-app.js");
importScripts(
    "https://www.gstatic.com/firebasejs/8.6.2/firebase-messaging.js",
);
// For an optimal experience using Cloud Messaging, also add the Firebase SDK for Analytics.
importScripts(
    "https://www.gstatic.com/firebasejs/8.2.9/firebase-analytics.js",
);

// importScripts("https://www.gstatic.com/firebasejs/7.16.1/firebase-app.js");
// importScripts(
//     "https://www.gstatic.com/firebasejs/7.16.1/firebase-messaging.js",
// );
// // For an optimal experience using Cloud Messaging, also add the Firebase SDK for Analytics.
// importScripts(
//     "https://www.gstatic.com/firebasejs/7.16.1/firebase-analytics.js",
// );


// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
    apiKey: "AIzaSyCkblEIURnfzV9r388MKfzMqrt6W3LPkkY",
    authDomain: "quanlh-49a4f.firebaseapp.com",
    projectId: "quanlh-49a4f",
    storageBucket: "quanlh-49a4f.appspot.com",
    messagingSenderId: "937817936447",
    appId: "1:937817936447:web:60f67b02b0c975166a5e8a",
    measurementId: "G-3BNWYD10B9"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    console.log(
        "[firebase-messaging-sw.js] Received background message ",
        payload,
    );
    // Customize notification here
    const notificationTitle = "Background Message Title";
    const notificationOptions = {
        body: "Background Message body.",
        icon: "/itwonders-web-logo.png",
    };

    return self.registration.showNotification(
        notificationTitle,
        notificationOptions,
    );
});
