@extends('admin-shop.layout')
@section('admin_shop_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách danh mục sản phẩm
                </div>
                <div class="row w3-res-tb row-action-bisto-css">
                    <div class="col-sm-7">
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control" name="option-category-level">
                            <option value="1" selected>Cấp độ 1</option>
                            <option value="2">Cấp độ 2</option>
                            <option value="3">Cấp độ 3</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group btn-search-bisto">
                            <input onkeydown="searchCategory()" type="search" placeholder="&#xf002 Tìm kiếm"
                                   class="form-control btn-search-bisto-css"
                                   name="keywords_submit">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th class="text-center">Tên danh mục</th>
                                <th class="text-center">Thuộc danh mục</th>
                                <th class="text-center">Hình ảnh</th>
                            </tr>
                            </thead>
                            <tbody id="body-table-cate">
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div style="" id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
    </div>
    <script>
        let dt = {currentPage: 1, level: 1, searchKey: '', loading: {colspan: 4, type: 'table'}};
        let pagination, _i;

        $(document).ready(function () {
            _i = defineImage({id: 'category'});
            loadCategory(dt);
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadCategory(dt);
                }
            });
        });

        function generateCategories(categories) {
            let tbody = '';
            _i.resetPhotos();
            for (let i = 0; i < categories.length; i++) {
                const id = categories[i].id
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${categories[i].name}</td>
                            <td class="text-center">${categories[i].parent_name}</td>
                            <td class="text-center">
                                ${_i.toHTML(categories[i].image_url, categories[i].name)}</td>
                          </tr>`;
            }
            $('.table-striped>tbody').empty().append(tbody);
            _i.animateOnloadImage({});
        }

        function loadCategory({currentPage = 1, level = 1, searchKey = '', loading = {colspan: 4, type: 'table'}}) {
            $.ajax({
                url: `categories/query?keyword=${searchKey}&level=${level}&page=${currentPage}`,
                method: "GET",
                loading
            }).done(res => {
                const categories = res?.categories?.data ?? [];
                if (categories.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadCategory(dt);
                } else if (categories.length === 0) {
                    $('.table-striped>tbody').empty().append(genNoContentTable(4, 'Không có danh mục nào'));
                } else {
                    generateCategories(categories);
                    pagination.setTotalItems(res.categories.total);
                }
            }).fail(err => {
            })
        }

        $('select[name=option-category-level]').on('change', function () {
            dt.level = $(this).val();
            loadCategory(dt);
        })

        function searchCategory() {
            if (event.key === 'Enter') {
                dt.searchKey = $('input[name=keywords_submit]').val();
                loadCategory(dt);
            }
        }
    </script>
@endsection
