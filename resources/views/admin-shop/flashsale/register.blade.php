@extends('admin-shop.layout')
@section('admin-shop_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách chương trình Flashsale
                </div>
                <div class="row w3-res-tb row-action-bisto-css">
                    <div class="col-sm-5 m-b-xs">
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search" placeholder="&#xf002 Tìm kiếm"
                                   class="btn-search-bisto-css input-sm form-control"
                                   name="keywords_submit">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th class="text-center">Tên chương trình</th>
                                <th class="text-center">Mô tả</th>
                                <th class="text-center">Đồng giá</th>
                                <th class="text-center">Hình ảnh quảng bá</th>
                                <th class="text-center">Từ ngày - Đến ngày</th>
                                <th class="text-center">Khung giờ</th>
                                <th class="text-center">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
    </div>

    <script type="text/javascript">
        let dt = {
            currentPage: 1, searchKey: '', loading: {colspan: 8, type: 'table'}, isEdit: false
        };
        let pagination, _i;
        $(document).ready(function () {
            _i = defineImage({id: 'flashsale'});
            loadFlashSales(dt);
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadFlashSales(dt);
                }
            });
        });

        function generateFlashsale(flashSales) {
            _i.resetPhotos();
            let tbody = '';
            for (let index = 0; index < flashSales.length; index++) {
                const id = flashSales[index].id;

                const startDate = toDate(flashSales[index].start_date);
                const endDate = toDate(flashSales[index].end_date);

                tbody += `<tr>
                    <td class="text-center">${index + 1}</td>
                    <td class="text-center">${flashSales[index].name}</td>
                    <td class="text-center">${flashSales[index].description}</td>
                    <td class="text-center">
                        <span class="text-ellipsis">${toVND(flashSales[index].amount)}</span>
                    </td>
                    <td>
                        ${_i.toHTML(flashSales[index].image, flashSales[index].name)}
                    </td>
                    <td class="text-center text-nowrap">${moment(flashSales[index].start_date).format('DD-MM-YYYY')} đến ${moment(flashSales[index].end_date).format('DD-MM-YYYY')}</td>
                    <td class="text-center text-nowrap">${flashSales[index].start_time ? flashSales[index].start_time.substring(0, 5) : '00:00'} - ${flashSales[index].end_time ? flashSales[index].end_time.substring(0, 5) : '00:00'}</td>
                    <td class="text-center">
                          <a type="button" href="flashsales/${id}" class="btn btn-primary">
                            Đăng ký
                        </a>
                    </td>
                </tr>`;
            }
            $('.table-striped>tbody').empty().append(tbody);
            _i.animateOnloadImage({});
        }

        function loadFlashSales({currentPage = 1, searchKey = '', loading = {colspan: 8, type: 'table'}}) {
            const _url = `flashsales?page=${currentPage}&keyword=${searchKey}`
            $.ajax({
                url: _url,
                loading,
                method: 'GET',
            })
                .done(res => {
                    let flashSales = res?.FlashSales?.data ?? [];
                    if (flashSales?.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        loadFlashSales(dt);
                    } else if (flashSales.length === 0 || res === undefined) {
                        $('.table-striped>tbody').empty().append(genNoContentTable(8, 'Không có chương trình nào'));
                    } else {
                        generateFlashsale(flashSales);
                        pagination.setTotalItems(res.FlashSales.total);
                    }
                })
                .fail(err => {
                    toastError(err, 'Không thể load ');
                });
        }

        document.querySelector('input[name=keywords_submit]').addEventListener('keydown', function (e) {
            if (e.key === 'Enter') {
                dt.searchKey = e.target.value;
                loadFlashSales(dt);
            }
        })
    </script>
@endsection
