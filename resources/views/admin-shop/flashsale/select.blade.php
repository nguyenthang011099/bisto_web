@extends('admin-shop.layout')
@section('admin-shop_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <section class="panel">
                    <header class="panel-heading">
                        Đăng ký chương trình flashsale
                    </header>
                    <div class="panel-body">
                        <div class="position-center">
                            <div class="row row-action-bisto-css">
                                <div class="col-sm-3 d-flex align-items-center justify-content-between">
                                    <h4 class="selected-product-discount" style="margin-left: 20px">Đã
                                        chọn 0 sản phẩm</h4><span type="button"
                                                                  class="fa fa-times fa-lg delete-all-product-discount"></span>
                                </div>
                                <div class="col-sm-5"></div>
                                <div class="input-group col-sm-4">
                                    <input type="search" placeholder="&#xf002 Tìm kiếm" onkeydown="searchProduct()"
                                           class="input-sm form-control btn-search-bisto-css" name="keywords_submit">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="iq-card-body">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <ul class="list-group overflow-auto l-pr-dis border" style="height: 20rem">
                                            </ul>
                                            <div class="super-center mt-2">
                                                <button id="apply" class="btn btn-primary">Áp dụng</button>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">STT</th>
                                                    <th class="text-center">Tên sản phẩm</th>
                                                    <th class="text-center">Ảnh</th>
                                                    <th class="text-center">Giá gốc</th>
                                                    <th class="text-center">Khuyến mãi</th>
                                                    <th class="text-center">Nhập số lượng sản phẩm</th>
                                                    <th class="text-center">Thao tác</th>
                                                </tr>
                                                </thead>
                                                <tbody class="list-product">
                                                </tbody>
                                            </table>
                                            <!-- PAGINATION START-->
                                            <div class="pagination-custom">
                                                <div style="" id="pagination-container"></div>
                                            </div>
                                            <!-- PAGINATION END-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            {{--            "{{$data}}"--}}
        </div>
    </div>
    <!-- select all checkobx -->
    <script>
        const flashsaleId = {{$id}};
        let dt = {currentPage: 1, searchKey: '', loading: {type: 'table', colspan: 7}}
        let productIds = [], pagination, _i, quantities = [];
        let productIdsIsRegistered = [], quantitiesIsRegistered = [];
        $(document).ready(function () {
            _i = defineImage({id: 'product'});
            getProductIsRegistered();
            validateRangeNumber('input[name=discount]', 0, 100);
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadProducts(dt);
                }
            });
        })

        //when user apply
        $(document).on('click', '#apply', function () {
            if (productIds.length === 0) return toastInfoMessage('Bạn chưa chọn sản phẩm nào');
            $.ajax({
                url: '{{url('/shop/flashsales')}}',
                method: 'POST',
                data: {
                    _token: '{{csrf_token()}}',
                    product_ids: productIds,
                    flash_sale_id: flashsaleId,
                    quantities,
                }
            }).done(res => {
                getProductIsRegistered();
                toastSuccess(res, 'Đăng kí thành công');
            }).fail(err => {
            })
        });

        //add product to list discount
        $(document).on('click', '.add-product-discount', function () {
            const productId = parseInt($(this).attr('productid'));
            const $inputQuantity = $(`input[name=quantity-${productId}]`);
            if (!validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Số lượng là bắt buộc'
                }, {
                    func: rangeValidation,
                    mes: 'Số lượng lớn hơn 0',
                    params: {
                        min: 1,
                        max: MAX_INT32,
                    }
                }],
                propertyName: `quantity-${productId}`
            })) {
                return;
            }
            $(this).attr('disabled', 'disabled');
            if (productIds.includes(productId)) {
                return toastInfoMessage('Sản phẩm đã được thêm vào danh sách của chương trình');
            }
            const productName = $(this).attr('productname');
            productIds.push(productId);
            quantities.push(parseInt($inputQuantity.val()));
            $inputQuantity.prop('readonly', true);
            const productDiscountItemElement = `<li class="border-left-0 border-right-0 d-flex justify-content-between list-group-item text-wrap"
                                                    type="button">${productName}<span>${$inputQuantity.val()}</span><i productid=${productId} class="super-center delete-product-discount fa fa-times"
                                                        aria-hidden="true"></i></li>`;
            $('.l-pr-dis').append(productDiscountItemElement);
            $('.selected-product-discount').text(`Đã chọn ${productIds.length} sản phẩm`);
        })

        //remove product from list discount
        $(document).on('click', '.delete-product-discount', function () {
            const productId = parseInt($(this).attr('productid'));
            $(`input[name=quantity-${productId}]`).prop('readonly', false);
            $(`button[productid=${productId}]`).attr('disabled', false);
            const index = productIds.indexOf(productId)
            productIds.splice(index, 1);
            quantities.splice(index, 1);
            $(this).parent().remove();
            $('.selected-product-discount').text(`Đã chọn ${productIds.length} sản phẩm`);
        });

        //remove all product from list discount
        $(document).on('click', '.delete-all-product-discount', function () {
            for (const productId of productIds) {
                $(`input[name=quantity-${productId}]`).prop('readonly', false);
                $(`button[productid=${productId}]`).attr('disabled', false);
            }
            productIds = [];
            quantities = [];
            $(".l-pr-dis").empty();
            $('.selected-product-discount').text(`Đã chọn 0 sản phẩm`);
        });

        //products to interface
        function generateProducts(products) {
            let tbody = '';
            //                            <button type="button" class="text-danger btn btn-link btn-sm">Hủy</button>
            _i.resetPhotos();
            for (let i = 0; i < products.length; i++) {
                let id = products[i].id;
                let quantity = 0;
                let isRegisted = false;
                let btn = `<button productid=${id} productname="${products[i].name}" class="btn btn-primary add-product-discount">Chọn</button>`;
                if (productIdsIsRegistered.includes(id)) {
                    isRegisted = true;
                    btn = `<span class="badge badge-success">Đã đăng ký</span>
                            `;
                    quantity = quantitiesIsRegistered[productIdsIsRegistered.findIndex(item => item === id) ?? 0];
                }
                if (productIds.includes(id)) {
                    btn = `<button productid=${id} productname="${products[i].name}" disabled class="btn btn-primary add-product-discount">Chọn</button>`;
                }
                tbody += `<tr>
			    <td class="text-center">${i + 1}</td>
				<td class="text-center">${products[i].name}</td>
		        <td class="text-center">
                    ${_i.toHTML(products[i].image, products[i].name)}
                </td>
				<td class="text-center">${products[i].price.formatVND()}</td>
				<td class="text-center">${products[i].discount} %</td>
				<td class="text-center">
                     <input value="${quantity}" type="number" class="form-control" ${isRegisted && "readonly"} placeholder="Nhập số lượng" name="quantity-${id}"/>
                    <div class="mes-quantity-${id} mes-v-display"></div>
                </td>
				<td class="text-center">
                     ${btn}
                </td>
			</tr>`;
            }
            $('tbody.list-product').empty().append(tbody);
            _i.animateOnloadImage({});
        }

        //call api get product by paging and generate on interface
        function loadProducts({currentPage = 1, searchKey = '', loading = {type: 'table', colspan: 7}}) {
            $.ajax({
                url: `{{url('/shop')}}/products?page=${currentPage}&keyword=${searchKey}`,
                method: 'GET',
                loading
            })
                .done(res => {
                    const products = res?.products?.data ?? [];
                    if (products.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadProducts(dt);
                    } else if (products.length === 0) {
                        $('.table-striped>tbody').empty().append(genNoContentTable(7, `Không có sản phẩm nào`));
                    } else {
                        generateProducts(products);
                        pagination.setTotalItems(res.products.total);
                    }
                })
                .fail(err => {
                });
        }

        function getProductIsRegistered() {
            $.ajax({
                url: `{{url('/shop')}}/flashsales/products/${flashsaleId}`,
                method: 'GET',
                loading: {type: 'table', colspan: 7}
            })
                .done(res => {
                    productIds = [];
                    quantities = [];
                    productIdsIsRegistered = res.product_ids;
                    quantitiesIsRegistered = res.quantities;
                    $('.l-pr-dis').empty();
                    $('.selected-product-discount').text(`Đã chọn 0 sản phẩm`);
                    loadProducts(dt);
                })
                .fail(err => {
                    console.log(err)
                });
        }

        //handle event when user typing enter on input search
        function searchProduct(e) {
            // user press enter search
            if (event.key === 'Enter') {
                dt.searchKey = $('input[name=keywords_submit]').val();
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadProducts(dt)
            }
        }
    </script>
@endsection
