@extends('admin-shop.layout')
@section('admin_shop_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div style="padding: 5px" class="iq-card">
                <div class="panel-heading">
                    Thông tin vận chuyển hàng
                </div>

                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                        <tr>
                            <th class="text-center">Tên người nhận</th>
                            <th class="text-center">Địa chỉ</th>
                            <th class="text-center">Thanh toán</th>
                            <th class="text-center">Đơn vị vận chuyển</th>
                            <th class="text-center">Tình trạng đơn</th>
                        </tr>
                        </thead>

                        <tbody class="shipping-info">
                        </tbody>
                    </table>
                </div>
                <div class="panel-heading">
                    Danh sách chi tiết đơn hàng
                </div>

                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Tên sản phẩm</th>
                            <th class="text-center">Số lượng mua</th>
                            <th class="text-center">Đơn giá</th>
                            <th class="text-center">Giá tiền</th>
                        </tr>
                        </thead>
                        <tbody class="order-products">
                        </tbody>
                    </table>
                    <div class="general-info">
                    </div>
                </div>

                <div class="super-center row text-center mb-3">
                    <div class="col-sm-6  d-flex justify-content-around order-action">
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Tình Trạng mặt hàng
                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped b-t b-light">
                            <thead>
                            <tr>
                                <th class="text-center">Ngày mua</th>
                                <th class="text-center">Ngày vận chuyển</th>
                                <th class="text-center">Lý do đổi/trả hàng</th>
                                <th class="text-center">Hình ảnh</th>
                            </tr>
                            </thead>
                            <tbody class="order-return">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        let _i;
        $(document).ready(function () {
            loadOrderDetail();
            _i = defineImage({id: 'order_return'});
        })

        function genShippingInfo(res) {
            const {shipping_buyer, order, shipping_order} = res;
            let tbody = `<tr>
                            <td class="text-center">${shipping_buyer.full_name}</td>
                            <td class="text-center">${shipping_buyer.address}</td>
                            <td class="text-center">
                                ${order.payment_method === 0 ? 'Chuyển khoản' : 'Tiền mặt'}
                            </td>
                            <td class="text-center"> ${order.provider_name ?? ''}</td>
                            <td class="text-center" style="color: ${order.color};"> ${order.status}</td>
                        </tr>`
            $('tbody.shipping-info').empty().append(tbody);
        }


        function genOrderProducts(res) {
            console.log(res);
            const {products, order} = res;
            let tbody = '';
            let totalMoney = 0;
            // <td class="text-center">${products[i]?.discount} %</td>
            // <td class="text-center">${(products[i]?.discount * products[i].price / 100 * products[i].quantity).formatVND()}</td>
            for (let i = 0; i < products.length; i++) {
                let discount = 0;
                if (isUnexpired(products[i].start_at, products[i].end_at)) {
                    discount = products[i].discount;
                }
                const pricePerProduct = isEmptyValue(products[i].price) ? 0 : products[i].price;
                let totalMoneyProduct = (pricePerProduct) * products[i].quantity;
                totalMoney += totalMoneyProduct;
                tbody += `<tr class="product-order-${products[i].id}">
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${products[i].name}</td>
                            <td class="text-center">${products[i].quantity}</td>
                            <td class="text-center">${pricePerProduct.formatVND()}</td>
                            <td class="text-center">${totalMoneyProduct.formatVND()}</td>
                        </tr>`;

            }

            const coupon = order?.coupon ?? {discount_voucher: 0};
            const generalInfo = `
                        <tr>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="font-weight-bolder text-center">Voucher:</td>
                            <td class="text-center">-${coupon.discount_voucher.formatVND()}</td>
                        </tr>
                        <tr>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center font-weight-bolder">Phí vận chuyển:</td>
                            <td class="text-center">${order.shipping_fee.formatVND()}</td>
                        </tr>
                        <tr>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center"></td>
                            <td class="text-center font-size-18 font-weight-bolder">Tổng số tiền:</td>
                            <td class="text-center font-size-18 font-weight-bolder">${(order.total_money + order.shipping_fee).formatVND()}</td>
                        </tr>
                       `
            $('tbody.order-products').empty().append(tbody).append(generalInfo);
            // $('div.general-info').empty().append(generalInfo);
        }

        function genOrderReturn(res) {
            _i.resetPhotos();
            const {order_return} = res;
            if (isEmptyValue(order_return)) return $('tbody.order-return').empty().append(`<tr><td colspan="4" class="text-center">Không có thông tin</td></tr>`);
            let imgsHTML = '';
            for (const url of order_return.source_urls) {
                imgsHTML += `<div class="col super-center">${_i.toHTML(url, order_return.reason ?? '')}</div>`;
            }
            let tbody = `<tr>
                        <td class="text-center">${order_return.created_at.formatDate()}</td>
                        <td class="text-center">${toDate(order_return.shipping_date)}</td>
                        <td class="text-center">${order_return.reason}</td>
                        <td> <div class="container px-sm-1"><div class="row mx-sm-n1 row-cols-${order_return?.source_urls?.length ?? 0}">${imgsHTML}</div></div></td>
                    </tr>`;
            $('tbody.order-return').empty().append(tbody);
            _i.animateOnloadImage({});
        }

        function loadOrderDetail() {
            const orderId = window.location.pathname.split('/').pop();
            $.ajax({
                url: `{{url('/admin-shop')}}/orders/${orderId}`,
                method: 'GET',
                loading: {type: 'content'},
            }).done(res => {
                genShippingInfo(res);
                genOrderProducts(res);
                genOrderReturn(res);
            }).fail(err => {
            })
        }
    </script>

@endsection
