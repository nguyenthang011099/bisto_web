@extends('admin-shop.layout')
@section('admin_shop_content')
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div id="loading-content">
                        <div class="loading-content-css">
                            <div class="spinner-border text-primary" role="status">
                            </div>
                        </div>
                    </div>
                    <div class="iq-card">
                        <div class="iq-card-body chat-page p-0">
                            <div class="chat-data-block">
                                <div class="row">
                                    <div class="col-lg-3 chat-data-left scroller">
                                        <div class="chat-search pt-3 pl-2">
                                            <div class="chat-searchbar">
                                                <div class="form-group chat-search-data m-0">
                                                    <input type="text" class="form-control round search-user" name=""
                                                           placeholder="Tìm kiếm">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="chat-sidebar-channel scroller mt-2 p-2">
                                            <ul class="iq-chat-ui nav flex-column nav-pills container-conversation">

                                            </ul>
                                        </div>
                                    </div>

                                    <div class="col-lg-9 chat-data p-0 pr-4 chat-data-right">
                                        <div class="tab-content">
                                            <div class="tab-pane fade active show" id="default-block" role="tabpanel">
                                                <div class="chat-start">
                                                <span class="iq-start-icon text-primary"><i
                                                        class="ri-message-3-line"></i></span>
                                                    <button id="chat-start" class="btn bg-white mt-3">Hãy bắt đầu cuộc
                                                        trò
                                                        chuyện nào!
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="chatbox" role="tabpanel">
                                                <div class="loading-selector" style="display: none">
                                                    <div class="loading-center">
                                                        <div class="spinner-border text-primary" role="status">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="chat-head">
                                                    <header
                                                        class="d-flex justify-content-between align-items-center bg-white pt-3 pr-3 pb-3">
                                                        <div class="d-flex align-items-center ml-2">
                                                            <div id="sidebar-toggle" class="sidebar-toggle">
                                                                <i class="ri-menu-3-line"></i>
                                                            </div>
                                                            <div class="avatar chat-user-profile m-0 mr-3">
                                                                <img src="" alt="avatar" class="avatar-50 ">
                                                            </div>
                                                            <h5 class="mb-0"></h5>
                                                        </div>
                                                    </header>
                                                </div>

                                                <!-- content message -->
                                                <div class="chat-content p-1 scroller">
                                                </div>

                                                <div class="chat-footer p-2">
                                                    <div class="form-message d-flex align-items-center">
                                                        <div
                                                            class="chat-attachment d-flex justify-content-between align-items-center"
                                                            style="width: 3rem">
                                                            <div><i class="fa fa-smile-o fa-lg"
                                                                    aria-hidden="true"></i>
                                                            </div>
                                                            <label for="file-comment" class="file-comment mb-0">
                                                                <i
                                                                    class="fa fa-paperclip fa-lg text-secondary mr-1"></i>
                                                            </label>
                                                            <input id="file-comment" type="file" hidden class=""
                                                                   multiple/>
                                                        </div>
                                                        <input type="text" class="form-control input-message"
                                                               placeholder="Nhập lời nhắn">
                                                        <button
                                                            class="btn btn-primary d-flex align-items-center p-2 btn-send ml-1">
                                                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                                                            <span class="d-none d-lg-block">Gửi</span>
                                                        </button>
                                                    </div>
                                                    <div class="container px-sm-1">
                                                        <div id="img-container"
                                                             class="message-img-container row-cols-6 row mx-sm-n1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="notif-pull-to-bottom">
                                                        Có tin nhắn mới
                                                    </div>
                                                    <div class="btn-pull-to-bottom">
                                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        let contextMessaging = {
            receiver_avatar: '',
            receiverId: 0,
            currentConversationId: 0,
            length: 0,
            currentPage: 1,
            scrollHeight: 0,
            currentNumberImg: 0,
            isEnd: false,
            isFirstLoad: true,
        }
        let dt = {
            currentPage: 1,
            searchKey: '',
            loading: {type: 'content'},
            conversations: [],
            defaultConversations: [],
            isFirstLoad: true,
        };
        let uploaderImageComment;
        let _i;
        let $chatcontent, $inputText;
        $(document).ready(function () {
            $inputText = $('.input-message');
            $chatcontent = $('.chat-content');
            _i = defineImage({id: 'message'});
            loadConversations(dt);
            uploaderImageComment = defineAsyncUploadFile({
                id: 'message',
                inputSelector: `#file-comment`,
                containerSelector: `#img-container`,
                imageName: 'source_urls',
                maxFiles: 10,
                cbWhenAddedFile: function () {
                    window.scrollTo(0, document.body.scrollHeight);
                }
            });
        });

        function clearMessage() {
            $inputText.val('');
            uploaderImageComment.getContainer().empty();
        }

        $(document).on('click', 'li.user-chat', function (e) {
            $('li.user-chat').removeClass('is-messaging');
            e.currentTarget.classList.add('is-messaging');
            contextMessaging = {
                receiver_avatar: '',
                receiverId: 0,
                currentConversationId: 0,
                length: 0,
                currentNumberImg: 0,
                currentPage: 1,
                scrollHeight: 0,
                isEnd: false,
                isFirstLoad: true,
            }
            contextMessaging.receiverId = parseInt(e.currentTarget.dataset.receiverid);
            window.location.hash = contextMessaging.receiverId;
            const conversation = dt.defaultConversations.find(item => item.receiver_id === contextMessaging.receiverId);
            contextMessaging.receiver_avatar = e.currentTarget.querySelector('.avatar-reveiver').src;
            contextMessaging.currentConversationId = conversation.conversation_id;
            $(".user-chat.active").removeClass("active");
            $chatcontent.empty();
            $('.chat-head h5').text(conversation.full_name);
            const avatar_image = conversation.avatar_image ? conversation.avatar_image : DEFAULT_AVATAR;
            $('.chat-head img').attr('src', avatar_image);
            $(`#conservation-${contextMessaging.receiverId}`).addClass('active').find('.preview-message').removeClass('is-not-seen-message');
            loadDetailConversation();
        });

        function getHTMLMessage(message, index, _i, isNewMessage = false) {
            let imgsHTML = '';
            const none_class = isNewMessage ? '' : 'd-none';
            const length = message?.source_urls?.length ?? 0;
            for (let j = 0; j < length; j++) {
                contextMessaging.currentNumberImg++;
                imgsHTML += `<div class="col super-center">${_i.toHTML(message.source_urls[j], index + j)}</div>`;
            }
            if (message.user_id != USER_ID) {
                let avatar_image = message.avatar_image ? message.avatar_image : DEFAULT_AVATAR;
                return `       <div class="chat-bisto"><div class="chat chat-left d-flex ml-2">
                                                            <div class="chat-user">
                                                                <a class="avatar m-0">
                                                                    <img src="${avatar_image}"
                                                                        class="avatar-40 img-thumbnail" alt="bisto">
                                                                </a>
                                                            </div>
                                                            <div class="chat-detail">
                                                                <div class="chat-message m-0">
                                                                    <p>${message.content}</p>
                                                                    <div class="chat-image-left">
                                                                        <div class="container container-img-chat px-sm-1">
                                                                            <div class="message-img-container row-cols-${length} row mx-sm-n1">
                                                                                ${imgsHTML}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <div class="collapse collapse-left-${index}">
                                                                            <div class="card p-0 card-body">
                                                                                ${message.created_at.formatDate()}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="chat-info-left chat-info">
                                                                <div class="dropdown chat-info-left-dropdown">
                                                                    <!-- <span class="fa fa-ellipsis-h" type="button"
                                                                        id="dropdownMenuButton" data-toggle="dropdown"
                                                                        aria-haspopup="true" aria-expanded="false">
                                                                    </span> -->
                                                                    <div>${message.created_at.formatDate()}</div>

                                                                    <div class="dropdown-menu"
                                                                        aria-labelledby="dropdownMenuButton">
                                                                        <div type="button" class="dropdown-item"
                                                                            data-toggle="collapse"
                                                                            data-target=".collapse-left-${index}">Chi
                                                                            tiết</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div></div>`;
            } else {
                return `                            <div class="chat-bisto">
                                                        <div class="chat d-flex justify-content-end">
                                                            <div class="chat-info-right chat-info">
                                                                <div class="dropdown chat-info-right-dropdown">
                                                                     <!--<span class="fa fa-ellipsis-h" type="button"
                                                                        id="dropdownMenuButton"
                                                                        data-toggle="dropdown" aria-haspopup="true"
                                                                        aria-expanded="false">
                                                                    </span>-->
                                                                    <div>${message.created_at.formatDate()}</div>

                                                                    <div class="dropdown-menu"
                                                                        aria-labelledby="dropdownMenuButton">
                                                                        <div type="button" class="dropdown-item">Chi tiết</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="chat-detail">
                                                                <div class="chat-message">
                                                                    <p>${message.content}</p>
                                                                    <div class="chat-image">
                                                                        <div class="container container-img-chat px-sm-1">
                                                                            <div class="message-img-container row-cols-${length} row justify-content-end d-flex mx-sm-n1">
                                                                                 ${imgsHTML}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div style="width:1rem;" class="d-flex align-items-end">
                                                              <i class="${none_class} m-1 is-sent-icon-${index} text-primary fa fa-circle-o" aria-hidden="true"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                        `;
            }
        }

        function loadDetailConversation() {
            $('#default-block').removeClass('show active');
            $('#chatbox').addClass('active show');
            if (contextMessaging.currentConversationId !== 0) {
                $.ajax({
                    url: `{{url('/admin-shop/messages')}}/${contextMessaging.currentConversationId}?page=${contextMessaging.currentPage}`,
                    method: 'GET',
                    loading: {type: 'content', selector: '.loading-selector'}
                }).done(res => {
                    let outputs = [], html = '';
                    const items = res?.results?.data ?? [];

                    if (items.length === 0) {
                        contextMessaging.isEnd = true;
                        return;
                    }
                    const oldNumberImg = contextMessaging.currentNumberImg;
                    for (let i = 0; i < items.length; i++) {
                        outputs[i] = getHTMLMessage(items[i], i + contextMessaging.length, _i);
                    }
                    contextMessaging.length += items.length;
                    if (contextMessaging.isFirstLoad) {
                        contextMessaging.isFirstLoad = false;
                        $chatcontent.empty().prepend(outputs.reverse());
                        _i.resetIndexPhotos({});
                        _i.animateOnloadImage({});
                        scrollToBottom();
                        clearMessage();
                        $inputText.focus();
                    } else {
                        $chatcontent.prepend(outputs.reverse());
                        _i.resetIndexPhotos({});
                        _i.animateOnloadImage({});
                        $chatcontent[0].scrollTop = $chatcontent[0].scrollHeight - contextMessaging.scrollHeight;
                    }
                    contextMessaging.scrollHeight = $chatcontent[0].scrollHeight;
                    onHoverMessage();
                }).fail(err => {
                    $('#default-block').addClass('show active');
                    $('#chatbox').removeClass('active show');
                });
            }
        }

        function onHoverMessage() {
            $('.chat-bisto').hover(function (e) {
                e.currentTarget.querySelector('.chat-info').style.display = 'block';
            }, function (e) {
                e.currentTarget.querySelector('.chat-info').style.display = 'none';
            })
        }

        FCM.onComingMessage((payload) => {
            if (payload?.data?.isMes === '1') {
                const message = {
                    ...payload.data,
                    avatar_image: contextMessaging.receiver_avatar,
                    created_at: moment().format(),
                    source_urls: parseJSON(payload.data.source_urls) ?? []
                }
                if (payload?.data?.user_id != USER_ID && contextMessaging.receiverId == payload?.data?.user_id) {
                    appendMessage(message, message?.user_id);
                } else {
                    appendConversation(message, message?.user_id);
                }
                // playSoundMessage();
            }
        })

        function appendMessage(message, receiverId, isNewMessage = false) {
            const output = getHTMLMessage(message, contextMessaging.length, _i, isNewMessage);
            contextMessaging.length += 1;
            const oldNumberImg = contextMessaging.currentNumberImg - 1;
            $chatcontent.append(output);
            _i.animateOnloadImage({});
            appendConversation(message, receiverId);
            onHoverMessage();
            if (isNewMessage) {
                scrollToBottom();
            } else {
                if ($('.btn-pull-to-bottom').css('display') === 'block') {
                    $('.notif-pull-to-bottom').show();
                } else {
                    scrollToBottom();
                }
            }
        }

        function appendConversation(message, receiverId) {
            let $userChat = $(`#conservation-${receiverId}`);
            if ($userChat.length !== 0) {
                if (isFirstElementInParent($userChat) === false) {
                    moveChildElementToFirstOrder($userChat);
                }
            } else {
                const old = dt.defaultConversations.find(item => item.conversation_id == message?.conversation_id);
                console.log(old);
                const conversation = {
                    ...old,
                    content: message?.content ?? '',
                    created_at: toDate(moment().format()),
                    source_urls: message?.source_urls ?? [],
                    type: message?.type,
                }
                $('.container-conversation').prepend(getConversationHTML(conversation));
            }
            if (message.user_id != USER_ID) {
                const text = `${message.source_urls.length ? `Đã gửi ảnh` : message.content}`;
                $(`#conservation-${receiverId}`).find('.preview-message').text(text).addClass('is-not-seen-message');
            } else {
                const text = `Bạn: ${message.source_urls.length ? `Đã gửi ảnh` : message.content}`;
                $(`#conservation-${receiverId}`).find('.preview-message').text(text).removeClass('is-not-seen-message');
            }
        }

        function scrollToBottom() {
            const chatContent = document.querySelector('.chat-content');
            chatContent.scrollTop = chatContent.scrollHeight;
        }

        document.querySelector('.chat-content').addEventListener('scroll', function (e) {
            const space = e.target.scrollHeight - e.target.scrollTop;
            if (space > 800) {
                $('.btn-pull-to-bottom').show();
            } else {
                $('.btn-pull-to-bottom').hide();
            }
            if (e.target.scrollTop === 0 && !contextMessaging.isFirstLoad) {
                // console.log('to top');
                contextMessaging.currentPage += 1;
                if (!contextMessaging.isEnd)
                    loadDetailConversation();
            }
        });

        document.getElementsByClassName('input-message')[0].addEventListener("paste", function (e) {
            document.getElementById('file-comment').dispatchEvent(new CustomEvent('change', {detail: {fileList: e.clipboardData.files}}));
        });

        $(document).on('click', '.btn-pull-to-bottom', function (e) {
            scrollToBottom();
            $('.btn-pull-to-bottom').hide();
            $('.notif-pull-to-bottom').hide();
        })

        $(document).on('click', '.notif-pull-to-bottom', function (e) {
            scrollToBottom();
            $('.btn-pull-to-bottom').hide();
            $('.notif-pull-to-bottom').hide();
        })

        function sendMessage() {
            const text = $inputText.val();
            let source_urls_local = uploaderImageComment.getLocalFileUrls().map(item => item.url) ?? [];
            // const _source_urls = formToData('#img-container').source_urls;
            // if (_source_urls) source_urls = Array.isArray(_source_urls) ? _source_urls : [_source_urls];
            if (isEmptyValue(text) && source_urls_local.length === 0)
                return;
            const message = {
                user_id: USER_ID,
                content: text,
                conversation_id: contextMessaging.currentConversationId,
                source_urls: source_urls_local,
                created_at: moment().format(),
                receiver_id: contextMessaging.receiverId,
            }
            const isFirstTime = contextMessaging.currentConversationId === 0;
            const index = contextMessaging.length;
            appendMessage(message, contextMessaging.receiverId, true);
            clearMessage();
            uploaderImageComment.asyncUploadImage(() => {
                const source_urls = uploaderImageComment.getFileUrls().map(item => item.url);
                $.ajax({
                    url: '{{url('/admin-shop/messages')}}',
                    method: 'POST',
                    data: {
                        _token: '{{csrf_token()}}',
                        contents: text,
                        conversation_id: contextMessaging.currentConversationId,
                        source_urls,
                        receiver_id: contextMessaging.receiverId,
                    }
                }).done(res => {
                    if (isFirstTime) {
                        contextMessaging.currentConversationId = res?.conversation_id ?? 0;
                        const index = dt.conversations.findIndex(item => item.receiver_id === contextMessaging.receiverId);
                        dt.conversations[index].conversation_id = contextMessaging.currentConversationId;
                        const index1 = dt.defaultConversations.findIndex(item => item.receiver_id === contextMessaging.receiverId);
                        dt.defaultConversations[index1].conversation_id = contextMessaging.currentConversationId;
                    }
                    $(`.is-sent-icon-${index}`).removeClass('fa-circle-o').addClass('fa-check-circle-o');
                    uploaderImageComment.resetAllFiles();
                }).fail(err => {
                    $(`.is-sent-icon-${index}`).removeClass('fa-circle-o').addClass('fa-times-circle');
                    uploaderImageComment.resetAllFiles();
                })
            })
        }

        function getConversationHTML(conversation) {
            const textSender = conversation?.user_id == USER_ID ? 'Bạn: ' : '';
            let avatar_image = conversation.avatar_image ? conversation.avatar_image : DEFAULT_AVATAR;
            const id = conversation.conversation_id;
            let created_at = conversation.created_at ? conversation.created_at.formatDate() : '';
            return `
                       <li class="user-chat border-bottom p-0"  style="width: 100%" data-id="${id ? id : 0}" id="conservation-${conversation.receiver_id}"
                                        data-userid="${conversation.user_id}" data-receiverid="${conversation.receiver_id}">
                            <div type="button" class="d-flex justify-content-between">
                                <div class="p-1 mr-1">
                                    <div class="avatar-50">
                                    <img src="${avatar_image}"
                                         class="rounded-circle avatar-50 avatar-reveiver" alt="${conversation.full_name}">
                                    </div>
                                </div>
                                <div class="chat-sidebar-name d-flex justify-content-between flex-column">
                                    <div class="d-flex justify-content-between align-items-start">
                                        <p class="mb-0 font-weight-bold">${conversation.full_name}</p>
                                        <span class="text-nowrap d-block font-size-12 ml-2">
                                            ${created_at}
                                        </span>
                                    </div>
                                    <div>
                                        <p class="preview-message text-truncate">${textSender}${conversation.source_urls.length ? `Đã gửi ảnh` : conversation.content}</p>
                                    </div>
                                </div>
                            </div>
                    </li>`;
        }

        function generateConversations(conversations) {
            let html = '';
            dt.conversations = conversations;
            if (dt.searchKey === '') dt.defaultConversations = conversations;
            for (let i = 0; i < conversations.length; i++) {
                html += getConversationHTML(conversations[i]);
            }
            $('.container-conversation').empty().append(html);
            if (dt.isFirstLoad) {
                const receiver_id = window.location.hash.split('#')[1];
                if (receiver_id) {
                    const conversationElement = document.getElementById(`conservation-${receiver_id}`);
                    $('li.user-chat').removeClass('is-messaging');
                    conversationElement.classList.add('is-messaging');
                    contextMessaging = {
                        receiver_avatar: '',
                        receiverId: 0,
                        currentConversationId: 0,
                        length: 0,
                        currentNumberImg: 0,
                        currentPage: 1,
                        scrollHeight: 0,
                        isEnd: false,
                        isFirstLoad: true,
                    }
                    contextMessaging.receiverId = parseInt(conversationElement.dataset.receiverid);
                    const conversation = dt.defaultConversations.find(item => item.receiver_id === contextMessaging.receiverId);
                    contextMessaging.receiver_avatar = conversationElement.querySelector('.avatar-reveiver').src;
                    contextMessaging.currentConversationId = conversation.conversation_id;
                    $(".user-chat.active").removeClass("active");
                    $chatcontent.empty();
                    $('.chat-head h5').text(conversation.full_name);
                    const avatar_image = conversation.avatar_image ? conversation.avatar_image : DEFAULT_AVATAR;
                    $('.chat-head img').attr('src', avatar_image);
                    $(`#conservation-${contextMessaging.receiverId}`).addClass('active').find('.preview-message').removeClass('is-not-seen-message');
                    loadDetailConversation();
                }
                dt.isFirstLoad = false;
            }
        }

        function loadConversations({currentPage = 1, searchKey = '', loading = {type: 'content'}}) {
            $.ajax({
                url: `{{url('/admin-shop/messages')}}?keyword=${searchKey}`,
                method: 'GET',
                loading
            }).done(res => {
                res?.results && generateConversations(res.results);
            }).fail(err => {
            })
        }

        $('input.search-user').on('keydown', function (e) {
            if (e.key === 'Enter') {
                dt.searchKey = e.currentTarget.value;
                loadConversations(dt);
            }
        })

        $('.btn-send').on('click', function (e) {
            sendMessage();
        });

        $('.input-message').on("keydown", function (e) {
            if (e.keyCode === 13) {
                sendMessage();
            }
        });

        // document.querySelector('.preview-image-instruction')
        //     .addEventListener('drop', (ev) => {
        //         ev.preventDefault();
        //         document.querySelector('.image-url').files = ev.dataTransfer.files;
        //     });
    </script>

@endsection
