@extends('admin-shop.layout')
@section('admin_shop_content')
    <div id="content-page" class="content-page">
        <div class="iq-card" id="brand">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="panel-heading">
                Danh sách Thương hiệu
            </div>

            <div class="row w3-res-tb row-action-bisto-css">
                <div class="col-sm-5 m-b-xs">
                    <button class="btn-add-bisto btn-show-add-bisto-css btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#modal-brand">
                        + Thêm thương hiệu
                    </button>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input id="input-text-brand"
                               onkeydown="searchBrand()" type="search" placeholder="&#xf002 Tìm kiếm"
                               class="input-sm form-control btn-search-bisto btn-search-bisto-css"
                               name="keywords_submit">
                    </div>
                </div>
            </div>

            <div class="iq-card-body">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center">STT</th>
                        <th class="text-center">Tên thương hiệu</th>
                        <th class="text-center">Hình ảnh</th>
                        <th class="text-center">Thao tác</th>
                    </tr>
                    </thead>

                    <tbody id="tbl-brand">
                    </tbody>
                </table>
            </div>
            <!-- PAGINATION START-->
            <div class="pagination-custom">
                <div style="" id="pagination-container"></div>
            </div>
            <!-- PAGINATION END-->
            <!-- footer and pagination -->
        </div>
        <div class="row justify-content-md-center row-size">
            <div class="col-sm col-sm-left">
            </div>
            <div class="col-sm col-sm-center">
                <div class="panel-heading">
                    Thông tin thanh toán
                </div>
                <table class="table table-bordered mt-4 text-left">
                    <tr>
                        <th>Tên công ty</th>
                        <td>Công ty TNHH Anberry Việt Nam</td>
                    </tr>
                    <tr>
                        <th>Loại ngân hàng</th>
                        <td>Ngân Hàng TMCP Á Châu ACB</td>
                    </tr>
                    <tr>
                        <th>Tên tài khoản nhận</th>
                        <td>Công ty TNHH Anberry Việt Nam</td>
                    </tr>
                    <tr>
                        <th>Số tài khoản nhận</th>
                        <td>9119 6868 6868</td>
                    </tr>
                </table>
            </div>
            <div class="col-sm col-sm-right">
            </div>
        </div>
        <div class="modal-container">
            <div id="modal-brand" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content modal-bisto-css">
                        <header class="panel-heading">
                            <h5 class="modal-title">Thêm thương hiệu</h5>
                        </header>
                        <section class="modal-body panel">
                            <div class="panel-body">
                                <div class="iq-card-body">
                                    <div id="form-brand">
                                        <div class="form-group">
                                            <label class="required-input">Tên thương hiệu</label>
                                            <input type="text" name="name" class="form-control"/>
                                            <div class="mes-name mes-v-display"></div>
                                        </div>
                                        <div class="form-group" style="overflow: auto">
                                            <label class="required-input">Hình ảnh thương hiệu</label><span
                                                class="note-img-1-11 note-img"></span>
                                            <div class="custom-file">
                                                <input name="brand-image" type="file"
                                                       class="custom-file-input inputfile"
                                                       id="image-brand">
                                                <div id="img-container" class="row align-items-start">
                                                </div>
                                                <label class="custom-file-label" for="customFile">Chọn
                                                    ảnh</label>
                                            </div>
                                            <div class="mes-imageUrl mes-v-display">
                                                Bạn phải thêm ít nhất 1 ảnh
                                            </div>
                                        </div>
                                    </div>
                                    <div class="">
                                        <button class="btn-save-brand btn-add-bisto-css btn btn-primary">
                                            Thêm thương hiệu
                                        </button>
                                        <button
                                            class="bisto-none btn-update-brand btn btn-update-bisto-css btn-primary">
                                            Cập nhật thương hiệu
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        let dt = {currentPage: 1, searchKey: '', loading: {colspan: 6, type: 'table'}};
        let pagination;
        let cropperBrand;
        let uploaderBrand, _i;
        $(document).ready(function () {
            _i = defineImage({id: 'brand'});
            loadBrand(dt);
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadBrand(dt);
                }
            });
            let _totalFiles = 0;
            uploaderBrand = defineUploadFile({id: 'brand', containerSelector: '#img-container'})
            cropperBrand = defineCropAndUploadImage({
                type: 'brand',
                inputSelector: 'input[name=brand-image]', cbWhenAddFile: () => {
                    if (uploaderBrand.getTotalFile() > 0) {
                        toastInfoMessage('Cho phép chọn tối đa 1 ảnh thương hiệu');
                        cropperBrand.stopUpload();
                    }
                }
            });
            cropperBrand.saveButton.addEventListener('click', function () {
                let u = uploaderBrand;
                _totalFiles = u.getTotalFile() + 1;
                const {fileElement} = u.createFileElement(cropperBrand.getImageUrl(), cropperBrand.getImgName());
                u.getFileUrls().push(cropperBrand.getImg())
                u.setTotalFile(_totalFiles);
                u.getContainer().append(fileElement);
                uploaderBrand.setIsNewUpdateImg(true);
                cropperBrand.$modal.modal('hide');
            });
        });

        function generateBrands(brands) {
            _i.resetPhotos();
            let tbody = '';
            for (let i = 0; i < brands.length; i++) {
                let id = brands[i].id;
                tbody += `<tr>
                                    <td class="text-center">${i + 1}</td>
                                    <td class="text-center">${brands[i].name}</td>
                                    <td class="text-center">
                                        ${_i.toHTML(brands[i].url, brands[i].name)}
                                    </td>
                                    <td class="text-center">
                                        <span brandid=${id} type="button" class="fas fa-lg fa-edit text-success btn-show-update-bisto-css btn-show-update-brand"></span>
                                        <span brandid=${id} type="button" class="btn-delete-brand">
                                            <i class="fa fa-times text-danger fa-lg btn-delete-bisto-css text"></i>
                                        </span>
                                    </td>
                                </tr>`;
            }
            $('.table-striped>tbody').empty().append(tbody);
            _i.animateOnloadImage({});
        }

        function loadBrand({currentPage = 1, searchKey = '', loading = {type: 'table'}}) {
            $.ajax({
                url: `{{url('/admin-shop')}}/brands?page=${currentPage}&keyword=${searchKey}`,
                method: 'GET',
                loading
            })
                .done(res => {
                    let brands = res?.brands?.data ?? [];
                    if (brands.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadBrand(dt);
                    } else if (brands.length === 0) {
                        $('.table-striped>tbody').empty().append(genNoContentTable(6, 'Không có brand nào'));
                    } else {
                        generateBrands(brands);
                        pagination.setTotalItems(res.brands.total);
                    }
                })
                .fail(err => {
                });
        }

        $(document).on('click', '.btn-delete-brand', function () {
            const brandId = $(this).attr('brandid');
            confirmDelete('Bạn có chắc là muốn xóa brand này ko?', function () {
                $.ajax({
                    url: `brands/${brandId}`,
                    method: 'DELETE',
                    data: {
                        _token: '{{csrf_token()}}'
                    },
                    loading: {type: 'content'}
                })
                    .done(res => {
                        toastSuccess(res, 'Xóa brand thành công');
                        loadBrand({...dt, loading: {type: 'content'}});
                    })
                    .fail(err => {
                    });
            })
        })

        function searchBrand(e) {
            // user press enter search
            if (event.key === 'Enter') {
                dt.searchKey = $('#input-text-brand').val();
                loadBrand(dt);
            }
        }

        function validateFormBrand() {
            return validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Tên thương hiệu là bắt buộc'
                }],
                propertyName: 'name'
            }) && validateRequiredElement({
                inputSelector: 'input[name=imageUrl]',
                mesSelector: '.mes-imageUrl',
                message: 'Bạn phải thêm ít nhất một ảnh',
            })
        }

        $('.btn-add-bisto', '#brand').on('click', function () {
            $('#modal-brand').modal('show');
            $('#modal-brand .modal-title').text('Thêm thương hiệu');
            $('.btn-save-brand').show();
            $('.btn-update-brand').hide();
            dataToForm({
                fieldInputs: [{selector: 'input[name=name]'}],
                fieldFiles: [{
                    uploaderInstance: uploaderBrand,
                    listFiles: []
                }],
                parentSelector: '#modal-brand', action: 'clear-validate'
            })
        })

        $('.btn-save-brand').on('click', function () {
            if (validateFormBrand()) {
                const dataObj = getDataFromForm('#form-brand');
                $.ajax({
                    url: "{{url('/admin-shop/brands')}}",
                    method: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        ...dataObj,
                        source_url: uploaderBrand.getOneImg()
                    },
                })
                    .done(res => {
                        toastSuccess(res, 'Thêm mới thương hiệu thành công');
                        $('#modal-brand').modal('hide');
                        loadBrand({...dt, loading: {type: 'content'}});
                    })
                    .fail(err => {
                        $('#modal-brand').modal('hide');
                        console.log(err);
                    })
            }
        })

        $(document).on('click', '.btn-show-update-brand', function () {
            const brandId = $(this).attr('brandid');
            $.ajax({
                url: `{{url('/admin-shop')}}/brands/${brandId}`,
                method: 'GET',
            })
                .done(res => {
                    const brand = res.brand;
                    $('input[name=name]').val(brand.name);
                    let _fileUrls = [];
                    const _totalFiles = 1;
                    uploaderBrand.getContainer().empty();
                    for (let i = 0; i < _totalFiles; i++) {
                        const {fileElement} = uploaderBrand.createFileElement(brand.url, brand.id);
                        _fileUrls.push({name: brand.id, url: brand.url, hash: '', size: 0, type: ''});
                        uploaderBrand.getContainer().append(fileElement);
                    }
                    uploaderBrand.setTotalFile(_totalFiles);
                    uploaderBrand.setFileUrls(_fileUrls);
                })
                .fail(err => {
                    console.log(err);
                })
            $('#modal-brand').modal('show');
            $('#modal-brand .modal-title').text('Cập nhật thương hiệu');
            $('.btn-save-brand').hide();
            $('.btn-update-brand').show().data('brandid', brandId);
        })

        $('.btn-update-brand').on('click', function () {
            const id = $(this).data('brandid');
            if (validateFormBrand()) {
                const dataObj = getDataFromForm('#form-brand');
                $.ajax({
                    url: `brands/${id}`,
                    method: 'PUT',
                    data: {
                        _token: "{{csrf_token()}}",
                        ...dataObj,
                        source_url: uploaderBrand.getOneImgUpdate()
                    },
                })
                    .done(res => {
                        toastSuccess(res, 'Cập nhật thương hiệu thành công');
                        $('#modal-brand').modal('hide');
                        loadBrand({...dt, loading: {type: 'content'}});
                    })
                    .fail(err => {
                        $('#modal-brand').modal('hide');
                        console.log(err);
                    })
            }
        })
    </script>
@endsection
