@extends('admin-shop.layout')
@section('admin_shop_content')
    <div id="content-page" class="content-page license-admin-shop">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Giấy phép kinh doanh
                </div>
                <div style="margin-top: 20px" class="row btn-bisto row-action-bisto-css">
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-4 super-center">
                        <div class="btn btn-primary btn-show-add-bisto btn-show-add-bisto-css bisto-none col-sm-6">+
                            Thêm giấy phép kinh doanh
                        </div>
                        <div class=" text-white btn btn-danger btn-delete-bisto bisto-none col-sm-6">Xóa
                            giấy phép
                            kinh doanh
                        </div>
                    </div>
                    <div class="col-sm-3">
                    </div>
                </div>

                <div style="margin-top: 20px" class="row">
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-4">
                        <div class="iq-card-body body-bisto">
                        </div>
                    </div>
                    <div class="col-sm-4">
                    </div>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->

            </div>
        </div>
        <!-- ADD PRODUCT START -->
        <div id="modal-container">
            <div class="modal fade modal-bisto" tabindex="-1" role="dialog"
                 aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content" style="border-radius: 20px">
                        <div class="iq-card">
                            <header class="panel-heading title-modal-bisto">
                                Thêm giấy phép kinh doanh
                            </header>
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="iq-card-body">
                                        <div class="form-bisto" role="form">
                                            @csrf
                                            <div class="form-group">
                                                <label class="required-input">Tên giấy phép kinh doanh</label>
                                                <input type="text" name="name" class="form-control">
                                                <div class="mes-name mes-v-display">
                                                    Tên giấy phép là bắt buộc
                                                </div>
                                            </div>

                                            <div class="form-group" style="overflow: auto">
                                                <label class="required-input">Hình ảnh giấy phép</label><span
                                                    class="note-img note-img-2-normal"></span>
                                                <div class="custom-file">
                                                    <input type="file"
                                                           class="form-control-file custom-file-input input-file-bisto"
                                                           multiple>
                                                    <div id="img-container" class="row align-items-start">
                                                    </div>
                                                    <label class="custom-file-label" for="customFile">Chọn
                                                        ảnh</label>
                                                </div>
                                                <div class="mes-image_urls mes-v-display">
                                                    Bạn phải thêm ít nhất 1 ảnh
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Mô tả</label>
                                                <textarea class="form-control textarea-bisto" name="description"
                                                          placeholder="Mô tả giấy phép kinh doanh"></textarea>
                                            </div>
                                        </div>
                                        <div>
                                            <button class="btn btn-primary btn-add-bisto">
                                                Lưu giấy phép
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-container">
        </div>
    </div>
    <script>
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: 4}};
        let _i;
        let uploaderLicense;
        let _;

        $(document).ready(function () {
            _i = defineImage({});
            _ = defineBisto({
                parentSelector: '.license-admin-shop', name: 'giấy phép kinh doanh', v: [
                    {
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Tên giấy phép là bắt buộc'
                        }],
                        propertyName: 'name'
                    },
                    {
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Bạn phải thêm ít nhất 1 ảnh'
                        }],
                        propertyName: 'image_urls'
                    }]
            });
            loadLicense(dt);
            uploaderLicense = defineUploadFile({
                inputSelector: '.input-file-bisto',
                containerSelector: '#img-container', imageName: 'image_urls', maxFiles: 2
            })

            _.onDelete((id) => {
                $.ajax({
                    url: `{{url('/admin-shop/license')}}`,
                    method: 'DELETE',
                    data: {
                        _token: '{{csrf_token()}}'
                    }
                }).done(res => {
                    toastSuccess(res, 'Xóa thành công');
                    loadLicense(dt);
                }).fail(err => {
                })
            })

            _.onAdd((data) => {
                //data.image_urls = Array.isArray(data.image_urls) ? data.image_urls : [data.image_urls];
                $.ajax({
                    url: `{{url('/admin-shop/license')}}`,
                    method: "POST",
                    data: {
                        ...data,
                        image_urls: uploaderLicense.getMultiImg(),
                    },
                    loading: {type: 'content'},
                }).done(res => {
                    toastSuccess(res, 'Lưu thành công');
                    _.$modal.modal('hide');
                    loadLicense(dt);
                }).fail(err => {
                });
            })
            _.onShowAdd(() => {
            })
        });

        function loadLicense({currentPage = 1, keyword = '', loading = {type: 'table', colspan: 4}}) {
            $.ajax({
                url: `{{url('/admin-shop/license')}}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
                loading: {type: 'content'},
            }).done(res => {
                if (!res) {
                    _.$btnDelete.hide();
                    _.$btnShowAdd.show();
                    _.$body.empty().append('<h3>Chưa có giấy phép kinh doanh</h3>')
                } else {
                    _.$btnDelete.show();
                    _.$btnShowAdd.hide();
                    generateLicense(res.result)
                }
            }).fail(err => {
            });
        }

        function generateLicense(license) {
            let body = '';
            let imgs = license.image_urls.map(item => {
                return {src: item, title: license.name}
            })
            let status;
            if (license.admin_status === 1) {
                status = `<span class="badge badge-success">Chấp nhận</span>`
            } else if (license.admin_status === 0) {
                status = `<span class="badge badge-warning">Đang chờ duyệt</span>`
            } else if (license.admin_status === -1) {
                status = `<span class="badge badge-danger">Từ chối</span>`
            }
            body += `<dl class="row">
                      <dt class="col-sm-3">Tên giấy phép</dt>
                      <dd class="col-sm-9">${license.name}</dd>
                      <dt class="col-sm-3">Mô tả</dt>
                      <dd class="col-sm-9">
                        <p>${license.description ?? ''}</p>
                      </dd>
                      <dt class="col-sm-3">Trạng thái</dt>
                      <dd class="col-sm-9">${status}</dd>
                      <dt class="col-sm-3">Hình ảnh</dt>
                      <dd class="col-sm-9">
                        ${_i.toMultiHTML(imgs)}
                      </dd>
                    </dl>`;
            _.$body.empty().append(body);
            _i.animateOnloadImage({});
        }

        {{--$(document).on('click', '.btn-show-delete', function () {--}}
        {{--    const id = $(this).attr('sizeimageid');--}}
        {{--    confirmDelete('Bạn có chắc muốn xóa ảnh này', function () {--}}
        {{--        $.ajax({--}}
        {{--            url: `image-sizes/${id}`,--}}
        {{--            method: 'DELETE',--}}
        {{--            data: {--}}
        {{--                _token: "{{csrf_token()}}",--}}
        {{--            }--}}
        {{--        }).done(res => {--}}
        {{--            loadLicense({...dt, loading: {type: 'content'}});--}}
        {{--            toastSuccess(res, 'Xóa ảnh thành công')--}}
        {{--        }).fail(err => {--}}
        {{--            $('.modal').modal('hide');--}}
        {{--        })--}}
        {{--    })--}}
        {{--})--}}
    </script>
@endsection
