@extends('admin-shop.layout')
@section('admin_shop_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách danh sách chi nhánh
                </div>
                <div class="row row-action-bisto-css">
                    <div class="col-sm-3">
                        <button class="btn btn-show-add-bisto-css btn-primary" data-toggle="modal"
                                data-target=".add-shop">+ Thêm cửa hàng
                        </button>
                    </div>
                    <div class="col-sm-6"></div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search" placeholder="&#xf002 Tìm kiếm theo tên chi nhánh"
                                   class="input-sm btn-search-bisto-css form-control"
                                   name="search-user">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Chủ chi nhánh</th>
                            <th class="text-center">Tên chi nhánh</th>
                            <th class="text-center">Số Bcoin</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Số điện thoại</th>
                            <th class="text-center">Địa chỉ</th>
                        </tr>
                        </thead>
                        <tbody class="list-shop">
                        </tbody>
                    </table>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
    </div>
    <!-- ADD SHOP -->
    <div class="modal fade add-shop" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="border-radius: 20px">
                <div class="iq-card">
                    <section class="panel">
                        <header class="panel-heading">
                            Thêm chi nhánh
                        </header>
                        <div class="iq-card-body">
                            <div class="position-center">
                                <div id="form-add-shop">
                                    <div class="form-group">
                                        <label class="required-input">Tên chi nhánh</label>
                                        <input type="text" name="name" class="form-control"/>
                                        <div class="mes-name mes-v-display"></div>
                                    </div>

                                    <div class="form-group">
                                        <label>Mô tả về shop</label>
                                        <textarea type="text" name="description" class="form-control"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label class="required-input">Chủ chi nhánh</label>
                                        <input required type="text" name="full_name" class="form-control"/>
                                        <div class="mes-full_name mes-v-display"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="required-input">Tên đăng nhập</label>
                                        <input required type="text" name="user_name" class="form-control"/>
                                        <div class="mes-user_name mes-v-display"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="required-input">Số điện thoại</label>
                                        <input required type="number" name="phone_number" class="form-control"/>
                                        <div class="mes-phone_number mes-v-display"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="required-input">Email</label>
                                        <input required type="email"
                                               name="email" class="form-control"/>
                                        <div class="mes-email mes-v-display"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="required-input">Mật khẩu</label>
                                        <input required type="password" name="password" class="form-control"/>
                                        <div class="mes-password mes-v-display"></div>
                                    </div>
                                    <button class="btn btn-save-shop btn-primary">Thêm chi nhánh</button>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <!-- END ADD SHOP -->

    <script>
        let dt = {currentPage: 1, keyword: '', loading: {colspan: 7, type: 'table'}}
        let pagination;
        $(document).ready(function () {
            loadListShops(dt);
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadListShops(dt);
                }
            });
        })

        function generateShops(users) {
            let tbody = ''
            for (let i = 0; i < users.length; i++) {
                tbody += `<tr>
                        <td class="text-center">${i + 1}</td>
                        <td class="text-center">${users[i].full_name}</td>
                        <td class="text-center">${users[i].name}</td>
                        <td class="text-center">${users[i].amount || 0}</td>
                        <td class="text-center">${users[i].email}</td>
                        <td class="text-center">${users[i].phone_number}</td>
                        <td class="text-center">${users[i].address}</td>
                    </tr>`;
            }
            $('tbody.list-shop').empty().append(tbody);
        }

        function loadListShops({currentPage = 1, keyword = '', loading = {colspan: 7, type: 'table'}}) {
            const _url = `shops?page=${currentPage}&keyword=${keyword}`;
            $.ajax({
                url: _url,
                method: 'GET',
                loading
            })
                .done(res => {
                    const users = res.results.data;
                    if (users.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadListShops(dt);
                    } else if (users.length === 0) {
                        $('.table-striped>tbody').empty().append(genNoContentTable(9, `Không có chi nhánh nào`));
                    } else {
                        generateShops(users);
                        pagination.setTotalItems(res.results.total);
                    }
                })
                .fail(err => {
                    toastErrorMessage('Load danh sách shop thất bại');
                });
        }

        $(document).on('keydown', 'input[name=search-user]', function () {
            if (event.key === 'Enter') {
                dt.keyword = $(this).val();
                loadListShops(dt);
            }
        })

        function validateFormShop() {
            return validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên chi nhánh là bắt buộc'
                    }],
                    propertyName: 'name'
                }) && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên chủ chi nhánh là bắt buộc'
                    }],
                    propertyName: 'full_name'
                }) && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên đăng nhập là bắt buộc'
                    }],
                    propertyName: 'user_name'
                }) && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Số điện thoại là bắt buộc'
                    }, {
                        func: phoneValidation,
                        mes: 'Số điện thoại không hợp lệ'
                    }],
                    propertyName: 'phone_number'
                })
                && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Email là bắt buộc'
                    }, {
                        func: emailValidation,
                        mes: 'Email không hợp lệ'
                    }],
                    propertyName: 'email'
                }) && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Mật khẩu là bắt buộc'
                    }, {
                        func: passwordValidation,
                        mes: 'Mật khẩu chỉ bao gồm đúng 6 chữ số'
                    }],
                    propertyName: 'password'
                })
        }

        $(document).on('click', '.btn-save-shop', function () {
            if (validateFormShop()) {
                const shop = getDataFromForm('#form-add-shop');
                $.ajax({
                    url: 'shops',
                    method: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        ...shop
                    }
                }).done(res => {
                    $('.modal').modal('hide');
                    dataToForm({
                        fieldInputs: [{selector: 'input[name=name]'},
                            {selector: 'textarea[name=description]'},
                            {selector: 'input[name=full_name]'},
                            {selector: 'input[name=user_name]'},
                            {selector: 'input[name=phone_number]'},
                            {selector: 'input[name=email]'},
                            {selector: 'input[name=password]'},
                        ], action: 'clear-validate'
                    })
                    loadListShops({...dt, loading: {type: 'content'}});
                    toastSuccess(res, 'Thêm chi nhánh mới thành công')
                }).fail(err => {
                    $('.modal').modal('hide');
                })
            }
        })

    </script>
@endsection
