@extends('admin-shop.layout')
@section('admin_shop_content')
    <?php
    $role_text = ['Tài khoản chưa được kích hoạt', 'Người dùng', 'Quản trị shop', 'Quản trị tổng shop', 'Quản trị viên'];
    ?>

    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="iq-card-body">
                    <!--  -->
                    <div class="iq-card-body profile-page p-0">
                        <div class="profile-header">
                            <div class="row" style="margin-bottom: 3%;">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-6">
                                    <!--left col-->
                                    {{--cover--}}
                                    <div class="text-center" style="position: relative;">
                                        <img class="img-fluid cover-show"
                                             id="image-cover" src="" alt="cover">
                                        <label
                                            style="cursor: pointer; color: orange; position: absolute; right: 5%; transform: translateY(-50%)  translateX(-50%); bottom: 0%"
                                            for="cover-img-input"><i class="fas fa-camera"></i></label>
                                        <input id="cover-img-input" type="file"
                                               class="text-center center-block file-upload"
                                               style="cursor: pointer; display: none;">
                                    </div>

                                    {{--avatar--}}
                                    <div class="text-center" id="avatar-admin"
                                         style="z-index: 10; position: absolute;width: 25%;left: 50%;transform: translateY(-85%)  translateX(-50%)">
                                        <img id="image-ava" class="avatar-show img-circle img-thumbnail" src=""
                                             alt="avatar">
                                        <label
                                            style="cursor: pointer; color: orange; position: absolute; left: 50%; transform: translateY(-50%)  translateX(-50%); bottom: 0%"
                                            for="avatar-img-input"><i class="fas fa-camera"></i></label>
                                        <input id="avatar-img-input" type="file"
                                               class="text-center center-block file-upload"
                                               style="cursor: pointer; display: none;">
                                    </div>
                                    <br><br>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Bcoin Admin shop</label>
                                                <div class="d-flex align-items-center d-flex"><h4
                                                        class="bcoins p-2"></h4>&nbsp;<span
                                                        class="fa fa-btc fa-lg p-2"
                                                        aria-hidden="true"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label for="full_name">Chủ chi nhánh</label>
                                                <input type="text" class="form-control" name="full_name">
                                                <div class="mes-full_name mes-v-display">
                                                    Tên chủ chi nhánh là bắt buộc
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label>Bcoin các shop cấp dưới </label>
                                            <div class="d-flex align-items-center d-flex"><h4
                                                    class="bcoins-of-shop p-2"></h4>&nbsp;<span
                                                    class="fa fa-btc fa-lg p-2"
                                                    aria-hidden="true"></span>
                                            </div>
                                            <div>
                                                      <span class="font-weight-bold text-nowrap">
                                                        <a href="{{URL::to('admin-shop/payment')}}"
                                                           style="text-decoration: underline!important;">Thông tin thanh toán</a>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label for="shop_name">Tên chuỗi cửa hàng</label>
                                                <input type="text" class="form-control" name="shop_name">
                                                <div class="mes-shop_name mes-v-display">
                                                    Tên chuỗi cửa hàng là bắt buộc
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="phone">Số điện thoại</label>
                                                <input readonly type="text" class="form-control" name="phone">
                                                <div class="mes-phone mes-v-display">
                                                    Số điện thoại là bắt buộc
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" class="form-control" name="email">
                                                <div class="mes-email mes-v-display">
                                                    Email là bắt buộc
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="admin_role">Quyền</label>
                                                <input type="text" class="form-control" readonly
                                                       id="admin_role" name="admin_role"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label for="description">Mô tả</label>
                                                <input type="text" class="form-control" name="description"/>
                                            </div>
                                            <input type="hidden" name="adm_id" id="shop_id"/>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for=""></label>
                                                <button onclick="onClickButtonSave()" id="btn-save"
                                                        class="btn btn-primary mr-2">Lưu
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal modal-stylish" tabindex="-1">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Chỉnh sửa phong cách</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="select-stylish"></div>
                        <div class="mes-stylish" style="display: none;color: red">
                            Bạn phải chọn ít nhất 1 phong cách
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn-save-stylish btn btn-primary">Lưu lại</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-container">

        </div>
    </div>

    <script>
        let cropperAvatar;
        let cropperCover;
        $(document).ready(function () {
            loadProfile();
            cropperAvatar = defineCropAndUploadImage({inputSelector: '#avatar-img-input'})
            cropperCover = defineCropAndUploadImage({inputSelector: '#cover-img-input', type: 'cover'})

            cropperAvatar.saveButton.addEventListener('click', function () {
                $.ajax({
                    url: '{{url('/admin-shop/upload-avatar')}}',
                    method: 'POST',
                    data: {
                        imageUrl: cropperAvatar.getImg(),
                        _token: '{{csrf_token()}}'
                    }
                }).done(res => {
                    cropperAvatar.$modal.modal('hide');
                    toastSuccess(res, 'Thay đổi avatar thành công');
                    loadProfile();
                }).fail(err => {
                    cropperAvatar.$modal.modal('hide');
                    toastError(err, 'Thay đổi avatar thất bại');
                })
            })

            cropperCover.saveButton.addEventListener('click', function () {
                $.ajax({
                    url: '{{url('/admin-shop/upload-cover')}}',
                    method: 'POST',
                    data: {
                        imageUrl: cropperCover.getImg(),
                        _token: '{{csrf_token()}}'
                    }
                }).done(res => {
                    cropperCover.$modal.modal('hide');
                    toastSuccess(res, 'Thay đổi ảnh bìa thành công');
                    loadProfile();
                }).fail(err => {
                    cropperCover.$modal.modal('hide');
                    toastError(err, 'Thay đổi ảnh bìa thất bại')
                })
            })
        })

        function validateProfileForm() {
            return validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên chủ chi nhánh là bắt buộc'
                    }],
                    propertyName: 'full_name',
                    type: 'onsubmit',
                }) &&
                validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên chuỗi cửa hàng là bắt buộc'
                    }],
                    propertyName: 'shop_name',
                    type: 'onsubmit',
                }) &&
                validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Số điện thoại là bắt buộc'
                    }],
                    propertyName: 'phone',
                    type: 'onsubmit',
                }) &&
                validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Email là bắt buộc'
                    }, {
                        func: emailValidation,
                        mes: 'Email không đúng định dạng'
                    }],
                    propertyName: 'email',
                    type: 'onsubmit', cssClassMes: 'visibility'
                });


        }

        function getProfileObject() {
            let obj = {};
            obj.admin_shop_id = $('input[name=admin_shop_id]').val();
            obj.full_name = $('input[name=full_name]').val();
            obj.email = $('input[name=email]').val();
            obj.shop_name = $('input[name=shop_name]').val();
            obj.description = $('input[name=description]').val();
            obj.phone = $('input[name=phone]').val();
            return obj;
        }

        function loadProfile() {
            let role_text = ['Tài khoản chưa được kích hoạt', 'Người dùng', 'Quản trị shop', 'Quản trị chuỗi shop', 'Quản trị viên'];
            $('#btn-save').prop('disabled', true);
            $.ajax({
                url: "{{url('/admin-shop/profiles')}}",
                method: "get",
                loading: {type: 'content'},
                success: function (obj, textStatus, xhr) {
                    $('.bcoins').text(obj.profile.bcoins.formatComma()).counterUp({
                        delay: 10,
                        time: 1000
                    });
                    $('.bcoins-of-shop').text(obj.profile.total_bcoins_of_shop.formatComma()).counterUp({
                        delay: 10,
                        time: 1000
                    });
                    $("#btn-save").prop('disabled', false);
                    $('input[name=full_name]').val(obj.profile.fullname);
                    $('input[name=shop_name]').val(obj.profile.shop_name);
                    $('input[name=phone]').val(obj.profile.phone);
                    $('input[name=email]').val(obj.profile.email);
                    $('input[name=admin_role]').val(role_text[obj.profile.role]);
                    $('input[name=admin_shop_id]').val(obj.profile.id);
                    $('input[name=description]').val(obj.profile.description);
                    setImage('#image-cover', obj?.profile?.cover_image, 'cover')
                    setImage('#image-ava', obj?.profile?.avatar_image, 'avatar')
                }
            });
        }

        function onClickButtonSave() {
            if (validateProfileForm()) {
                $.ajax({
                    url: "{{url('/admin-shop/profiles')}}",
                    method: "POST",
                    data: {
                        _token: "{{csrf_token()}}",
                        ...getProfileObject(),
                    },
                    loading: {type: 'content'},
                    success: function (obj, textStatus, xhr) {
                        loadProfile();
                        toastSuccess(obj.message, 'Lưu hồ sơ thành công');
                    }
                })
            }

        }
    </script>
@endsection
