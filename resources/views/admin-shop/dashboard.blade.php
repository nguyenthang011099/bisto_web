@extends('admin-shop.layout')
@section('admin_shop_content')
    <?php if (session('change_password')) {
        $message1 = session('change_password');
        session()->remove('change_password');
        echo "<script type='text/javascript'>alert('$message1');</script>";
    }

    if (session('change_profile')) {
        $message = session('change_profile');
        session()->remove('change_profile');
        echo "<script type='text/javascript'>alert('$message');</script>";
    }
    if (session('messageAddress')) {
        $message = session('messageAddress');
        session()->remove('messageAddress');
        echo "<script type='text/javascript'>alert('$message');</script>";
    }
    ?>
    <!-- DASHBOARD -->
    <div id="content-page" class="content-page">
        @csrf
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-3">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <a href="{{URL::to('/admin-shop/all-product')}}">
                            <div class="iq-card-body card-link-bisto">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h6>Tất cả sản phẩm</h6>
                                    <span class="iq-icon"></span>
                                </div>
                                <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
                                    <div class="d-flex align-items-center">
                                        <div class="rounded-circle iq-card-icon iq-bg-primary mr-2"><i
                                                class="far fa-tshirt"></i>
                                        </div>
                                        <h2 class="counter"><?php echo $product ?></h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-3">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <a href="{{URL::to('/admin-shop/manage-order')}}">
                            <div class="iq-card-body card-link-bisto">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h6>Tổng đơn hàng</h6>
                                    <span class="iq-icon"></span>
                                </div>
                                <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
                                    <div class="d-flex align-items-center">
                                        <div class="rounded-circle iq-card-icon iq-bg-danger mr-2"><i
                                                class="ri-radar-line"></i>
                                        </div>
                                        <h2 class="counter"><?php echo $order ?></h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-3">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <a href="{{URL::to('/admin-shop/manage-order?status='.\App\Order::DON_THANH_CONG)}}">
                            <div class="iq-card-body card-link-bisto">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h6>Đơn thành công</h6>
                                    <span class="iq-icon"></span>
                                </div>
                                <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
                                    <div class="d-flex align-items-center">
                                        <div class="rounded-circle iq-card-icon iq-bg-success mr-2"><i
                                                class="fas fa-check"></i>
                                        </div>
                                        <h2 class="counter"><?php echo $order_success ?></h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-3">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <a href="{{URL::to('/admin-shop/manage-order?status='.\App\Order::NGUOI_MUA_HUY_DON)}}">
                            <div class="iq-card-body card-link-bisto">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h6>Đơn hủy</h6>
                                    <span class="iq-icon"></span>
                                </div>
                                <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
                                    <div class="d-flex align-items-center">
                                        <div class="rounded-circle iq-card-icon iq-bg-danger mr-2"><i
                                                class="fas fa-window-close"></i></div>
                                        <h2 class="counter"><?php echo $order_failed ?></h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-3">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <a href="{{URL::to('/admin-shop/manage-order?status='.\App\Order::NGUOI_MUA_TRA_HANG)}}">
                            <div class="iq-card-body card-link-bisto">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h6>Trả Hàng/Chờ Hoàn Tiền Xử Lý</h6>
                                    <!--
                                <span class="iq-icon"><i class="ri-information-fill"></i></span> -->
                                </div>
                                <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
                                    <div class="d-flex align-items-center">
                                        <div class="rounded-circle iq-card-icon iq-bg-warning mr-2"><i
                                                class="fas fa-sync-alt"></i></div>
                                        <h2 class="counter"><?php echo $order_refund ?></h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-3">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <a href="{{URL::to('/admin-shop/all-product?admin_status=-1')}}">
                            <div class="iq-card-body card-link-bisto">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h6>Sản Phẩm Bị Tạm Khóa</h6>
                                </div>
                                <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
                                    <div class="d-flex align-items-center">
                                        <div class="rounded-circle iq-card-icon iq-bg-danger mr-2"><i
                                                class="fas fa-lock"></i>
                                        </div>
                                        <h2 class="counter">{{$count_product_block}}</h2>
                                    </div>
                                    <div class="iq-map text-danger font-size-32"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-3">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <a href="{{URL::to('/admin-shop/product-varieties-page?quantity=0')}}">
                            <div class="iq-card-body card-link-bisto">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h6>Sản Phẩm Hết Hàng</h6>
                                </div>
                                <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
                                    <div class="d-flex align-items-center">
                                        <div class="rounded-circle iq-card-icon iq-bg-warning mr-2"><i
                                                class="fas fa-minus-circle"></i></div>
                                        <h2 class="counter"><?php echo $count_product_sold_out ?></h2>
                                    </div>
                                    <div class="iq-map text-warning font-size-32"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-3">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <a href="{{URL::to('/admin-shop/list-promotion')}}">
                            <div class="iq-card-body card-link-bisto">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h6>Chương Trình Khuyến Mãi <span style="color: red;font-style: oblique">New</span>
                                    </h6>
                                </div>
                                <div class="iq-customer-box d-flex align-items-center justify-content-between mt-3">
                                    <div class="d-flex align-items-center">
                                        <div class="rounded-circle iq-card-icon iq-bg-info mr-2"><i
                                                class="fas fa-money-bill-alt"></i></div>
                                        <h2 class="counter">{{$count_promotion}}</h2>
                                    </div>
                                    <div class="iq-map text-info font-size-32"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="container-fluid">
            <div class="iq-card">
                <header class="panel-heading">
                    Phân tích bán hàng của các shop
                </header>
                <p id="date-now"
                   style="margin:15px;font-size: 15px;font-weight: bold;color: #adadad;text-align: center; ">
                </p>
                <div style="margin-right: 13%; border-radius: 20px;float: right;" class="row">
                    <div class="iq-card-body">
                        <select id="shop-select" class="form-control mb-3">
                            <?php
                            echo "<option value=\"0\">" . "Toàn bộ các shop" . "</option>";
                            foreach ($shops as $index => $shop) {
                                if ($shop) {
                                    echo "<option value=" . ($shop->id) . ">" . $shop->name . "</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="iq-card-body ">
                        <div class="row float-left profile-feed-items">
                            <div>
                                <select id="type-list" class="form-control mb-3">
                                    <option value="1" selected>Tuần</option>
                                    <option value="2">Tháng</option>
                                    <option value="3">Năm</option>
                                </select>
                            </div>
                            <div style="margin-left: 2px;">
                                <select class="form-control mb-3" id="month-list">
                                    <option value="0" selected disable hidden>--chọn tháng--</option>
                                    <?php
                                    $year_now = date('Y');
                                    for ($i = 1; $i <= 12; $i++) {
                                        echo "<option value=" . $i . ">" . $i . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>

                            <div style="margin-left: 2px;">
                                <select class="form-control mb-3" id="year-list">
                                    <option value="0" selected disable hidden>--chọn năm--</option>
                                    <?php
                                    $year_now = date('Y');
                                    for ($i = 2021; $i <= $year_now; $i++) {
                                        echo "<option value=" . $i . ">" . $i . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div style="margin-left: 2px;">
                                <select class="form-control mb-3" id="chart-type">
                                    <option value="1" selected>doanh số</option>
                                    <option value="2">đơn hàng</option>
                                </select>
                            </div>
                            <div>
                                <button style="height: 43px" type="button" onclick="sendChartOptions()"
                                        id="query-button"
                                        class="btn btn-primary mb-3">Truy vấn
                                </button>

                            </div>

                        </div>
                    </div>
                </div>
                <div style="width: 40%;margin-left: 8%;margin-top: 3%;">
                    <p style="font-weight: bold;font-size: 20px;margin-left: 28%;">Tổng doanh số: <a
                            id="total_sales"></a>
                    </p><br>
                </div>
                <div class="col-sm-1">

                </div>
                <div class="col-sm-9" style="margin-left: 13%;">
                    <canvas id="myChart" style="height: 300px;"></canvas>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js "></script>
    <script>

    </script>
    <script>
        function sendChartOptions() {
            $('#query-button').prop('disabled', true);

            const type_list = $('#type-list option:selected').val();
            const month_list = $('#month-list option:selected').val();
            const year_list = $('#year-list option:selected').val();
            const shop_list = $('#shop-select option:selected').val();

            var getUrl = window.location;
            var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
            var finalUrl = baseUrl + '/chart-option-shop';

            let options = {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        },
                        stacked: false,
                        scaleLabel: {
                            display: true,
                            labelString: ''
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        stacked: false,
                        scaleLabel: {
                            display: true,
                            labelString: 'tháng'
                        }
                    }]
                },
                legend: {
                    display: true,
                }
            }

            const data = {
                type_option: type_list,
                // month_option: month_list,
                // year_option: year_list,
                _token: "{{ csrf_token() }}"
            };
            if (type_list == 0) {
                alert('bạn hãy chọn thời gian hiển thị');
                $('#query-button').prop('disabled', false);
                return;
            } else if (type_list == 1) {
                data.month_option = month_list;
                data.year_option = year_list;
                options.scales.xAxes[0].scaleLabel.labelString = 'ngày';
            } else if (type_list == 2) {
                if (month_list == 0) {
                    alert('bạn ãy chọn tháng hiển thị');
                    $('#query-button').prop('disabled', false);
                    return
                } else if (year_list == 0) {
                    alert('bạn hãy chọn năm hiển thị');
                    $('#query-button').prop('disabled', false);
                    return;
                } else {
                    data.month_option = month_list;
                    data.year_option = year_list;
                    options.scales.xAxes[0].scaleLabel.labelString = 'ngày';
                }
            } else if (type_list == 3) {
                if (year_list == 0) {
                    alert('bạn hãy chọn năm hiển thị');
                    $('#query-button').prop('disabled', false);
                    return;
                } else {
                    data.month_option = month_list;
                    data.year_option = year_list;
                    options.scales.xAxes[0].scaleLabel.labelString = 'tháng';
                }
            } else {
                return;
            }

            data.shop_id = shop_list;
            if (shop_list == 0) {
                data.shop_parent_id = <?php echo(session('admin_shop_id')); ?> ;
            }

            jQuery.ajax({
                method: "POST",
                url: finalUrl,
                dataType: 'json',
                data: data,
                success: function (obj, textstatus) {
                    $('#query-button').prop('disabled', false);
                    try {
                        $('#sales-statistic').text(obj.statistic.sales.toLocaleString('it-IT', {
                            style: 'currency',
                            currency: 'VND'
                        }));
                        $('#order-statistic').text(obj.statistic.order_success + " đơn hàng");
                        $('#failed-order-statistic').text(obj.statistic.order_cancle + " đơn hủy");
                    } catch (e) {
                    }

                    const chartType = $('#chart-type option:selected').val();
                    let label = "";
                    let dataChart = {};
                    let xData = obj.label;

                    if (chartType == 1) {
                        options.scales.yAxes[0].ticks.callback =
                            function (label, index, labels) {
                                return label.toLocaleString('it-IT', {
                                    style: 'currency',
                                    currency: 'VND'
                                });
                            }

                        //Change price to VND
                        options.scales = {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value, index, values) {
                                        if (parseInt(value) >= 1000) {
                                            // code change price
                                            return value.toLocaleString('it-IT', {
                                                style: 'currency',
                                                currency: 'VND'
                                            });
                                        } else {
                                            return value + 'VND';
                                        }
                                    }
                                }
                            }]
                        }

                        let dataSales = [];
                        // show all price in order
                        let totalSales = 0;
                        // end
                        obj.result.forEach(function (value, index) {
                            if (!value[index + 1]) {
                                dataSales.push(value.sales);
                                totalSales = totalSales + value.sales;
                            } else {
                                dataSales.push(value[index + 1].sales);
                                totalSales = totalSales + value[index + 1].sales;
                            }
                        });
                        // echo price to VND
                        document.getElementById('total_sales').innerHTML = totalSales.toLocaleString('it-IT', {
                            style: 'currency',
                            currency: 'VND'
                        });
                        ;
                        label = "doanh số";
                        dataChart = {
                            labels: xData,
                            datasets: [{
                                label: label,

                                data: dataSales,
                                borderWidth: 1,
                                borderColor: 'rgb(37,136,14)',
                                lineTension: 0,
                            }]
                        };
                    } else if (chartType == 2) {
                        options.scales.yAxes[0].scaleLabel.labelString = 'đơn hàng';
                        label = "số đơn hàng";
                        let dataOrderSuccess = [];
                        let dataOrderCancle = [];
                        obj.result.forEach(function (value, index) {
                            if (!value[index + 1]) {
                                dataOrderSuccess.push(value.order_success);
                                dataOrderCancle.push(value.order_cancle);
                            } else {
                                dataOrderSuccess.push(value[index + 1].order_success);
                                dataOrderCancle.push(value[index + 1].order_cancle);
                            }
                        });

                        let dataSales = [];
                        // price all order charjs
                        let totalSales = 0;
                        // end
                        obj.result.forEach(function (value, index) {
                            if (!value[index + 1]) {
                                dataSales.push(value.sales);
                                totalSales = totalSales + value.sales;
                            } else {
                                dataSales.push(value[index + 1].sales);
                                totalSales = totalSales + value[index + 1].sales;
                            }
                        });

                        // echo price all order charjs
                        document.getElementById('total_sales').innerHTML = totalSales.toLocaleString('it-IT', {
                            style: 'currency',
                            currency: 'VND'
                        });

                        dataChart = {
                            labels: xData,
                            datasets: [{
                                label: 'đơn hàng',
                                data: dataOrderSuccess,
                                borderWidth: 1,
                                borderColor: 'rgb(64,255,0)',
                                lineTension: 0,

                            },
                                {
                                    label: 'Đơn hủy',
                                    data: dataOrderCancle,
                                    borderWidth: 1,
                                    borderColor: 'rgb(167,0,0)',
                                    lineTension: 0,

                                }
                            ]
                        };
                    }
                    updateChart(dataChart, options);
                },
                fail: function (obj, textstatus) {
                    alert('vui lòng thử lại, có lỗi xảy ra');
                    location.reload();
                }
            });


        }

        function displayOption() {
            const type = $('#type-list option:selected').val();
            if (type == 0) {
                $('#month-list').hide();
                $('#year-list').hide();
            } else if (type == 1) {
                $('#month-list').hide();
                $('#year-list').hide();
            } else if (type == 2) {
                $('#month-list').show();
                $('#year-list').show();
            } else if (type == 3) {
                $('#month-list').hide();
                $('#year-list').show();
            } else {
                $('#month-list').show();
                $('#year-list').show();
            }
        }

        window.addEventListener('load', function () {
            let now = new Date();
            document.getElementById('date-now').innerHTML = 'Ngày ' + now.getDate() + ' tháng ' + (now.getMonth() +
                1) + ' năm ' + now.getFullYear();
            document.getElementById('type-list').addEventListener('change', displayOption);
            $('#month-list').hide();
            $('#year-list').hide();
            sendChartOptions();
        })

        /**
         * update chart
         * @param data
         * @param chartType (line or bar)
         * @param options
         */
        function updateChart(data, options) {
            // this.myChart.destroy();
            var ctx = document.getElementById('myChart').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: data,
                options: options
            });
        }

    </script>

    <!-- END DASHBOARD -->


@endsection
