@extends('admin-shop.layout')
@section('admin_shop_content')
    @php
    $key2 = config('payment.zalopay.key2', '');
    $data = $_GET ?? null;
    @endphp

    @if($data)
        @php
        $checksumData = $data["appid"] ."|". $data["apptransid"] ."|". $data["pmcid"] ."|". $data["bankcode"] ."|". $data["amount"] ."|". $data["discountamount"] ."|". $data["status"];
        $mac = hash_hmac("sha256", $checksumData, $key2);
        @endphp

        <div id="content-page" class="content-page d-flex justify-content-between">
            <div class="col-sm-2 col-lg-2"></div>
            <div class="col-sm-4 col-lg-8">
                <div id="loading-content">
                    <div class="loading-content-css">
                        <div class="spinner-border text-primary" role="status">
                        </div>
                    </div>
                </div>
                <div class="iq-card">
                    <div class="panel-heading">
                        Thông tin đơn hàng
                    </div>
                    <div class="iq-card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered mt-4">
                                <tr>
                                    <td>Mã đơn hàng</td>
                                    <td>{{ $data['apptransid'] }}</td>
                                </tr>
                                <tr>
                                    <td>Số tiền</td>
                                    <td>{{ number_format($data['amount']) }} VNĐ</td>
                                </tr>
                                <tr>
                                    <td>Kết quả</td>
                                    <td>
                                        @if (strcmp($mac, $data["checksum"]) != 0)
                                            <span style="color:red">Chữ ký không hợp lệ</span>
                                        @elseif($data['status'] == 1)
                                            <span style="color:#44ab6c">Giao dịch thành công</span>
                                        @else
                                            <span style="color:red">Giao dịch thất bại</span>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 col-lg-2"></div>
        </div>
    @endif
@endsection
