@extends('admin-shop.layout')
@section('admin_shop_content')
    @php
         $secretKey = config('payment.momo.secret_key');
         $data = $_GET ?? null;
    @endphp

    @if($data)
        @php
            $partnerCode = $_GET["partnerCode"];
            $accessKey = $_GET["accessKey"];
            $orderId = $_GET["orderId"];
            $localMessage = $_GET["localMessage"];
            $message = $_GET["message"];
            $transId = $_GET["transId"];
            $orderInfo = $_GET["orderInfo"];
            $amount = $_GET["amount"];
            $errorCode = $_GET["errorCode"];
            $responseTime = $_GET["responseTime"];
            $requestId = $_GET["requestId"];
            $extraData = $_GET["extraData"];
            $payType = $_GET["payType"];
            $orderType = $_GET["orderType"];
            $extraData = $_GET["extraData"];
            $m2signature = $_GET["signature"]; //MoMo signature

            //Checksum
            $rawHash = "partnerCode=" . $partnerCode . "&accessKey=" . $accessKey . "&requestId=" . $requestId . "&amount=" . $amount . "&orderId=" . $orderId . "&orderInfo=" . $orderInfo .
                "&orderType=" . $orderType . "&transId=" . $transId . "&message=" . $message . "&localMessage=" . $localMessage . "&responseTime=" . $responseTime . "&errorCode=" . $errorCode .
                "&payType=" . $payType . "&extraData=" . $extraData;

            $partnerSignature = hash_hmac("sha256", $rawHash, $secretKey);
        @endphp

        <div id="content-page" class="content-page d-flex justify-content-between">
            <div class="col-sm-2 col-lg-2"></div>
            <div class="col-sm-4 col-lg-8">
                <div id="loading-content">
                    <div class="loading-content-css">
                        <div class="spinner-border text-primary" role="status">
                        </div>
                    </div>
                </div>
                <div class="iq-card">
                    <div class="panel-heading">
                        Thông tin đơn hàng
                    </div>
                    <div class="iq-card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered mt-4">
                                <tr>
                                    <td>Mã đơn hàng</td>
                                    <td>{{ $orderId }}</td>
                                </tr>
                                <tr>
                                    <td>Số tiền</td>
                                    <td>{{ number_format($data['amount']) }} VNĐ</td>
                                </tr>
                                <tr>
                                    <td>Kết quả</td>
                                    <td>
                                        @if ($m2signature !== $partnerSignature)
                                            <span style="color:red">Chữ ký không hợp lệ</span>
                                        @elseif($errorCode == '0')
                                            <span style="color:#44ab6c">Giao dịch thành công</span>
                                        @else
                                            <span style="color:red">Giao dịch thất bại</span>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 col-lg-2"></div>
        </div>
    @endif
@endsection
