@extends('admin-shop.layout')
@section('admin_shop_content')
    <div id="content-page" class="content-page product-admin-shop">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách sản phẩm
                </div>

                <div style="margin-top: 20px" class="row row-action-bisto-css">
                    <div class="col-sm-3">
                        <h5 style="margin-left: 20px">Tổng số sản phẩm: <span
                                id="total-product"></span></h5>
                    </div>

                    <div class="col-sm-2 d-flex justify-content-end super-center">
                        <select name="shopId" class="select-search-bisto-css form-control">
                            <option selected value="0">Tất cả cửa hàng</option>
                        </select>
                    </div>
                    <div class="col-sm-2 super-center">
                        <div class="btn-bisto-height-css">
                            <select class="form-control" id="admin_status_option">
                                <option selected value="0">Tất cả sản phẩm</option>
                                <option value="1">Sản phẩm không khóa</option>
                                <option value="-1">Sản phẩm bị khóa</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2 super-center">
                        <div class="btn btn-primary btn-bisto-height-css"
                             onclick="onShowModalAddProduct()">+ Thêm sản phẩm mới
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input onkeydown="searchProduct()" id="input-text-product" type="search"
                                   placeholder="&#xf002 Tìm kiếm" class="btn-search-bisto-css input-sm form-control"
                                   name="keywords_submit">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Shop</th>
                            <th class="text-center">Tên sản phẩm</th>
                            <th class="text-center">Giá</th>
                            <th class="text-center">Hình sản phẩm</th>
                            <th class="text-center">Danh mục</th>
                            <th class="text-center">Đã bán</th>
                            <th class="text-center">Số lượng</th>
                            <th class="text-center">Trạng thái</th>
                        </tr>
                        </thead>
                        <tbody id="tbl-product">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->

                <!-- footer and pagination -->
            </div>
        </div>
        <!-- ADD PRODUCT START -->
        <div id="modal-add-update-product-container">
            <div id="modal-add-update-product" class="modal fade add-product" tabindex="-1" role="dialog"
                 aria-labelledby="myExtraLargeModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content" style="border-radius: 20px">
                        <div class="iq-card">
                            <header class="panel-heading">
                                Thêm sản phẩm
                            </header>
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="iq-card-body">
                                        <div id="form-add-product">
                                            <div class="form-group">
                                                <label>Tên sản phẩm</label><span class="text-danger">*</span>
                                                <input type="text" name="name" class="need-validate form-control">
                                                <div class="mes-name mes-invalid">
                                                    Tên sản phẩm là bắt buộc
                                                </div>
                                            </div>

                                            <div class="form-group" style="overflow: auto">
                                                <label class="required-input">Hình ảnh sản phẩm</label><span
                                                    class="note-img note-img-10-normal"></span>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input inputfile"
                                                           id="productImg" multiple>
                                                    <div id="img-container" class="row align-items-start">
                                                    </div>
                                                    <label class="custom-file-label" for="customFile">Chọn
                                                        ảnh</label>
                                                </div>
                                                <div class="mes-imageUrl mes-invalid">
                                                    Bạn phải thêm ít nhất 1 ảnh
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Độ tuổi <span class="age-from"></span> - <span
                                                                class="age-to"></span></label>
                                                        <div class="range-age"></div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Phong cách <span
                                                                    class="fa fa-edit text-success update-stylish"></span></label>
                                                            <input class="form-control" style="text-overflow: ellipsis;"
                                                                   readonly
                                                                   name="stylish-text" value=""/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="required-input">Mô tả sản phẩm</label>
                                                <textarea style="resize: none;height: 150px;line-height: 1.5rem;" rows="8"
                                                          class="form-control"
                                                          name="description"
                                                          placeholder="Mô tả sản phẩm"></textarea>
                                                <div class="mes-description mes-v-display"></div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-4">
                                                    <label class="required-input">Danh mục gốc</label>
                                                    <select name="category_id" class="form-control input-sm m-bot15">
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="required-input">Danh mục con cấp 2</label>
                                                    <select name="category_id_level2"
                                                            class="form-control input-sm m-bot15">
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="required-input">Danh mục con cấp 3</label>
                                                    <select name="category_id_level3"
                                                            class="form-control input-sm m-bot15">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="mes-category_id_level3 mes-invalid">
                                                Bạn phải chọn danh mục cho sản phẩm
                                            </div>
                                            <div class="form-group">
                                                <label>Giá sản phẩm</label><span class="text-danger">*</span>
                                                <input type="number" data-validation="number"
                                                       name="price" class="need-validate form-control"/>
                                                <div class="mes-price mes-invalid">
                                                    Giá sản phẩm là bắt buộc
                                                </div>
                                            </div>

{{--                                            <div class="iq-card">--}}
{{--                                                <div class="form-row">--}}
{{--                                                    <div class="col">--}}
{{--                                                        <label>Ngày bắt đầu</label>--}}
{{--                                                        <input type="datetime-local"--}}
{{--                                                               class="form-control" id="start_at" name="start_at">--}}
{{--                                                    </div>--}}
{{--                                                    <div class="col">--}}
{{--                                                        <label>Ngày kết thúc</label>--}}
{{--                                                        <input type="datetime-local" class="form-control"--}}
{{--                                                               id="end_at" name="end_at">--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="mes-rangedate mes-invalid">--}}
{{--                                                    Ngày bắt đầu phải nhỏ hơn ngày kết thúc--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Trạng thái</label>
                                                <select name="self_status" class="form-control input-sm m-bot15">
                                                    <option value="1">Đang hoạt động</option>
                                                    <option value="-1">Khóa</option>
                                                </select>
                                            </div>

                                            <button id="btn-add-product"
                                                    class="btn btn-primary">Thêm sản phẩm
                                            </button>
                                            <button style="display: none" id="btn-update-product"
                                                    class="btn btn-primary">Lưu thay đổi sản phẩm
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-container">

            </div>
        </div>
        <!-- ADD PRODUCT END -->

        <!-- Modal choose stylish -->
        <div class="modal modal-stylish" tabindex="-1">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Chỉnh sửa phong cách</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="select-stylish"></div>
                        <div class="mes-stylish" style="display: none;color: red">
                            Bạn phải chọn ít nhất 1 phong cách
                        </div>
                        <div class="mes-overload-selected" style="display: none;color: red">
                            Bạn chỉ được chọn tối đa 3 phong cách
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn-save-stylish btn btn-primary">Đồng ý</button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            let dt = {
                currentPage: 1,
                searchKey: '',
                currentProductId: null,
                currentPageShop: 1,
                isMaxShops: false,
                shopId: 0,
                loading: {colspan: 9, type: 'table'}, admin_status: '0'
            }

            const $ageFrom = $('.age-from');
            const $ageTo = $('.age-to');

            let dtShop = {currentPage: 1, loading: {type: 'content'}};
            let $selectShop;
            let pagination;
            let optionsMale = ''
            let optionsFemale = ''
            let selecterCategory;
            let uploaderProductImage, _i;
            let countSelectedStylist = 0;
            let currentStylish = []
            $(document).ready(function () {
                loadStylishes();
                _i = defineImage({id: 'product'});
                const params = parseUrlParams();
                if (params?.admin_status === '-1') {
                    $('#admin_status_option').val('-1');
                    dt.admin_status = '-1';
                }
                loadProducts(dt);
                loadCategory();
                uploaderProductImage = defineUploadFile({
                    inputSelector: '#productImg',
                    containerSelector: '#img-container'
                })
                //select option lazy loading shop id
                $selectShop = defineSearchSelect({selectSelector: 'select[name=shopId]', id: 'shops_of_admin_shop'});
                $selectShop.onSelect(function (value) {
                    dt.shopId = value;
                    loadProducts(dt);
                })
                validateOnKeyUp();
                pagination = definePagination({
                    onPageClick: (pageNumber, event) => {
                        dt.currentPage = pageNumber;
                        loadProducts(dt);
                    }
                });

                $ageFrom.text(0);
                $ageTo.text(100);
                $(".range-age").slider({
                    range: true,
                    min: 0,
                    max: 100,
                    values: [0, 100],
                    slide: function (event, ui) {
                        $ageFrom.text(ui.values[0]);
                        $ageTo.text(ui.values[1]);
                    }
                });
            });

            //validate when user typing on input
            function validateOnKeyUp() {
                validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên sản phẩm là bắt buộc'
                    }],
                    propertyName: 'name',
                    type: 'onkeyup'
                });
                validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Giá sản phẩm là bắt buộc'
                    }, {
                        func: rangeValidation,
                        mes: `Giá tối thiểu là ${MIN_MONEY} VND`,
                        params: {
                            min: MIN_MONEY,
                            max: MAX_MONEY,
                        }
                    }], propertyName: 'price',
                    type: 'onkeyup'
                });
                validateRangeNumber('input[name=price]', MIN_MONEY, MAX_MONEY);
            }

            //set default current data for start_at end_at
            // function setDefaultInputDate() {
            //     setInputDateTime('start_at');
            //     setInputDateTime('end_at');
            // }

            //event when add new product
            function onShowModalAddProduct() {
                $('select[name=self_status]').val(1).attr('disabled', true);
                $('#modal-add-update-product').modal('show');
                $('button#btn-update-product').hide();
                $('button#btn-add-product').show();
            }

            //validate before submit
            function validateFormProduct(type) {
                return validateInput({
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Tên sản phẩm là bắt buộc'
                        }],
                        propertyName: 'name'
                    }) && validateInput({
                        listValidateFunction: [{
                            func: requiredElementValidation,
                            mes: 'Bạn phải thêm ít nhất một ảnh',
                            params: {classNameElement: 'input[name=imageUrl]'}
                        }], type: 'custom', customInput: 'input[name=imageUrl]', customMes: '.mes-imageUrl'
                    }) && validateInput({
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Mô tả là bắt buộc'
                        }], propertyName: 'description', type: 'textarea'
                    }) && validateInput({
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Bạn phải chọn cả 3 cấp độ danh mục trên'
                        }], propertyName: 'category_id_level3', type: 'select'
                    }) &&
                    validateInput({
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Giá sản phẩm là bắt buộc'
                        }, {
                            func: rangeValidation,
                            mes: `Giá tối thiểu là ${MIN_MONEY} VND`,
                            params: {
                                min: MIN_MONEY,
                                max: MAX_MONEY,
                            }
                        }], propertyName: 'price'
                    })
                    // && validateRangeDate('input[name=start_at]', 'input[name=end_at]', 'div.mes-rangedate');
            }

            $('#admin_status_option').on('change', function (e) {
                dt.admin_status = e.currentTarget.value;
                loadProducts(dt);
            })

            //get product object from form
            function getProductObject() {
                const listS = $('.select-stylish :input').serializeArray().map(item => parseInt(item.name));
                let product = {
                    name: $('input[name=name]').val() || '',
                    price: parseFloat($('input[name=price]').val()) || 0,
                    stylish: isEmptyValue(listS) ? currentStylish : listS,
                    range_age: [parseInt($ageFrom.text()), parseInt($ageTo.text())],
                    self_status: parseInt($('select[name=self_status]').val()),
                    // start_at: new Date($('input[name=start_at]').val()).getTime() / 1000 || null,
                    // end_at: new Date($('input[name=end_at]').val()).getTime() / 1000 || null,
                    category_id: selecterCategory.getCurrentValueSelected().value || 0,
                    description: $('textarea[name=description]').val() || ''
                }
                product.image_urls = uploaderProductImage.getMultiImg();
                return product;
            }

            //handle event onclick when user press save product
            $('#btn-add-product').on('click', function (e) {
                if (validateFormProduct()) {
                    $.ajax({
                        url: '{{url('/admin-shop/product/add')}}',
                        method: 'POST',
                        data: {
                            _token: "{{csrf_token()}}",
                            ...getProductObject(),
                        },
                    }).done((res, textStatus, xhr) => {
                        dataToForm({
                            fieldInputs: [{selector: 'input[name=name]'},
                                {selector: 'textarea[name=description]'},
                                {selector: 'select[name=category_id_level3]'},
                                {selector: 'input[name=price]'},
                                {selector: 'input[name=discount]'},
                            ],
                            fieldFiles: [{
                                uploaderInstance: uploaderProductImage, listFiles: []
                            }], action: 'clear-validate'
                        });
                        $('select[name=category_id_level2]').empty();
                        $('select[name=category_id_level3]').empty();
                        $(".range-age").slider("option", "values", [0, 100]);
                        currentStylish = [];
                        countSelectedStylist = 0;
                        $('input[name=stylish-text]').val('');
                        $ageFrom.text(0);
                        $ageTo.text(100);
                        if (xhr.code == 403) {
                            toastErrorMessage(res.message);
                        } else {
                            toastSuccessMessage(res.message);
                        }
                        $('#modal-add-update-product').modal('hide');
                        loadProducts({...dt, loading: {type: 'content'}});
                    })
                        .fail(err => {

                        })
                }
            })

            //when user un-active a product
            $(document).on('click', '.btn-unactive', function () {
                const productId = $(this).attr('productid');
                $.ajax({
                    url: `unactive-product/${productId}`,
                    method: 'GET'
                }).done(res => {
                    toastSuccess(res, 'Thay đổi trạng thái thành công');
                    loadProducts({...dt, loading: {type: 'content'}});
                }).fail(err => {
                    toastError(err, 'Thay đổi trạng thái thất bại');
                })
            });

            //when user active a product
            $(document).on('click', '.btn-active', function () {
                const productId = $(this).attr('productid');
                $.ajax({
                    url: `active-product/${productId}`,
                    method: 'GET'
                }).done(res => {
                    toastSuccess(res, 'Thay đổi trạng thái thành công');
                    loadProducts({...dt, loading: {type: 'content'}});
                }).fail(err => {
                    toastError(err, 'Thay đổi trạng thái thất bại');
                })
            })

            //products to interface
            function generateProducts(products) {
                let tbody = '';
                _i.resetPhotos();
                for (let i = 0; i < products.length; i++) {
                    let id = products[i].id;
                    let isActive = null;
                    if (products[i].self_status === 1)
                        isActive = `<span productid=${id} class="btn-unactive  btn-badge badge badge-success" type="button">Hiển thị</span>`
                    else isActive = `<span productid=${id} class="btn-active btn-badge badge badge-danger" type="button">Ẩn</span>`
                    let isBlock;
                    if (products[i].admin_status === 1)
                        isBlock = `<span id="lock-status-${id}" class="badge badge-success">Không khóa</span>`;
                    else isBlock = `<span id="lock-status-${id}" class="badge badge-danger">Bị khóa</span>`;
                    tbody += `<tr>
			    <td class="text-center">${i + 1}</td>
                <td class="text-center">${products[i].shop_name}</td>
				<td class="text-center">${products[i].name}</td>
				<td class="text-center">${products[i].price.formatVND()}</td>
				<td class="text-center">
                   ${_i.toHTML(products[i].image_urls[0], products[i].name)}
                </td>
				<td class="text-center">${products[i].category_name}</td>
                <td class="text-center">${products[i].sold}</td>
                <td class="text-center">${products[i].quantity}</td>
				<td class="text-center">
					<span class="text-ellipsis">${isActive}</span>
				`;
                    if (products[i].admin_status === 1)
                        isBlock = `<span id="lock-status-${id}" class="badge badge-success">Không khóa</span>`;
                    else isBlock = `<span id="lock-status-${id}" class="badge badge-danger">Bị khóa</span>`;
                    tbody += `<span class="text-ellipsis">${isBlock}</span>`;
                    tbody += `</td></tr>`;
                }
                $('.table-striped>tbody').empty().append(tbody);
                _i.animateOnloadImage({});
            }

            //call api get product by paging and generate on interface
            function loadProducts({
                                      currentPage = 1,
                                      searchKey = '',
                                      shopId = 0, loading = {colspan: 9, type: 'table'}, admin_status = '0'
                                  }) {
                $.ajax({
                    url: `{{url('/admin-shop')}}/product/query?key=${searchKey}&shopId=${shopId}&page=${currentPage}&admin_status=${admin_status}`,
                    method: 'GET',
                    loading
                })
                    .done(res => {
                        console.log(res)
                        const products = res?.results?.data ?? [];
                        if (products.length === 0 && dt.currentPage > 1) {
                            dt.currentPage -= 1;
                            pagination.setCurrentPage(1);
                            loadProducts(dt);
                        } else if (products.length === 0) {
                            $('.table-striped>tbody').empty().append(genNoContentTable(9, `Không có sản phẩm nào`));
                        } else {
                            generateProducts(products);
                            pagination.setTotalItems(res.results.total);
                        }
                        $('#total-product').text(res.results.total);
                        pagination.setTotalItems(res.results.total);
                    })
                    .fail(err => {
                    });
            }

            //load all-category
            function loadCategory() {
                selecterCategory = multiSelect({
                    listSelect: [{
                        selectSelector: 'select[name=category_id]',
                        url: 'root-category', valueName: 'id', showName: 'name'
                    }, {
                        selectSelector: 'select[name=category_id_level2]',
                        url: 'category-level-two/', valueName: 'id', showName: 'name'
                    }, {
                        selectSelector: 'select[name=category_id_level3]',
                        url: 'category-level-three/',
                        valueName: 'id',
                        showName: 'name'
                    }], loading: null
                });
                selecterCategory.loadRootSelect();
            }

            //handle event when user typing enter on input search
            function searchProduct(e) {
                // user press enter search
                if (event.key === 'Enter') {
                    dt.searchKey = $('#input-text-product').val();
                    dt.currentPage = 1;
                    pagination.setCurrentPage(1);
                    loadProducts(dt);
                }
            }

            function loadStylishes() {
                $.ajax({
                    url: '{{url('/admin-shop/all-stylish')}}',
                    method: 'GET',
                }).done(res => {
                    dt.stylishes = res?.stylishes ?? [];
                }).fail(err => {
                    toastError(err, 'Load stylish thất bại');
                });
            }

            // handle button update stylish click
            $('.update-stylish').on('click', function () {
                let allStylish = '';
                if (isEmptyValue(dt.stylishes)) return toastInfoMessage('Không có phong cách nào');
                for (const stylish of dt.stylishes) {
                    let check = '';
                    if (currentStylish.includes(stylish.id)) check = 'checked';
                    allStylish += `<div class="form-check">
                                <input name=${stylish.id} class="form-check-input" type="checkbox" value="${stylish.name}" ${check}>
                                <label class="form-check-label">${stylish.name}</label>
                            </div>`;
                }
                $('.modal-stylish .select-stylish').empty().append(allStylish);
                $('.modal-stylish').modal('show');
            })

            $(document).on("change", '.select-stylish :input', function () {
                if (this.checked) {
                    countSelectedStylist++;
                    if (countSelectedStylist > 3) {
                        $('.mes-overload-selected').css('display', 'block')
                        $('.btn-save-stylish').prop('disabled', true)
                    } else if (countSelectedStylist <= 3) {
                        currentStylish.push(parseInt(this.name))
                    }
                } else {
                    countSelectedStylist--;
                    if (countSelectedStylist <= 3) {
                        $('.btn-save-stylish').prop('disabled', false)
                        $('.mes-overload-selected').css('display', 'none')
                        const temp = currentStylish.indexOf(parseInt(this.name))
                        if (temp != -1) {
                            currentStylish.splice(temp, 1)
                        }
                    }
                }
            })

            //handle save stylish button
            $('.btn-save-stylish').on('click', function () {
                let stylishText = '';
                const listS = $('.select-stylish :input').serializeArray();
                if (isEmptyValue(listS)) {
                    return $('.mes-stylish').show();
                } else $('.mes-stylish').hide();
                for (const stylish of listS) {
                    stylishText += `${stylish.value}, `;
                }

                $('input[name=stylish-text]').val(stylishText);
                $('.modal-stylish').modal("hide");
            })
        </script>
@endsection
