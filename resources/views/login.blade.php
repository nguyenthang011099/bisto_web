<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ĐĂNG NHẬP VÀO ADMIN</title>
    <!-- Favicon -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="shortcut icon" href="{{asset('backend/images/Logo.png')}}"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('backend/css2/bootstrap.min.css')}}">
    <!-- Typography CSS -->
    <link rel="stylesheet" href="{{asset('backend/css2/typography.css')}}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('backend/css2/style.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('backend/css2/responsive.css')}}">
    <!-- toast CSS -->
    <link type="text/css" rel="stylesheet" href="{{asset('backend/css2/toastr.min.css')}}"/>
    <!-- custom CSS -->
    <link rel="stylesheet" href="{{asset('backend/css2/custom.css')}}">
</head>

<body>
<!-- loader Start -->
<div id="loading">
    <div id="loading-center">
    </div>
</div>
<!-- loader END -->
<!-- Sign in Start -->
<section class="sign-in-page">
    <div class="container bg-white mt-5 p-0">
        <div class="row no-gutters">
            <div id="loading-content">
                <div class="loading-center">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="col-sm-6 align-self-center">
                <div class="super-center">
                    <img src="{{asset('backend/images/Logo.png')}}" class="img-fluid" alt="logo" width="300"
                         height="100" style="min-width: 7rem;max-width: 13rem;">
                </div>
                <div class="sign-in-from">
                    <h1 class="mb-0"><a href="{{URL::to('/')}}"></a>Đăng nhập</h1>
                    <?php
                    $message = Session()->get('message');
                    if ($message) {
                        echo '<span class="text-alert">' . $message . '</span>';
                        Session()->put('message', null);
                    }
                    ?>
                    <p>Điền số điện thoại và mật khẩu để đăng nhập vào trang quản lý.</p>
                    <div class="mt-4" id="form-login">
                        @csrf
                        @foreach($errors->all() as $val)
                            <ul>
                                <li>{{$val}}</li>
                            </ul>
                        @endforeach
                        <div class="form-group">
                            <label>Số điện thoại</label>
                            <input type="text" name="phone_number" class="form-control mb-0" id="exampleInputEmail1"
                                   placeholder="Điền số điện thoại">
                            <div class="mes-v-display mes-phone_number">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Mật khẩu</label>
                            <a href="#" class="float-right">Quên mật khẩu?</a>
                            <input type="password" name="password" class="form-control mb-0"
                                   id="exampleInputPassword1" placeholder="Mật khẩu">
                            <div class="mes-v-display mes-password">
                            </div>
                        </div>
                        <div class="d-inline-block w-100">
                            <div class="custom-control custom-checkbox d-inline-block mt-2 pt-1">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Ghi nhớ</label>
                            </div>
                            <button type="submit" value="Đăng nhập" name="login"
                                    class="btn btn-primary float-right">Đăng nhập
                            </button>
                        </div>
                        <div class="sign-info">
                                <span class="dark-color d-inline-block line-height-2">Không có tài khoản? <a
                                        href="{{url('/register')}}">Đăng ký</a></span>
                            <ul class="iq-social-media">
                                <li><a href="https://www.facebook.com/BISTO.VN"><i
                                            class="fab fa-facebook-square"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter-square"></i></a></li>
                                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 text-center">
                <div class="sign-in-detail text-white">
                    <a class="sign-in-logo mb-5">BISTO</a>
                    <div class="owl-carousel" data-autoplay="true" data-loop="true" data-nav="false"
                         data-dots="true" data-items="1" data-items-laptop="1" data-items-tab="1"
                         data-items-mobile="1" data-items-mobile-sm="1" data-margin="0">
                        <div class="item">
                            <img src="{{asset('backend/images/login/1.png')}}" class="img-fluid mb-4"
                                 alt="logo">
                            <h4 class="mb-1 text-white">Theo dõi doanh thu chi tiết</h4>
                            <p>Một sự lựa chọn hoàn hảo khi đến với chúng tôi, bạn có thể theo dõi doanh thu cũng
                                như những lựa chọn cần thiết trong việc quản lý.</p>
                        </div>
                        <div class="item">
                            <img src="{{asset('backend/images/login/3.png')}}" class="img-fluid mb-4"
                                 alt="logo">
                            <h4 class="mb-1 text-white">Có ý tưởng hay sản phẩm mới</h4>
                            <p>Bạn có nhiều ưu đãi cho khách hàng mà không muốn phải chọn lựa từng sản phẩm một,
                                Bisto sẽ là giải pháp hữu hiệu để bạn gạt bỏ đi những bước rườm rà này.</p>
                        </div>
                        <div class="item">
                            <img src="{{asset('backend/images/login/4.png')}}" class="img-fluid mb-4"
                                 alt="logo">
                            <h4 class="mb-1 text-white">Có ý tưởng hay sản phẩm mới</h4>
                            <p>Bạn nhận ra rằng muốn nhập sản phẩm mới cũng như đầu mục sản phẩm mới? Bisto vui lòng
                                cung cấp cho bạn dịch vụ để quản lý sản phẩm tốt và hiệu quả.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Sign in END -->
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{asset('backend/js2/jquery.min.js')}}"></script>
<script src="{{asset('backend/js2/popper.min.js')}}"></script>
<script src="{{asset('backend/js2/bootstrap.min.js')}}"></script>
<!-- Appear JavaScript -->
<script src="{{asset('backend/js2/jquery.appear.js')}}"></script>
<!-- Countdown JavaScript -->
<script src="{{asset('backend/js2/countdown.min.js')}}"></script>
<!-- Counterup JavaScript -->
<script src="{{asset('backend/js2/waypoints.min.js')}}"></script>
<script src="{{asset('backend/js2/jquery.counterup.min.js')}}"></script>
<!-- Wow JavaScript -->
<script src="{{asset('backend/js2/wow.min.js')}}"></script>
<!-- Apexcharts JavaScript -->
<script src="{{asset('backend/js2/apexcharts.js')}}"></script>
<!-- Slick JavaScript -->
<script src="{{asset('backend/js2/slick.min.js')}}"></script>
<!-- Select2 JavaScript -->
<script src="{{asset('backend/js2/select2.min.js')}}"></script>
<!-- Owl Carousel JavaScript -->
<script src="{{asset('backend/js2/owl.carousel.min.js')}}"></script>
<!-- Magnific Popup JavaScript -->
<script src="{{asset('backend/js2/jquery.magnific-popup.min.js')}}"></script>
<!-- Smooth Scrollbar JavaScript -->
<script src="{{asset('backend/js2/smooth-scrollbar.js')}}"></script>
<!-- Custom JavaScript -->
<script src="{{asset('backend/js2/custom.js')}}"></script>
<!-- toast JavaScript -->
<script src="{{asset('backend/js2/toastr.min.js')}}"></script>
<!-- helper JavaScript -->
<script src="{{asset('backend/js2/helper.js')}}"></script>
<!-- validation JavaScript -->
<script src="{{asset('backend/js2/validation.js')}}"></script>

<script type="text/javascript">
    (function () {
        $('button[name=login]').on('click', function () {
            login();
        })

        $('input[name=phone_number]').on('keydown', function (e) {
            if (e?.key === 'Enter') login();
        })

        $('input[name=password]').on('keydown', function (e) {
            if (e?.key === 'Enter') login();
        })

        function login() {
            if (validateInput({
                listValidateFunction: [
                    {
                        func: requiredValidation,
                        mes: 'Bạn phải nhập số điện thoại',
                    }
                ], propertyName: 'phone_number'
            }) && validateInput({
                listValidateFunction: [
                    {
                        func: requiredValidation,
                        mes: 'Bạn phải nhập mật khẩu',
                    }
                ], propertyName: 'password'
            })) {
                const formData = formToData('#form-login');
                const $loader = $('#loading-content');
                $loader.show();
                $loader.parent().css('opacity', '0.5');
                fetch(
                    '{{url('/login')}}',
                    {
                        method: 'POST',
                        redirect: 'follow',
                        headers: {
                            'Content-Type': 'application/json'
                            // 'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: JSON.stringify(formData)
                    },
                ).then(response => {
                    if (response.redirected) {
                        window.location.href = response.url;
                    } else if (!response.ok) {
                        return response.json();
                    }
                }).then(json => {
                    $loader.hide();
                    $loader.parent().css('opacity', '1');
                    if (json.message)
                        toastErrorMessage(json.message ?? 'Đăng nhập thất bại');
                }).catch(err => {
                    $loader.hide();
                    $loader.parent().css('opacity', '1');
                });
            }
        }
    }())
</script>
</body>

</html>
