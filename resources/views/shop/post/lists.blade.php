@extends('shop.layout')
@section('shop_content')
    <?php
    $ID = 'post';
    ?>
    <div id="content-page" class="content-page post-shop">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách post
                </div>
                <div style="margin-top: 20px" class="row btn-bisto row-action-bisto-css">
                    <div class="col-sm-6">
                        <button class="btn-show-add-bisto btn-show-add-bisto-css btn btn-primary">+ Thêm post
                        </button>
                    </div>
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search"
                                   placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm btn-search-bisto btn-search-bisto-css form-control">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body body-bisto">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Tiêu đề</th>
                            <th class="text-center">Ngày tạo</th>
                            <th class="text-center">Mô tả</th>
                            <th class="text-center">Số like</th>
                            <th class="text-center">Số comment</th>
                            <th class="text-center">Hiển thị</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody class="tbody-bisto">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <div id="test-ck">

                </div>
                <!-- PAGINATION END-->
            </div>

            <div class="iq-card">
                <div class="panel-heading" id="form-post-title">
                    Thêm post
                </div>

                <div class="iq-card-body">
                    <section class="panel">
                        <div class="panel-body">
                            <div class="iq-card-body">
                                <div class="form-bisto" role="form" id="form-{{$ID}}">
                                    @csrf
                                    <div class="form-group">
                                        <label class="required-input">Tiêu đề</label>
                                        <input type="text" name="title" class="form-control">
                                        <div class="mes-title mes-v-display">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="required-input">Mô tả</label>
                                        <textarea style="height: 80px" rows="8"
                                                  class="form-control"
                                                  name="description"
                                                  placeholder=""></textarea>
                                        <div class="mes-description mes-v-display"></div>
                                    </div>

                                    <div class="form-group">
                                        <label class="required-input">Nội dung</label>
                                        <div class="" id="">

                                        </div>
                                        <div class="" id="post-content-editor">
                                        </div>
                                        <div class="mes-content mes-v-display">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="required-input">Hiển thị</label>
                                        <select name="self_status"
                                                class="form-control input-sm m-bot15">
                                            <option value="1">Hiển thị</option>
                                            <option value="-1">Ẩn</option>
                                        </select>
                                        <div class="mes-self_status mes-v-display">
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-show-cmt">Bình luận</div>
                                <div id="cmt-post-shop">

                                </div>
                                <div>
                                    <button class="btn btn-primary btn-add-bisto btn-add-bisto-css">
                                        Thêm post
                                    </button>
                                    <button class="btn btn-primary btn-update-bisto btn-update-bisto-css">
                                        Cập nhật post
                                    </button>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

        </div>

    </div>
    <script>
        const COLSPAN = 9;
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: COLSPAN}};
        let pagination,CMT;
        let ckeditor;
        const $formTitle = $('#form-post-title');

        $(document).ready(function () {
            _ = defineBisto({
                parentSelector: '.post-shop', name: 'post',
            });
            _.$btnUpdate.hide();
            ckeditor = createCkeditor5({selector: '#post-content-editor'})

            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadPost(dt);
                }
            });
            CMT = defineComment({ selector: '#cmt-post-shop', showMode : 'collapse' });
            loadPost(dt);

            _.onSearch((value) => {
                dt.keyword = value;
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadPost(dt);
            });

            _.onShowAdd(() => {
                ckeditor.getEditor().setData('');
                $formTitle.text('Thêm post');

                dataToForm({
                    fieldInputs: [
                        {selector: 'input[name=title]'},
                        {selector: 'textarea[name=description]'},
                        {selector: 'select[name=self_status]', value: '1'},
                    ],
                    parentSelector: '.form-bisto', action: 'clear-validate'
                })
            })

            _.onAdd((data) => {
                if (validateFormPost())
                    $.ajax({
                        url: `{{url('/shop/posts')}}`,
                        method: 'POST',
                        data: {
                            ...data,
                            admin_status: 1,
                            content: ckeditor.getEditor().getData(),
                        }
                    }).done(res => {
                        toastSuccess(res, 'Thêm post thành công');
                        loadPost({...dt, loading: {type: 'content'}});
                        _.$modalAdd.modal('hide');
                    }).fail(err => {
                        _.$modalAdd.modal('hide');
                    })
            })

            _.onShowUpdate((id, cbShowModalUpdate) => {
                $.ajax({
                    url: `{{url('/shop/posts')}}/${id}`,
                    method: 'GET',
                    loading: {type: 'content'}
                }).done(res => {
                    const post = res?.result ?? {title: '', created_at: null};
                    dataToForm({
                        fieldInputs: [
                            {selector: 'input[name=title]', value: post?.title},
                            {selector: 'textarea[name=description]', value: post?.description},
                            {selector: 'select[name=self_status]', value: post?.self_status},
                        ],
                        parentSelector: '.form-bisto', action: 'clear-validate'
                    })
                    ckeditor.getEditor().setData(post?.content)
                    $formTitle.text('Chỉnh sửa post');
                    CMT.initCommentsForPost(post?.id);
                    cbShowModalUpdate();
                }).fail(err => {
                })
            })

            _.onUpdate((id, data) => {
                if (validateFormPost())
                    $.ajax({
                        url: `{{url('/shop/posts')}}/${id}`,
                        method: 'PUT',
                        loading: {type: 'content'},
                        data: {
                            ...data,
                            content: ckeditor.getEditor().getData(),
                        }
                    }).done(res => {
                        toastSuccess(res, 'Cập nhật post thành công');
                        loadPost({...dt, loading: {type: 'content'}});
                        _.$modalUpdate.modal('hide');
                    }).fail(err => {
                        _.$modalUpdate.modal('hide');
                    })
            })

            _.onDelete((id) => {
                $.ajax({
                    url: `{{url('/shop/posts')}}/${id}`,
                    method: 'DELETE',
                    data: {
                        _token: '{{csrf_token()}}',
                    }
                }).done(res => {
                    toastSuccess(res, 'Xóa post thành công');
                    loadPost({...dt, loading: {type: 'content'}});
                    _.$modalUpdate.modal('hide');
                }).fail(err => {
                    _.$modalUpdate.modal('hide');
                })
            })
        });

        function validateFormPost() {

            function validateContent() {
                if (ckeditor.getEditor().getData().length == 0) {
                    toastInfoMessage('Bạn phải thêm nội dung')
                    return false
                } else return true
            }

            return validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Tên post là bắt buộc'
                }],
                propertyName: 'title'
            }) && validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Mô tả là bắt buộc',
                }], type: 'textarea', propertyName: 'description'
            }) && validateContent()
        }

        function loadPost({currentPage = 1, keyword = '', loading = {type: 'table', colspan: COLSPAN}}) {
            $.ajax({
                url: `{{url('/shop/posts')}}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
                loading,
            }).done(res => {
                const posts = res?.results?.data ?? [];
                if (posts?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadPost(dt);
                } else if (posts?.length === 0) {
                    _.$tbody.empty().append(genNoContentTable(COLSPAN, `Không có dữ liệu`));
                } else {
                    generatePosts(posts);
                    pagination.setTotalItems(res.results.total);
                }
            }).fail(err => {
            });
        }

        function generatePosts(posts) {
            let tbody = '';

            for (let i = 0; i < posts.length; i++) {
                const id = posts[i].id;

                let selfStatus = '';
                if (posts[i]?.self_status === 1)
                    selfStatus = `<span class="badge badge-success">Hiển thị</span>`;
                else selfStatus = `<span class="badge badge-danger">Ẩn</span>`;

                let adminStatus = '';
                if (posts[i]?.admin_status == `{{\App\Post::STATUS_ACCEPT}}`)
                    adminStatus = `<span class="badge badge-success">Không khóa</span>`;
                else adminStatus = `<span class="badge badge-danger">Bị khóa</span>`;

                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${posts[i]?.title ?? ''}</td>
                            <td class="text-center">${posts[i]?.created_at?.formatDate() ?? ''}</td>
                            <td class="text-center">${posts[i]?.description?.summary() ?? ''}</td>
                            <td class="text-center">${posts[i]?.count_like ?? 0}</td>
                            <td class="text-center">${posts[i]?.count_comment ?? 0}</td>
                            <td class="text-center">${selfStatus}</td>
                            <td class="text-center">${adminStatus}</td>
                            <td class="text-center">
                                <span type="button" data-id=${id} class="fas fa-edit fa-lg text-success btn-show-update-bisto btn-show-update-bisto-css"></span>
                                <span type="button" data-id=${id} class="fas fa-times fa-lg text-danger btn-delete-bisto-css btn-delete-bisto"></span>
                            </td>
                        </tr>`;
            }
            _.$tbody.empty().append(tbody);
        }

        $('.btn-show-cmt').on('click',function () {
            CMT.$commentContainer.collapse('toggle');
        })
    </script>
@endsection
