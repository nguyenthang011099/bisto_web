@extends('shop.layout')
@section('shop_content')
    <?php
    $role_text = ['Tài khoản chưa được kích hoạt', 'Người dùng', 'Quản trị shop', 'Quản trị viên'];
    ?>
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="iq-card-body">
                    <!--  -->
                    <div class="iq-card-body profile-page p-0">
                        <div class="profile-header">
                            <div class="row" style="margin-bottom: 3%;">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-6">
                                    <!--left col-->
                                    {{--cover--}}
                                    <div class="text-center position-relative">
                                        <img class="img-fluid cover-show"
                                             id="image-cover" src="" alt="bisto">
                                        <label
                                            style="cursor: pointer; color: orange; position: absolute; right: 5%; transform: translateY(-50%)  translateX(-50%); bottom: 0%"
                                            for="cover-img-input"><i class="fas fa-camera"></i></label>
                                        <input id="cover-img-input" type="file"
                                               class="text-center center-block file-upload"
                                               style="cursor: pointer; display: none;">
                                    </div>

                                    {{--avatar--}}
                                    <div class="text-center" id="avatar-admin"
                                         style="z-index: 10; position: absolute;width: 25%;left: 50%;transform: translateY(-85%)  translateX(-50%)">
                                        <img id="image-ava" class="avatar-show img-circle img-thumbnail" alt="avatar"
                                             src="">
                                        <label
                                            style="cursor: pointer; color: orange; position: absolute; left: 50%; transform: translateY(-50%)  translateX(-50%); bottom: 0"
                                            for="avatar-img-input"><i class="fas fa-camera"></i></label>
                                        <input id="avatar-img-input" type="file"
                                               class="text-center center-block file-upload"
                                               style="cursor: pointer; display: none;">
                                    </div>

                                    <div class="row mt-4 mb-2">
                                        <div
                                            class="col-5 justify-content-end d-flex text-success ratio-order-success"></div>
                                        <div class="col-2 super-center rates-of-user">
                                            <div class="ratings">
                                                <div class="empty-stars"></div>
                                                <div class="full-stars" style="width:0%"></div>
                                            </div>
                                        </div>
                                        <div class="col-5 text-danger ratio-order-fail">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Số Bcoin hiện tại </label>
                                                <div class="d-flex align-items-center d-flex"><h4
                                                        class="bcoins p-2"></h4>&nbsp;<span
                                                        class="fa fa-btc fa-lg p-2"
                                                        aria-hidden="true"></span>
                                                </div>
                                                <div>
                                                      <span class="font-weight-bold text-nowrap">
                                                        <a href="{{URL::to('shop/payment')}}"
                                                           style="text-decoration: underline!important;">Thông tin thanh toán</a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label for="shop_name" class="required-input">Tên shop</label>
                                                <input type="text" class="form-control" name="shop_name">
                                                <div class="mes-shop_name mes-v-display">
                                                    Tên shop là bắt buộc
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Phong cách <span
                                                        class="fa fa-edit text-success update-stylish"></span></label>
                                                <input class="form-control" style="text-overflow: ellipsis;" readonly
                                                       name="stylish-text" value=""/>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label for="full_name" class="required-input">Chủ shop</label>
                                                <input type="text" class="form-control" name="full_name">
                                                <div class="mes-full_name mes-v-display">
                                                    Tên chủ shop là bắt buộc
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Độ tuổi <span class="age-from"></span> - <span
                                                        class="age-to"></span></label>
                                            </div>
                                            <div class="range-age"></div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label for="email" class="required-input">Email</label>
                                                <input type="text" class="form-control" name="email">
                                                <div class="mes-email mes-v-display">
                                                    Email là bắt buộc
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="admin_role">Quyền</label>
                                                <input type="text" class="form-control" readonly
                                                       name="admin_role"/>
                                            </div>
                                        </div>
                                        @if($is_shop_without_branch == 0)
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <label for="description">Thuộc chi nhánh</label>
                                                    <input type="text" class="form-control" name="parent_shop"
                                                           readonly/>
                                                </div>
                                            </div>
                                        @elseif($is_shop_without_branch == 1)
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="required-input" for="cmnd">CMND</label>
                                                    <input type="text" class="form-control" name="cmnd"/>
                                                    <div class="mes-cmnd mes-v-display">
                                                        CMND là bắt buộc
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="required-input">Mã số thuế</label>
                                                    <input type="text" class="form-control" name="tax_code"/>
                                                    <div class="mes-tax_code mes-v-display">
                                                        Mã số thuế là bắt buộc
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="description" class="required-input">Mô tả</label>
                                                <textarea type="text" class="form-control"
                                                          style="height: 142px; resize: none"
                                                          name="description"></textarea>
                                                <div class="mes-description mes-v-display">
                                                    Mô tả là bắt buộc
                                                </div>
                                            </div>
                                            <button id="btn-save" class="btn btn-primary">Lưu</button>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label for="phone">Số điện thoại</label>
                                                <input type="text" class="form-control" readonly name="phone">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3"></div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="shop_id" id="shop_id"/>
                </div>
            </div>
        </div>
        <div class="modal modal-stylish" tabindex="-1">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Chỉnh sửa phong cách</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="select-stylish"></div>
                        <div class="mes-stylish" style="display: none;color: red">
                            Bạn phải chọn ít nhất 1 phong cách
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn-save-stylish btn btn-primary">Đồng ý</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-container">

        </div>
    </div>

    <script>
        const is_shop_without_branch = {{$is_shop_without_branch}};
        let cropperAvatar, dt = {stylishes: []};
        let cropperCover;
        let userId;
        let currentStylish = [];
        const $ageFrom = $('.age-from');
        const $ageTo = $('.age-to');
        $(document).ready(function () {
            loadProfile();
            loadStylishes();
            $('button#btn-save').on('click', onClickButtonSave);
            cropperAvatar = defineCropAndUploadImage({inputSelector: '#avatar-img-input'})
            cropperCover = defineCropAndUploadImage({inputSelector: '#cover-img-input', type: 'cover'})

            cropperAvatar.saveButton.addEventListener('click', function () {
                $.ajax({
                    url: '{{url('/shop/upload-avatar')}}',
                    method: 'POST',
                    data: {
                        imageUrl: cropperAvatar.getImg(),
                        _token: '{{csrf_token()}}'
                    }
                }).done(res => {
                    cropperAvatar.$modal.modal('hide');
                    toastSuccessMessage('Thay đổi avatar thành công');
                    loadProfile();
                }).fail(err => {
                    cropperAvatar.$modal.modal('hide');
                    toastErrorMessage('Thay đổi avatar thất bại');
                })
            })

            cropperCover.saveButton.addEventListener('click', function () {
                $.ajax({
                    url: '{{url('/shop/upload-cover')}}',
                    method: 'POST',
                    data: {
                        imageUrl: cropperCover.getImg(),
                        _token: '{{csrf_token()}}'
                    }
                }).done(res => {
                    cropperCover.$modal.modal('hide');
                    toastSuccessMessage('Thay đổi ảnh bìa thành công');
                    loadProfile();
                }).fail(err => {
                    cropperCover.$modal.modal('hide');
                    toastErrorMessage('Thay đổi ảnh bìa thất bại')
                })
            });

            $ageFrom.text(0);
            $ageTo.text(100);
            $(".range-age").slider({
                range: true,
                min: 0,
                max: 100,
                values: [0, 100],
                slide: function (event, ui) {
                    $ageFrom.text(ui.values[0]);
                    $ageTo.text(ui.values[1]);
                }
            });
        })

        $('.btn-save-stylish').on('click', function () {
            let stylishText = '';
            const listS = $('.select-stylish :input').serializeArray();
            if (isEmptyValue(listS)) {
                return $('.mes-stylish').show();
            } else $('.mes-stylish').hide();
            for (const stylish of listS) {
                stylishText += `${stylish.value}, `;
            }

            $('input[name=stylish-text]').val(stylishText.slice(0, -2));
            $('.modal-stylish').modal("hide");
        })

        function validateProfileForm() {
            let validateWithoutBranch = () => true;
            if (is_shop_without_branch === 1) {
                validateWithoutBranch = () => validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'CMND là bắt buộc'
                    }],
                    propertyName: 'cmnd',
                    type: 'onsubmit',
                }) && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Mã số thuế là bắt buộc'
                    }],
                    propertyName: 'tax_code',
                    type: 'onsubmit',
                })
            }

            return validateWithoutBranch() && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên shop là bắt buộc'
                    }],
                    propertyName: 'shop_name',
                    type: 'onsubmit',
                }) && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên chủ shop là bắt buộc'
                    }],
                    propertyName: 'full_name',
                    type: 'onsubmit',
                }) &&
                validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Email là bắt buộc'
                    }, {
                        func: emailValidation,
                        mes: 'Email Không đúng định dạng'
                    }],
                    propertyName: 'email',
                    type: 'onsubmit',
                }) &&
                validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Mô tả là bắt buộc'
                    }],
                    propertyName: 'description',
                    type: 'textarea',
                });
        }

        function getProfileObject() {
            let obj = {};
            const listS = $('.select-stylish :input').serializeArray().map(item => parseInt(item.name));
            obj.user_id = userId;
            obj.stylish = isEmptyValue(listS) ? currentStylish : listS;
            obj.range_age = [parseInt($ageFrom.text()), parseInt($ageTo.text())];
            obj.shop_id = $('input[name=shop_id]').val();
            obj.full_name = $('input[name=full_name]').val();
            obj.email = $('input[name=email]').val();
            obj.shop_name = $('input[name=shop_name]').val();
            obj.description = $('textarea[name=description]').val();
            obj.tax_code = $('input[name=tax_code]').val();
            obj.cmnd = $('input[name=cmnd]').val();
            return obj;
        }

        function loadProfile() {
            let role_text = ['Tài khoản chưa được kích hoạt', 'Người dùng', 'Quản trị shop', 'Quản trị chuỗi shop', 'Quản trị viên'];
            $('#btn-save').prop('disabled', true);
            $.ajax({
                url: "{{url('/shop/profiles')}}",
                method: "GET",
                loading: {type: 'content'},
                success: function (obj, textStatus, xhr) {
                    $('.bcoins').text(obj.profile.bcoins.formatComma()).counterUp({
                        delay: 10,
                        time: 1000
                    });
                    userId = obj.profile.id;
                    currentStylish = [];
                    let stylishText = '';
                    for (const stylish of obj.stylish) {
                        currentStylish.push(stylish.id);
                        stylishText += `${stylish.name}, `;
                    }
                    let rangeAge = isEmptyValue(obj?.shop?.range_age) ? [0, 100] : obj.shop.range_age.split(',');
                    $(".range-age").slider("option", "values", rangeAge);
                    $ageFrom.text(rangeAge[0]);
                    $ageTo.text(rangeAge[1]);
                    $('input[name=stylish-text]').val(stylishText.slice(0, -2));
                    $("#btn-save").prop('disabled', false);
                    $('input[name=full_name]').val(obj.profile.fullname);
                    $('input[name=shop_name]').val(obj.shop.name);
                    $('input[name=phone]').val(obj.profile.phone);
                    $('input[name=email]').val(obj.profile.email);
                    $('input[name=admin_role]').val(role_text[obj.profile.role]);
                    $('input[name=parent_shop]').val(obj.parent_shop);
                    $('input[name=shop_id]').val(obj.shop.id);
                    $('input[name=cmnd]').val(obj.shop.cmnd);
                    $('input[name=tax_code]').val(obj.shop.tax_code);
                    $('textarea[name=description]').val(obj.shop.description);
                    $('.ratio-order-success').text(`Tỉ lệ đơn thành công: ${obj.profile?.ratio_order_success ?? 0}%`);
                    $('.ratio-order-fail').text(`Tỉ lệ đơn thất bại: ${obj.profile?.ratio_order_fail ?? 0}%`);
                    $('.rates-of-user .full-stars').css('width', `${obj.profile?.rates / 5 * 100 ?? 0}%`);
                    setImage('#image-cover', obj?.cover?.url, 'cover');
                    setImage('#image-ava', obj?.avatar?.url, 'avatar');
                    // if (obj.cover) {
                    //     document.getElementById('image-cover').setAttribute('src', obj.cover.url);
                    // }
                    // if (obj.avatar) {
                    //     document.getElementById('image-ava').setAttribute('src', obj.avatar.url);
                    // }
                }
            });
        }

        function onClickButtonSave() {
            if (validateProfileForm()) {
                $.ajax({
                    url: "{{url('/shop/profiles')}}",
                    method: "post",
                    data: {
                        _token: "{{csrf_token()}}",
                        ...getProfileObject(),
                    },
                    success: function (obj, textStatus, xhr) {
                        loadProfile();
                        toastSuccessMessage(obj.message);
                    }
                })
            }

        }

        function loadStylishes() {
            $.ajax({
                url: '{{url('/shop/all-stylish')}}',
                method: 'GET',
            }).done(res => {
                dt.stylishes = res?.stylishes ?? [];
            }).fail(err => {
                toastError(err, 'Load stylish thất bại');
            });
        }

        $('.update-stylish').on('click', function () {
            let allStylish = '';
            if (isEmptyValue(dt.stylishes)) return toastInfoMessage('Không có phong cách nào');
            for (const stylish of dt.stylishes) {
                let check = '';
                if (currentStylish.includes(stylish.id)) check = 'checked';
                allStylish += `<div class="form-check">
                                        <input name=${stylish.id} class="form-check-input" type="checkbox" value="${stylish.name}" ${check}>
                                        <label class="form-check-label">${stylish.name}</label>
                                    </div>`;
            }
            $('.modal-stylish .select-stylish').empty().append(allStylish);
            $('.modal-stylish').modal('show');
        })
    </script>
@endsection
