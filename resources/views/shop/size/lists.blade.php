@extends('shop.layout')
@section('shop_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách các ảnh kích thước
                </div>

                <div class="row w3-res-tb row-action-bisto-css">
                    <div class="col-sm-5">
                        <button data-toggle="modal"
                                class="btn-show-add-bisto-css btn-show-add-size btn btn-primary"
                                data-target=".add-size">+
                            Thêm ảnh kích thước
                        </button>
                    </div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input onkeydown="searchSizeImage()" type="search" placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm form-control btn-search-bisto-css"
                                   name="keywords_submit">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th class="text-center">Tên</th>
                                <th class="text-center">Ảnh</th>
                                <th class="text-center">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody id="tbl-size-image">
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div style="" id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
    </div>

    <!-- ADD SIZE -->
    <div class="modal fade add-size" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content" style="border-radius: 20px">
                <div class="iq-card">
                    <section class="panel">
                        <header class="panel-heading title-size">
                            Thêm ảnh kích thước
                        </header>
                        <div class="iq-card-body">
                            <div class="position-center" id="form-size">
                                <div class="form-group">
                                    <label class="required-input" for="image-name">Tên ảnh tương ứng</label>
                                    <input type="text" name="name" class="form-control">
                                    <div class="mes-v-display mes-name"></div>
                                </div>
                                <div class="form-group" style="overflow: auto">
                                    <label class="required-input">Ảnh kích thước</label><span
                                        class="note-img note-img-1-normal"></span>
                                    <div class="custom-file">
                                        <input type="file"
                                               class="form-control-file custom-file-input" id="sizeImg">
                                        <div id="img-container" class="row align-items-start">
                                        </div>
                                        <label class="custom-file-label">Chọn
                                            ảnh</label>
                                    </div>
                                    <div class="mes-source_url mes-v-display">
                                        Bạn phải thêm ít nhất 1 ảnh
                                    </div>
                                </div>
                                <button
                                    class="btn btn-primary btn-save-size">Thêm ảnh
                                </button>
                                <button
                                    class="btn btn-primary bisto-none btn-update-size">Cập nhật ảnh
                                </button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <!-- END ADD SIZE -->
    <script>
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: 4}};
        let pagination, uploaderSize, _i;
        $(document).ready(function () {
            _i = defineImage({id: 'size'});
            loadSizeImage(dt);
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadSizeImage(dt);
                }
            });
            uploaderSize = defineUploadFile({
                inputSelector: '#sizeImg',
                containerSelector: '#img-container', imageName: 'source_url', maxFiles: 1
            })
        });

        function loadSizeImage({currentPage = 1, keyword = '', loading = {type: 'table', colspan: 4}}) {
            $.ajax({
                url: "{{url('/shop/image-sizes?page=')}}" + currentPage + "&keyword=" + keyword,
                method: "GET",
                loading,
                success: function (obj, textStatus, xhr) {
                    const sizeImages = obj.size_image.data;
                    if (sizeImages.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadSizeImage(dt);
                    } else if (sizeImages.length === 0) {
                        $('.table-striped>tbody').empty().append(genNoContentTable(4, 'Không có ảnh kích cỡ nào'));
                    } else {
                        generateSizeImage(sizeImages);
                        pagination.setTotalItems(obj.size_image.total);
                    }
                },
                fail: function (err, xhr) {
                }
            })
        }

        function searchSizeImage() {
            if (event.key === "Enter") {
                dt.keyword = $('input[name=keywords_submit]').val();
                loadSizeImage(dt);
            }
        }

        function generateSizeImage(sizeImages) {
            _i.resetPhotos();
            let tbody = '';
            for (let i = 0; i < sizeImages.length; i++) {
                tbody += `<tr>
                        <td class="text-center">${i + 1}</td>
                        <td class="text-center">${sizeImages[i].name}</td>
                        <td class="text-center">${_i.toHTML(sizeImages[i].url, sizeImages[i].name)}</td>
                        <td class="text-center">
                            <span sizeimageid="${sizeImages[i].id}"  class="fas fa-edit fa-lg btn-show-update-bisto-css btn-show-edit text-success"></span>
                            <span sizeimageid="${sizeImages[i].id}" class="fa fa-times fa-lg btn-show-delete text-danger btn-delete-bisto-css"></span>
                        </td>
                    </tr>`;
            }
            $('#tbl-size-image').empty().append(tbody);
            _i.animateOnloadImage({});
        }

        $(document).on('click', '.btn-show-delete', function () {
            const id = $(this).attr('sizeimageid');
            confirmDelete('Bạn có chắc muốn xóa ảnh này', function () {
                $.ajax({
                    url: `image-sizes/${id}`,
                    method: 'DELETE',
                    data: {
                        _token: "{{csrf_token()}}",
                    }
                }).done(res => {
                    loadSizeImage({...dt, loading: {type: 'content'}});
                    toastSuccess(res, 'Xóa ảnh thành công')
                }).fail(err => {
                    $('.modal').modal('hide');
                })
            })
        })

        function validateFormSize(parentSelector = document) {
            return validateInput({
                listValidateFunction: [{
                    func: requiredValidation,
                    mes: 'Tên ảnh bắt buộc'
                }],
                propertyName: 'name', parentSelector
            }) && uploaderSize.validateUploading('.mes-source_url') && validateRequiredElement({
                inputSelector: 'input[name=source_url]',
                mesSelector: '.mes-source_url',
                message: 'Bạn phải thêm ít nhất một ảnh', parentSelector
            });
        }

        $(document).on('click', '.btn-show-edit', function () {
            const id = parseInt($(this).attr('sizeimageid'));
            $('.btn-update-size').data('id', id).show();
            $('.btn-save-size').hide();
            $('.title-size').text('Cập nhật ảnh kích thước')
            $.ajax({
                url: `image-sizes/${id}`,
                method: 'GET',
            }).done(res => {
                dataToForm({
                    fieldInputs: [{selector: 'input[name=name]', value: res.result.name}],
                    fieldFiles: [{
                        uploaderInstance: uploaderSize,
                        listFiles: [{name: res.result.image_id, url: res.result.url}]
                    }],
                    parentSelector: '#form-size', action: 'clear-validate'
                })
                $('.modal.add-size').modal('show');
            }).fail(err => {
            })
        })

        $(document).on('click', '.btn-save-size', function () {
            if (validateFormSize()) {
                const size = formToData('#form-size');
                $.ajax({
                    url: `image-sizes`,
                    method: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        ...size,
                        source_url: uploaderSize.getOneImg(),
                    },
                }).done(res => {
                    toastSuccess(res, 'Thêm ảnh thành công');
                    loadSizeImage({...dt, loading: {type: 'content'}});
                    $('.modal').modal('hide');
                }).fail(err => {
                    console.log(err);
                })
            }
        })

        $(document).on('click', '.btn-update-size', function () {
            if (validateFormSize()) {
                const size = formToData('#form-size');
                $.ajax({
                    url: `image-sizes/${$('.btn-update-size').data('id')}`,
                    method: 'PUT',
                    data: {
                        _token: "{{csrf_token()}}",
                        ...size,
                        deleted_imageid: uploaderSize.getDeleteImageIds(),
                        source_url: uploaderSize.getOneImg(),
                    },
                }).done(res => {
                    toastSuccess(res, 'Cập nhật ảnh thành công');
                    loadSizeImage({...dt, loading: {type: 'content'}});
                    $('.modal.add-size').modal('hide');
                }).fail(err => {
                    console.log(err);
                })
            }
        })

        $(document).on('click', '.btn-show-add-size', function () {
            $('.btn-save-size').show();
            $('.btn-update-size').hide();
            $('.title-size').text('Thêm ảnh kích thước')
            dataToForm({
                fieldInputs: [{selector: 'input[name=name]', value: ''}],
                fieldFiles: [{
                    uploaderInstance: uploaderSize,
                    listFiles: []
                }],
                parentSelector: '#form-size', action: 'clear-validate'
            })
        })

    </script>
@endsection
