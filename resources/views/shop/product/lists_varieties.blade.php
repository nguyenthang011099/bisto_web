@extends('shop.layout')
@section('shop_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách sản phẩm hết hàng
                </div>

                <div style="margin-top: 20px" class="row row-action-bisto-css">
                    <div class="col-sm-6">
                        <h5 style="margin-left: 20px">Tổng số sản phẩm: <span
                                id="total-product"></span></h5>
                    </div>
                    <div class="col-sm-3 super-center">
                        {{--                        <div class="btn btn-primary btn-bisto-height-css"--}}
                        {{--                             onclick="onShowModalAddProduct()">+ Thêm sản phẩm mới--}}
                        {{--                        </div>--}}
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input onkeydown="searchProduct()" id="input-text-product" type="search"
                                   placeholder="&#xf002 Tìm kiếm" class="btn-search-bisto-css input-sm form-control"
                                   name="keywords_submit">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Tên sản phẩm</th>
                            <th class="text-center">Giá</th>
                            <th class="text-center">Hình sản phẩm</th>
                            <th class="text-center">Danh mục</th>
                            <th class="text-center">Màu sắc</th>
                            <th class="text-center">Size</th>
                            <th class="text-center">Giá biến thể</th>
                            <th class="text-center">Đã bán</th>
                            <th class="text-center">Số lượng</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody id="tbl-product">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->

                <!-- footer and pagination -->
            </div>
        </div>
        <!-- ADD PRODUCT START -->
        <div id="modal-add-update-product-container">
            <div id="modal-add-update-product" class="modal fade add-product" tabindex="-1" role="dialog"
                 aria-labelledby="myExtraLargeModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content" style="border-radius: 20px">
                        <div class="iq-card">
                            <header class="panel-heading title-product">
                                Thêm sản phẩm
                            </header>
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="iq-card-body">
                                        <div id="form-add-product" role="form"
                                             enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label>Tên sản phẩm</label><span class="text-danger">*</span>
                                                <input type="text" name="name" class="need-validate form-control">
                                                <div class="mes-name mes-invalid">
                                                    Tên sản phẩm là bắt buộc
                                                </div>
                                            </div>

                                            <div class="form-group" style="overflow: auto">
                                                <label class="required-input">Hình ảnh sản phẩm</label><span
                                                    class="note-img-10-normal note-img"></span>
                                                <div class="custom-file">
                                                    <input type="file"
                                                           class="form-control-file custom-file-input inputfile"
                                                           id="productImg" multiple>
                                                    <div id="img-container" class="row align-items-start">
                                                    </div>
                                                    <label class="custom-file-label" for="customFile">Chọn
                                                        ảnh</label>
                                                </div>
                                                <div class="mes-imageUrl mes-invalid">
                                                    Bạn phải thêm ít nhất 1 ảnh
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Độ tuổi <span class="age-from"></span> - <span
                                                                class="age-to"></span></label>
                                                        <div class="range-age"></div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Phong cách <span
                                                                    class="fa fa-edit text-success update-stylish"></span></label>
                                                            <input class="form-control" style="text-overflow: ellipsis;"
                                                                   readonly
                                                                   name="stylish-text" value=""/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="required-input">Mô tả sản phẩm</label>
                                                <textarea style="resize: none;height: 250px;line-height: 1.5rem;"
                                                          rows="5"
                                                          class="form-control"
                                                          name="description"
                                                          placeholder="Mô tả sản phẩ"></textarea>
                                                <div class="mes-description mes-v-display"></div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-sm-4">
                                                    <label class="required-input">Danh mục gốc</label>
                                                    <select name="category_id" class="form-control input-sm m-bot15">
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="required-input">Danh mục con cấp 2</label>
                                                    <select name="category_id_level2"
                                                            class="form-control input-sm m-bot15">
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="required-input">Danh mục con cấp 3</label>
                                                    <select name="category_id_level3"
                                                            class="form-control input-sm m-bot15">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="mes-category_id_level3 mes-invalid">
                                                Bạn phải chọn danh mục cho sản phẩm
                                            </div>
                                            <div class="form-group">
                                                <label>Giá sản phẩm</label><span class="text-danger">*</span>
                                                <input type="number" data-validation="number"
                                                       name="price" class="need-validate form-control"/>
                                                <div class="mes-price mes-invalid">
                                                    Giá sản phẩm là bắt buộc
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <h4>Thêm các biến thể của sản phẩm</h4>
                                                <div class="product-size-color-container">
                                                    <div class="product-size-color-1 product-size-color flex-row-wrap">
                                                        <div class="col-sm form-group">
                                                            <label for="exampleInputPassword1">Kích cỡ</label>
                                                            <select name="size_id" class="form-control">
                                                                <option value="1">XL</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm form-group">
                                                            <label for="exampleInputPassword1">Màu</label>
                                                            <select name="color_id"
                                                                    class="form-control">
                                                                <option value="1">Đỏ</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm form-group">
                                                            <label>Số lượng</label><span class="text-danger">*</span>
                                                            <input type="number" data-validation="number"
                                                                   data-validation-error-msg="Làm ơn điền số lượng"
                                                                   name="quantity"
                                                                   class="need-validate form-control">
                                                            <div class="mes-quantity mes-invalid">
                                                                Số lượng là bắt buộc
                                                            </div>
                                                        </div>
                                                        <div class="col-sm form-group">
                                                            <label>Giá</label><span class="text-danger">*</span>
                                                            <input type="number" data-validation="number"
                                                                   name="price-size-color"
                                                                   class="need-validate form-control"/>
                                                            <div class="mes-price-size-color mes-invalid">
                                                                Giá là bắt buộc
                                                            </div>
                                                        </div>
                                                        <div onclick="removeParent(this)"
                                                             class="col-sm super-center minus-product-size-color">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                 height="24"
                                                                 viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                                 stroke-width="2" stroke-linecap="round"
                                                                 stroke-linejoin="round"
                                                                 class="feather feather-minus-circle">
                                                                <circle cx="12" cy="12" r="10"></circle>
                                                                <line x1="8" y1="12" x2="16" y2="12"></line>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mes-dup-sc mes-invalid">
                                                    Trùng lựa chọn size và màu
                                                </div>
                                                <div class=" mes-required-psc mes-invalid">
                                                </div>
                                                <button type="button"
                                                        class="btn-add-product-size-color btn btn-primary">+ Thêm 1 biến
                                                    thể
                                                </button>
                                            </div>
                                            <div class="form-group">
                                                <label>Phần trăm giảm giá</label>
                                                <input type="number"
                                                       name="discount"
                                                       class="form-control"/>
                                                <div class="mes-discount mes-v-display">
                                                </div>
                                            </div>
                                            <div class="iq-card">
                                                <div class="form-row">
                                                    <div class="col">
                                                        <label>Ngày bắt đầu</label>
                                                        <input type="datetime-local"
                                                               class="form-control" id="start_at" name="start_at">
                                                    </div>
                                                    <div class="col">
                                                        <label>Ngày kết thúc</label>
                                                        <input type="datetime-local" class="form-control"
                                                               id="end_at" name="end_at">
                                                    </div>
                                                </div>
                                                <div class="mes-rangedate mes-invalid">
                                                    Ngày bắt đầu phải nhỏ hơn ngày kết thúc
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Trạng thái</label>
                                                <select name="self_status" class="form-control input-sm m-bot15">
                                                    <option value="1">Hiển thị</option>
                                                    <option value="-1">Ẩn</option>
                                                </select>
                                            </div>

                                            <button id="btn-add-product"
                                                    class="btn btn-primary btn-add-bisto-css">Thêm sản phẩm
                                            </button>
                                            <button style="display: none" id="btn-update-product"
                                                    class="btn btn-primary btn-update-bisto-css">Lưu thay đổi sản phẩm
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-container">

            </div>
        </div>
        <!-- ADD PRODUCT END -->

        <!-- Modal choose stylish -->
        <div class="modal modal-stylish" tabindex="-1">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Chỉnh sửa phong cách vip</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="select-stylish"></div>
                        <div class="mes-stylish" style="display: none;color: red">
                            Bạn phải chọn ít nhất 1 phong cách
                        </div>
                        <div class="mes-overload-selected" style="display: none;color: red">
                            Bạn chỉ được chọn tối đa 3 phong cách
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn-save-stylish btn btn-primary">Đồng ý</button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            let dt = {
                currentPage: 1,
                searchKey: '',
                currentProductId: null,
                loading: {type: 'table', colspan: 9},
                quantity: 0
            }
            let elementProductSizeColor = document.getElementsByClassName('product-size-color')[0].outerHTML || '';
            let modalAddUpdateProductElement = $('#modal-add-update-product')[0].outerHTML || '';
            let optionsMale = ''
            let optionsFemale = ''
            let currentStylish = []
            let selecterCategory, pagination, img = defineImage({id: 'product'});
            const $ageFrom = $('.age-from');
            const $ageTo = $('.age-to');
            let countSelectedStylist = 0;

            let uploaderProductImage;
            $(document).ready(function () {
                const params = parseUrlParams();
                if (params?.quantity === '0') {
                    loadProducts({...dt, quantity: '0'});
                } else loadProducts(dt);
                loadStylishes();
                loadSizeColorCategory();
                setDefaultInputDate();
                validateOnKeyUp();
                uploaderProductImage = defineUploadFile({
                    inputSelector: '#productImg',
                    containerSelector: '#img-container',
                })
                pagination = definePagination({
                    onPageClick: (pageNumber, event) => {
                        dt.currentPage = pageNumber;
                        loadProducts(dt);
                    }
                });

                $ageFrom.text(0);
                $ageTo.text(100);
                $(".range-age").slider({
                    range: true,
                    min: 0,
                    max: 100,
                    values: [0, 100],
                    slide: function (event, ui) {
                        $ageFrom.text(ui.values[0]);
                        $ageTo.text(ui.values[1]);
                    }
                });

                $('#modal-add-update-product').on('hide.bs.modal', function () {
                    countSelectedStylist = 0
                    $('.mes-overload-selected').css('display', 'none')
                })

                $('.modal-stylish').on('hide.bs.modal', function () {
                    $('.mes-overload-selected').css('display', 'none')
                    countSelectedStylist = currentStylish.length
                })

                $('.modal-stylish').on('show.bs.modal', function () {
                    countSelectedStylist = currentStylish.length
                })
            });

            //validate when user typing on input
            function validateOnKeyUp() {
                validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên sản phẩm là bắt buộc'
                    }],
                    propertyName: 'name',
                    type: 'onkeyup'
                });
                validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Giá sản phẩm là bắt buộc'
                    }, {
                        func: rangeValidation,
                        mes: 'Giá tối thiểu 1000 VND',
                        params: {
                            min: 1000,
                            max: MAX_MONEY,
                        }
                    }], propertyName: 'price',
                    type: 'onkeyup'
                });
                validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Số lượng là bắt buộc'
                    }, {
                        func: rangeValidation, mes: 'Số lượng tối thiểu là 1',
                        params: {
                            min: 1,
                            max: MAX_INT32,
                        }
                    }], propertyName: 'quantity',
                    type: 'onkeyup'
                });
                validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Giá là bắt buộc'
                    }, {
                        func: rangeValidation,
                        mes: 'Giá tối thiểu 1000 VND',
                        params: {
                            min: 1000,
                            max: MAX_MONEY,
                        }
                    }], propertyName: 'price-size-color',
                    type: 'onkeyup'
                });
                validateRangeNumber('input[name=price]', 1000, MAX_MONEY);
                validateRangeNumber('input[name=price-size-color]', 1000, MAX_MONEY);
                validateRangeNumber('input[name=quantity]', 1, MAX_INT32, true);
                validateRangeNumber('input[name=discount]', 0, 100);
            }

            function setDefaultInputDate() {
                setInputDateTime('start_at');
                setInputDateTime('end_at');
            }

            //event delete product-size-color element
            function removeParent(e) {
                e.parentElement.remove();
            }

            //add element input product size color
            $('.btn-add-product-size-color').on('click', function () {
                document.getElementsByClassName('product-size-color-container')[0].appendChild(createElementByText(elementProductSizeColor))
                validateOnKeyUp();
            })

            function onShowModalAddProduct() {
                $('.title-product').empty().text('Thêm sản phẩm');
                $('select[name=self_status]').val(1).attr('disabled', true);
                $('#modal-add-update-product').modal('show');
                $('button#btn-update-product').hide();
                $('button#btn-add-product').show();
                $('select[name=category_id_level2]').empty();
                $('select[name=category_id_level3]').empty();
                dataToForm({
                    fieldInputs: [{selector: 'input[name=name]'},
                        {selector: 'textarea[name=description]'},
                        {selector: 'select[name=category_id]'},
                        {selector: 'select[name=category_id_level3]'},
                        {selector: 'select[name=category_id_level2]'},
                        {selector: 'input[name=price]'},
                        {selector: 'input[name=discount]'},
                        {selector: 'input[name=start_at]'},
                        {selector: 'input[name=end_at]'}
                    ],
                    fieldFiles: [{
                        uploaderInstance: uploaderProductImage, listFiles: []
                    }], action: 'clear-validate'
                })
                $('.product-size-color-container').empty().append(elementProductSizeColor);
            }

            function onShowModalUpdateProduct() {
                clearValidate({});
                $('.title-product').empty().text('Cập nhật sản phẩm');
                $('select[name=self_status]').attr('disabled', false);
                $('button#btn-update-product').show();
                $('button#btn-add-product').hide();
                const id = event.target.getAttribute('productid');
                $('#modal-add-update-product').data('productid', id);
                $.ajax({
                    url: `{{url('/shop')}}/detail-product/${id}`,
                    method: 'GET',
                }).done(res => {
                    let stylist_id = [];
                    currentStylish = []
                    dt.stylishes.forEach(function (value) {
                        if (res.results.stylist_id.includes(value.id)) {
                            stylist_id.push(value.name)
                            currentStylish.push(value.id)
                        }
                    })
                    $('input[name=stylish-text]').val(stylist_id.join(', '))
                    $(".range-age").slider({
                        range: true,
                        min: 0,
                        max: 100,
                        values: res.results.range_age.split(','),
                        slide: function (event, ui) {
                            $ageFrom.text(ui.values[0]);
                            $ageTo.text(ui.values[1]);
                        }
                    });

                    let product = res.results || {};
                    let listValuesSelectCategory = [];
                    listValuesSelectCategory.push(product.category.id)
                    let _parent = product.category.parent;
                    while (_parent) {
                        listValuesSelectCategory.push(_parent.id);
                        _parent = _parent.parent;
                    }
                    selecterCategory.loadSelects(listValuesSelectCategory.reverse());
                    $('input[name=name]').val(product.name);
                    uploaderProductImage.setUpdateFileUrls(product.images);
                    if (product.description)
                        $('textarea[name=description]').val(product.description);
                    if (product.category_parent_id == 1) {
                        $('select[name=category_id]').empty().append(optionsMale);
                        $('input.male').attr('checked', true);
                    } else {
                        $('select[name=category_id]').empty().append(optionsFemale);
                        $('input.female').attr('checked', true);
                    }
                    $('select[name=category_id]').val(product.category_id);
                    $('input[name=discount]').val(product.discount);
                    $('input[name=price]').val(product.price);
                    $('select[name=self_status]').val(product.self_status);
                    let start_at = new Date(product.start_at);
                    start_at.setMinutes(start_at.getMinutes() - start_at.getTimezoneOffset());
                    $('input[name=start_at]')[0].value = start_at.toISOString().slice(0, 16);
                    let end_at = new Date(product.end_at);
                    end_at.setMinutes(end_at.getMinutes() - end_at.getTimezoneOffset());
                    $('input[name=end_at]')[0].value = end_at.toISOString().slice(0, 16);
                    $('div.product-size-color-container').empty();
                    for (const productSizeColor of product.product_size_color) {
                        let newElementProductSizeColor = createElementByText(elementProductSizeColor);
                        newElementProductSizeColor.querySelector('select[name=size_id]').value = productSizeColor.size_id;
                        newElementProductSizeColor.querySelector('select[name=color_id]').value = productSizeColor.color_id;
                        newElementProductSizeColor.querySelector('input[name=price-size-color]').value = productSizeColor.price;
                        newElementProductSizeColor.querySelector('input[name=quantity]').value = productSizeColor.quantity;
                        $('div.product-size-color-container').append(newElementProductSizeColor);
                    }
                    validateOnKeyUp();
                    $('#modal-add-update-product').modal('show');
                })
                    .fail(err => {
                    })
            }

            //validate before submit
            function validateFormProduct(type) {
                return validateInput({
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Tên sản phẩm là bắt buộc'
                        }],
                        propertyName: 'name'
                    }) && validateInput({
                        listValidateFunction: [{
                            func: requiredElementValidation,
                            mes: 'Bạn phải thêm ít nhất một ảnh',
                            params: {classNameElement: 'input[name=imageUrl]'}
                        }], type: 'custom', customInput: 'input[name=imageUrl]', customMes: '.mes-imageUrl'
                    }) &&
                    validateInput({
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Mô tả là bắt buộc',
                        }], type: 'textarea', propertyName: 'description'
                    }) &&
                    validateInput({
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Bạn phải chọn cả 3 cấp độ danh mục trên'
                        }], propertyName: 'category_id_level3', type: 'select'
                    }) &&
                    validateInput({
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Giá sản phẩm là bắt buộc'
                        }, {
                            func: rangeValidation,
                            mes: 'Giá tối thiểu 1000 VND',
                            params: {
                                min: 1000,
                                max: MAX_MONEY,
                            }
                        }], propertyName: 'price'
                    }) && validateInput({
                        listValidateFunction: [{
                            func: requiredElementValidation,
                            mes: 'Bạn phải thêm ít nhất 1 biến thể',
                            params: {classNameElement: 'div.product-size-color'}
                        }], type: 'custom', customMes: '.mes-required-psc'
                    }) && validateDuplicateSizeColorOnSubmit('.product-size-color') &&
                    validateInput({
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Số lượng là bắt buộc'
                        }, {
                            func: rangeValidation, mes: 'Số lượng tối thiểu là 1',
                            params: {
                                min: 1,
                                max: MAX_INT32,
                            }
                        }], propertyName: 'quantity'
                    }) &&
                    validateInput({
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Giá là bắt buộc'
                        }, {
                            func: rangeValidation,
                            mes: 'Giá tối thiểu là 1000 VND',
                            params: {
                                min: 1000,
                                max: MAX_MONEY,
                            }
                        }], propertyName: 'price-size-color'
                    }) &&
                    validateInput({
                        listValidateFunction: [{
                            func: rangeValidation,
                            mes: 'Phần trăm giảm giá lớn hơn 0 và nhỏ hơn 100',
                            params: {
                                min: 0,
                                max: 100,
                            }
                        }], propertyName: 'discount'
                    }) && validateRangeDate('input[name=start_at]', 'input[name=end_at]', 'div.mes-rangedate');
            }

            //get product object from form
            function getProductObject() {
                const listS = $('.select-stylish :input').serializeArray().map(item => parseInt(item.name));
                let product = {
                    name: $('input[name=name]').val() || '',
                    stylish: isEmptyValue(listS) ? currentStylish : listS,
                    range_age: [parseInt($ageFrom.text()), parseInt($ageTo.text())],
                    price: parseFloat($('input[name=price]').val()) || 0,
                    discount: parseFloat($('input[name=discount]').val()) || 0,
                    self_status: parseInt($('select[name=self_status]').val()),
                    start_at: new Date($('input[name=start_at]').val()).getTime() / 1000 || null,
                    end_at: new Date($('input[name=end_at]').val()).getTime() / 1000 || null,
                    category_id: selecterCategory.getCurrentValueSelected().value || 0,
                    /*description: {
                        detail: $('textarea[name=description]').val() || ''
                    }*/
                    description: $('textarea[name=description]').val() || ''
                }
                let product_size_color = [];
                for (const productSizeColorElement of $('.product-size-color')) {
                    let p = {};
                    p.color_id = parseInt(productSizeColorElement.querySelector('select[name=color_id]').value) || 0;
                    p.size_id = parseInt(productSizeColorElement.querySelector('select[name=size_id]').value) || 0;
                    p.quantity = parseInt(productSizeColorElement.querySelector('input[name=quantity]').value) || 0;
                    p.price = parseInt(productSizeColorElement.querySelector('input[name=price-size-color]').value) || 0;
                    product_size_color.push(p);
                }
                product.product_size_color = product_size_color;
                product.image_urls = uploaderProductImage.getMultiImg();
                return product;
            }

            //handle event onclick when user press save product
            $('button#btn-add-product').on('click', function (e) {
                if (validateFormProduct()) {
                    $.ajax({
                        url: '{{url('/shop/save-product')}}',
                        method: 'POST',
                        data: {
                            _token: "{{csrf_token()}}",
                            ...getProductObject(),
                        },
                    }).done((res, textStatus, xhr) => {
                        if (xhr.code == 403) {
                            toastErrorMessage(res.message);
                        } else {
                            toastSuccessMessage(res.message);
                        }
                        $('#modal-add-update-product').modal('hide');
                        loadProducts({...dt, loading: {type: 'content'}});
                    })
                        .fail(err => {

                        })
                }
            })

            //when user enter update-product
            $(document).on('click', '#btn-update-product', function (e) {
                const productId = $('#modal-add-update-product').data('productid');
                const _url = 'update-product/' + productId;
                if (validateFormProduct()) {
                    $.ajax({
                        url: _url,
                        method: 'POST',
                        data: {
                            _token: "{{csrf_token()}}",
                            ...getProductObject(),
                            image_urls: uploaderProductImage.getMultiImg(),
                        },
                    }).done(res => {
                        toastSuccessMessage('Cập nhật sản phẩm thành công')
                        $('#modal-add-update-product').modal('hide');
                        loadProducts({...dt, loading: {type: 'content'}});
                    })
                        .fail(err => {
                        })
                }
            })

            //when user enter delete-product
            $(document).on('click', '.on-delete-product', function () {
                const productId = $(this).attr('productid');
                confirmDelete('Bạn có chắc muốn xóa sản phẩm này?', function () {
                    const _url = 'product/' + productId;
                    $.ajax({
                        url: _url,
                        method: 'DELETE',
                        data: {
                            _token: '{{csrf_token()}}'
                        }
                    }).done(res => {
                        toastSuccessMessage('Xóa sản phẩm thành công')
                        loadProducts({...dt, loading: {type: 'content'}});
                    })
                        .fail(err => {
                        })
                })
            })

            //products to interface
            function generateProducts(products, search = false) {
                let tbody = '';
                img.resetPhotos();
                for (let i = 0; i < products.length; i++) {
                    const varieties = products[i].varieties;
                    const rowspan = varieties.length ?? 1;
                    const id = products[i].id;
                    let isActive = null;
                    if (products[i].self_status === 1)
                        isActive = `<span class="badge badge-success">Hiển thị</span>`;
                    else isActive = `<span class="badge badge-danger">Ẩn</span>`;
                    let isBlock;
                    if (products[i].admin_status === 1)
                        isBlock = `<span id="lock-status-${id}" class="badge badge-success">Không khóa</span>`;
                    else isBlock = `<span id="lock-status-${id}" class="badge badge-danger">Bị khóa</span>`;
                    let trr = `<tr>
                                    <td rowspan="${rowspan}" class="text-center">${i + 1}</td>
                                    <td rowspan="${rowspan}" class="text-center">${products[i].name}</td>
                                    <td rowspan="${rowspan}" class="text-center">${products[i].price.formatVND()}</td>
                                    <td rowspan="${rowspan}" class="text-center">
                                        ${img.toHTML(products[i].image, products[i].name)}
                                    </td>
                                    <td rowspan="${rowspan}" class="text-center">${products[i].category_name}</td>
                                    <td class="text-center">${varieties[0].color_name}</td>
                                    <td class="text-center">${varieties[0].size_name}</td>
                                    <td class="text-center">${varieties[0].price.formatVND()}</td>
                                    <td class="text-center">${varieties[0].sold}</td>
                                    <td class="text-center">${varieties[0].quantity}</td>
                                    <td class="text-center" rowspan="${rowspan}">
                                        <span class="text-ellipsis">${isActive}</span>
                                        <span class="text-ellipsis">${isBlock}</span>
                                    </td>
                                    <td class="text-center" rowspan="${rowspan}">
                                        <span onclick="onShowModalUpdateProduct()" productid=${id}
                                              class="btn-show-update-bisto-css fas fa-lg fa-edit btn-show-modal-edit-product text-success"></span>
                                        <span productid=${id}
                                              class="fa on-delete-product fa-times fa-lg text-danger btn-delete-bisto-css"></span>
                                    </td>
                                </tr>`;
                    for (let j = 1; j < varieties.length; j++) {
                        trr += `<tr>
                                    <td class="text-center">${varieties[j].color_name}</td>
                                    <td class="text-center">${varieties[j].size_name}</td>
                                    <td class="text-center">${varieties[j].price.formatVND()}</td>
                                    <td class="text-center">${varieties[j].sold}</td>
                                    <td class="text-center">${varieties[j].quantity}</td>
                                </tr>`;
                    }
                    tbody += trr;
                }
                $('.table-striped>tbody').empty().append(tbody);

                img.animateOnloadImage({});
            }

            //call api get product by paging and generate on interface
            function loadProducts({
                                      currentPage = 1,
                                      searchKey = '',
                                      loading = {type: 'table', colspan: 12},
                                      quantity = 0
                                  }) {
                $.ajax({
                    url: `{{url('/shop')}}/product-varieties?keyword=${searchKey}&page=${currentPage}&quantity=${quantity}`,
                    loading,
                    method: 'GET',
                })
                    .done(res => {
                        const products = res?.products?.data ?? [];
                        if (products.length === 0 && dt.currentPage > 1) {
                            dt.currentPage -= 1;
                            pagination.setCurrentPage(1);
                            loadProducts(dt);
                        } else if (products.length === 0) {
                            $('.table-striped>tbody').empty().append(genNoContentTable(12, `Không có sản phẩm nào`));
                        } else {
                            $('#total-product').text(res.products.total);
                            generateProducts(products);
                            pagination.setTotalItems(res.products.total);
                        }
                    })
                    .fail(err => {
                    });
            }

            function onOptionHover() {
                $('option', 'select[name=color_id]').hover(function (e) {
                })
            }

            $(document).on('change', 'select[name=color_id]', function (e) {
                e.currentTarget.style.backgroundColor = e.currentTarget.selectedOptions[0].style.color;
            })

            //call api get size color category and generate on interface
            function loadSizeColorCategory() {
                $.ajax({
                    url: '{{url('/shop/all-size')}}',
                    method: 'GET'
                }).done(res => {
                    let options = ''
                    res.results.forEach(item => {
                        options += `<option value="${item.id}">${item.name}</option>`
                    })
                    $('select[name=size_id]').empty().append(options);
                    elementProductSizeColor = $('div.product-size-color')[0].outerHTML;
                }).fail(err => {
                    // alert('Load size thất bại');
                });

                $.ajax({
                    url: '{{url('/shop/all-color')}}',
                    method: 'GET'
                }).done(res => {
                    let options = ''
                    res.results.forEach(item => {
                        // options += `<option style="color: ${item.value};background-color: #fff;font-weight: bolder" value="${item.id}"><div>${item.name}</div></option>`
                        options += `<option value="${item.id}">${item.name}</option>`
                    })
                    $('select[name=color_id]').empty().append(options);
                    // $('select[name=color_id]').css('background-color', res.results[0].value).css('color', '#fff !important').empty().append(options);
                    elementProductSizeColor = $('div.product-size-color')[0].outerHTML;
                    onOptionHover();
                }).fail(err => {
                    // alert('Load color thất bại');
                });

                selecterCategory = multiSelect({
                    listSelect: [{
                        selectSelector: 'select[name=category_id]',
                        url: 'root-category', valueName: 'id', showName: 'name'
                    }, {
                        selectSelector: 'select[name=category_id_level2]',
                        url: 'category-level-two/', valueName: 'id', showName: 'name'
                    }, {
                        selectSelector: 'select[name=category_id_level3]',
                        url: 'category-level-three/',
                        valueName: 'id',
                        showName: 'name'
                    }], loading: null
                });
                selecterCategory.loadRootSelect();
            }

            //handle event when user typing enter on input search
            function searchProduct(e) {
                // user press enter search
                if (event.key === 'Enter') {
                    dt.searchKey = $('#input-text-product').val();
                    loadProducts(dt);
                }
            }

            function loadStylishes() {
                $.ajax({
                    url: '{{url('/shop/all-stylish')}}',
                    method: 'GET',
                }).done(res => {
                    dt.stylishes = res?.stylishes ?? [];
                }).fail(err => {
                    toastError(err, 'Load stylish thất bại');
                });
            }

            // handle button update stylish click
            $('.update-stylish').on('click', function () {
                let allStylish = '';
                if (isEmptyValue(dt.stylishes)) return toastInfoMessage('Không có phong cách nào');
                for (const stylish of dt.stylishes) {
                    let check = '';
                    if (currentStylish.includes(stylish.id)) {
                        check = 'checked';
                    }
                    allStylish += `<div class="form-check">
                                <input name=${stylish.id} class="form-check-input stylist-checkbox" type="checkbox" value="${stylish.name}" ${check}>
                                <label class="form-check-label">${stylish.name}</label>
                            </div>`;
                }
                $('.modal-stylish .select-stylish').empty().append(allStylish);
                $('.modal-stylish').modal('show');


                $('.select-stylish :input').change(function () {

                    if (this.checked) {
                        countSelectedStylist++;
                        if (countSelectedStylist > 3) {
                            $('.mes-overload-selected').css('display', 'block')
                            $('.btn-save-stylish').prop('disabled', true)
                        } else if (countSelectedStylist <= 3) {
                            currentStylish.push(parseInt(this.name))
                        }
                    } else {
                        countSelectedStylist--;
                        if (countSelectedStylist <= 3) {
                            $('.btn-save-stylish').prop('disabled', false)
                            $('.mes-overload-selected').css('display', 'none')
                            const temp = currentStylish.indexOf(this.name)
                            if (temp != -1) {
                                currentStylish.splice(temp, 1)
                            }
                        }
                    }
                })
            })

            //handle save stylish button
            $('.btn-save-stylish').on('click', function () {
                let stylishText = '';
                const listS = $('.select-stylish :input').serializeArray();
                if (isEmptyValue(listS)) {
                    return $('.mes-stylish').show();
                } else $('.mes-stylish').hide();
                // for (const stylish of currentStylish) {
                stylishText = dt.stylishes.filter(function (value, index) {
                    if (currentStylish.includes(value.id)) {
                        return value;
                    }
                }).map(value => value.name).join(', ')
                // }

                $('input[name=stylish-text]').val(stylishText);
                $('.modal-stylish').modal("hide");
            })
        </script>
@endsection
