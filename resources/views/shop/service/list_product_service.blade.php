@extends('shop.layout')
@section('shop_content')
    <div id="content-page" class="content-page service-product-shop">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách gói dịch vụ đẩy sản phẩm lên top
                </div>
                <div style="margin-top: 20px" class="row btn-bisto row-action-bisto-css">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-3">
                        {{--                        <div class="input-group">--}}
                        {{--                            <input type="search"--}}
                        {{--                                   placeholder="&#xf002 Tìm kiếm"--}}
                        {{--                                   class="input-sm btn-search-bisto btn-search-bisto-css form-control">--}}
                        {{--                        </div>--}}
                    </div>
                </div>
                <div class="iq-card-body body-bisto">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Số ngày áp dụng</th>
                            <th class="text-center">Số sản phẩm tối đa</th>
                            <th class="text-center">Phí áp dụng</th>
                            <th class="text-center">Thao tác</th>
                        </tr>
                        </thead>

                        <tbody class="tbody-bisto">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <!-- ADD PRODUCT START -->
    </div>
    <script>
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: 5}};
        let _, pagination;

        $(document).ready(function () {
            _ = defineBisto({
                parentSelector: '.service-product-shop', name: 'gói dịch vụ',
            });

            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadProductService(dt);
                }
            });
            loadProductService(dt);

            _.onSearch((value) => {
                dt.keyword = value;
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadProductService(dt);
            });

        });

        function loadProductService({currentPage = 1, keyword = '', loading = {type: 'table', colspan: 5}}) {
            $.ajax({
                url: `{{url('/shop/product-services')}}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
                loading,
            }).done(res => {
                const services = res?.results?.data ?? [];
                if (services?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadProductService(dt);
                } else if (services?.length === 0) {
                    _.$tbody.empty().append(genNoContentTable(5, `Không có gói dịch vụ nào`));
                } else {
                    generateServices(services, res.product_service_registered_id);
                    pagination.setTotalItems(res.results.total);
                }
            }).fail(err => {
            });
        }

        function generateServices(services, productServiceRegisteredId) {
            let tbody = '';
            for (let i = 0; i < services.length; i++) {
                const id = services[i].id;
                let btn = '';
                if (productServiceRegisteredId === null) {
                    btn = `   <button type="button" data-id=${id} class="btn btn-register btn-primary">
                                    Đăng ký
                                </button>`;
                } else if (productServiceRegisteredId === id) {
                    btn = `   <a type="button" href="product-service-register/${id}" class="btn btn-success">
                                    Đã đăng ký
                                </a>`;
                }
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${services[i].date ?? 0}</td>
                            <td class="text-center">${services[i]?.max_product_apply ?? 0}</td>
                            <td class="text-center">${toVND(services[i].price)}</td>
                            <td class="text-center">
                               ${btn}
                            </td>
                        </tr>`;
            }
            _.$tbody.empty().append(tbody);
        }

        $(document).on('click', 'button.btn-register', function () {
            const productServiceId = parseInt($(this).attr('data-id'));
            $.ajax({
                url: `{{url('/shop/product-services')}}`,
                method: 'POST',
                data: {
                    _token: '{{csrf_token()}}',
                    product_service_id: productServiceId,
                },
                loading: {type: 'content'}
            }).done(res => {
                toastSuccess(res, 'Đăng ký gói dịch vụ thành công');
                window.location.href = `{{url('/shop/product-service-register')}}/${productServiceId}`;
            }).fail(err => {
            })
        })
    </script>
@endsection
