@extends('shop.layout')
@section('shop_content')
    <div id="content-page" class="content-page debt-shop">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            @if($is_shop_without_branch == '1')
                <div class="iq-card">
                    <div class="panel-heading">
                        Xuất Excel công nợ
                    </div>
                    <div style="padding: 20px" class="row btn-bisto row-action-bisto-css">
                        <div class="col-sm-3">
                            <select class="form-control" id="btn-range-date" name="btn-range-date">
                                <option value="0" selected>Trong cả tháng</option>
                                <option value="1">Nửa đầu tháng</option>
                                <option value="2">Nửa cuối tháng</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control" id="btn-month" name="btn-month">
                                <option selected value="0">Tất cả 12 tháng</option>
                                <option value="1">Tháng 1</option>
                                <option value="2">Tháng 2</option>
                                <option value="3">Tháng 3</option>
                                <option value="4">Tháng 4</option>
                                <option value="5">Tháng 5</option>
                                <option value="6">Tháng 6</option>
                                <option value="7">Tháng 7</option>
                                <option value="8">Tháng 8</option>
                                <option value="9">Tháng 9</option>
                                <option value="10">Tháng 10</option>
                                <option value="11">Tháng 11</option>
                                <option value="12">Tháng 12</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control" id="btn-year" name="btn-year">
                                @foreach($_years_ as $year)
                                    <option value="{{$year}}">Năm {{$year}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <button class="btn btn-query-debt btn-primary btn-bisto-height-css">Truy vấn</button>
                        </div>
                    </div>
                    <div class="iq-card-body body-bisto">
                        <div class="row d-flex justify-content-center">
                            <div type="button" id="export-shop"
                                 class="btn input-group-prepend btn-outline-success d-flex align-items-center justify-content-between">
                                <span class="fas mr-1 fa-file-excel fa-lg"></span>
                                Xuất excel công nợ
                            </div>
                        </div>
                        <div class="mt-3 body-bisto debt-container">
                            <div>
                                <div class="panel-heading">
                                </div>
                                <table class="table table-striped table-bordered">
                                    <thead class="thead-debt">
                                    <tr>
                                        <th class="text-center">Mã đơn</th>
                                        <th class="text-center">Thời gian</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th class="text-center">Thực thu</th>
                                        <th class="text-center">Phí cố định</th>
                                        <th class="text-center">Hoàn tiền</th>
                                        <th class="text-center">Phí v/c đổi trả</th>
                                    </tr>
                                    </thead>
                                    <tbody class="tbody-debt">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- PAGINATION START-->
                    <div class="pagination-custom">
                        <div id="pagination-container"></div>
                    </div>
                    <!-- PAGINATION END-->
                </div>
            @elseif($is_shop_without_branch == '0')
                <div class="iq-card">
                    <div class="panel-heading">
                        <h5>Shop chi nhánh không có công nợ</h5>
                    </div>
                </div>
            @endif
        </div>
        <!-- ADD PRODUCT START -->
    </div>
    <script>
        let dt = {
            currentPage: 1,
            keyword: '',
            option: '0',
            month: '0',
            year: '2021',
            loading: {type: 'table', colspan: 7},
        };
        let pagination;
        $(document).ready(function () {
            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadDebts(dt);
                }
            });
            loadDebts(dt);
        });


        $('select[name=btn-range-date]').on('change', function (e) {
            dt.option = e.currentTarget.value;
        });

        $('select[name=btn-month]').on('change', function (e) {
            dt.month = e.currentTarget.value;
        });

        $('select[name=btn-year]').on('change', function (e) {
            dt.year = e.currentTarget.value;
        });

        $('button.btn-query-debt').on('click', function (e) {
            loadDebts(dt);
        });

        function getStringParams() {
            const option = document.getElementById('btn-range-date').value;
            const month = document.getElementById('btn-month').value;
            const year = document.getElementById('btn-year').value;
            return `option=${option}&month=${month}&year=${year}`;
        }

        $('#export-shop').on('click', function () {
            const url = `{{url('/shop/debts')}}?${getStringParams()}`;
            $.ajax({
                url: url,
                method: 'GET',
                loading: {type: 'content'}
            }).done(res => {
                if (res) window.open(url, '_blank');
                else toastInfoMessage('Không có dữ liệu');
            }).fail(err => {
            })
        });


        function loadDebts({
                               currentPage = 1,
                               keyword = '',
                               option = '0',
                               month = '0',
                               year = '2021',
                               loading = {type: 'table', colspan: 7},
                           }) {
            $.ajax({
                url: `{{url('/shop')}}/debts?page=${currentPage}`,
                method: "POST",
                data: {
                    _token: '{{csrf_token()}}',
                    option: option,
                    month: month,
                    year: year,
                },
                loading,
            }).done(res => {
                const items = res?.result?.data ?? [];
                if (items?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadDebts(dt);
                } else if (items?.length === 0) {
                    $('.tbody-debt').empty().append(genNoContentTable(7, `Không có dữ liệu`));
                } else {
                    generateDebts(items);
                    pagination.setTotalItems(res?.result?.total ?? 0);
                }
            }).fail(err => {
            });
        }

        function generateDebts(items) {
            let tbody = '';
            for (const item of items) {
                tbody += `<tr>
                            <td class="text-center">${item.code}</td>
                            <td class="text-center">${item.created_at}</td>
                            <td class="text-center">${item.status}</td>
                            <td class="text-center">${item.real_money.formatVND()}</td>
                            <td class="text-center">${item.fixed_charge.formatVND()}</td>
                            <td class="text-center">${item.refund.formatVND()}</td>
                            <td class="text-center">${item.shipping_fee.formatVND()}</td>
                        </tr>`;
            }
            $('.tbody-debt').empty().append(tbody);
        }
    </script>
@endsection
