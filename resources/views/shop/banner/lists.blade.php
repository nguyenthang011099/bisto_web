@extends('shop.layout')
@section('shop_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            @if($is_shop_without_branch == '1')
                <div class="iq-card">
                    <div class="panel-heading">
                        Danh sách Banners
                    </div>

                    <div class="row row-action-bisto-css">
                        <div class="col-sm-5">
                        <span class="btn btn-show-add-bisto-css btn-show-modal-banner btn-primary">+ Thêm
                            banner</span>
                        </div>
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="search"
                                       onkeydown="searchBanner()"
                                       placeholder="&#xf002 Tìm kiếm"
                                       class="input-sm form-control btn-search-bisto-css"
                                       name="input-text-banner">
                            </div>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th class="text-center">Tên Banner</th>
                                <th class="text-center">Hình ảnh</th>
                                <th class="text-center">Vị trí banner</th>
                                <th class="text-center">Ngày bắt đầu</th>
                                <th class="text-center">Ngày kết thúc</th>
                                <th class="text-center">Hiển thị</th>
                                <th class="text-center">Phê duyệt</th>
                                <th class="text-center">Thao tác</th>
                            </tr>
                            </thead>

                            <tbody id="tbl-slider">
                            </tbody>
                        </table>
                    </div>
                    <!-- PAGINATION START-->
                    <div class="pagination-custom">
                        <div style="" id="pagination-container"></div>
                    </div>
                    <!-- PAGINATION END-->
                    <div class="container">
                        <!-- footer and pagination -->
                    </div>
                </div>
                <div class="row justify-content-md-center row-size">
                    <div class="col-sm col-sm-left">
                    </div>
                    <div class="col-sm col-sm-center">
                        <div class="panel-heading">
                            Thông tin thanh toán
                        </div>
                        <table class="table table-bordered mt-4 text-left">
                            <tr>
                                <th>Tên công ty</th>
                                <td>Công ty TNHH Anberry Việt Nam</td>
                            </tr>
                            <tr>
                                <th>Loại ngân hàng</th>
                                <td>Ngân Hàng TMCP Á Châu ACB</td>
                            </tr>
                            <tr>
                                <th>Tên tài khoản nhận</th>
                                <td>Công ty TNHH Anberry Việt Nam</td>
                            </tr>
                            <tr>
                                <th>Số tài khoản nhận</th>
                                <td>9119 6868 6868</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm col-sm-right">
                    </div>
                </div>
            @elseif($is_shop_without_branch == '0')
                <div class="iq-card">
                    <div class="panel-heading">
                        <h5>Liên hệ quản lý chuỗi cửa hàng để đăng ký banner</h5>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <!-- ADD BANNERS -->
    <div class="modal fade add-banner" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content" style="border-radius: 20px">
                <div class="iq-card">
                    <div class="title-banner panel-heading">
                        Đăng ký banner
                    </div>
                    <div class="iq-card-body">
                        <div id="banners-form">
                            <div class="form-group">
                                <label class="required-input">Tên banners</label>
                                <input type="text" class="form-control" id="name_banner" placeholder="Tên banners"
                                       name="name">
                                <div class="mes-invalid mes-name">
                                </div>
                            </div>
                            <div class="form-group" style="overflow: auto">
                                <label class="required-input">Hình ảnh cho banner</label><span
                                    class="note-img note-img-1-13"></span>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input"
                                           name="banner-img">
                                    <div id="img-container" class="row align-items-start"></div>
                                    <label class="custom-file-label">Chọn ảnh</label>
                                </div>
                                <div class="mes-imageUrl mes-invalid">
                                    Bạn phải thêm ít nhất 1 ảnh
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Vị trí banner</label>
                                <select class="form-control" name="position">
                                    <option value="1" selected>Giữa trang chủ</option>
                                    <option value="2">Dưới trang chủ</option>
                                    <option value="3">Đầu tab khám phá</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label>Ngày bắt đầu</label>
                                    <input type="datetime-local" class="form-control" id="start_at"
                                           placeholder="Ngày bắt đầu"
                                           name="start_at">
                                </div>
                                <div class="form-group col-sm-6" id="choose-service-banner">
                                    <label>Chọn dịch vụ</label>
                                    <select class="form-control" name="service_banner_id">
                                        <option value="" selected>---</option>
                                    </select>
                                </div>
                                {{--                                                                <div class="form-group col-sm-6">--}}
                                {{--                                                                    <label>Ngày kết thúc</label>--}}
                                {{--                                                                    <input type="datetime-local" class="form-control" id="end_at"--}}
                                {{--                                                                           placeholder="Ngày kết thúc"--}}
                                {{--                                                                           name="end_at">--}}
                                {{--                                                                </div>--}}
                            </div>
                            <div class="mes-invalid mes-rangedate">
                                Ngày bắt đầu phải nhỏ hơn ngày kết thúc
                            </div>
                            <div class="form-group">
                                <label>Trạng thái</label>
                                <select class="form-control" name="self_status">
                                    <option value="1" selected>Kích hoạt</option>
                                    <option value="-1">Ẩn</option>
                                </select>
                            </div>
                            <div class="form-group info-admin-status">

                            </div>
                            <input hidden id="source-url" type="text"/>
                            <button id="btn-save" type="submit"
                                    class="btn btn-primary" name="add_banner">Đăng ký banner
                            </button>
                            <button id="btn-update" type="submit"
                                    class="btn btn-primary" name="update_banner">Cập nhật banner
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        let dt = {currentPage: 1, searchKey: '', loading: {type: 'table', colspan: 9}};
        let _serviceBanners = [];
        let chooseServiceBannerText = '';
        let cropperBanner;
        let uploaderBanner;
        let pagination, _i;
        $(document).ready(function () {
            loadBanners(dt);
            setInputDateTime('start_at');
            // setInputDateTime('end_at');
            _i = defineImage({id: 'banner'});
            let _totalFiles = 0;
            uploaderBanner = defineUploadFile({id: 'banner', containerSelector: '#img-container', maxFiles: 1})
            cropperBanner = defineCropAndUploadImage({
                type: 'banner', inputSelector: 'input[name=banner-img]', cbWhenAddFile: () => {
                    if (uploaderBanner.getTotalFile() > 0) {
                        toastInfoMessage('Cho phép chọn tối đa 1 ảnh banner');
                        cropperBanner.stopUpload();
                    }
                }
            });
            cropperBanner.saveButton.addEventListener('click', function () {
                _totalFiles = uploaderBanner.getTotalFile() + 1;
                const {fileElement} = uploaderBanner.createFileElement(cropperBanner.getImageUrl(), cropperBanner.getImgName());
                uploaderBanner.getFileUrls().push(cropperBanner.getImg())
                uploaderBanner.setTotalFile(_totalFiles);
                uploaderBanner.getContainer().append(fileElement);
                cropperBanner.$modal.modal('hide');
            });
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadBanners(dt);
                }
            });
        });

        //Clear form data input
        function clearFormData() {
            $('input[name=start_at]').prop('disabled', false);
            setInputDateTime('start_at');
            $('#end_at').replaceWith(chooseServiceBannerText);
            // setInputDateTime('end_at');
            $('input[name=name]').val('');
            $('select[name=position]').val('1');
            $('#img-container').empty();
            uploaderBanner.setTotalFile(0);
            uploaderBanner.setFileUrls([]);
            $('select[name=self_status]').val('1').attr('disabled', true);
            $('.title-banner').empty().text('Đăng ký banner');
            $('.info-admin-status').empty().append('<div class="font-weight-bolder">Sau khi đăng ký, banner cần admin đợi duyệt</div>');
        }

        $(document).on('click', '.btn-show-modal-banner', function () {
            clearFormData();
            $('.modal.add-banner').modal('show');
            $('button[name=add_banner]').show();
            $('button[name=update_banner]').hide();
        })

        //call api get product by paging and generate on interface
        function loadBanners({currentPage = 1, searchKey = '', loading = {type: 'table', colspan: 9}}) {
            $.ajax({
                url: `{{url('/shop')}}/banners?page=${currentPage}&keyword=${searchKey}`,
                loading,
                method: 'GET',
            })
                .done(res => {
                    let banners = res?.banners?.data ?? [];
                    if (banners.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadBanners(dt);
                    } else if (banners?.length === 0) {
                        generateServiceBanner(res?.service_banners);
                        $('.table-striped>tbody').empty().append(genNoContentTable(9, `Không có banner nào`));
                    } else {
                        generateBanners(banners);
                        generateServiceBanner(res?.service_banners);
                        pagination.setTotalItems(res.banners.total);
                    }
                })
                .fail(err => {
                    toastErrorMessage(err, 'Không thể load banner');
                });
        }

        $('#btn-save').on('click', function (e) {
            let dataObj = formToData('#banners-form');
            if (validateFormBanner()) {
                $.ajax({
                    url: "{{url('/shop/banners')}}",
                    method: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        ...dataObj,
                        source_urls: uploaderBanner.getMultiImg()
                    },
                })
                    .done(res => {
                        toastSuccessMessage("Thêm mới banner thành công");
                        $('.add-banner').modal('hide');
                        loadBanners({...dt, loading: {type: 'content'}});
                    })
                    .fail(err => {
                        console.log(err);
                    })
            }
        });
        //
        $('#btn-update').click(function (e) {
            let dataObj = formToData('#banners-form');
            const id = $('.modal.add-banner').data('id');
            if (validateFormBanner()) {
                $.ajax({
                    url: `{{url('/shop')}}/banners/${id}`,
                    method: 'PUT',
                    data: {
                        _token: '{{csrf_token()}}',
                        ...dataObj,
                        source_urls: uploaderBanner.getNewImgs(),
                        deleted_imageids: uploaderBanner.getDeleteImageIds(),
                        admin_status: $('.modal.add-banner').data('admin_status'),
                    },
                    dataType: 'json',
                })
                    .done(res => {
                        $(".modal.add-banner").modal('hide');
                        toastSuccessMessage("Cập nhật banner thành công");
                        loadBanners({...dt, loading: {type: 'content'}});
                    })
                    .fail(err => {
                        console.log(err);
                    })
            }
        });

        $(document).on('click', '.btn-show-modal-update', function (e) {
                $('button[name=add_banner]').hide();
                $('button[name=update_banner]').show();
                $('select[name=self_status]').attr('disabled', false);
                $('.title-banner').empty().text('Cập nhật banner');
                const bannerId = e.target.getAttribute('bannerid');
                $.ajax({
                    url: `{{url('/shop')}}/banners/${bannerId}`,
                    method: 'GET',
                }).done(res => {
                    const banner = res.banner || {};
                    $('.modal.add-banner').data('id', bannerId);
                    $('input[name=name]').val(banner.name);
                    $('select[name=position]').val(banner.position);
                    $('select[name=self_status]').val(banner.self_status);
                    let _fileUrls = [];
                    const _totalFiles = banner.source_urls.length;
                    const image_ids = banner.image_ids.split(',');
                    $('.modal.add-banner').data('imageids', image_ids);
                    $('#img-container').empty();
                    for (let i = 0; i < _totalFiles; i++) {
                        const {fileElement} = uploaderBanner.createFileElement(banner.source_urls[i], image_ids[i]);
                        _fileUrls.push({name: image_ids[i], url: banner.source_urls[i]});
                        $('#img-container').append(fileElement);
                    }
                    uploaderBanner.setTotalFile(_totalFiles);
                    uploaderBanner.setFileUrls(_fileUrls);
                    setInputDateTime('start_at', new Date(banner.start_at));
                    $('input[name=start_at]').prop('disabled', 'disabled');
                    if (isExistElement('#choose-service-banner')) {
                        $('#choose-service-banner').replaceWith(`
                                                              <div class="form-group col-sm-6" id="end_at">
                                                                    <label>Ngày kết thúc</label>
                                                                    <input disabled type="datetime-local" class="form-control" value="${moment(banner?.end_at).format('YYYY-MM-DDTHH:mm:ss')}"
                                                                           placeholder="Ngày kết thúc"
                                                                           name="end_at">
                                                              </div>
                    `);
                    } else {
                        setInputDateTime('end_at', new Date(banner.end_at));
                    }
                    let adminStatus = null;
                    $('.modal.add-banner').data('admin_status', banner.admin_status);
                    if (banner.admin_status === 1) {
                        adminStatus = `<span class="badge badge-success">Đã duyệt</span>`
                    } else if (banner.admin_status === 0) {
                        adminStatus = `<span class="badge badge-warning">Đang chờ duyệt</span>`
                    } else if (banner.admin_status === -1) {
                        adminStatus = `<span class="badge badge-danger">Từ chối</span>`
                    }
                    $('.info-admin-status').empty().append(adminStatus);
                    $('.modal.add-banner').modal('show');
                }).fail(err => {
                    toastErrorMessage('Load banner thất bại');
                });
            }
        )

        function generateBanners(banners) {
            _i.resetPhotos();
            let tbody = '';
            for (let i = 0; i < banners.length; i++) {
                const id = banners[i].id;

                let adminStatus = null;
                if (banners[i].admin_status === 1) {
                    adminStatus = `<span class="badge badge-success" bannerid=${id}>Chấp nhận</span>`
                } else if (banners[i].admin_status === 0) {
                    adminStatus = `<span class="badge badge-warning" bannerid=${id}>Đang chờ duyệt</span>`
                } else if (banners[i].admin_status === -1) {
                    adminStatus = `<span class="badge badge-danger" bannerid=${id}>Từ chối</span>`
                }
                let locationString = '';
                if (banners[i].position == 1) {
                    locationString = 'Giữa trang chủ'
                } else if (banners[i].position == 2) {
                    locationString = 'Dưới trang chủ'
                } else if (banners[i].position == 3) {
                    locationString = 'Đầu tab khám phá'
                }
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${banners[i].name}</td>
                            <td class="text-center">${_i.toHTML(banners[i].source_urls[0], banners[i].name)}</td>
                            <td class="text-center">${locationString}</td>
                            <td class="text-center">${banners[i].start_at.formatDate()}</td>
                            <td class="text-center">${toDate(banners[i].end_at)}</td>
                            <td class="text-center">
                            ${banners[i].self_status == 1 ? `<span bannerid=${id} type="button" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Nhấn để ẩn banner" class="btn-unactive badge btn-badge badge-success">Kích hoạt</span>` : `<span type="button" bannerid=${id} data-bs-toggle="tooltip" data-bs-placement="bottom" title="Nhấn để kích hoạt banner" class="btn-badge btn-active badge badge-danger">Ẩn</span>`}
                            </td>
                            <td class="text-center">${adminStatus}</td>
                            <td class="text-center">
                                <span type="button" bannerid=${id} class="fas fa-lg fa-edit text-success btn-show-update-bisto-css btn-show-modal-update" data-toggle="modal" data-target=".edit-banner"></span>
                                <span type="button" bannerid=${id} class="fa-lg btn-delete fa fa-times btn-delete-bisto-css text-danger"></span>
                            </td>
                        </tr>`;
            }
            $('.table-striped>tbody').empty().append(tbody);
            _i.animateOnloadImage({});
        }

        function generateServiceBanner(serviceBanners) {
            if (!isEmptyValue(serviceBanners)) {
                let optionText = `<option value="${serviceBanners[0].id}">Số ngày: ${serviceBanners[0].day} ,Tên: ${serviceBanners[0].name}, Phí dịch vụ: ${serviceBanners[0].price.formatVND()}</option>`;
                for (let i = 1; i < serviceBanners.length; i++) {
                    optionText += `<option value="${serviceBanners[i].id}">Số ngày: ${serviceBanners[i].day} ,Tên: ${serviceBanners[i].name}, Phí dịch vụ: ${serviceBanners[i].price.formatVND()}</option>`
                }
                $('select[name=service_banner_id]').empty().append(optionText);
            }
            if (isExistElement('#choose-service-banner'))
                chooseServiceBannerText = document.getElementById('choose-service-banner').outerHTML;
        }

        function validateFormBanner() {
            return (validateInput({
                listValidateFunction: [
                    {
                        func: requiredValidation,
                        mes: 'Tên banner là bắt buộc',
                    }
                ], propertyName: 'name'
            }) && validateInput({
                listValidateFunction: [{
                    func: requiredElementValidation,
                    mes: 'Bạn phải thêm ít nhất một ảnh',
                    params: {classNameElement: 'input[name=imageUrl]'}
                }], type: 'custom', customInput: 'input[name=imageUrl]', customMes: '.mes-imageUrl'
            }))
            // && validateRangeDate('input[name=start_at]', 'input[name=end_at]', '.mes-rangedate'))
        }


        $(document).on('click', '.btn-delete', function (e) {
            let bannerId = e.target.getAttribute('bannerid');
            confirmDelete("Bạn có chắc muốn xóa banner này không?", function () {
                let _url = PREFIX_API_SHOP + `/banners/` + bannerId
                $.ajax(
                    {
                        url: _url,
                        method: 'DELETE',
                        data: {
                            _token: "{{csrf_token()}}"
                        }
                    }
                ).done(res => {
                    toastSuccessMessage('Xóa banner thành công');
                    loadBanners({...dt, loading: {type: 'content'}});
                }).fail(err => {
                    toastErrorMessage('Xóa thất bại');
                })
            })
        });

        //unactive
        $(document).on('click', '.btn-unactive', function (e) {
                const bannerId = e.target.getAttribute('bannerid');
                $.ajax(
                    {
                        url: `{{url('/shop')}}/banners/un-active/${bannerId}`,
                        method: 'PUT',
                        data: {
                            _token: "{{csrf_token()}}",
                        }
                    }
                ).done(res => {
                    toastSuccessMessage('Ẩn banner thành công');
                    loadBanners({...dt, loading: {type: 'content'}});
                }).fail(err => {
                    toastErrorMessage('Ẩn banner thất bại')
                })
            }
        )
        //active
        $(document).on('click', '.btn-active', function (e) {
                const bannerId = e.target.getAttribute('bannerid');
                $.ajax(
                    {
                        url: `{{url('/shop')}}/banners/active/${bannerId}`,
                        method: 'PUT',
                        data: {
                            _token: "{{csrf_token()}}",
                        }
                    }
                ).done(res => {
                    toastSuccessMessage('Kích hoạt banner thành công');
                    loadBanners({...dt, loading: {type: 'content'}});
                }).fail(err => {
                    toastErrorMessage('Kích hoạt banner thất bại')
                })
            }
        )


        function searchBanner() {
            // user press enter search
            if (event.key === 'Enter') {
                dt.searchKey = $('input[name=input-text-banner]').val();
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadBanners(dt);
            }
        }
    </script>

@endsection
