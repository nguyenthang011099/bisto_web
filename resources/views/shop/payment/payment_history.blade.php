@extends('shop.layout')
@section('shop_content')
    <div id="content-page" class="content-page payment-history-shop">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Lịch sử thanh toán
                </div>
                <div style="margin-top: 20px" class="row btn-bisto row-action-bisto-css">
                    <div class="col-sm-2">
                        <select style="margin-left: 20px" class="form-control" name="status">
                            <option selected value="0">Tất cả trạng thái</option>
                            <option value="{{\App\UserTransaction::STATUS_NEW}}">Mới</option>
                            <option value="{{\App\UserTransaction::STATUS_PROCESSING}}">Đang xử lý</option>
                            <option value="{{\App\UserTransaction::STATUS_SUCCESS}}">Thành công</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control" name="provider">
                            <option selected value="0">Đơn vị thanh toán</option>
                            <option value="{{\App\UserTransaction::PROVIDER_ZALOPAY}}">Zalopay</option>
                            <option value="{{\App\UserTransaction::PROVIDER_MOMO}}">Momo</option>
                            <option value="{{\App\UserTransaction::PROVIDER_VNPAY}}">Vnpay</option>
                        </select>
                    </div>

                    <div class="col-sm-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Từ ngày</span>
                            </div>
                            <input name="from_date" type="date" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Đến ngày</span>
                            </div>
                            <input name="to_date" type="date" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="float-sm-left btn-group" style="margin-right: 20px">
                            <button id="reset-date" class="fa text-primary fa-refresh"
                                    style="border:none;border-top-left-radius: 10px;border-bottom-left-radius: 10px"></button>
                            <button class="btn btn-bisto-height-css btn-primary btn-query-bisto">Truy vấn</button>
                        </div>
                    </div>
                </div>
                <div class="iq-card-body body-bisto">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Loại giao dịch</th>
                            <th class="text-center">Số tiền</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Đơn vị thanh toán</th>
                            <th class="text-center">Thời gian</th>
                        </tr>
                        </thead>

                        <tbody class="tbody-bisto">
                        </tbody>
                    </table>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
    </div>
    <script>
        let dt = {
            currentPage: 1,
            keyword: '',
            fromDate: '',
            toDate: '',
            status: '0',
            provider: '',
            loading: {type: 'table', colspan: 6}
        };
        let _, pagination;
        $(document).ready(function () {
            _ = defineBisto({
                parentSelector: '.payment-history-shop', name: 'lịch sử giao dịch',
            });
            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadPaymentHistory(dt);
                }
            });
            loadPaymentHistory(dt);
            // _.onSearch((value) => {
            //     dt.keyword = value;
            //     dt.currentPage = 1;
            //     pagination.setCurrentPage(1);
            //     loadPaymentHistory(dt);
            // });

            _.onQuery(() => {
                const fromDate = $('input[name=from_date]').val();
                const toDate = $('input[name=to_date]').val();
                if (fromDate === '' || toDate === '') return toastInfoMessage('Bạn chưa chọn đủ ngày bắt đầu và kết thúc');
                const time1 = new Date(fromDate).getTime();
                const time2 = new Date(toDate).getTime();
                if (time1 > time2) return toastInfoMessage('Ngày bắt đầu phải nhỏ hơn ngày kết thúc');
                dt.fromDate = fromDate;
                dt.toDate = toDate;
                loadPaymentHistory(dt);
            })
        });

        $('select[name=status]').on('change', function (e) {
            dt.status = e.currentTarget.value;
            loadPaymentHistory(dt);
        });

        $('select[name=provider]').on('change', function (e) {
            dt.provider = e.currentTarget.value;
            loadPaymentHistory(dt);
        });

        $('#reset-date').on('click', function () {
            $('input[name=from_date]').val('');
            $('input[name=to_date]').val('');
            dt.fromDate = '';
            dt.toDate = '';
            loadPaymentHistory(dt);
        })

        function loadPaymentHistory({
                                        currentPage = 1,
                                        keyword = '',
                                        fromDate = '',
                                        toDate = '',
                                        status = '0',
                                        provider = '',
                                        loading = {type: 'table', colspan: 6}
                                    }) {
            $.ajax({
                url: `{{url('/shop/payment-history')}}?page=${currentPage}&keyword=${keyword}&from_date=${fromDate}&to_date=${toDate}&status=${status}&provider=${provider}`,
                method: "GET",
                loading,
            }).done(res => {
                const trans = res?.results?.data ?? [];
                if (trans?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadPaymentHistory(dt);
                } else if (trans?.length === 0) {
                    _.$tbody.empty().append(genNoContentTable(6, `Không có thanh toán nào`));
                } else {
                    generateTrans(trans);
                    pagination.setTotalItems(res.results.total);
                }
            }).fail(err => {
            });
        }

        function generateTrans(trans) {
            let tbody = '';
            for (let i = 0; i < trans.length; i++) {
                const id = trans[i].id;
                const transType = trans[i].transaction_type == '{{\App\UserTransaction::TYPE_RECHARGE}}' ? `<span class="badge badge-success">Nạp</span>` : `<span class="badge badge-danger">Rút</span>`
                let transStatus = '';
                if (trans[i].status === '{{\App\UserTransaction::STATUS_PROCESSING}}') {
                    transStatus = `<span class="badge badge-warning">Đang xử lý</span>`
                } else if (trans[i].status === '{{\App\UserTransaction::STATUS_SUCCESS}}') {
                    transStatus = `<span class="badge badge-success">Thành công</span>`
                } else if (trans[i].status === '{{\App\UserTransaction::STATUS_FAIL}}') {
                    transStatus = `<span class="badge badge-danger">Lỗi</span>`
                } else if (trans[i].status === '{{\App\UserTransaction::STATUS_NEW}}') {
                    transStatus = `<span class="badge badge-secondary">Mới</span>`
                }
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${transType}</td>
                            <td class="text-center">${trans[i].transaction_amount.formatVND()}</td>
                            <td class="text-center">${transStatus}</td>
                            <td class="text-center">${trans[i].provider_type}</td>
                            <td class="text-center">${trans[i].created_at.formatDate()}</td>
                        </tr>`;
            }
            _.$tbody.empty().append(tbody);
        }
    </script>
@endsection

