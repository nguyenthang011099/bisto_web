@extends('shop.layout')
@section('shop_content')
    <?php
    $vnp_HashSecret = config('payment.vnpay.hash_secret', '');
    $vnp_SecureHash = $_GET['vnp_SecureHash'];
    $inputData = array();
    foreach ($_GET as $key => $value) {
        if (substr($key, 0, 4) == "vnp_") {
            $inputData[$key] = $value;
        }
    }
    unset($inputData['vnp_SecureHashType']);
    unset($inputData['vnp_SecureHash']);
    ksort($inputData);
    $i = 0;
    $hashData = "";
    foreach ($inputData as $key => $value) {
        if ($i == 1) {
            $hashData = $hashData . '&' . $key . "=" . $value;
        } else {
            $hashData = $hashData . $key . "=" . $value;
            $i = 1;
        }
    }
    $date_time = '';
    //$secureHash = md5($vnp_HashSecret . $hashData);
    $secureHash = hash('sha256', $vnp_HashSecret . $hashData);
    if ($secureHash == $vnp_SecureHash) {
        if ($_GET['vnp_ResponseCode'] == '00') {
            $order_id = $_GET['vnp_TxnRef'];
            $money = $_GET['vnp_Amount'] / 100;
            $note = $_GET['vnp_OrderInfo'];
            $vnp_response_code = $_GET['vnp_ResponseCode'];
            $code_vnpay = $_GET['vnp_TransactionNo'];
            $code_bank = $_GET['vnp_BankCode'];
            $time = $_GET['vnp_PayDate'];
            $date_time = substr($time, 0, 4) . '-' . substr($time, 4, 2) . '-' . substr($time, 6, 2) . ' ' . substr($time, 8, 2) . ':' . substr($time, 10, 2) . ':' . substr($time, 12, 2);
            $status = 'success';
        } else {
            $status = 'error';
        }
    } else {
        $status = 'invalid';
    }
    ?>
    <div id="content-page" class="content-page d-flex justify-content-between">
        <div class="col-sm-2 col-lg-2"></div>
        <div class="col-sm-4 col-lg-8">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Thông tin đơn hàng
                </div>
                <div class="iq-card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered mt-4">
                            <tr>
                                <td>Mã đơn hàng</td>
                                <td><?php echo $_GET['vnp_TxnRef'] ?></td>
                            </tr>
                            <tr>
                                <td>Số tiền</td>
                                <td><?=number_format($_GET['vnp_Amount'] / 100) ?> VNĐ</td>
                            </tr>
                            <tr>
                                <td>Nội dung thanh toán</td>
                                <td><?php echo $_GET['vnp_OrderInfo'] ?></td>
                            </tr>
                            <tr>
                                <td>Mã GD Tại VNPAY</td>
                                <td><?php echo $_GET['vnp_TransactionNo'] ?></td>
                            </tr>
                            <tr>
                                <td>Mã Ngân hàng</td>
                                <td><?php echo $_GET['vnp_BankCode'] ?></td>
                            </tr>
                            <tr>
                                <td>Thời gian thanh toán</td>
                                <td><?php echo $date_time ?></td>
                            </tr>
                            <tr>
                                <td>Kết quả</td>
                                <td>
                                    @if ($status == 'success')
                                        <span style="color:#44ab6c">Giao dịch thành công</span>
                                    @elseif($status == 'error')
                                        <span style="color:red">Giao dịch thất bại</span>
                                    @else
                                        <span style="color:red">Chữ ký không hợp lệ</span>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-2 col-lg-2"></div>
    </div>
@endsection
