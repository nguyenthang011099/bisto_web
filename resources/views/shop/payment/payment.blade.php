@extends('shop.layout')
@section('shop_content')
    <div id="content-page" class="content-page d-flex justify-content-between">
        <div class="col-sm-2 col-lg-2"></div>
        <div class="col-sm-4 col-lg-8">
            <div class="iq-card">
                <div class="panel-heading">
                    Thông tin thanh toán
                </div>
                <div class="iq-card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered mt-4">
                            <tr>
                                <td>Tên công ty</td>
                                <td>Công ty TNHH Anberry Việt Nam</td>
                            </tr>
                            <tr>
                                <td>Loại ngân hàng</td>
                                <td>Ngân Hàng TMCP Á Châu ACB</td>
                            </tr>
                            <tr>
                                <td>Tên tài khoản nhận</td>
                                <td>Công ty TNHH Anberry Việt Nam</td>
                            </tr>
                            <tr>
                                <td>Số tài khoản nhận</td>
                                <td>9119 6868 6868</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div>
                    <button style="margin: 20px" class="btn btn-primary on-payment">Thanh toán</button>
                </div>
            </div>
        </div>
        <div class="col-sm-2 col-lg-2"></div>
    </div>
    <script>
        let PM;
        $(document).ready(function () {
            PM = definePayment({type: 'shop'});
        });
        $('button.on-payment').on('click', function () {
            PM.$modalPayment.modal('show');
        });
    </script>
@endsection

