<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Xuất hóa đơn</title>
    <link rel="shortcut icon" href="{{asset('backend/images/Logo.png')}}"/>
    <link rel="stylesheet" href="{{public_path('backend/css2/bootstrap.min.css')}}"/>
    <style>
        body {
            font-family: DejaVu Sans, serif;
            font-size: 14px;
        }

        .super-center {
            display: grid !important;
            place-items: center !important;
        }


        .page-break {
            page-break-after: always;
        }

        .w-100 {
            min-width: 100%;
        }

        .w-50 {
            min-width: 50%;
        }
    </style>

</head>

<body>
<div class="container-fluid">
    <div class="page-1">
        <div>
            <table class="w-100">
                <tr>
                    <td class="w-50">
                        <div class="text-nowrap">
                            <img class="img-fluid" style="width: 1rem;height: 1rem" alt=""
                                 src="{{public_path('backend/images/Logo.png')}}"/> Mua sắm thời trang online
                        </div>
                        <div class="text-nowrap">Bisto.vn</div>
                    </td>
                    <td class="w-50">
                        <div class="text-nowrap"> Mã đơn hàng: {{ $order->code }}</div>
                        <div class="text-nowrap"> Ngày giờ đặt hàng: {{$order->created_at}}</div>
                    </td>
                </tr>
            </table>
        </div>

        <div style="text-align: center; font-size: 22px;font-weight: bolder" class="w-100">THÔNG TIN ĐƠN HÀNG</div>
        <div style="text-align: center; font-size: 12px;font-style: italic" class="w-100">(Liên 1 lưu tại cửa hàng)
        </div>

        <div>
            <table class="w-100">
                <tr>
                    <td>
                        <p>Thông tin cửa hàng: </p>
                        <p>Tên: {{ $seller->fullname }}</p>
                        <p>SĐT: {{ $seller->phone }}</p>
                        <p>Đ/c: {{ $seller->address }}</p>
                    </td>
                    <td>
                        <p>Thông tin người nhận: </p>
                        <p>Tên: {{ $buyer->fullname }}</p>
                        <p>SĐT: {{ $buyer->phone }}</p>
                        <p>Đ/c: {{ $buyer->address }}</p>
                    </td>
                </tr>
            </table>
        </div>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center">STT</th>
                <th class="text-center">Sản phẩm</th>
                <th class="text-center">Số lượng</th>
                <th class="text-center">Đơn giá</th>
                <th class="text-center">Thành tiền</th>
            </tr>
            </thead>

            <tbody>
            @foreach($products as $key => $product)
                <tr>
                    <td class="text-center">{{ $key + 1 }}</td>
                    <td class="text-center">{{ $product->name }}</td>
                    <td class="text-center">{{ $product->quantity }}</td>
                    <td class="text-center">@formatCurrency($product->price) VNĐ</td>
                    <td class="text-center">@formatCurrency($product->price * $product->quantity) VNĐ</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div style="text-align:left">
            <p>Đơn vị vận chuyển: Lalamove</p>
        </div>

        @if ($order->payment_method === 1)
            <p>Hình thức thanh toán: Đã thanh toán online</p>
        @else
            <p>Hình thức thanh toán: Tiền mặt</p>
            Tiền hàng: @formatCurrency($order->total_money) VNĐ<br/>
            Cước vận chuyển: @formatCurrency($order->shipping_fee) VNĐ<br/>
            Tổng cộng: @formatCurrency($order->total_order) VNĐ<br/><br/>
        @endif
        <div>
            <table class="w-100">
                <tr>
                    <td class="w-50">
                        Cửa hàng ký nhận
                    </td>
                    <td class="w-50">
                        Đơn vị giao hàng ký xác nhận
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="page-break"></div>
    <div class="page-2">
        <div>
            <table class="w-100">
                <tr>
                    <td class="w-50">
                        <div class="text-nowrap"><img class="img-fluid" style="width: 1rem;height: 1rem" alt=""
                                                      src="{{public_path('backend/images/Logo.png')}}"/> Mua sắm thời
                            trang
                            online
                        </div>
                        <div class="text-nowrap">Bisto.vn</div>
                    </td>
                    <td class="w-50">
                        <div class="text-nowrap"> ID đơn hàng: {{ $order->code }}</div>
                        <div class="text-nowrap"> Ngày giờ đặt hàng: {{$order->created_at}}</div>
                    </td>
                </tr>
            </table>
        </div>

        <div style="text-align: center; font-size: 22px;font-weight: bolder" class="w-100">THÔNG TIN ĐƠN HÀNG</div>
        <div style="text-align: center; font-size: 12px;font-style: italic" class="w-100">(Liên 2 giao người mua)
        </div>

        <div>
            <table class="w-100">
                <tr>
                    <td>
                        <p>Thông tin cửa hàng: </p>
                        <p>Tên: {{ $seller->fullname }}</p>
                        <p>SĐT: {{ $seller->phone }}</p>
                        <p>Đ/c: {{ $seller->address }}</p>
                    </td>
                    <td>
                        <p>Thông tin người nhận: </p>
                        <p>Tên: {{ $buyer->fullname }}</p>
                        <p>SĐT: {{ $buyer->phone }}</p>
                        <p>Đ/c: {{ $buyer->address }}</p>
                    </td>
                </tr>
            </table>
        </div>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="text-center">STT</th>
                <th class="text-center">Sản phẩm</th>
                <th class="text-center">Số lượng</th>
                <th class="text-center">Đơn giá</th>
                <th class="text-center">Thành tiền</th>
            </tr>
            </thead>

            <tbody>
            @foreach($products as $key => $product)
                <tr>
                    <td class="text-center">{{ $key + 1 }}</td>
                    <td class="text-center">{{ $product->name }}</td>
                    <td class="text-center">{{ $product->quantity }}</td>
                    <td class="text-center">@formatCurrency($product->price) VNĐ</td>
                    <td class="text-center">@formatCurrency($product->price * $product->quantity) VNĐ</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div style="text-align:left">
            <p>Đơn vị vận chuyển: Lalamove</p>
        </div>

        @if ($order->payment_method === 1)
            <p>Hình thức thanh toán: Đã thanh toán online</p>
        @else
            <p>Hình thức thanh toán: Tiền mặt</p>
            Tiền hàng: @formatCurrency($order->total_money) VND<br/>
            Cước vận chuyển: @formatCurrency($order->shipping_fee) VND<br/>
            Tổng cộng: @formatCurrency($order->total_order) VND<br/><br/>
        @endif
        <div>
            <table class="w-100">
                <tr>
                    <td class="w-50">
                        Cửa hàng ký nhận
                    </td>
                    <td class="w-50">
                        Đơn vị giao hàng ký xác nhận
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>

</html>
