@extends('shop.layout')
@section('shop_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div class="iq-card">
                <section class="panel">
                    <header class="panel-heading">
                        Quản lý giảm giá sản phẩm
                    </header>
                    <div class="panel-body">
                        <div class="position-center">
                            <div>
                                <div class="iq-card-body pb-0">
                                    <table class="table table-striped b-t b-light">
                                        <thead>
                                        <th class="text-center">Loại giảm giá</th>
                                        <th class="text-center">Số % giảm</th>
                                        <th class="text-center">Từ ngày</th>
                                        <th class="text-center">Đến ngày</th>
                                        </thead>
                                        <tr>
                                            <td class="text-center">Giảm theo %</td>
                                            <td class="text-center super-center"><input class="form-control w-25"
                                                                                        name="discount"
                                                                                        type="number" value="0">
                                            </td>
                                            <td class="text-center"><input name="start_at" class="form-control"
                                                                           type="datetime-local"></td>
                                            <td class="text-center"><input name="end_at" class="form-control"
                                                                           type="datetime-local"></td>
                                        </tr>
                                    </table>
                                    <div class="row">
                                        <div class="col-sm-6 mes-discount mes-v-visibility">
                                            Phần trăm giảm trong khoảng từ 0 tới 100
                                        </div>
                                        <div class="col-sm-6 mes-rangedate mes-v-display">
                                            Ngày bắt đầu nhỏ hơn ngày kết thúc
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 d-flex align-items-center justify-content-between">
                                    <h4 class="selected-product-discount" style="margin-left: 20px">Đã
                                        chọn 0 sản phẩm</h4><span type="button"
                                                                  class="fa fa-times
                                                                    fa-lg delete-all-product-discount"></span>
                                </div>
                                <div class="col-sm-5"></div>
                                <div class="input-group col-sm-4">
                                    <input type="search" placeholder="&#xf002 Tìm kiếm" onkeydown="searchProduct()"
                                           class="input-sm form-control btn-search-bisto-css" name="keywords_submit">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="iq-card-body">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <ul class="list-group overflow-auto l-pr-dis border" style="height: 20rem">
                                            </ul>
                                            <div class="super-center mt-2">
                                                <button id="apply" class="btn btn-primary">Áp dụng</button>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">STT</th>
                                                    <th class="text-center">Tên sản phẩm</th>
                                                    <th class="text-center">Ảnh</th>
                                                    <th class="text-center">Giá gốc</th>
                                                    <th class="text-center">Khuyến mãi</th>
                                                    <th class="text-center">Thao tác</th>
                                                </tr>
                                                </thead>
                                                <tbody class="list-product">
                                                </tbody>
                                            </table>
                                            <!-- PAGINATION START-->
                                            <div class="pagination-custom">
                                                <div style="" id="pagination-container"></div>
                                            </div>
                                            <!-- PAGINATION END-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- select all checkobx -->
    <script>
        let dt = {currentPage: 1, searchKey: '', loading: {type: 'table', colspan: 6, selector: '.list-product'}}
        let productIds = [], pagination, _i;
        $(document).ready(function () {
            loadProducts(dt);
            _i = defineImage({id: 'discount-product'});
            setInputDateTime('start_at');
            setInputDateTime('end_at');
            validateRangeNumber('input[name=discount]', 0, 100);
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadProducts(dt);
                }
            });
        })

        function validateOnApply() {
            return validateInput({
                listValidateFunction: [{
                    func: rangeValidation,
                    mes: 'Phần trăm giảm trong khoảng 0 tới 100',
                    params: {
                        min: 0,
                        max: 100,
                    }
                }], propertyName: 'discount', cssClassMes: 'visibility'
            }) && validateRangeDate('input[name=start_at]', 'input[name=end_at]', 'div.mes-rangedate');
        }

        //when user apply
        $(document).on('click', '#apply', function () {
            if (productIds.length === 0) return toastInfoMessage('Bạn chưa chọn sản phẩm nào');
            if (validateOnApply()) {
                const start_at = new Date($('input[name=start_at]').val()).getTime() / 1000 || null;
                const end_at = new Date($('input[name=end_at]').val()).getTime() / 1000 || null;
                $.ajax({
                    url: '{{url('/shop/discount')}}',
                    method: 'PUT',
                    data: {
                        _token: '{{csrf_token()}}',
                        product_ids: productIds,
                        start_at,
                        end_at,
                        discount: $('input[name=discount]').val()
                    }
                }).done(res => {
                    toastSuccessMessage('Giảm giá các sản phẩm thành công');
                    loadProducts(dt);
                }).fail(err => {
                })
            }
        });

        //add product to list discount
        $(document).on('click', '.add-product-discount', function () {
            const productId = parseInt($(this).attr('productid'));
            if (productIds.includes(productId)) {
                return toastInfoMessage('Sản phẩm đã được thêm vào danh sách giảm giá');
            }
            const productName = $(this).attr('productname');
            productIds.push(productId);
            const productDiscountItemElement = `<li class="border-left-0 border-right-0 d-flex justify-content-between list-group-item text-wrap"
                                                    type="button">${productName}<i productid=${productId} class="super-center delete-product-discount fa fa-times"
                                                        aria-hidden="true"></i></li>`;
            $('.l-pr-dis').append(productDiscountItemElement);
            $('.selected-product-discount').text(`Đã chọn ${productIds.length} sản phẩm`);
        })

        //remove product from list discount
        $(document).on('click', '.delete-product-discount', function () {
            const productId = parseInt($(this).attr('productid'));
            productIds.splice(productIds.indexOf(productId), 1);
            $(this).parent().remove();
            $('.selected-product-discount').text(`Đã chọn ${productIds.length} sản phẩm`);
        });

        //remove all product from list discount
        $(document).on('click', '.delete-all-product-discount', function () {
            productIds = [];
            $(".l-pr-dis").empty();
            $('.selected-product-discount').text(`Đã chọn 0 sản phẩm`);
        });

        //products to interface
        function generateProducts(products, search = false) {
            _i.resetPhotos();
            let tbody = '';
            for (let i = 0; i < products.length; i++) {
                let id = products[i].id;
                const img = search ? products[i].image_urls[0] : products[i].image;
                tbody += `<tr>
			    <td class="text-center">${i + 1}</td>
				<td class="text-center">${products[i].name}</td>
		        <td class="text-center">
                    ${_i.toHTML(img, products[i].name)}
                </td>
				<td class="text-center">${products[i].price.formatVND()}</td>
				<td class="text-center">${products[i].discount} %</td>
				<td class="text-center"><button productid=${id} productname="${products[i].name}" type="button" class="btn btn-primary add-product-discount">Chọn</button></td>
			</tr>`;
            }
            $('tbody.list-product').empty().append(tbody);
            _i.animateOnloadImage({});
        }

        //call api get product by paging and generate on interface
        function loadProducts({
                                  currentPage = 1,
                                  searchKey = '',
                                  loading = {type: 'table', colspan: 6, selector: '.list-product'}
                              }) {
            let _url = `products?page=${currentPage}&keyword=${searchKey}`;
            $.ajax({
                url: _url,
                method: 'GET', loading
            })
                .done(res => {
                    const products = res.products.data;
                    if (products.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadProducts(dt);
                    } else if (products.length === 0) {
                        $('.list-product').empty().append(genNoContentTable(6, `Không có sản phẩm nào`));
                    } else {
                        $('#total-product').text(res.products.total);
                        generateProducts(products);
                        pagination.setTotalItems(res.products.total);
                    }
                })
                .fail(err => {
                });
        }

        //handle event when user typing enter on input search
        function searchProduct(e) {
            // user press enter search
            if (event.key === 'Enter') {
                dt.searchKey = $('input[name=keywords_submit]').val();
                loadProducts(dt);
            }
        }
    </script>
@endsection
