@extends('shop.layout')
@section('shop_content')
    <div id="content-page" class="content-page service-brand-shop">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            @if($is_shop_without_branch == '1')
                <div class="iq-card">
                    <div class="panel-heading">
                        Dịch vụ hiện tại đang áp dụng
                    </div>
                    <div class="iq-card-body body-bisto">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">Ngày bắt đầu</th>
                                <th class="text-center">Ngày kết thúc</th>
                            </tr>
                            </thead>
                            <tbody id="current-brand">
                            <tr>
                                <td class="text-center" colspan="2">Chưa có dịch vụ nào được áp dụng</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="panel-heading">
                        Danh sách dịch vụ brand
                    </div>
                    <div class="iq-card-body body-bisto">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">STT</th>
                                <th class="text-center">Số ngày áp dụng</th>
                                <th class="text-center">Tên chiến dịch</th>
                                <th class="text-center">Phí áp dụng</th>
                                <th class="text-center">Thao tác</th>
                            </tr>
                            </thead>

                            <tbody class="tbody-bisto">
                            </tbody>
                        </table>
                    </div>

                    <!-- PAGINATION START-->
                    <div class="pagination-custom">
                        <div id="pagination-container"></div>
                    </div>
                    <!-- PAGINATION END-->
                </div>
            @elseif($is_shop_without_branch == '0')
                <div class="iq-card">
                    <div class="panel-heading">
                        <h5>Liên hệ quản lý chuỗi cửa hàng để đăng ký thương hiệu</h5>
                    </div>
                </div>
            @endif
            <div id="modal-container">
                <div class="modal fade modal-bisto" tabindex="-1" role="dialog"
                     aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content" style="border-radius: 20px">
                            <div class="iq-card">
                                <header class="panel-heading title-modal-bisto">
                                    Đăng ký thương hiệu
                                </header>
                                <section class="panel">
                                    <div class="panel-body">
                                        <div class="iq-card-body">
                                            <div class="form-bisto" role="form">
                                                @csrf
                                                <div class="form-group">
                                                    <label class="required-input">Chọn ngày bắt đầu</label>
                                                    <input type="datetime-local" name="start_at" class="form-control">
                                                    <div class="mes-start_at mes-v-display">
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <button class="btn btn-primary btn-update-bisto btn-update-bisto-css">
                                                    Đăng ký
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script>
        let dt = {currentPage: 1, keyword: '', loading: {type: 'table', colspan: 5, selector: '.tbody-bisto'}};
        let _, pagination;

        $(document).ready(function () {
            _ = defineBisto({
                parentSelector: '.service-brand-shop',
            });

            pagination = definePagination({
                onPageClick: (pageNumber) => {
                    dt.currentPage = pageNumber;
                    loadBrandService(dt);
                }
            });
            loadBrandService(dt);

            _.onSearch((value) => {
                dt.keyword = value;
                dt.currentPage = 1;
                pagination.setCurrentPage(1);
                loadBrandService(dt);
            });

            _.onShowUpdate((id) => {
                dataToForm({
                    fieldInputs: [
                        {selector: 'input[name=start_at]'},
                    ],
                    parentSelector: '.form-bisto', action: 'clear-validate'
                })
            })

            _.onUpdate((id, data) => {
                $.ajax({
                    url: `{{url('/shop/service-brands')}}`,
                    method: 'POST',
                    data: {
                        _token: '{{csrf_token()}}',
                        service_brand_id: id,
                        ...data
                    }
                }).done(res => {
                    toastSuccess(res, 'Đăng ký thành công');
                    window.location.reload();
                }).fail(err => {

                })
            })
        });

        function loadBrandService({
                                      currentPage = 1,
                                      keyword = '',
                                      loading = {type: 'table', colspan: 5, selector: '.tbody-bisto'}
                                  }) {
            $.ajax({
                url: `{{url('/shop/service-brands')}}?page=${currentPage}&keyword=${keyword}`,
                method: "GET",
                loading,
            }).done(res => {
                const services = res?.results?.data ?? [];
                if (services?.length === 0 && dt.currentPage > 1) {
                    dt.currentPage -= 1;
                    pagination.setCurrentPage(1);
                    loadBrandService(dt);
                } else if (services?.length === 0) {
                    _.$tbody.empty().append(genNoContentTable(5, `Không có gói dịch vụ nào`));
                } else {
                    generateServices(services, res?.current_brand);
                    pagination.setTotalItems(res.results.total);
                }
            }).fail(err => {
            });
        }

        function generateServices(services, currentBrand) {
            let tbody = '';
            let btn = '';
            if (!isEmptyValue(currentBrand)) {
                $('#current-brand').empty().append(
                    `<tr>
                            <td class="text-center">${currentBrand.start_at.formatDate()}</td>
                            <td class="text-center">${currentBrand.end_at.formatDate()}</td>
                     </tr>`
                );
            }
            for (let i = 0; i < services.length; i++) {
                if (isEmptyValue(currentBrand)) {
                    btn = `<button data-id="${services[i].id}" class="btn btn-primary btn-register-brand btn-show-update-bisto">Đăng ký</button>`
                }
                const id = services[i].id;
                tbody += `<tr>
                            <td class="text-center">${i + 1}</td>
                            <td class="text-center">${services[i].day ?? 0} ngày</td>
                            <td class="text-center">${services[i]?.name ?? ''}</td>
                            <td class="text-center">${toVND(services[i].price)}</td>
                            <td class="text-center">
                               ${btn}
                            </td>
                        </tr>`;
            }
            _.$tbody.empty().append(tbody);
        }
    </script>
@endsection
