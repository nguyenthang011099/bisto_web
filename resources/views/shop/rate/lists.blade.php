@extends('shop.layout')
@section('shop_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <header class="panel-heading">
                    Quản lý đánh giá sản phẩm
                </header>
                <div class="row w3-res-tb row-action-bisto-css">
                    <div class="col-sm-5 m-b-xs"></div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search" placeholder="&#xf002 Tìm kiếm"
                                   class="btn-search-bisto-css input-sm form-control" name="keywords_submit">
                        </div>
                    </div>
                </div>
                <div class="container px-lg-4">
                    <div class="rate-container row mx-lg-n3 row-cols-6">

                    </div>
                </div>

                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
    </div>
    <script>
        let dt = {
                currentPage: 1,
                searchKey: '',
                loading: {
                    type: 'content'
                }
            },
            pagination;
        $(document).ready(function () {
            loadRates(dt);
            pagination = definePagination({
                onPageClick: (pageNumber, event) => {
                    dt.currentPage = pageNumber;
                    loadRates(dt);
                }
            });
        });

        function generateRates(rates) {
            let div = ''
            for (let i = 0; i < rates.length; i++) {
                const percent = rates[i].rate / 5 * 100;
                div += `
                    <div class="col p-2">
                        <div class="bubble h-100">
                            <a href="${PREFIX_API_SHOP}/view-rate-product/${rates[i].product_id}">
                                <div class="img-cover img-width-higher">
                                    <img src="${rates[i].url}" alt="">
                                </div>
                                <div><span class="name-pr font-weight-bold">${rates[i].name}</span></div>
                                <div class="ratings">
                                    <div class="empty-stars"></div>
                                    <div class="full-stars" style="width:${percent}%"></div>
                                </div>
                                <div>Số bình luận: ${rates[i].comment} <i class="fa fa-comment" aria-hidden="true"></i></div>
                                <div>Số đánh giá: ${rates[i].number_of_rates} <i class="text-warning fa fa-star" aria-hidden="true"></i></div>
                            </a>
                        </div>
                    </div>`;
            }
            $('.rate-container').empty().append(div);
        }

        function loadRates({
                               currentPage = 1,
                               searchKey = '',
                               loading = {
                                   type: 'content'
                               }
                           }) {
            const _url = `products/ratings?page=${currentPage}&keyword=${searchKey}`;
            $.ajax({
                url: _url,
                method: 'GET',
                loading,
            })
                .done(res => {
                    const rates = res?.results?.data ?? [];
                    if (rates.length === 0 && dt.currentPage > 1) {
                        dt.currentPage -= 1;
                        pagination.setCurrentPage(1);
                        loadRates(dt);
                    } else if (rates.length === 0) {
                        toastInfoMessage('Không có đánh giá nào')
                        generateRates([]);
                    } else {
                        generateRates(rates);
                        pagination.setTotalItems(res.results.total);
                    }
                })
                .fail(err => {
                });
        }

        $(document).on('keydown', 'input[name=keywords_submit]', function (e) {
            if (event.key === 'Enter') {
                dt.searchKey = $(this).val();
                loadRates(dt);
            }
        })
    </script>
@endsection
