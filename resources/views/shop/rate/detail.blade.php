@extends('shop.layout')
@section('shop_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Thông tin sản phẩm
                    </div>
                    <div class="iq-card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr class="text-center">
                                    <td>Hình ảnh</td>
                                    <td>Thông tin sản phẩm</td>
                                    <td>Mô tả sản phẩm</td>
                                </tr>
                                </thead>
                                <tbody class="product-info">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Đánh giá sản phẩm
                    </div>
                    <div class="iq-card-body">
                        <div class="filter-rate"></div>
                        <div class="container-rate-comment">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-container">

        </div>
    </div>
    <script>
        (function () {
            const productId = {{$product_id}};
            let dt = {currentPage: 1, searchKey: '', loading: {type: 'content'}}, uploaderInstances = {};
            $(document).ready(function () {
                loadComments(dt);
            });

            function generateProductInfo(product) {
                let tbody = `<tr>
                            <td class="td-img-square">
                                <div><img src="${product.url[0]}" alt="bisto"></div>
                            </td>
                            <td class="">
                                <dl class="row m-1">
                                  <dt class="col-sm-3">Tên</dt>
                                  <dd class="col-sm-9">${product.name}</dd>
                                  <dt class="col-sm-3">Giá</dt>
                                  <dd class="col-sm-9">${product.price.formatVND()}</dd>
                                  <dt class="col-sm-3">Giảm giá</dt>
                                  <dd class="col-sm-9">${product.discount} % từ ${product.start_at.formatDate()} đến ${product.end_at.formatDate()}</dd>
                                </dl>
                            </td>
                            <td>
                                ${product.description || ''}
                            </td>
                        </tr>`;
                $('tbody.product-info').empty().append(tbody);
            }


            function getRateElement(rateComment, isShow = '') {
                let childText = '';
                for (const child of rateComment.child) {
                    childText += `
                                                    <div class="row item-comment">
                                                        <div class="col-sm-1 avatar-rate d-flex justify-content-center">
                                                            <img src="${child.avatar}"
                                                                class="img-fluid rounded-circle" alt=""/>
                                                        </div>
                                                        <div class="col-sm-11">
                                                            <div class="user-name font-weight-bold font-size-18">${child.full_name}</div>
                                                            <div class="font-size-16">${child.comment ? child.comment : ''}</div>
                                                            ${child.rate_url ? `<div class="">
                                                                <img src="${child.rate_url}" style="height: 7rem;" class="img-fluid mr-1" alt="...">
                                                            </div>` : ''}
                                                            <div class="">
                                                                <span>${child.created_at.formatDate()}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                        `
                }
                return `
                          <div class="item-rate item-rate-${rateComment.id}">
                                    <div class="row item-rate-comment mb-2">
                                        <div class="col-sm-1 avatar-rate d-flex justify-content-center">
                                            <img src="${rateComment.avatar}"
                                                class="img-fluid rounded-circle" alt="...">
                                        </div>
                                        <div class="col-sm-11">
                                            <div class="user-name font-weight-bold font-size-18">${rateComment.full_name}</div>
                                            <div class="ratings">
                                                <div class="empty-stars"></div>
                                                <div class="full-stars" style="width:${rateComment.rate / 5 * 100}%"></div>
                                            </div>
                                            <div class="font-size-16">${rateComment.comment ? rateComment.comment : ''}</div>

                                            ${rateComment.rate_url ? `<div class="rate-url">
                                                <img src="${rateComment.rate_url}"
                                                    style="height: 7rem;" class="img-fluid mr-1" alt="bisto"/>
                                            </div>` : ''}
                                            <div class="">
                                                <span>${rateComment.created_at.formatDate()}</span>
                                                <span class="text-primary reply-comment" data-toggle="collapse"
                                                    data-target=".container-child-comment-${rateComment.id}" type="button">Trả lời</span>
                                            </div>
                                            <div class="collapse container-child-comment-${rateComment.id} ${isShow}">
                                                <div class="card card-body">
                                                   ${childText}
                                                  <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <button class="btn btn-outline-primary" type="button">
                                                            <label for="file-${rateComment.id}">
                                                                <i class="fa fa-paperclip fa-lg"></i>
                                                            </label>
                                                            <input id="file-${rateComment.id}" name="file-${rateComment.id}" type="file"
                                                                hidden class="input-send-image-${rateComment.id}" />
                                                        </button>
                                                    </div>
                                                    <input type="text" name="comment" class="form-control input-send-text input-send-text-${rateComment.id}" data-id=${rateComment.id}
                                                        aria-label="" aria-describedby="basic-addon1">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-primary btn-send-comment" data-id="${rateComment.id}" type="button">Gửi</button>
                                                    </div>
                                                  </div>
                                                  <div class="form-group" style="overflow: auto">
                                                        <div id="img-container-${rateComment.id}" class="row align-items-start">
                                                  </div>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    `;
            }

            function generateRateComment(rateComments) {
                let rc = '';
                for (const rateComment of rateComments) {
                    rc += getRateElement(rateComment);
                }
                $('.container-rate-comment').empty().append(rc);
                for (const rateComment of rateComments) {
                    uploaderInstances[rateComment.id] = defineUploadFile({
                        inputSelector: `.input-send-image-${rateComment.id}`,
                        containerSelector: `#img-container-${rateComment.id}`, imageName: 'source_url', maxFiles: 1
                    });
                    const $collapse = $(`.container-child-comment-${rateComment.id}`);
                    $collapse.on('shown.bs.collapse', function () {
                        document.querySelector(`.input-send-text-${rateComment.id}`).focus()
                    })
                }
            }

            function generateReloadRateComment(rateComment) {
                let rc = '';
                rc = getRateElement(rateComment, 'show');
                $(`.item-rate-${rateComment.id}`).replaceWith(rc);
                uploaderInstances[rateComment.id] = defineUploadFile({
                    inputSelector: `.input-send-image-${rateComment.id}`,
                    containerSelector: `#img-container-${rateComment.id}`, imageName: 'source_url', maxFiles: 1
                })
                const $collapse = $(`.container-child-comment-${rateComment.id}`);

                $collapse.on('shown.bs.collapse', function () {
                    document.querySelector(`.input-send-text-${rateComment.id}`).focus()
                })

            }

            function loadComments({currentPage = 1, searchKey = '', loading = {type: 'content'}, idReload}) {
                const _url = `${PREFIX_API_SHOP}/view/ratings/${productId}`;
                $.ajax({
                    url: _url,
                    method: 'GET',
                    loading,
                })
                    .done(res => {
                        generateProductInfo(res.product);
                        if (idReload) {
                            generateReloadRateComment(res.results.find(item => item.id === idReload));
                        } else generateRateComment(res.results);
                    })
                    .fail(err => {
                    });
            }


            $(document).on('click', '.btn-send-comment', function () {
                sendComment(this.getAttribute('data-id'));
            })

            function sendComment(id) {
                const cmt = formToData(`.item-rate-${id}`)?.comment;
                if (isEmptyValue(cmt)) return toastInfoMessage('Bình luận không được để trống');
                $.ajax({
                    url: `{{url('/shop')}}/products/ratings`,
                    method: 'POST',
                    data: {
                        _token: "{{csrf_token()}}",
                        ...formToData(`.item-rate-${id}`),
                        parent_id: id,
                        image_file: uploaderInstances[id].getOneImg(),
                    }
                }).done(res => {
                    toastSuccess(res, 'Trả lời thành công');
                    loadComments({...dt, idReload: parseInt(id)});
                }).fail(err => {

                })
            }

            $(document).on('keydown', '.input-send-text', function (e) {
                if (e.keyCode === 13) {
                    sendComment(e.target.getAttribute('data-id'));
                }
            })
        })()
    </script>
@endsection
