@extends('shop.layout')
@section('shop_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách mã giảm giá
                </div>
                <div class="row row-action-bisto-css">
                    <div class="col-sm-5">
                        <span
                            class="btn show-modal-add-voucher btn-primary btn-show-add-bisto-css">+ Thêm
                            mã giảm giá</span>
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search"
                                   onkeydown="onSearchVoucher(this)"
                                   placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm form-control btn-search-bisto-css"
                                   name="keywords_submit">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">Tên mã giảm</th>
                                <th class="text-center">Mã code</th>
                                <th class="text-center">Điều kiện</th>
                                <th class="text-center">Đơn tối thiểu</th>
                                <th class="text-center">Tiền giảm tối đa</th>
                                <th class="text-center">Số lượng voucher</th>
                                <th class="text-center">Bắt đầu</th>
                                <th class="text-center">Kết thúc</th>
                                <th class="text-center">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <!-- ADD VOUCHER -->
        <div class="modal fade add-voucher" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style="border-radius: 20px">
                    <div class="iq-card">
                        <section class="panel">
                            <header class="panel-heading">
                                Thêm mã giảm giá
                            </header>
                            <div class="iq-card-body">
                                <div class="position-center f-add-voucher">
                                    <div class="form-group">
                                        <label>Tên mã giảm<span class="text-danger">*</span></label>
                                        <input type="text" name="name" class="form-control"
                                               placeholder="ex.Free ship">
                                        <div class="mes-name mes-invalid">
                                            Tên là bắt buộc
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Mã code<span class="text-danger">*</span></label>
                                        <input type="text" name="code" class="form-control"
                                               placeholder="Viết hoa các ký tự ex.FREESHIP">
                                        <div class="mes-code mes-invalid">
                                            Mã code là bắt buộc
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Số lượng mã<span class="text-danger">*</span></label>
                                        <input type="number" name="number_voucher" class="form-control">
                                        <div class="mes-number_voucher mes-invalid">
                                            Số lượng mã lớn hơn 0
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Mô tả<span class="text-danger">*</span></label>
                                        <textarea type="text" name="description"
                                                  class="textarea-bisto form-control"></textarea>
                                        <div class="mes-description mes-v-display">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Tính năng mã</label>
                                        <select id="option-promotion" name="type"
                                                class="form-control input-sm m-bot15">
                                            <option value="1" selected>Giảm theo tiền</option>
                                            <option value="2">Giảm theo %</option>
                                        </select>
                                    </div>

                                    <div class="form-group" id="money-or-percent">
                                        <label>Nhập số % hoặc số tiền giảm<span class="text-danger">*</span></label>
                                        <input type="number"
                                               name="price" class="form-control">
                                        <div class="mes-price mes-invalid">
                                            Số tiền giảm hơn 0
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Số tiền tối thiểu áp dụng trên 1 đơn hàng</label>
                                        <input type="number"
                                               name="minimum_price" class="form-control">
                                        <div class="mes-minimum_price mes-invalid">
                                            Số tiền tối thiểu lớn hơn 0
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Nhập số tiền giảm tối đa</label>
                                        <input type="number"
                                               name="max_reduce_price" class="form-control">
                                        <div class="mes-max_reduce_price mes-invalid">
                                            Số tiền giảm lớn hơn 0
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col">
                                            <label for="start_at">Ngày bắt đầu</label>
                                            <input type="datetime-local"
                                                   class="form-control" name="start_at">
                                        </div>
                                        <div class="form-group col">
                                            <label for="end_at">Ngày kết thúc</label>
                                            <input required type="datetime-local"
                                                   class="form-control" name="end_at">
                                        </div>
                                    </div>
                                    <div class="mes-rangedate mes-invalid">
                                        Ngày bắt đầu phải nhỏ hơn ngày kết thúc
                                    </div>
                                    <button type="submit" id="add_coupon" name="add_coupon" class="btn btn-primary">
                                        Thêm mã
                                    </button>
                                    <button type="button" id="edit_coupon" name="edit_coupon"
                                            class="btn btn-primary">
                                        Lưu thay đổi mã
                                    </button>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        <!-- END ADD VOUCHER -->
        <!-- EDIT VOUCHER -->

        <!-- END EDIT VOUCHER -->
        <script type="text/javascript">
            let dt = {currentPage: 1, text_search: '', loading: {colspan: 9, type: 'table'}};
            let pagination;
            $(document).ready(function () {
                loadCoupons(dt);
                validateOnKeyup();
                validateRangeNumber('input[name=number_voucher]', 0, Infinity);
                setInputDateTime('start_at');
                setInputDateTime('end_at');
                pagination = definePagination({
                    onPageClick: (pageNumber, event) => {
                        dt.currentPage = pageNumber;
                        loadCoupons(dt);
                    }
                });

            });

            $(document).on('click', '.btn.show-modal-add-voucher', function () {
                $('.modal.add-voucher').modal('show');
                $('button#add_coupon').show();
                $('button#edit_coupon').hide();
                dataToForm({
                    fieldInputs: [{selector: 'input[name=name]'},
                        {selector: 'input[name=code]'},
                        {selector: 'input[name=number_voucher]'},
                        {selector: 'select[name=type]', value: '1'},
                        {selector: 'input[name=price]'},
                        {selector: 'input[name=max_reduce_price]'},
                        {selector: 'textarea[name=description]'},
                        {selector: 'input[name=minimum_price]'},
                        {selector: 'input[name=start_at]'},
                        {selector: 'input[name=end_at]'}],
                    parentSelector: '.f-add-voucher', action: 'clear-validate'
                })
            })

            $(document).on('click', '.show-modal-update-voucher', function () {
                const voucherId = $(this).attr('voucherid');
                $('div.f-add-voucher').data('voucherid', voucherId);
                $.ajax({
                    url: `coupons/${voucherId}`,
                    method: 'GET',
                }).done(res => {
                    const voucher = res.coupon || {};
                    $('input[name=name]').val(voucher.name);
                    $('input[name=code]').val(voucher.code);
                    $('input[name=number_voucher]').val(voucher.number_voucher);
                    $('select[name=type]').val(voucher.type);
                    $('textarea[name=description]').val(voucher.description);
                    $('input[name=price]').val(voucher.amount);
                    $('input[name=minimum_price]').val(voucher.required_minimum_price);
                    $('input[name=max_reduce_price]').val(voucher.max_reduce_price);
                    setInputDateTime('start_at', new Date(voucher.start_at));
                    setInputDateTime('end_at', new Date(voucher.end_at));

                    $('.modal.add-voucher').modal('show');
                    $('button#add_coupon').hide();
                    $('button#edit_coupon').show();
                }).fail(err => {
                    toastErrorMessage('Load voucher thất bại')
                })
            })

            function validateOnKeyup() {
                validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên mã là bắt buộc'
                    }], propertyName: 'name', type: 'onkeyup'
                });
                validateInput({
                    listValidateFunction: [{
                        func: rangeValidation,
                        params: {min: 1, max: Infinity},
                        mes: 'Số lượng lớn hơn 0'
                    }], propertyName: 'number_voucher', type: 'onkeyup'
                });
                validateOnKeyupUppercase('input[name=code]');
            }

            $('select[name=type]').on("change", function () {
                if ($(this).val() === '1') {
                    validateInput({
                        listValidateFunction: [{
                            func: rangeValidation,
                            mes: 'Giá lớn hơn 0',
                            params: {min: 0, max: Infinity}
                        }], propertyName: 'price', type: 'onkeyup'
                    });
                } else {
                    validateInput({
                        listValidateFunction: [{
                            func: rangeValidation,
                            mes: MES_PERCENT,
                            params: {min: 0, max: 100}
                        }], propertyName: 'price', type: 'onkeyup'
                    });
                }
            })

            function validateOnSubmit() {
                let isValidateType = true;
                if ($('select[name=type]').val() === '2') {
                    isValidateType = () => validateInput({
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Số tiền hay % giảm là bắt buộc'
                        }, {
                            func: rangeValidation,
                            mes: MES_PERCENT,
                            params: {min: 0, max: 100}
                        }], propertyName: 'price'
                    });
                } else {
                    isValidateType = () => validateInput({
                        listValidateFunction: [{
                            func: requiredValidation,
                            mes: 'Số tiền hay % giảm là bắt buộc'
                        }, {
                            func: rangeValidation,
                            mes: `Số tiền tối thiểu ${MIN_MONEY} VND`,
                            params: {min: MIN_MONEY, max: MAX_MONEY}
                        }], propertyName: 'price'
                    });
                }
                return (validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Tên mã là bắt buộc'
                    }], propertyName: 'name'
                }) && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Mã code là bắt buộc'
                    }], propertyName: 'code'
                }) && validateInput({
                    listValidateFunction: [{
                        func: rangeValidation,
                        params: {min: 1, max: Infinity},
                        mes: 'Số lượng lớn hơn 0'
                    }, {
                        func: requiredValidation,
                        mes: 'Số lượng là bắt buộc'
                    }], propertyName: 'number_voucher'
                }) && validateInput({
                    listValidateFunction: [{
                        func: requiredValidation,
                        mes: 'Mô tả là bắt buộc'
                    }], propertyName: 'description', type: 'textarea'
                }) && isValidateType() && validateInput({
                    listValidateFunction: [{
                        func: rangeValidation,
                        params: {min: 0, max: Infinity},
                        mes: 'Giá tối thiểu lớn hơn 0'
                    }], propertyName: 'minimum_price'
                }) && validateInput({
                    listValidateFunction: [{
                        func: rangeValidation,
                        params: {min: 0, max: Infinity},
                        mes: 'Giá giảm lớn hơn 0'
                    }], propertyName: 'max_reduce_price'
                }) && validateRangeDate('input[name=start_at]', 'input[name=end_at]', '.mes-rangedate'))
            }

            function generateCoupons(coupons = []) {
                let tbody = '';
                $.each(coupons, function (index, item) {
                    let id = item.id;

                    let startAt = item.start_at.formatDate();
                    let endAt = item.end_at.formatDate();

                    let price = null;
                    if (item.type == 1) {
                        price = item.amount.formatVND();
                    } else {
                        price = `${item.amount} %`;
                    }
                    tbody += `<tr>
						<td class="text-center">${item.name}</td>
						<td class="text-center">${item.code}</td>
						<td class="text-center">
							<span class="text-ellipsis">${price}</span>
					    </td>
                        <td class="text-center">${item.required_minimum_price.formatVND()}</td>
                         <td class="text-center">${item.max_reduce_price.formatVND()}</td>
						<td class="text-center">${item.number_voucher}</td>
						<td class="text-center">${startAt}</td>
						<td class="text-center">${endAt}</td>
						<td class="text-center">
						<span voucherid=${id} class="fas fa-edit text-success btn-show-update-bisto-css show-modal-update-voucher fa-lg" >
						</span>
						<span voucherid=${id} class="btn-confirm-del fa fa-times text-danger btn-delete-bisto-css fa-lg">
						</span>
						</td>
					</tr>`;
                });
                $('.table-striped>tbody').empty().append(tbody);
            }

            $(document).on('click', 'span.btn-confirm-del', function () {
                const voucherId = $(this).attr('voucherid');
                confirmDelete('Bạn có chắc muốn xóa voucher này?', function () {
                    $.ajax({
                        url: `coupons/${voucherId}`,
                        method: 'DELETE',
                        data: {
                            _token: '{{csrf_token()}}'
                        }
                    }).done(res => {
                        toastSuccess(res, 'Xóa thành công')
                        loadCoupons({...dt, loading: {type: 'content'}});
                    }).fail(err => {
                        toastErrorMessage('Xóa thất bại')
                    });
                })
            })

            function loadCoupons({currentPage = 1, text_search = '', loading = {colspan: 9, type: 'table'}}) {
                let _url = `coupons?page=${currentPage}&keyword=${text_search}`
                $.ajax({
                    url: _url,
                    method: 'GET',
                    loading
                })
                    .done(res => {
                        const coupons = res.coupons.data;
                        if (coupons.length === 0 && dt.currentPage > 1) {
                            dt.currentPage -= 1;
                            pagination.setCurrentPage(1);
                            loadCoupons(dt);
                        } else if (coupons.length === 0) {
                            $('.table-striped>tbody').empty().append(`<tr><td class="text-center" colspan="9">Không có mã giảm gia nào</td></tr>`);
                        } else {
                            generateCoupons(coupons);
                            pagination.setTotalItems(res.coupons.total);
                        }
                    })
                    .fail(err => {
                        toastErrorMessage('Load dữ liệu thất bại')
                    });
            }

            $(document).on('click', 'button#add_coupon', function (e) {
                if (validateOnSubmit()) {
                    let voucher = {};
                    for (const obj of $('div.f-add-voucher :input').serializeArray()) {
                        voucher[obj.name] = obj.value || '';
                    }
                    voucher.minimum_price = voucher.minimum_price === '' ? 0 : voucher.minimum_price;
                    voucher.max_reduce_price = voucher.max_reduce_price === '' ? 0 : voucher.max_reduce_price;
                    console.log(voucher);
                    $.ajax({
                        url: 'coupons',
                        method: 'POST',
                        data: {
                            ...voucher,
                            _token: "{{csrf_token()}}",
                        },
                    }).done(res => {
                        toastSuccessMessage('Thêm voucher thành công');
                        loadCoupons({...dt, loading: {type: 'content'}});
                        $('.modal.add-voucher').modal('hide');
                    }).fail(err => {
                        toastErrorMessage('Thêm thất bại')
                    })
                }
            });

            $(document).on('click', 'button#edit_coupon', function (e) {
                if (validateOnSubmit()) {
                    const voucherId = $('div.f-add-voucher').data('voucherid');
                    let voucher = {};
                    for (const obj of $('div.f-add-voucher :input').serializeArray()) {
                        voucher[obj.name] = obj.value || '';
                    }
                    voucher.minimum_price = voucher.minimum_price == '' ? 0 : voucher.minimum_price;
                    voucher.max_reduce_price = voucher.max_reduce_price == '' ? 0 : voucher.max_reduce_price;
                    $.ajax({
                        url: `coupons/${voucherId}`,
                        method: 'PUT',
                        data: {
                            ...voucher,
                            _token: "{{csrf_token()}}",
                        },
                    }).done(res => {
                        toastSuccessMessage('Cập nhật voucher thành công');
                        loadCoupons({...dt, loading: {type: 'content'}});
                        $('.modal.add-voucher').modal('hide');
                    }).fail(err => {
                        toastErrorMessage('Cập nhật thất bại')
                    })
                }
            });

            function onSearchVoucher() {
                if (event.key === 'Enter') {
                    dt.text_search = $('input[name=keywords_submit]').val();
                    loadCoupons(dt);
                }
            }
        </script>
@endsection
