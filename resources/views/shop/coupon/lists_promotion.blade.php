@extends('shop.layout')
@section('shop_content')
    <div id="content-page" class="content-page">
        <div class="col-sm-12 col-lg-12">
            <div id="loading-content">
                <div class="loading-content-css">
                    <div class="spinner-border text-primary" role="status">
                    </div>
                </div>
            </div>
            <div class="iq-card">
                <div class="panel-heading">
                    Danh sách mã khuyến mại
                </div>
                <div class="row row-action-bisto-css">
                    <div class="col-sm-5">
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="search"
                                   onkeydown="onSearchVoucher(this)"
                                   placeholder="&#xf002 Tìm kiếm"
                                   class="input-sm form-control btn-search-bisto-css"
                                   name="keywords_submit">
                        </div>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">Tên mã giảm</th>
                                <th class="text-center">Mã code</th>
                                <th class="text-center">Điều kiện</th>
                                <th class="text-center">Đơn tối thiểu</th>
                                <th class="text-center">Tiền giảm tối đa</th>
                                <th class="text-center">Số lượng voucher</th>
                                <th class="text-center">Bắt đầu</th>
                                <th class="text-center">Kết thúc</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- PAGINATION START-->
                <div class="pagination-custom">
                    <div id="pagination-container"></div>
                </div>
                <!-- PAGINATION END-->
            </div>
        </div>
        <script type="text/javascript">
            let dt = {currentPage: 1, text_search: '', loading: {colspan: 9, type: 'table'}};
            let pagination;
            $(document).ready(function () {
                loadCoupons(dt);
                pagination = definePagination({
                    onPageClick: (pageNumber, event) => {
                        dt.currentPage = pageNumber;
                        loadCoupons(dt);
                    }
                });
            });

            function generateCoupons(coupons = []) {
                let tbody = '';
                $.each(coupons, function (index, item) {
                    let id = item.id;

                    let startAt = item.start_at.formatDate();
                    let endAt = item.end_at.formatDate();

                    let price = null;
                    if (item.type == 1) {
                        price = item.amount.formatVND();
                    } else {
                        price = `${item.amount} %`;
                    }
                    tbody += `<tr>
						<td class="text-center">${item.name}</td>
						<td class="text-center">${item.code}</td>
						<td class="text-center">
							<span class="text-ellipsis">${price}</span>
					    </td>
                        <td class="text-center">${item.required_minimum_price.formatVND()}</td>
                         <td class="text-center">${item.max_reduce_price.formatVND()}</td>
						<td class="text-center">${item.number_voucher}</td>
						<td class="text-center">${startAt}</td>
						<td class="text-center">${endAt}</td>
					</tr>`;
                });
                $('.table-striped>tbody').empty().append(tbody);
            }

            function loadCoupons({currentPage = 1, text_search = '', loading = {colspan: 9, type: 'table'}}) {
                let _url = `promotions?page=${currentPage}&keyword=${text_search}&type_creater=3`
                $.ajax({
                    url: _url,
                    method: 'GET',
                    loading
                })
                    .done(res => {
                        const coupons = res.coupons.data;
                        if (coupons.length === 0 && dt.currentPage > 1) {
                            dt.currentPage -= 1;
                            pagination.setCurrentPage(1);
                            loadCoupons(dt);
                        } else if (coupons.length === 0) {
                            $('.table-striped>tbody').empty().append(`<tr><td class="text-center" colspan="9">Không có mã giảm gia nào</td></tr>`);
                        } else {
                            generateCoupons(coupons);
                            pagination.setTotalItems(res.coupons.total);
                        }
                    })
                    .fail(err => {
                        toastErrorMessage('Load dữ liệu thất bại')
                    });
            }


            function onSearchVoucher() {
                if (event.key === 'Enter') {
                    dt.text_search = $('input[name=keywords_submit]').val();
                    loadCoupons(dt);
                }
            }
        </script>
@endsection
