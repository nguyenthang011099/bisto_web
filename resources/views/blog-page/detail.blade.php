@extends('layout')
@section('app_content')
    <?php
    $image = asset('frontend_landingpage/images/DEFAULT_BLOG_THUMB.jpg');
    if (isset($blog->image)) {
        $image = $blog->image->url;
    }
    ?>
    <div class="container blog-page-container">
        <div class="row blog-page-container__inner">
            <div class="blog-page-container__left col-sm-12 col-md-12 col-lg-8 col-xl-8 col-xxl-8">
                <div class="">
                    <img
                        src="{{$image}}"
                        alt="{{$blog->title}}" class="img-fluid blog-page__thumb">
                </div>
                <h2>{{$blog->title}}</h2>
                <div class="blog-page__info">
                    <div class="blog-page__info__left">
                        <div class="blog-page__logo">
                            <img
                                src="{{asset('frontend_landingpage/images/icons/title_1.svg')}}"
                                class="img-fluid" alt="{{$blog->title}}">
                        </div>
                        <span>By Bisto</span>
                    </div>
                    <div class="blog-page__createdAt">
                        <i class="fa fa-clock-o"></i>
                        {{date("d/m/Y", strtotime($blog->created_at))}}
                    </div>
                </div>
                <div class="blog-page__content" id="blog-page-content">
                    {!! $blog->content !!}
                </div>
                <br/>
                <div class="blog-page__tags">
                    <b>TAG</b>
                    <div class="blog-page__tags__container">
                        @foreach ($blog->tags as $tag)
                            <a href="#" class="blog-page__tag__item">{{$tag->name}}</a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="blog-page-container__right col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4">
                <div class="blog-page__banner blog-page__banner1 super-center">
                    <a href="">
                        <img
                            src="https://bizweb.dktcdn.net/100/438/408/themes/843441/assets/article_banner_1.jpg?1640406972340"
                            class="img-fluid"
                            alt="">
                    </a>
                </div>
                <div class="blog-page__banner blog-page__banner2 super-center">
                    <a href="">
                        <img
                            src="https://bizweb.dktcdn.net/100/438/408/themes/843441/assets/article_banner_2.jpg?1640406972340"
                            class="img-fluid"
                            alt="">
                    </a>

                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid blog-cate" id="blog-cate-container">

    </div>
    <script src="{{asset('frontend_landingpage/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js2/app.js')}}"></script>
    <script>

        DEFAULT_THUMB = '{{asset('frontend_landingpage/images/DEFAULT_BLOG_THUMB.jpg')}}';
        PER_PAGE = 10//parseInt('{{\App\Blog::PER_PAGE}}');
        LOGO_URL = '{{asset('frontend_landingpage/images/icons/title_1.svg')}}'
        BASE_URL = '{{url('')}}';

        const _ = initBlogPage({containerSelector: '#blog-cate-container', mode: 'slide'})

        const blog = @json($blog) ??
        {
        }
        ;
        const advertisements = @json($advertisements) ??
        [];
        const $adsContainer = $(".blog-page-container__right");
        $adsContainer.empty();

        let blogCategories = [{
            created_at: null,
            currentPage: 1,
            id: blog?.blog_category_id,
            slug: blog?.blog_category?.slug,
            name: null,
            updated_at: null
        }]
        const currentBlogId = parseInt('{{$blog->id}}');
        $(document).ready(function () {
            for (const blogCategory of blogCategories) {
                blogCategory.currentPage = 1;
                const $blogCate = _.createBlogCate(blogCategory);
                $blogCate.$self.hide();
                blogCategory.$el = $blogCate
                _.$container.append($blogCate.$self);
                _.loadBlogs({
                    page: 1, blog_category_id: blogCategory?.id, callback: function (blogs, total) {
                        blogs = blogs.filter(b => b?.id !== currentBlogId);
                        $blogCate.$title.text('TIN LIÊN QUAN');
                        // if (total <= blogCategory.currentPage * PER_PAGE) {
                        //     $blogCate.$btnMore.remove();
                        // }
                        if (blogs?.length === 0) {
                            $blogCate.$self.remove();
                        }
                        $blogCate.appendBlogCards(blogs);
                        $blogCate.$self.show();

                    }
                })
            }
        })

        for (const advertisement of advertisements) {
            $adsContainer.append(`
                             <div class="blog-page__banner super-center">
                                <a href="${advertisement?.url}" target="_blank">
                                    <img
                                        src="${advertisement?.image?.url}"
                                        class="img-fluid"
                                        alt="">
                                </a>
                            </div>
                        `)
        }
    </script>
@endsection
