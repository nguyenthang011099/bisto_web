@extends('layout')
@section('app_content')
    <?php

    ?>
    <div class="container-fluid blog-cate" id="blog-cate-container">

    </div>
    <script src="{{asset('frontend_landingpage/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js2/app.js')}}"></script>
    <script>
        DEFAULT_THUMB = '{{asset('frontend_landingpage/images/DEFAULT_BLOG_THUMB.jpg')}}';
        PER_PAGE = parseInt('{{\App\Blog::PER_PAGE}}');
        LOGO_URL = '{{asset('frontend_landingpage/images/icons/title_1.svg')}}'
        BASE_URL = '{{url('')}}';
        const _ = initBlogPage({containerSelector: '#blog-cate-container', mode: 'slide'})

        const TYPE = {
            hot: '{{\App\Blog::TYPE_HOT}}',
            normal: '{{\App\Blog::TYPE_NORMAL}}'
        }

        let blogCategories = @json($blog_categories) ??
        [];

        blogCategories = [{
            name: 'Tin tức nổi bật',
            type: TYPE.hot,
            blog_category_id: null,
            slug: '{{\App\Blog::SLUG_HOT_PAGE}}',
        }, ...blogCategories]

        $(document).ready(function () {
            for (const blogCategory of blogCategories) {
                blogCategory.currentPage = 1;
                const $blogCate = _.createBlogCate(blogCategory);
                $blogCate.$self.hide();
                blogCategory.$el = $blogCate
                _.$container.append($blogCate.$self);
                _.loadBlogs({
                    page: 1,
                    blog_category_id: blogCategory?.id,
                    limit: 10,
                    type: blogCategory?.type ?? TYPE.normal,
                    blogCateInstance: $blogCate,
                    callback: function (blogs, total) {
                        if (total <= blogCategory.currentPage * PER_PAGE) {
                            // $blogCate.$btnMore.remove();
                        }
                        if (blogs?.length === 0) {
                            $blogCate.$self.remove();
                        }
                        $blogCate.appendBlogCards(blogs);
                        $blogCate.$self.show();
                    }
                })
            }
        })
    </script>
@endsection
