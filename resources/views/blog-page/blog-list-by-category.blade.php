@extends('layout')
@section('app_content')
    <?php

    ?>
    <div class="container-fluid blog-cate" id="blog-cate-container">

    </div>
    <script src="{{asset('frontend_landingpage/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js2/app.js')}}"></script>
    <script>
        DEFAULT_THUMB = '{{asset('frontend_landingpage/images/DEFAULT_BLOG_THUMB.jpg')}}';
        PER_PAGE = 18 //parseInt('{{\App\Blog::PER_PAGE}}');
        LOGO_URL = '{{asset('frontend_landingpage/images/icons/title_1.svg')}}'
        BASE_URL = '{{url('')}}';
        const type = `{{$type}}`
        let blogCategory = @json($blog_category) ??
        {
        }
        ;

        $(document).ready(function () {
            const _ = initBlogPage({containerSelector: '#blog-cate-container'})
            blogCategory.currentPage = 1;
            const $blogCate = _.createBlogCate(blogCategory);
            $blogCate.$self.hide();
            blogCategory.$el = $blogCate
            _.$container.append($blogCate.$self);
            _.loadBlogs({
                page: 1,
                blog_category_id: blogCategory?.id,
                type: type,
                limit: PER_PAGE,
                callback: function (blogs, total) {
                    if (total <= blogCategory.currentPage * PER_PAGE) {
                        $blogCate.$btnMore.remove();
                    }
                    if (blogs?.length === 0) {
                        $blogCate.$self.remove();
                    }
                    $blogCate.appendBlogCards(blogs);
                    $blogCate.$self.show();
                }
            })
        })
    </script>
@endsection
