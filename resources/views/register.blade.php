<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ĐĂNG KÝ LÀM THÀNH VIÊN SHOP</title>
    <!-- Favicon -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="shortcut icon" href="{{asset('backend/images/Logo.png')}}"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('backend/css2/bootstrap.min.css')}}">
    <!-- Typography CSS -->
    <link rel="stylesheet" href="{{asset('backend/css2/typography.css')}}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('backend/css2/style.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('backend/css2/responsive.css')}}">
</head>

<body>
<!-- loader Start -->
<div id="loading">
    <div id="loading-center">
    </div>
</div>
<!-- loader END -->
<!-- Sign in Start -->
<section class="sign-in-">
    <div class="container mt-5 p-0 bg-white">
        <div class="row no-gutters">
            <div class="col-sm-6 align-self-center">
                <div class="sign-in-from">
                    <?php
                    $message = Session()->get('message');
                    if ($message) {
                        echo '<span class="text-alert">' . $message . '</span>';
                        Session()->put('message', null);
                    }
                    ?>
                    <form class="mt-4" action="{{URL::to('/register')}}" method="post">
                        {{ csrf_field() }}
                        @foreach($errors->all() as $val)
                            <ul>
                                <li>{{$val}}</li>
                            </ul>
                        @endforeach
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên tài khoản</label>
                            <input type="text" name="full_name" class="form-control mb-0" id="exampleInputEmail1"
                                   placeholder="Tên tài khoản" required="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên đăng nhập</label>
                            <input type="text" name="user_name" class="form-control mb-0" id="exampleInputEmail1"
                                   placeholder="Tên đăng nhập" required="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" name="email" class="form-control mb-0" id="exampleInputEmail1"
                                   placeholder="Email" required="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail2">Số điện thoại</label>
                            <input type="number" name="phone_number" class="form-control mb-0"
                                   id="exampleInputEmail2" placeholder="Số điện thoại" required="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mật khẩu</label>
                            <input type="password" name="password" class="form-control mb-0"
                                   id="exampleInputPassword1" placeholder="Mật khẩu" required="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Tên shop</label>
                            <input type="text" name="shop_name" class="form-control mb-0" id="exampleInputPassword1"
                                   placeholder="Tên shop" required="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mô tả</label>
                            <input type="text" name="shop_desc" class="form-control mb-0" id="exampleInputPassword1"
                                   placeholder="Mô tả" required="">
                        </div>
                        <div class="d-inline-block w-100">
                            <div class="custom-control custom-checkbox d-inline-block mt-2 pt-1">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Tôi chấp nhận <a
                                        href="#">Điều khoản sử dụng</a></label>
                            </div>
                            <br>
                            <div class="custom-control custom-checkbox d-inline-block mt-2 pt-1 form-check">
                                <input class="form-check-input custom-control-input" type="checkbox" value="1"
                                       name="check_admin_shop" id="customCheck2">
                                <label class="form-check-label custom-control-label" for="customCheck2">Bạn có muốn
                                    làm tổng chi nhánh?</label>
                            </div>
                            <button type="submit" class="btn btn-primary float-right" name="signup">Đăng ký</button>
                        </div>
                        <div class="sign-info">
                                <span class="dark-color d-inline-block line-height-2">Bạn đã có tài khoản? <a
                                        href="{{url('/login')}}">Đăng nhập</a></span>
                            <ul class="iq-social-media">
                                <li><a href="https://www.facebook.com/BISTO.VN"><i class="fab fa-facebook-square"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter-square"></i></a></li>
                                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-6 text-center">
                <div class="sign-in-detail text-white">
                    <a class="sign-in-logo mb-5">BISTO</a>
                    <div class="owl-carousel" data-autoplay="true" data-loop="true" data-nav="false"
                         data-dots="true" data-items="1" data-items-laptop="1" data-items-tab="1"
                         data-items-mobile="1" data-items-mobile-sm="1" data-margin="0">
                        <div class="item">
                            <img src="{{asset('backend/images/login/1.png')}}" class="img-fluid mb-4"
                                 alt="logo">
                            <h4 class="mb-1 text-white">Theo dõi doanh thu chi tiết</h4>
                            <p>Một sự lựa chọn hoàn hảo khi đến với chúng tôi, bạn có thể theo dõi doanh thu cũng
                                như những lựa chọn cần thiết trong việc quản lý.</p>
                        </div>
                        <div class="item">
                            <img src="{{asset('backend/images/login/3.png')}}" class="img-fluid mb-4"
                                 alt="logo">
                            <h4 class="mb-1 text-white">Có ý tưởng hay sản phẩm mới</h4>
                            <p>Bạn có nhiều ưu đãi cho khách hàng mà không muốn phải chọn lựa từng sản phẩm một,
                                Bisto sẽ là giải pháp hữu hiệu để bạn gạt bỏ đi những bước rườm rà này.</p>
                        </div>
                        <div class="item">
                            <img src="{{asset('backend/images/login/4.png')}}" class="img-fluid mb-4"
                                 alt="logo">
                            <h4 class="mb-1 text-white">Có ý tưởng hay sản phẩm mới</h4>
                            <p>Bạn nhận ra rằng muốn nhập sản phẩm mới cũng như đầu mục sản phẩm mới? Bisto vui lòng
                                cung cấp cho bạn dịch vụ để quản lý sản phẩm tốt và hiệu quả.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Sign in END -->
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{asset('backend/js2/jquery.min.js')}}"></script>
<script src="{{asset('backend/js2/popper.min.js')}}"></script>
<script src="{{asset('backend/js2/bootstrap.min.js')}}"></script>
<!-- Appear JavaScript -->
<script src="{{asset('backend/js2/jquery.appear.js')}}"></script>
<!-- Countdown JavaScript -->
<script src="{{asset('backend/js2/countdown.min.js')}}"></script>
<!-- Counterup JavaScript -->
<script src="{{asset('backend/js2/waypoints.min.js')}}"></script>
<script src="{{asset('backend/js2/jquery.counterup.min.js')}}"></script>
<!-- Wow JavaScript -->
<script src="{{asset('backend/js2/wow.min.js')}}"></script>
<!-- Apexcharts JavaScript -->
<script src="{{asset('backend/js2/apexcharts.js')}}"></script>
<!-- Slick JavaScript -->
<script src="{{asset('backend/js2/slick.min.js')}}"></script>
<!-- Select2 JavaScript -->
<script src="{{asset('backend/js2/select2.min.js')}}"></script>
<!-- Owl Carousel JavaScript -->
<script src="{{asset('backend/js2/owl.carousel.min.js')}}"></script>
<!-- Magnific Popup JavaScript -->
<script src="{{asset('backend/js2/jquery.magnific-popup.min.js')}}"></script>
<!-- Smooth Scrollbar JavaScript -->
<script src="{{asset('backend/js2/smooth-scrollbar.js')}}"></script>
<!-- Chart Custom JavaScript -->
<script src="{{asset('backend/js2/chart-custom.js')}}"></script>
<!-- Custom JavaScript -->
<script src="{{asset('backend/js2/custom.js')}}"></script>
</body>

</html>
