@extends('layout')
@section('app_content')
    <div class="container">
        <div class="row">
            <div class="order-md-2 p-b-30">
                <div class="p-t-7 p-l-85 p-l-15-lg p-l-0-md">
                    <h3 class="mtext-111 cl2 p-b-16 txt-center">
                        CƠ CHẾ BẢO MẬT THÔNG TIN
                    </h3>
                    <div class="WordSection1">
                        <p class="MsoNormal" style="text-align:justify;background:white"><b><i><span
                                        style="line-height:150%;color:black">1. Mục đích và phạm vi thu
                                            thập</span></i></b></p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Việc thu thập dữ
                                    liệu chủ yếu trên Sàn giao dịch TMĐT BISTO bao
                                    gồm: email, điện thoại, tên đăng nhập, mật
                                    khẩu đăng nhập, địa chỉ khách hàng (thành viên). Đây là
                                    các thông tin mà BISTO cần thành viên cung cấp bắt buộc
                                    khi đăng ký sử dụng dịch vụ và để BISTO
                                    liên hệ xác nhận khi khách hàng đăng ký sử dụng
                                    dịch vụ trên website nhằm đảm bảo quyền
                                    lợi cho cho người tiêu dùng.&nbsp;</span></p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Các thành viên sẽ tự
                                    chịu trách nhiệm về bảo mật và lưu giữ
                                    mọi hoạt động sử dụng dịch vụ
                                    dưới tên đăng ký, mật khẩu và hộp thư điện
                                    tử của mình. Ngoài ra, thành viên có trách nhiệm thông
                                    báo kịp thời cho Sàn giao dịch TMĐT BISTO về
                                    những hành vi sử dụng trái phép, lạm dụng, vi
                                    phạm bảo mật, lưu giữ tên đăng ký và mật
                                    khẩu của bên thứ ba để có biện pháp giải
                                    quyết phù hợp.&nbsp;</span></p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><b><i><span
                                        style="line-height:150%;color:black">2. Phạm vi sử dụng thông
                                            tin</span></i></b></p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Sàn giao dịch TMĐT
                                    BISTO&nbsp;sử dụng thông tin thành viên cung cấp để:</span></p>
                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol;color:black">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span><span style="line-height:150%;color:black">Cung cấp các
                                    dịch vụ đến Thành viên;</span></p>
                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol;color:black">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span><span style="line-height:150%;color:black">Gửi các thông
                                    báo về các hoạt động trao đổi thông tin giữa
                                    thành viên và Sàn giao dịch TMĐT BISTO;</span></p>
                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol;color:black">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span><span style="line-height:150%;color:black">Ngăn ngừa các
                                    hoạt động phá hủy tài khoản người dùng của
                                    thành viên hoặc các hoạt động giả mạo Thành viên;</span></p>
                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol;color:black">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span><span style="line-height:150%;color:black">Liên lạc và
                                    giải quyết với thành viên trong những trường
                                    hợp đặc biệt.</span></p>
                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol;color:black">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span><span style="line-height:150%;color:black">Không sử
                                    dụng thông tin cá nhân của thành viên ngoài mục đích xác
                                    nhận và liên hệ có liên quan đến giao dịch tại
                                    BISTO.</span></p>
                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol;color:black">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span><span style="line-height:150%;color:black">Trong trường
                                    hợp có yêu cầu của pháp luật: Sàn giao dịch TMĐT
                                    BISTO có trách nhiệm hợp tác cung cấp thông tin cá nhân thành
                                    viên khi có yêu cầu từ cơ quan tư pháp bao gồm: Viện
                                    kiểm sát, tòa án, cơ quan công an điều tra liên quan
                                    đến hành vi vi phạm pháp luật nào đó của khách hàng.
                                    Ngoài ra, không ai có quyền xâm phạm vào thông tin cá nhân
                                    của thành viên.</span></p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><b><i><span
                                        style="line-height:150%;color:black;background:white">3.&nbsp;Thời gian
                                            lưu trữ thông tin</span></i></b><span style="line-height:150%;color:black">&nbsp;</span>
                        </p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Dữ liệu cá nhân của
                                    Thành viên sẽ được lưu trữ cho đến khi có yêu cầu
                                    hủy bỏ hoặc tự thành viên đăng nhập và thực
                                    hiện hủy bỏ. Còn lại trong mọi
                                    trường hợp thông tin cá nhân thành viên sẽ được bảo
                                    mật trên máy chủ của BISTO.</span></p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><b><i><span
                                        style="line-height:150%;color:black">4. Địa chỉ của đơn
                                            vị thu thập và quản lý thông tin cá nhân</span></i></b></p>
                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%;color:black">1.<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span><span style="line-height:150%;color:black">Công ty TNHH </span><span
                                lang="EN-US" style="line-height:150%;color:black">Anberry Việt Nam</span></p>
                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%;color:black">2.<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span><span style="line-height:150%;color:black">Trụ sở
                                    chính:<i>&nbsp;<span style="background:white">Tầng</span></i></span><i><span
                                    lang="EN-US" style="line-height:150%;color:black;background:white"> 8, số
                                        82 Vương Thừa Vũ, phường Khương Trung, quận Thanh Xuân,
                                        thành phố Hà Nội</span></i><i><span style="line-height:150%;
color:black">&nbsp;</span></i></p>
                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%;color:black">3.<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span><span style="line-height:150%;color:black">Email: </span><span
                                style="color:black"><a href="mailto:Bistovn@gmail.com"><span lang="EN-US"
                                                                                             style="line-height:150%;color:black">Bistovn@gmail.com</span></a></span><span
                                lang="EN-US" style="line-height:150%;color:black"> hoặc<u>
                                        Anberry.Vn@gmail.com</u></span></p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><b><i><span
                                        style="line-height:150%;color:black;background:white">5.&nbsp;Phương tiện
                                            và công cụ để người dùng tiếp cận và chỉnh
                                            sửa dữ liệu cá nhân của mình.</span></i></b><span
                                style="line-height:150%;color:black">&nbsp;</span></p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Thành viên có quyền tự
                                    kiểm tra, cập nhật, điều chỉnh hoặc
                                    hủy bỏ thông tin cá nhân của mình bằng cách đăng
                                    nhập vào tài khoản và chỉnh sửa thông tin cá nhân
                                    hoặc yêu cầu BISTO thực hiện việc này. </span></p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Khi tiếp nhận những
                                    phản hồi này, BISTO sẽ xác nhận lại thông tin,
                                    phải có trách nhiệm trả lời lý do và hướng
                                    dẫn thành viên khôi phục hoặc xóa bỏ thông tin cá
                                    nhận khách hàng.</span></p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><b><i><span
                                        style="line-height:150%;color:black">6. Cam kết bảo mật thông
                                            tin cá nhân khách hàng</span></i></b></p>
                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%;color:black">1.<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span><span style="line-height:150%;color:black">Thông tin cá nhân
                                    của thành viên trên BISTO được BISTO cam kết bảo
                                    mật tuyệt đối theo chính sách bảo vệ thông tin cá
                                    nhân của BISTO. Việc thu thập và sử dụng thông
                                    tin của mỗi thành viên chỉ được thực hiện
                                    khi có sự đồng ý của khách hàng đó trừ
                                    những trường hợp pháp luật có quy định khác.</span></p>
                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%;color:black">2.<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span><span style="line-height:150%;color:black">Không sử
                                    dụng, không chuyển giao, cung cấp hay tiết lộ cho
                                    bên thứ 3 nào về thông tin cá nhân của thành viên khi không
                                    có sự cho phép đồng ý từ thành viên.</span></p>
                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%;color:black">3.<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span><span style="line-height:150%;color:black">Trong trường
                                    hợp máy chủ lưu trữ thông tin bị hacker tấn công
                                    dẫn đến mất mát dữ liệu cá nhân thành viên, BISTO
                                    sẽ có trách nhiệm thông báo vụ việc cho cơ quan
                                    chức năng điều tra xử lý kịp thời và thông
                                    báo cho thành viên được biết.</span></p>
                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%;color:black">4.<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span><span style="line-height:150%;color:black">Bảo mật
                                    tuyệt đối mọi thông tin giao dịch trực
                                    tuyến của Thành viên bao gồm thông tin hóa đơn kế toán
                                    chứng từ số hóa tại khu vực dữ liệu
                                    trung tâm an toàn cấp 1 của BISTO.</span></p>
                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%;color:black">5.<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span></span><span style="line-height:150%;color:black">Ban quản
                                    lý BISTO yêu cầu các cá nhân khi đăng ký/mua hàng là thành
                                    viên, phải cung cấp đầy đủ thông tin cá nhân có liên
                                    quan như: Họ và tên, địa chỉ liên lạc, email, số
                                    chứng minh nhân dân, điện thoại, số tài khoản,
                                    số thẻ thanh toán …., và chịu trách nhiệm về tính
                                    pháp lý của những thông tin trên. Ban quản lý
                                    BISTO không chịu trách nhiệm cũng như không giải
                                    quyết mọi khiếu nại có liên quan đến quyền
                                    lợi của Thành viên đó nếu xét thấy tất cả
                                    thông tin cá nhân của thành viên đó cung cấp khi đăng ký ban
                                    đầu là không chính xác.</span></p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><b><i><span
                                        style="line-height:150%;color:black;background:white">7.&nbsp;Cơ chế
                                            tiếp nhận và giải quyết khiếu nại liên quan
                                            đến việc thông tin cá nhân khách hàng</span></i></b><b><span
                                    style="line-height:150%;color:black">&nbsp;</span></b></p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Thành viên có quyền gửi
                                    khiếu nại về việc lộ thông tin các nhân cho bên
                                    thứ 3</span><span lang="EN-US" style="line-height:150%;color:black">
                                    với các thông tin, chứng cứ liên quan</span><span
                                style="line-height:150%;color:black"> đến Ban quản trị
                                    của Sàn giao dịch thương mại điện tử BISTO
                                    đến địa chỉ Công ty hoặc qua email.</span><span
                                style="line-height:150%;color:black"> </span><span style="color:black">Công ty
                                    cam kết sẽ phản hồi ngay lập tức trong vòng 24 tiếng để cùng Người
                                    dùng thống nhất phương án giải quyết.</span></p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Email:&nbsp;</span><span style="color:
black"><a href="mailto:Bistovn@gmail.com"><span lang="EN-US" style="line-height:
150%;color:black">Bistovn@gmail.com</span></a></span><span lang="EN-US" style="line-height:150%;color:black"> hoặc<u> Anberry.Vn@gmail.com</u></span>
                        </p>
                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Thời gian xứ lý
                                    phản ánh liên quan đến thông tin cá nhân khách hàng là </span><span lang="EN-US"
                                                                                                        style="line-height:150%;color:black">05</span><span
                                style="line-height:150%;color:black"> ngày.</span></p>
                        <p class="MsoNormal"><span lang="EN-US" style="color:black">&nbsp;</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
