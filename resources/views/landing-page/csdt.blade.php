@extends('layout')
@section('app_content')
    <div class="container">
        <div class="row">
            <div class="order-md-2 p-b-30">
                <div class="p-t-7 p-l-85 p-l-15-lg p-l-0-md">
                    <h3 class="mtext-111 cl2 p-b-16 txt-center">
                        CHÍNH SÁCH ĐỔI TRẢ
                    </h3>
                    <div class="WordSection1">

                        <p class="MsoListParagraph" style="margin-left:54.0pt;mso-add-space:auto;
text-align:justify;text-indent:-18.0pt;mso-pagination:none;mso-list:l3 level1 lfo10;
background:white"><!--[if !supportLists]--><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><span style="mso-list:Ignore">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span lang="EN-US" style="mso-bidi-font-size:
13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext;mso-ansi-language:EN-US">Đ</span></b><b><span
                                    style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">iều
kiện áp dụng</span></b><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><o:p></o:p></span></b></p>

                        <p class="MsoNormal" style="text-align:justify;mso-pagination:none;background:
white"><span style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;
mso-color-alt:windowtext">&nbsp;Theo các điều khoản và điều
kiện được quy định trong Chính sách Trả hàng và Hoàn
tiền này và tạo thành một phần của Điều
khoản dịch vụ, BISTO đảm bảo quyền
lợi của Người mua bằng cách cho phép gửi yêu
cầu hoàn trả sản phẩm và/hoặc hoàn tiền
trước khi hết hạn. Thời hạn BISTO Đảm
Bảo đã được qui định trong Điều khoản
Dịch vụ.</span><span style="mso-bidi-font-size:13.0pt;line-height:
150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span>
                        </p>

                        <p class="MsoListParagraph" style="margin-left:54.0pt;mso-add-space:auto;
text-align:justify;text-indent:-18.0pt;mso-pagination:none;mso-list:l3 level1 lfo10;
background:white"><!--[if !supportLists]--><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><span style="mso-list:Ignore">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Điều kiện
trả hàng</span></b><b><span style="mso-bidi-font-size:13.0pt;line-height:
150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></b>
                        </p>

                        <p class="MsoNormal" style="text-align:justify;mso-pagination:none;background:
white"><span style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;
mso-color-alt:windowtext">Người mua đồng ý rằng cô
ấy/anh ấy chỉ có thể yêu cầu trả hàng và
hoàn tiền trong các trường hợp sau:</span><span
                                style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;mso-pagination:
none;mso-list:l8 level1 lfo3;tab-stops:list 36.0pt;background:white"><!--[if !supportLists]--><span style="font-size:10.0pt;mso-bidi-font-size:13.0pt;line-height:150%;font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol"><span style="mso-list:Ignore">·<span
                                        style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Người mua
đã thanh toán nhưng không nhận được sản phẩm;</span><span
                                style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;mso-pagination:
none;mso-list:l8 level1 lfo3;tab-stops:list 36.0pt;background:white"><!--[if !supportLists]--><span style="font-size:10.0pt;mso-bidi-font-size:13.0pt;line-height:150%;font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol"><span style="mso-list:Ignore">·<span
                                        style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Sản phẩm
bị lỗi hoặc bị hư hại trong quá trình
vận chuyển;</span><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><o:p></o:p></span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;mso-pagination:
none;mso-list:l8 level1 lfo3;tab-stops:list 36.0pt;background:white"><!--[if !supportLists]--><span style="font-size:10.0pt;mso-bidi-font-size:13.0pt;line-height:150%;font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol"><span style="mso-list:Ignore">·<span
                                        style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Người bán giao
sai sản phẩm cho Người mua (VD: sai kích cỡ, sai màu
sắc, v.vv...);</span><span style="mso-bidi-font-size:13.0pt;line-height:
150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span>
                        </p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;mso-pagination:
none;mso-list:l8 level1 lfo3;tab-stops:list 36.0pt;background:white"><!--[if !supportLists]--><span style="font-size:10.0pt;mso-bidi-font-size:13.0pt;line-height:150%;font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol"><span style="mso-list:Ignore">·<span
                                        style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Sản phẩm
Người mua nhận được khác biệt một cách rõ
rệt so với thông tin mà Người bán cung cấp trong
mục mô tả sản phẩm;</span><span style="mso-bidi-font-size:
13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><o:p></o:p></span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;mso-pagination:
none;mso-list:l8 level1 lfo3;tab-stops:list 36.0pt;background:white"><!--[if !supportLists]--><span style="font-size:10.0pt;mso-bidi-font-size:13.0pt;line-height:150%;font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol"><span style="mso-list:Ignore">·<span
                                        style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Người bán
đã tự thỏa thuận và đồng ý cho
Người mua trả hàng (tuy nhiên BISTO sẽ cần Người
bán xác nhận lại những thoả thuận này).</span><span
                                style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></p>

                        <p class="MsoNormal" style="text-align:justify;mso-pagination:none;background:
white"><span style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;
mso-color-alt:windowtext">Tất cả các yêu cầu trả
hàng-hoàn tiền phải được thực hiện trên Ứng
dụng BISTO. BISTO luôn xem xét cẩn thận từng yêu
cầu trả hàng/hoàn tiền của Người mua và có
quyền đưa ra quyết định cuối cùng đối với
yêu cầu đó dựa trên các quy định nêu trên và &nbsp;theo
Điều khoản Dịch vụ của BISTO.</span><span
                                style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></p>

                        <p class="MsoListParagraph" style="margin-left:54.0pt;mso-add-space:auto;
text-align:justify;text-indent:-18.0pt;mso-pagination:none;mso-list:l3 level1 lfo10;
background:white"><!--[if !supportLists]--><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><span style="mso-list:Ignore">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Không thay đổi
ý định mua hàng</span></b><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><o:p></o:p></span></b></p>

                        <p class="MsoNormal" style="text-align:justify;mso-pagination:none;background:
white"><span style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;
mso-color-alt:windowtext">Trừ khi được đề cập trong
Chính sách Trả hàng và Hoàn tiền này, các trường hợp
trả hàng do Người mua thay đổi ý định mua hàng
sẽ không được chấp nhận.&nbsp;</span><span
                                style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></p>

                        <p class="MsoNormal" style="text-align:justify;mso-pagination:none;background:
white"><span style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;
mso-color-alt:windowtext">Khi nhận được yêu cầu trả
hàng và/hoặc hoàn tiền từ phía Người mua, BISTO
sẽ thông báo cho Người bán qua văn bản (thông báo trên
Ứng dụng và/hoặc thư điện tử và/hoặc tin
nhắn điện thoại). Người bán cần phải
gửi phản hồi theo hướng dẫn của BISTO trong
thời gian quy định. Sau thời gian đó, nếu BISTO không
nhận được bất cứ phản hồi nào từ
Người bán, BISTO sẽ hiểu rằng Người bán hoàn toàn
đồng ý với yêu cầu của Người mua, và
tự động hoàn tiền cho Người mua mà không thông báo
gì thêm.</span><span style="mso-bidi-font-size:13.0pt;line-height:150%;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span>
                        </p>

                        <p class="MsoNormal" style="text-align:justify;mso-pagination:none;background:
white"><span style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;
mso-color-alt:windowtext">BISTO luôn theo dõi phản hồi
của Người bán trong từng trường hợp và toàn
quyền ra quyết định cuối cùng đối với
&nbsp;yêu cầu trả hàng/hoàn tiền của Người mua.</span><span
                                style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></p>

                        <p class="MsoListParagraph" style="margin-left:54.0pt;mso-add-space:auto;
text-align:justify;text-indent:-18.0pt;mso-pagination:none;mso-list:l3 level1 lfo10;
background:white"><!--[if !supportLists]--><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><span style="mso-list:Ignore">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Tình trạng
của hàng trả lại</span></b><b><span style="mso-bidi-font-size:
13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><o:p></o:p></span></b></p>

                        <p class="MsoNormal" style="text-align:justify;mso-pagination:none;background:
white"><span style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;
mso-color-alt:windowtext">Để hạn chế các rắc rối
phát sinh liên quan đến trả hàng, Người mua lưu ý
cần phải gửi trả sản phẩm bao gồm
toàn bộ phụ kiện đi kèm, hóa đơn VAT, tem phiếu
bảo hành... nếu có và sản phẩm phải trong
tình trạng nguyên vẹn như khi nhận hàng. BISTO
khuyến khích Người mua chụp lại ảnh sản
phẩm ngay khi nhận được để làm bằng chứng
đối chiếu/khiếu nại về sau nếu
cần.&nbsp;</span><span style="mso-bidi-font-size:13.0pt;line-height:150%;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span>
                        </p>

                        <p class="MsoNormal" style="text-align:justify;mso-pagination:none;background:
white"><span lang="EN-US" style="mso-bidi-font-size:13.0pt;line-height:150%;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext;
background:white;mso-ansi-language:EN-US">BISTO </span><span style="mso-bidi-font-size:
13.0pt;line-height:150%;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;
mso-color-alt:windowtext;background:white">khuyến khích Người mua
chụp lại ảnh sản phẩm ngay khi nhận được
để làm bằng chứng đối chiếu/khiếu nại
về sau nếu cần.</span><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><o:p></o:p></span></p>

                        <p class="MsoListParagraph" style="margin-left:54.0pt;mso-add-space:auto;
text-align:justify;text-indent:-18.0pt;mso-pagination:none;mso-list:l3 level1 lfo10;
background:white"><!--[if !supportLists]--><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><span style="mso-list:Ignore">5.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Chi phí trả hàng</span></b><b><span
                                    style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></b></p>

                        <p class="MsoNormal" style="text-align:justify;mso-pagination:none;background:
white"><span style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;
mso-color-alt:windowtext">Người bán và Người mua cần
thống nhất về việc ai sẽ là người
chịu phí vận chuyển đối với hàng trả
lại.</span><span style="mso-bidi-font-size:13.0pt;line-height:150%;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span>
                        </p>

                        <p class="MsoNormal" style="text-align:justify;mso-pagination:none;background:
white"><b><u><span style="mso-bidi-font-size:13.0pt;line-height:150%;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black;mso-color-alt:windowtext">Lưu ý:</span></u></b><span
                                style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">&nbsp;Hiện
tại, BISTO chưa hỗ trợ phí vận chuyển cũng
như thương lượng phí vận chuyển trả hàng cho đơn hàng
khiếu nại.</span><span style="mso-bidi-font-size:13.0pt;line-height:
150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span>
                        </p>

                        <p class="MsoListParagraph" style="margin-left:54.0pt;mso-add-space:auto;
text-align:justify;text-indent:-18.0pt;mso-pagination:none;mso-list:l3 level1 lfo10;
background:white"><!--[if !supportLists]--><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><span style="mso-list:Ignore">6.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Hoàn tiền cho
Hàng trả lại</span></b><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><o:p></o:p></span></b></p>

                        <p class="MsoNormal" style="text-align:justify;mso-pagination:none;background:
white"><span style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;
mso-color-alt:windowtext">BISTO sẽ chỉ hoàn tiền cho Người
mua khi Người bán xác nhận đã nhận được Hàng
trả lại. Trong trường hợp Người bán không
phản hồi trong khoảng thời gian đã được
quy định trước, BISTO sẽ tự động hoàn tiền
cho Người mua mà không thông báo gì thêm. Để biết thêm
thông tin về thời hạn phản hồi của
Người bán, vui lòng bấm vào đây. Tùy từng trường
hợp, tiền hoàn trả sẽ được chuyển vào
thẻ tín dụng/tài khoản ngân hàng được chỉ
định của Người mua.</span><span style="mso-bidi-font-size:
13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><o:p></o:p></span></p>

                        <p class="MsoNormal" style="text-align:justify;mso-pagination:none;background:
white"><b><u><span style="mso-bidi-font-size:13.0pt;line-height:150%;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black;mso-color-alt:windowtext">Lưu ý:</span></u></b><span
                                style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">&nbsp;Với
đơn hàng COD, Người mua có trách nhiệm phải cung cấp
thông tin tài khoản ngân hàng để nhận tiền hoàn
trả.</span><span style="mso-bidi-font-size:13.0pt;line-height:150%;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span>
                        </p>

                        <p class="MsoListParagraph" style="margin-left:54.0pt;mso-add-space:auto;
text-align:justify;text-indent:-18.0pt;mso-pagination:none;mso-list:l3 level1 lfo10;
background:white"><!--[if !supportLists]--><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><span style="mso-list:Ignore">7.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span></b><!--[endif]--><b><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Liên lạc
giữa người bán và người mua</span></b><b><span
                                    style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></b></p>

                        <p class="MsoNormal" style="text-align:justify;mso-pagination:none;background:
white"><span style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;
mso-color-alt:windowtext">BISTO khuyến khích Người dùng liên
hệ để thương lượng trước với nhau khi có
bất cứ vấn đề nào phát sinh trong giao dịch. BISTO
sẽ chỉ can thiệp trong trường hợp Người bán
và Người mua không thể đi đến thỏa thuận
cuối cùng; do vậy Người mua nên chủ động liên
hệ với Người bán khi có bất cứ vấn đề
nào liên quan đến đơn hàng.</span><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><o:p></o:p></span></p>

                        <p style="margin:0cm;text-align:justify;text-indent:36.0pt;line-height:150%;
background:white"><strong><span lang="EN-US" style="font-size:13.0pt;line-height:
150%;color:black;mso-color-alt:windowtext;mso-no-proof:yes">8</span></strong><strong><span style="font-size:13.0pt;line-height:150%;color:black;mso-color-alt:windowtext;
mso-ansi-language:VI;mso-no-proof:yes">. Quyền lợi của
người bán:</span></strong><span style="font-size:13.0pt;line-height:150%;
mso-ansi-language:VI;mso-no-proof:yes"><o:p></o:p></span></p>

                        <p style="margin:0cm;text-align:justify;text-indent:36.0pt;line-height:150%;
background:white"><strong><span style="font-size:13.0pt;line-height:150%;
color:black;mso-color-alt:windowtext;mso-ansi-language:VI;mso-no-proof:yes">-&nbsp;</span></strong><span style="font-size:13.0pt;line-height:150%;color:black;mso-color-alt:windowtext;
mso-ansi-language:VI;mso-no-proof:yes">Khi nhận được yêu cầu
trả hàng và/hoặc hoàn tiền từ phía Người mua, </span><span lang="EN-US" style="font-size:13.0pt;line-height:150%;color:black;mso-color-alt:
windowtext;mso-no-proof:yes">BISTO</span><span style="font-size:13.0pt;
line-height:150%;color:black;mso-color-alt:windowtext;mso-ansi-language:VI;
mso-no-proof:yes"> sẽ thông báo cho Người bán qua văn bản
(thông báo trên Ứng dụng và/hoặc thư điện tử
và/hoặc tin nhắn điện thoại). Người bán cần
phải gửi phản hồi theo hướng dẫn của </span><span lang="EN-US" style="font-size:13.0pt;line-height:150%;color:black;mso-color-alt:
windowtext;mso-no-proof:yes">BISTO</span><span style="font-size:13.0pt;
line-height:150%;color:black;mso-color-alt:windowtext;mso-ansi-language:VI;
mso-no-proof:yes"> trong thời gian quy định. Sau thời gian
đó, nếu </span><span lang="EN-US" style="font-size:13.0pt;line-height:150%;
color:black;mso-color-alt:windowtext;mso-no-proof:yes">BISTO</span><span style="font-size:13.0pt;line-height:150%;color:black;mso-color-alt:windowtext;
mso-ansi-language:VI;mso-no-proof:yes"> không nhận được bất
cứ phản hồi nào từ Người bán, </span><span lang="EN-US" style="font-size:13.0pt;line-height:150%;color:black;mso-color-alt:
windowtext;mso-no-proof:yes">BISTO</span><span style="font-size:13.0pt;
line-height:150%;color:black;mso-color-alt:windowtext;mso-ansi-language:VI;
mso-no-proof:yes"> sẽ hiểu rằng Người bán hoàn toàn
đồng ý với yêu cầu của Người mua, và
tự động hoàn tiền cho Người mua mà không thông báo
gì thêm.</span><span style="font-size:13.0pt;line-height:150%;
mso-ansi-language:VI;mso-no-proof:yes"><o:p></o:p></span></p>

                        <p style="margin:0cm;text-align:justify;text-indent:36.0pt;line-height:150%;
background:white"><span style="font-size:13.0pt;line-height:150%;color:black;
mso-color-alt:windowtext;mso-ansi-language:VI;mso-no-proof:yes">- </span><span lang="EN-US" style="font-size:13.0pt;line-height:150%;color:black;mso-color-alt:
windowtext;mso-no-proof:yes">BISTO</span><span style="font-size:13.0pt;
line-height:150%;color:black;mso-color-alt:windowtext;mso-ansi-language:VI;
mso-no-proof:yes"> luôn theo dõi phản hồi của
Người bán trong từng trường hợp và toàn quyền ra
quyết định cuối cùng đối với &nbsp;yêu cầu
trả hàng/hoàn tiền của Người mua.</span><span style="font-size:13.0pt;line-height:150%;mso-ansi-language:VI;mso-no-proof:
yes"><o:p></o:p></span></p>

                        <p style="margin:0cm;text-align:justify;text-indent:36.0pt;line-height:150%;
background:white"><span style="font-size:13.0pt;line-height:150%;color:black;
mso-color-alt:windowtext;mso-ansi-language:VI;mso-no-proof:yes">9. <b><span style="mso-spacerun:yes">&nbsp;</span>Thời gian đổi trả</b></span><span
                                style="font-size:13.0pt;line-height:150%;mso-ansi-language:VI;mso-no-proof:
yes"><o:p></o:p></span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Trừ
khi được quy định khác đi trong phần giới thiệu
về sản phẩm trên </span><span lang="EN-US" style="mso-bidi-font-size:
13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext;mso-ansi-language:EN-US">ứng
dụng</span><span style="mso-bidi-font-size:13.0pt;line-height:150%;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black;mso-color-alt:windowtext"> của </span><span lang="EN-US"
                                                        style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext;
mso-ansi-language:EN-US">BISTO</span><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">, thời gian
đổi/trả/sửa các sản phẩm được xác
định như sau:</span><span style="mso-bidi-font-size:13.0pt;line-height:
150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span>
                        </p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;mso-list:l5 level1 lfo11;
tab-stops:list 36.0pt;background:white"><!--[if !supportLists]--><span style="font-size:10.0pt;mso-bidi-font-size:13.0pt;line-height:150%;font-family:
Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol"><span style="mso-list:Ignore">·<span
                                        style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Trước khi
nhận hàng:&nbsp;Khách hàng được quyền trả lại
sản phẩm cho nhân viên vận chuyển (Shipper) ngay
tại thời điểm giao nhận&nbsp;hàng</span><span
                                style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;mso-list:l5 level2 lfo11;
tab-stops:list 72.0pt;background:white"><!--[if !supportLists]--><span style="font-size:10.0pt;mso-bidi-font-size:13.0pt;line-height:150%;font-family:
&quot;Courier New&quot;;mso-fareast-font-family:&quot;Courier New&quot;"><span style="mso-list:
Ignore">o<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Sau khi nhận hàng</span><span lang="EN-US" style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;
mso-color-alt:windowtext;mso-ansi-language:EN-US"> K</span><span
                                style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">hách
hàng được quyền đổi/trả sản phẩm trong 02
ngày làm việc kể từ ngày nhận được sản
phẩm</span><span style="mso-bidi-font-size:13.0pt;line-height:150%;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span>
                        </p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><b><span
                                    style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;mso-color-alt:windowtext">Lưu
ý:</span></b><span style="mso-bidi-font-size:13.0pt;line-height:150%;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;
color:black;mso-color-alt:windowtext">&nbsp;Ngày làm việc không bao
gồm chủ nhật hay các ngày lễ tết và Thời
gian khách hàng gửi hàng được tính theo dấu bưu điện
ghi trên kiện hàng.</span><span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;"><o:p></o:p></span></p>

                        <p style="margin:0cm;text-align:justify;text-indent:36.0pt;line-height:150%;
background:white"><span style="font-size:13.0pt;line-height:150%;color:black;
mso-color-alt:windowtext;mso-ansi-language:VI;mso-no-proof:yes">10. <strong>&nbsp;Phương
thức đổi trả:</strong></span><span style="font-size:13.0pt;
line-height:150%;mso-ansi-language:VI;mso-no-proof:yes"><o:p></o:p></span></p>

                        <p style="margin:0cm;text-align:justify;text-indent:36.0pt;line-height:150%;
background:white"><span style="font-size:13.0pt;line-height:150%;color:black;
mso-color-alt:windowtext;mso-ansi-language:VI;mso-no-proof:yes">Khách hàng vui
lòng liên hệ với </span><span lang="EN-US" style="font-size:
13.0pt;line-height:150%;color:black;mso-color-alt:windowtext;mso-no-proof:yes">BISTO</span><span style="font-size:13.0pt;line-height:150%;color:black;mso-color-alt:windowtext;
mso-ansi-language:VI;mso-no-proof:yes"> qua Email, Facebook hoặc Số
điện thoại sau để được hướng dẫn về
địa điểm gửi trả hàng:</span><span style="font-size:
13.0pt;line-height:150%;mso-ansi-language:VI;mso-no-proof:yes"><o:p></o:p></span></p>

                        <p style="margin:0cm;text-align:justify;text-indent:36.0pt;line-height:150%;
background:white"><span style="font-size:13.0pt;line-height:150%;color:black;
mso-color-alt:windowtext;mso-ansi-language:VI;mso-no-proof:yes">Email:&nbsp;</span><strong><u><span lang="EN-US" style="font-size:13.0pt;line-height:150%;color:black;mso-color-alt:
windowtext;mso-no-proof:yes">Bistovn</span></u></strong><strong><u><span style="font-size:13.0pt;line-height:150%;color:black;mso-color-alt:windowtext;
mso-ansi-language:VI;mso-no-proof:yes">@gmail.com</span></u></strong><span style="font-size:13.0pt;line-height:150%;mso-ansi-language:VI;mso-no-proof:
yes"><o:p></o:p></span></p>

                        <p style="margin:0cm;text-align:justify;text-indent:36.0pt;line-height:150%;
background:white"><span style="font-size:13.0pt;line-height:150%;color:black;
mso-color-alt:windowtext;mso-ansi-language:VI;mso-no-proof:yes">SĐT: </span><span lang="EN-US" style="font-size:13.0pt;line-height:150%;color:black;mso-color-alt:
windowtext;mso-no-proof:yes">0963 228 283</span><span lang="EN-US"
                                                      style="font-size:13.0pt;line-height:150%;mso-no-proof:yes"><o:p></o:p></span>
                        </p>

                        <p style="margin:0cm;text-align:justify;text-indent:36.0pt;line-height:150%;
background:white"><strong><span style="font-size:13.0pt;line-height:150%;
color:black;mso-color-alt:windowtext;mso-ansi-language:VI;mso-no-proof:yes">Lưu
ý:</span></strong><span style="font-size:13.0pt;line-height:150%;
color:black;mso-color-alt:windowtext;mso-ansi-language:VI;mso-no-proof:yes">&nbsp;</span><span lang="EN-US" style="font-size:13.0pt;line-height:150%;color:black;mso-color-alt:
windowtext;mso-no-proof:yes">BISTO</span><span style="font-size:13.0pt;
line-height:150%;color:black;mso-color-alt:windowtext;mso-ansi-language:VI;
mso-no-proof:yes"> sẽ gọi lại cho quý khách hàng trong
vòng 02 ngày làm việc kể từ ngày nhận được
hàng đổi trả về. Tuy nhiên, để đảm bảo hàng
hóa không bị thất lạc trong quá trình vận
chuyển về </span><span lang="EN-US" style="font-size:13.0pt;
line-height:150%;color:black;mso-color-alt:windowtext;mso-no-proof:yes">BISTO</span><span style="font-size:13.0pt;line-height:150%;color:black;mso-color-alt:windowtext;
mso-ansi-language:VI;mso-no-proof:yes">, khách hàng vui lòng
thường xuyên kiểm tra lộ trình bưu kiện
mình gửi đi.</span><span style="font-size:13.0pt;line-height:150%;
mso-ansi-language:VI;mso-no-proof:yes"><o:p></o:p></span></p>

                        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-size:
13.0pt;line-height:150%;mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p>&nbsp;</o:p></span></p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
