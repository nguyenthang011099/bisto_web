@extends('layout')
@section('app_content')
    <div class="container">
        <div class="row">
            <div class="order-md-2 p-b-30">
                <div class="p-t-7 p-l-85 p-l-15-lg p-l-0-md">
                    <h2 class="mtext-111 cl2 p-b-16 txt-center" style="font-size: 50px;">
                        HỢP TÁC BÁN HÀNG
                    </h2><br>
                    <h3 class="mtext-111 txt-center" style="margin-bottom: 2%;">Hợp tác cùng phát triển</h3>
                    <section class="bg-white py-10">
                        <div class="container">
                            <div class="row" style="position: absolute;">
                                <div class="col-12 col-md-6 ct" style="margin-top: 10%;">
                                    <h1>
                                        Tham gia bán hàng cùng BISTO</h1>
                                    <p>Tăng trưởng doanh số,tối ưu chi phí bán hàng, trải nghiệm độc đáo, nhiều quyền
                                        lợi hấp dẫn khi hợp tác với BISTO!</p>
                                </div>
                            </div>
                            <div class="row">
                                <img src="{{asset('frontend_landingpage/images/bg-2.jpg')}}" alt=""
                                     style="width:100%;height: 600px;border-radius: 20px;">
                            </div>
                        </div>
                    </section>
                    <h3 class="mtext-111 txt-center" style="margin-top: 5%;">Lý do nên chọn BISTO</h3>
                    <section class="bg-white py-10">
                        <div class="container">
                            <div class="row text-center">
                                <div class="col-lg-4 mb-5 mb-lg-0">
                                    <div
                                        class="icon-stack icon-stack-xl bg-gradient-primary-to-secondary text-white mb-4">
                                        <i data-feather="layers"></i></div>
                                    <img
                                        src="https://png.pngtree.com/png-vector/20191122/ourmid/pngtree-team-metaphor-people-connecting-puzzle-elements-symbol-of-teamwork-cooperation-partnership-png-image_2018498.jpg"
                                        width="100" height="100"
                                        style="border-radius: 50%;border: 2px solid;margin-bottom: 2%;" alt="">
                                    <h3 class="mtext-111">Kết nối triệu người dùng</h3>
                                    <p class="mb-0 ">BISTO kết nối người tiêu dùng tới các Shop.Thương hiệu thời
                                        trang lớn với đa dạng sản phẩm, mẫu mã trên khắp Việt Nam!</p>
                                </div>
                                <div class="col-lg-4 mb-5 mb-lg-0">
                                    <div
                                        class="icon-stack icon-stack-xl bg-gradient-primary-to-secondary text-white mb-4">
                                        <i data-feather="smartphone"></i></div>
                                    <img
                                        src="https://media.istockphoto.com/vectors/piggy-bank-with-coin-symbol-of-new-year-2019-vector-illustration-vector-id1080435824?k=6&m=1080435824&s=612x612&w=0&h=5VdUB_2S7y2MYL70XHnpbgr0Kq5m6C7LjPQNERrA6o0="
                                        width="100" height="100"
                                        style="border-radius: 50%;border: 2px solid;margin-bottom: 2%;" alt="">
                                    <h3 class="mtext-111">Tiết kiệm chi phí</h3>
                                    <p class="mb-0">BISTO giúp nhà bán hàng hướng đến đúng đội tượng khách hàng mục
                                        tiêu, nâng cao doanh số, giảm bớt chi phí bán hàng.!</p>
                                </div>
                                <div class="col-lg-4">
                                    <div
                                        class="icon-stack icon-stack-xl bg-gradient-primary-to-secondary text-white mb-4">
                                        <i data-feather="code"></i></div>
                                    <img
                                        src="https://www.kindpng.com/picc/m/307-3073181_clip-art-clothing-rack-clip-art-hd-png.png"
                                        width="100" height="100"
                                        style="border-radius: 50%;border: 2px solid;margin-bottom: 2%;" alt="">
                                    <h3 class="mtext-111">Thế giới thời trang</h3>
                                    <p class="mb-0">BISTO tạo nên một cộng đồng người dùng về các sản phẩm thời
                                        trang chuyên nghiệp.</p>
                                </div>
                            </div>
                        </div>
                        <div class="svg-border-rounded text-light">
                            <svg style="border-radius: 15px" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 144.54 17.34" preserveAspectRatio="none" fill="currentColor">
                                <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0">
                                </path>
                            </svg>
                        </div>
                    </section>
                    <br>
                    <section class="bg-light py-10">
                        <div class="mb-4">
                            <h2 class="mtext-111 txt-center">Quyền lợi khi tham gia bán hàng cùng BISTO</h2>
                        </div>
                        <div class="container">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-md-9 col-lg-6 order-1 order-lg-0" data-aos="fade-right">
                                    <div class="content-skewed content-skewed-right" style="margin-left: -2%;"><img
                                            style="border-radius: 20px;"
                                            class="content-skewed-item img-fluid shadow-lg rounded-lg"
                                            src="https://cdn.tgdd.vn/Files/2018/02/08/1065580/say-bang-may-co-khien-quan-ao-bac-mau-khong2.jpg"/>
                                    </div>
                                </div>
                                <div class="col-lg-6 order-0 order-lg-1 mb-5 mb-lg-0" data-aos="fade-left">
                                    <div class="row" style="font-family: Roboto, sans-serif;">
                                        <div class="col-md-10 mb-4">
                                            <h6 style="font-size: 16px">1. Phí chiết khấu ưu đãi trên đơn hàng thành
                                                công</h6>
                                        </div>
                                        <div class="col-md-10 mb-4">
                                            <h6 style="font-size: 16px">2. Tham gia quảng cáo cùng BISTO trên các
                                                kênh Marketing</h6>
                                        </div>
                                    </div>
                                    <div class="row" style="font-family: Roboto, sans-serif;">
                                        <div class="col-md-10 mb-4">
                                            <h6 style="font-size: 16px">3. Cơ hội đặt Banner Promotion trên Website
                                                và App Mobile của BISTO</h6>
                                        </div>
                                        <div class="col-md-10 mb-4">
                                            <h6 style="font-size: 16px">4. Đối soát và thanh toán dễ dàng, nhanh
                                                chóng</h6>
                                        </div>
                                    </div>
                                    <div class="row" style="font-family: Roboto, sans-serif;">
                                        <div class="col-md-10 mb-4">
                                            <h6>5. Cùng nhiều ưu đãi khác của BISTO nhằm thu hút người dùng</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <br>
                    <section class="bg-light py-10">
                        <div class="mb-4">
                            <h2 class="mtext-111 txt-center">Hướng dẫn shop sử dụng BCOIN</h2>
                        </div>
                        <div class="container">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-lg-6 order-0 order-lg-1 mb-5 mb-lg-0" data-aos="fade-left">
                                    <div class="row" style="font-family: Roboto, sans-serif;">
                                        <div class="col-md-10 mb-4">
                                            <h6 style="font-size: 16px">Nhận từ Bcoin của người mua khi họ mua hàng</h6>
                                        </div>
                                        <div class="col-md-10 mb-4">
                                            <h6 style="font-size: 16px">Nhận theo các mốc đơn hàng thành công (Giá trị
                                                cộng dồn)</h6>
                                        </div>
                                    </div>
                                    <div class="row" style="font-family: Roboto, sans-serif;">
                                        <div class="col-md-10 mb-4">
                                            <h6 style="font-size: 16px">
                                                + Đạt 100 đơn: 50.000 Bcoin(Gốc)<br>

                                                + Đạt 200 đơn nhận thêm 60.000 Bcoin <br>

                                                + Đạt 500 đơn nhận thêm 100.000 Bcoin <br>

                                                + Đạt 1.000 đơn nhận thêm 200.000 Bcoin <br>

                                                + Đạt 2.000 đơn nhận thêm 300.000 Bcoin <br>

                                                + Đạt 5.000 đơn nhận thêm 500.000 Bcoin
                                            </h6>
                                        </div>
                                        <div class="col-md-10 mb-4">
                                            <h6 style="font-size: 16px">(Tổng giá trị nhận được nếu đạt 5.000 đơn là
                                                1.210.000 Bcoin, Mốc tính trong 1 tháng, sang tháng tiếp theo hoặc vượt
                                                quá 5.000 đơn thì reset đơn về ban đầu)</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-lg-6 order-1 order-lg-0" data-aos="fade-right">
                                    <div class="content-skewed content-skewed-right" style="margin-left: -2%;"><img
                                            style="border-radius: 20px;"
                                            class="content-skewed-item img-fluid shadow-lg rounded-lg"
                                            src="https://images.unsplash.com/photo-1472851294608-062f824d29cc?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8c2hvcHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&w=1000&q=80"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
