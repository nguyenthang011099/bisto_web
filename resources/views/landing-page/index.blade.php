@extends('layout')
@section('app_content')
    <section class="section-slide" style="margin-top: -70px">
        <div class="wrap-slick1 rs2-slick1" id="hook">
            <div class="slick1">
                <div class="item-slick1 bg-overlay1"
                     style="background-image: url({{asset('frontend_landingpage/images/sd-1.jpg')}});"
                     data-thumb="{{asset('frontend_landingpage/images/sd-1.jpg')}}">
                    <div class="container h-full">
                        <div class="flex-col-c-m h-full p-t-100 p-b-60 respon5">
                            <div class="layer-slick1 animated visible-false" data-appear="fadeInDown" data-delay="0">
                                <span class="ltext-201 txt-center cl0 respon2">
                                    BISTO
                                </span>
                            </div>
                            <div class="layer-slick1 animated visible-false" data-appear="fadeInUp" data-delay="800">
                                <h2 class="ltext-104 txt-center cl0 p-t-22 p-b-40 respon1">
                                    Thế giới thời trang của bạn
                                </h2>
                                <h4 style="color: white;text-align: center;">BISTO mong muốn được phục vụ tất cả mọi
                                    người
                                    nhu cầu mua sắm các sản phẩm thời trang tốt nhất và chất lượng nhất.
                                    <br>
                                    Mục tiêu trở thành Sàn giao dịch TMĐT hàng đầu tại Việt Nam, phục vụ người dân Việt
                                    Nam,
                                    của người Việt Nam làm chủ.</h4>
                            </div>
                            <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="1600">
                                <div class="download-app">
                                    <a href="https://play.google.com/store/apps/details?id=com.bisto.geer_fe&fbclid=IwAR2ilvXgPBbtehNZ_7acDmRXLKwO_vliQbpqR3guW6EhkcYYdFwkmZcaX3g"
                                       class="m-all-1 download-btn-app">
                                        <img src="{{asset('frontend_landingpage/images/icons/icon-play-0.png')}}"
                                             width="150px">
                                    </a>
                                    <a href="https://play.google.com/store/apps/details?id=com.bisto.geer_fe&fbclid=IwAR2ilvXgPBbtehNZ_7acDmRXLKwO_vliQbpqR3guW6EhkcYYdFwkmZcaX3g"
                                       class="m-all-1 download-btn-app">
                                        <img src="{{asset('frontend_landingpage/images/icons/icon-play-1.png')}}"
                                             width="150px">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item-slick1 bg-overlay1"
                     style="background-image: url({{asset('frontend_landingpage/images/sd-2.jpg')}});"
                     data-thumb="{{asset('frontend_landingpage/images/sd-2.jpg')}}">
                    <div class="container h-full">
                        <div class="flex-col-c-m h-full p-t-100 p-b-60 respon5">
                            <div class="layer-slick1 animated visible-false" data-appear="rollIn" data-delay="0">
                                <span class="ltext-201 txt-center cl0 respon2">
                                    Bisto
                                </span>
                            </div>
                            <div class="layer-slick1 animated visible-false" data-appear="lightSpeedIn"
                                 data-delay="800">
                                <h2 class="ltext-104 txt-center cl0 p-t-22 p-b-40 respon1">
                                    Thay đổi thói quen mua sắm
                                </h2>
                                <h4 style="color: white;text-align: center;">
                                    Chúng tôi luôn nỗ lực để cuộc sống của bạn trở nên tiện lợi hơn, vui vẻ hơn, hạnh
                                    phúc
                                    hơn.
                                    <br> Chúng tôi sẽ làm những điều tốt nhất tới người dùng, hạnh phúc của bạn cũng là
                                    hạnh
                                    phúc của chúng tôi.
                                </h4>
                            </div>
                            <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="1600">
                                <div class="download-app">
                                    <a href="https://play.google.com/store/apps/details?id=com.bisto.geer_fe&fbclid=IwAR2ilvXgPBbtehNZ_7acDmRXLKwO_vliQbpqR3guW6EhkcYYdFwkmZcaX3g"
                                       class="m-all-1 download-btn-app">
                                        <img src="{{asset('frontend_landingpage/images/icons/icon-play-0.png')}}"
                                             width="150px">
                                    </a>
                                    <a href="https://play.google.com/store/apps/details?id=com.bisto.geer_fe&fbclid=IwAR2ilvXgPBbtehNZ_7acDmRXLKwO_vliQbpqR3guW6EhkcYYdFwkmZcaX3g"
                                       class="m-all-1 download-btn-app">
                                        <img src="{{asset('frontend_landingpage/images/icons/icon-play-1.png')}}"
                                             width="150px">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item-slick1 bg-overlay1"
                     style="background-image: url({{asset('frontend_landingpage/images/sd-3.jpg')}});"
                     data-thumb="{{asset('frontend_landingpage/images/sd-3.jpg')}}">
                    <div class="container h-full">
                        <div class="flex-col-c-m h-full p-t-100 p-b-60 respon5">
                            <div class="layer-slick1 animated visible-false" data-appear="rollIn" data-delay="0">
                                <span class="ltext-201 txt-center cl0 respon2">
                                    Bisto
                                </span>
                            </div>
                            <div class="layer-slick1 animated visible-false" data-appear="lightSpeedIn"
                                 data-delay="800">
                                <h2 class="ltext-104 txt-center cl0 p-t-22 p-b-40 respon1">
                                    Câu chuyện BISTO
                                </h2>
                                <h4 style="color: white;text-align: center;">
                                    Những điều tốt đẹp và may mắn luôn đến với những người tự tin, và phong cách riêng
                                    của
                                    mỗi người cũng góp phần tạo nên sự tự tin đó.
                                    <br>Chúng tôi, đội ngũ BISTO sẽ đồng hành cùng bạn xây dựng phong cách thời trang
                                    riêng
                                    theo cá tính của bạn. Với những sản phẩm thời trang chất lượng đến từ các thương
                                    hiệu
                                    nổi tiếng trong nước và thế giới, bạn sẽ có đa dạng lựa chọn với những trải nghiệm
                                    khiến
                                    bạn hài lòng và thích thú nhất.
                                    <br>BISTO quyết tâm kiến tạo ra những chuẩn mực mới về chất lượng, BISTO luôn đặt sự
                                    hài
                                    lòng của bạn là mục tiêu trong từng hành động dù là nhỏ nhất. Hơn 90 triệu người dân
                                    Việt Nam là hơn 90 triệu lý do để BISTO nỗ lực, cố gắng hơn nữa nhằm nâng cao chất
                                    lượng
                                    dịch vụ và trải nghiệm tuyệt vời nhất tới mọi người.
                                </h4>
                            </div>
                            <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="1600">
                                <div class="download-app">
                                    <a href="https://play.google.com/store/apps/details?id=com.bisto.geer_fe&fbclid=IwAR2ilvXgPBbtehNZ_7acDmRXLKwO_vliQbpqR3guW6EhkcYYdFwkmZcaX3g"
                                       class="m-all-1 download-btn-app">
                                        <img src="{{asset('frontend_landingpage/images/icons/icon-play-0.png')}}"
                                             width="150px">
                                    </a>
                                    <a href="https://play.google.com/store/apps/details?id=com.bisto.geer_fe&fbclid=IwAR2ilvXgPBbtehNZ_7acDmRXLKwO_vliQbpqR3guW6EhkcYYdFwkmZcaX3g"
                                       class="m-all-1 download-btn-app">
                                        <img src="{{asset('frontend_landingpage/images/icons/icon-play-1.png')}}"
                                             width="150px">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="accueil" class="wow bounceInUp animate__rubberBand ">
        <div class="triangle_rose"></div>
        <div class="triangle_vert"></div>
        <div class="header-position">
            <div class="bg0 m-t-23 p-t-75">
                <div class="container-fluid add-pd" id="Stand2">
                    <div class="p-b-82">
                        <h3 class="ltext-105 cl5 txt-center respon1">
                            Mua sắm trên BISTO bạn sẽ nhận được
                        </h3>
                    </div>
                    <!-- Topic Cards -->
                    <div id="cards_landscape_wrap-2">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <a>
                                        <div class="card-flyer">
                                            <div class="text-box">
                                                <div class="image-box">
                                                    <img src="{{asset('frontend_landingpage/images/content/1.jpg')}}"
                                                         alt=""/>
                                                </div>
                                                <div class="text-container">
                                                    <h6>Cá nhân hóa trải nghiệm mua sắm Online</h6>
                                                    <p></p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <a>
                                        <div class="card-flyer">
                                            <div class="text-box">
                                                <div class="image-box">
                                                    <img src="{{asset('frontend_landingpage/images/content/2.jpg')}}"
                                                         alt=""/>
                                                </div>
                                                <div class="text-container">
                                                    <h6>Sản phẩm đa dạng</h6>
                                                    <p>Đầy đủ các sản phẩm từ các thương hiệu nổi tiếng.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <a>
                                        <div class="card-flyer">
                                            <div class="text-box">
                                                <div class="image-box">
                                                    <img src="{{asset('frontend_landingpage/images/content/3.jpg')}}"
                                                         alt=""/>
                                                </div>

                                                <div class="text-container">
                                                    <h6>Cập nhật phong cách mới cho mọi người</h6>
                                                    <p>Tùy chọn phong cách phù hợp theo sở thích của bạn.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <a>
                                        <div class="card-flyer">
                                            <div class="text-box">
                                                <div class="image-box">
                                                    <img src="{{asset('frontend_landingpage/images/content/4.jpg')}}"
                                                         alt=""/>
                                                </div>
                                                <div class="text-container">
                                                    <h6>Bisto mang tới những sản phẩm tốt nhất và chất lượng nhất</h6>
                                                    <p>Ưu tiên bảo vệ quyền lợi của người tiêu dùng.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <a>
                                        <div class="card-flyer">
                                            <div class="text-box">
                                                <div class="image-box">
                                                    <img src="{{asset('frontend_landingpage/images/content/5.jpg')}}"
                                                         alt=""/>
                                                </div>
                                                <div class="text-container">
                                                    <h6>Gợi ý các sản phẩm – giao hàng nhanh chóng</h6>
                                                    <p>Hiển thị các thương hiệu và sản phẩm gần bạn – Giao hàng nhanh
                                                        trong
                                                        30-40 phút.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <a>
                                        <div class="card-flyer">
                                            <div class="text-box">
                                                <div class="image-box">
                                                    <img src="{{asset('frontend_landingpage/images/content/6.jpg')}}"
                                                         alt=""/>
                                                </div>

                                                <div class="text-container">
                                                    <h6>Nhiều ưu đãi hấp dẫn tới từ BISTO và các Đối tác của BISTO.</h6>
                                                    <p></p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid add-pd" id="Stand2">
                    <div class="p-b-82">
                        <h3 class="ltext-105 cl5 txt-center respon1">
                            Danh mục sản phẩm
                        </h3>
                    </div>
                    <div class="un-grid">
                        <div class="wow bounceInLeft wom constent1" data-wow-duration="1s" data-wow-delay="0.5s">
                            <div class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                <div class="block1-txt-child2 p-b-4 trans-05">
                                    <div class="block1-link ltext-101 cl0 trans-09">
                                        <h1 style="font-weight: bold;">Áo</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" wow slideInLeft wom constent2" data-wow-duration="1.2s">
                            <div class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                <div class="block1-txt-child2 p-b-4 trans-05">
                                    <div class="block1-link ltext-101 cl0 trans-09">
                                        <h1 style="font-weight: bold;">Quần</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" wow slideInRight wom constent3" data-wow-duration="1.2s">
                            <div class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                <div class="block1-txt-child2 p-b-4 trans-05">
                                    <div class="block1-link ltext-101 cl0 trans-09">
                                        <h1 style="font-weight: bold;">Váy</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wow bounceInRight wom constent4" data-wow-duration="1.8s" data-wow-delay="0.5s">
                            <div class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                <div class="block1-txt-child2 p-b-4 trans-05">
                                    <div class="block1-link ltext-101 cl0 trans-09">
                                        <h1 style="font-weight: bold;">Đồ bầu</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wow bounceInUp wom constent5" data-wow-duration="1.9s" data-wow-delay="0.8s">
                            <div class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                <div class="block1-txt-child2 p-b-4 trans-05">
                                    <div class="block1-link ltext-101 cl0 trans-09">
                                        <h1 style="font-weight: bold;">Giày dép</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wow bounceInUp wom constent6" data-wow-duration="2.0s" data-wow-delay="0.8s">
                            <div class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                <div class="block1-txt-child2 p-b-4 trans-05">
                                    <div class="block1-link ltext-101 cl0 trans-09">
                                        <h1 style="font-weight: bold;">Túi xách</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wow bounceInUp wom constent7" data-wow-duration="2.1s" data-wow-delay="0.8s">
                            <div class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                <div class="block1-txt-child2 p-b-4 trans-05">
                                    <div class="block1-link ltext-101 cl0 trans-09">
                                        <h1 style="font-weight: bold;">Phụ kiện</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wow bounceInUp wom constent8" data-wow-duration="2.2s" data-wow-delay="0.8s">
                            <div class="block1-txt ab-t-l s-full flex-col-l-sb p-lr-38 p-tb-34 trans-03 respon3">
                                <div class="block1-txt-child2 p-b-4 trans-05">
                                    <div class="block1-link ltext-101 cl0 trans-09">
                                        <h1 style="font-weight: bold;">Khác</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-slide p-b-100" id="Stand4">
        <div class="wrap-slick1">
            <div class="slick1">
                <div class="item-slick1"
                     style="background-image: url({{asset('frontend_landingpage/images/slide-01.jpg')}});">
                    <div class="container h-full">
                        <div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">
                            <div class="layer-slick1 animated visible-false" data-appear="fadeInDown" data-delay="0">
                                <h1 class="cl2 ltext-201">Tải ứng dụng</h1>
                                <span class="ltext-101 cl2 respon2">
                                    <p class="ltext-101 cl2 respon2"> để trải nghiệm dịch vụ của chúng tôi</p>
                                </span>
                            </div>
                            <div class="layer-slick1 animated visible-false" data-appear="fadeInUp" data-delay="800">
                                <h2 class="ltext-101 cl2 p-t-19 p-b-43 respon1">
                                    <a
                                        href="https://play.google.com/store/apps/details?id=com.bisto.geer_fe&fbclid=IwAR2ilvXgPBbtehNZ_7acDmRXLKwO_vliQbpqR3guW6EhkcYYdFwkmZcaX3g">
                                        <img src="{{asset('frontend_landingpage/images/icons/icon-play-0.png')}}"
                                             width="200px" alt=""><br>

                                    </a>
                                    <a
                                        href="https://play.google.com/store/apps/details?id=com.bisto.geer_fe&fbclid=IwAR2ilvXgPBbtehNZ_7acDmRXLKwO_vliQbpqR3guW6EhkcYYdFwkmZcaX3g">
                                        <img src="{{asset('frontend_landingpage/images/icons/icon-play-1.png')}}"
                                             width="200px" alt="">

                                    </a>
                                </h2>
                            </div>
                            <div class="layer-slick1 animated visible-false" data-appear="fadeInUp" data-delay="800">
                                <table style="text-align: center;">
                                    <tr>
                                        <td style="font-size: 35px;color: #ffbb00">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </td>
                                        <td>&emsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="ltext-101 cl2 respon2">Đánh giá ứng dụng</td>
                                        <td>&emsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item-slick1"
                     style="background-image: url({{asset('frontend_landingpage/images/slider-3.jpg')}});">
                    <div class="container h-full">
                        <div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">
                            <div class="layer-slick1 animated visible-false" data-appear="fadeInDown" data-delay="0">
                                <h1 class="cl2 ltext-201">Tải ứng dụng</h1>
                                <span class="ltext-101 cl2 respon2">
                                    <p class="ltext-101 cl2 respon2"> để trải nghiệm dịch vụ của chúng tôi</p>
                                </span>
                            </div>
                            <div class="layer-slick1 animated visible-false" data-appear="fadeInUp" data-delay="800">
                                <h2 class="ltext-101 cl2 p-t-19 p-b-43 respon1">
                                    <a
                                        href="https://play.google.com/store/apps/details?id=com.bisto.geer_fe&fbclid=IwAR2ilvXgPBbtehNZ_7acDmRXLKwO_vliQbpqR3guW6EhkcYYdFwkmZcaX3g">
                                        <img src="{{asset('frontend_landingpage/images/icons/icon-play-0.png')}}"
                                             width="200px" alt=""><br>
                                    </a>
                                    <a
                                        href="https://play.google.com/store/apps/details?id=com.bisto.geer_fe&fbclid=IwAR2ilvXgPBbtehNZ_7acDmRXLKwO_vliQbpqR3guW6EhkcYYdFwkmZcaX3g">
                                        <img src="{{asset('frontend_landingpage/images/icons/icon-play-1.png')}}"
                                             width="200px" alt="">
                                    </a>
                                </h2>
                            </div>
                            <div class="layer-slick1 animated visible-false" data-appear="fadeInUp" data-delay="800">
                                <table style="text-align: center;">
                                    <tr>
                                        <td style="font-size: 35px;color: #ffbb00">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </td>
                                        <td>&emsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="ltext-101 cl2 respon2">Đánh giá ứng dụng</td>
                                        <td>&emsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-slide p-b-100 container" id="Stand4" style="margin-bottom: -155px;margin-top: -70px;">
        <div class="container">
            <div class="wrap-slick1 row">
                <div class="col-1"></div>
                <div class="col-6 shop">
                    <h3 class="ltext-105 cl5 txt-center respon1" style="margin-bottom: 40px">
                        Hướng dẫn người dùng sử dụng Bcoin
                    </h3>
                    <ul>
                        <li class="ltext-101 cl2 respon2"><i class="fas fa-arrow-circle-down"></i> Tải App và đăng ký
                            tài khoản nhận: 5.000 Bcoin
                        </li>
                        <li class="ltext-101 cl2 respon2"><i class="fas fa-sign-in-alt"></i> Đăng nhập mỗi ngày nhận:
                            200 Bcoin
                        </li>
                        <li class="ltext-101 cl2 respon2"><i class="fas fa-sign-in-alt"></i> Đăng nhập 7 ngày liên tiếp
                            nhận thêm: 500 Bcoin
                        </li>
                        <li class="ltext-101 cl2 respon2"><i class="fas fa-shopping-cart"></i> Mua hàng thành công nhận:
                            100 Bcoin/1 đơn
                        </li>
                        <li class="ltext-101 cl2 respon2"><i class="fas fa-share"></i> Chia sẻ Bisto lên facebook nhận:
                            200 Bcoin (1 ngày đc share 1 lần)
                        </li>
                        <li class="ltext-101 cl2 respon2"><i class="fas fa-user-friends"></i> Mời bạn bè tải app và đăng
                            ký: mời được 1 người nhận 200 Bcoin.
                        </li>
                    </ul>
                </div>
                <div class="col-4 shop">
                    <h3 class="ltext-105 cl5 txt-center respon1" style="margin-bottom: 40px">
                        Cách sử dụng Bcoin
                    </h3>
                    <ul>
                        <li class="ltext-101 cl2 respon2">Dùng để mua hàng (trừ tiền vào giá trị đơn hàng, chưa tính giá
                            ship)
                        </li>
                        <li class="ltext-101 cl2 respon2">Đổi trả hoàn tiền, cộng số tiền hoàn vào điểm Bcoin cho người
                            dùng
                        </li>
                        <li class="ltext-101 cl2 respon2">Thực hiện mua bán trên góc chuyển nhượng, bán hàng thành công
                            thì giá trị đơn hàng chuyển thành điểm thưởng Bcoin sẽ được cộng vào tài khoản của người bán
                        </li>
                    </ul>
                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="p-b-82">
            <h3 class="ltext-105 cl5 txt-center respon1"></h3>
        </div>
    </div>
    <section class="wow slideInRight bg-img1 txt-center p-lr-15 p-tb-92 p-b-52 hovereffect" data-wow-duration="1.2s"
             style="background-image: url('{{asset('frontend_landingpage/images/background-img.png')}}');">
        <div class="overlay">
            <h3 class="ltext-105 cl0 txt-center">
                Phong phú – Đa dạng
            </h3>
        </div>
    </section>
    <section class=" wow slideInLeft bg-img1 txt-center p-lr-15 p-tb-92 p-b-52 hovereffect" data-wow-duration="1.4s"
             data-wow-delay="0.5s"
             style="background-image: url('{{asset('frontend_landingpage/images/lalamove.jpg')}}');">
        <div class="overlay">
            <h3 class="ltext-105 cl0 txt-center">
                Tiện lợi - Nhanh chóng
            </h3>
        </div>
    </section>
    <section class="wow slideInRight  bg-img1 txt-center p-lr-15 p-tb-92 p-b-52 hovereffect" data-wow-duration="1.6s"
             data-wow-delay="1s"
             style="background-image: url('{{asset('frontend_landingpage/images/page-sale.jpg')}}');">
        <div class="overlay">
            <h3 class="ltext-105 cl0 txt-center">
                Tiết kiệm - Nhiều ưu đãi
            </h3>
        </div>
    </section>
    <div class="container">
        <div class="line-foot">
        </div>
    </div>
@endsection
