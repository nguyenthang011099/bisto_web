@extends('layout')
@section('app_content')
    <div class="container">
        <div class="row">
            <div class="order-md-2 p-b-30">
                <div class="p-t-7 p-l-85 p-l-15-lg p-l-0-md">
                    <h3 class="mtext-111 cl2 p-b-16 txt-center">
                        TIÊU CHUẨN CỘNG ĐỒNG
                    </h3>
                    <p>
                        Chúng tôi muốn xây dựng cộng đồng BISTO thân thiện và tích cực! Tiêu chuẩn này được đưa ra để
                        hướng dẫn người bán và người mua gìn giữ một thị trường mua bán an toàn. Vui lòng đọc những việc
                        nên và không nên làm dưới đây khi sử dụng ứng dụng BISTO. Bằng việc dùng ứng dụng này, bạn đồng
                        ý với các <a href="#">Điều khoản sử dụng</a> của chúng tôi. Chúng tôi cam kết nỗ lực gìn giữ sự
                        an toàn cho cộng đồng mua bán này.
                    </p>
                    <h3 class="mtext-111 cl2 p-b-16 txt-center">
                        NHỮNG VIỆC NÊN LÀM
                    </h3>
                    <h4 class="mtext-111 cl2 p-b-16">
                        1. Bán hàng thật, không quảng cáo
                    </h4>
                    <p>
                        BISTO là ứng dụng hỗ trợ người mua và người bán giao dịch với nhau chứ không phải là nền tảng để
                        quảng cáo. Bạn chỉ nên đăng những sản phẩm mà bạn muốn bán trên BISTO.
                        Dưới đây là một số ví dụ về việc vi phạm quy tắc này:
                    </p>
                    <div class="bor16 p-l-29 p-b-9 m-t-22">
                        <p>
                            - Kết nối trang đăng bán sản phẩm tới một website riêng biệt khác.
                        </p>
                        <p>
                            - Phần mô tả, hình ảnh hoặc tên gọi của sản phẩm chứa thông tin hướng dẫn người mua kết nối
                            với người bán qua một ứng dụng hoặc phương tiện khác.
                        </p>
                        <p>
                            - Đăng bán một sản phẩm lặp đi lặp lại trên cùng một danh mục sản phẩm hoặc tại các danh mục
                            sản phẩm khác nhau (spam)
                        </p>
                        <p>
                            - Kết nối trang đăng bán sản phẩm tới một website riêng biệt khác.
                        </p>
                        <p>
                            - Đăng bán với giá sản phẩm thật thấp để thu hút người mua nhưng thực tế không bán hàng với
                            giá như vậy và không cho người mua đặt hàng với giá như vậy
                        </p>
                    </div>

                    <p>
                        Chúng tôi hiểu rằng người mua và người bán luôn có nhu cầu trao đổi thông tin với nhau. Do vậy,
                        tính năng Trả giá đã được tích hợp trong BISTO để hỗ trợ tối đa mọi giao dịch của người dùng.
                    </p>
                    <h4 class="mtext-111 cl2 p-b-16">
                        2. Làm thế nào để sản phẩm của bạn nổi bật
                    </h4>
                    <p>
                        Người mua sẽ chú ý ngay nếu sản phẩm của bạn có ảnh hấp dẫn. Bên cạnh đó, hãy là người bán hàng
                        chân thật, mô tả sản phẩm thật chính xác và chi tiết vì khách hàng luôn muốn biết rõ về sản phẩm
                        mà họ mua. Nhờ đó, bạn sẽ nhận được những đánh giá tích cực từ họ!
                    </p>
                    <h4 class="mtext-111 cl2 p-b-16">
                        3. Tôn trọng lẫn nhau
                    </h4>
                    <p>
                        Chúng tôi muốn xây dựng một cộng đồng an toàn và văn minh để mọi người cùng tận hưởng niềm vui
                        mua sắm trên di động. Vì vậy, hãy đăng tải những nội dung lành mạnh và phù hợp với tất cả mọi
                        người; không sử dụng ngôn ngữ thô tục, đăng các nội dung xấu hay làm phiền người khác. Trong
                        cộng đồng BISTO, người dùng phải tôn trọng lẫn nhau và cư xử có thiện chí giữa người mua và
                        người bán, cũng như giữa người dùng và BISTO. Dù bạn mua hay bán hàng trên BISTO, hãy tuân thủ
                        những quy tắc dưới đây để đảm bảo mọi người cùng được mua sắm dễ dàng!
                    </p>
                    <h4 class="mtext-111 cl2 p-b-16">
                        4. Giữ “lịch sử bán hàng” tốt
                    </h4>
                    <p>
                        Để tạo ra trải nghiệm tốt cho khách hàng, bạn nên tìm hiểu và đáp ứng những nhu cầu của Người
                        mua. Khi Người mua cảm thấy hài lòng, họ sẽ tiếp tục mua hàng và đánh giá tích cực cho Shop bạn.
                    </p>
                    <p>
                        Để có lịch sử bán hàng tốt, bạn đừng quên duy trì đủ lượng hàng, giao hàng đúng hẹn và giảm
                        thiểu các trường hợp hủy, trả đơn hàng và hoàn tiền không cần thiết. Để đảm bảo trải nghiệm tốt
                        cho khách hàng, BISTO có thể gửi cảnh báo đến những Người bán có lịch sử bán hàng xấu. Trong một
                        vài trường hợp, BISTO cũng có thể loại bỏ một số đặc quyền của Người bán (VD: miễn phí vận
                        chuyển, Shop Uy Tín), giảm thứ hạng tìm kiếm, giới hạn hoặc tạm ngưng quyền truy cập của bạn.
                    </p>
                    <p>
                        Một số mẹo để có lịch sử bán hàng tốt: <br>
                    <div class="bor16 p-l-29 p-b-9 m-t-22">
                        - Quản lý và cập nhật nguồn hàng của bạn ít nhất 1 lần/ngày <br>
                        - Cung cấp thông tin mô tả chi tiết và ảnh sản phẩm trung thực. <br>
                        - Trả lời các câu hỏi của Người mua trong thời gian sớm nhất <br>
                        - Đảm bảo chất lượng hàng hóa trước khi vận chuyển <br>
                        - Đóng gói hàng hóa cẩn thận, đề phòng các hư hại trong quá trình vận chuyển <br>
                        - Trả lời nhanh chóng các yêu cầu hoàn tiền <br>
                        - Nếu bạn cần hủy các đơn hàng vì các trường hợp bất khả kháng, cần thông báo cho Người mua ngay
                        lập tức với lý do thuyết phục. <br>
                        - Quản lý và cập nhật nguồn hàng của bạn ít nhất 1 lần/ngày <br>
                        - Cung cấp thông tin mô tả chi tiết và ảnh sản phẩm trung thực. <br>
                        - Trả lời các câu hỏi của Người mua trong thời gian sớm nhất <br>
                        - Đảm bảo chất lượng hàng hóa trước khi vận chuyển <br>
                        - Đóng gói hàng hóa cẩn thận, đề phòng các hư hại trong quá trình vận chuyển <br>
                        - Trả lời nhanh chóng các yêu cầu hoàn tiền <br>
                        - Nếu bạn cần hủy các đơn hàng vì các trường hợp bất khả kháng, cần thông báo cho Người mua ngay
                        lập tức với lý do thuyết phục.
                    </div>

                    </p>
                    <h3 class="mtext-111 cl2 p-b-16 txt-center">
                        NHỮNG VIỆC KHÔNG NÊN LÀM
                    </h3>
                    <h4 class="mtext-111 cl2 p-b-16">
                        1. Mạo danh người khác
                    </h4>
                    <p>
                        Mạo danh người khác để lừa đảo dưới bất kỳ hình thức nào bị xem là hành vi vi phạm nghiêm trọng
                        Tiêu chuẩn cộng đồng của BISTO. Nếu bạn sử dụng hình ảnh của người khác, vui lòng tôn trọng bản
                        quyền và dẫn nguồn khi cần thiết. Một số ví dụ về hành vi mạo danh:
                    </p>
                    <div class="bor16 p-l-29 p-b-9 m-t-22">
                        <p>
                            - Sử dụng thông tin cá nhân của người khác (thông tin liên lạc, hình ảnh, v.v...) để tạo tài
                            khoản, đặt hàng, bán hàng trên BISTO mà người đó không biết hoặc không cho phép.
                        </p>
                        <p>
                            - Sử dụng danh tính của người khác với mục đích giả mạo và lừa đảo.
                        </p>
                    </div>

                    <p>
                        Hãy chung tay xây dựng cộng đồng BISTO lành mạnh! Nếu thấy có người dùng mạo danh bạn hay người
                        khác, vui lòng <a href="#">liên hệ </a> với chúng tôi.
                    </p>
                    <h4 class="mtext-111 cl2 p-b-16">
                        2. Đăng bán hàng giả, hàng nhái
                    </h4>
                    <p>Chỉ đăng bán các mặt hàng chính hãng trên BISTO. BISTO nghiêm cấm việc đăng bán hàng giả, hàng
                        nhái bởi đó là hành vi vi phạm pháp luật. BISTO có quyền báo cáo và xóa các mặt hàng này.</p>
                    <h4 class="mtext-111 cl2 p-b-16">
                        3. Tên sản phẩm đăng bán không phù hợp
                    </h4>
                    <p>
                        - Tên sản phẩm không rõ ràng, viết tắt dễ gây hiểm nhầm, không thể hiện sản phẩm đăng bán
                    </p>
                    <p>
                        - Tên sản phẩm chứa ký tự lạ: @, !, ^, {, [, …
                    </p>
                    <p>
                        - Tên sản phẩm có chứa thông tin quảng cáo.
                    </p>
                    <p>
                        - Tên sản phẩm được lặp lại rất nhiều lần, hoặc chứa hashtags, từ khóa, tên các nhãn hiệu và
                        những thông tin không liên quan.
                    </p>
                    <p>
                        - Tên và mô tả sản phẩm bị thay đổi toàn bộ thành một sản phẩm khác, nhằm lợi dụng đánh giá hoặc
                        bình luận tốt của người mua dành cho sản phẩm trước đó.
                    </p>
                    <p class="mtext-101 cl2 p-b-16">
                        Ví dụ minh họa cho tên sản phẩm không phù hợp:
                    </p>
                    <div class="bor16 p-l-29 p-b-9 m-t-22">
                        <p>
                            a) Trong tên sản phẩm son môi của The Face Shop xuất hiện tên của các nhãn hiệu khác như:
                            Dior, L'Oreal, Maybeline, Channel
                        </p>
                        <p>
                            b) Tên sản Apple iPhone 6s được ghi như sau: <br>
                            Apple iPhone 6s Apple iPhone 6s Apple iPhone 6s Apple iPhone 6s Apple iPhone 6s
                        </p>
                    </div>
                    <h4 class="mtext-111 cl2 p-b-16">
                        4. Giao dịch không hợp lệ trên BISTO
                    </h4>
                    <p>- Người bán tự tạo tài khoản và đặt hàng của chính mình nhằm lợi dụng các chương trình của
                        BISTO.</p>
                    <p>- Người bán đặt hộ và người mua không biết gì về BISTO</p>
                    <p>- Người bán đặt hộ người mua nhiều lần.</p>
                    <p>- Một người dùng tạo nhiều tài khoản để được hưởng nhiều lần đối với một chương trình của
                        BISTO</p>
                    <p>- Người bán và người mua tự thỏa thuật tăng giá sản phẩm để trục lợi phí vận chuyển, mã giảm giá
                        hoặc lạm dụng các chương trình khuyến mãi khác của BISTO.</p>
                    <p>- Người bán hoặc người mua tách nhỏ đơn hàng số lượng lớn/ mua sỉ thành nhiều đơn nhỏ để lạm dụng
                        chương trình miễn phí vận chuyển của BISTO</p>
                    <p>- Các trường hợp khác mà hệ thống của BISTO phát hiện được.</p>
                    <p>Tùy từng trường hợp, BISTO sẽ có biện pháp xử lý thích hợp, bao gồm nhưng không hạn chế khóa tài
                        khoản mà không cần báo trước.</p>


                    <h4 class="mtext-111 cl2 p-b-16">
                        5. Xây dựng chính sách hoàn trả không phù hợp
                    </h4>
                    <p>Người bán có quyền xây dựng chính sách hoàn trả riêng nếu không mâu thuẫn với chính sách hoàn trả
                        hiện thời của BISTO.</p>
                    <p>Mọi người dùng đều đóng vai trò quan trọng trong việc gìn giữ sự an toàn cho cộng đồng BISTO. Đây
                        là môi trường cởi mở để mọi người cùng mua bán dễ dàng. Hãy tôn trọng lẫn nhau và tạo nên cộng
                        đồng mua bán trên di động tuyệt vời nhất. Nếu phát hiện người dùng vi phạm các quy tắc này, bạn
                        hãy thông báo cho BISTO bằng cách <a href="#">Tố cáo người dùng/ sản phẩm</a>. Tuy nhiên, chúng
                        tôi không đảm bảo người dùng hay sản phẩm này sẽ bị xóa và quyền quyết định cuối cùng thuộc về
                        BISTO. </p>
                    <p>Chúng tôi mong nhận được phản hồi và sẵn sàng tiếp thu mọi ý kiến của bạn. Hãy liên hệ với chúng
                        tôi khi có bất cứ yêu cầu hay đề xuất nào nhằm tạo nên trải nghiệm mua bán trên di động tuyệt
                        vời nhất. </p>


                    <h4 class="mtext-111 cl2 p-b-16">
                        6. Lợi dụng chính sách hỗ trợ của BISTO và gian lận
                    </h4>
                    <p>
                        - Mã giảm giá và các chương trình trợ giá của BISTO đều nhằm hỗ trợ Người bán bán hàng hiệu quả.
                        Vì vậy, BISTO nghiêm cấm các hành vi sau:
                    </p>
                    <ul class="bor16 p-l-29 p-b-9 m-t-22">
                        <li>+ Tạo đơn hàng giả</li>
                        <li>+ Lợi dụng Mã giảm giá để trục lợi</li>
                        <li>+ Lợi dụng các chương trình trợ giá</li>
                        <li>+ Gian lận để được hưởng chênh lệch</li>
                    </ul>
                    <p>
                        - BISTO sẽ kiểm tra để đảm bảo Người dùng tuân theo đúng Tiêu chuẩn Cộng đồng. Nếu vi phạm tiêu
                        chuẩn này, Người dùng có thể bị:
                    </p>
                    <ul class="bor16 p-l-29 p-b-9 m-t-22">
                        <li>
                            + Khóa tài khoản vĩnh viễn
                        </li>
                        <li>+ Đóng băng số tiền đang được BISTO Đảm bảo để xác minh các đơn hàng</li>
                        <li>+ Truy cứu trách nhiệm dân sự hoặc yêu cầu bối thường thiệt hại</li>
                    </ul>
                    <p>
                        - Trong trường hợp BISTO nhận được thông báo về hành vi gian lận hoặc có dấu hiệu nghi ngờ gian
                        lận từ tài khoản của bạn, BISTO sẽ tạm giữ dư nợ trong BISTO đảm bảo của bạn trong 14 ngày (hoặc
                        có thể lâu hơn) để phục vụ việc điều tra. Bạn sẽ không sử dụng được tính năng “Rút tiền” và
                        “Giao dịch” của ví BISTO cho đến khi BISTO hoàn tất quá trình điều tra.
                    </p>
                    <h4 class="mtext-111 cl2 p-b-16">
                        7. Tạo một sản phẩm rất nhiều lần
                    </h4>

                    <p>
                        Nếu vi phạm tiêu chuẩn này, hệ thống sẽ tự động khóa sản phẩm
                    </p>


                    <h4 class="mtext-111 cl2 p-b-16">
                        8. Đăng giá sản phẩm không chính xác
                    </h4>
                    <p>
                        Hệ thống sẽ tự động khóa sản phẩm trong trường hợp giá được đăng rất thấp nhưng chất lượng sản
                        phẩm không được đảm bảo hoặc shop không có ý định bán sản phẩm đó.
                    </p>
                    <h4 class="mtext-111 cl2 p-b-16">
                        9. Sử dụng ngôn ngữ thô tục, có nội dung không lành mạnh
                    </h4>
                    <p>BISTO sẽ có hình thức xử phạt cũng như thông báo cho mỗi lần vi phạm.</p>
                </div>
            </div>
        </div>
    </div>
@endsection
