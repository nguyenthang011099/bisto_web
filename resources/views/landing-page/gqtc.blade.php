@extends('layout')
@section('app_content')
    <div class="container">
        <div class="row">
            <div class="order-md-2 p-b-30">
                <div class="p-t-7 p-l-85 p-l-15-lg p-l-0-md">
                    <h3 class="mtext-111 cl2 p-b-16 txt-center">
                        CƠ CHẾ GIẢI QUYẾT TRANH CHẤP
                    </h3>
                    <p>
                        BISTO và người bán có trách nhiệm tiếp nhận khiếu nại và hỗ trợ người mua liên quan đến giao
                        dịch tại ứng dụng BISTO.
                    </p>
                    <p>
                        Khi phát sinh tranh chấp, chúng tôi đề cao giải pháp thương lượng, hòa giải giữa các bên nhằm
                        duy trì sự tin cậy của thành viên vào chất lượng dịch vụ của BISTO và thực hiện theo các bước
                        sau:
                    </p>
                    <ul class="bor16 p-l-29 p-b-9 m-t-22">
                        <li>• Bước 1: Thành viên người mua khiếu nại về hàng hóa của người bán qua email hoặc hotline
                            hoặc tại trụ sở của BISTO
                        </li>
                        <li>• Bước 2: Bộ phận Chăm Sóc Khách Hàng của BISTO sẽ tiếp nhận các khiếu nại , làm rõ các yêu
                            cầu của khách hàng trong thời gian sớm nhất không quá 3 ngày làm việc kể từ khi nhận được
                            yêu cầu. Theo tính chất và mức độ của khiếu nại thì bên BISTO sẽ có những biện pháp cụ thể
                            hỗ trợ người mua để giải quyết tranh chấp đó.
                        </li>
                        <li>• Bước 3: BISTO yêu cầu người mua và người bán phải cung cấp đủ thông tin liên quan đến giao
                            dịch, hàng hóa để xác minh làm rõ vụ việc và có hướng xử lý thích hợp.
                        </li>
                        <li>• Bước 4: BISTO làm trọng tài phân xử các vấn đề liên quan đến giao dịch nếu một trong hai
                            bên có yêu cầu
                        </li>
                        <li>• Bước 5: Trong trường nằm ngoài khả năng và thẩm quyền của BISTO thì ban quản trị sẽ yêu
                            cầu người mua đưa vụ việc này ra cơ quan nhà nước có thẩm quyền giải quyết theo pháp luật.
                        </li>
                    </ul>
                    <div>
                        Người mua gửi khiếu nại tại địa chỉ:
                        <ul class="bor16 p-l-29 p-b-9 m-t-22">
                            <li>Công ty TNHH Anberry Việt Nam</li>
                            <li>Địa chỉ: Tầng 8 Số 82 Vương Thừa Vũ, phường Khương Trung, quận Thanh Xuân, thành phố Hà
                                Nội
                            </li>
                            <li>Hotline: 0963 228 283</li>
                            <li>Email: Bistovn@gmail.com hoặc Anberry.Vn@gmail.com</li>
                            <li>Thời gian tiếp nhận và xử lý phản ánh là 3 (ba) ngày làm việc kể từ thời điểm Công ty
                                nhận được phản ánh từ phía người dùng.
                            </li>
                        </ul>
                    </div>
                    <p>Thời gian tiếp nhận và xử lý phản ánh là 3 (ba) ngày làm việc kể từ thời điểm Công ty nhận được
                        phản ánh từ phía người dùng.</p>
                    <p>BISTO tôn trọng và nghiêm túc thực hiện các quy định của pháp luật về bảo vệ quyền lợi của người
                        mua (người tiêu dùng). Vì vậy, đề nghị các thành viên bán sản phẩm, dịch vụ trên sàn cung cấp
                        đầy đủ, chính xác, trung thực và chi tiết các thông tin liên quan đến sản phẩm, dịch vụ. Mọi
                        hành vi lừa đảo, gian lận trong kinh doanh đều bị lên án và phải chịu hoàn toàn trách nhiệm
                        trước pháp luật.</p>
                    <p> Các bên bao gồm người bán, người mua sẽ phải có vai trò trách nhiệm trong việc tích cực giải
                        quyết vấn đề. Đối với người bán hàng cần có trách nhiệm cung cấp văn bản giấy tờ chứng thực
                        thông tin liên quan đến sự việc đang gây mâu thuẫn cho khách hàng. Đối với BISTO sẽ có trách
                        nhiệm cung cấp những thông tin liên quan đến người mua và người bán nếu được người mua hoặc
                        người bán (liên quan đến tranh chấp đó) yêu cầu hoặc trong phạm vi được luật pháp cho phép.</p>
                    <p> Các bên bao gồm người bán, người mua sẽ phải có vai trò trách nhiệm trong việc tích cực giải
                        quyết vấn đề. Đối với người bán hàng cần có trách nhiệm cung cấp văn bản giấy tờ chứng thực
                        thông tin liên quan đến sự việc đang gây mâu thuẫn cho khách hàng. Đối với BISTO sẽ có trách
                        nhiệm cung cấp những thông tin liên quan đến người mua và người bán nếu được người mua hoặc
                        người bán (liên quan đến tranh chấp đó) yêu cầu hoặc trong phạm vi được luật pháp cho phép.
                        Sau khi người bán, người mua đã giải quyết xong tranh chấp phải có trách nhiệm báo lại cho ban
                        quản trị BISTO. Trong trường hợp giao dịch phát sinh mâu thuẫn mà lỗi thuộc về người bán: BISTO
                        sẽ có biện pháp cảnh cáo, khóa tài khoản hoặc chuyển cho cơ quan pháp luật có thẩm quyền tùy
                        theo mức độ của sai phạm. BISTO sẽ chấm dứt và gỡ bỏ toàn bộ tin bài về sản phẩm, dịch vụ của
                        người bán đó trên BISTO đồng thời yêu cầu người bán bồi hoàn cho khách hàng thỏa đáng trên cơ sở
                        thỏa thuận với khách hàng.
                    </p>
                    <p>Nếu thông qua hình thức thỏa thuận mà vẫn không thể giải quyết được mâu thuẫn phát sinh từ giao
                        dịch giữa 2 bên người mua, người bán, thì một trong 2 bên người mua và người bán sẽ có quyền nhờ
                        đến cơ quan pháp luật có thẩm quyền can thiệp nhằm đảm bảo lợi ích hợp pháp của các bên nhất là
                        cho khách hàng.</p>
                </div>
            </div>
        </div>
    </div>
@endsection
