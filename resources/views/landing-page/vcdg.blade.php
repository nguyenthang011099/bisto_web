@extends('layout')
@section('app_content')
    <div class="container">
        <div class="row">
            <div class="order-md-2 p-b-30">
                <div class="p-t-7 p-l-85 p-l-15-lg p-l-0-md">
                    <h3 class="mtext-111 cl2 p-b-16 txt-center">
                        VẬN CHUYỂN ĐÓNG GÓI &amp; KHIẾU NẠI
                        BỒI THƯỜNG
                    </h3>
                    <div class="WordSection1">


                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Bằng cách sử dụng
dịch vụ vận chuyển được hỗ trợ trên
sàn giao dịch TMĐT BISTO, bạn đã thừa nhận và
đồng ý với các yêu cầu, và/hoặc các Chính Sách,
thực tiễn áp dụng nêu trong Chính Sách Vận Chuyển
này. BISTO bảo lưu quyền sửa đổi Chính Sách Vận
Chuyển này vào bất cứ lúc nào.&nbsp;</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><b><span
                                    style="line-height:150%;color:black">A. &nbsp; QUY ĐỊNH VỀ HÀNG HÓA
KHÔNG VẬN CHUYỂN, HẠN CHẾ VẬN CHUYỂN,
VẬN CHUYỂN CÓ ĐIỀU KIỆN</span></b></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><b><i><span
                                        style="line-height:150%;color:black">Những trường hợp BISTO
không hỗ trợ vận chuyển</span></i></b></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">·&nbsp;Sản phẩm nằm
trong danh mục hàng hóa BISTO không vận chuyển:</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">· Hàng hóa thuộc danh mục
cấm giao dịch trên BISTO.&nbsp;</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">· Kim loại quý (vàng,
bạc, v.v), đá quý có giá trị cao (kim cương, hồng
ngọc, v.v)</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Với các mặt hàng thuộc
danh mục BISTO không hỗ trợ vận chuyển trên đây,
BISTO sẽ không chịu trách nhiệm nếu hàng hóa bị
thu giữ, tiêu hủy hay hư hỏng trong quá trình
vận chuyển. Người bán chịu hoàn toàn trách nhiệm
trước BISTO và pháp luật (nếu có) khi gửi hàng vi
phạm Chính Sách Vận Chuyển của BISTO và&nbsp;pháp
luật Việt Nam.</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Đơn hàng có dấu
hiệu gian lận, lợi dụng các Chính Sách, hỗ
trợ của BISTO</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Người dùng không
tuân theo các hướng dẫn, quy định và khuyến cáo
về vận chuyển của BISTO được nêu ra trong Chính
Sách này hoặc trong quá trình trao đổi trực tiếp
với nhân viên BISTO.</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Người dùng vi
phạm các Tiêu chuẩn cộng đồng của BISTO.&nbsp;</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Vận chuyển
hoàn trả sản phẩm sau khi Người Mua (Người
nhận) đã nhận hàng, bóc hàng và gửi Yêu cầu
Trả hàng-Hoàn tiền tới BISTO. Quá trình hoàn trả
hoặc đổi trả sản phẩm sẽ được
thực hiện dựa trên thỏa thuận trực
tiếp giữa Người Mua và Người Bán.</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Các đơn hàng vi
phạm về số lượng và giá trị mua hàng giới
hạn theo từng chương trình khuyến mại. Nội
dung chi tiết sẽ được thông báo theo từng chương
trình.</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">&nbsp;Để hạn chế
việc đơn hàng bị hủy, Người Bán cần nhập
đầy đủ cân nặng sau đóng gói và kích thước 3
chiều của bưu kiện để hệ thống tính khối
lượng quy đổi chính xác theo từng ĐVVC. Với đơn hàng có
khối lượng nhập thấp hơn thực tế, ĐVVC có
quyền từ chối vận chuyển đơn hàng hoặc
Người Bán sẽ phải trả phần phí vận
chuyển phát sinh thêm.</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><b><i><span
                                        style="line-height:150%;color:black">Hàng dễ hư hại/tổn
thất do đặc tính sản phẩm</span></i></b></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Sản phẩm
dễ hư hại/tổn thất trong quá trình vận
chuyển do đặc tính sản phẩm:</span></p>

                        <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%"
                               style="width:100.0%;background:white;border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td width="33%" valign="top" style="width:33.0%;border:solid #DDDDDD 1.0pt;
  padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" style="text-align:justify"><b><span style="line-height:
  150%">Đặc tính</span></b></p>
                                </td>
                                <td width="33%" valign="top" style="width:33.0%;border:solid #DDDDDD 1.0pt;
  border-left:none;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" style="text-align:justify"><b><span style="line-height:
  150%;color:black">Ví dụ cụ thể</span></b></p>
                                </td>
                                <td width="33%" valign="top" style="width:33.0%;border:solid #DDDDDD 1.0pt;
  border-left:none;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" style="text-align:justify"><b><span style="line-height:
  150%;color:black">Ghi chú</span></b></p>
                                </td>
                            </tr>
                            <tr>
                                <td width="33%" valign="top" style="width:33.0%;border:solid #DDDDDD 1.0pt;
  border-top:none;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" style="text-align:justify;text-indent:0cm"><span
                                            style="line-height:150%;color:black">Sản phẩm dễ bị
  nứt/vỡ bề mặt, nóng chảy dưới tác
  dụng của môi trường &nbsp;ánh sáng, nhiệt độ</span></p>
                                </td>
                                <td width="33%" valign="top" style="width:33.0%;border-top:none;border-left:
  none;border-bottom:solid #DDDDDD 1.0pt;border-right:solid #DDDDDD 1.0pt;
  padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" style="text-align:justify;text-indent:0cm"><span
                                            style="line-height:150%;color:black">Mỹ phẩm trang điểm như
  son môi, phấn má, phấn mắt, kem nền và các sản
  phẩm có yêu cầu bảo quản đặc biệt khác</span></p>
                                </td>
                                <td width="33%" valign="top" style="width:33.0%;border-top:none;border-left:
  none;border-bottom:solid #DDDDDD 1.0pt;border-right:solid #DDDDDD 1.0pt;
  padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" style="text-align:justify;text-indent:0cm"><span
                                            style="line-height:150%;color:black">Đặc biệt là các sản
  phẩm hàng xách tay, hàng nhập khẩu do điều kiện
  sản xuất, môi trường và khí hậu khác biệt
  nhiều so với khí hậu Việt Nam</span></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">&nbsp;</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Đối với
các sản phẩm có đặc tính trong danh sách trên hoặc
đặc tính gần giống, khả năng xảy ra rủi ro
trong quá trình vận chuyển rất cao, do vậy BISTO
có quyền từ chối hỗ trợ vận chuyển
hoặc vẫn hỗ trợ nếu có yêu cầu từ
Người Bán nhưng trách nhiệm chịu rủi ro khi vận
chuyển thuộc về Người Bán, trừ khi các bên có
thể xác định tình trạng thiệt hại hoàn
toàn xảy ra do lỗi của đơn vị vận
chuyển.&nbsp;</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Với các đơn hàng có chứa các
sản phẩm này, thời gian vận chuyển sẽ lâu
hơn thông thường từ 2-4 ngày do đơn vị vận chuyển
phải sử dụng các phương tiện đường bộ như ô
tô, tàu hỏa để vận chuyển hàng hóa. Người Bán có
trách nhiệm ghi chú rõ ràng thông tin "Hàng không vận chuyển
hàng không" lên trên gói hàng. Trường hợp Người Bán
không ghi chú rõ ràng dẫn đến việc hàng hóa bị
thu giữ, tiêu hủy, vận chuyển chậm hoặc
không thể vận chuyển được, BISTO sẽ không
chịu trách nhiệm.</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><b><span
                                    style="line-height:150%;color:black">B. &nbsp; QUY ĐỊNH VỀ ĐÓNG GÓI
HÀNG HÓA</span></b></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Trước khi vận chuyển,
Người Bán phải đảm bảo hàng hóa đã sẵn
sàng để được vận chuyển với quãng
đường tương ứng với từng đơn hàng.&nbsp;Cụ
thể như sau:</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">1. &nbsp; &nbsp;Yêu cầu chung</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Tất cả các
bưu kiện đều phải đóng gói sẵn sàng trước khi
vận chuyển, được niêm phong bởi Người Bán. Đơn vị
vận chuyển sẽ chỉ chịu trách nhiệm
vận chuyển hàng hóa theo nguyên tắc “nguyên đai, nguyên
kiện”, và sẽ không chịu trách nhiệm với nội
dung hàng hóa bên trong nếu sản phẩm được giao
tới tay Người Mua/hoàn về tay Người Bán trong
tình trạng còn nguyên niêm phong và bao bì không
bị rách/vỡ/ướt/móp méo.</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Trên&nbsp;bao
bì tất cả các bưu kiện đều phải có thông
tin:</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Thông tin Người
nhận, bao gồm: Tên người nhận, số điện
thoại và địa chỉ người nhận</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Mã vận
đơn của đơn hàng</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Ghi chú hàng không
vận chuyển được bằng đường hàng không (nếu
có ít nhất 01 sản phẩm trong bưu kiện nằm trong
nhóm sản phẩm không thể vận chuyển bằng
đường hàng không)</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Mã đơn hàng
trên&nbsp;hệ thống BISTO (không bắt buộc)</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Để đảm
bảo an toàn cho hàng hóa, BISTO khuyến cáo Người Bán
(Người Gửi) nên gửi kèm hóa đơn tài chính hợp lệ
của sản phẩm trong bưu kiện (nếu có). Hóa đơn tài
chính là căn cứ hỗ trợ quá trình xử lý
khiếu nại như: xác định giá trị thị trường
của hàng hóa, đảm bảo hàng hóa lưu thông hợp lệ,
không bị tịch thu bởi cơ quan quản lý thị
trường, v.v..</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Đơn vị vận
chuyển có quyền bóc mở bưu kiện để kiểm tra
nội dung hàng hóa trong trường hợp nghi ngờ Người
bán gửi sản phẩm không hỗ trợ vận chuyển
hoặc có hành vi gửi hộp rỗng không chứa hàng</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><b><span
                                    style="line-height:150%;color:black">C. &nbsp; QUY ĐỊNH VỀ
KHIẾU NẠI VÀ BỒI THƯỜNG</span></b></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><b><span lang="EN-US"
                                                                                                  style="line-height:150%;color:black">I</span></b><b><span
                                    style="line-height:150%;color:black">. &nbsp; Khiếu nại</span></b></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Thời hạn
khiếu nại:</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Hàng hóa hư hại
do vận chuyển: trong vòng 24 giờ sau khi Người
Mua nhận hàng hoặc Người Bán nhận được hàng
trả về.</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Không nhận
được hàng:&nbsp;trong vòng&nbsp;24 giờ sau
khi&nbsp;nhận được thông báo đơn hàng đã giao hàng thành
công cho Người Mua hoặc đơn hàng đã chuyển hoàn thành
công cho Người Bán.</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Kênh tiếp
nhận khiếu nại về vận chuyển:&nbsp;</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Email:&nbsp;</span><u><span lang="EN-US"
                                                                                                 style="line-height:150%;color:blue">Bistovn@gmail.com</span></u>
                        </p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">Hotline: </span><span lang="EN-US"
                                                                                           style="line-height:150%;color:black">0963 228 283</span>
                        </p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">BISTO có quyền
từ chối hỗ trợ và xử lý khiếu
nại vận chuyển nếu Người Gửi vi phạm
các quy định được nêu ra trong Chính Sách này, và/hoặc vi
phạm các quy định về vận chuyển hàng hóa theo
pháp luật Việt Nam (ví dụ: hàng hóa được lưu
thông/vận chuyển cần có hóa đơn, chứng từ
hợp lệ).</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Khi khiếu
nại, Người Bán cần cung cấp các thông tin sau cho
BISTO:</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%">1.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Mã đơn
hàng/mã vận đơn và ảnh chụp vận đơn giấy
xác nhận đã gửi hàng cho đơn vị vận
chuyển&nbsp;</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%">2.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Ảnh chụp
tình trạng bao bì gói hàng khi hàng được giao
tới Người Mua hoặc hoàn trả lại Người Bán</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%">3.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Ảnh chụp
tình trạng bao bì gói hàng ban đầu khi Người
Bán gửi hàng đi</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%">4.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Ảnh chụp
hoặc video ghi lại phần hư hại của hàng hóa bên
trong khiến cho sản phẩm không thể sử
dụng/vận hành được một cách bình thường</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="line-height:150%">5.<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Các&nbsp;bằng
chứng khác (nếu có)</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">- &nbsp; &nbsp;Thời hạn cung
cấp bằng chứng: Trong vòng 24 giờ kể
từ khi yêu cầu khiếu nại. &nbsp; &nbsp;&nbsp;</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">- &nbsp; &nbsp;Thời gian xử
lý khiếu nại vận chuyển của BISTO:
Từ 07 - 10 ngày làm việc</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">- &nbsp; &nbsp;Để giúp quá
trình xử lý khiếu nại được tiến
hành nhanh hơn, BISTO khuyến cáo Người Bán luôn chụp
lại hình ảnh bên ngoài của bưu kiện trước
khi vận chuyển, trên đó thể hiện rõ các thông
tin của bưu kiện (thông tin Người nhận, cảnh báo
vận chuyển, mã vận đơn, mã đơn hàng, v.v..)</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">- &nbsp; &nbsp; Lưu ý:</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Dịch vụ
vận chuyển của BISTO không cho phép Người Nhận
kiểm tra hàng trước khi thanh toán cho BISTO (Thanh toán thẻ
tín dụng hoặc Thanh toán khi nhận hàng).</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Người Bán hoàn
toàn chịu trách nhiệm đóng gói sản phẩm của
mình đúng quy cách khi giao hàng cho đơn vị vận
chuyển. Trường hợp bao bì/gói/hộp đựng
sản phẩm vẫn còn nguyên vẹn trong quá
trình vận chuyển nhưng hàng hóa bên trong lại bị
hư hại do đóng gói không đúng cách, BISTO sẽ không giải
quyết bất cứ khiếu nại yêu cầu bồi
thường nào từ phía Người Bán hoặc Người Mua
(trong trường hợp gửi trả hàng).</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Người Bán
cần phải giữ lại vận đơn/hóa đơn vận
chuyển mỗi khi giao hàng cho đơn vị vận chuyển.
Khi có bất kỳ khiếu nại hay tranh chấp nào
về mất mát hàng hóa xảy ra, vận đơn/hóa đơn vận
chuyển sẽ là bằng chứng vững chắc
nhất chứng minh Người Bán đã giao hàng.</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><b><span lang="EN-US"
                                                                                                  style="line-height:150%;color:black">II</span></b><b><span
                                    style="line-height:150%;color:black">. &nbsp; Bồi thường</span></b></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Kết quả
xử lý khiếu nại về vận chuyển
sẽ được BISTO thông báo trực tiếp cho Người Dùng
kèm theo thông tin về mức bồi thường (nếu có).</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Nếu quá
trình xử lý khiếu nại và điều tra
của BISTO về lỗi vận chuyển cho thấy
thiệt hại gặp phải không hoàn toàn do lỗi
của đơn vị vận chuyển mà do Người Dùng đã
không tuân theo các điều kiện cấm/hạn chế
hỗ trợ vận chuyển hoặc các hướng dẫn
đã được nêu ra trong Chính Sách này, thì BISTO có
quyền từ chối bồi thường các thiệt
hại này.</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Trường hợp
hàng hóa bị&nbsp;Cơ quan quản lý thị trường
hoặc cơ quan nhà nước&nbsp;có thẩm quyền&nbsp;thu
giữ do không có hóa đơn chứng từ hợp lệ, BISTO sẽ
không chịu trách nhiệm bồi thường.</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Các mức
bồi thường đối với đơn hàng sử dụng
dịch vụ vận chuyển của BISTO:</span></p>

                        <table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" width="670"
                               style="width:502.85pt;background:white;border-collapse:collapse;border:none">
                            <tbody>
                            <tr>
                                <td width="330" style="width:247.7pt;border:solid windowtext 1.0pt;padding:
  0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" align="center" style="text-align:center;text-indent:0cm"><span
                                            style="line-height:150%">Tình trạng</span></p>
                                </td>
                                <td width="340" style="width:9.0cm;border:solid windowtext 1.0pt;border-left:
  none;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" align="center" style="text-align:center;text-indent:0cm"><span
                                            style="line-height:150%;color:black">Mức bồi thường
  tối đa dựa trên giá bán của sản phẩm tại
  thời điểm Người Mua đặt hàng</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td width="330" style="width:247.7pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" style="text-align:justify;text-indent:0cm"><span
                                            style="line-height:150%;color:black">Mất hàng, Bưu gửi thất
  lạc hoàn toàn</span></p>
                                </td>
                                <td width="340" style="width:9.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" align="center" style="text-align:center;text-indent:0cm"><span
                                            style="line-height:150%;color:black">100%</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td width="330" style="width:247.7pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" style="text-align:justify;text-indent:0cm"><span
                                            style="line-height:150%;color:black">Rách, vỡ, ướt bao bì,
  gói bọc nhưng sản phẩm còn nguyên</span></p>
                                </td>
                                <td width="340" style="width:9.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" align="center" style="text-align:center;text-indent:0cm"><span
                                            style="line-height:150%;color:black">5%</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td width="330" style="width:247.7pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" style="text-align:justify;text-indent:0cm"><span
                                            style="line-height:150%;color:black">Rách tem niêm phong của nhà
  sản xuất nhưng sản phẩm còn nguyên</span></p>
                                </td>
                                <td width="340" style="width:9.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" align="center" style="text-align:center;text-indent:0cm"><span
                                            style="line-height:150%;color:black">10%</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td width="330" style="width:247.7pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" style="text-align:justify;text-indent:0cm"><span
                                            style="line-height:150%;color:black">Mất phụ kiện nhưng
  sản phẩm còn nguyên</span></p>
                                </td>
                                <td width="340" style="width:9.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" align="center" style="text-align:center;text-indent:0cm"><span
                                            style="line-height:150%;color:black">20%</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td width="330" style="width:247.7pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" style="text-align:justify;text-indent:0cm"><span
                                            style="line-height:150%;color:black">Hàng hóa bị móp méo, bể
  võ, hư hại từ 1% đến 30% và sản phẩm
  còn sử dụng được dù không cần sửa
  chữa gì</span></p>
                                </td>
                                <td width="340" style="width:9.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" align="center" style="text-align:center;text-indent:0cm"><span
                                            style="line-height:150%;color:black">30%</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td width="330" style="width:247.7pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" style="text-align:justify;text-indent:0cm"><span
                                            style="line-height:150%;color:black">Hàng hóa bị bể vỡ, hư
  hại vượt quá 30%</span></p>
                                </td>
                                <td width="340" style="width:9.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 0cm 0cm 0cm">
                                    <p class="MsoNormal" align="center" style="text-align:center;text-indent:0cm"><span
                                            style="line-height:150%;color:black">100%</span></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Lưu ý:
&nbsp;Để được bồi thường, Người Bán cần
phải cung cấp Hóa đơn hợp pháp của (các) sản
phẩm có trong đơn hàng (Hóa đơn chứng minh giá trị pháp
lý của hàng hóa).Trong trường hợp Người Bán
không cung cấp được Hóa đơn hợp lệ của sản
phẩm, mức bồi thường là 4 lần cước phí
vận chuyển của đơn hàng.&nbsp;</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Trong mọi
trường hợp, khoản tiền bồi thường sẽ
không vượt quá giá bán sản phẩm mà Người Bán đã
niêm yết trên Ứng dụng BISTO tại thời điểm
Người Mua đặt hàng. BISTO có quyền từ chối
bồi thường cho Người Bán nếu Người Bán gửi
hàng hóa sai khác so với nội dung sản phẩm đã
đăng ký trong đơn hàng.</span></p>

                        <p class="MsoNormal" style="text-align:justify;background:white"><span
                                style="line-height:150%;color:black">3. &nbsp; Các khuyến cáo về
việc vận chuyển</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Đơn vị vận
chuyển sẽ yêu cầu Người Mua thanh toán đầy
đủ hoặc xác nhận nhận hàng trước khi Người
Mua có thể bóc hàng và kiểm tra hàng.&nbsp;Nếu có bất cứ
vấn đề phát sinh với bưu kiện, Người Mua vui
lòng liên hệ tới hòm thư </span><span lang="EN-US" style="line-height:150%;color:black">Bistovn@gmail.com.</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Luôn chụp
lại gói hàng trước khi mở ra để lưu lại
bằng chứng về tình trạng bao bì gói
bọc, phục vụ cho quá trình xử lý
khiếu nại (nếu có).&nbsp;Do nguyên tắc “nguyên đai,
nguyên kiện”, các đơn vị vận chuyển sẽ không
giải quyết các khiếu nại bồi thường
nếu Người Mua/Người Bán đồng ý nhận
(lại) hàng và đã ký vào biên bản nhận
hàng/trả hàng. Do đó, vui lòng kiểm tra kỹ bao
bì của sản phẩm. Nếu bao bì sản
phẩm có dấu hiệu bị hư hại (rách/móp
méo/vỡ/ướt) và bạn nghi ngờ hàng hóa bên trong
đã bị tổn hại, vui lòng từ chối
nhận hàng và thông báo cho BISTO.</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Sau khi gửi Yêu
cầu Trả hàng/Hoàn tiền hoặc Khiếu nại
vận chuyển, vui lòng bảo quản và giữ
nguyên trạng bưu kiện và sản phẩm, không sử
dụng thử hay gây ra tác động khiến tình
trạng của sản phẩm còn không như, lúc nhận
hàng. Nếu BISTO xác định sản phẩm và bưu kiện
không còn nguyên trạng như khi Người Nhận (Người
Mua) nhận hàng, BISTO có quyền từ chối mọi Yêu
cầu Trả hàng/Hoàn tiền&nbsp;hoặc Khiếu nại
vận chuyển.</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Khi có khiếu
nại xảy ra, Người dùng phải có trách nhiệm
hỗ trợ BISTO trong quá trình xác minh thông tin bằng
cách cung cấp thông tin trung thực, chính xác khi BISTO liên
hệ. Trong trường hợp BISTO không thể liên hệ
với Người khiếu nại để xác minh thông tin, BISTO
có quyền kết thúc khiếu nại và không giải thích
gì thêm.</span></p>

                        <p class="MsoNormal" style="margin-left:0cm;text-align:justify;background:white"><span
                                style="font-size:10.0pt;line-height:150%;font-family:Symbol">·<span
                                    style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style="line-height:150%;color:black">Việc không
gửi hàng thật, đưa tiền cho nhân viên vận chuyển
để cập nhật sai trạng thái đơn hàng, tự đặt
hàng của chính mình, vv… nhằm lợi dụng Chính
Sách Vận Chuyển và/hoặc các chương trình khuyến
mại theo từng thời kỳ của BISTO sẽ
dẫn đến hình thức xử phạt nghiêm
khắc của BISTO. Ngoài ra, tất cả Người Bán có
trách nhiệm đảm bảo hàng hóa gửi đi không vi phạm
quy định của BISTO và pháp luật Việt Nam. Người
bán sẽ hoàn toàn chịu trách nhiệm trong trường
hợp hàng hóa gửi đi bị phát hiện vi phạm pháp
luật./.</span></p>

                        <p class="MsoNormal" style="text-align:justify"><span style="line-height:150%">&nbsp;</span></p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
