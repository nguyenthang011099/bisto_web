@extends('layout')
@section('app_content')
    <div class="container">
        <div class="row">
            <div class="order-md-2 p-b-30">
                <div class="p-t-7 p-l-85 p-l-15-lg p-l-0-md">
                    <h3 class="mtext-111 cl2 p-b-16 txt-center">
                        GIỚI THIỆU
                        VỀ BISTO
                    </h3>
                    <div class="WordSection1">

                        <p class="MsoNormal">
                            <o:p>&nbsp;</o:p>
                        </p>

                        <p class="MsoNormal">BISTO với mục tiêu hướng tới là trở
                            thành trung tâm giao dịch thương mại điện tử về
                            các sản phẩm thời trang. Bisto cung cấp cho người
                            dùng các trải nghiệm mới dựa trên các tính năng cá nhân
                            hóa tới người dùng, thao tác sử dụng dễ dàng, thuận
                            tiện và nhanh chóng qua phương pháp <span style="mso-bidi-font-size:13.0pt;
line-height:150%;mso-bidi-font-family:&quot;Times New Roman&quot;">hỗ trợ vận
chuyển và thanh toán linh hoạt.<o:p></o:p></span></p>

                        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-size:
13.0pt;line-height:150%;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;
mso-color-alt:windowtext;background:white">Với phương châm hoạt động
“Phụng sự Khách Hàng”, BISTO sẽ nỗ lực không ngừng
nhằm nâng cao chất lượng dịch vụ và sản phẩm,
từ đó mang đến trải nghiệm mua sắm trọn vẹn
cho Khách Hàng.</span><span style="mso-bidi-font-size:13.0pt;line-height:150%;
mso-bidi-font-family:&quot;Times New Roman&quot;;background:white"><o:p></o:p></span></p>

                        <p class="MsoNormal" style="text-align:justify;mso-outline-level:5"><b><span
                                    style="mso-bidi-font-size:13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-bidi-font-family:&quot;Times New Roman&quot;">Mục tiêu của chúng tôi<o:p></o:p></span></b></p>

                        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-size:
13.0pt;line-height:150%;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:
&quot;Times New Roman&quot;">Chúng tôi thật sự tin tưởng vào sức
mạnh khai triển của công nghệ và mong muốn góp
phần làm cho thế giới trở nên tốt đẹp hơn
bằng việc kết nối cộng đồng người mua
và người bán thông qua việc cung cấp một nền
tảng thương mại điện tử.<o:p></o:p></span></p>

                        <p class="MsoNormal" style="text-align:justify"><b><span style="mso-bidi-font-size:
13.0pt;line-height:150%;mso-bidi-font-family:&quot;Times New Roman&quot;">Vị Trí của
Chúng Tôi</span></b><b><span style="mso-bidi-font-size:13.0pt;line-height:150%;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;"><o:p></o:p></span></b>
                        </p>

                        <p class="desc" style="margin:0cm;text-align:justify;text-indent:36.0pt;
line-height:150%"><span style="font-size:13.0pt;line-height:150%;mso-ansi-language:
VI;mso-no-proof:yes">Đối với người dùng trong khu vực,
BISTO mang đến trải nghiệm mua sắm tích hợp
với vô số sản phẩm đa dạng chủng
loại, cộng đồng người dùng năng động và
chuỗi dịch vụ liền mạch.<o:p></o:p></span></p>

                        <p class="MsoNormal" style="text-align:justify"><b><span style="mso-bidi-font-size:
13.0pt;line-height:150%;mso-bidi-font-family:&quot;Times New Roman&quot;;color:black;
mso-color-alt:windowtext;background:white">Tiện ích của BISTO</span></b><b><span
                                    style="mso-bidi-font-size:13.0pt;line-height:150%;mso-bidi-font-family:&quot;Times New Roman&quot;;
background:white"><o:p></o:p></span></b></p>

                        <p class="MsoNormal" style="text-align:justify"><span style="mso-bidi-font-size:
13.0pt;line-height:150%;mso-bidi-font-family:&quot;Times New Roman&quot;">Sản phẩm
chất lượng từ những thương hiệu, nhà cung cấp
nổi tiếng và uy tín;<o:p></o:p></span></p>

                        <p class="MsoNormal" style="text-align:justify">Phương thức thanh toán đa dạng
                            và linh hoạt: Qua tài khoản, ví điện tử và thanh toán
                            trực tiếp;</p>

                        <p class="MsoNormal">Trò chuyện trực tiếp với cửa
                            hàng để tìm hiểu kỹ hơn về sản phẩm;</p>

                        <p class="MsoNormal">Vận chuyển và giao hàng nhanh chóng trong
                            vòng 30-40 phút;</p>

                        <p class="MsoNormal" style="text-indent:0cm"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Chính
                            sách bảo vệ người dùng tuyệt đối.</p>

                        <p class="MsoNormal" style="text-indent:0cm"><b><i>BISTO – Trải nghiệm
                                    cảm giác mua sắm thời trang tuyệt vời từ những
                                    thương hiệu uy tín
                                    <o:p></o:p>
                                </i></b></p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
