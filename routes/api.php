<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('vnpay/payment/callback', 'API\VnpayController@paymentCallback')->name('api.vnpay.payment.callback');
Route::post('zalopay/payment/callback', 'API\ZalopayController@paymentCallback')->name('api.zalopay.payment.callback');
Route::post('momo/payment/callback', 'API\MomoController@paymentCallback')->name('api.momo.payment.callback');
