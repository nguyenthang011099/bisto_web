<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Backend
/* static page*/
Route::get('/orders/success/{order_id}', function () {
    return view('static.order-success');
});

Route::get('/orders/fail/{order_id}', function () {
    return view('static.order-failure');
});

/* landing page */
Route::get('/gtbt', 'UserController@gtbt');
Route::get('/tccd', 'UserController@tccd');
Route::get('/qchd', 'UserController@qchd');
Route::get('/dkdv', 'UserController@dkdv');
Route::get('/htbh', 'UserController@htbh');
Route::get('/gqtc', 'UserController@gqtc');
Route::get('/csdt', 'UserController@csdt');
Route::get('/vcdg', 'UserController@vcdg');
Route::get('/bmtt', 'UserController@bmtt');

Route::get('/', 'UserController@index');
Route::get('/.well-known/assetlinks.json', 'UserController@ResponseDataApp');

/** blog-page */
Route::get('/_blog-categories', 'BlogCategoryController@GetBlogCategories');
Route::get('/_blogs', 'BlogController@GetBlogs');
Route::get('/blogs', 'BlogController@blog_list_all_category');
Route::get('/blogs/{slug}', 'BlogController@blog_detail');
Route::get('/blog-categories/{slug}', 'BlogCategoryController@blog_list_by_category');

/** authenticate */
Route::post('/shop/login', 'UserController@fakeLogin');
Route::post('/admin-shop/login', 'UserController@fakeLogin');
Route::post('/admin/login', 'UserController@fakeLogin');
Route::get('/login', 'UserController@login');
Route::post('/login', 'UserController@dashboard');
Route::get('/register', 'UserController@signup');
Route::post('/register', 'UserController@register');
Route::get('/logout', 'UserController@logout');

/* admin */
Route::group(['prefix' => 'admin'], function () {
    Route::get('/dashboard', 'AdminController@dashboard')->name('admin.dashboard');
    Route::get('/avatar', 'AdminController@GetAvatar');

    Route::get('/profile-page', 'AdminController@getProfilePage');
    Route::get('/profiles', 'AdminController@GetProfile');
    Route::post('/profiles', 'AdminController@UpdateProfile');

    Route::get('/password-page', 'AdminController@getPasswordPage');
    Route::get('/passwords', 'AdminController@GetPassword');
    Route::post('/passwords', 'AdminController@UpdatePassword');

    Route::get('/addresses', 'AdminController@GetAddress');
    Route::post('/addresses', 'AdminController@UpdateAddress');
    Route::get('/address-page', 'AdminController@getAddressPage');

    Route::post('/upload-avatar', 'AdminController@UploadAvatar');
    Route::post('/upload-cover', 'AdminController@UploadCover');
    Route::post('/chart-option-shop', 'AdminController@chart_option_shop');

    Route::get('/list-shop', 'AdminController@list_shop');
    Route::get('/list-shop-without-branch', 'AdminController@list_shop_without_branch');
    Route::get('/lists/admin-shops-and-shops', 'AdminController@GetListsAdminShopAndShopAdmin');
    Route::get('/lists/shops', 'AdminController@GetListsShopAdmin');
    Route::get('/lists/shops/export-excel', 'AdminController@ExportExcelListsShop');
    Route::get('/lists/shops/export-pdf', 'PrintController@ExportPdfListsShop');

    Route::get('/list-admin-shop', 'AdminController@list_admin_shop');
    Route::get('/lists/admin-shops', 'AdminController@GetOwnersAdminShop');
    Route::get('/lists/admin-shops/export-excel', 'AdminController@ExportExcelOwnersAdminShop');
    Route::get('/lists/admin-shops/export-pdf', 'PrintController@ExportPdfOwnersAdminShop');

    /** user */
    Route::get('/list-users', 'AdminController@list_users');
    Route::get('/users', 'AdminController@GetUsersAdmin');
    Route::get('/users/export-excel', 'AdminController@ExportExcelUsers');
    Route::get('/users/export-pdf', 'PrintController@ExportPdfUsers');

    Route::get('/list-admin', 'AdminController@list_admin');
    Route::get('/lists/admins', 'AdminController@GetListsAdmin');

    Route::get('/add-admin', 'AdminController@add_admin');
    Route::post('/save-admin', 'AdminController@SaveAdmin');

    Route::put('/block/shop/{id}', 'AdminController@BlockShop');
    Route::put('/un-block/shop/{id}', 'AdminController@UnBlockShop');

    /** chatting */
    Route::get('/chats', 'ChatController@adminShowMessages');
    Route::get('/messages', 'ChatController@AdminListMessages');
    Route::get('/messages/{id}', 'ChatController@AdminDetailMessage');
    Route::post('/messages', 'ChatController@AdminSendMessages');

    /** category product */
    Route::post('/save-category', 'CategoryController@saveCategory');
    Route::get('/category-level-two/{id}', 'CategoryController@getCategoryLevelTwo');
    Route::get('/root-category', 'CategoryController@getCategoryLevelOne');
    Route::get('/all-category', 'CategoryController@allCategoryPage');
    Route::get('/categories/query', 'CategoryController@QueryCategoryAdmin');
    Route::delete('/categories/{id}', 'CategoryController@DeleteCategory');

    Route::get('/category-level-three/{id}', 'CategoryController@getCategoryLevelThree');
    Route::get('/category-level-two/{id}', 'CategoryController@getCategoryLevelTwo');
    Route::get('/root-category', 'CategoryController@getCategoryLevelOne');

    Route::get('/categories', 'CategoryController@GetCategories');
    Route::get('/categories/{id}', 'CategoryController@GetCategoryById');
    Route::get('/parent-category', 'CategoryController@GetParentCategories');
    Route::post('/categories', 'CategoryController@AddCategories');
    Route::put('/categories/{id}', 'CategoryController@UpdateCategories');
    /** coupon */
    Route::get('/list-coupon', 'CouponController@admin_list_coupon');
    Route::get('/list-promotion', 'CouponController@admin_list_promotion');
    Route::get('/coupons', 'CouponController@GetCouponsAdmin');
    Route::get('/promotions', 'CouponController@GetPromotionsAdmin');
    Route::get('/coupons/{id}', 'CouponController@GetCouponAdminById');
    Route::post('/coupons', 'CouponController@AdminInsertCoupon');
    Route::put('/coupons/{id}', 'CouponController@AdminUpdateCoupon');
    Route::delete('/coupons/{id}', 'CouponController@AdminDeleteCoupon');

    /** order */
    Route::get('/manage-order', 'OrderController@admin_manage_order');
    Route::get('/orders', 'OrderController@GetOrdersAdmin');
    Route::get('/orders/{id}', 'OrderController@GetOrdersAdminById');
    Route::post('/orders/query', 'OrderController@QueryOrdersAdmin');
    Route::get('/order-detail/{id}', 'OrderController@admin_order_detail');

    /** flashsale */
    Route::get('/flashsales', 'FlashsaleController@GetFlashSales');
    Route::get('/flashsales/{id}', 'FlashsaleController@GetFlashSaleById');
    Route::post('/flashsales', 'FlashsaleController@PostFlashSale');
    Route::put('/flashsales/{id}', 'FlashsaleController@PutFlashSale');
    Route::delete('/flashsales/{id}', 'FlashsaleController@DeleteFlashSale');

    Route::get('/add-flashsale', 'FlashsaleController@add_flashsale');
    Route::get('/list-flashsale', 'FlashsaleController@admin_list_flashsale');

    /** banner */
    Route::get('/manage-banner', 'BannerController@admin_manage_banner');
    Route::get('/manage-banner-admin-shop', 'BannerController@admin_manage_banner_admin_shop');
    Route::get('/manage-banner-shop', 'BannerController@admin_manage_banner_shop');
    Route::get('/service-banner', 'BannerController@admin_service_banner');
    Route::get('/banners', 'BannerController@GetBannersAdmin');
    Route::get('/banners/{id}', 'BannerController@GetAdminBannerById');
    Route::post('/banners', 'BannerController@InsertBannerAdmin');
    Route::put('/banners/{id}', 'BannerController@UpdateBannerAdmin');
    Route::delete('/banners/{id}', 'BannerController@DeleteBannerAdmin');

    Route::put('/banners/un-active/{id}', 'BannerController@UnActiveBannerAdmin');
    Route::put('/banners/active/{id}', 'BannerController@ActiveBannerAdmin');
    Route::put('/banners/reject/{id}', 'BannerController@RejectBannerAdmin');
    Route::put('/banners/accept/{id}', 'BannerController@AcceptBannerAdmin');

    Route::get('/shop/banners', 'BannerController@AdminGetBannersShop');
    Route::get('/admin-shop/banners', 'BannerController@AdminGetBannersAdminShop');

    /**service-banner*/
    Route::get('/service-banners', 'ServiceBannerController@GetServiceBannersAdmin');
    Route::get('/service-banners/{id}', 'ServiceBannerController@GetAdminServiceBannerById');
    Route::post('/service-banners', 'ServiceBannerController@InsertServiceBannerAdmin');
    Route::put('/service-banners/{id}', 'ServiceBannerController@UpdateServiceBannerAdmin');
    Route::delete('/service-banners/{id}', 'ServiceBannerController@DeleteServiceBannerAdmin');


    /**service-brand*/
    Route::get('/service-brand', 'ServiceBrandController@admin_service_brand');
    Route::get('/service-brands', 'ServiceBrandController@GetServiceBrandsAdmin');
    Route::get('/service-brands/{id}', 'ServiceBrandController@GetAdminServiceBrandById');
    Route::post('/service-brands', 'ServiceBrandController@InsertServiceBrandAdmin');
    Route::put('/service-brands/{id}', 'ServiceBrandController@UpdateServiceBrandAdmin');
    Route::delete('/service-brands/{id}', 'ServiceBrandController@DeleteServiceBrandAdmin');

    /** product */
    Route::get('/product-page', 'ProductController@getProductPageAdmin');
    Route::post('/get-products', 'ProductController@getProductForAdmin');
    Route::get('/detail-product', 'ProductController@getDetailProductPage');
    Route::get('/list-product', 'ProductController@getListProductPage');

    Route::get('/products', 'ProductController@GetProductsAdmin');
    Route::get('/product-varieties-page', 'ProductController@admin_product_varieties_page');
    Route::get('/product-varieties', 'ProductController@GetProductVarietiesAdmin');
    Route::get('/detail-product/{id}', 'ProductController@getDetailProductAdmin');
    Route::get('/product/block/{id}', 'ProductController@blockProduct');
    Route::get('/product/un-block/{id}', 'ProductController@unBlockProduct');

    /** stylish */
    Route::get('/stylish-page', 'StylishController@lists_stylishes');
    Route::get('/stylishes', 'StylishController@GetStylishes');
    Route::get('/stylishes/{id}', 'StylishController@GetStylishById');
    Route::post('/stylishes', 'StylishController@PostStylish');
    Route::put('/stylishes/{id}', 'StylishController@PutStylish');
    Route::delete('/stylishes/{id}', 'StylishController@DeleteStylish');

    /** bcoin */
    Route::post('/bcoins', 'BcoinController@InsertBcoinAdmin');

    /** licenses */
    Route::get('/license-shop-page', 'LicenseController@getLicenseShopPageForAdmin');
    Route::get('/license-admin-shop-page', 'LicenseController@getLicenseAdminShopPageForAdmin');
    Route::get('/license/query/{adminShopId}', 'LicenseController@adminQueryLicense');
    Route::get('/license?shop_user_id', 'LicenseController@adminQueryLicense');
    Route::put('/license/approve/{id}', 'LicenseController@adminApprove');
    Route::put('/license/reject/{id}', 'LicenseController@adminReject');

    Route::get('/all-size', 'ProductController@getAllSizeAdmin');
    Route::get('/all-color', 'ProductController@getAllColorAdmin');

    /** notification  */
    Route::get('/notification-page', 'AdminController@getNotificationPage');
    Route::get('/send-notification-page', 'AdminController@getSendingNotificationPage');
    Route::post('/notification', 'AdminController@notifyAllUser');

    /** payment */
    Route::get('/payment', 'AdminController@payment');
    Route::get('/payment-history-page', 'UserTransactionController@admin_payment_history_page');
    Route::get('/payment-history', 'UserTransactionController@getPaymentHistoryAdmin');

    /** fee services & payment method */
    Route::get('/fee-app-page', 'DebtController@getFeeAppPage');
    Route::get('/app-services', 'DebtController@GetAppServices');
    Route::get('/app-services/{id}', 'DebtController@GetOneAppServices');
    Route::post('/app-services', 'DebtController@InsertAppServices');
    Route::put('/app-services/{id}', 'DebtController@UpdateAppServices');
    Route::delete('/app-services/{id}', 'DebtController@DeleteAppServices');

    Route::get('/fee-payment-page', 'DebtController@getFeePaymentPage');
    Route::get('/payment-services', 'DebtController@GetPaymentServices');
    Route::get('/payment-services/{id}', 'DebtController@GetOnePaymentServices');
    Route::post('/payment-services', 'DebtController@InsertPaymentServices');
    Route::put('/payment-services/{id}', 'DebtController@UpdatePaymentServices');
    Route::delete('/payment-services/{id}', 'DebtController@DeletePaymentServices');

    /** shipping order */
    Route::get('/list-shipping-order', 'DeliveryController@getShippingOrder');
    Route::post('/shipping-orders', 'DeliveryController@QueryShippingOrder');

    /** product services */
    Route::get('/product-service-page', 'ServiceProductController@admin_product_service_page');
    Route::get('/product-services', 'ServiceProductController@GetProductServices');
    Route::get('/product-services/{id}', 'ServiceProductController@GetOneProductServices');
    Route::post('/product-services', 'ServiceProductController@InsertProductServices');
    Route::put('/product-services/{id}', 'ServiceProductController@UpdateProductServices');
    Route::delete('/product-services/{id}', 'ServiceProductController@DeleteProductServices');

    /** service-system */
    Route::put('/service-system', 'AdminController@updateServiceSystem');
    Route::get('/service-system', 'AdminController@getServiceSystem');
    Route::post('/service-system', 'AdminController@createServiceSystem');
    Route::get('/service-system-page', 'AdminController@service_system_page');

    /** debt */
    Route::get('/revenue-bisto-one', 'BistoExportController@ExportExcelRevenueBistoOne');
    Route::post('/revenue-bisto-one', 'BistoExportController@RevenueBistoOne');
    Route::get('/revenue-bisto-two', 'BistoExportController@ExportExcelRevenueBistoTwo');
    Route::post('/revenue-bisto-two', 'BistoExportController@RevenueBistoTwo');
    Route::get('/subsidy-bisto', 'BistoExportController@ExportExcelSubsidyBisto');
    Route::post('/subsidy-bisto', 'BistoExportController@SubsidyBisto');

    Route::get('/delivery-unit', 'DeliveryController@ExportExcelDelivery');
    Route::post('/delivery-unit', 'DeliveryController@DeliveryUnit');

    /** debts */
    Route::get('/admin-shop/debts', 'DebtController@ExportDebtOfAdminShop');
    Route::post('/admin-shop/debts', 'DebtController@QueryDebtOfAdminShop');
    Route::get('/shop/debts', 'DebtController@ExportDebtOfShop');
    Route::post('/shop/debts', 'DebtController@QueryDebtOfShop');
    Route::get('/debt-page', 'DebtController@debt_page_admin');

    /** payment */
    Route::get('/payment-debts', 'DebtController@ExportExcelPayment');
    Route::post('/payment-debts', 'DebtController@QueryPayment');

    /** blogs */
    Route::get('/blog-page', 'BlogController@AdminListsBlog');
    Route::get('/blogs', 'BlogController@AdminGetBlogs');
    Route::get('/blogs/{id}', 'BlogController@AdminGetBlogById');
    Route::post('/blogs', 'BlogController@AdminPostBlog');
    Route::delete('/blogs/{id}', 'BlogController@AdminDeleteBlog');
    Route::put('/blogs/{id}', 'BlogController@AdminPutBlog');

    /** blog-categories */
    Route::get('/blog-category-page', 'BlogCategoryController@AdminListsBlogCategory');
    Route::get('/blog-categories', 'BlogCategoryController@AdminGetBlogCategories');
    Route::get('/blog-categories/{id}', 'BlogCategoryController@AdminGetBlogCategoryById');
    Route::post('/blog-categories', 'BlogCategoryController@AdminPostBlogCategory');
    Route::delete('/blog-categories/{id}', 'BlogCategoryController@AdminDeleteBlogCategory');
    Route::put('/blog-categories/{id}', 'BlogCategoryController@AdminPutBlogCategory');


    /** blog-templates */
    Route::get('/blog-template-page', 'BlogTemplateController@AdminListsBlogTemplate');
    Route::get('/blog-templates', 'BlogTemplateController@AdminGetBlogTemplates');
    Route::get('/blog-templates/{id}', 'BlogTemplateController@AdminGetBlogTemplateById');
    Route::post('/blog-templates', 'BlogTemplateController@AdminPostBlogTemplate');
    Route::delete('/blog-templates/{id}', 'BlogTemplateController@AdminDeleteBlogTemplate');
    Route::put('/blog-templates/{id}', 'BlogTemplateController@AdminPutBlogTemplate');

    /** tags */
    Route::get('/tag-page', 'TagController@AdminListsTag');
    Route::get('/tags', 'TagController@AdminGetTags');
    Route::get('/tags/{id}', 'TagController@AdminGetTagById');
    Route::post('/tags', 'TagController@AdminPostTag');
    Route::delete('/tags/{id}', 'TagController@AdminDeleteTag');
    Route::put('/tags/{id}', 'TagController@AdminPutTag');

    /** marketings */
    Route::get('/marketing-page', 'AdvertisementController@AdminListsAdvertisement');
    Route::get('/marketings', 'AdvertisementController@AdminGetAdvertisements');
    Route::get('/marketings/{id}', 'AdvertisementController@AdminGetAdvertisementById');
    Route::post('/marketings', 'AdvertisementController@AdminPostAdvertisement');
    Route::delete('/marketings/{id}', 'AdvertisementController@AdminDeleteAdvertisement');
    Route::put('/marketings/{id}', 'AdvertisementController@AdminPutAdvertisement');

    /** posts */
    Route::get('/post-page', 'PostController@AdminListsPost');
    Route::get('/posts', 'PostController@AdminGetPosts');
    Route::get('/posts/{id}', 'PostController@AdminGetPostById');
    Route::put('/posts/admin-status/{id}', 'PostController@AdminChangePostAdminStatus');
});

/* admin-shop */
Route::group(['prefix' => 'admin-shop'], function () {
    Route::get('/dashboard', 'AdminShopController@dashboard');

    Route::get('/addresses', 'AdminShopController@GetAddress');
    Route::post('/addresses', 'AdminShopController@UpdateAddress');
    Route::get('/address-page', 'AdminShopController@getAddressPage');

    Route::get('/avatar', 'AdminShopController@GetAvatar');

    Route::get('/profile-page', 'AdminShopController@getProfilePage');
    Route::get('/profiles', 'AdminShopController@GetProfile');
    Route::post('/profiles', 'AdminShopController@UpdateProfile');

    Route::get('/password-page', 'AdminShopController@getPasswordPage');
    Route::get('/passwords', 'AdminShopController@GetPassword');
    Route::post('/passwords', 'AdminShopController@UpdatePassword');

    Route::post('/upload-cover', 'AdminShopController@UploadCover');
    Route::post('/upload-avatar', 'AdminShopController@UploadAvatar');

    Route::get('/shops', 'AdminShopController@GetAllShop');
    Route::get('/shop-page', 'AdminShopController@shop_page');
    Route::get('/shops/{id}', 'AdminShopController@GetUserShopById');
    Route::post('/shops', 'AdminShopController@SaveUserShop');
    Route::put('/shops/{id}', 'AdminShopController@UpdateUserShop');
    Route::delete('/shops/{id}', 'AdminShopController@DeleteUserShop');

    /** licenses */
    Route::get('/license-page', 'LicenseController@getLicensePage');
    Route::get('/license', 'LicenseController@getLicense');
    Route::post('/license', 'LicenseController@uploadLicense');
    Route::delete('/license', 'LicenseController@deleteLicense');

    /** chart */
    Route::post('/chart-option-shop', 'AdminController@chart_option_shop');

    /** category product */
    Route::get('/category-level-three/{id}', 'CategoryController@getCategoryLevelThree');
    Route::get('/category-level-two/{id}', 'CategoryController@getCategoryLevelTwo');
    Route::get('/root-category', 'CategoryController@getCategoryLevelOne');
    Route::get('/categories', 'CategoryController@GetCategoriesAdminShop');
    Route::get('/all-category', 'CategoryController@categoryAdminShopPage');
    Route::get('/categories/query', 'CategoryController@QueryCategoryAdminShop');

    /** product */
    Route::get('/all-product', 'ProductController@admin_shop_all_product');
    Route::get('/product-varieties-page', 'ProductController@admin_shop_product_varieties_page');
    Route::get('/product-varieties', 'ProductController@GetProductVarietiesAdminShop');
    Route::get('/products/shop/{shopId}', 'ProductController@getProductByShopId');
    Route::post('/product/add', 'ProductController@adminShopAddProduct');
    Route::get('/product/query', 'ProductController@AdminShopQueryProduct');
    Route::get('/unactive-product/{id}', 'ProductController@admin_shop_unactive_product');
    Route::get('/active-product/{id}', 'ProductController@admin_shop_active_product');

    /** coupon */
    Route::get('/list-coupon', 'CouponController@admin_shop_list_coupon');
    Route::get('/list-promotion', 'CouponController@admin_shop_list_promotion');
    Route::get('/coupons', 'CouponController@GetCouponsAdminShop');
    Route::get('/promotions', 'CouponController@GetPromotionsAdminShop');

    Route::get('/coupons/{id}', 'CouponController@GetCouponAdminShopById');
    Route::post('/coupons', 'CouponController@AdminShopInsertCoupon');
    Route::put('/coupons/{id}', 'CouponController@AdminShopUpdateCoupon');
    Route::delete('/coupons/{id}', 'CouponController@AdminShopDeleteCoupon');

    /** order */
    Route::get('/manage-order', 'OrderController@admin_shop_manage_order');
    Route::get('/orders', 'OrderController@GetOrdersAdminShop');
    Route::get('/orders/{id}', 'OrderController@GetOrderAdminShopById');
    Route::post('/orders/query', 'OrderController@QueryOrdersAdminShop');
    Route::get('/order-detail/{id}', 'OrderController@admin_shop_order_detail');

    /** brand */
    Route::get('/lists-brand', 'BrandController@ListsBrand');
    Route::get('/brands', 'BrandController@GetBrands');

    Route::get('/brands/{id}', 'BrandController@GetBrandById');
    Route::post('/brands', 'BrandController@SaveBrand');
    Route::put('/brands/{id}', 'BrandController@UpdateBrand');
    Route::delete('/brands/{id}', 'BrandController@deleteBrand');

    /**service-brand*/
    Route::get('/service-brand', 'ServiceBrandController@admin_shop_service_brand');
    Route::get('/service-brands', 'ServiceBrandController@GetServiceBrandsAdminShop');
    Route::post('/service-brands', 'ServiceBrandController@PostServiceBrandsAdminShop');

    /** banner */
    Route::get('/manage-banner', 'BannerController@admin_shop_manage_banner');
    Route::get('/banners', 'BannerController@GetBannersAdminShop');
    Route::get('/banners/{id}', 'BannerController@GetAdminShopBannerById');
    Route::post('/banners', 'BannerController@InsertBannerAdminShop');
    Route::put('/banners/{id}', 'BannerController@UpdateBannerAdminShop');
    Route::delete('/banners/{id}', 'BannerController@DeleteBannerAdminShop');
    Route::put('/banners/un-active/{id}', 'BannerController@UnActiveBannerAdminShop');
    Route::put('/banners/active/{id}', 'BannerController@ActiveBannerAdminShop');

    /** payment */
    Route::get('/payment', 'AdminShopController@payment');
    Route::get('/payment-history-page', 'UserTransactionController@admin_shop_payment_history_page');
    Route::get('/payment-history', 'UserTransactionController@getPaymentHistoryAdminShop');
    Route::get('/payment-return/{provider}', 'AdminShopController@payment_return')->name('admin_shop_payment.return');
    Route::post('/payment-redirect', 'UserTransactionController@getRechargeAdminShop');

    /** notification */
    Route::put('/notification-status', 'AdminShopController@updateStatusNotification');
    Route::get('/get-notifications/{index}', 'AdminShopController@getNotifications');

    /** image-size */
    Route::get('/list-image-size', 'AdminShopController@getImageSizePage');
    Route::get('/image-sizes', 'AdminShopController@GetImageSize');
    Route::get('/image-sizes/{id}', 'AdminShopController@GetImageSizeById');
    Route::post('/image-sizes', 'AdminShopController@InsertImageSize');
    Route::put('/image-sizes/{id}', 'AdminShopController@UpdateImageSize');
    Route::delete('/image-sizes/{id}', 'AdminShopController@DeleteImageSize');

    /** chatting */
    Route::get('/chats', 'ChatController@adminShopShowMessages');
    Route::get('/messages', 'ChatController@AdminShopListMessages');
    Route::get('/messages/{id}', 'ChatController@AdminShopDetailMessage');
    Route::post('/messages', 'ChatController@AdminShopSendMessages');

    /** flashsale */
    Route::get('/flashsales', 'FlashsaleController@GetAdminShopFlashSales');
    Route::get('/flashsales/products/{id}', 'FlashsaleController@AdminShopFlashSalesProducts');
    Route::get('/flashsales/{id}', 'FlashsaleController@GetFlashSalesById');
    Route::post('/flashsales', 'FlashsaleController@AdminShopApplyFlashSale');
    Route::get('/register-flashsale', 'FlashsaleController@AdminShopRegisterFlashSale');
    Route::get('/select-product', 'FlashsaleController@AdminShopSelectProduct');

    /** stylist */
    Route::get('/all-stylish', 'AdminShopController@getAllStylish');

    /** debts */
    Route::get('/debt-page', 'DebtController@debt_page_admin_shop');
    Route::get('/debts', 'DebtController@ExportExcelDebtAdminShop');
    Route::post('/debts', 'DebtController@QueryDebtAdminShop');
});

/* shop */
Route::group(['prefix' => 'shop'], function () {
    Route::get('/dashboard', 'ShopController@dashboard');

    Route::get('/profile-page', 'ShopController@getProfilePage');
    Route::get('/profiles', 'ShopController@GetProfile');
    Route::post('/profiles', 'ShopController@UpdateProfile');

    Route::get('/addresses', 'ShopController@GetAddress');
    Route::post('/addresses', 'ShopController@UpdateAddress');
    Route::get('/address-page', 'ShopController@getAddressPage');

    Route::post('/upload-avatar', 'ShopController@UploadAvatar');
    Route::post('/upload-cover', 'ShopController@UploadCover');

    Route::get('/password-page', 'ShopController@getPasswordPage');
    Route::get('/passwords', 'ShopController@GetPassword');
    Route::post('/passwords', 'ShopController@UpdatePassword');

    Route::get('/list-image-size', 'ShopController@getImageSizePage');
    Route::get('/image-sizes', 'ShopController@GetImageSize');
    Route::get('/image-sizes/{id}', 'ShopController@GetImageSizeById');
    Route::post('/image-sizes', 'ShopController@InsertImageSize');
    Route::put('/image-sizes/{id}', 'ShopController@UpdateImageSize');
    Route::delete('/image-sizes/{id}', 'ShopController@DeleteImageSize');
    Route::get('/avatar', 'ShopController@GetAvatar');

    /** chart */
    Route::post('/chart-option-shop', 'AdminController@chart_option_shop');

    /* chatting */
    Route::get('/chats', 'ChatController@shopShowMessages');
    Route::get('/messages', 'ChatController@ShopListMessages');
    Route::get('/messages/{id}', 'ChatController@ShopDetailMessage');
    Route::post('/messages', 'ChatController@ShopSendMessage');

    /** category product */
    Route::get('/category-level-three/{id}', 'CategoryController@getCategoryLevelThree');
    Route::get('/category-level-two/{id}', 'CategoryController@getCategoryLevelTwo');
    Route::get('/root-category', 'CategoryController@getCategoryLevelOne');

    /** stylish */
    Route::get('/all-stylish', 'ShopController@getAllStylish');
    /** product */
    Route::get('/add-product', 'ProductController@add_product');
    Route::delete('/product/{id}', 'ProductController@delete_product');
    Route::get('/search-product', 'ProductController@searchProduct');
    Route::get('/detail-product/{id}', 'ProductController@getDetailProductShop');

    Route::get('/all-size', 'ProductController@getAllSize');
    Route::get('/all-color', 'ProductController@getAllColor');

    Route::post('/save-product', 'ProductController@save_product');
    Route::post('/update-product/{product_id}', 'ProductController@update_product');

    Route::post('/remove-img-product', 'ProductController@remove_img_product');
    Route::post('/apply-discount', 'ProductController@apply_discount');

    Route::get('/all-product', 'ProductController@all_product');
    Route::get('/products', 'ProductController@GetProductsShop');
    Route::get('/product-varieties', 'ProductController@GetProductVarietiesShop');
    Route::get('/product-varieties-page', 'ProductController@shop_product_varieties_page');

    Route::get('/unactive-product/{id}', 'ProductController@unactive_product');
    Route::get('/active-product/{id}', 'ProductController@active_product');

    /** discount product */
    Route::get('/list-product-discount', 'ProductController@list_product_discount');
    Route::put('/discount', 'ProductController@applyDiscount');
    Route::post('/search-product-discount', 'ProductController@search_product_discount');

    /** coupon */
    Route::get('/list-coupon', 'CouponController@shop_list_coupon');
    Route::get('/promotions', 'CouponController@GetPromotionsShop');
    Route::get('/list-promotion', 'CouponController@shop_list_promotion');
    Route::get('/coupons', 'CouponController@GetCouponsShop');
    Route::get('/coupons/{id}', 'CouponController@GetCouponShopById');
    Route::post('/coupons', 'CouponController@ShopInsertCoupon');
    Route::put('/coupons/{id}', 'CouponController@ShopUpdateCoupon');
    Route::delete('/coupons/{id}', 'CouponController@ShopDeleteCoupon');

    /** flashsale */
    Route::get('/flashsales', 'FlashsaleController@GetShopFlashSales');
    Route::get('/flashsales/products/{id}', 'FlashsaleController@ShopFlashSalesProducts');
    Route::get('/flashsales/{id}', 'FlashsaleController@ShopGetOneFlashSales');
    Route::post('/flashsales', 'FlashsaleController@ShopApplyFlashSale');
    Route::get('/register-flashsale', 'FlashsaleController@ShopRegisterFlashSale');
    Route::get('/select-product', 'FlashsaleController@ShopSelectProduct');

    /** order */
    Route::get('/manage-order', 'OrderController@shop_manage_order');
    Route::get('/orders', 'OrderController@GetOrdersShop');
    Route::get('/orders/{id}', 'OrderController@GetOrderShopById');
    Route::put('/orders/confirm/{id}', 'OrderController@ConfirmOrderShopById');
    Route::put('/orders/cancel/{id}', 'OrderController@CancelOrderShopById');
    Route::post('/orders/query', 'OrderController@QueryOrdersShop');
    Route::get('/orders/print/{id}', 'OrderController@PrintOrderShop');
    Route::get('/order-detail/{id}', 'OrderController@shop_order_detail');

    /** notification */
    Route::get('/get-notifications/{index}', 'ShopController@getNotifications');
    Route::put('/notification-status', 'ShopController@updateStatusNotification');

    /** banner */
    Route::get('/manage-banner', 'BannerController@shop_manage_banner');
    Route::get('/banners', 'BannerController@GetBannersShop');
    Route::get('/banners/{id}', 'BannerController@GetShopBannerById');
    Route::post('/banners', 'BannerController@InsertBannerShop');
    Route::put('/banners/{id}', 'BannerController@UpdateBannerShop');
    Route::delete('/banners/{id}', 'BannerController@DeleteBannerShop');

    Route::put('/banners/un-active/{id}', 'BannerController@UnActiveBannerShop');
    Route::put('/banners/active/{id}', 'BannerController@ActiveBannerShop');

    /**service-brand*/
    Route::get('/service-brand', 'ServiceBrandController@shop_service_brand');
    Route::get('/service-brands', 'ServiceBrandController@GetServiceBrandsShop');
    Route::post('/service-brands', 'ServiceBrandController@PostServiceBrandsShop');


    /** product rating */
    Route::get('/manage-rate-product', 'ShopController@manage_rate_product');
    Route::get('/products/ratings', 'ShopController@GetRatingProducts');
    Route::get('/view-rate-product/{product_id}', 'ShopController@view_rate_product');
    Route::get('/view/ratings/{id}', 'ShopController@ViewProductRatings');
    Route::post('/products/ratings', 'ShopController@SaveReplyRatings');

    /** licenses */
    Route::get('/license-page', 'LicenseController@getLicensePageShop');
    Route::get('/license', 'LicenseController@getLicenseShop');
    Route::post('/license', 'LicenseController@uploadLicenseShop');

    /** payment */
    Route::get('/payment', 'ShopController@payment');
    Route::get('/payment-return/{provider}', 'ShopController@payment_return')->name('shop_payment.return');
    Route::get('/payment-history-page', 'UserTransactionController@shop_payment_history_page');
    Route::get('/payment-history', 'UserTransactionController@getPaymentHistoryShop');
    Route::post('/payment-redirect', 'UserTransactionController@getRechargeShop');

    /** product service */
    Route::get('/product-service-page', 'ServiceProductController@shop_product_service_page');
    Route::get('/product-service-register/{id}', 'ServiceProductController@shop_product_service_register');
    Route::get('/product-services', 'ServiceProductController@GetShopProductServices');
    Route::get('/product-services/{id}', 'ServiceProductController@GetShopOneProductServices');
    Route::post('/product-services', 'ServiceProductController@ShopProductService');
    Route::put('/product-services/{id}', 'ServiceProductController@ShopApplyProductService');

    /** debts */
    Route::get('/debt-page', 'DebtController@debt_page_shop');
    Route::get('/debts', 'DebtController@ExportExcelDebtShop');
    Route::post('/debts', 'DebtController@QueryDebtShop');

    /** posts */
    Route::get('/post-page', 'PostController@ShopListsPost');
    Route::get('/posts', 'PostController@ShopGetPosts');
    Route::get('/posts/{id}', 'PostController@ShopGetPostById');
    Route::post('/posts', 'PostController@ShopPostPost');
    Route::delete('/posts/{id}', 'PostController@ShopDeletePost');
    Route::put('/posts/{id}', 'PostController@ShopPutPost');

    /** comments */
    Route::get('/comments', 'CommentPostController@ShopGetCommentsByPostId');
});

//Delivery
Route::post('/insert-delivery', 'DeliveryController@insert_delivery');
Route::post('/fcm-token', 'UserController@registerDevice');
Route::delete('/fcm-token/{id}', 'UserController@unregisterDevice');
Route::put('/fcm-token/{id}', 'UserController@changeUserDevice');

/** address: province-district-ward */
Route::get('/provinces', 'AddressController@GetProvinces');
Route::get('/provinces/{id}', 'AddressController@GetProvinceById');
Route::get('/districts', 'AddressController@GetDistrictsByProvince');
Route::get('/districts/{id}', 'AddressController@GetDistrictById');
Route::get('/wards', 'AddressController@GetWardsByDistrict');
Route::get('/wards/{id}', 'AddressController@GetWardById');

Route::get('/test-api', function () {
//    return vn_to_lowercase('Tỉnh Thái Bình');
    return response()->json(['result' => (\App\Services\Shipping\Ghn::getInstance()->getStoreById(81450))]);
});

