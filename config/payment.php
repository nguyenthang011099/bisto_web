<?php
return [
    'vnpay' => [
        'payment_url' => env('VNPAY_PAYMENT_URL'),
        'tmn_code' => env('VNPAY_TMN_CODE'),
        'hash_secret' => env('VNPAY_HASH_SECRET'),
    ],
    'zalopay' => [
        'base_uri' => env('ZALOPAY_BASE_URI'),
        'key1' => env('ZALOPAY_KEY1'),
        'key2' => env('ZALOPAY_KEY2'),
        'app_id' => (int)env('ZALOPAY_APP_ID'),
    ],
    'momo' => [
        'base_uri' => env('MOMO_BASE_URI'),
        'partner_code' => env('MOMO_PARTNER_CODE'),
        'access_key' => env('MOMO_ACCESS_KEY'),
        'secret_key' => env('MOMO_SECRET_KEY'),
    ]
];
