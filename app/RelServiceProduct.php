<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelServiceProduct extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên tỉnh, thành phố
     * type: Tỉnh, Thành phố
     */
    protected $fillable = [
        'product_id', 'product_service_id', 'user_id'
    ];

    protected $primaryKey = 'id';
    protected $table = 'rel_service_product_top';

    /* Lấy ra sản phẩm top nửa đầu tháng */
    public static function getProductsFirstMonth($data)
    {
        $date1 = date($data['year'] . '-' . $data['month'] . '-01');
        $date2 = date($data['year'] . '-' . $data['month'] . '-15');
        return self::getProductsByDay($date1, $date2);
    }

    /* Lấy ra sản phẩm top nửa cuối tháng */
    public static function getProductsLastMonth($data)
    {
        $date1 = date($data['year'] . '-' . $data['month'] . '-15');
        $date2 = date($data['year'] . '-' . $data['month'] . '-t');
        return self::getProductsByDay($date1, $date2);
    }

    /* Lấy ra sản phẩm top của cả tháng */
    public static function getProductsOfMonth($data)
    {
        $date1 = date($data['year'] . '-' . $data['month'] . '-01');
        $date2 = date($data['year'] . '-' . $data['month'] . '-t');
        return self::getProductsByDay($date1, $date2);
    }

    /* Lấy ra sản phẩm top nửa đầu quý */
    public static function getProductsFirstPrecious($data)
    {
        $precious = $data['precious'];
        $lastMonthOfPrecious = 3 * $precious;

        $date1 = date($data['year'] . '-' . ($lastMonthOfPrecious - 2) . '-01');
        $date2 = date($data['year'] . '-' . ($lastMonthOfPrecious - 1) . '-15');
        return self::getProductsByDay($date1, $date2);
    }

    /* Lấy ra sản phẩm top nửa cuối quý */
    public static function getProductsLastPrecious($data)
    {
        $precious = $data['precious'];
        $lastMonthOfPrecious = 3 * $precious;

        $date1 = date($data['year'] . '-' . ($lastMonthOfPrecious - 1) . '-15');
        $date2 = date($data['year'] . '-' . $lastMonthOfPrecious . '-t');
        return self::getProductsByDay($date1, $date2);
    }

    /* Lấy ra sản phẩm top của cả quý */
    public static function getProductsOfPrecious($data)
    {
        $precious = $data['precious'];
        $lastMonthOfPrecious = 3 * $precious;

        $date1 = date($data['year'] . '-' . ($lastMonthOfPrecious - 2) . '-01');
        $date2 = date($data['year'] . '-' . $lastMonthOfPrecious . '-t');
        return self::getProductsByDay($date1, $date2);
    }

    /* Lấy ra sản phẩm top nửa đầu năm */
    public static function getProductsFirstYear($data)
    {
        $startMonthOfYear = 1;
        $endMonthOfYear = 6;

        $date1 = date($data['year'] . '-' . $startMonthOfYear . '-01');
        $date2 = date($data['year'] . '-' . $endMonthOfYear . '-t');
        return self::getProductsByDay($date1, $date2);
    }

    /* Lấy ra sản phẩm top nửa cuối năm */
    public static function getProductsLastYear($data)
    {
        $startMonthOfYear = 7;
        $endMonthOfYear = 12;

        $date1 = date($data['year'] . '-' . $startMonthOfYear . '-01');
        $date2 = date($data['year'] . '-' . $endMonthOfYear . '-t');
        return self::getProductsByDay($date1, $date2);
    }

    /* Lấy ra sản phẩm top của cả năm */
    public static function getProductsOfYear($data)
    {
        $startMonthOfYear = 1;
        $endMonthOfYear = 12;

        $date1 = date($data['year'] . '-' . $startMonthOfYear . '-01');
        $date2 = date($data['year'] . '-' . $endMonthOfYear . '-t');
        return self::getProductsByDay($date1, $date2);
    }

    /* Lấy ra các sản phẩm top theo điều kiện ngày */
    private static function getProductsByDay($date1, $date2)
    {
        $products = RelServiceProduct::whereBetween('created_at', [$date1, $date2])
            ->select('created_at', 'started_at', 'expired_at',
                'user_id', 'product_service_id')->get();

        if (!count($products)) return array();

        foreach ($products as $item) {
            self::convertProduct($item);
        }
        return $products;
    }

    private static function convertProduct($item)
    {
        $item->created_at = date("H:m d/m/Y", strtotime($item->created_at));
        $item->shop = User::where('id', $item->user_id)->value('fullname');
        $item->type_advertise = 'Đẩy sản phẩm lên top';

        $item->start_at = date("H:m d/m/Y", strtotime($item->started_at));
        $item->end_at = date("H:m d/m/Y", strtotime($item->expired_at));

        $item->money = 0;
        $money = ServiceProduct::where('id', $item->product_service_id)->value('price');
        if ($money) {
            $item->money = $money;
        }

        unset($item->user_id);
        unset($item->product_service_id);
        unset($item->started_at);
        unset($item->expired_at);
        return $item;
    }
}
