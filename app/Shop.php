<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên shop
     * description: mô tả shop
     * parent_id: thuộc shop nào
     * business_license_ids:
     */
    protected $fillable = [
        'name', 'description', 'parent_id', 'business_license_ids'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_shops';

    public static function getListsChildShop($adminShopId)
    {
        $parentShopId = User::where('id', $adminShopId)->value('shop_id');
        $parentShop = Shop::where('id', $parentShopId)->first();
        $results = array();

        if ($parentShop->parent_id == 0) {
            $shopChidlren = Shop::where('parent_id', $parentShop->id)->get();
            foreach ($shopChidlren as $item) {
                $temp = User::where('shop_id', $item->id)->value('id');
                array_push($results, $temp);
            }
            return $results;
        } else if (is_null($parentShop->parent_id)) {
            array_push($results, $adminShopId);
            return $results;
        }
    }

    public static function getListsShop()
    {
        $users = User::where('role', 2)
            ->select('id')->get();
        $result = array();
        foreach ($users as $item) {
            array_push($result, $item->id);
        }
        return $result;
    }

    public static function getShopName($userId)
    {
        $shopId = User::where('id', $userId)->value('shop_id');
        if (!$userId) return '';
        $shopName = Shop::where('id', $shopId)->value('name');
        return $shopName;
    }

    public static function isChildShop($userId)
    {
        $shopId = User::where('id', $userId)->value('shop_id');
        if (!$shopId) return array(false, );

        $shop = Shop::where('id', $shopId)->select('parent_id', 'name')->first();

        if ($shop->parent_id) {
            return array(true, );
        }
        $item = (object)[
            'id' => $shopId,
            'name' => $shop->name
        ];
        return array(false, $item);
    }

    public static function getAllShops()
    {
        $users = User::where('role', 2)
            ->select('id')->get();
        $result = array();
        foreach ($users as $item) {
            list($isChildShop, $item) = self::isChildShop($item->id);
            if (!$isChildShop && $item) {
                array_push($result, $item);
            }
        }
        return $result;
    }

    public static function getListAdminShops()
    {
        $users = User::where('role', 3)
            ->select('id', 'shop_id')->get();
        $result = array();
        foreach ($users as $item) {
            $shop = Shop::where('id', $item->shop_id)
                ->select('name')->get();
            if ($shop) {
                $shop->id = $item->id;
                array_push($result, $shop);
            }
        }
        return $result;
    }
}
