<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    public $timestamps = false; //set time to false

    protected $fillable = [
        'is_seen', 'user_id', 'notification_id'
    ];

    protected $primaryKey = 'id';
    protected $table = 'rel_user_notifications';
}
