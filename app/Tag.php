<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tag extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên
     */
    protected $fillable = [
        'name', 'created_at', 'updated_at'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_tags';
}
