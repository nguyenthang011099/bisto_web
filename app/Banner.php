<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Banner extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên banner
     * image_ids: id của hình ảnh
     * self_status: 1-hiển thị, -1-ẩn (mặc định 1)
     * admin_status: -1 là reject , 0 là pending, 1 là accept (mặc định 0)
     * position: vị trí hiển thị (1-TOP, 2-MIDDLE, 3-BOTTOM)
     * user_id: người tạo banner
     * start_at: ngày bắt đầu
     * end_at: ngày kết thúc
     * description: mô tả
     */
    protected $fillable = [
        'name', 'image_ids', 'self_status', 'position', 'user_id',
        'admin_status', 'start_at', 'end_at', 'description'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_banners';

    /* Lấy ra banner nửa đầu tháng */
    public static function getBannersFirstMonth($data)
    {
        $date1 = date($data['year'] . '-' . $data['month'] . '-01');
        $date2 = date($data['year'] . '-' . $data['month'] . '-15');
        return self::getBannersByDay($date1, $date2);
    }

    /* Lấy ra banner nửa cuối tháng */
    public static function getBannersLastMonth($data)
    {
        $date1 = date($data['year'] . '-' . $data['month'] . '-15');
        $date2 = date($data['year'] . '-' . $data['month'] . '-t');
        return self::getBannersByDay($date1, $date2);
    }

    /* Lấy ra banner của cả tháng */
    public static function getBannersOfMonth($data)
    {
        $date1 = date($data['year'] . '-' . $data['month'] . '-01');
        $date2 = date($data['year'] . '-' . $data['month'] . '-t');
        return self::getBannersByDay($date1, $date2);
    }

    /* Lấy ra banner nửa đầu quý */
    public static function getBannersFirstPrecious($data)
    {
        $precious = $data['precious'];
        $lastMonthOfPrecious = 3 * $precious;

        $date1 = date($data['year'] . '-' . ($lastMonthOfPrecious - 2) . '-01');
        $date2 = date($data['year'] . '-' . ($lastMonthOfPrecious - 1) . '-15');
        return self::getBannersByDay($date1, $date2);
    }

    /* Lấy ra banner nửa cuối quý */
    public static function getBannersLastPrecious($data)
    {
        $precious = $data['precious'];
        $lastMonthOfPrecious = 3 * $precious;

        $date1 = date($data['year'] . '-' . ($lastMonthOfPrecious - 1) . '-15');
        $date2 = date($data['year'] . '-' . $lastMonthOfPrecious . '-t');
        return self::getBannersByDay($date1, $date2);
    }

    /* Lấy ra banner của cả quý */
    public static function getBannersOfPrecious($data)
    {
        $precious = $data['precious'];
        $lastMonthOfPrecious = 3 * $precious;

        $date1 = date($data['year'] . '-' . ($lastMonthOfPrecious - 2) . '-01');
        $date2 = date($data['year'] . '-' . $lastMonthOfPrecious . '-t');
        return self::getBannersByDay($date1, $date2);
    }

    /* Lấy ra banner nửa đầu năm */
    public static function getBannersFirstYear($data)
    {
        $startMonthOfYear = 1;
        $endMonthOfYear = 6;

        $date1 = date($data['year'] . '-' . $startMonthOfYear . '-01');
        $date2 = date($data['year'] . '-' . $endMonthOfYear . '-t');
        return self::getBannersByDay($date1, $date2);
    }

    /* Lấy ra banner nửa cuối năm */
    public static function getBannersLastYear($data)
    {
        $startMonthOfYear = 7;
        $endMonthOfYear = 12;

        $date1 = date($data['year'] . '-' . $startMonthOfYear . '-01');
        $date2 = date($data['year'] . '-' . $endMonthOfYear . '-t');
        return self::getBannersByDay($date1, $date2);
    }

    /* Lấy ra banner của cả năm */
    public static function getBannersOfYear($data)
    {
        $startMonthOfYear = 1;
        $endMonthOfYear = 12;

        $date1 = date($data['year'] . '-' . $startMonthOfYear . '-01');
        $date2 = date($data['year'] . '-' . $endMonthOfYear . '-t');
        return self::getBannersByDay($date1, $date2);
    }

    /* Lấy ra các banners theo điều kiện ngày */
    private static function getBannersByDay($date1, $date2)
    {
        $banners = Banner::whereBetween('created_at', [$date1, $date2])
            ->select('created_at', 'user_id', 'start_at', 'end_at')
            ->get();

        if (!count($banners)) return array();

        foreach ($banners as $item) {
            self::convertBanner($item);
        }
        return $banners;
    }

    private static function convertBanner($item)
    {
        $item->created_at = date("H:m d/m/Y", strtotime($item->created_at));
        $item->shop = User::where('id', $item->user_id)->value('fullname');
        $item->type_advertise = 'Banner';

        $start_at = $item->start_at;
        $end_at = $item->start_at;

        unset($item->start_at);
        unset($item->end_at);

        $item->start_at = date("H:m d/m/Y", strtotime($start_at));
        $item->end_at = date("H:m d/m/Y", strtotime($end_at));

        $diff = abs(strtotime($start_at) - strtotime($end_at));
        $item->money = 0;
        $money = DB::table('app_service_banners')
            ->where('day', $diff)->value('price');
        if ($money) {
            $item->money = $money;
        }

        unset($item->user_id);
        return $item;
    }
}
