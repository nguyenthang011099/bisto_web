<?php

namespace App;

use App\Exceptions\CustomException;
use Illuminate\Database\Eloquent\Model;

class ServiceSystem extends Model
{
    public $timestamps = false; //set time to false

    protected $fillable = [
        'amount'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_service_buyer';
}
