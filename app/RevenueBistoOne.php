<?php

namespace App;

class RevenueBistoOne extends BaseOrder
{
    public function convertOrder($order, $item, $index) {
        parent::checkVoucher($order);
        $item->number = $index;
        unset($item->created_at);
        $item->number = $index;
        $item->code = $order->code;
        $item->created_at = date("H:m d/m/Y", strtotime($order->created_at));

        $item->shop_name = Shop::getShopName($order->seller_id);
        $item->buyer_name = User::getFullName($order->user_id);

        if ($order->payment_status) {
            $item->payment_status = 'CK';
        } else {
            $item->payment_status = 'TM';
        }

        $item->payment_method_name = parent::showPaymentMethod($order->payment_method);
        $item->provider_name = '';
        if ($order->shipping_fee) {
            $item->provider_name = $order->provider_name;
        }

        $item->status = $order->status;
        $item->offer_total = ($order->bisto_voucher + $order->total_money + $order->shop_voucher);
        $item->bisto_voucher = $order->bisto_voucher;
        $item->shop_voucher = $order->shop_voucher;
        $item->total_money = $order->total_money;

        $item->fee_service = parent::getFeeCollection();
        $item->shipping_fee = 0;
        if ($order->shipping_fee) {
            $item->shipping_fee = $order->shipping_fee;
        }
        $item->money = ($order->total_money + $item->fee_service + $order->shipping_fee);

        $item->fixed_charge = parent::getFeeCharge($item->total_money);
        $item->pay = 3000;
        $item->refund = 4000;
        $item->discount = 0;
        $item->note = $order->note;

        unset($item->id);
        return $item;
    }
}
