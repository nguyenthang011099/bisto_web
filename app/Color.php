<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên màu sản phẩm
     * value: giá trị
     */
    protected $fillable = [
        'name', 'value'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_colors';
}
