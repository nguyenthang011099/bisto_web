<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public $timestamps = false; //set time to false

    /**
     * title: tiêu đề thông báo
     * content: nội dung của thông báo
     */
    protected $fillable = [
        'title', 'content'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_notifications';
}
