<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFlashSale extends Model
{
    public $timestamps = false; //set time to false

    /**
     * flash_sale_id: id của flash sale
     * user_id: id chủ shop
     * product_id: sản phẩm áp dụng
     * quantity: số lượng sản phẩm
     * sold: đã bán
     */
    protected $fillable = [
        'flash_sale_id', 'user_id', 'product_id', 'quantity', 'sold'
    ];

    protected $primaryKey = 'id';
    protected $table = 'rel_product_flash_sales';
}
