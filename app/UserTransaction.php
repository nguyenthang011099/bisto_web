<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserTransaction
 * @package App
 *
 * @property $user_id
 * @property $code
 * @property $transaction_type
 * @property $transaction_amount
 * @property $currency
 * @property $status
 * @property $note
 * @property $response_receive
 * @property $provider_type
 */
class UserTransaction extends Model
{
    protected $guarded = [];
    protected $table = 'app_user_transactions';

    const TYPE_RECHARGE = 'nap';

    const STATUS_NEW = 'moi';
    const STATUS_SUCCESS = 'thanh_cong';
    const STATUS_PROCESSING = 'dang_xu_ly';
    const STATUS_FAIL = 'loi';
    const STATUS_CANCEL = 'huy';

    const CURRENCY_VND = 'vnd';

    const PROVIDER_MOMO = 'momo';
    const PROVIDER_VNPAY = 'vnpay';
    const PROVIDER_ZALOPAY = 'zalopay';

    const PROVIDERS = [
        self::PROVIDER_MOMO => 'MoMo',
        self::PROVIDER_VNPAY => 'Vnpay',
        self::PROVIDER_ZALOPAY => 'ZaloPay',
    ];


    /**
     * Cổng giao dịch
     *
     * @param $transaction_type
     * @param $data
     * @return UserTransaction
     */
    public static function gateWay($transaction_type, $data): UserTransaction
    {
        switch ($transaction_type) {
            case self::TYPE_RECHARGE :
                return self::createInitRechargeTransaction($data['amount'], $data['provider'], $data['user_id']);
                break;
            default:
                break;
        }
    }

    /**
     * @param int $amount
     * @param string $provider
     * @param int $user_id
     * @return UserTransaction
     */
    private static function createInitRechargeTransaction(int $amount, string $provider, int $user_id)
    {
        $transaction = new self();
        $transaction->transaction_amount = $amount;
        $transaction->transaction_type = self::TYPE_RECHARGE;
        $transaction->user_id = $user_id;//session('shop_id');
        $transaction->code = date("ymd_") . uniqid();
        $transaction->status = self::STATUS_NEW;
        $transaction->currency = 'vnd';
        $transaction->provider_type = $provider;
        $transaction->save();

        return $transaction;
    }


}
