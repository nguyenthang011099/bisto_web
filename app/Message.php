<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public $timestamps = false; //set time to false

    /**
     * type: 1-text, 2-hình ảnh,...
     * content: nội dung của tin nhắn
     * seender_ids: những người đã xem tin nhắn
     * user_id: người gửi tin nhắn
     * conversation_id: tương ứng với id trong bảng conservation (khóa ngoại)
     * deleted_at: thời điểm xóa tin nhắn
     */
    protected $fillable = [
        'type', 'content', 'seender_ids', 'user_id', 'conversation_id', 'deleted_at'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_messages';
}
