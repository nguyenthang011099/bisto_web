<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên thể loại sản phẩm
     * icon_id: ảnh icon
     * parent_id: 0-nếu thể loại là danh mục lớn, còn lại thì thuộc danh mục khác theo id của parent
     */
    protected $fillable = [
        'name', 'icon_id', 'parent_id'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_categories';
}
