<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên phường, xã
     * type: Phường, Xã
     * district_id: khóa ngoại
     */
    protected $fillable = [
        'name', 'type', 'district_id'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_wards';
}
