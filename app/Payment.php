<?php

namespace App;

class Payment extends BaseOrder
{
    public function convertOrder($order, $item, $index) {
        parent::checkVoucher($order);
        $item->number = $index;
        unset($item->created_at);
        $item->created_at = date("H:m d/m/Y", strtotime($order->created_at));
        $item->code = $order->code;

        $item->shop_name = Shop::getShopName($order->seller_id);
        $item->buyer_name = User::getFullName($order->user_id);

        $item->status = $order->status;
        $item->payment_method_name = parent::showPaymentMethod($order->payment_method);
        $item->total_money = $order->total_money;

        list(, $feeService, $note) = PaymentService::getFeePayment($item->total_money, $order->payment_method);
        $item->fee_service = $feeService;
        $item->note = $note;

        unset($order->user_id);
        unset($order->color);
        return $item;
    }
}
