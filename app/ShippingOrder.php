<?php

namespace App;

class ShippingOrder extends BaseOrder
{
    public $timestamps = false; //set time to false

    /**
     * delivery_unit_id: 1-giao hàng nhanh, 2-lalamove
     * code: mã code
     * detail: chi tiết đơn hàng
     * fee_ship: tiền ship
     * order_id: id đơn hàng
     */
    const GIAO_HANG_NHANH = 1;
    const AHAMOVE = 3;
    const GHTK = 2;
    protected $fillable = [
        'delivery_unit_id', 'code', 'detail', 'fee_ship', 'order_id'
    ];

    protected $primaryKey = 'id';
    protected $table = 'rel_order_shippings';

    public function convertOrder($order, $item, $index)
    {
        $item->number = $index;
        unset($item->created_at);
        $item->created_at = date("H:m d/m/Y", strtotime($order->created_at));
        $item->code = $order->code;
        $item->shop_name = Shop::getShopName($order->seller_id);
        $item->buyer_name = User::getFullName($order->user_id);

        $item->status = $order->status;
        $item->total_money = $order->total_money;
        $item->fee = self::getFeeOrder($item->id);
        $item->note = $order->note;

        unset($item->id);
        return $item;
    }

    public static function getFeeOrder($orderId)
    {
        $shipping = ShippingOrder::where('order_id', $orderId)->first();
        if ($shipping) {
            return $shipping->shipping_fee;
        }
        return 0;
    }
}
