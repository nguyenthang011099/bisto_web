<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPhysical extends Model
{
    public $timestamps = false; //set time to false

    protected $fillable = [
        'weight', 'height'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_user_physicals';
}
