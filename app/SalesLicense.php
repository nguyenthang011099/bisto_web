<?php


namespace App;


class SalesLicense extends \Illuminate\Database\Eloquent\Model
{
    public $timestamps = false; //set time to false

    protected $fillable = [
        'name', 'description', 'image_ids', 'user_id', 'admin_status'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_licenses';
    /**
     * @var mixed
     */
    private $name;
}
