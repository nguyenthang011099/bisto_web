<?php

namespace App;

use App\Exceptions\CustomException;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public $timestamps = false; //set time to false

    /**
     * filename: tên file upload
     * url: đường dẫn firebase
     * size: kích thước file
     * hash: mã code
     * type: 1-image, 2-video
     * extension: đuôi file
     * deleted_at: ngày xóa file
     */
    protected $fillable = [
        'filename', 'url', 'size', 'hash', 'type',
        'extension', 'deleted_at'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_files';

    /**
     * @throws CustomException
     */
    public static function saveFiles($url, $filename, $size, $type)
    {
        try {
            $data = new File();
            $data->url = $url;
            $data->filename = $filename;
            $data->size = $size;
            $data->type = $type;
            $data->save();
            return $data->id;
        } catch (\Exception $e) {
            throw CustomException::makeServerError($e->getMessage(), 500);
        }
    }

    public static function getAvatarUrlById($id)
    {
        return File::where('id', $id)->value('url') ?? DEFAULT_AVATAR;
    }
}
