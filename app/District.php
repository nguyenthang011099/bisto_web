<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên quận, huyện
     * city_id: khóa ngoại
     * type: Quận, Huyện
     */
    protected $fillable = [
        'name', 'city_id', 'type'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_districts';
}
