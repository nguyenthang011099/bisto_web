<?php

namespace App;

use App\Exceptions\CustomException;
use Illuminate\Database\Eloquent\Model;

class ServiceProduct extends Model
{
    public $timestamps = false; //set time to false

    /**
     * date: số ngày
     * max_product_apply: sản phẩm tối đa
     * price: giá tiền
     */
    protected $fillable = [
        'date', 'max_product_apply', 'price'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_service_product';

    public static function insertServiceProduct($date, $max_product_apply, $price)
    {
        $data = new ServiceProduct();
        if (!$date || !$max_product_apply || !$price) {
            return false;
        }

        $data->date = $date;
        $data->max_product_apply = $max_product_apply;
        $data->price = $price;
        $result = $data->save();

        if ($result) return true;
        return false;
    }

    /**
     * @throws CustomException
     */
    public static function updateServiceProduct($id, $date, $max_product_apply, $price)
    {
        $data = ServiceProduct::where('id', $id)->first();
        if (is_null($data)) {
            throw new CustomException('Không tìm thấy dịch vụ sản phẩm nào', 400);
        }

        if (!$date || !$max_product_apply || !$price) {
            return false;
        }

        $data->date = $date;
        $data->max_product_apply = $max_product_apply;
        $data->price = $price;
        $result = $data->save();

        if ($result) return true;
        return false;
    }
}
