<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RelUserPostComment extends Model
{
    public $timestamps = false; //set time to false

    /**
     */
    protected $fillable = [
        'user_id', 'post_id', 'parent_id', 'content', 'image_ids', 'created_at', 'updated_at'
    ];

    protected $primaryKey = 'id';
    protected $table = 'rel_user_post_comment';
}
