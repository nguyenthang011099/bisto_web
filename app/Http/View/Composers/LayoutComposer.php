<?php

namespace App\Http\View\Composers;

use App\File;
use App\Http\Controllers\UserController;
use App\Shop;
use App\User;
use Illuminate\View\View;

class LayoutComposer
{
    public function __construct()
    {
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $info = UserController::getUserInfo();
        $user = User::where('id', $info['id'])->first();
//        $avatar_id = User::where('id', $info['id'])->value('avatar_id');
        $source_avatar = DEFAULT_AVATAR;
        if (!is_null($user) && !is_null($user->avatar_id)) {
            $source_avatar = File::where('id', $user->avatar_id)->value('url');
        }
        $is_shop_without_branch = -1;
        if ($info['role'] == 'shop') {
            $shop = Shop::find($user->shop_id);
            $is_shop_without_branch = 0;
            if (is_null($shop->parent_id)) {
                $is_shop_without_branch = 1;
            }
        }
        $_user_id_ = $info['id'];
        $role = $info['role'];
        $_years_ = [2021, 2022, 2023, 2024, 2025];
        $view->with(compact('source_avatar', '_user_id_', 'role', 'is_shop_without_branch', '_years_'));
    }
}
