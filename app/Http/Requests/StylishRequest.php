<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use App\Exceptions\CustomException;

class StylishRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'bail|required',
            'type' => 'bail|required',
            'icon' => 'bail|required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'name.required' => 'Tên là bắt buộc',
            'type.required' => 'Nam hay nữ là bắt buộc',
            'icon.required' => 'Icon là bắt buộc',
        ];
    }

    /**
     * @throws \Exception
     */
    public function failedValidation(Validator $validator): \Illuminate\Http\JsonResponse
    {
        throw new CustomException($validator->messages()->first(), 400);
    }
}
