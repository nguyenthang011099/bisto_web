<?php

namespace App\Http\Requests;

use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use App\Exceptions\CustomException;

class PaymentServicePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'unit_payment' => 'bail|required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'unit_payment.required' => 'Đơn vị thanh toán là bắt buộc',
        ];
    }

    /**
     * @throws Exception
     */
    public function failedValidation(Validator $validator): \Illuminate\Http\JsonResponse
    {
        throw new CustomException($validator->messages()->first(), 400);
    }
}
