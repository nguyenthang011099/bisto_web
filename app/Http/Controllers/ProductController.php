<?php

namespace App\Http\Controllers;

use App\Category;
use App\Color;
use App\File;
use App\Product;
use App\ProductVariety;
use App\SalesLicense;
use App\Shop;
use App\Size;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{
    public function AuthShopOrAdminShop()
    {
        $shop_id = Session()->get('shop_id');
        $admin_shop_id = Session()->get('admin_shop_id');
        $roles_id = Session()->get('roles_id');
        if (($admin_shop_id || $shop_id) && $roles_id) {

            return Redirect::to('dashboard-admin-shop');
        } else {
            return Redirect::to('admin')->send();
        }
    }

    //get all size in database
    private function getAllSizeArray()
    {
        $size = Size::get();
        $results = array();
        foreach ($size as $key => $val) {
            array_push($results, $val->name);
        }
        return $results;
    }

    //get all color in database
    private function getAllColorArray()
    {
        $color = Color::get();
        $results = array();
        foreach ($color as $key => $val) {
            array_push($results, $val->name);
        }
        return $results;
    }


    //Begin Admin Page
    public function add_product()
    {
        UserController::AuthShop();
        $cate_product = Category::where('parent_id', 0)->get();
        $sizes = $this->getAllSizeArray();
        $colors = $this->getAllColorArray();

        return view('shop.product.add')->with('cate_product', $cate_product)
            ->with('sizes', $sizes)
            ->with('colors', $colors);
    }

    public function getProductPageAdmin()
    {
        UserController::AuthAdmin();
        return view('admin.product.list');
    }


    //show list product of shop
    public function all_product()
    {
        UserController::AuthShop();
        return view('shop.product.lists');
    }

    //show list product of shop
    public function shop_product_varieties_page()
    {
        UserController::AuthShop();
        return view('shop.product.lists_varieties');
    }

    //show list product of shop
    public function admin_shop_product_varieties_page()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.product.lists_varieties');
    }

    public function admin_product_varieties_page()
    {
        UserController::AuthAdmin();
        return view('admin.product.list_varieties');
    }

    /**
     * @param $id
     * @return array
     */
    private function getDetailCategory($id)
    {
        $category = Category::where('id', $id)->first();
        $results = array();
        if (is_null($category)) {
            return $results;
        }
        $results['id'] = $category->id;
        $results['name'] = $category->name;
        if ($category->parent_id == 0) {
            $results['parent'] = null;
        } else {
            $parent = Category::where('id', $category->parent_id)->first();
            $temp = array();
            $temp['id'] = $parent->id;
            $temp['name'] = $parent->name;
            $results['parent'] = $temp;
            if ($parent->parent_id != 0) {
                $grandParent = Category::where('id', $parent->parent_id)->first();
                $temp = array();
                $temp['id'] = $grandParent->id;
                $temp['name'] = $grandParent->name;
                $results['parent']['parent'] = $temp;
            } else {
                $results['parent']['parent'] = null;
            }
        }
        return $results;
    }

    /**
     * GET: http://localhost/bisto_web/shop/products
     */
    public function GetProductsShop()
    {
        UserController::AuthShop();
        $owner = Session()->get('shop_id');

        $keywords = request()->keyword;
        $admin_statuss = [1, -1];
        if (request()->admin_status && request()->admin_status != '0') $admin_statuss = [request()->admin_status];
        $products = Product::where('user_id', $owner)->whereIn('admin_status', $admin_statuss)
            ->orderby('id', 'desc')
            ->where('name', 'like', '%' . $keywords . '%')
            ->get();

        $list_ids = array();
        foreach ($products as $product) {
            array_push($list_ids, $product->id);
        }
        // add image and category name for product array
        $products = $this->show_query_products($list_ids);
        return response()->json(['products' => $products->appends(Input::except('page'))]);
    }

    /**
     * GET: http://localhost/bisto_web/shop/product-varieties
     */
    public function GetProductVarietiesShop()
    {
        UserController::AuthShop();
        $owner = Session()->get('shop_id');
        return $this->get_product_varieties([$owner]);
    }

    /**
     * GET: http://localhost/bisto_web/admin/products
     */
    public function GetProductsAdmin()
    {
        UserController::AuthAdmin();

        $keywords = request()->keyword;
        $adminShopId = request()->adminShopId;
        $admin_statuss = [1, -1];
        if (request()->admin_status && request()->admin_status != '0') $admin_statuss = [request()->admin_status];
        if ($adminShopId == 0) {
            $products = Product::where('name', 'like', '%' . $keywords . '%')->whereIn('admin_status', $admin_statuss)
                ->select('id', 'name', 'created_at')
                ->orderby('created_at', 'desc')
                ->get();
        } else {
            $user = User::find($adminShopId);
            $userShop = Shop::find($user->shop_id);
            if ($user->role == User::ROLE_SHOP) {
                $products = Product::where('name', 'like', '%' . $keywords . '%')->whereIn('admin_status', $admin_statuss)
                    ->where('user_id', $adminShopId)
                    ->select('id', 'name', 'created_at')
                    ->orderby('created_at', 'desc')
                    ->get();
            } else if ($user->role == User::ROLE_ADMIN_SHOP) {
                $childrenShopIds = Shop::where('parent_id', $userShop->id)->pluck('id')->toArray();
                $childrenUserIds = User::whereIn('shop_id', $childrenShopIds)->pluck('id')->toArray();
                $products = Product::where('name', 'like', '%' . $keywords . '%')->whereIn('admin_status', $admin_statuss)
                    ->whereIn('user_id', $childrenUserIds)
                    ->select('id', 'name', 'created_at')
                    ->orderby('created_at', 'desc')
                    ->get();
            }
        }

        $list_ids = array();
        foreach ($products as $product) {
            array_push($list_ids, $product->id);
        }
        // add image and category name for product array
        $products = $this->show_query_products($list_ids);
        return response()->json(['products' => $products->appends(Input::except('page'))]);
    }

    public function getDetailProductShop($id)
    {
        UserController::AuthShop();
        $owner = Session()->get('shop_id');
        $product = Product::where('id', $id)->where('user_id', $owner)->first();
        $color = Color::get();
        $size = Size::get();

        $image_id_array = explode(',', $product->image_ids);
        $images = File::whereIn('id', $image_id_array)->get();

        $product->images = $images;
        $product->category = $this->getDetailCategory($product->category_id);
        $product_size_color = ProductVariety::where('product_id', $product->id)
            ->get();
        $size_color_temp = array();
        foreach ($product_size_color as $item) {
            $pro_size_color = array();
            $pro_size_color['price'] = $item->price;
            $pro_size_color['quantity'] = $item->quantity;
            $pro_size_color['size_id'] = $item->size_id;
            $pro_size_color['color_id'] = $item->color_id;
            foreach ($color as $cl) {
                if ($cl->id == $item->color_id) {
                    $pro_size_color['color_name'] = $cl->name;
                    break;
                }
            }
            foreach ($size as $sz) {
                if ($sz->id == $item->size_id) {
                    $pro_size_color['size_name'] = $sz->name;
                    break;
                }
            }
            array_push($size_color_temp, $pro_size_color);
        }

        $product->product_size_color = $size_color_temp;
        $product->description = $product->description;


        return response()->json(["success" => '1', 'results' => $product]);
    }


    public function getDetailProductAdmin(Request $request, $id)
    {
        UserController::AuthAdmin();
        $product = Product::where('id', $id)->first();
        $color = Color::get();
        $size = Size::get();

        $image_id_array = explode(',', $product->image_ids);
        $images = File::whereIn('id', $image_id_array)->get();
        $temp = array();
        foreach ($images as $item) {
            array_push($temp, $item->url);
        }
        $product->image_urls = $temp;
        $product->category = $this->getDetailCategory($product->category_id);
        $product_size_color = ProductVariety::where('product_id', $product->id)
            ->get();
        $size_color_temp = array();
        foreach ($product_size_color as $item) {
            $pro_size_color = array();
            $pro_size_color['price'] = $item->price;
            $pro_size_color['quantity'] = $item->quantity;
            $pro_size_color['size_id'] = $item->size_id;
            $pro_size_color['color_id'] = $item->color_id;
            foreach ($color as $cl) {
                if ($cl->id == $item->color_id) {
                    $pro_size_color['color_name'] = $cl->name;
                    break;
                }
            }
            foreach ($size as $sz) {
                if ($sz->id == $item->size_id) {
                    $pro_size_color['size_name'] = $sz->name;
                    break;
                }
            }
            array_push($size_color_temp, $pro_size_color);
        }

        $product->product_size_color = $size_color_temp;
        $product->description = $product->description;


        return response()->json(["success" => '1', 'results' => $product]);
    }

    //search product
    public function searchProduct(Request $request)
    {
        UserController::AuthShop();
        $owner = Session()->get('shop_id');
        //get query string from input search
        $keywords = $request->keyword;
        $page = $request->page;
        //get product by input search
        $products = Product::where('user_id', $owner)
            ->where('name', 'like', '%' . $keywords . '%')
            ->orderby('app_products.id', 'desc')->paginate(10);

        $category = Category::get();
        $color = Color::get();
        $size = Size::get();
        if (count($products) > 0) {
            foreach ($products as $product) {
                $image_id_array = explode(',', $product->image_ids);
                $images = File::whereIn('id', $image_id_array)->get();
                $temp = array();
                foreach ($images as $item) {
                    array_push($temp, $item->source_url);
                }
                $product->image_urls = $temp;
                foreach ($category as $item) {
                    if ($item->id == $product->category_id) {
                        $product->category_name = $item->name;
                        break;
                    }
                }
                $product_size_color = ProductVariety::where('product_id', $product->id)
                    ->get();
                $size_color_temp = array();
                foreach ($product_size_color as $item) {
                    $pro_size_color = array();
                    $pro_size_color['price'] = $item->price;
                    $pro_size_color['quantity'] = $item->quantity;
                    $pro_size_color['size_id'] = $item->size_id;
                    $pro_size_color['color_id'] = $item->color_id;
                    foreach ($color as $cl) {
                        if ($cl->id == $item->color_id) {
                            $pro_size_color['color_name'] = $cl->name;
                            break;
                        }
                    }
                    foreach ($size as $sz) {
                        if ($sz->id == $item->size_id) {
                            $pro_size_color['size_name'] = $sz->name;
                            break;
                        }
                    }
                    array_push($size_color_temp, $pro_size_color);
                }

                $product->product_size_color = $size_color_temp;
                $product->description = $product->description;

            }
        }
        return response()->json(["success" => '1', 'results' => $products], 200);
    }

    /**
     * PUT /shop/discount
     * @param Request $req
     * @return \Illuminate\Http\JsonResponse
     */
    public function applyDiscount(Request $req)
    {
        try {
            UserController::AuthShop();
            $list_id = $req->product_ids;
            if (count($list_id)) {
                $productRequest = Product::whereIn('id', $list_id)->get();
                $shop_id = Session()->get('shop_id');
                // have records
                if (count($productRequest) > 0) {
                    // some id is invlid
                    if (count($list_id) > count($productRequest)) {
                        return response()->json(['message' => 'some id is invalid'], 400);
                    }
                    foreach ($productRequest as $pro) {
                        if ($shop_id != $pro->user_id) {
                            return response()->json(['message' => 'list product is not to owner buy this shop'], 400);
                        }
                    }
                    $start_at = \date('Y-m-d h:m:s', $req->start_at);
                    $end_at = \date('Y-m-d h:m:s', $req->end_at);

                    $products = Product::whereIn('id', $list_id);
                    $products->update(['start_at' => $start_at, 'end_at' => $end_at, 'discount' => $req->discount]);

                    return response()->json(['message' => 'thêm giảm giá đồng loạt thành công']);

                } // list_id array is empty
                else {
                    return response()->json(['message' => 'list product is empty'], 400);
                }
            } else {
                return response()->json(['message' => 'list product id is empty'], 400);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'server is error'], 500);
        }
    }

    // Add discount using checkbox in product shop
    public function list_product_discount()
    {
        UserController::AuthShop();
        return view('shop.discount.add');
    }

    //search discount product
    public function search_product_discount(Request $request)
    {
        UserController::AuthShop();
        $owner = Session()->get('shop_id');
        //get query string from input search
        $keywords = $request->keywords_submit;
        //get product by input search
        $products = Product::where('user_id', $owner)
            ->where('name', 'like', '%' . $keywords . '%')
            ->orderby('app_products.id', 'desc')->get();

        $list_ids = array();
        foreach ($products as $product) {
            array_push($list_ids, $product->id);
        }

        $products = $this->show_query_products($list_ids, $products);
        return view('shop.discount.add')->with('products', $products);
    }


    // call when shop add new product
    public function save_product(Request $request)
    {
        try {
            UserController::AuthShop();

            //check license of admin shop
            $shopId = User::find(Session()->get('shop_id'))->shop_id;
            $parentShopId = Shop::find($shopId)->parent_id;
            if (is_null($parentShopId)) {
                $id = Session()->get('shop_id');
            } else {
                $parentUserId = User::where('shop_id', $parentShopId)->value('id');
                $id = $parentUserId;
            }

            $license = SalesLicense::where('user_id', $id)->first();
            if (!$license) {
                if (is_null($parentShopId))
                    return response()->json(['message' => 'Shop chưa tải lên giấy phép kinh doanh'], 403);
                return response()->json(['message' => 'Admin shop chưa tải lên giấy phép kinh doanh'], 403);
            } else {
                if ($license->admin_status != 1) {
                    return response()->json(['message' => 'giấy phép kinh doanh chưa được phê duyệt, hãy liên hệ admin'], 403);
                }
            }

            $data = array();
            // insert multiple image product
            $images = $request->image_urls;
            $image_ids = array();
            foreach ($images as $image) {
                $imageId = File::insertGetId($image);
                array_push($image_ids, $imageId);
            }


            // for save product table
            $data['name'] = $request->name;
            $data['price'] = $request->price;
            $data['image_ids'] = implode(",", $image_ids);
            $data['description'] = $request->description;

            $data['discount'] = $request->discount;
            $data['start_at'] = \date('Y-m-d h:m:s', $request->start_at);
            $data['end_at'] = \date('Y-m-d h:m:s', $request->end_at);
            $data['user_id'] = Session()->get('shop_id');
            $data['self_status'] = $request->self_status;
            $data['category_id'] = $request->category_id;
            $data['range_age'] = implode(',', $request->range_age);
            $data['stylist_ids'] = implode(',', $request->stylish);

            $productId = Product::insertGetId($data);

            // for product_size_color table
            $productSizeColor = $request->product_size_color;
            if (is_array($productSizeColor) || is_object($productSizeColor)) {
                foreach ($productSizeColor as $key => $value) {
                    $temp = array();
                    $temp['product_id'] = $productId;
                    $temp['quantity'] = $value['quantity'];
                    $temp['color_id'] = $value['color_id'];
                    $temp['size_id'] = $value['size_id'];
                    if (isset($value['price'])) {
                        $temp['price'] = $value['price'];
                    } else {
                        $temp['price'] = null;
                    }
                    $temp['sold'] = 0;
                    ProductVariety::insert($temp);
                }
            }

            return response()->json(['message' => 'Thêm sản phẩm thành công'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Lỗi server'], 500);
        }
    }


    // return vierw of update product page
    public function editProduct($id)
    {
        UserController::AuthShop();
        $category = Category::where('product_id', $id)->first();

        $edit_product = Product::where('id', $id)->first();
        $edit_product->category_name = $category->name;

        $size_ids = ProductVariety::where('product_id', $edit_product->id)->groupBy('size_id')->get();
        $product_sizes = array();
        foreach ($size_ids as $key => $value) {
            $size_name = Size::where('id', $value->size_id)->first();
            array_push($product_sizes, $size_name);
        }

        $color_ids = ProductVariety::where('product_id', $edit_product->id)->groupBy('color_id')->get();
        $product_colors = array();
        foreach ($color_ids as $key => $value) {
            $color_name = Color::where('id', $value->color_id)->first();
            array_push($product_colors, $color_name);
        }

        $image_ids = explode(",", $edit_product->image_ids);
        $images = File::whereIn('id', $image_ids)->get();

        //get category parent current of product
        $parent_tag = $category->parent_tag;
        $parent_name = Category::where('id', $parent_tag)->value('name');

        $sizes = $this->getAllSizeArray();
        $colors = $this->getAllColorArray();

        $manager_product = view('shop.product.edit')->with('edit_product', $edit_product)
            ->with('parent_name', $parent_name)->with('all_images', $images)
            ->with('product_sizes', $product_sizes)->with('sizes', $sizes)
            ->with('product_colors', $product_colors)->with('colors', $colors);

        return view('shop.layout')->with('shop.product.edit', $manager_product);
    }

    public function update_product(Request $request, $id)
    {
        try {
            UserController::AuthShop();
            $data = array();

            // save list image from request
            $images = $request->image_urls;
            $image_ids = array();
            if (count($images)) {
                foreach ($images as $image) {
                    array_push($image_ids, File::insertGetId($image));
                }
            }
            //for save data to product table
            $data['name'] = $request->name;
            $data['price'] = $request->price;
            $data['image_ids'] = implode(",", $image_ids);
            $data['description'] = $request->description;
            $data['discount'] = $request->discount;
            $data['start_at'] = \date('Y-m-d h:m:s', $request->start_at);
            $data['end_at'] = \date('Y-m-d h:m:s', $request->end_at);
            $data['self_status'] = $request->self_status;
            $data['category_id'] = $request->category_id;
            $data['range_age'] = implode(',', $request->range_age);
            $data['stylist_ids'] = implode(',', $request->stylish);
            Product::where('id', $id)->update($data);

            //delete all product_size_color
            ProductVariety::where('product_id', $id)->delete();

            // for product_size_color table
            $productSizeColor = $request->product_size_color;
            foreach ($productSizeColor as $key => $value) {
                $temp = array();
                $temp['product_id'] = $id;
                $temp['quantity'] = $value['quantity'];
                $temp['color_id'] = $value['color_id'];
                $temp['size_id'] = $value['size_id'];
                if ((isset($value['price']) && $value['price'] > 0) || !isset($value['price'])) {
                    if ($value['quantity'] > 0) {
                        $temp = array();
                        $temp['product_id'] = $id;
                        $temp['quantity'] = $value['quantity'];
                        $temp['color_id'] = $value['color_id'];
                        $temp['size_id'] = $value['size_id'];
                        if (isset($value['price'])) {
                            $temp['price'] = $value['price'];
                        } else {
                            $temp['price'] = null;
                        }
                        $temp['sold'] = 0;
                        ProductVariety::insert($temp);
                    }
                }

            }
            return response()->json(['message' => 'Cập nhật sản phẩm thành công']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    public function delete_product($product_id)
    {
        $this->AuthShopOrAdminShop();
        $product = Product::where('id', $product_id)->first();
        $imageId = explode(',', $product->image_ids);
        Product::where('id', $product_id)->delete();
        File::whereIn('id', $imageId)->delete();
        Session()->put('message', 'Xóa sản phẩm thành công');
        return response()->json(['success' => 1]);
    }

    public function unactive_product($id)
    {
        UserController::AuthShop();
        Product::where('id', $id)->update(['self_status' => -1]);
        Session()->put('message', 'Ẩn sản phẩm thành công');
        return Redirect::to('all-product');
    }

    public function active_product($id)
    {
        UserController::AuthShop();
        Product::where('id', $id)->update(['self_status' => 1]);
        Session()->put('message', 'Kích hoạt sản phẩm thành công');
        return Redirect::to('all-product');
    }

    /** GET /admin-shop/active-product/25 */
    public function admin_shop_unactive_product($id)
    {
        UserController::AuthAdminShop();
        try {
            $owner = Shop::getListsChildShop(Session()->get('admin_shop_id'));
            $user = Product::whereIn('user_id', $owner)->where('id', $id)->first();
            if (is_null($user)) {
                return response(['messsage' => 'sản phẩm này không thuộc chuỗi cửa hàng của bạn'], 400);
            } else {
                Product::where('id', $id)->update(['self_status' => -1]);
                return response(['message' => 'tạm dừng sản phẩm thành công']);
            }
        } catch (\Exception $e) {
            return response(['messsage' => 'lỗi hệ thống'], 500);
        }

    }

    /** GET /admin-shop/unactive-product/25 */
    public function admin_shop_active_product($id)
    {
        UserController::AuthAdminShop();
        try {
            $owner = Shop::getListsChildShop(Session()->get('admin_shop_id'));
            $user = Product::whereIn('user_id', $owner)->where('id', $id)->first();
            if (is_null($user)) {
                return response(['messsage' => 'sản phẩm này không thuộc chuỗi cửa hàng của bạn'], 400);
            } else {
                Product::where('id', $id)->update(['self_status' => 1]);
                return response(['message' => 'kích hoạt sản phẩm thành công']);
            }
        } catch (\Exception $e) {
            return response(['messsage' => 'lỗi hệ thống'], 500);
        }
    }

    public function admin_shop_all_product()
    {
        return view('admin-shop.product.lists');
    }

    /**
     * @param $list_ids list user_id have product to add detail (image_urls, color, size, category)
     */
    private function getDetailForProduct($list_ids)
    {
        $products = Product::whereIn('user_id', $list_ids)->paginate(10);
        $category = Category::get();
        $color = Color::get();
        $size = Size::get();
        if (count($products) > 0) {
            foreach ($products as $product) {
                $image_id_array = explode(',', $product->image_ids);
                $images = File::whereIn('id', $image_id_array)->get();
                $temp = array();
                foreach ($images as $item) {
                    array_push($temp, $item->source_url);
                }
                $product->image_urls = $temp;
                foreach ($category as $item) {
                    if ($item->id == $product->category_id) {
                        $product->category_name = $item->name;
                        break;
                    }
                }
                $product_size_color = ProductVariety::where('product_id', $product->id)
                    ->get();
                $size_color_temp = array();
                foreach ($product_size_color as $item) {
                    $pro_size_color = array();
                    $pro_size_color['price'] = $item->price;
                    $pro_size_color['quantity'] = $item->quantity;
                    $pro_size_color['size_id'] = $item->size_id;
                    $pro_size_color['color_id'] = $item->color_id;
                    foreach ($color as $cl) {
                        if ($cl->id == $item->color_id) {
                            $pro_size_color['color_name'] = $cl->name;
                            break;
                        }
                    }
                    foreach ($size as $sz) {
                        if ($sz->id == $item->size_id) {
                            $pro_size_color['size_name'] = $sz->name;
                            break;
                        }
                    }
                    array_push($size_color_temp, $pro_size_color);
                }

                $product->product_size_color = $size_color_temp;
                $product->description = $product->description;

            }
        }

        return $products;
    }

    /** GET /admin-shop/products/shop/{shopId} */
    public function getProductByShopId($shopId)
    {
        UserController::AuthAdminShop();
        $listUserShopId = Shop::getListsChildShop(Session()->get('admin_shop_id'));
        // bas request
        if (!in_array($shopId, $listUserShopId)) {
            return response(['message' => 'this id not in admin shop children id'], 400);
        } else {
            $list_id = array();
            array_push($list_id, $shopId);
            $products = $this->getDetailForProduct($list_id);
            return response(['products' => $products, 'message' => 'truy vấn thành công'], 200);
        }
    }

    /** POST admin-shop/product/add */
    public function adminShopAddProduct(Request $request)
    {
        try {
            UserController::AuthAdminShop();

            //check license of admin shop
            $license = SalesLicense::where('user_id', Session()->get('admin_shop_id'))->first();
            if (!$license) {
                return response()->json(['message' => 'admin shop chưa tải lên giấy phép kinh doanh'], 403);
            } else {
                if ($license->admin_status != 1) {
                    return response()->json(['message' => 'giấy phép kinh doanh chưa được phê duyệt, hãy liên hệ admin'], 403);
                }
            }

            $listChildrenShopId = Shop::getListsChildShop(Session()->get('admin_shop_id'));

            // insert multiple image product
            $images = $request->image_urls;
            $image_ids = array();
            foreach ($images as $item) {
                $image = array();
                $image['url'] = $item['url'];
                $image['type'] = $item['type'];
                $image['size'] = $item['size'];
                $image['filename'] = $item['filename'];
                $image['hash'] = '';
                $imageId = File::insertGetId($image);
                array_push($image_ids, $imageId);
            }


            if (count($listChildrenShopId) > 0) {
                foreach ($listChildrenShopId as $shopId) {
                    // for save product table
                    $data = array();
                    $data['name'] = $request->name;
                    $data['price'] = $request->price;
                    $data['image_ids'] = implode(",", $image_ids);
                    $data['description'] = $request->description;
                    $data['start_at'] = \date('Y-m-d h:m:s', $request->start_at);
                    $data['end_at'] = \date('Y-m-d h:m:s', $request->end_at);
                    $data['user_id'] = $shopId;
                    $data['self_status'] = $request->self_status;
                    $data['category_id'] = $request->category_id;
                    $data['range_age'] = implode(',', $request->range_age);
                    $data['stylist_ids'] = implode(',', $request->stylish);
                    Product::insert($data);
                }
            } else {
                return response(['message' => 'không tồn tại id nào ứng với admin_shop_id trên'], 400);
            }

            return response()->json(['message' => 'Thêm sản phẩm thành công'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Lỗi server'], 500);
        }
    }

    /** GET /admin-shop/query/product?key=abc&shopId=0&page=1 */
    public function AdminShopQueryProduct(Request $req)
    {
        UserController::AuthAdminShop();
        if ($req->has('key')) {
            if (is_null($req->key)) {
                $key = '';
            } else {
                $key = $req->key;
            }
        } else {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }
        if ($req->has('shopId')) {
            $shopId = $req->shopId;
        } else {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $listShopId = Shop::getListsChildShop(Session()->get('admin_shop_id'));
        if ($shopId == 0) {
            $list_ids = $listShopId;
        } else {
            if (in_array($shopId, $listShopId)) {
                $list_ids = [$shopId];
            } else {
                return response()->json(['message' => 'Cửa hàng không tồn tại'], 400);
            }
        }
        if (count($list_ids) > 0) {
            $admin_statuss = [1, -1];
            if (request()->admin_status && request()->admin_status != '0') $admin_statuss = [request()->admin_status];
            $products = Product::whereIn('user_id', $list_ids)->whereIn('admin_status', $admin_statuss)
                ->where('name', 'like', '%' . $key . '%')
                ->select('id', 'name', 'user_id', 'created_at', 'price', 'image_ids',
                    'category_id', 'self_status', 'admin_status')
                ->orderBy('created_at', 'DESC')
                ->paginate(10);

            if (count($products) > 0) {
                foreach ($products as $product) {
                    //get shop name
                    $shopId = User::where('id', $product->user_id)->value('shop_id');
                    $shopName = Shop::where('id', $shopId)->value('name');
                    $product->shop_name = $shopName;

                    $image_id_array = explode(',', $product->image_ids);
                    $images = File::whereIn('id', $image_id_array)->get();
                    $temp = array();
                    foreach ($images as $item) {
                        array_push($temp, $item->url);
                    }
                    $product->image_urls = $temp;
                    $product->category_name = Category::where('id', $product->category_id)->value('name');

                    $product_variety = ProductVariety::where('product_id', $product->id)
                        ->select('quantity', 'sold')
                        ->get();
                    $sold = 0;
                    $quantity = 0;

                    foreach ($product_variety as $item) {
                        $quantity += $item->quantity;
                        $sold += $item->sold;
                    }
                    $product->quantity = $quantity;
                    $product->sold = $sold;
                }
            }
            return response()->json(["success" => '1', 'results' => $products->appends(Input::except('page'))]);
        } else {
            return response()->json(['message' => 'database is wrong'], 500);
        }
    }

    /**
     * GET: http://localhost/bisto_web/shop/product-varieties
     */
    public function GetProductVarietiesAdminShop(Request $req)
    {
        UserController::AuthAdminShop();
        if ($req->has('shopId')) {
            $shopId = $req->shopId;
        } else {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }
        $listShopId = Shop::getListsChildShop(Session()->get('admin_shop_id'));
        if ($shopId == 0) {
            $list_ids = $listShopId;
        } else {
            if (in_array($shopId, $listShopId)) {
                $list_ids = [$shopId];
            } else {
                return response()->json(['message' => 'Cửa hàng không tồn tại'], 400);
            }
        }
        return $this->get_product_varieties($list_ids);
    }

    public function GetProductVarietiesAdmin(Request $req)
    {
        UserController::AuthAdmin();
        $adminShopId = request()->adminShopId;
        if ($adminShopId == 0) {
            $list_ids = 0;
        } else {
            $user = User::find($adminShopId);
            $childs = Shop::where('parent_id', $user->shop_id)->first();
            if ($childs) {
                $list_ids = Shop::getListsChildShop($adminShopId);
            } else {
                $list_ids = [$user->id];
            }
        }
        return $this->get_product_varieties($list_ids);
    }

    private function get_product_varieties($list_ids): \Illuminate\Http\JsonResponse
    {
        try {
            $quantity = request()->quantity;
            $keyword = request()->keyword;
            if ($list_ids == 0) {
                $products = Product::join('rel_product_varieties', 'app_products.id', '=', 'rel_product_varieties.product_id')
                    ->where('rel_product_varieties.quantity', $quantity)
                    ->orderby('app_products.id', 'desc')
                    ->where('name', 'like', '%' . $keyword . '%')->distinct()->select('app_products.id', 'name', 'image_ids', 'app_products.price', 'category_id',
                        'self_status', 'admin_status', 'app_products.created_at', 'app_products.discount', 'app_products.user_id')->paginate(10);
            } else {
                $products = Product::whereIn('user_id', $list_ids)->join('rel_product_varieties', 'app_products.id', '=', 'rel_product_varieties.product_id')
                    ->where('rel_product_varieties.quantity', $quantity)
                    ->orderby('app_products.id', 'desc')
                    ->where('name', 'like', '%' . $keyword . '%')->distinct()->select('app_products.id', 'name', 'image_ids', 'app_products.price', 'category_id',
                        'self_status', 'admin_status', 'app_products.created_at', 'app_products.discount', 'app_products.user_id')->paginate(10);

            }
            foreach ($products as $product) {
                //get shop name
                $shopId = User::where('id', $product->user_id)->value('shop_id');
                $shopName = Shop::where('id', $shopId)->value('name');
                $product->shop_name = $shopName;
                $product->category_name = Category::where('id', $product->category_id)->value('name');
                $product->image = File::where('id', $product->image_ids)->value('url');
                $varieties = ProductVariety::where('product_id', $product->id)->where('quantity', $quantity)->get();
                foreach ($varieties as $variant) {
                    $variant->color_name = Color::where('id', $variant->color_id)->first()->name;
                    $variant->size_name = Size::where('id', $variant->size_id)->first()->name;
                }
                $product['varieties'] = $varieties;
            }
            return response()->json(['products' => $products]);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    //get details product
    private function show_query_products($list_ids)
    {
        $products = Product::whereIn('id', $list_ids)
            ->select('id', 'name', 'image_ids', 'price', 'category_id',
                'self_status', 'admin_status', 'created_at', 'discount')
            ->orderby('created_at', 'desc')
            ->paginate(10);

        foreach ($products as $item) {
            $category_name = Category::where('id', $item->category_id)->value('name');

            $productImage = File::where('id', $item->image_ids)->value('url');

            $product_variety = ProductVariety::where('product_id', $item->id)
                ->select('sold', 'quantity')
                ->get();

            $item->category_name = $category_name;
            $item->image = $productImage;

            $sold = 0;
            $quantity = 0;
            if ($product_variety) {
                foreach ($product_variety as $variety) {
                    $sold += $variety->sold;
                    $quantity += $variety->quantity;
                }
            }
            $item->quantity = $quantity;
            $item->sold = $sold;
        }
        return $products;
    }
    /* page for admin/shop */

    //Remove img in date
    public function remove_img_product(Request $request)
    {
        UserController::AuthShop();
        $img = $request->get();
        $imgId = $img['id'];
        $product = Product::where('image_ids', 'like', '%' . $imgId . '%')->first();
        $imgIdArray = explode(',', $product->image_ids);
        if (($key = array_search($imgId, $imgIdArray)) !== false) {
            unset($imgIdArray[$key]);
        }
        $product->image_ids = implode(',', $imgIdArray);
        $product->save();
        File::where('id', $imgId)->delete();
        return 'Success';
    }

    public function apply_discount(Request $req)
    {
        UserController::AuthShop();
        $data = $req->get();
        $discount = $data['discount'];
        $fromDate = date('Y-m-d h:i:s', $data['fromDate'] / 1000);
        $toDate = date('Y-m-d h:i:s', $data['toDate'] / 1000);
        $ids = $data['ids'];

        $regex = '/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/';
        if (count($ids)) {
            if (!preg_match($regex, $fromDate) || !preg_match($regex, $toDate)) {
                return \redirect('/list-product-discount')->with('message', 'Ngày tháng không dúng, hãy thử lại');
            }
            $products = Product::whereIn('id', $ids)->get();
            foreach ($products as $pro) {
                $pro->discount = $discount;
                $pro->start_at = $fromDate;
                $pro->end_at = $toDate;
                $pro->save();
            }
            return \redirect('shop/list-product-discount')->with('message', 'Áp dụng mã giảm giá thành công');
        }
        return \redirect('shop/list-product-discount')->with('message', 'không tồn tại id của sản phẩm');
    }

    public function getAllSize()
    {
        $results = Size::get();
        return response()->json(['results' => $results]);
    }

    public function getAllSizeAdmin()
    {
        UserController::AuthAdmin();
        $results = Size::get();
        return response()->json(['results' => $results]);
    }

    public function getAllColor()
    {
        $results = Color::get();
        return response()->json(['results' => $results]);
    }

    public function getAllColorAdmin()
    {
        UserController::AuthAdmin();
        $results = Color::get();
        return response()->json(['results' => $results]);
    }

    // admin query product with admin_shop_id, text_search, and paginate for products
    public function getProductForAdmin(Request $req)
    {
        UserController::AuthAdmin();
        $data = $req->all();
        $admin_shop_id = $data['admin_shop_id'];
        $textSearch = $data['text_search'];
        // query all shop
        if ($admin_shop_id == 0) {
            $products = Product::where('name', 'like', '%' . $textSearch . '%')
                ->join('app_users', 'app_products.user_id', '=', 'app_users.id')
                ->paginate(10);
        } // query with specific admin shop
        else {
            $userParentShop = User::where('id', $admin_shop_id)->first();
            $idParentShop = Shop::where('id', $userParentShop->shop_id)->value('id');
            $listChildrenShop = Shop::where('parent_id', $idParentShop)->get();
            $listId = array();
            foreach ($listChildrenShop as $key => $value) {
                array_push($listId, $value->id);
            }
            //admin shop have more than 1 branch
            if (count($listId) > 0) {
                $listShopUsers = User::whereIn('shop_id', $listId)->pluck('id')->toArray();
                $products = User::join('app_products', 'app_products.user_id', '=', 'app_users.id')
                    ->where('app_products.name', 'like', '%' . $textSearch . '%')
                    ->whereIn('app_products.user_id', $listShopUsers)->paginate(10);

            } else {
                $products = Product::where('app_products.user_id', $userParentShop->id)
                    ->where('app_products.name', 'like', '%' . $textSearch . '%')
                    ->join('app_users', 'app_products.user_id', '=', 'app_users.id')
                    ->paginate(10);
            }
        }

        return response()->json(['results' => $this->detail_product($products)]);
    }

    //block product by id
    public function blockProduct($id)
    {
        UserController::AuthAdmin();
        $product = Product::where('id', $id)->first();
        $product->admin_status = $product->admin_status = -1;
        $product->save();
        return response()->json(['message' => 'Khóa sản phẩm thành công']);
    }

    public function unBlockProduct($id)
    {
        UserController::AuthAdmin();
        $product = Product::where('id', $id)->first();
        if ($product) {
            $product->admin_status = $product->admin_status = 1;
            $product->save();
        } else {
            return response()->json(['message' => 'Không tồn tại sản phẩm'], 400);
        }
        return response()->json(['message' => 'Mở khóa sản phẩm thành công'], 200);
    }

    public function getDetailProductPage()
    {
        UserController::AuthAdmin();
        return view('admin.product.detail');
    }

    public function getListProductPage()
    {
        UserController::AuthAdmin();
        return view('admin.product.lists');
    }

    // add image and category for product array
    private function detail_product($products)
    {
        foreach ($products as $item) {
            $categoryName = Category::where('id', $item->category_id)->value('name');
            $productImage = File::where('id', $item->image_ids)->value('url');

            $item->category_name = $categoryName;
            $item->image = $productImage;
        }
        return $products;
    }
}
