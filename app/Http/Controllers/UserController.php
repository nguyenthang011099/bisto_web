<?php

namespace App\Http\Controllers;

use App\Bcoin;
use App\Exceptions\CustomException;
use App\UserDevice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index()
    {
        return view('landing-page.index');
    }

    public function gtbt()
    {
        return view('landing-page.gtbt');
    }

    public function tccd()
    {
        return view('landing-page.tccd');
    }

    public function qchd()
    {
        return view('landing-page.qchd');
    }

    public function dkdv()
    {
        return view('landing-page.dkdv');
    }

    public function htbh()
    {
        return view('landing-page.htbh');
    }

    public function gqtc()
    {
        return view('landing-page.gqtc');
    }

    public function csdt()
    {
        return view('landing-page.csdt');
    }

    public function vcdg()
    {
        return view('landing-page.vcdg');
    }

    public function bmtt()
    {
        return view('landing-page.bmtt');
    }

    public function login()
    {
        return view('login');
    }

    public static function AuthAdmin()
    {
        $admin_id = Session()->get('admin_id');
        $roles_id = Session()->get('roles_id');

        if ($admin_id && $roles_id) {
            return Redirect::to('admin/dashboard');
        }
        return Redirect::to('/')->send();
    }

    public static function AuthAdminShop()
    {
        $admin_id = Session()->get('admin_shop_id');
        $roles_id = Session()->get('roles_id');

        if ($admin_id && $roles_id) {
            return Redirect::to('admin/dashboard');
        }
        return Redirect::to('/')->send();
    }

    public static function AuthShop()
    {
        $admin_id = Session()->get('shop_id');
        $roles_id = Session()->get('roles_id');

        if ($admin_id && $roles_id) {
            return Redirect::to('admin/dashboard');
        }
        return Redirect::to('/')->send();
    }

    public static function AuthLogin()
    {
        $admin_id = Session()->get('admin_id');
        $admin_shop_id = Session()->get('admin_shop_id');
        $shop_id = Session()->get('shop_id');

        if (is_null($admin_id) && is_null($admin_shop_id) && is_null($shop_id)) {
            return Redirect::to('/')->send();
        }
    }

    public function fakeLogin(Request $request)
    {
        $data = $request->validate([
            'phone_number' => 'required',
            'password' => 'required',
        ]);

        $phone = $data['phone_number'];
        $result = User::where('phone', $phone)->first();
        if (!$result) {
            Session()->put('message', 'Mật khẩu hoặc tài khoản bị sai.Làm ơn nhập lại');
            return Redirect::to('/');
        }

        $checked = password_verify($request->password, $result->password);
        if (!$checked) {
            Session()->put('message', 'Mật khẩu hoặc tài khoản bị sai.Làm ơn nhập lại');
            return Redirect::to('/');
        }

        $roleId = $result->role;
        switch ($roleId) {
            case 4:
                Session()->put('admin_name', $result->fullname);
                Session()->put('admin_id', $result->id);
                Session()->put('roles_id', $roleId);
                return response()->json(['_token' => csrf_token(), 'api' => url('/')]);
            case 3:
                Session()->put('admin_shop_name', $result->fullname);
                Session()->put('admin_shop_id', $result->id);
                Session()->put('roles_id', $roleId);
                return response()->json(['_token' => csrf_token(), 'api' => url('/')]);
            case 2:
                Session()->put('shop_name', $result->fullname);
                Session()->put('shop_id', $result->id);
                Session()->put('roles_id', $roleId);
                return response()->json(['_token' => csrf_token(), 'api' => url('/')]);
            default:
                Session()->put('message', 'Bạn không có quyền truy cập vào trang này!');
                return Redirect::to('/');
        }
    }

    public function dashboard(Request $request)
    {
        $data = $request->validate([
            'phone_number' => 'required',
            'password' => 'required',
        ]);

        $phone = $data['phone_number'];
        $result = User::where('phone', $phone)->first();
        if (!$result) {
            return response()->json(['message' => 'Mật khẩu hoặc tài khoản bị sai.Làm ơn nhập lại'], 401);
        }

        $checked = password_verify($request->password, $result->password);
        if (!$checked) {
            return response()->json(['message' => 'Mật khẩu hoặc tài khoản bị sai.Làm ơn nhập lại'], 401);
        }

        $roleId = $result->role;
        switch ($roleId) {
            case 4:
                Session()->put('admin_name', $result->fullname);
                Session()->put('admin_id', $result->id);
                Session()->put('roles_id', $roleId);
                return Redirect::to('admin/dashboard');
            case 3:
                Session()->put('admin_shop_name', $result->fullname);
                Session()->put('admin_shop_id', $result->id);
                Session()->put('roles_id', $roleId);
                return Redirect::to('admin-shop/dashboard');
            case 2:
                $user = User::where('phone', $phone)->first();
                if ($user->status == 1) {
                    Session()->put('shop_name', $result->fullname);
                    Session()->put('shop_id', $result->id);
                    Session()->put('roles_id', $roleId);
                    return Redirect::to('shop/dashboard');
                } else {
                    return response()->json(['message' => 'Shop của bạn đã bị khóa vì lỹ do nào đó, hãy liên hệ quản trị viên!'], 403);
                }

            default:
                return response()->json(['message' => 'Mật khẩu hoặc tài khoản bị sai.Làm ơn nhập lại'], 401);
        }
    }

    private function validation($request)
    {
        return $this->validate($request, [
            'full_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|max:255'
        ]);
    }

    public function signup()
    {
        return view('register');
    }

    public function register(Request $request)
    {
        $this->validation($request);
        $data = array();
        $data['fullname'] = $request->full_name;
        $data['username'] = $request->user_name;
        $data['email'] = $request->email;
        $phone = $request->phone_number;
        $data['phone'] = $request->phone_number;

        $password = password_hash($request->password, PASSWORD_BCRYPT, array('cost' => 12));
        $data['password'] = $password;

        $check_admin_shop = $request->check_admin_shop;
        if ($check_admin_shop == 1) {
            $data['role'] = 3;
        } else {
            $data['role'] = 2;
        }

        $check = User::where('phone', $phone)->first();
        if (is_null($check)) {
            $shop = array();
            $shop['name'] = $request->shop_name;
            $shop['description'] = $request->shop_desc;
            if ($check_admin_shop == 1) {
                $shop['parent_id'] = 0;
            }

            DB::transaction(function () use ($shop, $data) {
                $shopId = DB::table('app_shops')->insertGetId($shop);

                $data['shop_id'] = $shopId;
                $user = User::create($data);

                if($user) {
                    Bcoin::create([
                        'user_id' => $user->id,
                        'amount' => Bcoin::AMOUNT_INIT,
                    ]);
                }
            });

            return redirect('shop/dashboard');
        }
        return redirect('/register')->with('message', 'Tài khoản đã tồn tại, vui lòng nhập lại');
    }

    public function logout()
    {
        switch (Session()->get('roles_id')) {
            case 4:
                Session()->put('admin_name', null);
                Session()->put('admin_id', null);
                Session()->put('roles_id', null);
                break;
            case 3:
                Session()->put('admin_shop_name', null);
                Session()->put('admin_shop_id', null);
                Session()->put('roles_id', null);
                break;
            case 2:
                Session()->put('shop_name', null);
                Session()->put('shop_id', null);
                Session()->put('roles_id', null);
                break;
            default:
                return Redirect::to('/login');
        }
        return Redirect::to('/login');
    }

    public static function getUserInfo()
    {
        $roles_id = Session()->get('roles_id');
        $info = array();
        $role = '';
        $id = 0;
        switch ($roles_id) {
            case 4:
                $id = Session()->get('admin_id');
                $role = 'admin';
                break;
            case 3:
                $id = Session()->get('admin_shop_id');
                $role = 'admin_shop';
                break;
            case 2:
                $id = Session()->get('shop_id');
                $role = 'shop';
                break;
            default:
        }
        $info['role'] = $role;
        $info['id'] = $id;
        return $info;
    }

    /**
     * post /fcm-token
     * @throws CustomException
     */
    public function registerDevice(Request $request)
    {
        try {
            self::AuthLogin();
            $fcm_token = $request->fcm_token;
            $userId = self::getUserInfo()['id'];
            $userDevice = new  UserDevice();
            $userDevice->message_notification_type = $request->message_notification_type;
            $userDevice->fcm_token = $fcm_token;
            $userDevice->user_id = $userId;
            if (is_null($fcm_token))
                throw  CustomException::makeBadRequest('Fcm token là bắt buộc');
            $userDevice->save();
            return response()->json(['user_device' => $userDevice], 200);
        } catch (\Exception $e) {
            throw CustomException::makeServerError($e->getMessage());
        }
    }

    /**
     * delete /fcm-token
     * @throws CustomException
     */
    public function unregisterDevice($id)
    {
        try {
            self::AuthLogin();
            if (is_null($id)) {
                throw  CustomException::makeBadRequest('user_device_id is null');
            }
            $userDevice = UserDevice::where('id', $id)->where('message_notification_type', 3)->first();
            if (is_null($userDevice))
                throw  CustomException::makeBadRequest('Không tìm thấy fcm token');
            $userDevice->delete();
            return response()->json(['message' => 'Xóa thành công'], 200);
        } catch (\Exception $e) {
            throw CustomException::makeServerError($e->getMessage());
        }
    }

    /**
     * put /fcm-token
     * @throws CustomException
     */
    public function changeUserDevice(Request $request, $id)
    {
        try {
            self::AuthLogin();
            $fcmToken = $request->fcm_token;
            if (is_null($id)) {
                throw  CustomException::makeBadRequest('user_device_id is null');
            }
            $userId = self::getUserInfo()['id'];
            $userDevice = UserDevice::where('id', $id)->where('message_notification_type', 3)->first();
            if (is_null($userDevice))
                throw  CustomException::makeBadRequest('Không tìm thấy fcm token');
            $userDevice->user_id = $userId;
            $userDevice->fcm_token = $fcmToken;
            $userDevice->save();
            return response()->json(['message' => 'Thay đổi user_id thành công'], 200);
        } catch (\Exception $e) {
            throw CustomException::makeServerError($e->getMessage());
        }
    }

    public function ResponseDataApp()
    {
        $data = '[
            {
                "relation": [
                "delegate_permission/common.handle_all_urls"
            ],
                "target": {
                "namespace": "android_app",
                    "package_name": "app.bisto.vn",
                    "sha256_cert_fingerprints": [
                    "81:AC:70:35:2D:A3:39:A4:8C:90:96:B7:4B:BA:80:48:1F:42:F4:4B:62:5C:94:CB:AD:65:DD:9C:E9:20:3C:AC"
                ]
                }
            }
        ]';
        return response($data)->header('Content-Type', 'application/json');
    }
}
