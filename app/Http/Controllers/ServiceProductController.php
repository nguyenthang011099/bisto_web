<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\RelServiceProduct;
use App\ServiceProduct;
use DateInterval;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ServiceProductController extends Controller
{
    /**
     * GET: http://127.0.0.1/bisto_web/admin/product-service-page
     */
    public function admin_product_service_page()
    {
        UserController::AuthAdmin();
        return view('admin.service.list_product_service');
    }

    /**
     * GET: http://127.0.0.1/bisto_web/admin/product-services?page=1
     */
    public function GetProductServices()
    {
        UserController::AuthAdmin();
        $productServices = ServiceProduct::orderBy('created_at', 'desc')->paginate(10);
        return response()->json(['results' => $productServices->appends(Input::except('page'))]);
    }

    /**
     * GET: http://127.0.0.1/bisto_web/admin/product-services/1
     */
    public function GetOneProductServices($id)
    {
        UserController::AuthAdmin();
        $productService = ServiceProduct::where('id', $id)->first();
        if ($productService) {
            return response()->json(['result' => $productService]);
        }
        return response()->json([], 204);
    }

    /**
     * POST: http://127.0.0.1/bisto_web/admin/product-services
     */
    public function InsertProductServices(Request $request)
    {
        try {
            UserController::AuthAdmin();
            $date = $request->date;
            $max_product_apply = $request->max_product_apply;
            $price = $request->price;

            $result = ServiceProduct::insertServiceProduct($date, $max_product_apply, $price);
            if ($result) {
                return response()->json(['message' => 'Thêm phí dịch vụ sản phẩm thành công'], 201);
            }
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * PUT: http://127.0.0.1/bisto_web/admin/product-services/1
     */
    public function UpdateProductServices(Request $request, $id)
    {
        try {
            UserController::AuthAdmin();
            $date = $request->date;
            $max_product_apply = $request->max_product_apply;
            $price = $request->price;

            $result = ServiceProduct::updateServiceProduct($id, $date, $max_product_apply, $price);
            if ($result) {
                return response()->json(['message' => 'Cập nhật phí dịch vụ sản phẩm thành công']);
            }
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * DELETE: http://127.0.0.1/bisto_web/admin/product-services/1
     */
    public function DeleteProductServices($id)
    {
        try {
            UserController::AuthAdmin();
            $productService = ServiceProduct::where('id', $id)->first();
            if (is_null($productService)) {
                throw new CustomException('Không tìm thấy phí dịch vụ sản phẩm', 400);
            }

            $productService->delete();
            return response()->json(['message' => 'Xóa phí dịch vụ sản phẩm thành công']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * GET: http://127.0.0.1/bisto_web/shop/product-service-page
     */
    public function shop_product_service_page()
    {
        UserController::AuthShop();
        return view('shop.service.list_product_service');
    }

    /**
     * GET: http://127.0.0.1/bisto_web/shop/product-service-register
     */
    public function shop_product_service_register($id)
    {
        UserController::AuthShop();
        return view('shop.service.product_service_register')->with('id', $id);
    }

    /**
     * GET: http://127.0.0.1/bisto_web/shop/product-services?page=1
     */
    public function GetShopProductServices()
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');
        $productServiceTop = RelServiceProduct::where('user_id', $shop_id)->orderBy('expired_at', 'desc')->first();
        $productServiceRegisteredId = null;

        if ($productServiceTop) {
            $date_now = new DateTime();
            $date_now->setTimezone(new DateTimeZone('Asia/Ho_Chi_Minh'));
            $expired_at = new DateTime($productServiceTop->expired_at);
            if ($date_now < $expired_at) $productServiceRegisteredId = $productServiceTop->product_service_id;
        }
        $productServices = ServiceProduct::orderBy('created_at', 'desc')->paginate(10);
        return response()->json(['results' => $productServices->appends(Input::except('page')), 'product_service_registered_id' => $productServiceRegisteredId]);
    }

    /**
     * GET: http://127.0.0.1/bisto_web/shop/product-services/1
     */
    public function GetShopOneProductServices($id)
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');
        $productService = ServiceProduct::where('id', $id)->first();
        $productServiceTop = RelServiceProduct::where('user_id', $shop_id)->where('product_service_id', $id)->orderBy('expired_at', 'desc')->first();
        $product_ids = array();
        if ($productServiceTop) {
            $date_now = new DateTime();
            $date_now->setTimezone(new DateTimeZone('Asia/Ho_Chi_Minh'));
            $expired_at = new DateTime($productServiceTop->expired_at);
            if ($date_now < $expired_at && $productServiceTop->product_ids) $product_ids = array_map('intval', explode(',', $productServiceTop->product_ids));
        }
        if ($productService) {
            return response()->json(['result' => $productService, 'product_ids' => $product_ids]);
        }
        return response()->json([], 204);
    }

    /**
     * POST: http://127.0.0.1/bisto_web/shop/product-services
     */
    public function ShopProductService(Request $request)
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');
        if (is_null($request->product_service_id)) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $product_service_id = $request->product_service_id;
        $productService = ServiceProduct::where('id', $product_service_id)->first();
        if (is_null($productService)) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $productServiceTop = RelServiceProduct::where('user_id', $shop_id)
            ->where('product_service_id', $product_service_id)->orderBy('expired_at', 'desc')
            ->first();
        if ($productServiceTop) {
            return response()->json(['message' => 'Bạn đã mua gói dịch vụ này, vui lòng chọn mua gói khác'], 400);
        }

        $data = array();
        $data['product_service_id'] = $request->product_service_id;
        $data['user_id'] = $shop_id;
        $date_now = new DateTime();
        $date_now->setTimezone(new DateTimeZone('Asia/Ho_Chi_Minh'));
        $interval = new DateInterval('P' . $productService->date . 'D');
        $data['expired_at'] = $date_now->add($interval);
        $result = RelServiceProduct::insert($data);

        if ($result) {
            return response()->json(['message' => 'Mua gói dịch vụ thành công']);
        }
        return response()->json(['message' => 'Có lỗi xảy ra'], 400);
    }

    /**
     * PUT: http://localhost/bisto_web/shop/product-services/3
     */
    public function ShopApplyProductService(Request $request, $id)
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');
        return $this->applyProductServices($request, $shop_id, $id);
    }

    private function applyProductServices(Request $request, $user_id, $product_service_id)
    {
        $product_ids = $request->product_ids;

        if (is_null($product_ids) || is_null($product_service_id)) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $productService = ServiceProduct::where('id', $product_service_id)->first();
        if (is_null($productService)) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $productServiceTop = RelServiceProduct::where('user_id', $user_id)
            ->where('product_service_id', $product_service_id)->orderBy('expired_at', 'desc')
            ->first();
        if (is_null($productServiceTop)) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        if ($productService->max_product_apply < count($product_ids)) {
            return response()->json(['message' => 'Vui lòng chọn ít sản phẩm hơn'], 400);
        }

        $productServiceTop->product_ids = implode(',', $product_ids);
        $result = $productServiceTop->save();
        if ($result) {
            return response()->json(['message' => 'Cập nhật sản phẩm vào gói dịch vụ thành công'], 201);
        }
        return response()->json(['message' => 'Có lỗi xảy ra, vui lòng thao tác lại'], 400);
    }
}
