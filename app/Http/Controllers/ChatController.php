<?php

namespace App\Http\Controllers;

use App\Message;
use App\Conversation;
use App\File;
use App\Services\FcmService;
use App\User;
use App\UserConversation;
use App\UserDevice;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    /** Admin */
    public function adminShowMessages()
    {
        UserController::AuthAdmin();
        return view('admin.chat.messages');
    }

    /**
     * GET: http://localhost/bisto_web/admin/messages
     */
    public function AdminListMessages()
    {
        UserController::AuthAdmin();
        $admin_id = Session()->get('admin_id');
        return $this->showMessages($admin_id);
    }

    /**
     * GET: http://localhost/bisto_web/admin/messages/3
     */
    public function AdminDetailMessage($id)
    {
        UserController::AuthAdmin();
        $admin_id = Session()->get('admin_id');
        return $this->detailMessages($id, $admin_id);
    }

    /**
     * POST: http://localhost/bisto_web/admin/messages
     */
    public function AdminSendMessages(Request $request)
    {
        UserController::AuthAdmin();
        $admin_id = Session()->get('admin_id');
        $admin_name = Session()->get('admin_name');
        return $this->sendMessage($request, $admin_id, $admin_name);
    }

    /** Admin Shop */
    public function adminShopShowMessages()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.chat.messages');
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/messages
     */
    public function AdminShopListMessages()
    {
        UserController::AuthAdminShop();
        $admin_shop_id = Session()->get('admin_shop_id');
        return $this->showMessages($admin_shop_id);
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/messages/3
     */
    public function AdminShopDetailMessage($id)
    {
        UserController::AuthAdminShop();
        $admin_shop_id = Session()->get('admin_shop_id');
        return $this->detailMessages($id, $admin_shop_id);
    }

    /**
     * POST: http://localhost/bisto_web/admin-shop/messages
     */
    public function AdminShopSendMessages(Request $request)
    {
        UserController::AuthAdmin();
        $admin_shop_id = Session()->get('admin_shop_id');
        $admin_shop_name = Session()->get('admin_shop_name');
        return $this->sendMessage($request, $admin_shop_id, $admin_shop_name);
    }

    /** Shop */
    public function shopShowMessages()
    {
        UserController::AuthShop();
        return view('shop.chat.messages');
    }

    /**
     * GET: http://localhost/bisto_web/shop/messages
     */
    public function ShopListMessages()
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');
        return $this->showMessages($shop_id);
    }

    /**
     * GET: http://localhost/bisto_web/shop/messages/3
     */
    public function ShopDetailMessage($id)
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');
        return $this->detailMessages($id, $shop_id);
    }

    /**
     * POST: http://localhost/bisto_web/shop/messages
     */
    public function ShopSendMessage(Request $request)
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');
        $shop_name = Session()->get('shop_name');
        return $this->sendMessage($request, $shop_id, $shop_name);
    }

    /**
     * Hàm dùng chung
     */
    /* Hiển thị list tin nhắn của khách hàng */
    private function showMessages($user_id)
    {
        $keywords = request()->keyword;
        $chats = array();
        $conversations = UserConversation::where('user_id', $user_id)
            ->get('conversation_id');

        $list_admins = User::where('role', 4)
            ->where('id', '<>', $user_id)
            ->select('id', 'fullname as full_name', 'avatar_id')->get();

        if (is_null($conversations)) {
            $chats = $this->getInfoAdmin($list_admins, $user_id);
            return response()->json(['results' => $chats]);
        }
        $chats_admins = array();

        $conversations = UserConversation::whereIn('conversation_id', $conversations)
            ->join('app_users', 'rel_user_conversations.user_id', '=', 'app_users.id')
            ->where('rel_user_conversations.user_id', '<>', $user_id)
            ->where('fullname', 'like', '%' . $keywords . '%')
            ->get('conversation_id');

        $conversations = Message::whereIn('conversation_id', $conversations)
            ->select('created_at', 'conversation_id')
            ->orderby('created_at', 'desc')
            ->get()->unique('conversation_id');

        foreach ($conversations as $item) {
            $conversation_id = $item->conversation_id;
            $message = Message::where('conversation_id', $conversation_id)
                ->select('user_id', 'content', 'conversation_id',
                    'type', 'created_at')->latest()->first();
            $receiverId = UserConversation::where('conversation_id', $conversation_id)
                ->where('user_id', '<>', $user_id)->first()->user_id;
            $message->receiver_id = $receiverId;
            $message->owner_id = $user_id;
            $this->getUserConversation($message);
            if ($message->user_id === $user_id) {
                $conversation = Conversation::where('id', $message->conversation_id)
                    ->select('user_id')->first();
                if ($conversation->user_id === $user_id) {
                    //Nhắn 1 vs 1
                    $receiver = UserConversation::where('conversation_id', $message->conversation_id)
                        ->where('user_id', '<>', $user_id)->select('user_id')->first();
                    $user = User::where('id', $receiver->user_id)
                        ->select('fullname', 'avatar_id')->first();

                    $profile = '';
                    if ($user->avatar_id) {
                        $profile = File::where('id', $user->avatar_id)->value('url');
                    }
                    $message->full_name = $user->fullname;
                    $message->avatar_image = $profile;

                    //Nhắn với nhóm...
                } else {
                    $user = User::where('id', $conversation->user_id)
                        ->select('fullname', 'avatar_id')->first();

                    $profile = '';
                    if ($user->avatar_id) {
                        $profile = File::where('id', $user->avatar_id)->value('url');
                    }

                    $message->full_name = $user->fullname;
                    $message->avatar_image = $profile;
                }
            }

            if ($list_admins) {
                $count = 0;
                foreach ($list_admins as $index => $admin) {
                    if ($message->user_id === $admin->id || $message->receiver_id === $admin->id) {
                        unset($list_admins[$index]);
                        array_unshift($chats_admins, $message);
                        $count++;
                    }
                }

                if ($count == 0) {
                    array_push($chats, $message);
                }
            } else {
                array_push($chats, $message);
            }
        }

        if ($list_admins) {
            [$items] = $this->getInfoAdmin($list_admins, $user_id);
            foreach ($items as $item) {
                array_unshift($chats_admins, $item);
            }
        }
        foreach ($chats_admins as $item) {
            array_unshift($chats, $item);
        }

        return response()->json(['results' => $chats]);
    }

    /* Chi tiết tin nhắn */
    private function detailMessages($id, $user_id)
    {
        if (is_null($id) || $id == 0) {
            return response()->json([], 204);
        }

        $isExist = UserConversation::where('conversation_id', $id)
            ->where('user_id', $user_id)->first();
        if (is_null($isExist)) {
            return response()->json([], 204);
        }

        $messages = Message::where('conversation_id', $id)
            ->orderBy('created_at', 'DESC')
            ->select('user_id', 'content', 'type', 'created_at')
            ->paginate(10);
        foreach ($messages as $message) {
            $this->getUserConversation($message);
        }
        return response()->json(['results' => $messages]);
    }

    /* Trả lời tin nhắn */
    private function sendMessage(Request $request, $user_id, $full_name)
    {
        if (!$request->contents && !$request->source_urls) {
            return response()->json(['message' => 'Bạn chưa nhập tin nhắn'], 400);
        }

        $conversation_id = $request->conversation_id;
        if (!$conversation_id || $conversation_id == 0) {
            if (!$request->receiver_id) {
                return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
            }

            $data = array();
            $data['user_id'] = $user_id;
            $data['name'] = $full_name;
            $conversation_id = Conversation::insertGetId($data);

            $user_conversation = array();
            $user_conversation['conversation_id'] = $conversation_id;
            $user_conversation['user_id'] = $user_id;
            UserConversation::insert($user_conversation);

            $user_conversation['user_id'] = $request->receiver_id;
            UserConversation::insert($user_conversation);
        }

        $message = new Message();
        $message->conversation_id = $conversation_id;
        $message->content = $request->contents;
        $message->type = 1;
        $message->user_id = $user_id;

        if ($request->source_urls) {
            $imageIds = '';
            $source_urls = $request->source_urls;
            foreach ($source_urls as $source) {
                $imageId = File::saveFiles($source, '', 0, '');
                $imageIds .= $imageId . ',';
            }
            $message->content = substr($imageIds, 0, -1);
            $message->type = 2;
        }

        $result = $message->save();
        $full_name_and_avatar = User::getFullNameAndAvatar($user_id);
        $fcmMessage = $message;
        $fcmMessage->isMes = 1;
        $fcmMessage->receiver_avatar = $full_name_and_avatar['source_avatar'];
        $fcmMessage->full_name = $full_name_and_avatar['full_name'];
        $fcmMessage->source_urls = [];
        if ($request->source_urls) {
            $fcmMessage->source_urls = $request->source_urls;
            $fcmMessage->type = 2;
        }
        $messageService = new FcmService();
        $receiverId = UserConversation::where('conversation_id', $conversation_id)
            ->where('user_id', '<>', $user_id)->first()->user_id;
        $deviceTokens = UserDevice::whereIn('message_notification_type', [3])
            ->where('user_id', $receiverId)->whereNotNull('fcm_token')
            ->pluck('fcm_token')->toArray();

        if (count($deviceTokens) != 0) {
            $messageService->sendBatchMessage($deviceTokens, $fcmMessage);
        }
        if ($result) {
            return response()->json(['message' => 'Gửi tin nhắn thành công', 'conversation_id' => $conversation_id], 201);
        }
        return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
    }

    /* Lấy tên, hình ảnh người nhắn, nhóm */
    private function getUserConversation($message)
    {
        $user = User::where('id', $message->user_id)
            ->select('fullname', 'avatar_id')->first();

        $profile = '';
        if ($user->avatar_id) {
            $profile = File::where('id', $user->avatar_id)->value('url');
        }

        $message->full_name = $user->fullname;
        $message->avatar_image = $profile;
        $message->source_urls = [];

        if ($message->type != 1) {
            $file_ids = explode(',', $message->content);
            $source_urls = array();
            foreach ($file_ids as $id) {
                $url = File::where('id', $id)->value('url');
                array_push($source_urls, $url);
            }
            $message->source_urls = $source_urls;
        }
    }

    /* Lấy thông tin của các admin tổng */
    private function getInfoAdmin($list_admins, $user_id)
    {
        $chats = array();
        if ($list_admins) {
            foreach ($list_admins as $item) {
                $profile = '';
                if ($item->avatar_id) {
                    $profile = File::where('id', $item->avatar_id)->value('url');
                }
                $item->conversation_id = 0;
                $item->avatar_image = $profile;
                $item->content = '';
                $item->source_urls = [];
                $item->receiver_id = $item->id;
                $item->owner_id = $user_id;
                unset($item->id);
            }
            array_push($chats, $list_admins);
        }
        return $chats;
    }
}
