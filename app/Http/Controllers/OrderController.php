<?php

namespace App\Http\Controllers;

use App\BaseOrder;
use App\Category;
use App\City;
use App\District;
use App\Exceptions\CustomException;
use App\GiaoHangNhanh;
use App\OrderItem;
use App\ProductVariety;
use App\Services\Shipping\Ahamove;
use App\Services\Shipping\Ghn;
use App\Services\Shipping\Ghtk;
use App\Ward;
use Illuminate\Http\Request;
use App\ShippingOrder;
use App\ShippingInfo;
use App\Order;
use App\User;
use App\Shop;
use PDF;

class OrderController extends BaseController
{
    /* Shop */
    /*show list order*/
    public function shop_manage_order()
    {
        UserController::AuthShop();
        return view('shop.order.lists');
    }

    /*show order detail view*/
    public function shop_order_detail()
    {
        UserController::AuthShop();
        return view('shop.order.detail');
    }

    /**
     * GET: http://localhost/bisto_web/shop/orders
     */
    public function GetOrdersShop()
    {
        UserController::AuthShop();
        $owner = parent::getShopId();
        $keywords = request()->keyword;
        $orderObj = new Order();

        $year = date('Y');
        $orders = $this->QueryOrder(0, 0, $year, 0, $owner, $orderObj, $keywords);

        if (is_null($orders)) {
            return parent::responseNoContent();
        }

        $orders = parent::paginate($orders);
        return response()->json(['orders' => $orders]);
    }

    /**
     * GET: http://localhost/bisto_web/shop/orders/3
     * @throws CustomException
     */
    public function GetOrderShopById($id)
    {
        UserController::AuthShop();
        $owner = parent::getShopId();
        $orderObj = new Order();

        $order = Order::where('id', $id)->where('seller_id', $owner)
            ->select('id', 'created_at', 'user_id', 'seller_id')
            ->first();

        if (!$order) {
            return parent::responseNoContent();
        }
        return $orderObj->getOrderById($order);
    }

    /**
     * PUT: http://localhost/bisto_web/shop/orders/confirm/3
     */
    public function ConfirmOrderShopById($id): \Illuminate\Http\JsonResponse
    {
        try {
            UserController::AuthShop();
            $seller_id = parent::getShopId();
            $order = Order::where('id', $id)->first();
            if (is_null($order) || $order->status != Order::NGUOI_MUA_DA_TAO_DON) CustomException::makeBadRequest();
            $buyer = User::where('id', $order->user_id)->first();
            $buyer_address = ShippingInfo::where("user_id", $order->user_id)->where("is_default", 1)->first();
            $seller = User::where('id', $seller_id)->first();

            $seller_address = ShippingInfo::where('user_id', $seller_id)->where('is_default', 1)->first();
            $orderItems = OrderItem::where('order_id', $id)->get();
            $shipping_order = ShippingOrder::where('order_id', $id)->first();
            $items = [];
            $cod = $order->payment_status == Order::DA_THANH_TOAN ? 0 : $order->total;
            foreach ($orderItems as $orderItem) {
                $product = ProductVariety::where('rel_product_varieties.id', $orderItem->product_variety_id)
                    ->join('app_products', 'rel_product_varieties.product_id', '=', 'app_products.id')
                    ->select('app_products.name', 'app_products.id as id', 'app_products.category_id')
                    ->first();
                $category_level3 = Category::where('id', $product->category_id)->first();
                $category_level2 = Category::where('id', $category_level3->parent_id)->first();
                $category_level1 = Category::where('id', $category_level2->parent_id)->first();
                switch ($shipping_order->delivery_unit_id) {
                    case  ShippingOrder::GIAO_HANG_NHANH:
                        array_push($items, [
                            'name' => $product->name,
                            'code' => $product->id . "",
                            'quantity' => $orderItem->quantity,
                            'price' => $orderItem->price,
                            'length' => Ghn::LENGTH_DEFAULT,
                            'width' => Ghn::WIDTH_DEFAULT,
                            'height' => Ghn::HEIGHT_DEFAULT,
                            'category' => [
                                'level1' => $category_level1->name,
                                'level2' => $category_level2->name,
                                'level3' => $category_level3->name,
                            ]
                        ]);
                        break;
                    case  ShippingOrder::GHTK:
                        array_push($items, [
                            'name' => $product->name,
                            'quantity' => $orderItem->quantity,
                            'price' => $orderItem->price,
                            'weight' => Ghtk::WEIGHT_DEFAULT,
                            'product_code' => '',
                        ]);
                        break;
                    case  ShippingOrder::AHAMOVE:
                        array_push($items, [
                            '_id' => $product->id,
                            'num' => $orderItem->quantity,
                            'name' => $product->name,
                            'price' => $orderItem->price,
                        ]);
                        break;
                }
            }
            switch ($shipping_order->delivery_unit_id) {
                case  ShippingOrder::GIAO_HANG_NHANH:
                    $shopIdGhn = GiaoHangNhanh::where('shop_id', $seller->shop_id)->first()->giao_hang_nhanh_shop_id;
                    $ghn_store = Ghn::getInstance()->getStoreById($shopIdGhn);
                    $ghn_address_info = Ghn::getInstance()->searchAddressByBisto($buyer_address->city_id, $buyer_address->district_id, $buyer_address->ward_id);
                    if (is_null($ghn_address_info)) throw CustomException::makeBadRequest('Không tìm thấy địa điểm giao hàng');
                    $ghn = array(
                        'items' => $items,
                        'shop_id' => $shopIdGhn,
                        'to_name' => $buyer->fullname,
                        'to_phone' => $buyer->phone,
                        'to_address' => $buyer_address->address,
                        'to_ward_code' => $ghn_address_info['ward']->WardCode,//"20308",//$shipping_info->ward_id,
                        'to_district_id' => $ghn_address_info['district']->DistrictID,//1444,//$shipping_info->district_id + 0,
                        'cod_amount' => $cod,//COD
                        'return_phone' => $seller->phone,
                        'return_address' => $ghn_store->address,
                        'return_district_id' => $ghn_store->district_id,//$seller_address->district_id + 0,
                        'return_ward_code' => $ghn_store->ward_code,//$seller_address->ward_id,
                        'content' => '',
                        'note' => $order->note,
                        'coupon' => null,
                    );
                    $response = Ghn::getInstance()->createOrder(...array_values($ghn));
                    if ($response->getStatusCode() != 200) throw CustomException::makeBadRequest('Xảy ra lỗi với đối tác giao hàng');
                    $body_str = $response->getBody()->getContents();
                    $shipping_order->shipping_object = $body_str;
                    $shipping_object = json_decode($body_str);
                    $shipping_order->code = $shipping_object->data->order_code;
                    $shipping_order->shipping_fee = $shipping_object->data->total_fee;
                    $shipping_order->save();
                    $order->status = Order::NGUOI_BAN_XAC_NHAN_DON;
                    $order->save();
                    return response()->json(['message' => 'Xác nhận đơn hàng thành công']);
                case ShippingOrder::GHTK:
                    $seller_province = City::where('id', $seller_address->city_id)->first();
                    $seller_district = District::where('id', $seller_address->district_id)->first();
                    $seller_ward = Ward::where('id', $seller_address->ward_id)->first();
                    $buyer_province = City::where('id', $buyer_address->city_id)->first();
                    $buyer_district = District::where('id', $buyer_address->district_id)->first();
                    $buyer_ward = Ward::where('id', $buyer_address->ward_id)->first();
                    $ghtk = [
                        'products' => $items,
                        'id' => $id,
                        'pick_name' => $seller->fullname,
                        'pick_money' => $cod,//COD
                        'pick_address_id' => '',
                        'pick_address' => $seller_address->address,
                        'pick_province' => $seller_province->name,
                        'pick_district' => $seller_district->name,
                        'pick_ward' => $seller_ward->name,
                        'pick_street' => $seller_address->address,
                        'pick_tel' => $seller->phone,
                        'pick_email' => $seller->email,
                        'name' => $buyer->fullname,
                        'address' => $buyer_address->address,
                        'province' => $buyer_province->name,
                        'district' => $buyer_district->name,
                        'ward' => $buyer_ward->name,
                        'street' => $buyer_address->address,
                        'hamlet' => 'Khác',
                        'tel' => $buyer->phone,
                        'note' => $order->note,
                        'email' => $buyer->email,
                        'value' => $order->total,// Giá trị đóng bảo hiểm, là căn cứ để tính phí bảo hiểm và bồi thường khi có sự cố phải > 0
                        'is_freeship' => 0,
                        'use_return_address' => 0,
                    ];
                    $response_ghtk = Ghtk::getInstance()->createOrder(...array_values($ghtk));
                    if ($response_ghtk->getStatusCode() != 200) throw CustomException::makeBadRequest('Xảy ra lỗi với đối tác giao hàng');
                    $body_str = $response_ghtk->getBody()->getContents();
                    $shipping_order->shipping_object = $body_str;
                    $shipping_object = json_decode($body_str);
                    if ($shipping_object->success == true) {
                        $shipping_order->code = $shipping_object->order->label;
                        $shipping_order->shipping_fee = $shipping_object->order->fee;
                        $shipping_order->save();
                        $order->status = Order::NGUOI_BAN_XAC_NHAN_DON;
                        $order->save();
                        return response()->json(['message' => 'Xác nhận đơn hàng thành công']);
                    } else if (isset($shipping_object->error)) throw CustomException::makeBadRequest('GHTK: ' . $shipping_object->message);
                    else throw CustomException::makeBadRequest('Xảy ra lỗi với đối tác giao hàng');
                case ShippingOrder::AHAMOVE:
                    $seller_province = City::where('id', $seller_address->city_id)->first();
                    $seller_district = District::where('id', $seller_address->district_id)->first();
                    $seller_ward = Ward::where('id', $seller_address->ward_id)->first();
                    $buyer_province = City::where('id', $buyer_address->city_id)->first();
                    $buyer_district = District::where('id', $buyer_address->district_id)->first();
                    $buyer_ward = Ward::where('id', $buyer_address->ward_id)->first();
                    $path = [
                        [
                            //"address" => "725 Hẻm số 7 Thành Thái, Phường 14, Quận 10, Hồ Chí Minh, Việt Nam",
                            "address" => $seller_address->address . ', ' . $seller_ward->name . ', ' . $seller_district->name . ', ' . $seller_province->name,
                            "short_address" => $seller_address->address,
                            "name" => $seller->fullname,
                            'mobile' => $seller->phone,
                            "remarks" => "Gọi trước cho tôi khi bạn đến",
                            'tracking_number' => $id,
                            'cod' => $cod,//COD
                        ],
                        [
                            //"address" => "Miss Ao Dai Building, 21 Nguyễn Trung Ngạn, Bến Nghé, Quận 1, Hồ Chí Minh, Vietnam",
                            "address" => $buyer_address->address . ', ' . $buyer_ward->name . ', ' . $buyer_district->name . ', ' . $buyer_province->name,
                            "short_address" => $buyer_address->address,
                            "name" => $buyer->fullname,
                            'mobile' => $buyer->phone,
                        ]
                    ];
                    $service_id = Ahamove::SERVICE_ID_DEFAULT;
                    $city_name = vn_to_lowercase($seller_province->name);
                    if (isset(Ahamove::SERVICE_IDS[$city_name])) $service_id = Ahamove::SERVICE_IDS[$city_name];
                    else throw CustomException::makeBadRequest('Ahamove không khả dụng tại địa điểm giao hàng');
                    $ahamove = [
                        'order_time' => 0,
                        'path' => $path,
                        'service_id' => $service_id,
                        'requests' => [],
                        'payment_method' => Ahamove::PAYMENT_METHOD_DEFAULT,
                        'images' => [],
                        'promo_code' => '',
                        'remarks' => "Call me when arrived",
                        'idle_until' => 0,
                        'items' => $items,
                        'type' => null,
                        'need_optimize_route' => null,
                    ];
                    $response_ahamove = Ahamove::getInstance()->createOrder(...array_values($ahamove));
                    if ($response_ahamove->getStatusCode() != 200) throw CustomException::makeBadRequest('Xảy ra lỗi với đối tác giao hàng');
                    $body_str = $response_ahamove->getBody()->getContents();
                    $shipping_order->shipping_object = $body_str;
                    $shipping_object = json_decode($body_str);
                    $shipping_order->code = $shipping_object->order_id;
                    $shipping_order->shipping_fee = $shipping_object->order->total_fee;
                    $shipping_order->save();
                    $order->status = Order::NGUOI_BAN_XAC_NHAN_DON;
                    $order->save();
                    return response()->json(['message' => 'Xác nhận đơn hàng thành công']);
                default :
                    throw CustomException::makeBadRequest('Đơn vị giao hàng không hợp lệ');
            }

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * PUT: http://localhost/bisto_web/shop/orders/cancel/3
     */
    public function CancelOrderShopById($id): \Illuminate\Http\JsonResponse
    {
        try {
            UserController::AuthShop();
            $order = Order::where('id', $id)->first();
            if (is_null($order) || $order->status != Order::NGUOI_MUA_DA_TAO_DON)
                CustomException::makeBadRequest('Không thể hủy đơn hàng');
            $order->status = Order::NGUOI_BAN_HUY_DON;
            $order->save();
            return response()->json(['message' => 'Hủy đơn hàng thành công']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /*
     * GET: http://localhost/bisto_web/shop/orders/print/3
     */
    public function PrintOrderShop($orderId)
    {
        UserController::AuthShop();
        $owner = parent::getShopId();
        $orderObj = new Order();

        $order = Order::where('id', $orderId)->where('seller_id', $owner)
            ->select('created_at', 'user_id', 'payment_method', 'code')
            ->first();
        if (is_null($order)) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $buyer = ShippingInfo::where('user_id', $order->user_id)
            ->where('is_default', 1)
            ->select('city_id', 'district_id', 'ward_id', 'address')
            ->first();
        $user = User::getInformation($order->user_id);
        $buyer->address = User::getAddress($buyer);
        $buyer->phone = $user->phone;
        $buyer->fullname = $user->fullname;

        $seller = ShippingInfo::where('user_id', $owner)
            ->where('is_default', 1)
            ->select('city_id', 'district_id', 'ward_id', 'address')
            ->first();
        $user = User::getInformation($owner);
        $seller->address = User::getAddress($seller);
        $seller->phone = $user->phone;
        $seller->fullname = $user->fullname;

        list($item, $products) = $orderObj->getDetailOrder($orderId);
        $order->total_money = $item->total_money;
        $createdAt = date('d/m/Y', strtotime($order->created_at));
        $hourTime = date('h:m', strtotime($order->created_at));
        $order->created_at = $createdAt . ' ' . $hourTime;
        $order->shipping_fee = ShippingOrder::where('order_id', $orderId)->value('shipping_fee');
        $order->total_order = $order->total_money + $order->shipping_fee;

        $pdf = PDF::loadView('shop.order.print', compact('order', 'products', 'buyer', 'seller'));
        return $pdf->stream();
    }

    /**
     * POST: http://localhost/bisto_web/shop/orders/query
     */
    public function QueryOrdersShop(Request $request)
    {
        UserController::AuthShop();
        $userId = parent::getShopId();
        $orderObj = new Order();

        $option = $request->option;
        $month = $request->month;
        $year = $request->year;
        $status = $request->status;

        if (!is_null($option) && !is_null($year)) {
            $result = $this->QueryOrder($option, $month, $year, $status, $userId, $orderObj);
            if (!$result) {
                return parent::responseNoContent();
            }
            $result = parent::paginate($result);
            return response()->json(['data' => $result]);
        } else {
            return parent::responseBadRequest();
        }
    }

    /* Admin-Shop */
    public function admin_shop_manage_order()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.order.lists');
    }

    /*show order detail view*/
    public function admin_shop_order_detail()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.order.detail');
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/orders
     */
    public function GetOrdersAdminShop()
    {
        UserController::AuthAdminShop();
        $owner = parent::getAdminShopId();
        $keywords = request()->keyword;
        $orderObj = new Order();

        $year = date('Y');
        $userIds = Shop::getListsChildShop($owner);
        $orders = $this->QueryOrder(0, 0, $year, 0, $userIds, $orderObj, $keywords);
        if (!$orders) {
            return parent::responseNoContent();
        }

        $orders = parent::paginate($orders);
        return response()->json(['orders' => $orders]);
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/orders/3
     */
    public function GetOrderAdminShopById($id)
    {
        UserController::AuthAdminShop();
        $orderObj = new Order();
        $order = Order::where('id', $id)
            ->select('id', 'created_at', 'user_id', 'seller_id')
            ->first();
        return $orderObj->getOrderById($order);
    }

    /**
     * POST: http://localhost/bisto_web/admin-shop/orders/query
     */
    public function QueryOrdersAdminShop(Request $request)
    {
        UserController::AuthAdminShop();
        $userId = parent::getAdminShopId();
        $orderObj = new Order();

        $option = $request->option;
        $month = $request->month;
        $year = $request->year;
        $status = $request->status;
        $shopId = $request->shopId;

        if (!is_null($option) && !is_null($year)) {
            if (is_null($shopId)) {
                return parent::responseBadRequest();
            }

            if ($shopId == 0) {
                $userIds = Shop::getListsChildShop($userId);
                $result = $this->QueryOrder($option, $month, $year, $status, $userIds, $orderObj);
            } else {
                $result = $this->QueryOrder($option, $month, $year, $status, $shopId, $orderObj);
            }

            if (!$result) {
                return parent::responseNoContent();
            }
            $result = parent::paginate($result);
            return response()->json(['data' => $result]);
        } else {
            return parent::responseBadRequest();
        }
    }

    /* Admin */
    public function admin_manage_order()
    {
        UserController::AuthAdmin();
        return view('admin.order.lists');
    }

    /*show order detail view*/
    public function admin_order_detail()
    {
        UserController::AuthAdmin();
        return view('admin.order.detail');
    }

    /**
     * GET: http://localhost/bisto_web/admin/orders
     */
    public function GetOrdersAdmin()
    {
        UserController::AuthAdmin();
        $keywords = request()->keyword;
        $orderObj = new Order();

        $userIds = Shop::getListsShop();
        $year = date('Y');

        $orders = $this->QueryOrder(0, 0, $year, 0, $userIds, $orderObj, $keywords);
        if ($orders) {
            $orders = parent::paginate($orders);
        }

        $adminShops = User::where('role', 3)
            ->join('app_shops', 'app_users.shop_id', '=', 'app_shops.id')
            ->select('app_users.id as user_id', 'app_shops.name as shop_name')
            ->get();
        return response()->json(['orders' => $orders, 'admin_shops' => $adminShops]);
    }

    /**
     * GET: http://localhost/bisto_web/admin/orders/3
     */
    public function GetOrdersAdminById($id)
    {
        UserController::AuthAdmin();
        $orderObj = new Order();
        $order = Order::where('id', $id)
            ->select('id', 'created_at', 'user_id', 'seller_id')
            ->first();

        return $orderObj->getOrderById($order);
    }

    /**
     * POST: http://localhost/bisto_web/admin/orders/query
     */
    public function QueryOrdersAdmin(Request $request)
    {
        UserController::AuthAdmin();
        $orderObj = new Order();

        $option = $request->option;
        $month = $request->month;
        $year = $request->year;
        $status = $request->status;
        $shopId = $request->admin_shop_id;

        if (!is_null($option) && !is_null($year) && !is_null($shopId)) {
            if (is_null($shopId)) {
                return parent::responseBadRequest();
            }

            if ($shopId != 0) {
                $userIds = Shop::getListsChildShop($shopId);
            } else {
                $userIds = Shop::getListsShop();
            }

            $result = $this->QueryOrder($option, $month, $year, $status, $userIds, $orderObj);
            if (!$result) {
                return parent::responseNoContent();
            }

            $result = parent::paginate($result);
            return response()->json(['result' => $result]);
        } else {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }
    }

    /**
     * Hàm thực hiện query lên order theo shop hay admin-shop
     */
    private function QueryOrder($option, $month, $year, $status, $userIds, BaseOrder $baseOrder, $keyword = null)
    {
        $order_ids = array();
        if (is_array($userIds)) {
            $order = Order::whereIn('seller_id', $userIds)
                ->select('id')->get();
        } else {
            $order = Order::where('seller_id', $userIds)
                ->select('id')->get();
        }

        if (!count($order)) {
            return array();
        }

        foreach ($order as $item) {
            array_push($order_ids, $item->id);
        }

        $data['month'] = $month;
        $data['year'] = $year;

        if (!is_null($option) && !is_null($year)) {
            if ($month) {
                if ($option == 0) {
                    return $baseOrder->getOrdersOfMonth($data, $order_ids, $status, $keyword);
                }

                if ($option == 1) {
                    return $baseOrder->getOrdersFirstMonth($data, $order_ids, $status, $keyword);
                }

                if ($option == 2) {
                    return $baseOrder->getOrdersLastMonth($data, $order_ids, $status, $keyword);
                }
            } else {
                if ($option == 0) {
                    return $baseOrder->getOrdersOfYear($data, $order_ids, $status, $keyword);
                }

                if ($option == 1) {
                    return $baseOrder->getOrdersFirstYear($data, $order_ids, $status, $keyword);
                }

                if ($option == 2) {
                    return $baseOrder->getOrdersLastYear($data, $order_ids, $status, $keyword);
                }
            }
        } else {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }
    }
}
