<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bcoin;

class BcoinController extends Controller
{
    /**
     * POST: http://localhost/bisto_web/admin/bcoins
     */
    public function InsertBcoinAdmin(Request $request)
    {
        UserController::AuthAdmin();
        return $this->edit_bcoin($request);
    }

    public function edit_bcoin(Request $request)
    {
        $user = $request->user_id;
        $bcoin = Bcoin::where('user_id', $user)->first();
        if ($bcoin) {
            $bcoin->amount += $request->amount;

            $result = $bcoin->save();
            if ($result) {
                return response()->json(['message' => 'Cập nhật bcoin thành công']);
            }
        } else {
            $data = array();

            $data['amount'] = $request->amount;
            $data['user_id'] = $user;
            $result = Bcoin::insert($data);
            if ($result) {
                return response()->json(['message' => 'Thêm bcoins thành công'], 201);
            }
        }

        return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
    }
}
