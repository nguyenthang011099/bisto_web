<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

class BaseController extends Controller
{
    protected function getShopId()
    {
        return Session()->get('shop_id');
    }

    protected function getAdminShopId()
    {
        return Session()->get('admin_shop_id');
    }

    protected function getAdminId()
    {
        Session()->get('admin_id');
    }

    protected function setCache($key, $data, $isExistCache)
    {
        $expiredAt = Carbon::now()->addMinutes(10);
        if ($isExistCache) {
            Cache::put($key, $data, $expiredAt);
        } else {
            Cache::add($key, $data, $expiredAt);
        }
    }

    protected function paginate($items, $page = null, $perPage = 10)
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $total = count($items);
        $offset = ($page * $perPage) - $perPage;

        if (!is_array($items)) {
            $items = $items->toArray();
        }
        $itemsDisplay = array_slice($items, $offset, $perPage);
        return new LengthAwarePaginator($itemsDisplay, $total, $perPage);
    }

    protected function responseNoContent()
    {
        return response()->json(['message' => 'Không có dữ liệu'], 204);
    }

    protected function responseBadRequest()
    {
        return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
    }
}
