<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogCategory;
use App\Exceptions\CustomException;
use App\File;
use App\RelTag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogCategoryController extends Controller
{
    public function AdminListsBlogCategory()
    {
        UserController::AuthAdmin();
        return view('admin.blog-category.lists');
    }

    /**
     * GET: http://localhost/bisto_web/admin/blog-categories
     */
    public function AdminGetBlogCategories(): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdmin();
        $id = Session()->get('admin_id');

        $keyword = request()->keyword;
        $blogCategories = BlogCategory::orderBy('app_blog_categories.created_at', 'DESC')
            ->where('name', 'like', '%' . $keyword . '%')
            ->select('id', 'name', 'created_at', 'updated_at')
            ->paginate(10);

        foreach ($blogCategories as $blogCategory) {
            $blogCategory['count_blog'] = Blog::where('blog_category_id', $blogCategory->id)->count();
        }

        return response()->json(['success' => 1, 'results' => $blogCategories]);
    }

    /**
     * GET: http://localhost/bisto_web/admin/blog-categories/2
     */
    public function AdminGetBlogCategoryById(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdmin();
        $user_id = Session()->get('admin_id');

        $blogCategory = BlogCategory::where('id', $id)
            ->select('id', 'name', 'created_at', 'updated_at')
            ->first();
        return response()->json(['result' => $blogCategory]);
    }

    /**
     * POST: http://localhost/bisto_web/admin/blog-categories
     * @throws CustomException
     */
    public function AdminPostBlogCategory(Request $request)
    {
        try {
            UserController::AuthAdmin();
            $data = $request->all();
            $user_id = Session()->get('admin_id');
            $blogCategory = new BlogCategory();
            $blogCategory->name = $data['name'];
            $blogCategory->slug = $data['slug'];

            $result = $blogCategory->save();
            return response()->json(['message' => 'Thêm danh mục blog thành công'], 201);
        } catch (\Exception $e) {
            throw CustomException::makeServerError($e->getMessage());
        }
    }

    /**
     * PUT: http://localhost/bisto_web/admin/blog-categories/2
     */
    public function AdminPutBlogCategory(Request $request, $id)
    {
        UserController::AuthAdmin();
        $user_id = Session()->get('admin_id');

        $blogCategory = BlogCategory::where('id', $id)->first();
        if (is_null($blogCategory)) {
            return response()->json([], 204);
        }
        $blogCategory->name = $request['name'];
        $result = $blogCategory->save();
        if ($result) {
            return response()->json(['message' => 'Cập nhật danh mục blog thành công']);
        }
        return response()->json(['message', 'Dữ liệu không hợp lệ'], 400);
    }

    /**
     * DELETE: http://localhost/bisto_web/admin/blog-categories/2
     */
    public function AdminDeleteBlogCategory($id)
    {
        UserController::AuthAdmin();
        $blogCategory = BlogCategory::where('id', $id)->first();
        if (is_null($blogCategory)) {
            return response()->json([], 204);
        }

        $blogCategory->delete();
        return response()->json(['message' => 'Xóa danh mục blog thành công']);
    }

    /**
     * GET: http://localhost/bisto_web/_blog-categories
     */
    public function GetBlogCategories(): \Illuminate\Http\JsonResponse
    {
        $keyword = request()->keyword;
        $limit = request()->limit ?? 10;
        $blogCategories = BlogCategory::orderBy('app_blog_categories.created_at', 'DESC')
            ->where('name', 'like', '%' . $keyword . '%')
            ->select('id', 'name', 'slug', 'created_at', 'updated_at')
            ->paginate($limit);
        return response()->json(['success' => 1, 'results' => $blogCategories]);
    }

    public function blog_list_by_category($slug)
    {
        $blog_category = BlogCategory::where('slug', $slug)->first();
        $type = Blog::TYPE_NORMAL;
        if ($slug == Blog::SLUG_HOT_PAGE) {
            $type = Blog::TYPE_HOT;
            $blog_category = ['name' => 'Tin nổi bật'];
        }
        if (is_null($blog_category)) return view('404page');
        return view('blog-page.blog-list-by-category')->with(compact('slug', 'type', 'blog_category'));;
    }
}
