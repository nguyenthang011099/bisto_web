<?php

namespace App\Http\Controllers;

use App\Bcoin;
use App\Brand;
use App\Exceptions\CustomException;
use App\UserTransaction;
use DateInterval;
use DateTime;
use DateTimeZone;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\ServiceBrand;
use App\File;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ServiceBrandController extends Controller
{
    /** Admin */
    public function admin_service_brand()
    {
        UserController::AuthAdmin();
        return view('admin.brand.lists_service_brand');
    }

    /** admin-shop */
    public function admin_shop_service_brand()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.brand.lists_service_brand');
    }

    /** shop */
    public function shop_service_brand()
    {
        UserController::AuthShop();
        return view('shop.brand.lists_service_brand');
    }

    /**
     * GET: http://localhost/bisto_web/admin/banners
     */
    public function GetServiceBrandsAdmin()
    {
        UserController::AuthAdmin();
        $user = Session()->get('admin_id');

        $keyword = request()->keyword;
        $serviceBrands = ServiceBrand::orderBy('created_at', 'DESC')->where('user_id', $user)
            ->where('name', 'like', '%' . $keyword . '%')->paginate(10);
        return response()->json(['results' => $serviceBrands->appends(Input::except('page'))], 200);
    }

    /**
     * GET: http://localhost/bisto_web/admin/banners
     */
    public function GetServiceBrandsShop(): \Illuminate\Http\JsonResponse
    {
        UserController::AuthShop();
        $user_id = Session()->get('shop_id');
        return $this->getServiceBrands($user_id);
    }

    /**
     * @throws CustomException
     */
    public function PostServiceBrandsShop(Request $request): \Illuminate\Http\JsonResponse
    {
        UserController::AuthShop();
        $user_id = Session()->get('shop_id');
        return $this->registerServiceBrand($user_id, $request);
    }

    /**
     * GET: http://localhost/bisto_web/admin/banners
     */
    public function GetServiceBrandsAdminShop(): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdminShop();
        $user_id = Session()->get('admin_shop_id');
        return $this->getServiceBrands($user_id);
    }

    /**
     * @throws CustomException
     */
    public function PostServiceBrandsAdminShop(Request $request): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdminShop();
        $user_id = Session()->get('admin_shop_id');
        return $this->registerServiceBrand($user_id, $request);
    }

    private function getServiceBrands($user_id): \Illuminate\Http\JsonResponse
    {
        $brand = Brand::where('user_id', $user_id)->orderBy('end_at', 'desc')->first();
        if ($brand) {
            $date_now = new DateTime();
            $date_now->setTimezone(new DateTimeZone('Asia/Ho_Chi_Minh'));
            $end_at = new DateTime($brand->end_at);
            if ($date_now > $end_at) {
                $brand = null;
            }
        }
        $keyword = request()->keyword;
        $serviceBrands = ServiceBrand::orderBy('created_at', 'DESC')
            ->where('name', 'like', '%' . $keyword . '%')->paginate(10);
        return response()->json(['results' => $serviceBrands->appends(Input::except('page')), 'current_brand' => $brand], 200);
    }

    private function registerServiceBrand($user_id, $request): \Illuminate\Http\JsonResponse
    {
        try {
            DB::transaction(function () use ($user_id, $request) {
                $serviceBrand = ServiceBrand::where('id', $request->service_brand_id)->first();
                $bcoin = Bcoin::where('user_id', $user_id)->first();
                $price = $serviceBrand->price;
                if ($price > $bcoin->amount) {
                    throw CustomException::makeBadRequest("Số bcoin của bạn hiện tại là {$bcoin->amount} không đủ, vui lòng nạp thêm");
                } else {
                    $bcoin->amount = $bcoin->amount - $price;
                    $bcoin->save();
                }
                $data = new Brand();
                $data['service_brand_id'] = $request->service_brand_id;
                $_start_at = new DateTime();
                $_start_at->setTimezone(new DateTimeZone('Asia/Ho_Chi_Minh'));
                $data['start_at'] = $request->start_at ?? $_start_at;

                $start_at = new DateTime($request->start_at ?? $_start_at);
                $interval = new DateInterval('P' . $serviceBrand->day . 'D');
                $end_at = $start_at->add($interval);
                $data['end_at'] = $end_at;
                $data['user_id'] = $user_id;
                $data->save();
            });
            return response()->json(['message' => 'Đăng ký dịch vụ thành công']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * GET: http://localhost/bisto_web/admin/banners/2
     */
    public function GetAdminServiceBrandById($id)
    {
        UserController::AuthAdmin();
        $user_id = Session()->get('admin_id');
        $serviceBrand = ServiceBrand::where('id', $id)->where('user_id', $user_id)->first();
        return response()->json(['result' => $serviceBrand], 200);
    }


    /**
     * POST: http://localhost/bisto_web/admin/banners
     */
    public function InsertServiceBrandAdmin(Request $request)
    {
        try {
            UserController::AuthAdmin();
            $user_id = Session()->get('admin_id');
            ServiceBrand::insertServiceBrand($request->day, $request->name, $request->price, $user_id);
            return response()->json(['message' => 'Thêm dịch vụ thành công']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * PUT: http://localhost/bisto_web/admin/banners/2
     */
    public function UpdateServiceBrandAdmin(Request $request, $id)
    {
        try {
            UserController::AuthAdmin();
            $user_id = Session()->get('admin_id');
            ServiceBrand::updateServiceBrand($id, $request->day, $request->name, $request->price, $user_id);
            return response()->json(['message' => 'Cập nhật dịch vụ thành công']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * DELETE: http://localhost/bisto_web/admin/banners/2
     */
    public function DeleteServiceBrandAdmin($id)
    {
        try {
            UserController::AuthAdmin();
            $user_id = Session()->get('admin_id');
            ServiceBrand::deleteServiceBrand($id, $user_id);
            return response()->json(['message' => 'Xóa dịch vụ thành công']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}
