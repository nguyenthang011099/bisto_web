<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Exceptions\CustomException;
use App\File;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function ListsBrand()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.brand.lists');
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/brands
     */
    public function GetBrands()
    {
        UserController::AuthAdminShop();
        $id = Session()->get('admin_shop_id');

        $keywords = request()->keyword;
        $brands = Brand::where('user_id', $id)
            ->where('name', 'like', '%' . $keywords . '%')
            ->select('app_brands.id', 'app_brands.image_id', 'app_brands.created_at',
                'app_files.url', 'app_brands.name')
            ->join('app_files', 'app_brands.image_id', '=', 'app_files.id')
            ->orderBy('created_at', 'DESC')
            ->paginate(10);

        if ($brands) {
            return response()->json(['success' => 1, 'brands' => $brands]);
        }
        return response()->json([], 204);
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/brands/2
     */
    public function GetBrandById($id)
    {
        UserController::AuthAdminShop();
        $data = Brand::where('app_brands.id', $id)->select('app_brands.id', 'app_brands.name',
            'app_files.url', 'app_brands.user_id', 'app_brands.image_id')
            ->join('app_files', 'app_brands.image_id', '=', 'app_files.id')->first();

        if ($data) {
            return response()->json(['brand' => $data]);
        }
        return response()->json([], 204);
    }

    /**
     * POST: http://localhost/bisto_web/admin-shop/brands
     * @throws CustomException
     */
    public function SaveBrand(Request $request)
    {
        try {
            UserController::AuthAdminShop();
            $data = $request->all();
            $user_id = Session()->get('admin_shop_id');

            $isExist = Brand::where('user_id', $user_id)->first();
            if ($isExist) {
                throw CustomException::makeBadRequest('Chuỗi cửa hàng đã có thương hiệu');
            }

            if (is_null($data['source_url'])) {
                throw CustomException::makeBadRequest('Chọn hình ảnh cần thêm');
            }

            $imageId = File::saveFiles($request->source_url['url'], $request->source_url['filename'], $request->source_url['size'], $request->source_url['type']);

            $brand = new Brand();
            $brand->image_id = $imageId;
            $brand->user_id = $user_id;
            $brand->name = $data['name'];
            $result = $brand->save();
            return response()->json(['message' => 'Thêm thương hiệu thành công'], 201);
        } catch (\Exception $e) {
            throw CustomException::makeServerError($e->getMessage(), 500);
        }
    }

    /**
     * PUT: http://localhost/bisto_web/admin-shop/brands/2
     */
    public function UpdateBrand(Request $request, $id)
    {
        UserController::AuthAdminShop();
        $user = Session()->get('admin_shop_id');

        $brand = Brand::where('id', $id)->where('user_id', $user)->first();
        if (is_null($brand)) {
            return response()->json([], 204);
        }

        $oldImage = File::where('id', $brand->image_id)->first();

        if ($request->source_url) {
            $oldImage->url = $request->source_url['url'];
            $oldImage->size = $request->source_url['size'];
            $oldImage->type = $request->source_url['type'];
            $oldImage->hash = $request->source_url['hash'];
            $oldImage->filename = $request->source_url['filename'];
            $oldImage->save();
        }

        $brand->name = $request->name;
        $result = $brand->save();

        if ($result) {
            return response()->json(['message' => 'Cập nhật thương hiệu thành công']);
        }
        return response()->json(['message', 'Dữ liệu không hợp lệ'], 400);
    }

    /**
     * DELETE: http://localhost/bisto_web/admin-shop/brands/2
     */
    public function DeleteBrand($id)
    {
        UserController::AuthAdminShop();
        $brand = Brand::where('id', $id)->where('user_id', Session()->get('admin_shop_id'))->first();
        if (is_null($brand)) {
            return response()->json([], 204);
        }

        File::where('id', $brand->image_id)->delete();
        $brand->delete();
        return response()->json(['message' => 'Xóa thương hiệu thành công']);
    }
}
