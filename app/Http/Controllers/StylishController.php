<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use App\Stylish;
use App\Http\Requests\StylishRequest;
use Illuminate\Support\Facades\Input;

class StylishController extends Controller
{
    /**
     * GET: http://localhost/bisto_web/admin/stylishes
     */
    public function GetStylishes()
    {
        UserController::AuthAdmin();
        $keyword = request()->keyword;
        $list_stylishes = Stylish::where('name', 'like', '%' . $keyword . '%')->orderBy('created_at', 'desc')->paginate(10);
        foreach ($list_stylishes as $item) {
            $icon_id = $item->icon_id;
            $item->icon = File::find($icon_id);
        }
        return response()->json(['results' => $list_stylishes->appends(Input::except('page'))], 200);
    }

    /**
     * GET: http://localhost/bisto_web/admin/stylishes/3
     */
    public function GetStylishById($id)
    {
        UserController::AuthAdmin();
        $stylish = Stylish::where('id', $id)->first();
        if (!is_null($stylish))
            $stylish->icon = File::find($stylish->icon_id);
        return response()->json(['result' => $stylish], 200);
    }

    /**
     * POST: http://localhost/bisto_web/admin/stylishes
     */
    public function PostStylish(StylishRequest $request)
    {
        try {
            UserController::AuthAdmin();
            $data = array();
            $data['name'] = $request->name;
            $data['type'] = $request->type;
            $data['icon_id'] = File::insertGetId($request->icon);
            $result = Stylish::insert($data);
            return response()->json(['message' => 'Thêm phong cách thành công'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * PUT: http://localhost/bisto_web/admin/stylishes/3
     */
    public function PutStylish(Request $request, $id)
    {
        try {
            UserController::AuthAdmin();
            $data = array();
            $data['name'] = $request->name;
            $data['type'] = $request->type;
            $stylish = Stylish::find($id);
            $oldIcon = File::find($stylish->icon_id);

            if (is_null($oldIcon)) {
                $data['icon_id'] = File::insertGetId($request->icon);
            } else  File::find($stylish->icon_id)->update($request->icon);

            $result = Stylish::where('id', $id)->update($data);
            return response()->json(['message' => 'Cập nhật phong cách thành công']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    /**
     * DELETE: http://localhost/bisto_web/admin/stylishes/3
     */
    public function DeleteStylish($id)
    {
        UserController::AuthAdmin();
        $result = Stylish::where('id', $id)->delete();

        if ($result) {
            return response()->json(['message' => 'Xoá dữ liệu thành công']);
        }
        return response()->json([], 204);
    }

    public function lists_stylishes()
    {
        UserController::AuthAdmin();
        return view('admin.stylish.lists');
    }
}
