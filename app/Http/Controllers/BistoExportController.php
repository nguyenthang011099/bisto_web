<?php

namespace App\Http\Controllers;

use App\Banner;
use App\BaseOrder;
use App\Brand;
use App\Exports\RevenueBistoOneExport;
use App\Exports\RevenueBistoTwoExport;
use App\Exports\SubsidyBistoExport;
use App\RelServiceProduct;
use App\RevenueBistoOne;
use App\Subsidy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class BistoExportController extends BaseController
{
    /**
     * GET: http://127.0.0.1:8000/admin/revenue-bisto-one?option=0&month=&precious=3&year=2021
     */
    public function ExportExcelRevenueBistoOne()
    {
        UserController::AuthAdmin();
        $option = request()->option;
        $month = request()->month;
        $precious = request()->precious;
        $year = request()->year;
        $bistoOne = new RevenueBistoOne();

        $isExistCache = Cache::has('revenue_bisto_one');
        if ($isExistCache) {
            $data = Cache::get('revenue_bisto_one');
        } else {
            $data = $this->QueryOrder($option, $month, $precious, $year, $bistoOne);
        }

        if (!$data) {
            return response()->json(['result' => 'Không có dữ liệu xuất khẩu'], 204);
        }

        $subTitle = $bistoOne->convertDateTime($option, $month, $precious, $year);
        $information = (object)[
            'SubTitle' => $subTitle
        ];
        return Excel::download(new RevenueBistoOneExport($data, $information), 'Doanh thu bisto 1_' . date('d-m-Y_H-i-s') . '.xlsx');
    }

    /**
     * POST: http://127.0.0.1:8000/admin/revenue-bisto-one
     */
    public function RevenueBistoOne(Request $request)
    {
        UserController::AuthAdmin();
        $option = $request->option;
        $month = $request->month;
        $precious = $request->precious;
        $year = $request->year;
        $page = request()->page;
        $bistoOne = new RevenueBistoOne();

        $isExistCache = Cache::has('revenue_bisto_one');
        if ($isExistCache) {
            $data = Cache::get('revenue_bisto_one');
        } else {
            $data = $this->QueryOrder($option, $month, $precious, $year, $bistoOne);
        }

        if (!$data) {
            return response()->json(['result' => 'Không có dữ liệu'], 204);
        }

        parent::setCache('revenue_bisto_one', $data, $isExistCache);
        $result = parent::paginate($data, $page);
        return response()->json(['result' => $result]);
    }

    /**
     *
     * GET: http://127.0.0.1:8000/admin/revenue-bisto-two?option=0&month=&precious=3&year=2021
     */
    public function ExportExcelRevenueBistoTwo()
    {
        UserController::AuthAdmin();
        $option = request()->option;
        $month = request()->month;
        $precious = request()->precious;
        $year = request()->year;

        $isExistCache = Cache::has('revenue_bisto_two');
        if ($isExistCache) {
            $data = Cache::get('revenue_bisto_two');
        } else {
            $data = $this->QueryRevenueBistoTwo($option, $month, $precious, $year);
        }

        if (!$data) {
            return response()->json(['result' => 'Không có dữ liệu xuất khẩu'], 204);
        }

        $bistoTwo = new RevenueBistoOne();
        $subTitle = $bistoTwo->convertDateTime($option, $month, $precious, $year);
        $information = (object)[
            'SubTitle' => $subTitle
        ];
        return Excel::download(new RevenueBistoTwoExport($data, $information), 'Doanh thu bisto 2_' . date('d-m-Y_H-i-s') . '.xlsx');
    }

    /**
     * POST: http://127.0.0.1:8000/admin/revenue-bisto-two
     */
    public function RevenueBistoTwo(Request $request)
    {
        UserController::AuthAdmin();
        $option = $request->option;
        $month = $request->month;
        $precious = $request->precious;
        $year = $request->year;
        $page = request()->page;

        $isExistCache = Cache::has('revenue_bisto_two');
        if ($isExistCache) {
            $data = Cache::get('revenue_bisto_two');
        } else {
            $data = $this->QueryRevenueBistoTwo($option, $month, $precious, $year);
        }

        if (!$data) {
            return response()->json(['result' => 'Không có dữ liệu'], 204);
        }

        parent::setCache('revenue_bisto_two', $data, $isExistCache);
        $result = parent::paginate($data, $page);
        return response()->json(['result' => $result]);
    }

    /**
     * GET: http://127.0.0.1:8000/admin/subsidy-bisto?option=0&month=&precious=3&year=2021
     */
    public function ExportExcelSubsidyBisto()
    {
        UserController::AuthAdmin();
        $option = request()->option;
        $month = request()->month;
        $precious = request()->precious;
        $year = request()->year;
        $subsidy = new Subsidy();

        $isExistCache = Cache::has('subsidy_bisto');
        if ($isExistCache) {
            $data = Cache::get('subsidy_bisto');
        } else {
            $data = $this->QueryOrder($option, $month, $precious, $year, $subsidy);
        }

        if (!$data) {
            return response()->json(['result' => 'Không có dữ liệu xuất khẩu'], 204);
        }

        $subTitle = $subsidy->convertDateTime($option, $month, $precious, $year);
        $information = (object)[
            'SubTitle' => $subTitle
        ];
        return Excel::download(new SubsidyBistoExport($data, $information), 'Bisto trợ giá_' . date('d-m-Y_H-i-s') . '.xlsx');
    }

    /**
     * POST: http://127.0.0.1:8000/admin/subsidy-bisto
     */
    public function SubsidyBisto(Request $request)
    {
        UserController::AuthAdmin();
        $option = $request->option;
        $month = $request->month;
        $precious = $request->precious;
        $year = $request->year;
        $page = request()->page;

        $isExistCache = Cache::has('subsidy_bisto');
        if ($isExistCache) {
            $data = Cache::get('subsidy_bisto');
        } else {
            $data = $this->QueryOrder($option, $month, $precious, $year, new Subsidy());
        }

        if (!$data) {
            return response()->json(['result' => 'Không có dữ liệu'], 204);
        }

        parent::setCache('subsidy_bisto', $data, $isExistCache);
        $result = parent::paginate($data, $page);
        return response()->json(['result' => $result]);
    }

    private function QueryOrder($option, $month, $precious, $year, BaseOrder $subsidy)
    {
        $order_ids = array();
        $order = DB::table('app_orders')
            ->join('app_coupons', 'app_coupons.id', '=', 'app_orders.coupon_id')
            ->where('app_coupons.type_creater', 4)
            ->select('app_orders.id')
            ->get();

        if (!count($order)) {
            return array();
        }

        foreach ($order as $item) {
            array_push($order_ids, $item->id);
        }

        $data['month'] = $month;
        $data['precious'] = $precious;
        $data['year'] = $year;

        if (!is_null($option) && !is_null($year)) {
            if ($month) {
                if ($option == 0) {
                    return $subsidy->getOrdersOfMonth($data, $order_ids, null);
                }

                if ($option == 1) {
                    return $subsidy->getOrdersFirstMonth($data, $order_ids, null);
                }

                if ($option == 2) {
                    return $subsidy->getOrdersLastMonth($data, $order_ids, null);
                }
            } else if ($precious) {
                if ($option == 0) {
                    return $subsidy->getOrdersOfPrecious($data, $order_ids, null);
                }

                if ($option == 1) {
                    return $subsidy->getOrdersFirstPrecious($data, $order_ids, null);
                }

                if ($option == 2) {
                    return $subsidy->getOrdersLastPrecious($data, $order_ids, null);
                }
            } else {
                if ($option == 0) {
                    return $subsidy->getOrdersOfYear($data, $order_ids, null);
                }

                if ($option == 1) {
                    return $subsidy->getOrdersFirstYear($data, $order_ids, null);
                }

                if ($option == 2) {
                    return $subsidy->getOrdersLastYear($data, $order_ids, null);
                }
            }
        } else {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }
    }

    private function QueryRevenueBistoTwo($option, $month, $precious, $year)
    {
        $result = array();
        $data['month'] = $month;
        $data['precious'] = $precious;
        $data['year'] = $year;

        if (!is_null($option) && !is_null($year)) {
            if ($month) {
                if ($option == 0) {
                    $item = Banner::getBannersOfMonth($data);
                    array_push($result, ...$item);
                    $item = Brand::getBrandsOfMonth($data);
                    array_push($result, ...$item);
                    $item = RelServiceProduct::getProductsOfMonth($data);
                    array_push($result, ...$item);
                }

                if ($option == 1) {
                    $item = Banner::getBannersFirstMonth($data);
                    array_push($result, ...$item);
                    $item = Brand::getBrandsFirstMonth($data);
                    array_push($result, ...$item);
                    $item = RelServiceProduct::getProductsFirstMonth($data);
                    array_push($result, ...$item);
                }

                if ($option == 2) {
                    $item = Banner::getBannersLastMonth($data);
                    array_push($result, ...$item);
                    $item = Brand::getBrandsLastMonth($data);
                    array_push($result, ...$item);
                    $item = RelServiceProduct::getProductsLastMonth($data);
                    array_push($result, ...$item);
                }
            } else if ($precious) {
                if ($option == 0) {
                    $item = Banner::getBannersOfPrecious($data);
                    array_push($result, ...$item);
                    $item = Brand::getBrandsOfPrecious($data);
                    array_push($result, ...$item);
                    $item = RelServiceProduct::getProductsOfPrecious($data);
                    array_push($result, ...$item);
                }

                if ($option == 1) {
                    $item = Banner::getBannersFirstPrecious($data);
                    array_push($result, ...$item);
                    $item = Brand::getBrandsFirstPrecious($data);
                    array_push($result, ...$item);
                    $item = RelServiceProduct::getProductsFirstPrecious($data);
                    array_push($result, ...$item);
                }

                if ($option == 2) {
                    $item = Banner::getBannersLastPrecious($data);
                    array_push($result, ...$item);
                    $item = Brand::getBrandsLastPrecious($data);
                    array_push($result, ...$item);
                    $item = RelServiceProduct::getProductsLastPrecious($data);
                    array_push($result, ...$item);
                }
            } else {
                if ($option == 0) {
                    $item = Banner::getBannersOfYear($data);
                    array_push($result, ...$item);
                    $item = Brand::getBrandsOfYear($data);
                    array_push($result, ...$item);
                    $item = RelServiceProduct::getProductsOfYear($data);
                    array_push($result, ...$item);
                }

                if ($option == 1) {
                    $item = Banner::getBannersFirstYear($data);
                    array_push($result, ...$item);
                    $item = Brand::getBrandsFirstYear($data);
                    array_push($result, ...$item);
                    $item = RelServiceProduct::getProductsFirstYear($data);
                    array_push($result, ...$item);
                }

                if ($option == 2) {
                    $item = Banner::getBannersLastYear($data);
                    array_push($result, ...$item);
                    $item = Brand::getBrandsLastYear($data);
                    array_push($result, ...$item);
                    $item = RelServiceProduct::getProductsLastYear($data);
                    array_push($result, ...$item);
                }
            }
            return $result;
        } else {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }
    }
}
