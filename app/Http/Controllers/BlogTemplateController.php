<?php

namespace App\Http\Controllers;

use App\BlogTemplate;
use App\Exceptions\CustomException;
use App\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogTemplateController extends Controller
{
    public function AdminListsBlogTemplate()
    {
        UserController::AuthAdmin();
        return view('admin.blog-template.lists');
    }

    /**
     * GET: http://localhost/bisto_web/admin/blog-categories
     */
    public function AdminGetBlogTemplates(): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdmin();
        $id = Session()->get('admin_id');

        $keyword = request()->keyword;
        $blogTemplates = BlogTemplate::orderBy('app_blog_templates.created_at', 'DESC')
            ->where('name', 'like', '%' . $keyword . '%')
            ->select('id', 'name', 'created_at', 'updated_at')
            ->paginate(10);
        return response()->json(['success' => 1, 'results' => $blogTemplates]);
    }

    /**
     * GET: http://localhost/bisto_web/admin/blog-categories/2
     */
    public function AdminGetBlogTemplateById(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdmin();
        $user_id = Session()->get('admin_id');

        $blogTemplate = BlogTemplate::where('id', $id)
            ->select('id', 'name', 'created_at', 'updated_at')
            ->first();
        return response()->json(['result' => $blogTemplate]);
    }

    /**
     * POST: http://localhost/bisto_web/admin/blog-categories
     * @throws CustomException
     */
    public function AdminPostBlogTemplate(Request $request)
    {
        try {
            UserController::AuthAdmin();
            $data = $request->all();
            $user_id = Session()->get('admin_id');
            $blogTemplate = new BlogTemplate();
            $blogTemplate->content = $data['content'];

            $result = $blogTemplate->save();
            return response()->json(['message' => 'Thêm blog template thành công'], 201);
        } catch (\Exception $e) {
            throw CustomException::makeServerError($e->getMessage());
        }
    }

    /**
     * PUT: http://localhost/bisto_web/admin/blog-categories/2
     */
    public function AdminPutBlogTemplate(Request $request, $id)
    {
        UserController::AuthAdmin();
        $user_id = Session()->get('admin_id');

        $blogTemplate = BlogTemplate::where('id', $id)->first();
        if (is_null($blogTemplate)) {
            return response()->json([], 204);
        }
        $blogTemplate->content = $request['content'];
        $result = $blogTemplate->save();
        if ($result) {
            return response()->json(['message' => 'Cập nhật blog template thành công']);
        }
        return response()->json(['message', 'Dữ liệu không hợp lệ'], 400);
    }

    /**
     * DELETE: http://localhost/bisto_web/admin/blog-categories/2
     */
    public function AdminDeleteBlogTemplate($id)
    {
        UserController::AuthAdmin();
        $blogTemplate = BlogTemplate::where('id', $id)->first();
        if (is_null($blogTemplate)) {
            return response()->json([], 204);
        }

        $blogTemplate->delete();
        return response()->json(['message' => 'Xóa blog template thành công']);
    }
}
