<?php

namespace App\Http\Controllers;

use App\Bcoin;
use App\City;
use App\Coupon;
use App\District;
use App\Exceptions\CustomException;
use App\Exports\AdminShopExport;
use App\Exports\ShopExport;
use App\Exports\UserExport;
use App\Services\FcmService;
use App\Services\Utils;
use App\ServiceSystem;
use App\ShippingInfo;
use App\File;
use App\Shop;
use App\User;
use App\UserDevice;
use App\Notification;
use App\Ward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Order;
use Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use PDF;

class AdminController extends Controller
{
    public function payment()
    {
        UserController::AuthAdmin();
        return view('admin.payment.payment');
    }

    public function dashboard()
    {
        UserController::AuthAdmin();
        $product = Product::all()->count();

        $order = Order::all()->count();
        $order_success = Order::where('status', Order::DON_THANH_CONG)->count();
        $order_failed = Order::where('status', Order::NGUOI_MUA_HUY_DON)->count();
        $order_refund = Order::where('status', Order::NGUOI_MUA_TRA_HANG)->count();

        $product_sold_out = Product::join('rel_product_varieties', 'app_products.id', '=', 'rel_product_varieties.product_id')
            ->groupBy('rel_product_varieties.product_id')
            ->havingRaw('SUM(rel_product_varieties.quantity) = ?', ['SUM(rel_product_varieties.sold)'])->count();

        $shops = array();
        $shop = Shop::where('parent_id', 0)->join('app_users', 'app_users.shop_id', '=', 'app_shops.id')->get();
        foreach ($shop as $item)
            array_push($shops, $item);

        $count_promotion = Coupon::where('type_creater', '3')->count();
        $count_product_block = Product::whereIn('admin_status', [-1])->count();
        $count_product_sold_out = Product::join('rel_product_varieties', 'app_products.id', '=', 'rel_product_varieties.product_id')
            ->where('rel_product_varieties.quantity', 0)->distinct()->count();
        return view('admin.dashboard')->with(compact('product', 'order', 'shops', 'order_success', 'order_failed', 'order_refund', 'product_sold_out', 'count_promotion', 'count_product_block', 'count_product_sold_out'));
    }

    /**
     * GET: http://localhost/bisto_web/admin/profile-page
     */
    public function getProfilePage()
    {
        UserController::AuthAdmin();
        return view('admin.user.profile');
    }

    /**
     * GET: http://localhost/bisto_web/admin/profiles
     */
    public function GetProfile()
    {
        UserController::AuthAdmin();
        $id = Session()->get('admin_id');
        $profile = User::where('id', $id)
            ->select('fullname', 'email', 'phone', 'role',
                'avatar_id', 'cover_id')->first();
        $totalBcoins = Bcoin::sum('amount');
        $profile->cover_image = File::where('id', $profile->cover_id)->value('url');
        $profile->avatar_image = File::where('id', $profile->avatar_id)->value('url');
        $profile->total_bcoins = $totalBcoins;
        return response()->json(['profile' => $profile]);
    }

    /**
     * POST: http://localhost/bisto_web/admin/profiles
     */
    public function UpdateProfile(Request $req)
    {
        UserController::AuthAdmin();
        $user_id = Session::get('admin_id');
        $data = $req->all();
        $admin = new User();

        $admin = $admin->find($user_id);
        if (is_null($data['full_name'])) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $admin->fullname = $data['full_name'];
        $admin->email = $data['email'];
        $admin->phone = $data['phone_number'];
        $admin->balance = 0;

        $admin->save();
        return response()->json(['message' => 'Thay đổi thông tin thành công']);
    }

    public function check_password(string $password): int
    {
        UserController::AuthAdmin();
        $admin = new User();
        $admin = $admin->find(Session()->get('admin_id'));
        if (password_verify($password, $admin->password)) {
            return 1;
        }
        return 0;
    }

    public function get_location_page()
    {
        UserController::AuthAdmin();
        return view('admin.user.location');
    }

    /**
     * GET: http://localhost/bisto_web/admin/password-page
     */
    public function getPasswordPage()
    {
        UserController::AuthAdmin();
        return view('admin.user.update_password');
    }

    /**
     * GET: http://localhost/bisto_web/admin/passwords
     */
    public function GetPassword()
    {
        UserController::AuthAdmin();
        $admin = new User();
        $admin = $admin->find(Session()->get('admin_id'));

        if ($admin) {
            return response()->json(['user' => $admin], 200);
        }
        return response()->json([], 204);
    }

    /**
     * POST: http://localhost/bisto_web/admin/passwords
     */
    public function UpdatePassword(Request $req)
    {
        UserController::AuthAdmin();
        $admin = new User();
        $admin = $admin->find(Session()->get('admin_id'));
        $data = $req->all();
        $password = $data['current_password'];
        if (password_verify($password, $admin->password)) {
            $admin->password = password_hash($data['new_password'], PASSWORD_BCRYPT, array('cost' => 12));
            $admin->save();
            return response()->json(['message' => 'Thay đổi mật khẩu thành công']);
        } else {
            return response()->json(['message' => 'Mật khẩu không đúng'], 400);
        }
    }

    /**
     * GET: http://localhost/bisto_web/admin/addresses
     */
    public function GetAddress()
    {
        UserController::AuthAdmin();
        $shipping_info = ShippingInfo::where("user_id", Session()->get('admin_id'))
            ->where('is_default', 1)->first();
        if ($shipping_info) {
            return response()->json(['address' => $shipping_info]);
        }
        return response()->json([], 204);
    }

    /**
     * GET: http://localhost/bisto_web/admin/address-page
     */
    public function getAddressPage()
    {
        UserController::AuthAdmin();
        $shipping_info = ShippingInfo::where("user_id", Session()->get('admin_id'))
            ->where("is_default", 1)->first();
        return view('admin.user.add_address')->with(compact('shipping_info'));
    }

    /**
     * POST: http://localhost/bisto_web/admin/addresses
     */
    public function UpdateAddress(Request $request)
    {
        UserController::AuthAdmin();
        $data = array();
        $user_id = Session()->get('admin_id');

        if (is_null($request->address)) {
            return response()->json(['message' => 'Địa chỉ không được để trống'], 400);
        }

        $data['address'] = $request->address;
        $data['city_id'] = $request->province_id;
        $data['district_id'] = $request->district_id;
        $data['ward_id'] = $request->ward_id;

        $user = User::where('id', $user_id)->select('fullname', 'phone')->first();
        if (is_null($user)) {
            return response()->json(['message' => 'Địa chỉ người dùng không tồn tại'], 400);
        }

        $data['name'] = $user->fullname;
        $data['phone'] = $user->phone;
        $data['user_id'] = $user_id;
        $data['is_default'] = 1;

        $shipping_info = ShippingInfo::where('user_id', $user_id)
            ->where('is_default', 1)->first();

        if (!$shipping_info) {
            $result = ShippingInfo::insert($data);
            if ($result) {
                return response()->json(['message' => 'Thêm mới địa chỉ thành công'], 201);
            }
        } else {
            $result = ShippingInfo::where('id', $shipping_info->id)->update($data);
            if ($result) {
                return response()->json(['message' => 'Thay đổi địa chỉ thành công']);
            }
        }
        return response()->json(['message' => 'Thay đổi địa chỉ không thành công'], 400);
    }

    /**
     * POST: http://localhost/bisto_web/admin/avatars
     */
    public function GetAvatar()
    {
        UserController::AuthAdmin();
        $id = Session()->get('admin_id');
        $avatar_id = User::where('id', $id)->value('avatar_id');
        if (is_null($avatar_id)) {
            $url = DEFAULT_AVATAR;
        } else {
            $url = File::where('id', $avatar_id)->value('url');
        }
        return response()->json(array('source' => $url));
    }

    /**
     * POST: http://localhost/bisto_web/admin/upload-avatar
     */
    public function UploadAvatar(Request $request)
    {
        UserController::AuthAdmin();
        $data = $request->imageUrl;
        $id = File::saveFiles($data['url'], $data['name'], $data['size'], $data['type']);

        $user = User::where('id', Session()->get('admin_id'))->first();
        if ($user->avatar_id) {
            File::find($user->avatar_id)->delete();
        }
        $user->avatar_id = $id;
        $user->save();

        return response()->json(array('url' => $data['url'], 'success' => 'true'));
    }

    /**
     * POST: http://localhost/bisto_web/admin/upload-cover
     */
    public function UploadCover(Request $request)
    {
        UserController::AuthAdmin();
        $data = $request->imageUrl;
        $id = File::saveFiles($data['url'], $data['name'], $data['size'], $data['type']);

        $user = User::where('id', Session()->get('admin_id'))->first();
        if ($user->cover_id) {
            File::find($user->cover_id)->delete();
        }
        $user->cover_id = $id;
        $user->save();

        return response()->json(array('url' => $data['url'], 'success' => 'true'));
    }

    public function BlockShop($id)
    {
        UserController::AuthAdmin();
        $user = User::find($id);
        if ($user) {
            $user->status = -1;
            $user->save();
            return response()->json(['message' => 'khóa shop thành công'], 200);
        } else {
            return response()->json(['message' => 'khóa shop thất bại'], 400);
        }
    }

    public function UnBlockShop($id)
    {
        UserController::AuthAdmin();
        $user = User::find($id);
        if ($user) {
            $user->status = 1;
            $user->save();
            return response()->json(['message' => 'mở khóa shop thành công'], 200);
        } else {
            return response()->json(['message' => 'mở khóa shop thất bại'], 400);
        }
    }

    public function chart_option_shop(Request $req)
    {
        UserController::AuthLogin();
        $data = $req->all();

        $type = $data['type_option'];
        $month = $data['month_option'];
        $year = $data['year_option'];

        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $statistic = array();
        $date1 = date('Y-m-d');
        $date2 = date('Y-m-d', strtotime("+1 days"));

        $list_shop_id = array();

        // this case is super admin query
        if ($data['shop_id'] == 0) {
            // this case super admin query all total shop
            if ($data['shop_parent_id'] == 0) {
                $shop_user = User::get();
                if ($shop_user) {
                    foreach ($shop_user as $key => $user) {
                        array_push($list_shop_id, $user->id);
                    }
                }
            } else {
                $shop_user = User::where('id', $data['shop_parent_id'])->first();
                if ($shop_user) {
                    $shop_parent = Shop::where('id', $shop_user->shop)->first();
                    if ($shop_parent) {
                        $shop_children = Shop::where('parent_id', $shop_parent->id)->get();
                        foreach ($shop_children as $key => $shop_child) {
                            $user_temp = User::where('shop', $shop_child->id)->first();
                            array_push($list_shop_id, $user_temp->id);
                        }
                    }
                }
            }
        }

        $query = Order::join('rel_order_items', 'app_orders.id', '=', 'rel_order_items.order_id')
            ->select(DB::raw('app_orders.id, app_orders.created_at, app_orders.code,
                            app_orders.status, app_orders.payment_method, app_orders.user_id,
                            app_orders.coupon_id, app_orders.seller_id, app_orders.note,
                            app_orders.payment_status, sum(rel_order_items.price) as total'))
            ->groupBy('app_orders.id', 'app_orders.created_at', 'app_orders.code',
                'app_orders.status', 'app_orders.payment_method', 'app_orders.user_id',
                'app_orders.coupon_id', 'app_orders.seller_id', 'app_orders.note',
                'app_orders.payment_status')
            ->orderBy('app_orders.created_at', 'ASC');

        //week
        if ($type == 1) {

            $date_now = date('Y-m-d h:i:s', strtotime(now()));
            $week_previous = date('Y-m-d', strtotime("-6 days"));
            $query = $query->whereBetween('app_orders.created_at', [$week_previous, $date_now]);
            $group_by_date = array();
            for ($i = 0; $i < 7; $i++) {
                $group_by_date[$i] = array();
            }
            $summary = array();
            //not super admin
            if ($data['shop_id'] != 0) {
                $order = $query->where('seller_id', $data['shop_id'])->get();
            } // super admin
            else {
                //super admin query all shop
                if ($data['shop_parent_id'] == 0) {
                    $order = $query->get();
                } else {
                    $order = $query->whereIn('seller_id', $list_shop_id)->get();
                }
            }
            foreach ($order as $item) {
                $date_difference = (int)((strtotime($item->created_at) - strtotime($week_previous)) / 86400);
                for ($i = 6; $i >= 0; $i--) {
                    if ($date_difference == $i) {
                        array_push($group_by_date[$i], $item);
                        break;
                    }
                }
            }
            $result = array();
            $summary['sales'] = 0;
            $summary['order_success'] = 0;
            $summary['order_cancle'] = 0;
            $label = array();
            foreach ($group_by_date as $index => $item) {
                $result[$index]['sales'] = 0;
                $result[$index]['order_success'] = 0;
                $result[$index]['order_cancle'] = 0;

                foreach ($item as $record) {

                    $result[$index]['sales'] += $record->total;
                    $summary['sales'] += $record->total;
                    if ($record->status == 9) {
                        $summary['order_success']++;
                        $result[$index]['order_success']++;
                    }
                    if ($record->status == 6) {
                        $summary['order_cancle']++;
                        $result[$index]['order_cancle']++;
                    }

                }
            }
            for ($i = 6; $i >= 0; $i--) {
                array_push($label, getdate(date(strtotime("-" . $i . " days")))['mday'] . "/" . getdate(date(strtotime("-" . $i . " days")))['mon']);
            }
            return response()->json(array('success' => 'true', 'type' => 1, 'label' => $label, 'result' => $result, 'summary' => $summary), 200);
        }

        //month
        if ($type == 2 && $month && $year) {
            $first_day_of_month = date('Y-m-01', strtotime($year . '-' . $month));
            $first_day_of_next_month = date('Y-m-t', strtotime($year . '-' . ($month + 1)));
            $last_day_of_month = date('Y-m-t', strtotime(date('Y-m-t', strtotime($year . '-' . $month) + 86400)));
            $total_date = getdate(strtotime($last_day_of_month))['mday'];

            $query = $query->whereBetween('app_orders.created_at', [$first_day_of_month, $first_day_of_next_month]);

            $group_by_date = array();
            for ($i = 1; $i <= $total_date; $i++) {
                $group_by_date[$i] = array();
            }
            $summary = array();

            //not super admin
            if ($data['shop_id'] != 0) {
                $order = $query->where('seller_id', $data['shop_id'])->get();
            } //super admin
            else {
                //super admin query all shop
                if ($data['shop_parent_id'] == 0) {
                    $order = $query->get();
                } else {
                    $order = $query->whereIn('seller_id', $list_shop_id)->get();
                }
            }
            foreach ($order as $item) {
                $date_difference = 1 + (int)((strtotime($item->created_at) - strtotime($first_day_of_month)) / 86400);
                for ($i = 1; $i <= $total_date; $i++) {
                    if ($date_difference == $i) {
                        array_push($group_by_date[$i], $item);
                        break;
                    }
                }
            }
            $result = array();
            $label = array();
            $summary['sales'] = 0;
            $summary['order_success'] = 0;
            $summary['order_cancle'] = 0;
            foreach ($group_by_date as $index => $item) {
                if ($index == 0) continue;
                $date_str = getdate(strtotime($first_day_of_month) + ($index - 1) * 86400)['mday'];
                array_push($label, $date_str);
                $temp = array();
                $temp['sales'] = 0;
                $temp['order_success'] = 0;
                $temp['order_cancle'] = 0;

                foreach ($item as $record) {

                    $temp['sales'] += $record->total;
                    $summary['sales'] += $record->total;
                    if ($record->status == 9) {
                        $summary['order_success']++;
                        $temp['order_success']++;
                    }
                    if ($record->status == 6) {
                        $summary['order_cancle']++;
                        $temp['order_cancle']++;
                    }

                }
                array_push($result, $temp);
            }
            return response()->json(array('success' => 'true', 'type' => 2, 'label' => $label, 'result' => $result, 'summary' => $summary));
        }

        //year
        if ($type == 3 && $year) {

            $first_month_of_year = date('Y-01-01', strtotime($year));
            $last_month_of_year = date('Y-12-31', strtotime($year));

            $query = $query->whereBetween('app_orders.created_at', [$first_month_of_year, $last_month_of_year]);

            $group_by_month = array();
            for ($i = 1; $i <= 12; $i++) {
                $group_by_month[$i] = array();
            }
            $summary = array();
            //not super admin
            if ($data['shop_id'] != 0) {
                $order = $query->where('seller_id', $data['shop_id'])->get();
            } // super admin
            else {
                //super admin query all shop
                if ($data['shop_parent_id'] == 0) {
                    $order = $query->get();
                } else {
                    $order = $query->whereIn('seller_id', $list_shop_id)->get();
                }
            }

            foreach ($order as $item) {

                $month_difference = getdate(strtotime($item->created_at))['mon'];
                for ($i = 1; $i <= 12; $i++) {
                    if ($month_difference == $i) {
                        array_push($group_by_month[$i], $item);
                        break;
                    }
                }
            }
            $result = array();
            $label = array();
            $summary['sales'] = 0;
            $summary['order_success'] = 0;
            $summary['order_cancle'] = 0;

            foreach ($group_by_month as $index => $item) {
                if ($index == 0) continue;
                array_push($label, $index);
                $date_str = $index;
                $tmp = array();
                $tmp[$date_str] = array();
                $tmp[$date_str]['sales'] = 0;
                $tmp[$date_str]['order_success'] = 0;
                $tmp[$date_str]['order_cancle'] = 0;

                foreach ($item as $record) {

                    $tmp[$date_str]['sales'] += $record->total;
                    $summary['sales'] += $record->total;
                    if ($record->status == 9) {
                        $summary['order_success']++;
                        $tmp[$date_str]['order_success']++;
                    }
                    if ($record->status == 6) {
                        $summary['order_cancle']++;
                        $tmp[$date_str]['order_cancle']++;
                    }

                }
                array_push($result, $tmp);

            }
            return response()->json(array('success' => 'true', 'type' => 3, 'label' => $label, 'result' => $result, 'summary' => $summary));

        }
        return response()->json(array('success' => 'false'), 400);
    }

    /**
     * POST: http://localhost/bisto_web/admin/save-admin
     */
    public function SaveAdmin(Request $request)
    {
        UserController::AuthAdmin();
        $data = $request->all();
        $admin = new User();
        $admin->fullname = $data['full_name'];
        $admin->phone = $data['phone_number'];
        $admin->email = $data['email'];
        $admin->password = password_hash($data['password'], PASSWORD_BCRYPT, array('cost' => 12));
        $admin->username = $data['username'];
        $admin->role = 4;

        $check_email = User::where('email', $admin->email)->first();

        if ($check_email !== null) {
            return response()->json(['message' => 'Email đã tồn tại'], 400);
        }

        $check_phone = User::where('phone', $admin->phone)->first();
        if ($check_phone !== null) {
            return response()->json(['message' => 'Số điện thoại đã tồn tại'], 400);
        }

        $check_username = User::where('username', $admin->username)->first();
        if ($check_username !== null) {
            return response()->json(['message' => 'Username đã tồn tại'], 400);
        }

        $admin->save();
        return response()->json(['message' => 'Thêm admin mới thành công'], 200);
    }

    //show all admin
    public function list_admin()
    {
        return view('admin.user.lists_admin');
    }

    /**
     * GET: http://localhost/bisto_web/admin/lists/admins
     */
    public function GetListsAdmin()
    {
        UserController::AuthAdmin();

        $keywords = request()->keyword;
        $users = User::orderBy('id', 'DESC')->where('role', 4)
            ->where('fullname', 'like', '%' . $keywords . '%')
            ->select('id as user_id', 'fullname', 'email', 'phone')
            ->paginate(10);

        if (!$users) {
            return response()->json([], 204);
        }

        foreach ($users as $item) {
            $shipping_info = ShippingInfo::where('user_id', $item->user_id)
                ->where('is_default', 1)->first();
            $item->address = '';
            if ($shipping_info) {
                $item->address = $this->getAddressShippingInfo($shipping_info);
            }
        }

        return response()->json(['users' => $users]);
    }

    //show all customers
    public function list_users()
    {
        return view('admin.user.lists_users');
    }

    /**
     * GET: http://localhost/bisto_web/admin/users
     */
    public function GetUsersAdmin()
    {
        UserController::AuthAdmin();

        $keywords = request()->keyword;
        $users = User::orderBy('app_users.id', 'DESC')->where('role', 1)
            ->where('fullname', 'like', '%' . $keywords . '%')
            ->select('id as user_id', 'fullname', 'email', 'phone')
            ->paginate(10);

        if (!$users) {
            return response()->json([], 204);
        }

        foreach ($users as $item) {
            $shipping_info = ShippingInfo::where('user_id', $item->user_id)
                ->where('is_default', 1)->first();
            $bcoins = Bcoin::where('user_id', $item->user_id)->value('amount');
            if (!is_null($bcoins)) {
                $item->amount = $bcoins;
            }
            $item->address = '';
            if ($shipping_info) {
                $item->address = $this->getAddressShippingInfo($shipping_info);
            }
        }

        return response()->json(["users" => $users]);
    }

    /**
     * GET: http://localhost/bisto_web/admin/users/export-excel
     */
    public function ExportExcelUsers()
    {
        UserController::AuthAdmin();
        return Excel::download(new UserExport(), 'users.xlsx');
    }

    //show all shop
    public function list_shop()
    {
        return view('admin.user.lists_shop');
    }

    public function list_shop_without_branch()
    {
        return view('admin.user.lists_shop_without_branch');
    }

    /**
     * GET: http://localhost/bisto_web/admin/lists/admin-shops-and-shops?page=1&limit=50&keyword=
     */
    public function GetListsAdminShopAndShopAdmin(Request $request)
    {
        UserController::AuthAdmin();
        $limit = $request->limit ?: 50;
        $keyword = $request->keyword;
        $page = $request->page ?: 1;

        $adminShops = User::orderBy('app_users.id', 'DESC')->where('role', 3)
            ->join('app_shops', 'app_shops.id', '=', 'app_users.shop_id')->select('shop_id as admin_shop_id', 'app_shops.name as admin_shop_name', 'app_users.fullname', 'app_users.id as user_id')->paginate($limit);

        foreach ($adminShops as $adminShop) {
            $adminShop->shops = Shop::where('parent_id', $adminShop->admin_shop_id)->where('name', 'like', '%' . $keyword . '%')->join('app_users', 'app_shops.id', '=', 'app_users.shop_id')
                ->select('app_users.id as user_id', 'app_shops.id as shop_id', 'fullname', 'app_shops.name as shop_name', 'app_users.fullname as fullname')->get();
        }

        $shopsWithoutBranch = User::where('role', 2)
            ->join('app_shops', 'app_shops.id', '=', 'app_users.shop_id')->whereNull('app_shops.parent_id')->orWhere('app_shops.parent_id', 0)->select('shop_id', 'app_shops.name as shop_name', 'app_users.fullname', 'app_users.id as user_id')->get();
        return response()->json(['results' => $adminShops->appends(Input::except('page')), 'shops_without_branch' => $shopsWithoutBranch], 200);
    }

    /**
     * GET: http://localhost/bisto_web/admin/lists/shops
     */
    public function GetListsShopAdmin()
    {
        UserController::AuthAdmin();
        $is_without_branch = request()->is_without_branch;
        $keywords = request()->keyword;
        $shops = User::orderBy('app_users.id', 'DESC')->where('role', 2)
            ->join('app_shops', 'app_shops.id', '=', 'app_users.shop_id')
            ->where('app_shops.name', 'like', '%' . $keywords . '%')
            ->select('app_users.id as user_id', 'name as shop_name', 'email', 'phone', 'status', 'app_users.shop_id as shop_id', 'app_shops.parent_id as parent_id', 'cmnd', 'tax_code');
        if ($is_without_branch == 1) {
            $shops = $shops->where('parent_id', null)->paginate(10);
        } else if ($is_without_branch == 0) {
            $shops = $shops->where('parent_id', '<>', null)->paginate(10);
        }
        if (!$shops) {
            return response()->json([], 204);
        }

        foreach ($shops->items() as $item) {
            $shipping_info = ShippingInfo::where('user_id', $item->user_id)
                ->where('is_default', 1)->first();
            $bcoins = Bcoin::where('user_id', $item->user_id)->value('amount');
            if (!is_null($bcoins)) {
                $item->amount = $bcoins;
            }

            $item->address = '';
            if ($shipping_info) {
                $item->address = $this->getAddressShippingInfo($shipping_info);
            }
        }

        return response()->json(["users" => $shops]);
    }

    /**
     * GET: http://localhost/bisto_web/admin/lists/shops/export-excel
     */
    public function ExportExcelListsShop()
    {
        UserController::AuthAdmin();
        $is_without_branch = request()->is_without_branch;
        return Excel::download(new ShopExport($is_without_branch), 'shops.xlsx');
    }

    // add account admin bisto
    public function add_admin()
    {
        UserController::AuthAdmin();
        return view('admin.user.add_admin');
    }

    //show all admin shop
    public function list_admin_shop()
    {
        return view('admin.user.lists_admin_shop');
    }

    /**
     * GET: http://localhost/bisto_web/admin/lists/admin-shops
     */
    public function GetOwnersAdminShop()
    {
        UserController::AuthAdmin();
        $is_paginate = request()->paginate;
        $keywords = request()->keyword;
        $admin_shops = User::orderBy('app_users.id', 'DESC')
            ->where('app_users.role', User::ROLE_ADMIN_SHOP)
            ->join('app_shops', 'app_shops.id', '=', 'app_users.shop_id')
            ->where('app_shops.name', 'like', '%' . $keywords . '%')
            ->whereNull('app_shops.parent_id')->orWhere('app_shops.parent_id', 0)
            ->select('app_users.id as user_id', 'name as shop_name', 'shop_id', 'email', 'phone');

        if ($is_paginate != 'false' || is_null($is_paginate)) {
            $admin_shops = $admin_shops->paginate(10);
        } else {
            $admin_shops = $admin_shops->get();
        }

        if (!$admin_shops) {
            return response()->json([], 204);
        }

        foreach ($admin_shops as $item) {
            $shipping_info = ShippingInfo::where('user_id', $item->user_id)
                ->where('is_default', 1)->first();
            $item->amount = $this->get_bcoins_admin_shop($item);

            $item->address = '';
            if ($shipping_info) {
                $item->address = $this->getAddressShippingInfo($shipping_info);
            }
        }

        return response()->json(["users" => $admin_shops]);
    }

    /**
     * GET: http://localhost/bisto_web/admin/lists/admin-shops/export-excel
     */
    public function ExportExcelOwnersAdminShop()
    {
        UserController::AuthAdmin();
        return Excel::download(new AdminShopExport(), 'admin-shops.xlsx');
    }

    /* get total bcoins of shop */
    private function get_bcoins_admin_shop($item)
    {
        $bcoins = 0;
        $list_shops = Shop::where('parent_id', $item->shop_id)->get();

        foreach ($list_shops as $shop) {
            $user = User::where('shop_id', $shop->id)->value('id');
            $bcoin = Bcoin::where('user_id', $user)->value('amount');
            $bcoins += $bcoin;
        }
        return $bcoins;
    }

    /* get address of user */
    private function getAddressShippingInfo($user)
    {
        $province = City::where('id', $user->city_id)->value('name');
        $district = District::where('id', $user->district_id)->value('name');
        $ward = Ward::where('id', $user->ward_id)->value('name');
        return $user->address . ", " . $ward . ", " . $district . ", " . $province;
    }

    public function getNotificationPage()
    {
        UserController::AuthAdmin();
        return view('admin.notification.list');
    }

    public function getSendingNotificationPage()
    {
        UserController::AuthAdmin();
        return view('admin.notification.sending');
    }

    public function notifyAllUser(Request $req)
    {
        UserController::AuthAdmin();
        $title = $req->title;
        $content = $req->content;
        if (!$title || !$content) {
            return response()->json(['message' => 'TIêu đề và nội dung không được bỏ trống'], 400);
        }
        //send notification for all user
        $deviceTokens = UserDevice::whereIn('message_notification_type', [2, 3])->whereNotNull('fcm_token')->pluck('fcm_token')->toArray();
        $message = [
            'topicName' => 'customNotification',
            'title' => $title,
            'body' => $content,
        ];
        $notificationService = new FcmService();
        $userNote = new Notification();
        $userNote->title = $message['title'];
        $userNote->content = $message['body'];
        $userNote->save();
        $message['id'] = $userNote->id;
        $message['created_at'] = $userNote->created_at;

        $notificationService->sendBatchNotification($deviceTokens, $message);

        $relUserNote = array();
        $userDevice = UserDevice::whereIn('fcm_token', $deviceTokens)->get();
        // all user mobile
        foreach ($userDevice as $ud) {
            $rel = array();
            $rel['is_seen'] = false;
            $rel['user_id'] = $ud->user_id;
            $rel['notification_id'] = $userNote->id;
            array_push($relUserNote, $rel);
        }

        // all shop user
        $userShop = User::whereIn('role', [2, 3])->get();
        foreach ($userShop as $user) {
            $rel = array();
            $rel['is_seen'] = false;
            $rel['user_id'] = $user->id;
            $rel['notification_id'] = $userNote->id;
            array_push($relUserNote, $rel);
        }

        $utils = new Utils();
        $relUserNote = $utils->getUniqueArrayNotification($relUserNote);
        DB::table('rel_user_notifications')->insert($relUserNote);
        //end send notification for all user

        return response()->json(['message' => 'Thành công thông báo cho toàn bộ hệ thống']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateServiceSystem(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            UserController::AuthAdmin();
            $amount = $request->amount;
            if ($amount < 0) throw CustomException::makeBadRequest('Dữ liệu không hợp lệ');
            $service_system = ServiceSystem::first();
            $service_system->amount = $amount;
            $service_system->save();
            return response()->json(['message' => 'Cập nhật phí sử dụng hệ thống thành công']);
        } catch (\Exception $exception) {
            return response()->json(['message' => 'Không thể cập nhật phí sử dụng hệ thống']);
        }
    }

    public function getServiceSystem(): \Illuminate\Http\JsonResponse
    {
        try {
            UserController::AuthAdmin();
            $service_system = ServiceSystem::first();
            return response()->json(['result' => $service_system]);
        } catch (\Exception $exception) {
            return response()->json(['message' => 'Không thể lấy phí sử dụng hệ thống']);
        }
    }

    public function createServiceSystem(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            UserController::AuthAdmin();
            $amount = $request->amount;
            if ($amount < 0) throw CustomException::makeBadRequest('Dữ liệu không hợp lệ');
            $service_system['amount'] = $amount;
            $service_system = ServiceSystem::insert($service_system);
            if ($service_system) return response()->json(['message' => 'Thêm phí sử dụng hệ thống thành công']);
            else throw CustomException::makeBadRequest('Không thể thêm sử dụng hệ thống');
        } catch (\Exception $exception) {
            return response()->json(['message' => 'Không thể thêm sử dụng hệ thống']);
        }
    }

    public function service_system_page()
    {
        UserController::AuthAdmin();
        return view('admin.service.service_system');
    }
}
