<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Exceptions\CustomException;
use App\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TagController extends Controller
{
    public function AdminListsTag()
    {
        UserController::AuthAdmin();
        return view('admin.tag.lists');
    }

    /**
     * GET: http://localhost/bisto_web/admin/tags
     */
    public function AdminGetTags(): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdmin();
        $id = Session()->get('admin_id');

        $keyword = request()->keyword;
        $tags = Tag::orderBy('app_tags.created_at', 'DESC')
            ->where('name', 'like', '%' . $keyword . '%')
            ->select('id', 'name', 'created_at', 'updated_at')
            ->paginate(10);
        return response()->json(['success' => 1, 'results' => $tags]);
    }

    /**
     * GET: http://localhost/bisto_web/admin/tags/2
     */
    public function AdminGetTagById(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdmin();
        $user_id = Session()->get('admin_id');

        $tag = Tag::where('id', $id)
            ->select('id', 'name', 'created_at', 'updated_at')
            ->first();
        return response()->json(['result' => $tag]);
    }

    /**
     * POST: http://localhost/bisto_web/admin/tags
     * @throws CustomException
     */
    public function AdminPostTag(Request $request)
    {
        try {
            UserController::AuthAdmin();
            $data = $request->all();

            if ($this->checkDuplicate($data['name']))
                return response()->json(['message' => 'Tên bị trùng lặp'], 400);

            $tag = new Tag();
            $tag->name = $data['name'];

            $result = $tag->save();
            return response()->json(['message' => 'Thêm tag thành công'], 201);
        } catch (\Exception $e) {
            throw CustomException::makeServerError($e->getMessage());
        }
    }

    /**
     * PUT: http://localhost/bisto_web/admin/tags/2
     */
    public function AdminPutTag(Request $request, $id)
    {
        UserController::AuthAdmin();
        $user_id = Session()->get('admin_id');

        $tag = Tag::where('id', $id)->first();
        if (is_null($tag)) {
            return response()->json([], 204);
        }
        if ($this->checkDuplicate($request['name']))
            return response()->json(['message' => 'Tên bị trùng lặp'], 400);

        $tag->name = $request['name'];
        $result = $tag->save();
        if ($result) {
            return response()->json(['message' => 'Cập nhật tag thành công']);
        }
        return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
    }

    /**
     * DELETE: http://localhost/bisto_web/admin/tags/2
     */
    public function AdminDeleteTag($id)
    {
        UserController::AuthAdmin();
        $tag = Tag::where('id', $id)->first();
        if (is_null($tag)) {
            return response()->json([], 204);
        }

        $tag->delete();
        return response()->json(['message' => 'Xóa tag thành công']);
    }

    private function checkDuplicate($name)
    {
        $tag = Tag::where('name', $name)->first();
        return !is_null($tag);
    }
}
