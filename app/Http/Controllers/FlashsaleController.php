<?php

namespace App\Http\Controllers;

use App\Services\FcmService;
use App\Services\Utils;
use App\Shop;
use App\User;
use App\UserDevice;
use App\Notification;
use Illuminate\Http\Request;
use App\FlashSale;
use App\File;
use App\ProductFlashSale;
use Illuminate\Support\Facades\DB;

class FlashsaleController extends Controller
{
    /** Admin */
    public function add_flashsale()
    {
        UserController::AuthAdmin();
        return view('admin.flashsale.add');
    }

    public function admin_list_flashsale()
    {
        UserController::AuthAdmin();
        return view('admin.flashsale.lists');
    }

    /**
     * GET: http://localhost/bisto_web/admin/flashsales
     */
    public function GetFlashSales()
    {
        UserController::AuthAdmin();
        $owner = Session()->get('admin_id');
        $keywords = request()->keyword;

        $list_flash_sales = FlashSale::where('user_id', $owner)
            ->orderBy('created_at', 'DESC')
            ->where('name', 'like', '%' . $keywords . '%')
            ->paginate(10);

        if (!count($list_flash_sales)) {
            return response()->json([], 204);
        }

        foreach ($list_flash_sales as $index => $item) {
            $sourceUrl = File::where('id', $item->image_id)->value('url');
            $list_flash_sales[$index]->image_url = $sourceUrl;
        }

        return response()->json(['FlashSales' => $list_flash_sales]);
    }

    /**
     * GET: http://localhost/bisto_web/admin/flashsales/2
     */
    public function GetFlashSaleById($id)
    {
        UserController::AuthAdmin();
        $owner = Session()->get('admin_id');
        $flash_sale = FlashSale::where('id', $id)->where('user_id', $owner)->get();

        if (!count($flash_sale)) {
            return response()->json([], 204);
        }

        $sourceUrl = File::where('id', $flash_sale[0]->image_id)->value('url');
        $flash_sale[0]->image = $sourceUrl;
        return response()->json(['FlashSale' => $flash_sale]);
    }

    /**
     * POST: http://localhost/bisto_web/admin/flashsales
     */
    public function PostFlashSale(Request $request)
    {
        UserController::AuthAdmin();
        $owner = Session()->get('admin_id');
        $data = array();

        if (!$request->source_urls) {
            return response()->json(['message' => 'Hãy chọn ảnh cho flash sale'], 400);
        }

        //send notification for all user
        $deviceTokens = UserDevice::whereIn('message_notification_type', [2, 3])->whereNotNull('fcm_token')->pluck('fcm_token')->toArray();
        $message = [
            'topicName' => 'flashsales',
            'title' => 'Siêu khuyến mãi',
            'body' => 'Chương trình siêu khuyến mãi vừa được mở ra, nhanh tay đừng bỏ lỡ bạn nhé',
        ];
        $notificationService = new FcmService();
        $userNote = new Notification();
        $userNote->title = $message['title'];
        $userNote->content = $message['body'];
        $userNote->save();
        $message['id'] = $userNote->id;
        $message['created_at'] = $userNote->created_at;

        $notificationService->sendBatchNotification($deviceTokens, $message);

        $relUserNote = array();
        $userDevice = UserDevice::whereIn('fcm_token', $deviceTokens)->get();
        foreach ($userDevice as $ud) {
            $rel = array();
            $rel['is_seen'] = false;
            $rel['user_id'] = $ud->user_id;
            $rel['notification_id'] = $userNote->id;
            array_push($relUserNote, $rel);
        }

        // all shop user
        $userShop = User::whereIn('role', [2, 3])->get();
        foreach ($userShop as $usr) {
            $rel = array();
            $rel['is_seen'] = false;
            $rel['user_id'] = $usr->id;
            $rel['notification_id'] = $userNote->id;
            array_push($relUserNote, $rel);
        }

        $utils = new Utils();
        $relUserNote = $utils->getUniqueArrayNotification($relUserNote);

        DB::table('rel_user_notifications')->insert($relUserNote);
        //end send notification for all user


        $data['name'] = $request->name;
        $data['amount'] = $request->amount;
        $data['type'] = 1;
        $data['description'] = $request->description;
        $data['start_time'] = $request->start_time;
        $data['end_time'] = $request->end_time;
        $data['start_date'] = explode('+', $request->start_date)[0];
        $data['end_date'] = explode('+', $request->end_date)[0];
        $data['status'] = $request->status;
        $data['user_id'] = $owner;

        $source = $request->source_urls[0];
        $imageId = File::saveFiles($source['url'], $source['filename'], $source['size'], $source['type']);
        $data['image_id'] = $imageId;

        /*$source_urls = $request->source_urls;
        if ($source_urls != null) {
            $imageIds = '';
            foreach ($source_urls as $source) {
                $imageId = File::saveFiles($source['url'], $source['filename'], $source['size'], $source['type']);
                $imageIds .= $imageId . ',';
            }
            $data['image_id'] = substr($imageIds, 0, -1);
        }*/

        $result = FlashSale::insert($data);
        if ($result) {
            return response()->json(['message' => 'Thêm flash sale thành công'], 201);
        }
        return response()->json(['message' => 'Dữ liệu không hợp lệ, vui lòng nhập lại'], 400);
    }

    /**
     * PUT: http://localhost/bisto_web/admin/flashsales/2
     */
    public function PutFlashSale(Request $request, $id)
    {
        UserController::AuthAdmin();
        $owner = Session()->get('admin_id');

        $flashSale = FlashSale::where('id', $id)->where('user_id', $owner)->first();

        /*$imageIds = $flashSale->image_id;
        $deleted_imageIds = $request->deleted_image_ids;
        if ($deleted_imageIds != null) {
            $imageIds = explode(',', $imageIds);
            foreach ($deleted_imageIds as $id) {
                if (($key = array_search(strval($id), $imageIds)) !== false) {
                    unset($imageIds[$key]);
                }
                File::where('id', $id)->delete();
            }
            $imageIds = implode(",", $imageIds);
        }

        $source_urls = $request->source_urls;
        if ($source_urls != null) {
            if (!empty($imageIds)) {
                $imageIds .= ',';
            }

            foreach ($source_urls as $source) {
                $imageId = File::saveFiles($source['url'], $source['filename'], $source['size'], $source['type']);
                $imageIds .= $imageId . ',';
            }
            $imageIds = substr($imageIds, 0, -1);
        }
        $flashSale->image_id = $imageIds;*/

        if (is_null($flashSale)) {
            return response()->json([], 204);
        }

        if ($request->deleted_image_ids && !$request->source_urls) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $deleted_imageId = $request->deleted_image_ids;
        if ($deleted_imageId != null) {
            File::where('id', $deleted_imageId)->delete();
        }

        $imageId = $flashSale->image_id;
        /*if ($request->source_urls[0]) {
            $source = $request->source_urls[0];
            $imageId = File::saveFiles($source['url'], $source['filename'], $source['size'], $source['type']);
        }*/

        $flashSale->image_id = $imageId;
        $flashSale->name = $request->name;
        $flashSale->amount = $request->amount;
        $flashSale->description = $request->description;
        $flashSale->start_time = $request->start_time;
        $flashSale->end_time = $request->end_time;
        $flashSale->start_date = $request->start_date;
        $flashSale->end_date = $request->end_date;
        $flashSale->status = $request->status;

        $result = $flashSale->save();
        if ($result) {
            return response()->json(['message' => 'Cập nhật flash sale thành công']);
        }
        return response()->json(['message' => 'Dữ liệu không hợp lệ, vui lòng nhập lại'], 400);
    }

    /**
     * DELETE: http://localhost/bisto_web/admin/flashsales/2
     */
    public function DeleteFlashSale($id)
    {
        UserController::AuthAdmin();
        $owner = Session()->get('admin_id');
        $flash = FlashSale::where('id', $id)->where('user_id', $owner)->first();

        if (is_null($flash)) {
            return response()->json([], 204);
        }

        ProductFlashSale::where('flash_sale_id', $id)->delete();
        File::where('id', $flash->image_id)->delete();
        $result = $flash->delete();

        if ($result) {
            return response()->json(['message' => 'Xoá flash sale thành công']);
        }
        return response()->json([], 204);
    }

    /** Admin-shop */
    public function AdminShopRegisterFlashSale()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.flashsale.register');
    }

    public function AdminShopSelectProduct()
    {
        UserController::AuthAdminShop();
        $id = Session()->get('id');
        $ids = Session()->get('data');

        $productIds = array();
        if ($ids) {
            foreach ($ids as $value) {
                array_push($productIds, (int)$value);
            }
        }
        return view('admin-shop.flashsale.select')
            ->with('id', $id)->with('ids', $productIds);
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/flashsales
     */
    public function GetAdminShopFlashSales()
    {
        UserController::AuthAdminShop();
        return $this->getFlashSalesList();
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/flashsales/2
     */
    public function AdminShopGetOneFlashSales($id)
    {
        UserController::AuthAdminShop();
        return view('admin-shop.flashsale.select')->with(['id' => $id]);
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/flashsales/products/2
     */
    public function AdminShopFlashSalesProducts(Request $request, $id)
    {
        UserController::AuthAdminShop();
        $admin_shop_id = Session()->get('admin_shop_id');

        if ($request->has('shopId')) {
            $shopId = $request->shopId;
        } else {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $listShopId = Shop::getListsChildShop($admin_shop_id);
        if ($shopId == 0) {
            $list_ids = $listShopId;
        } else {
            if (in_array($shopId, $listShopId)) {
                $list_ids = [$shopId];
            } else {
                return response()->json(['message' => 'Cửa hàng không tồn tại'], 400);
            }
        }
        return $this->getProductsFlashSale($id, $list_ids);
    }

    /**
     * POST: http://localhost/bisto_web/admin-shop/flashsales
     */
    public function AdminShopApplyFlashSale(Request $request)
    {
        UserController::AuthAdminShop();
        if (is_null($request->user_ids)) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }
        $user_ids = $request->user_ids;
        return $this->applyFlashSale($request, $user_ids);
    }

    /** Shop */
    public function ShopRegisterFlashSale()
    {
        UserController::AuthShop();
        return view('shop.flashsale.register');
    }

    public function ShopSelectProduct()
    {
        UserController::AuthShop();
        $id = Session()->get('id');
        $ids = Session()->get('data');

        $productIds = array();
        if ($ids) {
            foreach ($ids as $value) {
                array_push($productIds, (int)$value);
            }
        }
        return view('shop.flashsale.select')
            ->with('id', $id)->with('ids', $productIds);
    }

    /**
     * GET: http://localhost/bisto_web/shop/flashsales
     */
    public function GetShopFlashSales()
    {
        UserController::AuthShop();
        return $this->getFlashSalesList();
    }

    /**
     * GET: http://localhost/bisto_web/shop/flashsales/2
     */
    public function ShopGetOneFlashSales($id)
    {
        UserController::AuthShop();
        return view('shop.flashsale.select')->with(['id' => $id]);
    }

    /**
     * GET: http://localhost/bisto_web/shop/flashsales/products/2
     */
    public function ShopFlashSalesProducts($id)
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');
        return $this->getProductsFlashSale($id, $shop_id);
    }

    /**
     * POST: http://localhost/bisto_web/shop/flashsales
     */
    public function ShopApplyFlashSale(Request $request)
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');
        return $this->applyFlashSale($request, $shop_id);
    }

    /* Hàm dùng chung */
    private function getFlashSalesList()
    {
        $keywords = request()->keyword;
        $list_flash_sales = FlashSale::where('status', 1)
            ->orderBy('created_at', 'DESC')
            ->where('name', 'like', '%' . $keywords . '%')
            ->paginate(10);

        if (!count($list_flash_sales)) {
            return response()->json([], 204);
        }

        foreach ($list_flash_sales as $index => $item) {
            $sourceUrl = File::where('id', $item->image_id)->value('url');
            $list_flash_sales[$index]->image = $sourceUrl;
        }

        return response()->json(['FlashSales' => $list_flash_sales]);
    }

    private function getProductsFlashSale($id, $user_id)
    {
        $flash_sale = FlashSale::where('id', $id)->where('status', 1)
            ->select('name', 'type', 'amount', 'description',
                'start_time', 'end_time', 'start_date', 'end_date', 'image_id')->first();
        if (is_null($flash_sale)) {
            return response()->json([], 204);
        }

        $image = File::where('id', $flash_sale->image_id)->first();
        $flash_sale->image = $image;
        if (is_array($user_id)) {
            $list_flash_sales = ProductFlashSale::whereIn('user_id', $user_id)
                ->where('flash_sale_id', $id)
                ->select('quantity', 'product_id')
                ->get();
        } else {
            $list_flash_sales = ProductFlashSale::where('user_id', $user_id)
                ->where('flash_sale_id', $id)
                ->select('quantity', 'product_id')
                ->get();
        }

        if (count($list_flash_sales)) {
            $product_ids = array();
            $quantities = array();

            foreach ($list_flash_sales as $item) {
                array_push($product_ids, $item->product_id);
                array_push($quantities, $item->quantity);
            }
            return response()->json(['product_ids' => $product_ids, 'quantities' => $quantities,
                'flash_sale' => $flash_sale]);
        }
        return response()->json(['product_ids' => [], 'quantities' => [], 'flash_sale' => $flash_sale]);
    }

    private function applyFlashSale(Request $request, $user_ids)
    {
        $product_ids = $request->product_ids;
        $quantities = $request->quantities;
        $user_id = $user_ids;

        if (is_null($product_ids) || is_null($quantities)) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        for ($i = 0; $i < count($product_ids); $i++) {
            $product_id = $product_ids[$i];
            $quantity = $quantities[$i];
            if (is_array($user_ids)) {
                $user_id = $user_ids[$i];
            }

            $product_flash_sale = ProductFlashSale::where('user_id', $user_id)
                ->where('flash_sale_id', $request->flash_sale_id)
                ->where('product_id', $product_id)->first();

            if ($product_flash_sale) {
                $product_flash_sale->quantity = $quantity;
                $product_flash_sale->save();
            } else {
                $data = array();
                $data['flash_sale_id'] = $request->flash_sale_id;
                $data['user_id'] = $user_id;
                $data['product_id'] = $product_id;
                $data['quantity'] = $quantity;
                $data['sold'] = 0;
                ProductFlashSale::insert($data);
            }
        }
        return response()->json(['message' => 'Đăng kí flash sale thành công'], 201);
    }
}
