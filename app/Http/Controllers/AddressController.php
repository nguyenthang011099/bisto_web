<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AddressController extends Controller
{
    /**
     * GET: http://localhost/bisto_web/provinces
     */
    public function GetProvinces()
    {
        UserController::AuthLogin();
        $provinces = DB::table('app_cities')->get();

        if (!count($provinces)) {
            return response()->json([], 204);
        }

        foreach ($provinces as $item) {
            $item->id = strval($item->id);
        }

        return response()->json(['results' => $provinces]);
    }

    /**
     * GET: http://localhost/bisto_web/provinces/03
     */
    public function GetProvinceById($id)
    {
        UserController::AuthLogin();
        $province = DB::table('app_cities')->where('id', $id)->get();
        if (!count($province)) {
            return response()->json([], 204);
        }
        return response()->json(['results' => $province]);
    }

    /**
     * GET: http://localhost/bisto_web/districts?provinceId=
     */
    public function GetDistrictsByProvince(Request $request)
    {
        UserController::AuthLogin();
        $matp = request()->provinceId;

        $districts = DB::table('app_districts')->where('city_id', $matp)->get();
        if (!count($districts)) {
            return response()->json([], 204);
        }
        return response()->json(['results' => $districts]);
    }

    /**
     * GET: http://localhost/bisto_web/districts/005
     */
    public function GetDistrictById($id)
    {
        UserController::AuthLogin();
        $district = DB::table('app_districts')->where('id', $id)->get();
        if (!count($district)) {
            return response()->json([], 204);
        }
        return response()->json(['results' => $district]);
    }

    /**
     * GET: http://localhost/bisto_web/wards?districtId=
     */
    public function GetWardsByDistrict(Request $request)
    {
        UserController::AuthLogin();
        $maqh = request()->districtId;

        $wards = DB::table('app_wards')->where('district_id', $maqh)->get();
        if (!count($wards)) {
            return response()->json([], 204);
        }
        return response()->json(['results' => $wards]);
    }

    /**
     * GET: http://localhost/bisto_web/wards/00005
     */
    public function GetWardById($id)
    {
        UserController::AuthLogin();
        $ward = DB::table('app_wards')->where('id', $id)->get();
        if (!count($ward)) {
            return response()->json([], 204);
        }
        return response()->json(['results' => $ward]);
    }
}
