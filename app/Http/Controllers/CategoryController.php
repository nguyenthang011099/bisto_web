<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    /**
     * GET: http://localhost/bisto_web/admin/categories
     */
    public function GetCategories()
    {
        UserController::AuthAdmin();
        $keywords = request()->keyword;

        $all_categories = Category::where('parent_id', '<>', null)
            ->where('name', 'like', '%' . $keywords . '%')->get();
        $categories = $this->show_query_category($all_categories);

        if ($categories) {
            return response()->json(['categories' => $categories]);
        }
        return response()->json([], 204);
    }

    //get details categories
    private function show_query_category($all_categories)
    {
        //find parent category\
        foreach ($all_categories as $category) {
            if ($category->parent_id != null) {
                //get parent category name
                $parent_name = Category::where('id', $category->parent_id)->value('name');
                $category->parent_name = $parent_name;
            } else {
                $category->parent_name = '';
            }
        }
        return $all_categories;
    }

    /**
     * GET: http://localhost/bisto_web/admin/parent-category
     */
    public function GetParentCategories()
    {
        UserController::AuthAdmin();
        $parent_categories = Category::where('parent_id', null)->get();

        if ($parent_categories) {
            return response()->json(['parent_categories' => $parent_categories]);
        }
        return response()->json([], 204);
    }

    /**
     * PUT: http://localhost/bisto_web/admin/categories/3
     */
    public function UpdateCategories(Request $request, $id)
    {
        UserController::AuthAdmin();
        $data = $request->all();
        $category = Category::find($id);
        if (!$category) {
            return response()->json(['message' => 'Không tồn tại id danh mục này'], 400);
        }
        $icon_id = $category->icon_id;
        $icon = File::find($icon_id);
        if ($data['image_url']) {
            if (!is_null($data['image_url']['url'])) {
                if ($icon) {
                    $icon->update(['url' => $data['image_url']['url'], 'filename' => $data['image_url']['filename'], 'size' => $data['image_url']['size'], 'type' => $data['image_url']['url'], 'hash' => '']);
                    $iconId = $icon->id;
                } else {
                    $icon = new File();
                    $iconId = $icon->insertGetId(['url' => $data['image_url']['url'], 'filename' => $data['image_url']['filename'], 'size' => $data['image_url']['size'], 'type' => $data['image_url']['url'], 'hash' => '']);
                }
                $cate['icon_id'] = $iconId;
            }
        }
        $cate = array();
        $cate['id'] = $id;
        $cate['name'] = $request->nameCateEdit;

        Category::where('id', $id)->update($cate);
        return response()->json(['message' => 'Cập nhật danh mục thành công'], 200);
    }

    /**
     * POST: http://localhost/bisto_web/admin/categories
     */
    public
    function AddCategories(Request $request)
    {
        UserController::AuthAdmin();
        $name = $request->name;
        $isExist = Category::where('name', '=', $name)->first();
        if ($isExist) {
            return response()->json(['message' => 'Tên danh mục sản phẩm bị trùng'], 400);
        }

        $parentId = $request->parent_id;
        $level = $request->level;

        $category = new Category();
        $category->name = $name;
        $temp = Category::where('parent_id', $parentId)->count();

        if (!$temp && $parentId != 1) {
            return response()->json(['message' => 'Không tồn tại danh mục cấp cha'], 400);
        }

        if ($level != 1) {
            $category->parent_id = $parentId;
        }

        $category->save();
        return response()->json(['message' => 'Thêm danh mục sản phẩm thành công'], 201);
    }

    /**
     * GET: http://localhost/bisto_web/admin/categories/2
     */
    public function GetCategoryById($id)
    {
        UserController::AuthAdmin();
        $category = Category::where('id', $id)->first();
        if ($category) {
            if ($category->icon_id) {
                $category->image_url = File::find($category->icon_id)->url;
            }
            return response()->json(['category' => $category]);
        }
        return response()->json([], 204);
    }


    /**
     * GET :: admin/category-level-two/1
     */
    public function getCategoryLevelTwo($id)
    {
        $categories = Category::where('parent_id', $id)->get();
        return response()->json(['results' => $categories]);
    }

    /**
     * GET :: shop/category-level-two/1
     */
    public function getCategoryLevelTwoForShop($id)
    {
        UserController::AuthShop();
        $categories = Category::where('parent_id', $id)->get();
        return response()->json(['results' => $categories]);
    }

    /**
     * GET :: shop/category-level-three/1
     */
    public function getCategoryLevelThree($id)
    {
        $categories = Category::where('parent_id', $id)->get();
        return response()->json(['results' => $categories]);
    }

    /**
     * GET :: admin/root-category
     */
    public function getCategoryLevelOne()
    {
        $categories = Category::where('parent_id', null)->get();
        return response()->json(['results' => $categories]);
    }

    /**
     * GET :: shop/root-category
     */
    public function getCategoryLevelOneForShop()
    {
        UserController::AuthShop();
        $categories = Category::where('parent_id', null)->get();
        return response()->json(['results' => $categories]);
    }

    /**
     * POST: admin/save-category
     */
    public function saveCategory(Request $req)
    {
        UserController::AuthAdmin();
        $data = $req->all();
        $image_cate = array();
        $image_cate['url'] = $data['image_url']['url'];
        $image_cate['size'] = $data['image_url']['size'];
        $image_cate['filename'] = $data['image_url']['filename'];
        $image_cate['type'] = $data['image_url']['type'];
        $icon_id = File::insertGetId($image_cate);
        $category = array();
        $category['name'] = $data['name'];
        $category['icon_id'] = $icon_id;
        if ($data['category_level'] == 1) {
            $category['parent_id'] = null;
        } else if ($data['category_level'] == 2) {
            $category['parent_id'] = $data['parent_id'];
        } else if ($data['category_level'] == 3) {
            $category['parent_id'] = $data['parent_id'];
        }
        Category::insert($category);
        return response()->json(['message' => 'thêm mới danh mục thành công'], 200);

    }


    public function allCategoryPage()
    {
        UserController::AuthAdmin();
        return view('admin.category.lists');
    }

    /**
     * GET: https://localhost/bisto_web/admin/categories/query?keyword=&level=1
     */
    public function QueryCategoryAdmin()
    {
        UserController::AuthAdmin();
        return $this->QueryCategory();
    }

    public function categoryAdminShopPage()
    {
        UserController::AuthAdminShop();
        $all_categories = Category::where('parent_id', '<>', null)->get();
        $categories = $this->show_query_category($all_categories);
        return view('admin-shop.category.lists')->with('categories', $categories);
    }

    /**
     * GET: https://localhost/bisto_web/admin-shop/categories/query?keyword=&level=1
     */
    public function QueryCategoryAdminShop()
    {
        UserController::AuthAdminShop();
        return $this->QueryCategory();
    }

    private function QueryCategory()
    {
        $keyword = request()->keyword;
        $level = request()->level;

        switch ($level) {
            case 1:
                $categories = Category::where('name', 'like', '%' . $keyword . '%')
                    ->whereNull('parent_id')->orderBy('created_at', 'desc')->paginate(10);

                if ($categories) {
                    foreach ($categories as $item) {
                        $icon_id = $item->icon_id;
                        $icon = File::find($icon_id);
                        if ($icon) {
                            $item->image_url = $icon->url;
                        } else {
                            $item->image_url = null;
                        }
                        $item->parent_name = '';
                    }
                    return response()->json(['categories' => $categories]);
                }
                return response()->json([], 204);
            case 2:
                $parents = Category::whereNull('parent_id')->get();
                $ids = array();
                foreach ($parents as $item) {
                    $categories = Category::where('parent_id', $item->id)
                        ->where('name', 'like', '%' . $keyword . '%')->get();
                    foreach ($categories as $category) {
                        array_push($ids, $category->id);
                    }
                }

                $categories = Category::whereIn('id', $ids)->orderBy('created_at', 'desc')->paginate(10);
                if ($categories) {
                    foreach ($categories as $item) {
                        $icon_id = $item->icon_id;
                        $icon = File::find($icon_id);
                        if ($icon) {
                            $item->image_url = $icon->url;
                        } else {
                            $item->image_url = null;
                        }
                        $item->parent_name = Category::where('id', $item->parent_id)->value('name');
                    }
                    return response()->json(['categories' => $categories]);
                }
                return response()->json([], 204);
            case 3:
                $parents = Category::whereNull('parent_id')->get();
                $ids = array();
                foreach ($parents as $item) {
                    $categoryLvTwo = Category::where('parent_id', $item->id)->get();
                    foreach ($categoryLvTwo as $value) {
                        $categories = Category::where('name', 'like', '%' . $keyword . '%')
                            ->where('parent_id', $value->id)->get();
                        foreach ($categories as $item) {
                            array_push($ids, $item->id);
                        }
                    }
                }
                $categories = Category::whereIn('id', $ids)->orderBy('created_at', 'desc')->paginate(10);
                if ($categories) {
                    foreach ($categories as $item) {
                        $icon_id = $item->icon_id;
                        $icon = File::find($icon_id);
                        if ($icon) {
                            $item->image_url = $icon->url;
                        } else {
                            $item->image_url = null;
                        }
                        $item->parent_name = Category::where('id', $item->parent_id)->value('name');
                    }
                    return response()->json(['categories' => $categories]);
                }
                return response()->json([], 204);
            default:
                return response()->json([], 204);
        }
    }

    /**
     * DELETE: http://localhost/bisto_web/admin/categories/2
     */
    public function DeleteCategory($id)
    {
        UserController::AuthAdmin();
        //delete category in table category
        $category = Category::find($id);
        if ($category) {
            $image = File::find($category->icon_id);
            if ($image) {
                $image->delete();
            }
            $category->delete();
        }

        /*foreach ($product_ids as $id) {
            DB::table('product')->where('id', $id)->delete();
            DB::table('product_category')->where('productId', $id)->delete();
        }*/

        return response()->json(['message' => 'Xóa danh mục sản phẩm thành công']);
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/categories
     */
    public function GetCategoriesAdminShop(Request $request)
    {
        UserController::AuthAdminShop();
        $keywords = request()->keyword;
        $all_categories = Category::where('parent_id', '<>', null)->get();

        //find parent category
        $categories = array();
        foreach ($all_categories as $category) {
            //get parent category name
            $parent_name = Category::where('id', $category->parent_tag)
                ->where('name', 'like', '%' . $keywords . '%')->value('name');
            $category->parent_name = $parent_name;
            array_push($categories, $category);
        }
        if ($categories) {
            return response()->json(['categories' => $categories]);
        }
        return response()->json([], 204);
    }
}
