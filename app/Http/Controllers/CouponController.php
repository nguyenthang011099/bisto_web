<?php

namespace App\Http\Controllers;

use App\Services\FcmService;
use App\Services\PushNotificationJob;
use App\Services\Utils;
use App\User;
use App\UserDevice;
use App\Notification;
use Illuminate\Http\Request;
use App\Coupon;
use Illuminate\Support\Facades\DB;

class CouponController extends Controller
{
    /** admin */
    public function admin_list_coupon()
    {
        UserController::AuthAdmin();
        return view('admin.coupon.lists');
    }

    /** admin */
    public function admin_list_promotion()
    {
        UserController::AuthAdmin();
        return view('admin.coupon.lists_promotion');
    }

    /**
     * GET: http://localhost/bisto_web/admin/coupons
     */
    public function GetCouponsAdmin()
    {
        UserController::AuthAdmin();
        $owner = Session()->get('admin_id');

        $keywords = request()->keyword;
        $coupons = Coupon::where('user_id', $owner)
            ->orderBy('created_at', 'DESC')
            ->where('name', 'like', '%' . $keywords . '%')
            ->paginate(10);

        if ($coupons) {
            return response()->json(['coupons' => $coupons]);
        }
        return response()->json([], 204);
    }

    /**
     * GET: http://localhost/bisto_web/admin/coupon/2
     */
    public function GetCouponAdminById($id)
    {
        UserController::AuthAdmin();
        return $this->get_coupon($id);
    }

    /**
     * POST: http://localhost/bisto_web/admin/coupon
     */
    public function AdminInsertCoupon(Request $request)
    {
        UserController::AuthAdmin();
        $user = Session()->get('admin_id');
        $deviceTokens = UserDevice::whereIn('message_notification_type', [2, 3])->whereNotNull('fcm_token')->pluck('fcm_token')->toArray();
        $data = [
            'topicName' => 'coupons',
            'title' => 'Khuyến mãi mới',
            'body' => 'Xin chúc mừng bạn nhận được mã khuyến mãi từ team admin',
        ];
        $notificationService = new FcmService();
        $userNote = new Notification();
        $userNote->title = $data['title'];
        $userNote->content = $data['body'];
        $userNote->save();
        $data['id'] = $userNote->id;
        $data['created_at'] = $userNote->created_at;
        $notificationService->sendBatchNotification($deviceTokens, $data);

        $relUserNote = array();
        $userDevice = UserDevice::whereIn('fcm_token', $deviceTokens)->get();
        foreach ($userDevice as $ud) {
            $rel = array();
            $rel['is_seen'] = false;
            $rel['user_id'] = $ud->user_id;
            $rel['notification_id'] = $userNote->id;
            array_push($relUserNote, $rel);
        }

        // all shop user
        $userShop = User::whereIn('role', [2, 3])->get();
        foreach ($userShop as $usr) {
            $rel = array();
            $rel['is_seen'] = false;
            $rel['user_id'] = $usr->id;
            $rel['notification_id'] = $userNote->id;
            array_push($relUserNote, $rel);
        }

        $utils = new Utils();
        $relUserNote = $utils->getUniqueArrayNotification($relUserNote);

        DB::table('rel_user_notifications')->insert($relUserNote);
//        PushNotificationJob::dispatch('sendBatchNotification', [
//            $deviceTokens,
//            $data,
//        ]);
        return $this->insert_coupon($request, $user);
    }

    /**
     * PUT: http://localhost/bisto_web/admin/coupon/2
     */
    public function AdminUpdateCoupon(Request $request, $id)
    {
        UserController::AuthAdmin();
        $user = Session()->get('admin_id');
        return $this->update_coupon($request, $id, $user);
    }

    /**
     * DELETE: http://localhost/bisto_web/admin/coupon/2
     */
    public function AdminDeleteCoupon($id)
    {
        UserController::AuthAdmin();
        return $this->delete_coupon($id);
    }

    /* Admin-Shop */
    public function admin_shop_list_coupon()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.coupon.lists');
    }

    /* Admin-Shop */
    public function admin_shop_list_promotion()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.coupon.lists_promotion');
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/coupons
     */
    public function GetCouponsAdminShop()
    {
        UserController::AuthAdminShop();
        $owner = Session()->get('admin_shop_id');

        $keywords = request()->keyword;
        $coupons = Coupon::where('user_id', $owner)
            ->orderBy('created_at', 'DESC')
            ->where('name', 'like', '%' . $keywords . '%')
            ->paginate(10);

        if ($coupons) {
            return response()->json(['coupons' => $coupons]);
        }
        return response()->json([], 204);
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/coupon/2
     */
    public function GetCouponAdminShopById($id)
    {
        UserController::AuthAdminShop();
        return $this->get_coupon($id);
    }

    /**
     * POST: http://localhost/bisto_web/admin-shop/coupon
     */
    public function AdminShopInsertCoupon(Request $request)
    {
        UserController::AuthAdminShop();
        $user = Session()->get('admin_shop_id');
        return $this->insert_coupon($request, $user);
    }

    /**
     * PUT: http://localhost/bisto_web/admin-shop/coupon/2
     */
    public function AdminShopUpdateCoupon(Request $request, $id)
    {
        UserController::AuthAdminShop();
        $user = Session()->get('admin_shop_id');
        return $this->update_coupon($request, $id, $user);
    }

    /**
     * DELETE: http://localhost/bisto_web/admin-shop/coupon/2
     */
    public function AdminShopDeleteCoupon($id)
    {
        UserController::AuthAdminShop();
        return $this->delete_coupon($id);
    }

    /* Shop */
    public function shop_list_coupon()
    {
        UserController::AuthShop();
        return view('shop.coupon.lists');
    }

    /* Shop */
    public function shop_list_promotion()
    {
        UserController::AuthShop();
        return view('shop.coupon.lists_promotion');
    }

    /**
     * GET: http://localhost/bisto_web/shop/coupon/2
     */
    public function GetCouponsShop()
    {
        UserController::AuthShop();
        $owner = Session()->get('shop_id');

        $keywords = request()->keyword;
        $coupons = Coupon::where('user_id', $owner)
            ->orderBy('created_at', 'DESC')
            ->where('name', 'like', '%' . $keywords . '%')
            ->paginate(10);

        if ($coupons) {
            return response()->json(['coupons' => $coupons]);
        }
        return response()->json([], 204);
    }

    /**
     * GET: http://localhost/bisto_web/shop/promotions?page
     */
    public function GetPromotionsShop()
    {
        UserController::AuthShop();
        return $this->get_promotions();
    }

    public function GetPromotionsAdmin()
    {
        UserController::AuthAdmin();
        return $this->get_promotions();
    }

    public function GetPromotionsAdminShop()
    {
        UserController::AuthAdminShop();
        return $this->get_promotions();
    }

    public function get_promotions()
    {
        $keywords = request()->keyword;
        $type_creater = request()->type_creater ?? 1;
        $coupons = Coupon::where('type_creater', $type_creater)
            ->orderBy('created_at', 'DESC')
            ->where('name', 'like', '%' . $keywords . '%')
            ->paginate(10);

        if ($coupons) {
            return response()->json(['coupons' => $coupons]);
        }
        return response()->json([], 204);
    }

    /**
     * GET: http://localhost/bisto_web/shop/coupon/2
     */
    public function GetCouponShopById($id)
    {
        UserController::AuthShop();
        return $this->get_coupon($id);
    }

    /**
     * POST: http://localhost/bisto_web/shop/coupon
     */
    public function ShopInsertCoupon(Request $request)
    {
        UserController::AuthShop();
        $user = Session()->get('shop_id');
        return $this->insert_coupon($request, $user);
    }

    /**
     * PUT: http://localhost/bisto_web/shop/coupon/2
     */
    public function ShopUpdateCoupon(Request $request, $id)
    {
        UserController::AuthShop();
        $user = Session()->get('shop_id');
        return $this->update_coupon($request, $id, $user);
    }

    /**
     * DELETE: http://localhost/bisto_web/shop/coupon/2
     */
    public function ShopDeleteCoupon($id)
    {
        UserController::AuthShop();
        return $this->delete_coupon($id);
    }

    /**Viết các hàm dùng chung */
    private function get_coupon($id)
    {
        $coupon = Coupon::find($id);
        if ($coupon) {
            return response()->json(['coupon' => $coupon]);
        }
        return response()->json([], 204);
    }

    private function insert_coupon(Request $request, $user)
    {
        $isExist = Coupon::where('code', $request->code)->count();
        if ($isExist) {
            return response()->json(['message' => 'Mã code bị trùng'], 400);
        }

        if (!$request->description || !$request->code || !$request->name) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $data = array();

        $data['name'] = $request->name;
        $data['description'] = $request->description;
        $data['number_voucher'] = $request->number_voucher;
        $data['code'] = $request->code;
        $data['amount'] = $request->price;
        $data['type'] = $request->type;
        $data['required_minimum_price'] = $request->minimum_price;
        $data['max_reduce_price'] = $request->max_reduce_price;
        $data['start_at'] = $request->start_at;
        $data['end_at'] = $request->end_at;
        $data['used'] = 0;
        $data['user_id'] = $user;
        $data['description'] = $request->description;

        $result = Coupon::insert($data);
        if ($result) {
            return response()->json(['message' => 'Thêm mã giảm giá thành công'], 201);
        }
        return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
    }

    private function update_coupon(Request $request, $id, $user)
    {
        $coupon = Coupon::where('id', $id)->where('user_id', $user)->first();
        if (is_null($coupon)) {
            return response()->json([], 204);
        }

        if (!$request->description || !$request->code || !$request->name) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $isExist = Coupon::where('code', '=', $request->code)->where('id', '<>', $id)->first();
        if ($isExist) {
            return response()->json(['message' => 'Mã code bị trùng'], 400);
        }

        $coupon->name = $request->name;
        $coupon->description = $request->description;
        $coupon->number_voucher = $request->number_voucher;
        $coupon->code = $request->code;
        $coupon->amount = $request->price;
        $coupon->type = $request->type;
        $coupon->required_minimum_price = $request->minimum_price;
        $coupon->max_reduce_price = $request->max_reduce_price;
        $coupon->start_at = $request->start_at;
        $coupon->end_at = $request->end_at;

        $result = $coupon->save();
        if ($result) {
            return response()->json(['message' => 'Cập nhật mã giảm giá thành công']);
        }
        return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
    }

    private function delete_coupon($id)
    {
        $coupon = Coupon::find($id);
        $coupon->delete();
        return response()->json(['message' => 'Xóa mã giảm giá thành công']);
    }
}
