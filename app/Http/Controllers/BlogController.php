<?php

namespace App\Http\Controllers;

use App\Advertisement;
use App\Blog;
use App\BlogCategory;
use App\Exceptions\CustomException;
use App\File;
use App\RelTag;
use App\Tag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function AdminListsBlog()
    {
        UserController::AuthAdmin();
        return view('admin.blog.lists');
    }

    /**
     * GET: http://localhost/bisto_web/admin/blogs
     */
    public function AdminGetBlogs(): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdmin();
        $id = Session()->get('admin_id');

        $keyword = request()->keyword;
        $blog_category_id = request()->blog_category_id;

        $blogs = Blog::where('user_id', $id)
            ->orderBy('app_blogs.created_at', 'DESC')
            ->where('title', 'like', '%' . $keyword . '%')
            ->select('id', 'title', 'content', 'type', 'description', 'slug', 'blog_category_id', 'image_id', 'created_at', 'updated_at');
        if (!is_null($blog_category_id))
            $blogs = $blogs->where('blog_category_id', $blog_category_id);

        $blogs = $blogs->paginate(10);

        foreach ($blogs->items() as $blog) {
            $image = File::where('id', $blog->image_id)->first();
            $blog_category = BlogCategory::where('id', $blog->blog_category_id)->first();
            $tags = RelTag::where('blog_id', $blog->id)
                ->rightJoin('app_tags', 'rel_tag.tag_id', '=', 'app_tags.id')->get();

            $blog['tags'] = $tags;
            $blog['image'] = $image;
            $blog['blog_category'] = $blog_category;
        }

        return response()->json(['success' => 1, 'results' => $blogs]);
    }

    /**
     * GET: http://localhost/bisto_web/admin/blogs/2
     */
    public function AdminGetBlogById(Request $request, $id)
    {
        UserController::AuthAdmin();
        $user_id = Session()->get('admin_id');

        $blog = Blog::where('app_blogs.id', $id)->where('user_id', $user_id)
            ->select('id', 'title', 'content', 'description', 'type', 'slug', 'blog_category_id', 'image_id', 'created_at', 'updated_at')
            ->first();
        $blog['image'] = File::where('id', $blog->image_id)->first();
        $blog['tags'] = RelTag::where('blog_id', $blog->id)
            ->rightJoin('app_tags', 'rel_tag.tag_id', '=', 'app_tags.id')->get();

        $blog['blog_category'] = BlogCategory::where('id', $blog->blog_category_id)->first();
        return response()->json(['result' => $blog]);
    }

    /**
     * POST: http://localhost/bisto_web/admin/blogs
     * @throws CustomException
     */
    public function AdminPostBlog(Request $request)
    {
        try {
            UserController::AuthAdmin();
            $data = $request->all();
            $user_id = Session()->get('admin_id');

            if (is_null($data['image'])) {
                throw CustomException::makeBadRequest('Chọn hình ảnh cần thêm');
            }

            $image = $data['image'];

            $imageId = File::saveFiles($image['url'], $image['filename'], $image['size'], $image['type']);

            $blog = new Blog();
            $blog->image_id = $imageId;
            $blog->user_id = $user_id;
            $blog->blog_category_id = $data['blog_category_id'];
            $blog->title = $data['title'];
            $blog->content = $data['content'];
            $blog->type = $data['type'];
            $blog->slug = $data['slug'];
            $blog->description = $data['description'];

            $result = $blog->save();

            $rel_tags = [];
            foreach ($data['tag_ids'] as $tag_id) {
                array_push($rel_tags, ['tag_id' => $tag_id, 'blog_id' => $blog->id]);
            }
            RelTag::insert($rel_tags);

            return response()->json(['message' => 'Thêm blog thành công'], 201);

        } catch (\Exception $e) {
            throw CustomException::makeServerError($e->getMessage());
        }
    }

    /**
     * PUT: http://localhost/bisto_web/admin/blogs/2
     */
    public function AdminPutBlog(Request $request, $id)
    {
        UserController::AuthAdmin();
        $user_id = Session()->get('admin_id');

        $blog = Blog::where('id', $id)->where('user_id', $user_id)->first();
        if (is_null($blog)) {
            return response()->json([], 204);
        }
        $oldImage = File::where('id', $blog->image_id)->first();

        if ($request->image) {
//            $oldImage->delete();
            $newImage = new File();
            $newImage->url = $request->image['url'];
            $newImage->size = $request->image['size'];
            $newImage->type = $request->image['type'];
            $newImage->filename = $request->image['filename'];
            $newImage->save();
            $blog->image_id = $newImage->id;
        }

        $blog->title = $request->title;
        $blog->content = $request['content'];
        $blog->description = $request->description;
        $blog->type = $request->type;

        if (!is_null($request->slug)) {
            $blog->slug = $request->slug;
        }
        $blog->blog_category_id = $request->blog_category_id;
        $result = $blog->save();
        if (!is_null($request->tag_ids_delete)) {
            foreach ($request->tag_ids_delete as $tag_id_delete) {
                RelTag::where('blog_id', $id)->where('tag_id', $tag_id_delete)->delete();
            }
        }

        $rel_tags = [];
        if (!is_null($request->tag_ids_create)) {
            foreach ($request->tag_ids_create as $tag_id_create) {
                array_push($rel_tags, ['tag_id' => $tag_id_create, 'blog_id' => $id]);
            }
        }
        RelTag::insert($rel_tags);

        if ($result) {
            return response()->json(['message' => 'Cập nhật blog thành công']);
        }
        return response()->json(['message', 'Dữ liệu không hợp lệ'], 400);
    }

    /**
     * DELETE: http://localhost/bisto_web/admin/blogs/2
     */
    public function AdminDeleteBlog($id)
    {
        UserController::AuthAdmin();
        $blog = Blog::where('id', $id)->where('user_id', Session()->get('admin_id'))->first();
        if (is_null($blog)) {
            return response()->json([], 204);
        }

        File::where('id', $blog->image_id)->delete();
        RelTag::where('blog_id', $id)->delete();
        $blog->delete();
        return response()->json(['message' => 'Xóa blog thành công']);
    }

    public function blog_list_all_category()
    {
        $blog_categories = BlogCategory::orderBy('app_blog_categories.created_at', 'ASC')->get();
//        foreach ($blog_categories as $blog_category) {
//            $blog_category['blog_data'] = $this->getBlogByCate(Blog::TYPE_NORMAL, 10, $blog_category->id);
//        }
//
//        $blog_data_hot = $this->getBlogByCate(Blog::TYPE_HOT, 10, null);
        return view('blog-page.blog-list-all-category')->with(compact('blog_categories'));;
    }

    public function blog_detail($slug)
    {
        if (is_null($slug)) return view('404page');

        $blog = Blog::where('slug', $slug)
            ->select('id', 'title', 'content', 'description', 'type', 'slug', 'blog_category_id', 'image_id', 'created_at', 'updated_at')
            ->first();

        $advertisements = Advertisement::orderBy('app_advertisements.created_at', 'DESC')
            ->select('id', 'url', 'image_id', 'created_at', 'updated_at')
            ->paginate(10);
        foreach ($advertisements->items() as $advertisement) {
            $image = File::where('id', $advertisement->image_id)->first();
            $advertisement['image'] = $image;
        }
        $advertisements = $advertisements->items();

        if (is_null($blog)) return view('404page');
        $blog['image'] = File::where('id', $blog->image_id)->first();
        $blog['tags'] = RelTag::where('blog_id', $blog->id)
            ->rightJoin('app_tags', 'rel_tag.tag_id', '=', 'app_tags.id')->get();

        $blog['blog_category'] = BlogCategory::where('id', $blog->blog_category_id)->first();

        return view('blog-page.detail')->with(compact('blog', 'advertisements'));;
    }

    /**
     * GET: http://localhost/bisto_web/blogs
     */
    public function GetBlogs(): \Illuminate\Http\JsonResponse
    {
        $keyword = request()->keyword;
        $limit = request()->limit ?? Blog::PER_PAGE;
        $blog_category_id = request()->blog_category_id;
        $type = request()->type;

        $blogs = Blog::orderBy('app_blogs.created_at', 'DESC')
            ->where('title', 'like', '%' . $keyword . '%')
            ->select('id', 'title', 'content', 'description', 'type', 'slug', 'blog_category_id', 'image_id', 'created_at', 'updated_at');

        if ($type == Blog::TYPE_HOT) {
            $blogs = $blogs->where('type', $type);
            $blog_category_id = null;
        }

        if (!is_null($blog_category_id))
            $blogs = $blogs->where('blog_category_id', $blog_category_id);

        $blogs = $blogs->paginate($limit);

        foreach ($blogs->items() as $blog) {
            $image = File::where('id', $blog->image_id)->first();
            $blog_category = BlogCategory::where('id', $blog->blog_category_id)->first();
            $tags = RelTag::where('blog_id', $blog->id)
                ->rightJoin('app_tags', 'rel_tag.tag_id', '=', 'app_tags.id')->get();

            $blog['tags'] = $tags;
            $blog['image'] = $image;
            $blog['blog_category'] = $blog_category;
        }

        return response()->json(['success' => 1, 'results' => $blogs]);
    }

    public function getBlogByCate($type, $limit, $blog_category_id)
    {
        $blogs = Blog::orderBy('app_blogs.created_at', 'DESC')
            ->select('id', 'title', 'description', 'type', 'slug', 'blog_category_id', 'image_id', 'created_at', 'updated_at');

        if ($type == Blog::TYPE_HOT) {
            $blogs = $blogs->where('type', $type);
            $blog_category_id = null;
        }

        if (!is_null($blog_category_id))
            $blogs = $blogs->where('blog_category_id', $blog_category_id);

        $blogs = $blogs->paginate($limit);

        foreach ($blogs->items() as $blog) {
            $image = File::where('id', $blog->image_id)->first();
            $blog_category = BlogCategory::where('id', $blog->blog_category_id)->first();
            $tags = RelTag::where('blog_id', $blog->id)
                ->rightJoin('app_tags', 'rel_tag.tag_id', '=', 'app_tags.id')->get();

            $blog['tags'] = $tags;
            $blog['image'] = $image;
            $blog['blog_category'] = $blog_category;
        }

        return $blogs;
    }
}
