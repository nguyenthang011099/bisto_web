<?php

namespace App\Http\Controllers\API;

use App\Http\Response\API\BaseResponse as BaseAPIResponse;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseCoreController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class BaseController extends BaseCoreController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, BaseAPIResponse;
}
