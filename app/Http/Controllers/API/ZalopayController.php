<?php

namespace App\Http\Controllers\API;

use App\Bcoin;
use App\Http\Controllers\Controller;
use App\UserTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ZalopayController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paymentCallback(Request $request)
    {
        $result = [];

        $request_mac = $request->post('mac');
        $request_data = $request->post('data');

        try {
            $key2 = config('payment.zalopay.key2');

            $mac = hash_hmac("sha256", json_encode($request_data), $key2);

            // kiểm tra callback hợp lệ (đến từ ZaloPay server)
            if (strcmp($mac, $request_mac) != 0) {
                // callback không hợp lệ
                $result["return_code"] = -1;
                $result["return_message"] = "mac not equal";
            } else {
                $transaction = UserTransaction::where('code', $request_data['app_trans_id'])->first();

                if($transaction != null && $transaction->status == UserTransaction::STATUS_PROCESSING) {

                    DB::transaction(function() use ($request_data, $transaction, $request){
                            // thanh toán thành công
                            $transaction->status = UserTransaction::STATUS_SUCCESS;
                            // update bcoin user
                            Bcoin::where('user_id', $transaction->user_id)->increment('amount', (int)$transaction->transaction_amount);

                        $transaction->response_receive = json_encode($request->all());
                        $transaction->note = json_encode($request_data['item']);
                        $transaction->save();
                    });
                }

                $result["return_code"] = 1;
                $result["return_message"] = "success";
            }
        } catch (\Exception $e) {
            $result["return_code"] = 0; // ZaloPay server sẽ callback lại (tối đa 3 lần)
            $result["return_message"] = $e->getMessage();
        }

        return response()->json($result);
    }

}
