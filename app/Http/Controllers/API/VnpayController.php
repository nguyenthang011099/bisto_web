<?php

namespace App\Http\Controllers\API;

use App\Bcoin;
use App\Http\Controllers\Controller;
use App\UserTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VnpayController extends Controller
{
    public function paymentCallback(Request $request)
    {
        $returnData = array();
        try {
            $result = $this->parseUrl($request);

            //Kiểm tra checksum của dữ liệu
            if ($result['secureHash'] == $result['vnp_SecureHash']) {
                //Lấy thông tin đơn hàng lưu trong Database và kiểm tra trạng thái của đơn hàng, mã đơn hàng là: $orderId
                //Việc kiểm tra trạng thái của đơn hàng giúp hệ thống không xử lý trùng lặp, xử lý nhiều lần một giao dịch
                $transaction = UserTransaction::where('code', $result['orderId'])->first();

                if ($transaction != NULL) {
                    if ($transaction->status == UserTransaction::STATUS_PROCESSING) {
                        DB::transaction(function() use ($result, $transaction, $request){
                            if ($result['vnp_ResponseCode'] == '00') {
                                // thanh toán thành công
                                $transaction->status = UserTransaction::STATUS_SUCCESS;

                                // update bcoin user
                                Bcoin::where('user_id', $transaction->user_id)->increment('amount', (int)$transaction->transaction_amount);
                            } else {
                                $transaction->status = UserTransaction::STATUS_FAIL;
                            }
                            $transaction->response_receive = json_encode($request->all());
                            $transaction->note = $result['vnp_OrderInfo'];
                            $transaction->save();
                        });

                        //Trả kết quả về cho VNPAY: Website TMĐT ghi nhận yêu cầu thành công
                        $returnData['RspCode'] = '00';
                        $returnData['Message'] = 'Confirm Success';
                    } else {
                        $returnData['RspCode'] = '02';
                        $returnData['Message'] = 'Order already confirmed';
                    }
                } else {
                    $returnData['RspCode'] = '01';
                    $returnData['Message'] = 'Order not found';
                }
            } else {
                $returnData['RspCode'] = '97';
                $returnData['Message'] = 'Chu ky khong hop le';
            }
        } catch (\Exception $e) {
            Log::debug('Error VnpayController::paymentCallback(): ' . $e->getMessage() . $e->getTraceAsString());
            $returnData['RspCode'] = '99';
            $returnData['Message'] = 'Unknow error';
        }

        return $returnData;
    }

    private function parseUrl(Request $request)
    {
        $data = $request->all();
        $vnp_HashSecret = config('payment.vnpay.hash_secret', '');
        foreach ($data as $key => $value) {
            if (substr($key, 0, 4) == "vnp_") {
                $inputData[$key] = $value;
            }
        }

        $vnp_SecureHash = $inputData['vnp_SecureHash'];
        unset($inputData['vnp_SecureHashType']);
        unset($inputData['vnp_SecureHash']);
        ksort($inputData);
        $i = 0;
        $hashData = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashData = $hashData . '&' . $key . "=" . $value;
            } else {
                $hashData = $hashData . $key . "=" . $value;
                $i = 1;
            }
        }
        $vnpTranId = $inputData['vnp_TransactionNo']; //Mã giao dịch tại VNPAY
        $vnp_BankCode = $inputData['vnp_BankCode']; //Ngân hàng thanh toán
        $secureHash = hash('sha256', $vnp_HashSecret . $hashData);
        $orderId = $inputData['vnp_TxnRef'];
        $vnp_ResponseCode = $inputData['vnp_ResponseCode'];
        $vnp_OrderInfo = $inputData['vnp_OrderInfo'];

        return [
            'vnp_SecureHash' => $vnp_SecureHash,
            'secureHash' => $secureHash,
            'vnp_ResponseCode'=> $vnp_ResponseCode,
            'orderId' => $orderId,
            'vnpTranId' => $vnpTranId,
            'vnp_BankCode' => $vnp_BankCode,
            'vnp_OrderInfo' => $vnp_OrderInfo
        ];
    }

}
