<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\File;
use App\GiaoHangNhanh;
use App\Notification;
use App\ProductRate;
use App\Services\Shipping\Ghn;
use App\ShippingInfo;
use App\ShopRate;
use App\Stylish;
use App\User;
use App\UserNotification;
use App\UserTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Order;
use App\Shop;
use App\SizeInformation;
use App\Bcoin;

class ShopController extends Controller
{
    public function dashboard()
    {
        UserController::AuthShop();
        $product = Product::where('user_id', Session()->get('shop_id'))->count();
        $order = Order::where('seller_id', Session()->get('shop_id'))->count();
        $order_success = Order::where('seller_id', Session()->get('shop_id'))->where('status', Order::DON_THANH_CONG)->count();
        $order_failed = Order::where('seller_id', Session()->get('shop_id'))->where('status', Order::NGUOI_MUA_HUY_DON)->count();
        $order_refund = Order::where('seller_id', Session()->get('shop_id'))->where('status', Order::NGUOI_MUA_TRA_HANG)->count();
        $shop_id = Session()->get('shop_id');
        $count_promotion = Coupon::where('type_creater', '3')->count();
        $count_product_block = Product::where('user_id', $shop_id)->whereIn('admin_status', [-1])->count();
        $count_product_sold_out = Product::where('user_id', $shop_id)->join('rel_product_varieties', 'app_products.id', '=', 'rel_product_varieties.product_id')
            ->where('rel_product_varieties.quantity', 0)->distinct()->count();
        $product_sold_out = Product::where('app_products.user_id', $shop_id)->join('rel_product_varieties', 'app_products.id', '=', 'rel_product_varieties.product_id')
            ->groupBy('rel_product_varieties.product_id')
            ->havingRaw('SUM(rel_product_varieties.quantity) = ?', ['SUM(rel_product_varieties.sold)'])->count();
        return view('shop.dashboard')->with(compact('product', 'order', 'order_success', 'order_failed', 'order_refund', 'product_sold_out', 'count_promotion', 'count_product_block', 'count_product_sold_out'));
    }

    /**
     * GET: http://localhost/bisto_web/shop/password-page
     */
    public function getPasswordPage()
    {
        UserController::AuthShop();
        return view('shop.user.update_password');
    }

    /**
     * GET: http://localhost/bisto_web/shop/passwords
     */
    public function GetPassword()
    {
        UserController::AuthShop();
        $user = new User();
        $user = $user->find(Session()->get('shop_id'));

        if ($user) {
            return response()->json(['user' => $user], 200);
        }
        return response()->json([], 204);
    }

    /**
     * POST: http://localhost/bisto_web/shop/passwords
     */
    public function UpdatePassword(Request $req)
    {
        UserController::AuthShop();
        $admin = new User();
        $admin = $admin->find(Session()->get('shop_id'));
        $data = $req->all();
        $passwordReq = $data['current_password'];

        if (password_verify($passwordReq, $admin->password)) {
            $admin->password = password_hash($data['new_password'], PASSWORD_BCRYPT, array('cost' => 12));
            $admin->save();
            return response()->json(['message', 'Thay đổi mật khẩu thành công'], 200);
        }
        return response()->json(['message', 'Mật khẩu không đúng'], 400);
    }

    /**
     * GET: http://localhost/bisto_web/shop/profile-page
     */
    public function getProfilePage()
    {
        UserController::AuthShop();
        return view('shop.user.profile');
    }

    /**
     * GET: http://localhost/bisto_web/shop/profiles
     */
    public function GetProfile()
    {
        UserController::AuthShop();
        $id = Session()->get('shop_id');

        //get information of owner
        $profile = User::where('id', $id)
            ->select('id', 'fullname', 'email', 'avatar_id',
                'cover_id', 'role', 'shop_id', 'phone')->first();
        $bcoins = Bcoin::where('user_id', $id)->value('amount');

        $profile->bcoins = 0;
        if (!is_null($bcoins)) {
            $profile->bcoins = $bcoins;
        }
        $profile->rates = ShopRate::getAvgRates($id);

        //Lấy tỉ lệ đơn thành công, thất bại
        $totalOrders = Order::getOrderByStatus($id, null);
        $ordersSuccess = Order::getOrderByStatus($id, 9);
        $ordersFail = Order::getOrderByStatus($id, 6);

        $profile->ratio_order_success = 0;
        $profile->ratio_order_fail = 0;
        if ($totalOrders) {
            $profile->ratio_order_success = round(100 * ($ordersSuccess / $totalOrders), 2);
            $profile->ratio_order_fail = round(100 * ($ordersFail / $totalOrders), 2);
        }

        //get information of shop
        $shop = Shop::where('id', $profile->shop_id)
            ->select('id', 'name', 'description', 'range_age', 'cmnd', 'tax_code',
                'stylists', 'parent_id')->first();
        //get stylish information
        $stylish_id = explode(',', $shop->stylists);
        $stylish = Stylish::whereIn('id', $stylish_id)->get();

        //get name of parent shop
        $parent_shop = Shop::where('id', $shop->parent_id)->value('name');

        $coverImage = File::where('id', $profile->cover_id)->first();
        $avatarImage = File::where('id', $profile->avatar_id)->first();

        return response()->json(['profile' => $profile, 'shop' => $shop, 'stylish' => $stylish,
            'parent_shop' => $parent_shop, 'avatar' => $avatarImage, 'cover' => $coverImage]);
    }

    /**
     * POST: http://localhost/bisto_web/shop/profiles
     */
    public function UpdateProfile(Request $request)
    {
        UserController::AuthShop();
        $user_id = Session()->get('shop_id');
        $user = User::where('id', $user_id)->first();
        if (is_null($user)) {
            return response()->json([], 204);
        }

        if (is_null($request->full_name)) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $user->fullname = $request->full_name;
        $user->email = $request->email;
        $user->save();

        $shop = Shop::where('id', $user->shop_id)->first();
        $shop->name = $request->shop_name;
        $shop->description = $request->description;
        $shop->cmnd = $request->cmnd;
        $shop->tax_code = $request->tax_code;

        if ($request->stylish) {
            $shop->stylists = implode(',', $request->stylish);
        }

        if ($request->range_age) {
            $shop->range_age = implode(',', $request->range_age);
        }

        $shop->save();
        return response()->json(['message', 'Thay đổi thông tin thành công']);
    }

    /**
     * GET: http://localhost/bisto_web/shop/addresses
     */
    public function GetAddress()
    {
        UserController::AuthShop();
        $shipping_info = ShippingInfo::where("user_id", Session()->get('shop_id'))
            ->where("is_default", 1)->first();

        if ($shipping_info) {
            return response()->json(['address' => $shipping_info]);
        }
        return response()->json([], 204);
    }

    /**
     * PUT: http://localhost/bisto_web/shop/address-page
     */
    public function getAddressPage()
    {
        UserController::AuthShop();
        $shipping_info = ShippingInfo::where('user_id', Session()->get('shop_id'))
            ->where('is_default', 1)->first();
        return view('shop.user.add_address')->with(compact('shipping_info'));
    }

    /**
     * POST: http://localhost/bisto_web/shop/addresses
     */
    public function UpdateAddress(Request $request)
    {
        UserController::AuthShop();
        $user_id = Session()->get('shop_id');
        if (is_null($request->address)) {
            return response()->json(['message' => 'Địa chỉ không được để trống'], 400);
        }

        $data['address'] = $request->address;
        $data['city_id'] = $request->province_id;
        $data['district_id'] = $request->district_id;
        $data['ward_id'] = $request->ward_id;

        $user = User::where('id', $user_id)->select('fullname', 'phone', 'shop_id')->first();
        if (is_null($user)) {
            return response()->json(['message' => 'Địa chỉ người dùng không tồn tại'], 400);
        }

        $data['name'] = $user->fullname;
        $data['phone'] = $user->phone;
        $data['user_id'] = $user_id;
        $data['is_default'] = 1;
        $ghn_message = '';
        try {
            $ghn_address_info = Ghn::getInstance()->searchAddressByBisto($data['city_id'], $data['district_id'], $data['ward_id']);
            if (!is_null($ghn_address_info)) {
                $response_store = Ghn::getInstance()->createStore($ghn_address_info['district']->DistrictID, $ghn_address_info['ward']->WardCode, $data['name'], $data['phone'], $data['address']);
                if ($response_store->getStatusCode() == 200) {
                    $response_store = json_decode($response_store->getBody()->getContents());
                    $ghn_shop_id = $response_store->data->shop_id;
                    $ghn = GiaoHangNhanh::where('shop_id', $user->shop_id)->first();
                    if (is_null($ghn)) {
                        $ghn = [
                            'giao_hang_nhanh_shop_id' => $ghn_shop_id,
                            'shop_id' => $user->shop_id
                        ];
                        GiaoHangNhanh::insert($ghn);
                    } else {
                        $ghn->giao_hang_nhanh_shop_id = $ghn_shop_id;
                        $ghn->save();
                    }
                } else $ghn_message = ', địa chỉ này không khả dụng với Giao hàng nhanh';
            } else $ghn_message = ', địa chỉ này không khả dụng với Giao hàng nhanh';

        } catch (\Exception $exception) {
            $ghn_message = ', địa chỉ này không khả dụng với Giao hàng nhanh';
        }

        $shipping_info = ShippingInfo::where('user_id', $user_id)
            ->where('is_default', 1)->first();

        if (is_null($shipping_info)) {
            $result = ShippingInfo::insert($data);
            if ($result) {
                return response()->json(['message' => 'Thêm mới địa chỉ thành công' . $ghn_message], 201);
            }
        } else {
            $result = ShippingInfo::where('id', $shipping_info->id)->update($data);
            if ($result) {
                return response()->json(['message' => 'Thay đổi địa chỉ thành công' . $ghn_message]);
            }
        }
        return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
    }

    /**
     * GET: http://localhost/bisto_web/shop/avatar
     */
    public function GetAvatar()
    {
        UserController::AuthShop();
        $id = Session()->get('shop_id');
        $avatar_id = User::where('id', $id)->value('avatar_id');
        if (is_null($avatar_id)) {
            $url = DEFAULT_AVATAR;
        } else {
            $url = File::where('id', $avatar_id)->value('url');
        }
        return response()->json(array('source' => $url));
    }

    /**
     * POST: http://localhost/bisto_web/shop/upload-avatar
     */
    public function UploadAvatar(Request $request)
    {
        UserController::AuthShop();
        $data = $request->imageUrl;
        $id = File::saveFiles($data['url'], $data['name'], $data['size'], $data['type']);

        $user = User::where('id', Session()->get('shop_id'))->first();
        if ($user->avatar_id) {
            File::find($user->avatar_id)->delete();
        }
        $user->avatar_id = $id;
        $user->save();

        return response()->json(array('url' => $data['url'], 'success' => 'true'));
    }

    /**
     * POST: http://localhost/bisto_web/shop/upload-cover
     */
    public function UploadCover(Request $request)
    {
        UserController::AuthShop();
        $data = $request->imageUrl;
        $id = File::saveFiles($data['url'], $data['name'], $data['size'], $data['type']);

        $user = User::where('id', Session()->get('shop_id'))->first();
        if ($user->cover_id) {
            File::find($user->cover_id)->delete();
        }
        $user->cover_id = $id;
        $user->save();

        return response()->json(array('url' => $data['url'], 'success' => 'true'));
    }

    /**
     * GET /shop/all-stylish
     */
    public function getAllStylish()
    {
        UserController::AuthShop();
        $stylishes = Stylish::get();
        return response()->json(['stylishes' => $stylishes]);
    }

    /**
     * GET /shop/list-image-size
     */
    public function getImageSizePage()
    {
        UserController::AuthShop();
        return view('shop.size.lists');
    }

    /**
     * GET: http://localhost/bisto_web/shop/image-sizes
     */
    public function GetImageSize()
    {
        UserController::AuthShop();

        $keywords = request()->keyword;
        $size_image = SizeInformation::where('user_id', Session()->get('shop_id'))
            ->join('app_files', 'app_size_informations.image_id', '=', 'app_files.id')
            ->select('app_size_informations.id as id', 'app_size_informations.name',
                'app_size_informations.image_id', 'app_files.url')
            ->where('app_size_informations.name', 'like', '%' . $keywords . '%')
            ->orderBy('app_size_informations.created_at', 'DESC')
            ->paginate(10);

        if ($size_image) {
            return response()->json(['size_image' => $size_image]);
        }

        return response()->json([], 204);
    }

    /**
     * GET: http://localhost/bisto_web/shop/image-sizes/3
     */
    public function GetImageSizeById($id)
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');
        $size_info = SizeInformation::where('id', $id)->where('user_id', $shop_id)
            ->select('id', 'name', 'image_id')
            ->first();

        if (is_null($size_info)) {
            return response()->json([], 204);
        }

        $size_info->url = File::where('id', $size_info->image_id)->value('url');
        return response()->json(['result' => $size_info]);
    }

    /**
     * POST: http://localhost/bisto_web/shop/image-sizes
     */
    public function InsertImageSize(Request $request)
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');
        $size = array();

        $isExist = SizeInformation::where('name', '=', $request->name)->first();
        if ($isExist) {
            return response()->json(['message' => 'Tên size bị trùng'], 400);
        }

        $size['name'] = $request->name;
        if (is_null($request->source_url)) {
            return response()->json(['message' => 'Hãy chọn ảnh cho size'], 400);
        }
        $data = $request->source_url;

        if ($request->name) {
            $image_id = File::saveFiles($data['url'], $data['filename'], $data['size'], $data['type']);
            $size['image_id'] = $image_id;
            $size['user_id'] = $shop_id;
            SizeInformation::insert($size);
            return response()->json(['message' => 'Thêm thông tin size thành công'], 201);
        } else {
            return response()->json(['message' => 'Lưu ảnh không thành công, hãy thử lại'], 400);
        }
    }

    /**
     * PUT: http://localhost/bisto_web/shop/image-sizes/2
     */
    public function UpdateImageSize(Request $request, $id)
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');

        $size_info = SizeInformation::where('id', $id)->where('user_id', $shop_id)->first();
        if (is_null($size_info)) {
            return response()->json([], 204);
        }

        $isExist = SizeInformation::where('name', '=', $request->name)->where('id', '<>', $id)->first();
        if ($isExist) {
            return response()->json(['message' => 'Tên thông tin size bị trùng'], 400);
        }

        $deleted_imageId = $request->deleted_imageid;
        if ($deleted_imageId != null) {
            File::where('id', $deleted_imageId)->delete();
        }

        $imageId = $size_info->image_id;
        if ($request->source_url) {
            $data = $request->source_url;
            $imageId = File::saveFiles($data['url'], $data['filename'], $data['size'], $data['type']);
        }

        $size_info->image_id = $imageId;
        $size_info->name = $request->name;
        $result = $size_info->save();
        if ($result) {
            return response()->json(['message' => 'Sửa thông tin size thành công']);
        }
        return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
    }

    /**
     * DELETE: http://localhost/bisto_web/shop/image-sizes/2
     */
    public function DeleteImageSize($id)
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');
        $size_info = SizeInformation::where('id', $id)->where('user_id', $shop_id)->first();

        if (is_null($size_info)) {
            return response()->json([], 204);
        }

        File::where('id', $size_info->image_id)->delete();
        $size_info->delete();
        return response()->json(['message' => 'Xóa hình ảnh size thành công']);
    }

    /* manage rating and comments of products */
    public function manage_rate_product()
    {
        UserController::AuthShop();
        return view('shop.rate.lists');
    }

    /**
     * GET: http://localhost/bisto_web/shop/products/ratings
     */
    public function GetRatingProducts()
    {
        UserController::AuthShop();
        $owner = Session()->get('shop_id');
        $keywords = request()->keyword;

        $product_ids = ProductRate::select('product_id')
            ->distinct()->get();

        $ids = array();
        foreach ($product_ids as $item) {
            array_push($ids, $item->product_id);
        }

        $products = Product::where('user_id', $owner)->whereIn('id', $ids)
            ->where('name', 'like', '%' . $keywords . '%')
            ->select('id as product_id', 'name', 'image_ids')
            ->paginate(12);

        foreach ($products as $item) {
            // Lấy hình ảnh sản phẩm
            $image_ids = explode(",", $item->image_ids);
            $index = rand(0, count($image_ids) - 1);
            $source_url = File::where('id', $image_ids[$index])->value('url');
            $item->url = $source_url;

            list($comments, $rates, $number_rates) = $this->calculate_avg_rates($item->product_id);
            $item->comment = $comments;
            $item->rate = $rates;
            $item->number_of_rates = $number_rates;
        }
        return response()->json(['results' => $products]);
    }

    // Lấy bình luận và tính trung bình ratings
    private function calculate_avg_rates($id)
    {
        $rates = ProductRate::where('product_id', $id)
            ->where('rate', '<>', null)->get();
        $cnt = ProductRate::where('product_id', $id)->count('id');
        $avg_rates = 0;
        foreach ($rates as $rate) {
            $avg_rates += $rate->rate;
        }

        $number_rates = count($rates);
        $avg_rates /= $number_rates;
        return array($cnt, $avg_rates, $number_rates);
    }

    //Chi tiết đánh giá
    public function view_rate_product($product_id)
    {
        UserController::AuthShop();
        return view('shop.rate.detail')->with(['product_id' => $product_id]);
    }

    /**
     * GET: http://localhost/bisto_web/shop/view/ratings/3
     */
    public function ViewProductRatings($id)
    {
        UserController::AuthShop();
        $owner = Session()->get('shop_id');

        $users = ProductRate::where('product_id', $id)->whereNull('parent_id')
            ->select('id', 'rate', 'comment', 'user_id', 'created_at', 'file_id')
            ->get();

        $results = array();
        foreach ($users as $key => $item) {
            $user = User::where('id', $item->user_id)
                ->select('fullname', 'avatar_id')->first();
            $item->full_name = $user->fullname;
            $item->avatar = File::getAvatarUrlById($user->avatar_id);
            $item->rate_url = File::where('id', $item->file_id)->value('url');

            $item->child = ProductRate::where('parent_id', $item->id)
                ->select('comment', 'user_id', 'created_at', 'file_id')
                ->get();

            if ($item->child) {
                foreach ($item->child as $rate) {
                    $user = User::where('id', $rate->user_id)
                        ->select('fullname', 'avatar_id')->first();
                    $rate->full_name = $user->fullname;
                    $rate->avatar = File::getAvatarUrlById($user->avatar_id);
                    $rate->rate_url = File::where('id', $rate->file_id)->value('url');
                }
            }
            array_push($results, $item);
        }

        //Chi tiết sản phẩm
        $product = Product::where('id', $id)->where('user_id', $owner)
            ->select('name', 'price', 'discount', 'start_at',
                'end_at', 'description', 'image_ids')
            ->first();

        if (is_null($product)) {
            return response()->json([], 204);
        }

        //Lấy ra hình ảnh sản phẩm
        $image_ids = explode(",", $product->image_ids);
        $source_urls = array();
        foreach ($image_ids as $id) {
            $url = File::where('id', $id)->value('url');
            array_push($source_urls, $url);
        }
        $product->url = $source_urls;

        return response()->json(['results' => $results, 'product' => $product]);
    }

    /**
     * POST: http://localhost/bisto_web/shop/products/ratings
     */
    public function SaveReplyRatings(Request $request)
    {
        UserController::AuthShop();
        $owner = Session()->get('shop_id');

        if (!$request->comment && !$request->image_file) {
            return response()->json(['message' => 'Bình luận không được để trống'], 400);
        }

        if (!$request->parent_id) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $product_id = ProductRate::where('id', $request->parent_id)->value('product_id');
        if (is_null($product_id)) {
            return response()->json([], 204);
        }

        $isExist = Product::where('id', $product_id)->where('user_id', $owner)->first();
        if (is_null($isExist)) {
            return response()->json([], 204);
        }

        $rate = new ProductRate();
        $rate->comment = $request->comment;
        $rate->parent_id = $request->parent_id;
        $rate->user_id = $owner;
        $rate->product_id = $product_id;

        if ($request->image_file) {
            $data = $request->image_file;
            $file_id = File::insertGetId($data);
            $rate->file_id = $file_id;
        }

        $result = $rate->save();
        if ($result) {
            return response()->json(['message' => 'Trả lời bình luận thành công']);
        }
        return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
    }

    //get notifications
    public function getNotifications($index)
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');
        $notificationIds = DB::table('rel_user_notifications')->where('user_id', $shop_id)->pluck('notification_id')->toArray();
        $notifications = Notification::whereIn('id', $notificationIds)->orderBy('created_at', "DESC")->get();
        $notificationNotSeenIds = DB::table('rel_user_notifications')->where('user_id', $shop_id)->where('is_seen', false)->pluck('notification_id')->toArray();
        $notificationsNotSeenCount = UserNotification::whereIn('id', $notificationNotSeenIds)->count();
        $results = array();
        $haveMore = 0;
        // if have more 5 records remain
        if (count($notifications) > (5 * $index)) {
            $haveMore = 1;
            for ($i = 0; $i < 5; $i++) {
                array_push($results, $notifications[5 * $index - 5 + $i]);
            }
        } // if have little 5 records remain
        else {
            for ($i = 0; $i + 5 * $index - 5 < count($notifications); $i++) {
                array_push($results, $notifications[5 * $index - 5 + $i]);
            }
        }
        return response()->json(['success' => 1, 'results' => $results, 'haveMore' => $haveMore, 'countSeenNotification' => $notificationsNotSeenCount], 200);
    }

    /**
     * SHOW PAYMENT
     */
    public function payment()
    {
        UserController::AuthShop();
        return view('shop.payment.payment');
    }

    /**
     * SHOW PAYMENT
     */
    public function payment_return($provider)
    {
        UserController::AuthShop();
        switch ($provider) {
            case UserTransaction::PROVIDER_VNPAY:
                return view('shop.payment.vnpay_return');
            case UserTransaction::PROVIDER_ZALOPAY:
                return view('shop.payment.zalopay_return');
            case UserTransaction::PROVIDER_MOMO:
                return view('shop.payment.momo_return');
            default:
                return route('admin.dashboard');
        }
    }

    public function updateStatusNotification(Request $req)
    {
        UserController::AuthShop();
        $shop_id = Session()->get('shop_id');
        $notificationIds = DB::table('rel_user_notifications')->where('user_id', $shop_id)->whereIn('notification_id', $req->listId)->update(['is_seen' => true]);
        $count = DB::table('rel_user_notifications')->where('user_id', $shop_id)->where('is_seen', false)->count();
        return response()->json(['count' => $count], 200);
    }
}
