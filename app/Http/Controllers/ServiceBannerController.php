<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use Illuminate\Http\Request;
use App\ServiceBanner;
use App\File;
use App\User;
use Illuminate\Support\Facades\Input;

class ServiceBannerController extends Controller
{
    /** Admin */
    public function admin_service_banner()
    {
        UserController::AuthAdmin();
        return view('admin.banner.lists_service_banner');
    }

    /**
     * GET: http://localhost/bisto_web/admin/banners
     */
    public function GetServiceBannersAdmin()
    {
        UserController::AuthAdmin();
        $user = Session()->get('admin_id');

        $keyword = request()->keyword;
        $serviceBanners = ServiceBanner::orderBy('created_at', 'DESC')->where('user_id', $user)
            ->where('name', 'like', '%' . $keyword . '%')->paginate(10);
        return response()->json(['results' => $serviceBanners->appends(Input::except('page'))], 200);
    }

    /**
     * GET: http://localhost/bisto_web/admin/banners/2
     */
    public function GetAdminServiceBannerById($id)
    {
        UserController::AuthAdmin();
        $user_id = Session()->get('admin_id');
        $serviceBanner = ServiceBanner::where('id', $id)->where('user_id', $user_id)->first();
        return response()->json(['result' => $serviceBanner], 200);
    }


    /**
     * POST: http://localhost/bisto_web/admin/banners
     */
    public function InsertServiceBannerAdmin(Request $request)
    {
        try {
            UserController::AuthAdmin();
            $user_id = Session()->get('admin_id');
            ServiceBanner::insertServiceBanner($request->day, $request->name, $request->price, $user_id);
            return response()->json(['message' => 'Thêm dịch vụ thành công']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * PUT: http://localhost/bisto_web/admin/banners/2
     */
    public function UpdateServiceBannerAdmin(Request $request, $id)
    {
        try {
            UserController::AuthAdmin();
            $user_id = Session()->get('admin_id');
            ServiceBanner::updateServiceBanner($id, $request->day, $request->name, $request->price, $user_id);
            return response()->json(['message' => 'Cập nhật dịch vụ thành công']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * DELETE: http://localhost/bisto_web/admin/banners/2
     */
    public function DeleteServiceBannerAdmin($id)
    {
        try {
            UserController::AuthAdmin();
            $user_id = Session()->get('admin_id');
            ServiceBanner::deleteServiceBanner($id, $user_id);
            return response()->json(['message' => 'Xóa dịch vụ thành công']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}
