<?php

namespace App\Http\Controllers;

use App\Services\Momo\MomoGateway;
use App\Services\VnPay\VnPayGateway;
use App\Services\ZaloPay\ZaloPayGateway;
use App\User;
use App\UserTransaction;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use MongoDB\Driver\Session;

class UserTransactionController extends Controller
{

    public function getRechargeAdminShop(Request $request): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdminShop();
        $user_id = session("admin_shop_id");
        return $this->getRecharge($request, $user_id, 'admin_shop_payment.return');
    }

    public function getRechargeShop(Request $request): \Illuminate\Http\JsonResponse
    {
        UserController::AuthShop();
        $user_id = session("shop_id");
        return $this->getRecharge($request, $user_id, 'shop_payment.return');
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getRecharge(Request $request, int $user_id, string $route_name_return): \Illuminate\Http\JsonResponse
    {
        $data = Validator::make($request->all(), [
            'provider' => [
                'required',
                Rule::in(array_keys(UserTransaction::PROVIDERS))
            ],
            'amount' => 'integer|required|min:1000',
        ], [
            'provider.required' => 'Nhà cung cấp là trường bắt buộc',
            'provider.in' => 'Nhà cung cấp không hợp lệ',
            'amount.required' => 'Số tiền là trường bắt buộc',
            'amount.min' => 'Số tiền tối thiểu phải lớn hơn :min',
            'amount.integer' => 'Số tiền phải là một số',
        ])->validated();

        $data['user_id'] = $user_id;
        // tạo mới giao dịch nạp
        $transaction = UserTransaction::gateWay(UserTransaction::TYPE_RECHARGE, $data);


        switch ($data['provider']) {
            case UserTransaction::PROVIDER_VNPAY :
                $urlRedirect = VnPayGateway::getInstance()->getRedirectUrlPayment(
                    $transaction->code,
                    $data['amount'],
                    UserTransaction::TYPE_RECHARGE,
                    'Nạp bcoin',
                    route($route_name_return, ['provider' => 'vnpay']),
                    'VNPAYQR'
                );
                break;
            case UserTransaction::PROVIDER_ZALOPAY :
                $urlRedirect = ZaloPayGateway::getInstance()->createOrder(
                    $data['user_id'],
                    $transaction->code,
                    $data['amount'],
                    'Nạp bcoin',
                    route('api.zalopay.payment.callback'),
                    [['item' => 'bcoin', 'amount' => $data['amount']]],
                    ["promotion" => "none"]
                );
                break;

            case UserTransaction::PROVIDER_MOMO :
                $urlRedirect = MomoGateway::getInstance()->createPayment(
                    $data['amount'],
                    $transaction->code,
                    route($route_name_return, ['provider' => 'momo']),
                    route('api.momo.payment.callback')
                );
                break;
            default:
                $urlRedirect = route('admin.dashboard');
                break;

        }

        $transaction->status = UserTransaction::STATUS_PROCESSING;
        $transaction->save();

        return response()->json(['payment_url' => $urlRedirect]);
    }

    public function shop_payment_history_page()
    {
        UserController::AuthShop();
        return view('shop.payment.payment_history');
    }

    public function admin_shop_payment_history_page()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.payment.payment_history');
    }

    public function admin_payment_history_page()
    {
        UserController::AuthAdmin();
        return view('admin.payment.payment_history');
    }

    /**
     * @throws \Exception
     */
    public function getPaymentHistoryShop(Request $request): \Illuminate\Http\JsonResponse
    {
        UserController::AuthShop();
        $user_id = Session()->get('shop_id');
        return $this->getPaymentHistory([$user_id], $request);

    }

    /**
     * @throws \Exception
     */
    public function getPaymentHistoryAdminShop(Request $request): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdminShop();
        $user_id = Session()->get('admin_shop_id');
        return $this->getPaymentHistory([$user_id], $request);
    }

    /**
     * @throws \Exception
     */
    public function getPaymentHistoryAdmin(Request $request): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdmin();
        $user_id = Session()->get('admin_id');
        return $this->getPaymentHistory(null, $request);
    }

    /**
     * @throws \Exception
     */
    public function getPaymentHistory($user_ids, $request): \Illuminate\Http\JsonResponse
    {
        try {
            $from_date = $request->from_date;
            $to_date = $request->to_date;
            $status = $request->status;
            $provider = $request->provider;
            $user_trans = UserTransaction::orderBy('created_at', 'desc');
            if (!is_null($user_ids))
                $user_trans = UserTransaction::whereIn('user_id', $user_ids);
            if ($status) {
                $user_trans = $user_trans->where('status', $status);
            }
            if ($provider) {
                $user_trans = $user_trans->where('provider_type', $provider);
            }
            if (!is_null($from_date) && !is_null($to_date) && $from_date != 'null' && $to_date != 'null') {
                $from_date = new DateTime($from_date);
                $to_date = new DateTime($to_date);
                $interval = new DateInterval('P' . 1 . 'D');
                $to_date = $to_date->add($interval);
                $user_trans = $user_trans->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date);
            }
            $user_trans = $user_trans->paginate(10);
            if (is_null($user_ids)) {
                foreach ($user_trans as $user_tran) {
                    $user_tran->full_name = User::where('id', $user_tran->user_id)->first()->fullname;
                }
            }
            return response()->json(['results' => $user_trans], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}
