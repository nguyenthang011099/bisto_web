<?php

namespace App\Http\Controllers;

use App\File;
use App\SalesLicense;
use App\Shop;
use App\User;
use Illuminate\Http\Request;

class LicenseController extends \Illuminate\Routing\Controller
{
    /**
     * POST admin-shop/license
     */
    public function uploadLicense(Request $req)
    {
        UserController::AuthAdminShop();
        $license = new SalesLicense();
        if (is_null($req->name) || is_null($req->image_urls)) {
            return response()->json(['message' => 'hãy nhập đủ các trường dữ liệu bắt buộc (tên, ảnh)'], 400);
        }
        $user_id = Session()->get('admin_shop_id');

        // validate user_id
        $userShop = User::find($user_id);
        $shop = Shop::find($userShop->shop);
        // check if user_id has already exist
        $licenseExist = SalesLicense::where('user_id', $user_id)->count();
        if ($licenseExist != 0) {
            return response()->json(['message' => 'license with this user_id has already exist'], 400);
        }

        $image_ids = array();
        foreach ($req->image_urls as $url) {
            $imageId = File::saveFiles($url['url'], $url['filename'], $url['size'], $url['type']);
            array_push($image_ids, $imageId);
        }

        $license->name = $req->name;
        $license->description = $req->description;
        $license->image_ids = implode(',', $image_ids);
        $license->user_id = $user_id;
        $license->admin_status = 0;
        $license->save();

        return response()->json(['message' => 'upload thành công'], 200);
    }

    /**
     * POST shop/license
     */
    public function uploadLicenseShop(Request $req)
    {
        UserController::AuthShop();
        $license = new SalesLicense();
        if (is_null($req->name) || is_null($req->image_urls)) {
            return response()->json(['message' => 'hãy nhập đủ các trường dữ liệu bắt buộc (tên, ảnh)'], 400);
        }
        $user_id = Session()->get('shop_id');

        // validate user_id
        $userShop = User::find($user_id);
        $shop = Shop::find($userShop->shop_id);
        if (!is_null($shop->parent_id)) {
            return response()->json(['message' => 'user must be shop without branch'], 400);
        }
        // check if user_id has already exist
        $licenseExist = SalesLicense::where('user_id', $user_id)->count();
        if ($licenseExist != 0) {
            return response()->json(['message' => 'license with this user_id has already exist'], 400);
        }

        $image_ids = array();
        foreach ($req->image_urls as $url) {
            $imageId = File::saveFiles($url['url'], $url['filename'], $url['size'], $url['type']);
            array_push($image_ids, $imageId);
        }

        $license->name = $req->name;
        $license->description = $req->description;
        $license->image_ids = implode(',', $image_ids);
        $license->user_id = $user_id;
        $license->admin_status = 0;
        $license->save();

        return response()->json(['message' => 'upload thành công'], 200);
    }

    /**
     * delete admin-shop/license
     */
    public function deleteLicense()
    {
        UserController::AuthAdminShop();
        $id = Session()->get('admin_shop_id');
        $license = SalesLicense::where('user_id', $id);
        if ($license) {
            $license->delete();
            return response()->json(['message' => 'xóa thành công'], 200);
        } else {
            return response()->json(['message' => 'không tồn tại license tương ứng'], 400);
        }
    }

    /**
     * get admin-shop/license
     */
    public function getLicense()
    {
        UserController::AuthAdminShop();
        $user_id = Session()->get('admin_shop_id');
        $license = SalesLicense::where('user_id', $user_id)->first();
        if ($license) {
            $image_ids = explode(',', $license->image_ids);
            $image = File::whereIn('id', $image_ids)->get();
            $license->image_urls = array();
            $image_urls = array();
            foreach ($image as $key => $val) {
                array_push($image_urls, $val->url);
            }
            $license->image_urls = array_merge($license->image_urls, $image_urls);
            return response()->json(['result' => $license, 'message' => 'thành công'], 200);
        } else {
            return response()->json(['message' => 'không có giấy phép nào được tải lên'], 204);
        }
    }

    /**
     * get shop/license
     */
    public function getLicenseShop()
    {
        UserController::AuthShop();
        $shop_id = User::where('id', Session()->get('shop_id'))->value('shop_id');
        $shop = Shop::find($shop_id);
        if ($shop->parent_id == 0) {
            $user_id = Session()->get('shop_id');
        } else {
            $parentShop = Shop::find($shop->parent_id);
            $user_id = User::where('shop_id', $parentShop->id)->value('id');
        }

        $license = SalesLicense::where('user_id', $user_id)->first();
        if ($license) {
            $image_ids = explode(',', $license->image_ids);
            $image = File::whereIn('id', $image_ids)->get();
            $license->image_urls = array();
            $image_urls = array();
            foreach ($image as $key => $val) {
                array_push($image_urls, $val->url);
            }
            $license->image_urls = array_merge($license->image_urls, $image_urls);
            return response()->json(['result' => $license, 'message' => 'thành công'], 200);
        } else {
            return response()->json(['message' => 'không có giấy phép nào được tải lên'], 204);
        }
    }

    /**
     * get admin-shop/license-page
     */
    public function getLicensePage()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.license.detail');
    }

    /**
     * get shop/license-page
     */
    public function getLicensePageShop()
    {
        UserController::AuthShop();
        $userShop = User::find(Session()->get('shop_id'));
        $shop = Shop::find($userShop->shop_id);
        $isShopWithoutBranch = 0;
        if (is_null($shop->parent_id)) {
            $isShopWithoutBranch = 1;
        }
        return view('shop.license.detail')->with(compact('isShopWithoutBranch'));
    }

    /**
     * get admin/license-shop-page
     */
    public function getLicenseShopPageForAdmin()
    {
        UserController::AuthAdmin();
        return view('admin.license.list_of_shop');
    }

    /**
     * get admin/license-admin-shop-page
     */
    public function getLicenseAdminShopPageForAdmin()
    {
        UserController::AuthAdmin();
        return view('admin.license.list_of_admin_shop');
    }

    /**
     * get admin/license/query/adminShopId
     * @param $adminShopId
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminQueryLicense($adminShopId)
    {
        UserController::AuthAdmin();
        if ($adminShopId != 0) {
            $parent_id = User::where('app_users.id', $adminShopId)->join('app_shops', 'app_shops.id', '=', 'app_users.shop_id')->first()->parent_id;
            if (!is_null($parent_id)) {
                $adminShopId = User::where('shop_id', $parent_id)->select('app_users.id as user_id')->first()->user_id;
            }
            $license = SalesLicense::where('user_id', $adminShopId)->join('app_users', 'app_users.id', '=', 'app_licenses.user_id')
                ->select('app_licenses.user_id as user_id', 'app_licenses.id as id', 'app_licenses.name as name', 'app_licenses.image_ids', 'app_licenses.description', 'app_licenses.admin_status', 'app_shops.name as shop_name', 'app_users.shop_id')
                ->join('app_shops', 'app_shops.id', '=', 'shop_id')->paginate(10);
        } else {
            $license = SalesLicense::join('app_users', 'app_users.id', '=', 'app_licenses.user_id')
                ->select('app_licenses.user_id as user_id', 'app_licenses.id as id', 'app_licenses.name as name', 'app_licenses.image_ids', 'app_licenses.description', 'app_licenses.admin_status', 'app_shops.name as shop_name', 'app_users.shop_id')
                ->join('app_shops', 'app_shops.id', '=', 'shop_id')->paginate(10);
        }
        if ($license) {
            foreach ($license as $item) {
                $image_ids = explode(',', $item['image_ids']);
                $image = File::whereIn('id', $image_ids)->get();
                $temp = array();
                foreach ($image as $key => $val) {
                    array_push($temp, $val->url);
                }
                $item['image_urls'] = $temp;
            }
            return response()->json(['results' => $license, 'message' => 'truy vấn thành công'], 200);
        } else {
            return response()->json(['message' => 'không có dữ liệu'], 204);
        }
    }

    /**
     * put admin/license/approve/4
     */
    public function adminApprove($id)
    {
        UserController::AuthAdmin();
        $license = SalesLicense::find($id);
        if (!$license) {
            return response()->json(['message' => 'invalid this id'], 400);
        } else {
            $license->admin_status = 1;
            $license->save();
            return response()->json(['message' => 'phê duyệt thành công'], 200);
        }
    }

    /**
     * put admin/license/reject/4
     */
    public function adminReject($id)
    {
        UserController::AuthAdmin();
        $license = SalesLicense::find($id);
        if (!$license) {
            return response()->json(['message' => 'invalid this id'], 400);
        } else {
            $license->admin_status = -1;
            $license->save();
            return response()->json(['message' => 'từ chối thành công'], 200);
        }
    }
}
