<?php

namespace App\Http\Controllers;

use App\AppService;
use App\BaseOrder;
use App\Exceptions\CustomException;
use App\Exports\DebtShopExport;
use App\Exports\PaymentExport;
use App\Http\Requests\AppServicePostRequest;
use App\Http\Requests\PaymentServicePostRequest;
use App\Order;
use App\Payment;
use App\PaymentService;
use App\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;

class DebtController extends BaseController
{
    public function debt_page_shop()
    {
        UserController::AuthShop();
        return view('shop.debt.lists');
    }

    public function debt_page_admin_shop()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.debt.lists');
    }

    public function debt_page_admin()
    {
        UserController::AuthAdmin();
        return view('admin.debt.lists');
    }

    /**
     * GET: http://127.0.0.1:8000/shop/debts?option=0&month=&precious=2&year=2021
     */
    public function ExportExcelDebtShop()
    {
        UserController::AuthShop();
        $userId = Session()->get('shop_id');
        list($isChildShop,) = Shop::isChildShop($userId);
        if ($isChildShop) {
            return response()->json(['result' => 'Đây là shop chi nhánh không có công nợ']);
        }
        return $this->GetExportExcel($userId);
    }

    /**
     * GET: http://127.0.0.1:8000/admin-shop/debts?option=0&month=&precious=2&year=2021
     */
    public function ExportExcelDebtAdminShop()
    {
        UserController::AuthAdminShop();
        $userId = Session()->get('admin_shop_id');
        return $this->GetExportExcel($userId, true);
    }

    /**
     * GET: http://127.0.0.1:8000/admin/shop/debts?shopId=&option=0&month=&precious=2&year=2021
     */
    public function ExportDebtOfShop()
    {
        UserController::AuthAdmin();
        $userId = request()->shopId;
        if (!$userId) {
            return parent::responseNoContent();
        }
        return $this->GetExportExcel($userId, true);
    }

    /**
     * GET: http://127.0.0.1:8000/admin/admin-shop/debts?shopId=11&option=0&month=&precious=2&year=2021
     */
    public function ExportDebtOfAdminShop()
    {
        UserController::AuthAdmin();
        $userId = request()->shopId;
        if (!$userId) {
            return parent::responseNoContent();
        }
        return $this->GetExportExcel($userId, true);
    }

    /**
     * GET: http://127.0.0.1:8000/admin/payment-debts?option=0&month=&precious=3&year=2021
     */
    public function ExportExcelPayment()
    {
        UserController::AuthAdmin();
        $option = request()->option;
        $month = request()->month;
        $precious = request()->precious;
        $year = request()->year;

        $payment = new Payment();
        $data = $this->QueryOrderPayment($option, $month, $precious, $year, $payment);

        if (!$data) {
            return response()->json(['result' => 'Không có dữ liệu xuất khẩu'], 204);
        }

        $subTitle = $payment->convertDateTime($option, $month, $precious, $year);
        $information = (object)[
            'SubTitle' => $subTitle
        ];
        return Excel::download(new PaymentExport($data, $information), 'Thanh toán công nợ_' . date('d-m-Y_H-i-s') . '.xlsx');
    }

    /**
     * Hàm xử lý chung khi xuất khẩu
     */
    private function GetExportExcel($userId, $isParentShop = false)
    {
        $option = request()->option;
        $month = request()->month;
        $precious = request()->precious;
        $year = request()->year;

        $order = new Order();
        $key = 'debts_shop_' . $userId;
        $shopName = Shop::getShopName($userId);

        if ($isParentShop) {
            $userId = Shop::getListsChildShop($userId);
        }

        $isExistCache = Cache::has($key);
        if ($isExistCache) {
            $data = Cache::get($key);
        } else {
            $data = $this->QueryOrder($option, $month, $precious, $year, $userId, $order);
        }

        if (!$data) {
            return response()->json(['result' => 'Không có dữ liệu xuất khẩu'], 204);
        }

        $subTitle = $order->convertDateTime($option, $month, $precious, $year);
        $information = (object)[
            'Name' => $shopName,
            'SubTitle' => $subTitle
        ];
        list($hasReceivedMoney, $noReceivedMoney, $summaryHasReceivedMoney,
            $summaryNoReceivedMoney, $summary) = $order->customOrdersExcel($data);

        return Excel::download(new DebtShopExport($hasReceivedMoney, $noReceivedMoney, $summaryHasReceivedMoney,
            $summaryNoReceivedMoney, $summary, $information), 'Công nợ shop_' . date('d-m-Y_H-i-s') . '.xlsx');
    }

    /**
     * POST: http://127.0.0.1/shop/debts
     */
    public function QueryDebtShop(Request $request)
    {
        UserController::AuthShop();
        $userId = parent::getShopId();
        list($isChildShop,) = Shop::isChildShop($userId);
        if ($isChildShop) {
            return response()->json(['result' => 'Đây là shop chi nhánh không có công nợ']);
        }
        return $this->PostQueryOrder($request, $userId);
    }

    /**
     * POST: http://127.0.0.1/admin-shop/debts
     */
    public function QueryDebtAdminShop(Request $request)
    {
        UserController::AuthAdminShop();
        $userId = parent::getAdminShopId();
        return $this->PostQueryOrder($request, $userId, true);
    }

    /**
     * POST: http://127.0.0.1:8000/admin/shop/debts
     */
    public function QueryDebtOfShop(Request $request)
    {
        UserController::AuthAdmin();
        $userId = $request->shopId;
        list($isChildShop,) = Shop::isChildShop($userId);
        if ($isChildShop) {
            return response()->json(['result' => 'Đây là shop chi nhánh không có công nợ']);
        }
        return $this->PostQueryOrder($request, $userId);
    }

    /**
     * POST: http://127.0.0.1/admin/admin-shop/debts
     */
    public function QueryDebtOfAdminShop(Request $request)
    {
        UserController::AuthAdmin();
        $userId = $request->shopId;
        return $this->PostQueryOrder($request, $userId, true);
    }

    /**
     * POST: http://127.0.0.1/admin/payment-debts
     */
    public function QueryPayment(Request $request)
    {
        UserController::AuthAdmin();
        $option = $request->option;
        $month = $request->month;
        $precious = $request->precious;
        $year = $request->year;
        $page = request()->page;

        $data = $this->QueryOrderPayment($option, $month, $precious, $year, new Payment());

        if (!$data) {
            return response()->json(['result' => 'Không có dữ liệu'], 204);
        }

        $result = parent::paginate($data, $page);
        return response()->json(['result' => $result]);
    }

    /**
     * Hàm chung xử lý thực hiện query order
     */
    private function PostQueryOrder(Request $request, $userId, $isParentShop = false)
    {
        $option = $request->option;
        $month = $request->month;
        $precious = $request->precious;
        $year = $request->year;
        $page = request()->page;

        $key = 'debts_shop_' . $userId;
        if ($isParentShop) {
            $userId = Shop::getListsChildShop($userId);
        }

        $isExistCache = Cache::has($key);
        if ($isExistCache) {
            $data = Cache::get($key);
        } else {
            $data = $this->QueryOrder($option, $month, $precious, $year, $userId, new Order());
        }

        if (!$data) {
            return response()->json(['result' => 'Không có dữ liệu'], 204);
        }

        parent::setCache($key, $data, $isExistCache);
        $result = parent::paginate($data, $page);
        return response()->json(['result' => $result]);
    }

    /**
     * Hàm thực hiện query lên order theo shop hay admin-shop
     */
    private function QueryOrder($option, $month, $precious, $year, $userIds, BaseOrder $baseOrder)
    {
        $order_ids = array();
        if (is_array($userIds)) {
            $order = Order::whereIn('seller_id', $userIds)
                ->select('id')->get();
        } else {
            $order = Order::where('seller_id', $userIds)
                ->select('id')->get();
        }

        if (!count($order)) {
            return array();
        }

        foreach ($order as $item) {
            array_push($order_ids, $item->id);
        }

        return $this->baseQueryOrder($option, $month, $precious, $year, $order_ids, $baseOrder);
    }

    private function QueryOrderPayment($option, $month, $precious, $year, $baseOrder)
    {
        $order_ids = array();
        $paymentMethods = [0, 1, 2];
        $order = Order::whereIn('payment_method', $paymentMethods)
            ->select('id')->get();

        if (!count($order)) {
            return array();
        }

        foreach ($order as $item) {
            array_push($order_ids, $item->id);
        }
        return $this->baseQueryOrder($option, $month, $precious, $year, $order_ids, $baseOrder);
    }

    private function baseQueryOrder($option, $month, $precious, $year, $order_ids, BaseOrder $baseOrder)
    {
        $data['month'] = $month;
        $data['precious'] = $precious;
        $data['year'] = $year;
        if (!is_null($option) && !is_null($year)) {
            if ($month) {
                if ($option == 0) {
                    return $baseOrder->getOrdersOfMonth($data, $order_ids, null);
                }

                if ($option == 1) {
                    return $baseOrder->getOrdersFirstMonth($data, $order_ids, null);
                }

                if ($option == 2) {
                    return $baseOrder->getOrdersLastMonth($data, $order_ids, null);
                }
            } else if ($precious) {
                if ($option == 0) {
                    return $baseOrder->getOrdersOfPrecious($data, $order_ids, null);
                }

                if ($option == 1) {
                    return $baseOrder->getOrdersFirstPrecious($data, $order_ids, null);
                }

                if ($option == 2) {
                    return $baseOrder->getOrdersLastPrecious($data, $order_ids, null);
                }
            } else {
                if ($option == 0) {
                    return $baseOrder->getOrdersOfYear($data, $order_ids, null);
                }

                if ($option == 1) {
                    return $baseOrder->getOrdersFirstYear($data, $order_ids, null);
                }

                if ($option == 2) {
                    return $baseOrder->getOrdersLastYear($data, $order_ids, null);
                }
            }
        } else {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }
    }

    /** Fee Services */
    /**
     * GET: http://127.0.0.1/bisto_web/admin/fee-app-page
     */
    public function getFeeAppPage()
    {
        UserController::AuthAdmin();
        return view('admin.service.list_app_service');
    }
    /** Fee Services */
    /**
     * GET: http://127.0.0.1/bisto_web/admin/fee-services?page=1
     */
    public function GetAppServices()
    {
        UserController::AuthAdmin();
        $appServices = AppService::first();
        return response()->json(['result' => $appServices]);
    }

    /**
     * GET: http://127.0.0.1/bisto_web/admin/fee-services/1
     */
    public function GetOneAppServices($id)
    {
        UserController::AuthAdmin();
        $appService = AppService::where('id', $id)->first();
        return response()->json(['result' => $appService]);
    }

    /**
     * POST: http://127.0.0.1/bisto_web/admin/fee-services
     */
    public function InsertAppServices(AppServicePostRequest $request)
    {
        try {
            UserController::AuthAdmin();
            $service_amount = $request->amount;
            $type = $request->type;
            AppService::insertAppServices($service_amount, $type);
            return response()->json(['message' => 'Thêm phí thành công'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * PUT: http://127.0.0.1/bisto_web/admin/fee-services/1
     */
    public function UpdateAppServices(Request $request, $id)
    {
        try {
            UserController::AuthAdmin();
            $service_amount = $request->amount;
            $type = $request->type;
            AppService::updateAppServices($service_amount, $type);
            return response()->json(['message' => 'Cập nhật phí thành công']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * DELETE: http://127.0.0.1/bisto_web/admin/fee-services/1
     */
    public function DeleteAppServices($id)
    {
        try {
            UserController::AuthAdmin();
            $appService = AppService::where('id', $id)->first();
            if (is_null($appService)) throw new CustomException('Không tìm thấy bản ghi phí', 400);
            $appService->delete();
            return response()->json(['message' => 'Xóa phí thành công']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /** Payment Method */
    /**
     * GET: http://127.0.0.1/bisto_web/admin/fee-payment-page
     */
    public function getFeePaymentPage()
    {
        UserController::AuthAdmin();
        return view('admin.service.list_payment_service');
    }

    /** Payment Method */
    /**
     * GET: http://127.0.0.1/bisto_web/admin/payment-services?page=1&keyword=
     */
    public function GetPaymentServices()
    {
        UserController::AuthAdmin();
        $keyword = request()->keyword;
        $paymentServices = PaymentService::where('unit_payment', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')->paginate(10);
        return response()->json(['results' => $paymentServices->appends(Input::except('page'))]);
    }

    /**
     * GET: http://127.0.0.1/bisto_web/admin/payment-services/3
     */
    public function GetOnePaymentServices($id)
    {
        UserController::AuthAdmin();
        $paymentMethod = PaymentService::where('id', $id)->first();
        return response()->json(['result' => $paymentMethod]);
    }

    /**
     * POST: http://127.0.0.1/bisto_web/admin/payment-services
     */
    public function InsertPaymentServices(PaymentServicePostRequest $request)
    {
        try {
            UserController::AuthAdmin();
            $unit_payment = $request->unit_payment;
            $plus_percent = $request->plus_percent;
            $plus_amount = $request->plus_amount;
            $isExistPayment = PaymentService::where('unit_payment', $unit_payment)->first();
            if (!is_null($isExistPayment)) throw new CustomException('Tên đơn vị thanh toán đã tồn tại', 400);
            $result = PaymentService::insertPaymentServices($unit_payment, $plus_percent, $plus_amount);
            if ($result) {
                return response()->json(['message' => 'Thêm dịch vụ thanh toán thành công'], 201);
            }
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * PUT: http://127.0.0.1/bisto_web/admin/payment-services/3
     */
    public function UpdatePaymentServices(Request $request, $id)
    {
        try {
            UserController::AuthAdmin();
            $unit_payment = $request->unit_payment;
            $plus_percent = $request->plus_percent;
            $plus_amount = $request->plus_amount;
            $result = PaymentService::updatePaymentServices($id, $unit_payment, $plus_percent, $plus_amount);
            if ($result) {
                return response()->json(['message' => 'Cập nhật dịch vụ thanh toán thành công']);
            }
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * DELETE: http://127.0.0.1/bisto_web/admin/payment-services/3
     */
    public function DeletePaymentServices($id)
    {
        try {
            UserController::AuthAdmin();
            $paymentMethod = PaymentService::where('id', $id)->first();
            if (is_null($paymentMethod)) throw new CustomException('Không tìm thấy bản ghi phí thanh toán', 400);
            $paymentMethod->delete();
            return response()->json(['message' => 'Xóa dịch vụ thanh toán thành công']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}
