<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use App\Exceptions\CustomException;
use App\File;
use App\RelTag;
use App\RelUserPostComment;
use App\RelUserPostCommentLike;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommentPostController extends Controller
{

    /**
     * GET: http://localhost/bisto_web/shop/posts
     */
    public function ShopGetCommentsByPostId(): \Illuminate\Http\JsonResponse
    {
        UserController::AuthShop();
        $id = Session()->get('shop_id');
        $postId = \request()->post_id;
        $comments = RelUserPostComment::orderBy('rel_user_post_comment.created_at', 'DESC')
            ->where('post_id', $postId)
            ->select('id', 'user_id', 'image_ids', 'content', 'created_at', 'updated_at')
            ->paginate(10);

        foreach ($comments as $comment) {
            $comment['user'] = User::where('app_users.id', $comment->user_id)
                ->join('app_files', 'app_files.id', '=', 'app_users.avatar_id')
                ->select('app_users.fullname', 'app_files.url as avatar_url', 'app_users.id')
                ->first();
            if (!is_null($comment->image_ids)) {
                $imgIds = explode(",", $comment->image_ids);
                if (is_array($imgIds)) {
                    $images = File::whereIn('id', $imgIds)
                        ->select('id', 'url')
                        ->get();
                    $comment['images'] = $images;
                }
            }

        }

        return response()->json(['success' => 1, 'results' => $comments]);
    }


}
