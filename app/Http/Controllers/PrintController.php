<?php

namespace App\Http\Controllers;

use App\User;
use PDF;

class PrintController extends Controller
{
    public function ShowListsExportPdf()
    {
        UserController::AuthAdmin();
        $data = User::getShops();
        $title = '';
        return view('admin.export.shops', compact('data', 'title'));
    }

    /**
     * GET: http://localhost/bisto_web/admin/lists/admin-shops/export-pdf
     */
    public function ExportPdfOwnersAdminShop()
    {
        UserController::AuthAdmin();
        $data = User::getAdminShops();
        $title = 'Danh sách các admin-shop';
        $pdf = PDF::loadView('admin.export.lists', compact('data', 'title'));
        return $pdf->download('admin-shops.pdf');
    }

    /**
     * GET: http://localhost/bisto_web/admin/lists/shops/export-pdf
     */
    public function ExportPdfListsShop()
    {
        UserController::AuthAdmin();
        $is_without_branch = request()->is_without_branch;
        $data = User::getShopsWithOption($is_without_branch);
        $title = 'Danh sách các shop trong chuỗi';
        $pdf = PDF::loadView('admin.export.lists', compact('data', 'title'));

        if ($is_without_branch == 1) {
            $title = 'Danh sách các shop lẻ';
            $pdf = PDF::loadView('admin.export.lists_without_branch', compact('data', 'title'));
        }
        return $pdf->download('shops.pdf');
    }

    /**
     * GET: http://localhost/bisto_web/admin/users/export-pdf
     */
    public function ExportPdfUsers()
    {
        UserController::AuthAdmin();
        $data = User::getUsers();
        $title = 'Danh sách khách hàng';
        $pdf = PDF::loadView('admin.export.lists', compact('data', 'title'));
        return $pdf->download('users.pdf');
    }

}
