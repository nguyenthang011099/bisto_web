<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Post;
use App\Exceptions\CustomException;
use App\File;
use App\RelTag;
use App\RelUserPostComment;
use App\RelUserPostCommentLike;
use App\Shop;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    public function ShopListsPost()
    {
        UserController::AuthShop();
        return view('shop.post.lists');
    }

    public function AdminListsPost()
    {
        UserController::AuthAdmin();
        return view('admin.post.lists');
    }

    /**
     * GET: http://localhost/bisto_web/shop/posts
     */
    public function ShopGetPosts(): \Illuminate\Http\JsonResponse
    {
        UserController::AuthShop();
        $id = Session()->get('shop_id');

        $keyword = request()->keyword;
        $posts = Post::orderBy('app_posts.created_at', 'DESC')
            ->where('user_id', $id)
            ->where('title', 'like', '%' . $keyword . '%')
            ->select('id', 'title', 'content', 'description', 'admin_status', 'self_status', 'created_at', 'updated_at')
            ->paginate(10);

        foreach ($posts as $post) {
            $post['count_like'] = RelUserPostCommentLike::where('post_id', $post->id)->whereNotNull('user_id')->count();
            $post['count_comment'] = RelUserPostComment::where('post_id', $post->id)->whereNotNull('user_id')->count();
        }

        return response()->json(['success' => 1, 'results' => $posts]);
    }

    /**
     * GET: http://localhost/bisto_web/shop/posts/2
     */
    public function ShopGetPostById(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        UserController::AuthShop();
        $user_id = Session()->get('shop_id');

        $post = Post::where('id', $id)
            ->where('user_id', $user_id)
            ->select('id', 'title', 'content', 'description', 'admin_status', 'self_status', 'created_at', 'updated_at')
            ->first();

        $post['count_like'] = RelUserPostCommentLike::where('post_id', $post->id)->whereNotNull('user_id')->count();
        $post['count_comment'] = RelUserPostComment::where('post_id', $post->id)->whereNotNull('user_id')->count();
        return response()->json(['result' => $post]);
    }

    /**
     * POST: http://localhost/bisto_web/shop/posts
     * @throws CustomException
     */
    public function ShopPostPost(Request $request)
    {
        try {
            UserController::AuthShop();
            $data = $request->all();
            $user_id = Session()->get('shop_id');
            $post = new Post();
            $post->title = $data['title'];
            $post->content = $data['content'];
            $post->description = $data['description'];
            $post->admin_status = $data['admin_status'];
            $post->self_status = $data['self_status'];
            $post->user_id = $user_id;

            $result = $post->save();
            return response()->json(['message' => 'Thêm bài post thành công'], 201);
        } catch (\Exception $e) {
            throw CustomException::makeServerError($e->getMessage());
        }
    }

    /**
     * PUT: http://localhost/bisto_web/shop/posts/2
     */
    public function ShopPutPost(Request $request, $id)
    {
        UserController::AuthShop();
        $user_id = Session()->get('shop_id');

        $post = Post::where('id', $id)->first();
        if (is_null($post)) {
            return response()->json([], 204);
        }
        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->description = $request['description'];
        $post->self_status = $request['self_status'];
        $result = $post->save();
        if ($result) {
            return response()->json(['message' => 'Cập nhật bài post thành công']);
        }
        return response()->json(['message', 'Dữ liệu không hợp lệ'], 400);
    }

    /**
     * DELETE: http://localhost/bisto_web/shop/posts/2
     */
    public function ShopDeletePost($id)
    {
        UserController::AuthShop();
        $post = Post::where('id', $id)->first();
        if (is_null($post)) {
            return response()->json([], 204);
        }

        $post->delete();
        return response()->json(['message' => 'Xóa bài post thành công']);
    }

    /**
     * GET: http://localhost/bisto_web/admin/posts
     */
    public function AdminGetPosts(): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdmin();
        $keyword = request()->keyword;
        $posts = Post::orderBy('app_posts.created_at', 'DESC')
            ->where('title', 'like', '%' . $keyword . '%')
            ->join('app_users', 'app_users.id', '=', 'app_posts.user_id')
            ->select('app_posts.id', 'app_posts.title', 'app_posts.user_id', 'app_posts.content', 'app_posts.description',
                'app_posts.admin_status', 'app_posts.self_status', 'app_posts.created_at', 'app_posts.updated_at',
                'app_users.shop_id')
            ->paginate(10);

        foreach ($posts as $post) {
            $post['shop'] = Shop::where('id', $post->shop_id)
                ->select('name', 'id')
                ->first();
            $post['count_like'] = RelUserPostCommentLike::where('post_id', $post->id)->whereNotNull('user_id')->count();
            $post['count_comment'] = RelUserPostComment::where('post_id', $post->id)->whereNotNull('user_id')->count();
        }

        return response()->json(['success' => 1, 'results' => $posts]);
    }

    /**
     * GET: http://localhost/bisto_web/admin/posts/2
     */
    public function AdminGetPostById(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdmin();

        $post = Post::where('app_posts.id', $id)
            ->join('app_users', 'app_users.id', '=', 'app_posts.user_id')
            ->select('app_posts.id', 'app_posts.title', 'app_posts.user_id', 'app_posts.content', 'app_posts.description',
                'app_posts.admin_status', 'app_posts.self_status', 'app_posts.created_at', 'app_posts.updated_at',
                'app_users.shop_id')
            ->first();

        $post['shop'] = Shop::where('id', $post->shop_id)
            ->select('name', 'id')
            ->first();
        $post['count_like'] = RelUserPostCommentLike::where('post_id', $post->id)->whereNotNull('user_id')->count();
        $post['count_comment'] = RelUserPostComment::where('post_id', $post->id)->whereNotNull('user_id')->count();
        return response()->json(['result' => $post]);
    }

    public function AdminChangePostAdminStatus(Request $request, $id)
    {
        UserController::AuthAdmin();
        $admin_status = $request->admin_status;
        $affected = Post::where('id', $id)
            ->update(['admin_status' => $admin_status]);
        if ($affected != 0)
            return response()->json(['message' => "Thay đổi trạng thái thành công"]);
        else throw CustomException::makeBadRequest("Thay đổi thất bại");
    }
}
