<?php

namespace App\Http\Controllers;

use App\BaseOrder;
use App\Exports\DeliveryUnitExport;
use App\Order;
use App\ShippingOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class DeliveryController extends BaseController
{
    public function getShippingOrder()
    {
        UserController::AuthAdmin();
        return view('admin.delivery.lists_shipping_order');
    }

    /**
     * POST: http://127.0.0.1:8000/admin/shipping-order
     */
    public function QueryShippingOrder(Request $request)
    {
        UserController::AuthAdmin();
        $data = $request->all();

        if (is_null($data['month']) && is_null($data['year'])) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $firstDayOfMonth = date($data['year'] . '-' . $data['month'] . '-01');
        $middleDayOfMonth = date($data['year'] . '-' . $data['month'] . '-16');
        $lastDayOfMonth = date($data['year'] . '-' . $data['month'] . '-t');

        if (is_null($data['provider']) && is_null($data['option'])) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $option = $data['option'];
        $provider = $data['provider'];

        if ($data['month'] == 0) {
            $firstMonth = 1;
            $lastMonth = 12;
            $option = 0;

            $firstDayOfMonth = date($data['year'] . '-' . $firstMonth . '-01');
            $lastDayOfMonth = date($data['year'] . '-' . $lastMonth . '-t');
        }

        switch ($option) {
            case 0:
                return $this->getShippingOrderOfMonth($provider, $firstDayOfMonth, $lastDayOfMonth);
            case 1:
                return $this->getShippingOrderOfMonth($provider, $firstDayOfMonth, $middleDayOfMonth);
            case 2:
                return $this->getShippingOrderOfMonth($provider, $middleDayOfMonth, $lastDayOfMonth);
            default:
                return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }
    }

    /**
     * Lấy các đơn ship theo nửa đầu, nửa cuối hoặc cả tháng
     */
    private function getShippingOrderOfMonth($provider, $date1, $date2)
    {
        if ($provider == 0) {
            $shippingOrders = ShippingOrder::whereBetween('created_at', [$date1, $date2])
                ->select('created_at', 'shipping_fee', 'code', 'delivery_unit_id')
                ->orderBy('created_at', 'DESC')
                ->paginate(10);
        } else {
            $shippingOrders = ShippingOrder::where('delivery_unit_id', $provider)
                ->whereBetween('created_at', [$date1, $date2])
                ->select('created_at', 'shipping_fee', 'code', 'delivery_unit_id')
                ->orderBy('created_at', 'DESC')
                ->paginate(10);
        }

        if (is_null($shippingOrders)) {
            return response()->json([], 204);
        }

        foreach ($shippingOrders as $item) {
            $item->provider_name = Order::showProviderName($item->delivery_unit_id);
        }
        return response()->json(['results' => $shippingOrders]);
    }

    /**
     * GET: http://127.0.0.1:8000/admin/delivery-unit?unit=1&option=0&month=&precious=3&year=2021
     */
    public function ExportExcelDelivery()
    {
        UserController::AuthAdmin();
        $unit = request()->unit;
        $option = request()->option;
        $month = request()->month;
        $precious = request()->precious;
        $year = request()->year;
        $shippingOrder = new ShippingOrder();

        $key = 'delivery_unit_' . $unit;
        $isExistCache = Cache::has($key);
        if ($isExistCache) {
            $data = Cache::get($key);
        } else {
            $data = $this->QueryOrder($unit, $option, $month, $precious, $year, $shippingOrder);
        }

        if (!$data) {
            return response()->json(['result' => 'Không có dữ liệu xuất khẩu'], 204);
        }

        $unitName = BaseOrder::showProviderName($unit);
        $subTitle = $shippingOrder->convertDateTime($option, $month, $precious, $year);
        $information = (object)[
            'UnitName' => $unitName,
            'SubTitle' => $subTitle
        ];
        return Excel::download(new DeliveryUnitExport($data, $information),
            'Vận chuyển ' . $unitName . '_' . date('d-m-Y_H-i-s') . '.xlsx');
    }

    /**
     * POST: http://127.0.0.1:8000/admin/delivery-unit
     */
    public function DeliveryUnit(Request $request)
    {
        UserController::AuthAdmin();
        $unit = $request->unit;
        $option = $request->option;
        $month = $request->month;
        $precious = $request->precious;
        $year = $request->year;
        $page = request()->page;

        $key = 'delivery_unit_' . $unit;
        $isExistCache = Cache::has($key);
        if ($isExistCache) {
            $data = Cache::get($key);
        } else {
            $data = $this->QueryOrder($unit, $option, $month, $precious, $year, new ShippingOrder());
        }

        if (!$data) {
            return response()->json(['result' => 'Không có dữ liệu'], 204);
        }

        parent::setCache($key, $data, $isExistCache);
        $result = parent::paginate($data, $page);
        return response()->json(['result' => $result]);
    }

    private function QueryOrder($unit, $option, $month, $precious, $year, BaseOrder $shippingOrder)
    {
        $order_ids = array();
        $order = DB::table('app_orders')
            ->join('rel_order_shippings', 'rel_order_shippings.order_id', '=', 'app_orders.id')
            ->where('rel_order_shippings.delivery_unit_id', $unit)
            ->select('app_orders.id')
            ->get();

        if (!count($order)) {
            return array();
        }

        foreach ($order as $item) {
            array_push($order_ids, $item->id);
        }

        $data['month'] = $month;
        $data['precious'] = $precious;
        $data['year'] = $year;

        if (!is_null($option) && !is_null($year)) {
            if ($month) {
                if ($option == 0) {
                    return $shippingOrder->getOrdersOfMonth($data, $order_ids, null);
                }

                if ($option == 1) {
                    return $shippingOrder->getOrdersFirstMonth($data, $order_ids, null);
                }

                if ($option == 2) {
                    return $shippingOrder->getOrdersLastMonth($data, $order_ids, null);
                }
            } else if ($precious) {
                if ($option == 0) {
                    return $shippingOrder->getOrdersOfPrecious($data, $order_ids, null);
                }

                if ($option == 1) {
                    return $shippingOrder->getOrdersFirstPrecious($data, $order_ids, null);
                }

                if ($option == 2) {
                    return $shippingOrder->getOrdersLastPrecious($data, $order_ids, null);
                }
            } else {
                if ($option == 0) {
                    return $shippingOrder->getOrdersOfYear($data, $order_ids, null);
                }

                if ($option == 1) {
                    return $shippingOrder->getOrdersFirstYear($data, $order_ids, null);
                }

                if ($option == 2) {
                    return $shippingOrder->getOrdersLastYear($data, $order_ids, null);
                }
            }
        } else {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }
    }
}
