<?php

namespace App\Http\Controllers;

use App\Bcoin;
use App\Exceptions\CustomException;
use App\ServiceBanner;
use DateInterval;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use App\Banner;
use App\File;
use App\User;
use Illuminate\Support\Facades\DB;

class BannerController extends Controller
{
    /** Admin */
    public function admin_manage_banner()
    {
        UserController::AuthAdmin();
        return view('admin.banner.lists');
    }

    public function admin_manage_banner_admin_shop()
    {
        UserController::AuthAdmin();
        return view('admin.banner.lists_of_admin_shop');
    }

    public function admin_manage_banner_shop()
    {
        UserController::AuthAdmin();
        return view('admin.banner.lists_of_shop');
    }

    public function admin_service_banner()
    {
        UserController::AuthAdmin();
        return view('admin.banner.lists_service_banner');
    }

    /**
     * GET: http://localhost/bisto_web/admin/banners
     */
    public function GetBannersAdmin()
    {
        UserController::AuthAdmin();
        $user = Session()->get('admin_id');

        $keywords = request()->keyword;
        $banners = Banner::orderBy('created_at', 'DESC')->where('user_id', $user)
            ->where('name', 'like', '%' . $keywords . '%')->paginate(10);
        return $this->get_banners($banners);
    }

    /**
     * GET: http://localhost/bisto_web/admin/banners/2
     */
    public function GetAdminBannerById($id)
    {
        UserController::AuthAdmin();
        $user = Session()->get('admin_id');
        return $this->get_detail_banner($user, $id);
    }

    /**
     * PUT: http://localhost/bisto_web/admin/banners/un-active/2
     */
    public function UnActiveBannerAdmin($id)
    {
        UserController::AuthAdmin();
        Banner::where('id', $id)->update(['self_status' => -1]);
        return response()->json(['message' => 'Ẩn banner thành công']);
    }

    /**
     * PUT: http://localhost/bisto_web/admin/banners/active/2
     */
    public function ActiveBannerAdmin($id)
    {
        UserController::AuthAdmin();
        Banner::where('id', $id)->update(['self_status' => 1]);
        return response()->json(['message' => 'Kích hoạt banner thành công']);
    }

    /**
     * PUT: http://localhost/bisto_web/admin/banners/reject/2
     */
    public function RejectBannerAdmin($id)
    {
        UserController::AuthAdmin();
        Banner::where('id', $id)->update(['admin_status' => -1]);
        return response()->json(['message' => 'Từ chối đăng kí banner thành công']);
    }

    /**
     * PUT: http://localhost/bisto_web/admin/banners/accept/2
     */
    public function AcceptBannerAdmin($id)
    {
        UserController::AuthAdmin();
        Banner::where('id', $id)->update(['admin_status' => 1]);
        return response()->json(['message' => 'Chấp nhận đăng kí banner thành công']);
    }

    /**
     * POST: http://localhost/bisto_web/admin/banners
     */
    public function InsertBannerAdmin(Request $request)
    {
        UserController::AuthAdmin();
        $user = Session()->get('admin_id');
        $admin_status = $request->admin_status;
        return $this->insert_banner($request, $user, $admin_status);
    }

    /**
     * PUT: http://localhost/bisto_web/admin/banners/2
     */
    public function UpdateBannerAdmin(Request $request, $id)
    {
        UserController::AuthAdmin();
        $user = Session()->get('admin_id');
        $admin_status = $request->admin_status;
        return $this->update_banner($request, $id, $user, $admin_status);
    }

    /**
     * DELETE: http://localhost/bisto_web/admin/banners/2
     */
    public function DeleteBannerAdmin($id)
    {
        UserController::AuthAdmin();
        $user = Session()->get('admin_id');
        return $this->delete_banner($id, $user);
    }

    /**
     * GET: http://localhost/bisto_web/admin/shop/banners
     */
    public function AdminGetBannersShop()
    {
        $keywords = request()->keyword;
        $banners = User::where('role', 2)
            ->join('app_banners', 'app_banners.user_id', '=', 'app_users.id')
            ->join('app_shops', 'app_shops.id', '=', 'app_users.shop_id')
            ->where('app_banners.name', 'like', '%' . $keywords . '%')
            ->select('app_users.id as user_id', 'app_banners.id as banner_id',
                'app_banners.name as banner_name', 'position', 'self_status',
                'admin_status', 'app_banners.start_at as start_at', 'app_banners.created_at',
                'app_banners.end_at as end_at', 'app_shops.name as shop_name', 'image_ids')
            ->orderBy('created_at', 'DESC')
            ->paginate(10);
        return $this->get_banners($banners);
    }

    /**
     * GET: http://localhost/bisto_web/admin/admin-shop/banners
     */
    public function AdminGetBannersAdminShop()
    {
        $keywords = request()->keyword;
        $banners = User::where('role', 3)
            ->join('app_banners', 'app_banners.user_id', '=', 'app_users.id')
            ->join('app_shops', 'app_shops.id', '=', 'app_users.shop_id')
            ->where('app_banners.name', 'like', '%' . $keywords . '%')
            ->select('app_users.id as user_id', 'app_banners.id as banner_id',
                'app_banners.name as banner_name', 'position', 'self_status',
                'admin_status', 'app_banners.start_at as start_at', 'app_banners.created_at',
                'app_banners.end_at as end_at', 'app_shops.name as shop_name', 'image_ids')
            ->orderBy('created_at', 'DESC')
            ->paginate(10);
        return $this->get_banners($banners);
    }

    /** Shop */
    public function shop_manage_banner()
    {
        UserController::AuthShop();
        return view('shop.banner.lists');
    }

    /**
     * GET: http://localhost/bisto_web/shop/banners
     */
    public function GetBannersShop()
    {
        UserController::AuthShop();
        $user = Session()->get('shop_id');

        $keywords = request()->keyword;
        $banners = Banner::orderBy('created_at', 'DESC')->where('user_id', $user)
            ->where('name', 'like', '%' . $keywords . '%')->paginate(10);
        return $this->get_banners($banners);
    }

    /**
     * GET: http://localhost/bisto_web/shop/banners/2
     */
    public function GetShopBannerById($id)
    {
        UserController::AuthShop();
        $user = Session()->get('shop_id');
        return $this->get_detail_banner($user, $id);
    }

    /**
     * PUT: http://localhost/bisto_web/shop/banners/un-active/2
     */
    public function UnActiveBannerShop($id)
    {
        UserController::AuthShop();
        Banner::where('user_id', Session()->get('shop_id'))
            ->where('id', $id)->update(['self_status' => -1]);
        return response()->json(['message' => 'Ẩn banner thành công']);
    }

    /**
     * PUT: http://localhost/bisto_web/shop/banners/active/2
     */
    public function ActiveBannerShop($id)
    {
        UserController::AuthShop();
        Banner::where('user_id', Session()->get('shop_id'))
            ->where('id', $id)->update(['self_status' => 1]);
        return response()->json(['message' => 'Kích hoạt banner thành công']);
    }

    /**
     * POST: http://localhost/bisto_web/shop/banners
     */
    public function InsertBannerShop(Request $request)
    {
        UserController::AuthShop();
        $user = Session()->get('shop_id');
        $admin_status = 0;
        return $this->insert_banner($request, $user, $admin_status);
    }

    /**
     * PUT: http://localhost/bisto_web/shop/banners/2
     */
    public function UpdateBannerShop(Request $request, $id)
    {
        UserController::AuthShop();
        $user = Session()->get('shop_id');
        $admin_status = Banner::where('id', $id)->where('user_id', $user)->value('admin_status');
        return $this->update_banner($request, $id, $user, $admin_status);
    }

    /**
     * DELETE: http://localhost/bisto_web/shop/banners/2
     */
    public function DeleteBannerShop($id)
    {
        UserController::AuthShop();
        $user = Session()->get('shop_id');
        return $this->delete_banner($id, $user);
    }

    /** Admin-Shop */
    public function admin_shop_manage_banner()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.banner.lists');
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/banners
     */
    public function GetBannersAdminShop()
    {
        UserController::AuthAdminShop();
        $user = Session()->get('admin_shop_id');

        $keywords = request()->keyword;
        $banners = Banner::orderBy('created_at', 'DESC')->where('user_id', $user)
            ->where('name', 'like', '%' . $keywords . '%')->paginate(10);
        return $this->get_banners($banners);
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/banners/2
     */
    public function GetAdminShopBannerById($id)
    {
        UserController::AuthAdminShop();
        $user = Session()->get('admin_shop_id');
        return $this->get_detail_banner($user, $id);
    }

    /**
     * PUT: http://localhost/bisto_web/admin-shop/banners/un-active/2
     */
    public function UnActiveBannerAdminShop($id)
    {
        UserController::AuthAdminShop();
        Banner::where('user_id', Session()->get('admin_shop_id'))
            ->where('id', $id)->update(['self_status' => -1]);
        return response()->json(['message' => 'Ẩn banner thành công']);
    }

    /**
     * PUT: http://localhost/bisto_web/admin-shop/banners/active/2
     */
    public function ActiveBannerAdminShop($id)
    {
        UserController::AuthAdminShop();
        Banner::where('user_id', Session()->get('admin_shop_id'))
            ->where('id', $id)->update(['self_status' => 1]);
        return response()->json(['message' => 'Kích hoạt banner thành công']);
    }

    /**
     * POST: http://localhost/bisto_web/admin-shop/banners
     */
    public function InsertBannerAdminShop(Request $request)
    {
        UserController::AuthAdminShop();
        $user = Session()->get('admin_shop_id');
        $admin_status = 0;
        return $this->insert_banner($request, $user, $admin_status);
    }

    /**
     * PUT: http://localhost/bisto_web/admin-shop/banners/2
     */
    public function UpdateBannerAdminShop(Request $request, $id)
    {
        UserController::AuthAdminShop();
        $user = Session()->get('admin_shop_id');
        $admin_status = Banner::where('id', $id)->where('user_id', $user)->value('admin_status');
        return $this->update_banner($request, $id, $user, $admin_status);
    }

    /**
     * DELETE: http://localhost/bisto_web/admin-shop/banners/2
     */
    public function DeleteBannerAdminShop($id)
    {
        UserController::AuthAdminShop();
        $user = Session()->get('admin_shop_id');
        return $this->delete_banner($id, $user);
    }

    /** Hàm dùng chung */
    private function get_banners($banners)
    {
        if (is_null($banners)) {
            return response()->json([], 204);
        }

        foreach ($banners as $item) {
            $imgIds = explode(",", $item->image_ids);
            $source_url = array();
            if ($imgIds) {
                foreach ($imgIds as $image_ids) {
                    $source = File::where('id', $image_ids)->value('url');
                    array_push($source_url, $source);
                }
            }
            $item->source_urls = $source_url;
        }

        $serviceBanners = ServiceBanner::get();
        return response()->json(['banners' => $banners, 'service_banners' => $serviceBanners]);
    }

    private function get_detail_banner($user, $id)
    {
        $data = Banner::where('id', $id)->where('user_id', $user)->first();
        if (is_null($data)) {
            return response()->json([], 204);
        }

        $imgIds = explode(",", $data->image_ids);

        $source_url = array();
        if ($imgIds) {
            foreach ($imgIds as $image_ids) {
                $source = File::where('id', $image_ids)->value('url');
                array_push($source_url, $source);
            }
        }
        $data->source_urls = $source_url;
        return response()->json(['banner' => $data]);
    }

    /**
     * @throws CustomException
     */
    private function insert_banner(Request $request, $user, $admin_status)
    {
        try {
            DB::transaction(function () use ($request, $user, $admin_status) {
                $data = array();
                $isExist = Banner::where('name', '=', $request->name)->first();
                if ($isExist) {
                    return response()->json(['message' => 'Tên banner bị trùng'], 400);
                }
                $serviceBannerId = $request->service_banner_id;
                $serviceBanner = ServiceBanner::where('id', $serviceBannerId)->first();

                $info = UserController::getUserInfo();
                if ($info['role'] != 'admin') {
                    $bcoin = Bcoin::where('user_id', $user)->first();
                    $price = $serviceBanner->price;
                    if ($price > $bcoin->amount) {
                        throw CustomException::makeBadRequest("Số bcoin của bạn hiện tại là {$bcoin->amount} không đủ, vui lòng nạp thêm");
                    } else {
                        $bcoin->amount = $bcoin->amount - $price;
                        $bcoin->save();
                    }
                    $data['start_at'] = $request->start_at;
                    $start_at = new DateTime($data['start_at']);
                    $interval = new DateInterval('P' . $serviceBanner->day . 'D');
                    $data['end_at'] = $start_at->add($interval);
                } else {
                    $data['start_at'] = $request->start_at;
                    $data['end_at'] = $request->end_at;
                }
                $source_urls = $request->source_urls;
                if ($source_urls != null) {
                    $imageIds = '';
                    foreach ($source_urls as $source) {
                        $imageId = File::saveFiles($source['url'], $source['filename'], $source['size'], $source['type']);
                        $imageIds .= $imageId . ',';
                    }
                    $data['image_ids'] = substr($imageIds, 0, -1);
                    $data['name'] = $request->name;
                    $data['position'] = $request->position;
                    $data['user_id'] = $user;
                    $data['self_status'] = 1;
                    $data['admin_status'] = $admin_status;
                    $result = Banner::insert($data);
                } else throw CustomException::makeBadRequest('Hình ảnh là bắt buộc');
            });
            return response()->json(['message' => 'Đăng ký banner thành công, banner đang đợi admin duyệt'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    private function update_banner(Request $request, $id, $user, $admin_status)
    {
        $banner = Banner::where('id', $id)->where('user_id', $user)->first();
        $info = UserController::getUserInfo();
        if (is_null($banner)) {
            return response()->json([], 204);
        }

        $isExist = Banner::where('name', '=', $request->name)->where('id', '<>', $id)->first();
        if ($isExist) {
            return response()->json(['message' => 'Tên banner bị trùng'], 400);
        }

        $imageIds = $banner->image_ids;

        $deleted_imageIds = $request->deleted_imageids;
        if ($deleted_imageIds != null) {
            $imageIds = explode(',', $imageIds);
            foreach ($deleted_imageIds as $id) {
                if (($key = array_search(strval($id), $imageIds)) !== false) {
                    unset($imageIds[$key]);
                }
                File::where('id', $id)->delete();
            }
            $imageIds = implode(",", $imageIds);
        }

        $source_urls = $request->source_urls;

        if ($source_urls != null) {
            if (!empty($imageIds)) {
                $imageIds .= ',';
            }

            foreach ($source_urls as $source) {
                $imageId = File::saveFiles($source['url'], $source['filename'], $source['size'], $source['type']);
                $imageIds .= $imageId . ',';
            }
            $imageIds = substr($imageIds, 0, -1);
        }

        $banner->image_ids = $imageIds;
        $banner->name = $request->name;
        $banner->position = $request->position;
        if ($info['role'] == 'admin') {
            $banner->start_at = $request->start_at;
            $banner->end_at = $request->end_at;
        }
        $banner->self_status = $request->self_status;
        $banner->admin_status = $admin_status;

        $result = $banner->save();
        if ($result) {
            return response()->json(['message' => 'Sửa banner thành công']);
        }
        return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
    }

    private
    function delete_banner($id, $user)
    {
        $banner = Banner::where('user_id', $user)->where('id', $id)->first();
        if (is_null($banner)) {
            return response()->json([], 204);
        }

        $imgIds = explode(",", $banner->image_ids);
        if ($imgIds) {
            foreach ($imgIds as $image_ids) {
                File::where('id', $image_ids)->delete();
            }
        }
        $banner->delete();
        return response()->json(['message' => 'Xóa banner thành công']);
    }
}
