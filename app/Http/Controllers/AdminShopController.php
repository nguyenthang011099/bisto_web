<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Notification;
use App\ShippingInfo;
use App\File;
use App\Stylish;
use App\User;
use App\City;
use App\District;
use App\UserNotification;
use App\UserTransaction;
use App\Ward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Order;
use App\Shop;
use App\SizeInformation;
use App\Bcoin;

class AdminShopController extends Controller
{
    public function dashboard()
    {
        UserController::AuthAdminShop();
        $list_shop_id = array();
        $user_shop_parent = User::where('id', Session()->get('admin_shop_id'))->first();
        $parent_shop = Shop::where('id', $user_shop_parent->shop_id)->first();
        $shop_children = Shop::where('parent_id', $parent_shop->id)
            ->select('id')->get();

        if (count($shop_children) > 0) {
            foreach ($shop_children as $child) {
                $temp = User::where('shop_id', $child->id)->select('id')->first();
                if ($temp)
                    array_push($list_shop_id, $temp->id);
            }
        }

        $product = Product::whereIn('user_id', $list_shop_id)->count();
        $order = Order::whereIn('seller_id', $list_shop_id)->count();
        $order_success = Order::whereIn('seller_id', $list_shop_id)->where('status', Order::DON_THANH_CONG)->count();
        $order_failed = Order::whereIn('seller_id', $list_shop_id)->where('status', Order::NGUOI_MUA_HUY_DON)->count();
        $order_refund = Order::whereIn('seller_id', $list_shop_id)->where('status', Order::NGUOI_MUA_TRA_HANG)->count();
        $product_sold_out = Product::whereIn('app_products.user_id', $list_shop_id)->join('rel_product_varieties', 'app_products.id', '=', 'rel_product_varieties.product_id')
            ->groupBy('rel_product_varieties.product_id')
            ->havingRaw('SUM(rel_product_varieties.quantity) = ?', ['SUM(rel_product_varieties.sold)'])->count();

        $user = User::where('id', Session()->get('admin_shop_id'))->first();
        $shops = Shop::where('parent_id', $user->shop_id)->join('app_users', 'app_users.shop_id', '=', 'app_shops.id')->get();
        $count_promotion = Coupon::where('type_creater', '3')->count();
        $count_product_block = Product::whereIn('user_id', $list_shop_id)->whereIn('admin_status', [-1])->count();
        $count_product_sold_out = Product::whereIn('user_id', $list_shop_id)->join('rel_product_varieties', 'app_products.id', '=', 'rel_product_varieties.product_id')
            ->where('rel_product_varieties.quantity', 0)->distinct()->count();

        return view('admin-shop.dashboard')->with(compact('product', 'order', 'shops', 'order_success', 'order_failed', 'order_refund', 'product_sold_out', 'count_promotion', 'count_product_block', 'count_product_sold_out'));
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/profile-page
     */
    public function getProfilePage()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.user.profile');
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/profiles
     */
    public function GetProfile()
    {
        UserController::AuthAdminShop();
        $id = Session()->get('admin_shop_id');

        //get information of admin shop
        $profile = User::where('id', $id)
            ->select('email', 'phone', 'fullname', 'shop_id',
                'cover_id', 'avatar_id', 'role')
            ->first();

        //get information of shop
        $shop = Shop::where('id', $profile->shop_id)
            ->select('name', 'description')->first();
        $profile->shop_name = $shop->name;
        $profile->description = $shop->description;

        $profile->cover_image = File::where('id', $profile->cover_id)->value('url');
        $profile->avatar_image = File::where('id', $profile->avatar_id)->value('url');
        $profile->bcoins = Bcoin::where('user_id', $id)->first()->amount ?? 0;
        $profile->total_bcoins_of_shop = $this->get_bcoins_admin_shop($profile->shop_id);
        return response()->json(['profile' => $profile]);
    }

    /**
     * POST: http://localhost/bisto_web/admin-shop/profiles
     */
    public function UpdateProfile(Request $request)
    {
        UserController::AuthAdminShop();
        $user_id = Session()->get('admin_shop_id');
        $admin = User::where('id', $user_id)->first();

        if (is_null($request->full_name)) {
            return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
        }

        $admin->fullname = $request->full_name;
        $admin->email = $request->email;
        $admin->balance = 0;
        $admin->status = 1;
        $admin->save();

        //get information of admin_shop and save
        $shop = Shop::where('id', $admin->shop_id)->first();
        $shop->name = $request->shop_name;
        $shop->description = $request->description;
        $shop->save();

        return response()->json(['message' => 'Thay đổi thông tin thành công']);
    }

    /* get total bcoins of shop */
    private function get_bcoins_admin_shop($id)
    {
        $bcoins = 0;
        $list_shops = Shop::where('parent_id', $id)->get();

        foreach ($list_shops as $shop) {
            $user = User::where('shop_id', $shop->id)->value('id');
            $bcoin = Bcoin::where('user_id', $user)->value('amount');
            $bcoins += $bcoin;
        }
        return $bcoins;
    }

    /**
     * POST: http://localhost/bisto_web/admin-shop/upload-avatar
     */
    public function UploadAvatar(Request $request)
    {
        UserController::AuthAdminShop();
        $data = $request->imageUrl;
        $id = File::saveFiles($data['url'], $data['name'], $data['size'], $data['type']);

        $user = User::where('id', Session()->get('admin_shop_id'))->first();
        if ($user->avatar_id) {
            File::find($user->avatar_id)->delete();
        }
        $user->avatar_id = $id;
        $user->save();

        return response()->json(array('url' => $data['url'], 'success' => 'true'));
    }

    /**
     * POST: http://localhost/bisto_web/admin-shop/upload-cover
     */
    public function UploadCover(Request $request)
    {
        UserController::AuthAdminShop();
        $data = $request->imageUrl;
        $id = File::saveFiles($data['url'], $data['name'], $data['size'], $data['type']);

        $user = User::where('id', Session()->get('admin_shop_id'))->first();
        if ($user->cover_id) {
            File::find($user->cover_id)->delete();
        }
        $user->cover_id = $id;
        $user->save();

        return response()->json(array('url' => $data['url'], 'success' => 'true'));
    }

    /**
     * GET /admin-shop/list-image-size
     */
    public function getImageSizePage()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.size.lists');
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/image-sizes
     */
    public function GetImageSize()
    {
        UserController::AuthAdminShop();

        $keywords = request()->keyword;
        $size_image = SizeInformation::where('user_id', Session()->get('admin_shop_id'))
            ->join('app_files', 'app_size_informations.image_id', '=', 'app_files.id')
            ->select('app_size_informations.id as id', 'app_size_informations.name',
                'app_size_informations.image_id', 'app_files.url')
            ->where('app_size_informations.name', 'like', '%' . $keywords . '%')
            ->orderBy('app_size_informations.created_at', 'DESC')
            ->paginate(10);

        if ($size_image) {
            return response()->json(['size_image' => $size_image]);
        }

        return response()->json([], 204);
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/image-sizes/3
     */
    public function GetImageSizeById($id)
    {
        UserController::AuthAdminShop();
        $admin_shop_id = Session()->get('admin_shop_id');
        $size_info = SizeInformation::where('id', $id)->where('user_id', $admin_shop_id)
            ->select('id', 'name', 'image_id')
            ->first();

        if (is_null($size_info)) {
            return response()->json([], 204);
        }

        $size_info->url = File::where('id', $size_info->image_id)->value('url');
        return response()->json(['result' => $size_info]);
    }

    /**
     * POST: http://localhost/bisto_web/admin-shop/image-sizes
     */
    public function InsertImageSize(Request $request)
    {
        UserController::AuthAdminShop();
        $admin_shop_id = Session()->get('admin_shop_id');
        $size = array();

        $isExist = SizeInformation::where('name', '=', $request->name)->first();
        if ($isExist) {
            return response()->json(['message' => 'Tên size bị trùng'], 400);
        }

        $size['name'] = $request->name;
        if (is_null($request->source_url)) {
            return response()->json(['message' => 'Hãy chọn ảnh cho size'], 400);
        }
        $data = $request->source_url;

        if ($request->name) {
            $image_id = File::saveFiles($data['url'], $data['filename'], $data['size'], $data['type']);
            $size['image_id'] = $image_id;
            $size['user_id'] = $admin_shop_id;
            SizeInformation::insert($size);
            return response()->json(['message' => 'Thêm thông tin size thành công'], 201);
        } else {
            return response()->json(['message' => 'Lưu ảnh không thành công, hãy thử lại'], 400);
        }
    }

    /**
     * PUT: http://localhost/bisto_web/admin-shop/image-sizes/2
     */
    public function UpdateImageSize(Request $request, $id)
    {
        UserController::AuthAdminShop();
        $admin_shop_id = Session()->get('admin_shop_id');

        $size_info = SizeInformation::where('id', $id)->where('user_id', $admin_shop_id)->first();
        if (is_null($size_info)) {
            return response()->json([], 204);
        }

        $isExist = SizeInformation::where('name', '=', $request->name)->where('id', '<>', $id)->first();
        if ($isExist) {
            return response()->json(['message' => 'Tên thông tin size bị trùng'], 400);
        }

        $deleted_imageId = $request->deleted_imageid;
        if ($deleted_imageId != null) {
            File::where('id', $deleted_imageId)->delete();
        }

        $imageId = $size_info->image_id;
        if ($request->source_url) {
            $data = $request->source_url;
            $imageId = File::saveFiles($data['url'], $data['filename'], $data['size'], $data['type']);
        }

        $size_info->image_id = $imageId;
        $size_info->name = $request->name;
        $result = $size_info->save();
        if ($result) {
            return response()->json(['message' => 'Sửa thông tin size thành công']);
        }
        return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
    }

    /**
     * DELETE: http://localhost/bisto_web/admin-shop/image-sizes/2
     */
    public function DeleteImageSize($id)
    {
        UserController::AuthAdminShop();
        $admin_shop_id = Session()->get('admin_shop_id');
        $size_info = SizeInformation::where('id', $id)->where('user_id', $admin_shop_id)->first();

        if (is_null($size_info)) {
            return response()->json([], 204);
        }

        File::where('id', $size_info->image_id)->delete();
        $size_info->delete();
        return response()->json(['message' => 'Xóa hình ảnh size thành công']);
    }

    /* manage rating and comments of products */
    public function manage_rate_product()
    {
        UserController::AuthAdminShop();
        return view('shop.rate.lists');
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/password-page
     */
    public function getPasswordPage()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.user.update_password');
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/passwords
     */
    public function GetPassword()
    {
        UserController::AuthAdminShop();
        $admin = new User();
        $admin = $admin->find(Session()->get('admin_shop_id'));
        return response()->json(['user' => $admin]);
    }

    /**
     * POST: http://localhost/bisto_web/admin-shop/passwords
     */
    public function UpdatePassword(Request $req)
    {
        UserController::AuthAdminShop();
        $admin = new User();
        $admin = $admin->find(Session()->get('admin_shop_id'));
        $data = $req->all();
        $passwordReq = $data['current_password'];
        if (password_verify($passwordReq, $admin->password)) {
            $admin->password = password_hash($data['new_password'], PASSWORD_BCRYPT, array('cost' => 12));
            $admin->save();
            return response()->json(['message' => 'Thay đổi mật khẩu thành công']);
        } else {
            return response()->json(['message' => 'Mật khẩu không đúng'], 400);
        }
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/addresses
     */
    public function GetAddress()
    {
        UserController::AuthAdminShop();
        $shipping_info = ShippingInfo::where('user_id', Session()->get('admin_shop_id'))
            ->where('is_default', 1)->first();
        if ($shipping_info) {
            return response()->json(['address' => $shipping_info]);
        }
        return response()->json([], 204);
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/addresses
     */
    public function getAddressPage()
    {
        UserController::AuthAdminShop();
        $shipping_info = ShippingInfo::where('user_id', Session()->get('admin_shop_id'))
            ->where('is_default', 1)->first();
        return view('admin-shop.user.add_address')->with(compact('shipping_info'));
    }

    /**
     * PUT: http://localhost/bisto_web/admin-shop/addresses
     */
    public function UpdateAddress(Request $request)
    {
        UserController::AuthAdminShop();
        $user_id = Session()->get('admin_shop_id');

        if (is_null($request->address)) {
            return response()->json(['message' => 'Địa chỉ không được để trống'], 400);
        }

        $data['address'] = $request->address;
        $data['city_id'] = $request->province_id;
        $data['district_id'] = $request->district_id;
        $data['ward_id'] = $request->ward_id;

        $user = User::where('id', $user_id)->select('fullname', 'phone')->first();
        if (is_null($user)) {
            return response()->json(['message' => 'Địa chỉ người dùng không tồn tại'], 400);
        }

        $data['name'] = $user->fullname;
        $data['phone'] = $user->phone;
        $data['user_id'] = $user_id;
        $data['is_default'] = 1;

        $shipping_info = ShippingInfo::where('user_id', $user_id)
            ->where('is_default', 1)->first();

        if (is_null($shipping_info)) {
            $result = ShippingInfo::insert($data);
            if ($result) {
                return response()->json(['message' => 'Thêm mới địa chỉ thành công'], 201);
            }
        } else {
            $result = ShippingInfo::where('id', $shipping_info->id)->update($data);
            if ($result) {
                return response()->json(['message' => 'Thay đổi địa chỉ thành công']);
            }
        }
        return response()->json(['message' => 'Dữ liệu không hợp lệ'], 400);
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/avatars
     */
    public function GetAvatar()
    {
        UserController::AuthAdminShop();
        $id = Session()->get('admin_shop_id');
        $avatar_id = User::where('id', $id)->value('avatar_id');
        if (is_null($avatar_id)) {
            $url = DEFAULT_AVATAR;
        } else {
            $url = File::where('id', $avatar_id)->value('url');
        }
        return response()->json(array('source' => $url));
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/shop-page
     */
    public function shop_page()
    {
        UserController::AuthAdminShop();
        return view('admin-shop.shop.lists');
    }

    /**
     * GET: http://localhost/bisto_web/admin-shop/shops
     */
    public function GetAllShop()
    {
        UserController::AuthAdminShop();
        $user_id = Session()->get('admin_shop_id');
        $shop_id = User::where('id', $user_id)->value('shop_id');

        $keywords = request()->keyword;
        $list_shop = Shop::where('parent_id', $shop_id)
            ->where('name', 'like', '%' . $keywords . '%')
            ->select('id', 'name')
            ->get();

        //return list details shop with pagination
        $user_shop = $this->show_query_shop($list_shop);
        if ($user_shop) {
            return response()->json(['results' => $user_shop]);
        }
        return response()->json([], 204);
    }

    /** get details information of shop */
    private function show_query_shop($list_shop)
    {
        $users_shop = array();
        foreach ($list_shop as $shop) {
            array_push($users_shop, $shop);
        }

        $list_id = array();
        foreach ($list_shop as $item) {
            array_push($list_id, $item->id);
        }

        $user_shop = Shop::whereIn('id', $list_id)
            ->select('id', 'name', 'parent_id', 'description')
            ->paginate(10);
        foreach ($user_shop as $shop) {
            $user = User::where('shop_id', $shop->id)
                ->select('id as user_id', 'fullname', 'email', 'phone')
                ->first();

            $shipping_info = ShippingInfo::where('user_id', $user->user_id)
                ->where('is_default', 1)
                ->select('address', 'city_id', 'district_id', 'ward_id')
                ->first();
            $bcoins = Bcoin::where('user_id', $user->user_id)->value('amount');
            if (!is_null($bcoins)) {
                $shop->amount = $bcoins;
            }
            if ($shipping_info) {
                $province = City::where('id', $shipping_info->city_id)->value('name');
                $district = District::where('id', $shipping_info->district_id)->value('name');
                $ward = Ward::where('id', $shipping_info->ward_id)->value('name');
                $shop->address = $shipping_info->address . ', ' . $ward . ', ' . $district . ', ' . $province;
            } else {
                $shop->address = '';
            }
            $shop->full_name = $user->fullname;
            $shop->email = $user->email;
            $shop->phone_number = $user->phone;
            $shop->id = $user->user_id;
        }
        return $user_shop;
    }


    /**
     * GET /admin-shop/all-stylish
     */
    public function getAllStylish()
    {
        UserController::AuthAdminShop();
        $stylishes = Stylish::get();
        return response()->json(['stylishes' => $stylishes]);
    }


    /**
     * GET: http://localhost/bisto_web/admin-shop/shops/2
     */
    public function GetUserShopById($id)
    {
        UserController::AuthAdminShop();
        $shop = Shop::where('id', $id)->first();
        $user = User::where('shop_id', $id)->first();

        if ($shop && $user) {
            return response()->json(['shop' => $shop, 'user' => $user]);
        }
        return response()->json([], 204);
    }

    /**
     * POST: http://localhost/bisto_web/admin-shop/shops
     */
    public function SaveUserShop(Request $request)
    {
        UserController::AuthAdminShop();
        $user_id = Session()->get('admin_shop_id');
        $shop_id = User::where('id', $user_id)->value('shop_id');

        $user = array();
        $user['status'] = '1';
        $user['fullname'] = $request->full_name;
        $user['phone'] = $request->phone_number;
        $user['username'] = $request->user_name;
        $email = $request->email;
        $user['email'] = $email;

        $password = $request->password;
        $hash = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
        $user['password'] = $hash;
        $user['role'] = 2;

        $check = User::where('email', $email)->first();
        if ($check !== null) {
            return response()->json(['message' => 'Tài khoản đã tồn tại'], 400);
        }

        $shop = array();
        $shop['name'] = $request->name;
        $shop['range_age'] = '0,100';
        $shop['description'] = $request->description;
        $shop['parent_id'] = $shop_id;

        DB::transaction(function () use ($shop, $user) {
            $shopId = Shop::insertGetId($shop);
            $user['shop_id'] = $shopId;
            $userId = User::insertGetId($user);
            if ($user) {
                Bcoin::create([
                    'user_id' => $userId,
                    'amount' => Bcoin::AMOUNT_INIT,
                ]);
            }
        });
        return response()->json(['message' => 'Tài khoản được thêm thành công'], 201);
    }

    /**
     * PUT: http://localhost/bisto_web/admin-shop/shops/3
     */
    public function UpdateUserShop(Request $request, $id)
    {
        UserController::AuthAdminShop();
        $user = array();
        $user['fullname'] = $request->full_name;
        $user['phone'] = $request->phone_number;
        $email = $request->email;
        $user['email'] = $email;

        $password = $request->password;
        if ($password !== null) {
            $hash = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
            $user['password'] = $hash;
        }

        $email_user = User::where('shop_id', $id)->value('email');
        $check = User::where('email', $email)->get();

        if (count($check) === 0 || (count($check) === 1 && strcmp($email, $email_user) === 0)) {
            $shop = array();
            $shop['name'] = $request->name;
            $shop['description'] = $request->description;
            DB::table('app_shops')->where('id', $id)->update($shop);

            Shop::where('id', $id)->value('parent_id');
            User::where('shop_id', $id)->update($user);
            return response()->json(['message' => 'Cập nhật tài khoản thành công']);
        }
        return response()->json(['message' => 'Tài khoản đã tồn tại'], 400);
    }

    /**
     * DELETE: http://localhost/bisto_web/admin-shop/shops/3
     */
    public function DeleteUserShop($id)
    {
        UserController::AuthAdminShop();
        Shop::where('id', $id)->delete();

        /* delete all products */
        $owner = User::where('shop_id', $id)->value('owner');
        $all_products = Product::where('user_id', $owner)->value('id');
        foreach ($all_products as $id) {
            Product::where('id', $id)->delete();

            /*foreach ($all_products as $id) {
                DB::table('product_image')->where('image', $id)->delete();
                DB::table('image')->where('id', $id)->delete();
            }*/
        }

        /* delete avatar-picture cover */
        $avatar = User::where('shop_id', $id)->value('avatar_id');
        if ($avatar) {
            File::where('id', $avatar)->delete();
        }

        $cover = User::where('shop_id', $id)->value('cover_id');
        if ($cover) {
            File::where('id', $cover)->delete();
        }

        User::where('shop_id', $id)->delete();
        return response()->json(['message' => 'Xóa tài khoản thành công']);
    }

    /**
     * SHOW PAYMENT
     */
    public function payment()
    {
        UserController::AuthAdminShop();
        $id = Session()->get('admin_shop_id');
        $shop_id = User::where('id', $id)->first()->shop_id;
        $bcoins = Bcoin::where('user_id', $id)->first()->amount ?? 0;
        $total_bcoins_of_shop = $this->get_bcoins_admin_shop($shop_id);
        return view('admin-shop.payment.payment', ['bcoins' => $bcoins, 'total_bcoins_of_shop' => $total_bcoins_of_shop]);
    }

    /**
     * SHOW PAYMENT
     */
    public function payment_return($provider)
    {
        UserController::AuthAdminShop();
        switch ($provider) {
            case UserTransaction::PROVIDER_VNPAY:
                return view('admin-shop.payment.vnpay_return');
            case UserTransaction::PROVIDER_ZALOPAY:
                return view('admin-shop.payment.zalopay_return');
            case UserTransaction::PROVIDER_MOMO:
                return view('admin-shop.payment.momo_return');
            default:
                return route('admin.dashboard');
        }
    }

    public function updateStatusNotification(Request $req)
    {
        UserController::AuthAdminShop();
        $shop_id = Session()->get('admin_shop_id');
        $notificationIds = DB::table('rel_user_notifications')->where('user_id', $shop_id)->whereIn('notification_id', $req->listId)->update(['is_seen' => true]);
        $count = DB::table('rel_user_notifications')->where('user_id', $shop_id)->where('is_seen', false)->count();
        return response()->json(['count' => $count], 200);
    }


    //get notifications
    public function getNotifications($index)
    {
        UserController::AuthAdminShop();
        $shop_id = Session()->get('admin_shop_id');
        $notificationIds = DB::table('rel_user_notifications')->where('user_id', $shop_id)->pluck('notification_id')->toArray();
        $notifications = Notification::whereIn('id', $notificationIds)->orderBy('created_at', "DESC")->get();
        $notificationNotSeenIds = DB::table('rel_user_notifications')->where('user_id', $shop_id)->where('is_seen', false)->pluck('notification_id')->toArray();
        $notificationsNotSeenCount = UserNotification::whereIn('id', $notificationNotSeenIds)->count();
        $results = array();
        $haveMore = 0;
        // if have more 5 records remain
        if (count($notifications) > (5 * $index)) {
            $haveMore = 1;
            for ($i = 0; $i < 5; $i++) {
                array_push($results, $notifications[5 * $index - 5 + $i]);
            }
        } // if have little 5 records remain
        else {
            for ($i = 0; $i + 5 * $index - 5 < count($notifications); $i++) {
                array_push($results, $notifications[5 * $index - 5 + $i]);
            }
        }
        return response()->json(['success' => 1, 'results' => $results, 'haveMore' => $haveMore, 'countSeenNotification' => $notificationsNotSeenCount], 200);
    }

}
