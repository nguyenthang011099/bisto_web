<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Advertisement;
use App\BlogCategory;
use App\Exceptions\CustomException;
use App\File;
use App\RelTag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdvertisementController extends Controller
{
    public function AdminListsAdvertisement()
    {
        UserController::AuthAdmin();
        return view('admin.advertisement.lists');
    }

    /**
     * GET: http://localhost/bisto_web/admin/advertisements
     */
    public function AdminGetAdvertisements(): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdmin();
        $id = Session()->get('admin_id');
        $advertisements = Advertisement::orderBy('app_advertisements.created_at', 'DESC')
            ->select('id', 'url', 'image_id', 'created_at', 'updated_at')
            ->paginate(10);

        foreach ($advertisements->items() as $advertisement) {
            $image = File::where('id', $advertisement->image_id)->first();
            $advertisement['image'] = $image;
        }
        return response()->json(['success' => 1, 'results' => $advertisements]);
    }

    /**
     * GET: http://localhost/bisto_web/admin/advertisements/2
     */
    public function AdminGetAdvertisementById(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        UserController::AuthAdmin();
        $user_id = Session()->get('admin_id');

        $advertisement = Advertisement::where('id', $id)
            ->select('id', 'url', 'image_id', 'created_at', 'updated_at')
            ->first();
        $advertisement['image'] = File::where('id', $advertisement->image_id)->first();

        return response()->json(['result' => $advertisement]);
    }

    /**
     * POST: http://localhost/bisto_web/admin/advertisements
     * @throws CustomException
     */
    public function AdminPostAdvertisement(Request $request)
    {
        try {
            UserController::AuthAdmin();
            $data = $request->all();

            if (is_null($data['image'])) {
                throw CustomException::makeBadRequest('Chọn hình ảnh cần thêm');
            }
            $user_id = Session()->get('admin_id');

            $image = $data['image'];
            $imageId = File::saveFiles($image['url'], $image['filename'], $image['size'], $image['type']);

            $advertisement = new Advertisement();
            $advertisement->url = $data['url'];
            $advertisement->image_id = $imageId;

            $result = $advertisement->save();
            return response()->json(['message' => 'Thêm ảnh quảng cáo thành công'], 201);
        } catch (\Exception $e) {
            throw CustomException::makeServerError($e->getMessage());
        }
    }

    /**
     * PUT: http://localhost/bisto_web/admin/advertisements/2
     */
    public function AdminPutAdvertisement(Request $request, $id)
    {
        UserController::AuthAdmin();
        $user_id = Session()->get('admin_id');

        $advertisement = Advertisement::where('id', $id)->first();
        if (is_null($advertisement)) {
            return response()->json([], 204);
        }

        if ($request->image) {
            $newImage = new File();
            $newImage->url = $request->image['url'];
            $newImage->size = $request->image['size'];
            $newImage->type = $request->image['type'];
            $newImage->filename = $request->image['filename'];
            $newImage->save();
            $advertisement->image_id = $newImage->id;
        }

        $advertisement->url = $request['url'];
        $result = $advertisement->save();
        if ($result) {
            return response()->json(['message' => 'Cập nhật ảnh quảng cáo thành công']);
        }
        return response()->json(['message', 'Dữ liệu không hợp lệ'], 400);
    }

    /**
     * DELETE: http://localhost/bisto_web/admin/advertisements/2
     */
    public function AdminDeleteAdvertisement($id)
    {
        UserController::AuthAdmin();
        $advertisement = Advertisement::where('id', $id)->first();
        if (is_null($advertisement)) {
            return response()->json([], 204);
        }

        $advertisement->delete();
        return response()->json(['message' => 'Xóa ảnh quảng cáo thành công']);
    }
}
