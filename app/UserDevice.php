<?php


namespace App;


class UserDevice extends \Illuminate\Database\Eloquent\Model
{
    protected $fillable = [
        'user_id', 'socket_id', 'fcm_token', 'message_notification_type'
    ];

    public $timestamps = false;

    protected $primaryKey = 'id';

    protected $table = 'app_user_devices';

}
