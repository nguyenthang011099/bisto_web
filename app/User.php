<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model
{
    public $timestamps = false; //set time to false

    const ROLE_SHOP = 2;
    const ROLE_ADMIN_SHOP = 3;
    const ROLE_ADMIN = 4;
    /**
     * username:
     * fullname: họ và tên
     * email
     * phone: số điện thoại
     * shop_id: id của shop
     * password: mã bcrypt với sđt
     * role: 1-customer, 2-shop, 3-admin-shop, 4-admin
     * status: 1-active, -1-hide
     * gender: giới tính
     * date_of_birth: ngày sinh
     * balance:
     * avatar_id: id hình ảnh profile
     * user_physical_id: thông tin thể chất
     * cover_id: id hình ảnh nền
     */
    protected $fillable = [
        'username', 'fullname', 'email', 'password', 'phone', 'shop_id', 'status',
        'role', 'is_active', 'gender', 'date_of_birth', 'balance',
        'avatar_id', 'user_physical_id', 'cover_id', 'activation_code'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_users';

    public function bcoin()
    {
        return $this->hasOne(Bcoin::class);
    }

    public static function getUsers()
    {
        $users = DB::table('app_users')->where('role', 1)
            ->select('id', 'fullname', 'email', 'phone')
            ->get()->toArray();

        return self::getInfo($users);
    }

    public static function getShops()
    {
        $users = DB::table('app_users')->where('role', 2)
            ->select('id', 'fullname', 'email', 'phone')
            ->get()->toArray();
        return self::getInfo($users);
    }

    public static function getShopsWithOption($is_without_branch = 0)
    {
        $users = DB::table('app_users')->where('role', 2)
            ->join('app_shops', 'app_shops.id', '=', 'app_users.shop_id');
        if ($is_without_branch == 1) {
            $users = $users->where('app_shops.parent_id', null)
                ->select('app_users.id', 'fullname', 'email', 'phone', 'cmnd', 'tax_code');
        } else if ($is_without_branch == 0) {
            $users = $users->where('app_shops.parent_id', '<>', null)
                ->select('app_users.id', 'fullname', 'email', 'phone');
        }
        $users = $users->get()->toArray();
        return self::getInfo($users);
    }

    public static function getAdminShops()
    {
        $users = DB::table('app_users')->where('role', 3)
            ->select('id', 'fullname', 'email', 'phone')
            ->get()->toArray();
        return self::getInfo($users);
    }

    private static function getInfo($users)
    {
        foreach ($users as $item) {
            $shipping_info = DB::table('app_shipping_informations')->where('user_id', $item->id)
                ->where('is_default', 1)->first();
            $item->address = '';
            if ($shipping_info) {
                $item->address = self::getAddress($shipping_info);
            }
            unset($item->id);
        }
        return $users;
    }

    public static function getAddress($user)
    {
        $province = DB::table('app_cities')->where('id', $user->city_id)->value('name');
        $district = DB::table('app_districts')->where('id', $user->district_id)->value('name');
        $ward = DB::table('app_wards')->where('id', $user->ward_id)->value('name');
        return $user->address . ", " . $ward . ", " . $district . ", " . $province;
    }

    public static function getInformation($userId)
    {
        return User::where('id', $userId)->select('phone', 'fullname')->first();
    }

    public static function getFullNameAndAvatar($userId)
    {
        $user = User::where('id', $userId)->first();
        $source_avatar = DEFAULT_AVATAR;
        if (!is_null($user->avatar_id)) {
            $source_avatar = File::where('id', $user->avatar_id)->value('url');
        }
        return ['source_avatar' => $source_avatar, 'full_name' => $user->fullname];
    }

    public static function getFullName($userId)
    {
        $user = User::where('id', $userId)->first();
        if ($user) {
            return $user->fullname;
        }
        return '';
    }
}
