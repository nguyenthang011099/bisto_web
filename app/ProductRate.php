<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRate extends Model
{
    public $timestamps = false; //set time to false

    /**
     * rate: đánh giá
     * comment: bình luận sản phẩm
     * reply: trả lời bình luận
     * product_id: id của sản phẩm
     * parent_id: bình luận cấp cao
     * user_id: id của người dùng
     * file_id: file đánh giá
     */
    protected $fillable = [
        'rate', 'comment', 'reply', 'parent_id',
        'product_id', 'user_id', 'file_id'
    ];

    protected $primaryKey = 'id';
    protected $table = 'rel_user_product_rates';
}
