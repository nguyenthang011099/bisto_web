<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bcoin extends Model
{
    public $timestamps = false; //set time to false

    /**
     * amount: số lượng bcoin
     * user_id: id người sử dụng
     * sender_id: id người gửi
     */
    protected $fillable = [
        'amount', 'user_id'
    ];

    const AMOUNT_INIT = 0;

    protected $primaryKey = 'id';
    protected $table = 'app_bcoins';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
