<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SizeInformation extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên size
     * image_id: id hình ảnh
     * user_id: id của chủ shop
     */
    protected $fillable = [
        'name', 'image_id', 'user_id'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_size_informations';
}
