<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRateAttachment extends Model
{
    public $timestamps = false; //set time to false

    /**
     * source_url: đường dẫn của ảnh trên firebase
     * type: hình ảnh
     * product_rate: id của đánh giá
     */
    protected $fillable = [
        'source_url', 'type', 'product_rate'
    ];

    protected $primaryKey = 'id';
    protected $table = 'product_rate_attachment';
}
