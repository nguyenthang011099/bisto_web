<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingInfo extends Model {
    public $timestamps = false; //set time to false

    /**
     * full_name: họ và tên
     * address: địa chỉ
     * phone_number: số điện thoai
     * city_id: id của tỉnh thành
     * district_id: id quận Huyện
     * ward_id: id xã phường
     * ghn_id:
     * user_id: id của người dùng
     * is_default: 1-hiển thị, 0-ẩn
     */
    protected $fillable = [
    	'full_name', 'address', 'phone_number', 'city_id', 'district_id',
        'ward_id', 'ghn_id', 'user', 'is_default'
    ];

    protected $primaryKey = 'id';
 	protected $table = 'app_shipping_informations';
}
