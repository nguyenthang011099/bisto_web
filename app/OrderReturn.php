<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderReturn extends Model
{
    public $timestamps = false; //set time to false

    /**
     * reason: lí do trả hàng
     * order_id: id của đơn hàng
     * image_ids: list id hình ảnh
     */
    protected $fillable = [
        'reason', 'order_id', 'image_ids'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_order_returns';

    public static function showOrderReturn($orderId)
    {
        $orderReturn = OrderReturn::where('order_id', $orderId)
            ->select('reason', 'image_ids', 'created_at')
            ->first();
        if (!$orderReturn) return;

        $orderReturn->shipping_date = ShippingOrder::where('order_id', $orderId)
            ->value('created_at');

        $images = array();
        $imageIds = explode(",", $orderReturn->image_ids);
        foreach ($imageIds as $id) {
            $source = File::where('id', $id)->value('url');
            array_push($images, $source);
        }

        $orderReturn->source_urls = $images;
        return $orderReturn;
    }
}
