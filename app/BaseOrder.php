<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

abstract class BaseOrder extends Model
{
    public function getDetailOrder($orderId)
    {
        $order = Order::where('id', $orderId)
            ->select('status', 'code', 'payment_method', 'note',
                'payment_status', 'user_id', 'total', 'seller_id',
                'amount_voucher', 'coupon_id', 'created_at')
            ->first();

        $orderItemIds = OrderItem::where('order_id', $orderId)
            ->select('id')->get();

        if (is_null($orderItemIds)) {
            return array();
        }

        $items = $this->getProductsOrder($orderItemIds);
        $order->total_money = $order->total;
        list($status, $color) = $this->showDetailStatus($order->status);
        $order->status_code = $order->status;
        $order->status = $status;
        $order->color = $color;

        $shippingOrder = ShippingOrder::where('order_id', $orderId)
            ->select('shipping_fee', 'delivery_unit_id')->first();
        if ($shippingOrder) {
            $order->shipping_fee = $shippingOrder->shipping_fee;
            $order->provider_name = self::showProviderName($shippingOrder->delivery_unit_id);
        }

        return array($order, $items);
    }

    private function showDetailStatus($status)
    {
        switch ($status) {
            case Order::NGUOI_MUA_DANG_MUA_HANG:
                $status = 'Người mua đang mua hàng';
                $color = '#ffff00';
                break;
            case Order::NGUOI_MUA_DA_TAO_DON:
                $status = 'Người mua đã tạo đơn hàng';
                $color = '#000000';
                break;
            case Order::NGUOI_BAN_XAC_NHAN_DON:
                $status = 'Người bán xác nhận đơn hàng';
                $color = '#000000';
                break;
            case Order::DON_HANG_DANG_GIAO:
                $status = 'Đơn hàng đang giao';
                $color = '#f7ea00';
                break;
            case Order::NGUOI_MUA_DA_NHAN_HANG:
                $status = 'Người mua đã nhận được đơn hàng';
                $color = '#3366ff';
                break;
            case Order::NGUOI_MUA_HUY_DON:
                $status = 'Người mua hủy đơn hàng';
                $color = '#ff0000';
                break;
            case Order::NGUOI_MUA_TRA_HANG:
                $status = 'Người mua trả hàng';
                $color = '#ff0000';
                break;
            case Order::NGUOI_MUA_DOI_HANG:
                $status = 'Người mua đổi hàng';
                $color = '#ff0000';
                break;
            case Order::DON_THANH_CONG:
                $status = 'Đơn thành công';
                $color = '#00cc00';
                break;
            case Order::NGUOI_BAN_HUY_DON:
                $status = 'Người bán hủy đơn hàng';
                $color = '#ff0000';
                break;
            default:
                $status = '';
                $color = '';
                break;
        }
        return array($status, $color);
    }

    public static function showProviderName($provider)
    {
        switch ($provider) {
            case ShippingOrder::GIAO_HANG_NHANH:
                $providerName = 'Giao hàng nhanh';
                break;
            case ShippingOrder::GHTK:
                $providerName = 'Giao hàng tiết kiệm';
                break;
            case ShippingOrder::AHAMOVE:
                $providerName = 'Ahamove';
                break;
            default:
                $providerName = 'Trống';
        }
        return $providerName;
    }

    /* Lấy ra đơn hàng nửa đầu tháng */
    public function getOrdersFirstMonth($data, $orderIds, $statusOrder, $keyword = null)
    {
        $date1 = date($data['year'] . '-' . $data['month'] . '-01');
        $date2 = date($data['year'] . '-' . $data['month'] . '-15');
        return $this->getOrdersByDay($orderIds, $statusOrder, $date1, $date2, $keyword);
    }

    /* Lấy ra đơn hàng nửa cuối tháng */
    public function getOrdersLastMonth($data, $orderIds, $statusOrder, $keyword = null)
    {
        $date1 = date($data['year'] . '-' . $data['month'] . '-15');
        $date2 = date($data['year'] . '-' . $data['month'] . '-t');
        return $this->getOrdersByDay($orderIds, $statusOrder, $date1, $date2, $keyword);
    }

    /* Lấy ra đơn hàng của cả tháng */
    public function getOrdersOfMonth($data, $orderIds, $statusOrder, $keyword = null)
    {
        $date1 = date($data['year'] . '-' . $data['month'] . '-01');
        $date2 = date($data['year'] . '-' . $data['month'] . '-t');
        return $this->getOrdersByDay($orderIds, $statusOrder, $date1, $date2, $keyword);
    }

    /* Lấy ra đơn hàng nửa đầu quý */
    public function getOrdersFirstPrecious($data, $orderIds, $statusOrder, $keyword = null)
    {
        $precious = $data['precious'];
        $lastMonthOfPrecious = 3 * $precious;

        $date1 = date($data['year'] . '-' . ($lastMonthOfPrecious - 2) . '-01');
        $date2 = date($data['year'] . '-' . ($lastMonthOfPrecious - 1) . '-15');
        return $this->getOrdersByDay($orderIds, $statusOrder, $date1, $date2, $keyword);
    }

    /* Lấy ra đơn hàng nửa cuối quý */
    public function getOrdersLastPrecious($data, $orderIds, $statusOrder, $keyword = null)
    {
        $precious = $data['precious'];
        $lastMonthOfPrecious = 3 * $precious;

        $date1 = date($data['year'] . '-' . ($lastMonthOfPrecious - 1) . '-15');
        $date2 = date($data['year'] . '-' . $lastMonthOfPrecious . '-t');
        return $this->getOrdersByDay($orderIds, $statusOrder, $date1, $date2, $keyword);
    }

    /* Lấy ra đơn hàng của cả quý */
    public function getOrdersOfPrecious($data, $orderIds, $statusOrder, $keyword = null)
    {
        $precious = $data['precious'];
        $lastMonthOfPrecious = 3 * $precious;

        $date1 = date($data['year'] . '-' . ($lastMonthOfPrecious - 2) . '-01');
        $date2 = date($data['year'] . '-' . $lastMonthOfPrecious . '-t');
        return $this->getOrdersByDay($orderIds, $statusOrder, $date1, $date2, $keyword);
    }

    /* Lấy ra đơn hàng nửa đầu năm */
    public function getOrdersFirstYear($data, $orderIds, $statusOrder, $keyword = null)
    {
        $startMonthOfYear = 1;
        $endMonthOfYear = 6;

        $date1 = date($data['year'] . '-' . $startMonthOfYear . '-01');
        $date2 = date($data['year'] . '-' . $endMonthOfYear . '-t');
        return $this->getOrdersByDay($orderIds, $statusOrder, $date1, $date2, $keyword);
    }

    /* Lấy ra đơn hàng nửa cuối năm */
    public function getOrdersLastYear($data, $orderIds, $statusOrder, $keyword = null)
    {
        $startMonthOfYear = 7;
        $endMonthOfYear = 12;

        $date1 = date($data['year'] . '-' . $startMonthOfYear . '-01');
        $date2 = date($data['year'] . '-' . $endMonthOfYear . '-t');
        return $this->getOrdersByDay($orderIds, $statusOrder, $date1, $date2, $keyword);
    }

    /* Lấy ra đơn hàng của cả năm */
    public function getOrdersOfYear($data, $orderIds, $statusOrder, $keyword = null)
    {
        $startMonthOfYear = 1;
        $endMonthOfYear = 12;

        $date1 = date($data['year'] . '-' . $startMonthOfYear . '-01');
        $date2 = date($data['year'] . '-' . $endMonthOfYear . '-t');
        return $this->getOrdersByDay($orderIds, $statusOrder, $date1, $date2, $keyword);
    }

    /* Lấy ra các đơn hàng theo điều kiện ngày */
    protected function getOrdersByDay($orderIds, $statusOrder, $date1, $date2, $keyword)
    {
        if (is_null($statusOrder)) {
            $statusOrder = ['6', '7', '8', '9'];
            $orders = Order::whereIn('id', $orderIds)
                ->whereIn('status', $statusOrder)
                ->whereBetween('created_at', [$date1, $date2])
                ->where('code', 'like', '%' . $keyword . '%')
                ->select('id', 'created_at')
                ->get();
        } else {
            switch ($statusOrder) {
                case 0:
                    $orders = Order::whereIn('id', $orderIds)
                        ->whereBetween('created_at', [$date1, $date2])
                        ->where('code', 'like', '%' . $keyword . '%')
                        ->select('id', 'created_at')
                        ->get();
                    break;
                default:
                    $orders = Order::whereIn('id', $orderIds)
                        ->where('status', $statusOrder)
                        ->whereBetween('created_at', [$date1, $date2])
                        ->where('code', 'like', '%' . $keyword . '%')
                        ->select('id', 'created_at')
                        ->get();
                    break;
            }

        }

        if (!count($orders)) {
            return array();
        }

        foreach ($orders as $index => $item) {
            list($order,) = $this->getDetailOrder($item->id);
            $this->convertOrder($order, $item, $index + 1);
        }
        return $orders;
    }

    protected abstract function convertOrder($order, $item, $index);

    /* Tính số lượng đơn hàng theo trạng thái */
    public function getOrderByStatus($seller, $status)
    {
        if ($status) {
            $orders = Order::where('seller_id', $seller)
                ->where('status', $status)
                ->select('id', 'created_at')
                ->get();
        } else {
            $orders = Order::where('seller_id', $seller)
                ->where('status', $status)
                ->select('id', 'created_at')
                ->get();
        }
        return count($orders);
    }

    /** Kiểm tra mã giảm của shop hay bisto */
    protected function checkVoucher($order)
    {
        $order->bisto_voucher = 0;
        $order->shop_voucher = 0;
        if (is_null($order->coupon_id) || $order->amount_voucher == 0) {
            return $order;
        }

        $type_creater = Coupon::where('id', $order->coupon_id)
            ->select('type_creater')->get();
        if (!$type_creater) {
            return $order;
        }

        if ($type_creater == 4) {
            $order->bisto_voucher = $order->amount_voucher;
        } else {
            $order->shop_voucher = $order->amount_voucher;
        }
        return $order;
    }

    protected function showPaymentMethod($paymentMethod)
    {
        switch ($paymentMethod) {
            case 0:
                $paymentMethodName = 'MOMO';
                break;
            case 1:
                $paymentMethodName = 'ZaloPay';
                break;
            case 2:
                $paymentMethodName = 'VNPay';
                break;
            default:
                $paymentMethodName = 'TM';
                break;
        }
        return $paymentMethodName;
    }

    protected function feePaymentMethod($paymentMethod, $money)
    {
        list($amount, ,) = PaymentService::getFeePayment($money, $paymentMethod);
        return $amount;
    }

    /**
     * Tính ngày bắt đầu và ngày kết thúc theo tham số truyền vào để xuất khẩu excel
     */
    public function convertDateTime($option, $month, $precious, $year)
    {
        if (!is_null($option) && !is_null($year)) {
            if ($month) {
                if ($option == 0) {
                    $date1 = date($year . '-' . $month . '-01');
                    $date2 = date($year . '-' . $month . '-t');
                }

                if ($option == 1) {
                    $date1 = date($year . '-' . $month . '-01');
                    $date2 = date($year . '-' . $month . '-15');
                }

                if ($option == 2) {
                    $date1 = date($year . '-' . $month . '-15');
                    $date2 = date($year . '-' . $month . '-t');
                }
            } else if ($precious) {
                $lastMonthOfPrecious = 3 * $precious;
                if ($option == 0) {
                    $date1 = date($year . '-' . ($lastMonthOfPrecious - 2) . '-01');
                    $date2 = date($year . '-' . $lastMonthOfPrecious . '-t');
                }

                if ($option == 1) {
                    $date1 = date($year . '-' . ($lastMonthOfPrecious - 2) . '-01');
                    $date2 = date($year . '-' . ($lastMonthOfPrecious - 1) . '-15');
                }

                if ($option == 2) {
                    $date1 = date($year . '-' . ($lastMonthOfPrecious - 1) . '-15');
                    $date2 = date($year . '-' . $lastMonthOfPrecious . '-t');
                }
            } else {
                $startMonthOfYear = 1;
                $endMonthOfYear = 12;
                if ($option == 0) {
                    $date1 = date($year . '-' . $startMonthOfYear . '-01');
                    $date2 = date($year . '-' . $endMonthOfYear . '-t');
                }

                if ($option == 1) {
                    $endMonthOfHalfYear = 6;

                    $date1 = date($year . '-' . $startMonthOfYear . '-01');
                    $date2 = date($year . '-' . $endMonthOfHalfYear . '-t');
                }

                if ($option == 2) {
                    $startMonthOfHalfYear = 7;

                    $date1 = date($year . '-' . $startMonthOfHalfYear . '-01');
                    $date2 = date($year . '-' . $endMonthOfYear . '-t');
                }
            }

            $date1 = date("d/m", strtotime($date1));
            $date2 = date("d/m", strtotime($date2));
            return $this->setDateToSubTitle($date1, $date2, $year);
        }
        return null;
    }

    /**
     * Trả ra tiêu đề ngày bắt đầu kết thúc
     */
    private function setDateToSubTitle($date1, $date2, $year)
    {
        return 'Từ ngày ' . $date1 . ' đến ngày ' . $date2 . ' năm ' . $year;
    }

    /**
     * Phí thu hộ
     */
    protected function getFeeCollection()
    {
        $fee = DB::table('app_service_buyer')->first();
        if ($fee) {
            return $fee->amount;
        }
        return 0;
    }

    /**
     * Phí cố định
     */
    protected function getFeeCharge($money)
    {
        $fee = DB::table('app_service_shop')->first();
        if (!$fee) {
            return 0;
        }

        if ($fee->type == 2) {
            return ($money * $fee->amount) / 100;
        }
        return $fee->amount;

    }

    /**
     * Lấy lên danh sách sản phẩm
     */
    protected function getProductsOrder($orderItemIds)
    {
        $items = array();
        $totalMoney = 0;
        foreach ($orderItemIds as $orderItem) {
            $orderItem = OrderItem::getOrderItem($orderItem->id);
            $productVarietyId = $orderItem->product_variety_id;
            $productId = ProductVariety::where('id', $productVarietyId)
                ->value('product_id');

            if (is_null($productId)) continue;

            $product = Product::getDetailProduct($productId);
            if (is_null($product)) continue;

            $totalMoney += $orderItem->sub_total;
            $product->price = $orderItem->price;
            $product->quantity = $orderItem->quantity;
            array_push($items, $product);
        }
        return $items;
    }
}
