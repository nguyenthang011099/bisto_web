<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    public $timestamps = false; //set time to false

    /**
     * product_variety_id: sản phẩm
     * quantity: số lượng sản phẩm
     * order_id: khóa ngoại đến tbl order
     * price: giá sau khi mua
     */
    protected $fillable = [
        'product_variety_id', 'order_id', 'quantity', 'price'
    ];

    protected $primaryKey = 'id';
    protected $table = 'rel_order_items';

    public static function getOrderItem($id) {
        $orderItem = OrderItem::where('id', $id)
            ->select('price', 'quantity', 'product_variety_id')
            ->first();
        $orderItem->sub_total = $orderItem->price * $orderItem->quantity;
        return $orderItem;
    }
}
