<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariety extends Model
{
    public $timestamps = false; //set time to false

    protected $fillable = [
        'size_id', 'product_id', 'price', 'quantity', 'sold', 'color_id'
    ];

    protected $primaryKey = 'id';
    protected $table = 'rel_product_varieties';
}
