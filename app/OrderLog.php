<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderLog extends Model
{
    public $timestamps = false; //set time to false

    /**
     * old_status: trạng thái cũ
     * new_status: trạng thái mới
     * content: nội dung đơn hàng
     * order_id: khóa ngoại đến tbl order
     */
    protected $fillable = [
        'old_status', 'new_status', 'content', 'order_id'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_order_logs';
}
