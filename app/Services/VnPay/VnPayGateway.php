<?php

namespace App\Services\VnPay;

/**
 * Class VnPayGateway
 */
class VnPayGateway
{
    const VNP_VERSION = '2.0.0';
    const VNP_COMMAND = 'pay';
    const VNP_CURRENT_CODE = 'VND';

    protected $vnpay_payment_url;
    protected $vnpay_return_url;
    protected $vnpay_tmn_code;
    protected $vnpay_hash_secret;

    protected static $instance = null;

    private function __construct()
    {
        $this->vnpay_payment_url = config('payment.vnpay.payment_url', '');
        $this->vnpay_return_url = config('payment.vnpay.return_url', '');
        $this->vnpay_tmn_code = config('payment.vnpay.tmn_code', '');
        $this->vnpay_hash_secret = config('payment.vnpay.hash_secret', '');
    }

    public static function getInstance()
    {
        if (!self::$instance)
            self::$instance = new self();

        return self::$instance;
    }

    /**
     * Lấy url redirect thanh toán VNPAY
     *
     * @param string $order_code Mã order (Lấy từ bisto)
     * @param int $amount Số tiền thanh toán
     * @param string $order_type Mã danh mục hàng hóa.
     * @param string $order_description Mô tả thanh toán
     * @param string $bank_code Mã Ngân hàng thanh toán
     * @param string $return_url Url chuyển hướng khi thanh toán xong
     * @return string
     */
    public function getRedirectUrlPayment(string $order_code, int $amount, string $order_type, string $order_description, string $return_url, string $bank_code = "VNPAYQR")
    {
        $params = [
            "vnp_Version" => self::VNP_VERSION,
            "vnp_TmnCode" => $this->vnpay_tmn_code,
            "vnp_Amount" => $amount * 100,
            "vnp_Command" => self::VNP_COMMAND,
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => self::VNP_CURRENT_CODE,
            "vnp_IpAddr" => $_SERVER['SERVER_ADDR'],
            "vnp_Locale" => config('app.locale'),
            "vnp_OrderInfo" => $order_description,
            "vnp_OrderType" => $order_type,
            "vnp_ReturnUrl" => $return_url,
            "vnp_TxnRef" => $order_code,
            'vnp_BankCode' => $bank_code
        ];

        return $this->generateQueryString($this->vnpay_payment_url, $params);
    }


    /**
     * Tạo query string url
     *
     * @param string $url
     * @param array $params
     * @return string
     */
    private function generateQueryString(string $url, array $params)
    {
        ksort($params);
        $query_string = '';
        $i = 0;
        $hash_data = '';

        foreach ($params as $key => $value) {
            if ($i == 1) {
                $hash_data .= '&' . $key . '=' . $value;
            } else {
                $hash_data .= $key . '=' . $value;
                $i = 1;
            }
            $query_string .= urlencode($key) . '=' . urlencode($value) . '&';
        }

        $vnp_secure_hash = hash('sha256', $this->vnpay_hash_secret . $hash_data);
        $query_string .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnp_secure_hash;

        return $url . '?' . $query_string;
    }


}
