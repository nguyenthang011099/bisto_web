<?php


namespace App\Services;


class Utils
{
    /**
     * @param $relUserNote array
     * @return array
     */
    public function getUniqueArrayNotification($relUserNote)
    {
        $listUserId = array();
        $results = array();
        foreach($relUserNote as $rel){
            if(!in_array($rel['user_id'], $listUserId)){
                array_push($results, $rel);
                array_push($listUserId, $rel['user_id']);
            }
        }
        return $results;
    }
}
