<?php

namespace App\Services\Momo;

use GuzzleHttp\Client;
use GuzzleHttp\Client as HttpClient;

/**
 * Class MomoGateway
 */
class MomoGateway
{
    protected $http_client;

    protected static $instance = null;

    private function __construct()
    {
        $this->http_client = new HttpClient([
            'base_uri' => config('payment.momo.base_uri'),
            'headers' => [
                'content-type' => 'application/json;charset=UTF-8',
                'accept'     => 'application/json;charset=UTF-8'
            ]
        ]);

    }

    public static function getInstance()
    {
        if (!self::$instance)
            self::$instance = new self();

        return self::$instance;
    }

    public function createPayment($amount, $orderId, $returnUrl, $notifyUrl)
    {
        $partnerCode = config('payment.momo.partner_code');
        $accessKey = config('payment.momo.access_key');
        $secretKey = config('payment.momo.secret_key');
        $orderInfo = "Thanh toán qua MoMo";
        $extraData = "merchantName=Bisto";
        $requestId = time() . "";
        $requestType = "captureMoMoWallet";
        //before sign HMAC SHA256 signature
        $rawHash = "partnerCode=" . $partnerCode . "&accessKey=" . $accessKey .
            "&requestId=" . $requestId . "&amount=" . $amount . "&orderId=" .
            $orderId . "&orderInfo=" . $orderInfo . "&returnUrl=" . $returnUrl . "&notifyUrl=" .
            $notifyUrl . "&extraData=" . $extraData;
        $signature = hash_hmac("sha256", $rawHash, $secretKey);

        $data = [
            'partnerCode' => $partnerCode,
            'accessKey' => $accessKey,
            'requestId' => $requestId,
            'amount' => $amount,
            'orderId' => $orderId,
            'orderInfo' => $orderInfo,
            'returnUrl' => $returnUrl,
            'notifyUrl' => $notifyUrl,
            'extraData' => $extraData,
            'requestType' => $requestType,
            'signature' => $signature
        ];

        $response = $this->http_client->request('post', '/gw_payment/transactionProcessor', [
            'body' => json_encode($data)
        ]);

        if($response->getStatusCode() == 200){
            $data = json_decode($response->getBody());
            return $data->payUrl;
        }
        return null;
    }




}
