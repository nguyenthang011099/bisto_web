<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Response;

class FcmService implements NotificationServiceInterface
{
    protected $key;
    protected $urlSubscribeBatch;
    protected $urlUnsubscribeBatch;
    protected $urlSend;

    public function __construct()
    {
        $this->key = env('FCM_SERVER_KEY');
        $this->urlSubscribeBatch = 'https://iid.googleapis.com/iid/v1:batchAdd';
        $this->urlUnsubscribeBatch = 'https://iid.googleapis.com/iid/v1:batchRemove';
        $this->urlSend = 'https://fcm.googleapis.com/fcm/send';
    }

    /**
     * @param $deviceTokens string or array
     * @param $data
     * @throws GuzzleException
     */
    public function sendBatchNotification($deviceTokens, $data = [])
    {
        self::subscribeTopic($deviceTokens, $data['topicName']);
        self::sendNotification($data, $data['topicName']);
        self::unsubscribeTopic($deviceTokens, $data['topicName']);
    }

    /**
     * @param $data
     * @param $topicName
     * @throws GuzzleException
     */
    public function sendNotification($data, $topicName = null)
    {
        $body = [
            'to' => '/topics/' . $topicName,
            'notification' => [
                'body' => $data['body'] ?? 'Something',
                'title' => $data['title'] ?? 'Something',
                'image' => $data['image'] ?? null,
            ],
            'data' => [
                'notif_id' => $data['id'] ?? null,
                'created_at' => $data['created_at'] ?? null,
            ]
            /*,
            'data' => [
                'url' => $data['url'] ?? null,
                'redirect_to' => $data['redirect_to'] ?? null,
            ]
            ,
            'apns' => [
                'payload' => [
                    'aps' => [
                        'mutable-content' => 1,
                    ],
                ],
                'fcm_options' => [
                    'image' => $data['image'] ?? null,
                ],
            ],*/
        ];

        $this->execute($this->urlSend, $body);
    }

    /**
     * @param $deviceToken array or string
     * @param $topicName
     * @throws GuzzleException
     */
    public function subscribeTopic($deviceTokens, $topicName = null)
    {
        $data = [
            'to' => '/topics/' . $topicName,
            'registration_tokens' => $deviceTokens,
        ];

        $this->execute($this->urlSubscribeBatch, $data);
    }

    /**
     * @param $deviceToken array or string
     * @param $topicName
     * @throws GuzzleException
     */
    public function unsubscribeTopic($deviceTokens, $topicName = null)
    {
        $data = [
            'to' => '/topics/' . $topicName,
            'registration_tokens' => $deviceTokens,
        ];

        $this->execute($this->urlUnsubscribeBatch, $data);
    }

    /**
     * @param $url
     * @param array $dataPost
     * @param string $method
     * @return bool
     * @throws GuzzleException
     */
    private function execute($uri, $dataPost = [])
    {
        $client = new Client();
        $params['headers'] = [
            'Content-Type' => 'application/json',
            'Authorization' => 'key=' . $this->key,
        ];
        $params['json'] = $dataPost;
        $result = $client->post($uri, $params);
        $result = $result->getStatusCode() == Response::HTTP_OK;
        return $result;
    }

    /**
     * @throws GuzzleException
     */
    public function sendBatchMessage($deviceTokens, $data = [])
    {
        self::subscribeTopic($deviceTokens, 'mess');
        self::sendMessage($data, 'mess');
        self::unsubscribeTopic($deviceTokens, 'mess');
    }

    public function sendMessage($data, $topicName = null)
    {
        $body = [
            'to' => '/topics/' . $topicName,
            'data' => $data,
        ];
        $this->execute($this->urlSend, $body);
    }
}
