<?php

namespace App\Services\Shipping;

use App\Exceptions\CustomException;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class Ghtk
{
    const WEIGHT_DEFAULT = 0.5; //kg
    protected $prefix_url;
    protected $token;
    protected $client;

    protected static $instance = null;

    private function __construct()
    {
        $this->token = env('GHTK_TOKEN', '');
        $this->client = new Client();
        $this->prefix_url = env('GHTK_PREFIX_URL', '');
    }

    public static function getInstance(): Ghtk
    {
        if (!self::$instance)
            self::$instance = new self();
        return self::$instance;
    }

    /**
     *
     * @param array $products
     * @param string $id
     * @param string $pick_name
     * @param int $pick_money
     * @param string $pick_address_id
     * @param string $pick_address
     * @param string $pick_province
     * @param string $pick_district
     * @param string $pick_ward
     * @param string $pick_street
     * @param string $pick_tel
     * @param string $pick_email
     * @param string $name
     * @param string $address
     * @param string $province
     * @param string $district
     * @param string $ward
     * @param string $street
     * @param string $hamlet
     * @param string $tel
     * @param string $note
     * @param string|null $email
     * @param int $value
     * @param int $is_freeship
     * @param int $use_return_address
     * @param array $return_info
     * @param array $extra_info
     * @return ResponseInterface
     * @throws CustomException
     */
    public function createOrder(array $products, string $id, string $pick_name, int $pick_money, string $pick_address_id, string $pick_address, string $pick_province,
                                string $pick_district, string $pick_ward, string $pick_street, string $pick_tel, string $pick_email, string $name, string $address,
                                string $province, string $district, string $ward, string $street, string $hamlet, string $tel, string $note, string $email = null,
                                int $value = 0, int $is_freeship = 0, int $use_return_address = 0, array $return_info = [], array $extra_info = []
    ): ResponseInterface
    {
        try {
            $params['headers'] = [
                'Content-Type' => 'application/json',
                'token' => $this->token,
            ];
            $params['json'] = [
                'products' => $products,
                'order' => [
                    'id' => $id,
                    'pick_name' => $pick_name,
                    'pick_money' => $pick_money,
                    'pick_address' => $pick_address,
                    'pick_province' => $pick_province,
                    'pick_district' => $pick_district,
                    'pick_ward' => $pick_ward,
                    'pick_street' => $pick_street,
                    'pick_tel' => $pick_tel,
                    'pick_email' => $pick_email,
                    'name' => $name,
                    'address' => $address,
                    'province' => $province,
                    'district' => $district,
                    'ward' => $ward,
                    'street' => $street,
                    'hamlet' => $hamlet,
                    'tel' => $tel,
                    'note' => $note,
                    'email' => $email,
                    'use_return_address' => $use_return_address,
                    'is_freeship' => $is_freeship,
                    'value' => $value
                ],
            ];
            return $this->client->post($this->prefix_url . '/services/shipment/order', $params);
        } catch (\Exception $exception) {
            throw CustomException::makeBadRequest('Có lỗi xảy ra với GHTK');
        }
    }

    /**
     * @param string $order_id
     * @return ResponseInterface
     */
    public function cancelOrder(string $order_id): ResponseInterface
    {
        $params['headers'] = [
            'Content-Type' => 'application/json',
            'token' => $this->token,
        ];
        return $this->client->post($this->prefix_url . '/services/shipment/cancel/partner_id:' . $order_id, $params);
    }

    /**
     * @param string $term
     * @return ResponseInterface
     */
    public function getProductInformation(string $term): ResponseInterface
    {
        $params['headers'] = [
            'Content-Type' => 'application/json',
            'token' => $this->token,
        ];
        return $this->client->get($this->prefix_url . '/services/kho-hang/thong-tin-san-pham?term=' . $term, $params);
    }
}
