<?php

namespace App\Services\Shipping;

use App\Exceptions\CustomException;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class Ahamove
{
    const SERVICE_ID_DEFAULT = 'HAN-BIKE';
    const PAYMENT_METHOD_DEFAULT = 'CASH';
    protected $prefix_url;
    private $token;
    protected $client;
    private $api_key;
    private $refresh_token;
    const SERVICE_IDS = [
        'thanhphohochiminh' => 'SGN-BIKE',
        'thanhphohanoi' => 'HAN-BIKE'
    ];

    protected static $instance = null;

    private function __construct()
    {
        $this->client = new Client();
        $this->token = env('AHAMOVE_TOKEN', '');
        $this->api_key = env('AHAMOVE_API_KEY', '');
        $this->refresh_token = env('AHAMOVE_REFRESH_TOKEN', '');
        $this->prefix_url = env('AHAMOVE_PREFIX_URL', '');
    }

    public static function getInstance(): Ahamove
    {
        if (!self::$instance)
            self::$instance = new self();
        return self::$instance;
    }

    /**
     *
     * @param int $order_time
     * @param array $path
     * @param string $service_id
     * @param array $requests
     * @param string $payment_method
     * @param array $images
     * @param string $promo_code
     * @param string $remarks
     * @param int $idle_until
     * @param array $items
     * @param string|null $type
     * @param bool $need_optimize_route
     * @return ResponseInterface
     * @throws CustomException
     */
    public function createOrder(
        int $order_time, array $path, string $service_id, array $requests, string $payment_method, array $images, string $promo_code,
        string $remarks, int $idle_until, array $items, string $type = null, bool $need_optimize_route = null
    ): ResponseInterface
    {
        try {
            $params['headers'] = [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ];
            $params['form_params'] = [
                'token' => $this->token,
                'order_time' => $order_time,
                'path' => json_encode($path),
                'service_id' => $service_id,
                'requests' => json_encode($requests),
                'payment_method' => $payment_method,
                'images' => json_encode($images),
                'promo_code' => $promo_code,
                'remarks' => $remarks,
                'idle_until' => $idle_until,
                'items' => json_encode($items),
                'type' => $type,
                'need_optimize_route' => $need_optimize_route,
            ];
            return $this->client->post($this->prefix_url . '/v1/order/create', $params);
        } catch (\Exception $exception) {
            throw CustomException::makeBadRequest('Dịch vụ vận chuyển Ahamove không khả dụng tại vị trí của bạn');
//            $response = \GuzzleHttp\json_decode($exception->getResponse()->getBody()->getContents());
//            throw CustomException::makeBadRequest($response->description);
        }
    }

    public function getListOfService(string $city_id)
    {

    }
}
