<?php

namespace App\Services\Shipping;

use App\City;
use App\District;
use App\Ward;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class Lalamove
{
    protected $client;
    protected static $key;
    protected static $secret;
    protected static $baseURL;
    protected static $region;

    protected static $instance = null;

    private function __construct()
    {
        $this->client = new Client();
        self::$key = env('LALAMOVE_KEY', '');
        self::$secret = env('LALAMOVE_SECRET', '');
        self::$baseURL = env('LALAMOVE_BASE_URL', '');
        self::$region = '';
    }

    public static function getInstance(): Lalamove
    {
        if (!self::$instance)
            self::$instance = new self();
        return self::$instance;
    }

    protected static function generateHeaders(string $method, string $path, string $body): array
    {
        $time = time() * 1000;
        $rawSignature = "{$time}\r\n{$method}\r\n{$path}\r\n\r\n{$body}";
        $signature = hash_hmac("sha256", $rawSignature, self::$secret);
        $token = self::$key . ':' . $time . ':' . $signature;
        return [
            'Content-type' => 'application/json; charset=utf-8',
            'Authorization' => 'hmac ' . $token,
            'Accept' => 'application/json',
            'X-LLM-Market' => self::$region
        ];
    }
}
