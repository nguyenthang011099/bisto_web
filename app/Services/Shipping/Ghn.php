<?php

namespace App\Services\Shipping;

use App\City;
use App\District;
use App\Exceptions\CustomException;
use App\Ward;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class Ghn
{
    const WEIGHT_DEFAULT = 500; //cm
    const LENGTH_DEFAULT = 30;//cm
    const HEIGHT_DEFAULT = 3;//cm
    const WIDTH_DEFAULT = 25;//cm
    const PICK_SHIFT_DEFAULT = [2, 3];
    const SERVICE_ID_DEFAULT = 0;
    const SERVICE_TYPE_ID_DEFAULT = 2;
    const INSURANCE_VALUE_DEFAULT = 0;
    const PAYMENT_TYPE_ID_DEFAULT = 2;
    const REQUIRED_NOTE_DEFAULT = "KHONGCHOXEMHANG";
    protected $prefix_url;
    protected $token;
    protected $client;

    protected static $instance = null;

    private function __construct()
    {
        $this->token = env('GHN_TOKEN', '');
        $this->client = new Client();
        $this->prefix_url = env('GHN_PREFIX_URL', '');
    }

    public static function getInstance(): ?Ghn
    {
        if (!self::$instance)
            self::$instance = new self();
        return self::$instance;
    }

    /**
     *
     * @param array $items
     * @param int $shop_id
     * @param string $to_name
     * @param string $to_phone
     * @param string $to_address
     * @param string $to_ward_code
     * @param int $to_district_id
     * @param int $cod_amount
     * @param string $return_phone
     * @param string $return_address
     * @param int|null $return_district_id
     * @param string|null $return_ward_code
     * @param string $content
     * @param string $note
     * @param string|null $coupon
     * @param array $pick_shift
     * @param int $weight
     * @param int $length
     * @param int $width
     * @param int $height
     * @param int|null $pick_station_id
     * @param string $client_order_code
     * @param int $insurance_value
     * @param int $service_type_id
     * @param int $service_id
     * @param int $payment_type_id
     * @param string $required_note
     * @return ResponseInterface
     * @throws CustomException
     */
    public function createOrder(array $items, int $shop_id, string $to_name, string $to_phone, string $to_address, string $to_ward_code,
                                int $to_district_id, int $cod_amount = 0, string $return_phone = "", string $return_address = "", int $return_district_id = null, string $return_ward_code = null,
                                string $content = "", string $note = '', string $coupon = null, array $pick_shift = Ghn::PICK_SHIFT_DEFAULT,
                                int $weight = Ghn::WEIGHT_DEFAULT, int $length = Ghn::LENGTH_DEFAULT, int $width = Ghn::WIDTH_DEFAULT,
                                int $height = Ghn::HEIGHT_DEFAULT, int $pick_station_id = null, string $client_order_code = '', int $insurance_value = Ghn::INSURANCE_VALUE_DEFAULT,
                                int $service_type_id = Ghn::SERVICE_TYPE_ID_DEFAULT, int $service_id = Ghn::SERVICE_ID_DEFAULT,
                                int $payment_type_id = Ghn::PAYMENT_TYPE_ID_DEFAULT, string $required_note = Ghn::REQUIRED_NOTE_DEFAULT): ResponseInterface
    {
        try {
            $params['headers'] = [
                'Content-Type' => 'application/json',
                'ShopId' => $shop_id,
                'Token' => $this->token,
            ];
            $params['json'] = [
                'to_name' => $to_name,
                'to_phone' => $to_phone,
                'to_address' => $to_address,
                'to_ward_code' => $to_ward_code,
                'to_district_id' => $to_district_id,
                'return_phone' => $return_phone,
                'return_address' => $return_address,
                'return_district_id' => $return_district_id,
                'return_ward_code' => $return_ward_code,
                'client_order_code' => $client_order_code,
                'cod_amount' => $cod_amount,
                'content' => $content,
                'weight' => $weight,
                'length' => $length,
                'width' => $width,
                'height' => $height,
                'pick_station_id' => $pick_station_id,
                'insurance_value' => $insurance_value,
                'coupon' => $coupon,
                'service_type_id' => $service_type_id,
                'service_id' => $service_id,
                'payment_type_id' => $payment_type_id,
                'note' => $note,
                'required_note' => $required_note,
                'pick_shift' => $pick_shift,
                'order_value' => 0,
                'items' => $items
            ];
            return $this->client->post($this->prefix_url . '/shiip/public-api/v2/shipping-order/create', $params);
        } catch (\Exception $exception) {
            throw CustomException::makeBadRequest('Có lỗi xảy ra với Giao hàng nhanh');
        }
    }

    /**
     * @param int $shop_id
     * @param array $order_codes
     * @return ResponseInterface
     */
    public function cancelOrder(int $shop_id, array $order_codes): ResponseInterface
    {
        $params['headers'] = [
            'Content-Type' => 'application/json',
            'ShopId' => $shop_id,
            'Token' => $this->token,
        ];
        $params['json'] = [
            'order_codes' => $order_codes
        ];
        return $this->client->post($this->prefix_url . '/shiip/public-api/v2/switch-status/cancel', $params);
    }

    public function getProvince(): ResponseInterface
    {
        $params['headers'] = [
            'Content-Type' => 'application/json',
            'Token' => $this->token,
        ];
        return $this->client->get($this->prefix_url . '/shiip/public-api/master-data/province', $params);
    }

    public function getDistrictByProvinceId(int $province_id): ResponseInterface
    {
        $params['headers'] = [
            'Content-Type' => 'application/json',
            'Token' => $this->token,
        ];
        $params['json'] = [
            'province_id' => $province_id
        ];
        return $this->client->get($this->prefix_url . '/shiip/public-api/master-data/district', $params);
    }

    public function getWardByDistrictId(int $district_id): ResponseInterface
    {
        $params['headers'] = [
            'Content-Type' => 'application/json',
            'Token' => $this->token,
        ];
        $params['json'] = [
            'district_id' => $district_id
        ];
        return $this->client->get($this->prefix_url . '/shiip/public-api/master-data/ward?district_id', $params);
    }

    /**
     * @param string $city_id
     * @param string $district_id
     * @param string $ward_id
     * @return array|null
     */
    public function searchAddressByBisto(string $city_id, string $district_id, string $ward_id): ?array
    {
        try {
            $find_province = null;
            $find_district = null;
            $find_ward = null;
            $ghn_address_info = [];
            $city = City::where('id', $city_id)->first();
            $city_name = vn_to_lowercase($city->name);
            if (vn_to_lowercase($city->type) == 'tinh') {
                $city_name = str_replace('tinh', '', $city_name);
            } else {
                $city_name = str_replace('thanhpho', '', $city_name);
            }
            $res_1 = $this->getProvince();
            if ($res_1->getStatusCode() != 200) return null;
            $ghn_provinces = json_decode($res_1->getBody()->getContents())->data;
            for ($i = 0; $i < count($ghn_provinces); ++$i) {
                if (vn_to_lowercase($ghn_provinces[$i]->ProvinceName) == $city_name) {
                    $find_province = $ghn_provinces[$i];
                    $ghn_address_info['province'] = $find_province;
                    break;
                }
            }

            if (!is_null($find_province)) {
                $district = District::where('id', $district_id)->first();
                $district_name = vn_to_lowercase($district->name);
                $res_2 = $this->getDistrictByProvinceId($find_province->ProvinceID);
                if ($res_2->getStatusCode() != 200) return null;
                $ghn_districts = json_decode($res_2->getBody()->getContents())->data;
                for ($i = 0; $i < count($ghn_districts); ++$i) {
                    if (vn_to_lowercase($ghn_districts[$i]->DistrictName) == $district_name) {
                        $find_district = $ghn_districts[$i];
                        $ghn_address_info['district'] = $find_district;
                        break;
                    }
                }
            }

            if (!is_null($find_district)) {
                $ward = Ward::where('id', $ward_id)->first();
                $ward_name = vn_to_lowercase($ward->name);
                $res_3 = $this->getWardByDistrictId($find_district->DistrictID);
                if ($res_3->getStatusCode() != 200) return null;
                $ghn_wards = json_decode($res_3->getBody()->getContents())->data;
                for ($i = 0; $i < count($ghn_wards); ++$i) {
                    if (vn_to_lowercase($ghn_wards[$i]->WardName) == $ward_name) {
                        $find_ward = $ghn_wards[$i];
                        $ghn_address_info['ward'] = $find_ward;
                        break;
                    }
                }
            }
            return $ghn_address_info;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param int $district_id
     * @param string $ward_code
     * @param string $name
     * @param string $phone
     * @param string $address
     * @return ResponseInterface
     */
    public function createStore(int $district_id, string $ward_code, string $name, string $phone, string $address): ResponseInterface
    {
        $params['headers'] = [
            'Content-Type' => 'application/json',
            'Token' => $this->token,
        ];
        $params['json'] = [
            'district_id' => $district_id,
            'ward_code' => $ward_code,
            'name' => $name,
            'phone' => $phone,
            'address' => $address
        ];
        return $this->client->get($this->prefix_url . '/shiip/public-api/v2/shop/register', $params);
    }

    /**
     * @param string $order_code
     * @return ResponseInterface
     */
    public function getOrderDetail(string $order_code): ResponseInterface
    {
        $params['headers'] = [
            'Content-Type' => 'application/json',
            'Token' => $this->token,
        ];
        $params['json'] = [
            'order_code' => $order_code,
        ];
        return $this->client->get($this->prefix_url . '/shiip/public-api/v2/shipping-order/detail', $params);
    }

    public function getAllStore(int $offset = 0, int $limit = 50, string $client_phone = ''): ResponseInterface
    {
        $params['headers'] = [
            'Content-Type' => 'application/json',
            'Token' => $this->token,
        ];
        $params['json'] = [
            'offset' => $offset,
            'limit' => $limit,
            'client_phone' => $client_phone
        ];
        return $this->client->get($this->prefix_url . '/shiip/public-api/v2/shop/all', $params);
    }

    /**
     * @param int $id
     * @return mixed|null
     */
    public function getStoreById(int $id)
    {
        try {
            $all_store = json_decode(self::getAllStore(0, 50, '')->getBody()->getContents())->data->shops;;
            $key = array_search($id, array_column($all_store, '_id'));
            return $all_store[$key];
        } catch (\Exception $exception) {
            return null;
        }
    }
}
