<?php

namespace App\Services\ZaloPay;

use GuzzleHttp\Client as HttpClient;

/**
 * Class ZaloPayGateway
 */
class ZaloPayGateway
{
    protected $http_client;

    protected static $instance = null;

    private function __construct()
    {
        $this->http_client = new HttpClient([
            'base_uri' => config('payment.zalopay.base_uri'),
            'timeout'  => 20,
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept'     => 'application/json'
            ]
        ]);

    }

    public static function getInstance()
    {
        if (!self::$instance)
            self::$instance = new self();

        return self::$instance;
    }


    /**
     * @param int $user_id
     * @param string $transaction_code
     * @param int $amount
     * @param string $description
     * @param string $callback_url
     * @param array $item
     * @param string $embed_data
     * @return array|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createOrder(int $user_id, string $transaction_code, int $amount, string $description, string $callback_url, array $item = [], $embed_data = '')
    {
        $order = [
            'app_id' => config('payment.zalopay.app_id'),
            'app_user' => $user_id,
            'app_trans_id' => $transaction_code,
            'app_time' => (int)round(microtime(true) * 1000),
            'amount' => $amount,
            'description' => $description,
            'callback_url' => $callback_url,
            'item' => json_encode($item),
            'embed_data' => json_encode($embed_data),
        ];

        $hash_data = $order["app_id"] . "|" . $order["app_trans_id"] . "|" . $order["app_user"] . "|" . $order["amount"]
            . "|" . $order["app_time"] . "|" . $order["embed_data"] . "|" . $order["item"];
        $order["mac"] = hash_hmac("sha256", $hash_data, config('payment.zalopay.key1', ''));

        $response = $this->http_client->request('post', '/v2/create', [
            'form_params' => $order
        ]);

        if($response->getStatusCode() == 200){
            $data = json_decode($response->getBody());
            return $data->order_url;
        }
        return null;
    }




}
