<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;

class SecondSheetDebtExport implements FromCollection, WithHeadings,
    ShouldAutoSize, WithEvents, WithCustomStartCell, WithColumnFormatting
{
    private $noReceivedMoney;
    private $summaryNoReceivedMoney;
    private $info;

    function __construct($hasReceivedMoney, $summaryNoReceivedMoney, $info)
    {
        $this->noReceivedMoney = $hasReceivedMoney;
        $this->summaryNoReceivedMoney = $summaryNoReceivedMoney;
        $this->info = $info;
    }

    public function collection()
    {
        $orders = collect($this->noReceivedMoney)->map(function ($item) {
            return $item->only(['number', 'created_at', 'code',
                'buyer_name', 'payment_status', 'provider_name',
                'payment_method_name', 'status', 'total_money',
                'bisto_voucher', 'shop_voucher', 'real_money',
                'discount', 'fee_service', 'fixed_charge',
                'pay', 'refund', 'shipping_fee']);
        });
        return collect($orders);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $fontFamily = 'Times New Roman';
                $event->sheet->getDelegate()->mergeCells('B2:S2');
                $event->sheet->getDelegate()->setCellValue('B2', 'BẢNG ĐỐI SOÁT CÔNG NỢ - ' . $this->info->Name);
                $event->sheet->getDelegate()->getStyle('B2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('B2')->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle('B2')->getFont()->setName($fontFamily);

                $event->sheet->getDelegate()->mergeCells('B3:S3');
                $event->sheet->getDelegate()->setCellValue('B3', $this->info->SubTitle);
                $event->sheet->getDelegate()->getStyle('B3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('B3')->getFont()->setItalic(true);
                $event->sheet->getDelegate()->getStyle('B3')->getFont()->setName($fontFamily);

                $event->sheet->getDelegate()->mergeCells('B5:M5');
                $event->sheet->getDelegate()->setCellValue('B5', 'DOANH THU');
                $event->sheet->getDelegate()->getStyle('B5')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('B5')->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle('B5:M5')->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
                $event->sheet->getDelegate()->getStyle('B5')->getFont()->setName($fontFamily);

                $event->sheet->getDelegate()->mergeCells('N5:S5');
                $event->sheet->getDelegate()->setCellValue('N5', 'CHI PHÍ');
                $event->sheet->getDelegate()->getStyle('N5')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('N5')->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle('N5:S5')->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
                $event->sheet->getDelegate()->getStyle('N5')->getFont()->setName($fontFamily);

                $headerStyles = 'B6:S6';
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setSize(11);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setName($fontFamily);

                $count = count($this->noReceivedMoney)? count($this->noReceivedMoney) + 7 : 8;
                $styleTable = 'B6:S' . $count;
                $event->sheet->getDelegate()->getStyle($styleTable)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                $colFirst = 'A7:A' . ($count - 1);
                $event->sheet->getDelegate()->mergeCells($colFirst);
                $event->sheet->getDelegate()->setCellValue('A7', 'Chưa nhận tiền');
                $event->sheet->getDelegate()->getStyle('A7')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('A7')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle('A7')->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($styleTable)->getFont()->setName($fontFamily);

                $cellLast = 'C' . $count . ':I' . $count;
                $event->sheet->getDelegate()->mergeCells($cellLast);
                $event->sheet->getDelegate()->setCellValue('C' . $count, 'Cộng thành tiền');
                $event->sheet->getDelegate()->getStyle('C' . $count)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('C' . $count)->getFont()->setBold(true);

                $event->sheet->getDelegate()->setCellValue('J' . $count, '=TEXT(' . $this->summaryNoReceivedMoney->total_money . ',"#,##0")');
                $event->sheet->getDelegate()->setCellValue('K' . $count, '=TEXT(' . $this->summaryNoReceivedMoney->bisto_voucher . ',"#,##0")');
                $event->sheet->getDelegate()->setCellValue('L' . $count, '=TEXT(' . $this->summaryNoReceivedMoney->shop_voucher . ',"#,##0")');
                $event->sheet->getDelegate()->setCellValue('M' . $count, '=TEXT(' . $this->summaryNoReceivedMoney->real_money . ',"#,##0")');
                $event->sheet->getDelegate()->setCellValue('N' . $count, '=TEXT(' . $this->summaryNoReceivedMoney->discount . ',"#,##0")');
                $event->sheet->getDelegate()->setCellValue('O' . $count, '=TEXT(' . $this->summaryNoReceivedMoney->fee_service . ',"#,##0")');
                $event->sheet->getDelegate()->setCellValue('P' . $count, '=TEXT(' . $this->summaryNoReceivedMoney->fixed_charge . ',"#,##0")');
                $event->sheet->getDelegate()->setCellValue('Q' . $count, '=TEXT(' . $this->summaryNoReceivedMoney->pay . ',"#,##0")');
                $event->sheet->getDelegate()->setCellValue('R' . $count, '=TEXT(' . $this->summaryNoReceivedMoney->refund . ',"#,##0")');
                $event->sheet->getDelegate()->setCellValue('S' . $count, '=TEXT(' . $this->summaryNoReceivedMoney->shipping_fee . ',"#,##0")');
                $cols = ['J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];
                $this->setStylePosition($event, $count, $cols);
            },
        ];
    }

    private function setStylePosition($event, $count, $cols)
    {
        foreach ($cols as $col) {
            $event->sheet->getDelegate()->getStyle($col . $count)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
        }
    }

    public function startCell(): string
    {
        return 'B6';
    }

    public function columnFormats(): array
    {
        return [
            'J' => '#,###',
            'K' => '#,###',
            'L' => '#,###',
            'M' => '#,###',
            'N' => '#,###',
            'O' => '#,###',
            'P' => '#,###',
            'Q' => '#,###',
            'R' => '#,###',
            'S' => '#,###'
        ];
    }

    public function headings(): array
    {
        return [
            'STT', 'Thời gian', 'Mã đơn', 'Người mua', 'TM/CK',
            'Đơn vị V/C', 'Hình thức TT', 'Trạng thái', 'Số tiền',
            'BISTO trợ giá', 'Shop giảm giá', 'Thực thu', '% Chiết khấu',
            'Phí dịch vụ thu hộ', 'Phí cố định', 'Thanh toán', 'Hoàn tiền',
            'Phí V/C đổi trả'
        ];
    }
}
