<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class DebtShopExport implements WithMultipleSheets
{
    private $hasReceivedMoney;
    private $noReceivedMoney;
    private $summaryHasReceivedMoney;
    private $summaryNoReceivedMoney;
    private $summary;
    private $info;

    function __construct($hasReceivedMoney, $noReceivedMoney, $summaryHasReceivedMoney,
                         $summaryNoReceivedMoney, $summary, $info)
    {
        $this->hasReceivedMoney = $hasReceivedMoney;
        $this->noReceivedMoney = $noReceivedMoney;
        $this->summaryHasReceivedMoney = $summaryHasReceivedMoney;
        $this->summaryNoReceivedMoney = $summaryNoReceivedMoney;
        $this->summary = $summary;
        $this->info = $info;
    }

    public function sheets(): array
    {
        return [
            'Đã nhận tiền' => new FirstSheetDebtExport($this->hasReceivedMoney, $this->summaryHasReceivedMoney,
                $this->summary, $this->info),
            'Chưa nhận tiền' => new SecondSheetDebtExport($this->noReceivedMoney, $this->summaryNoReceivedMoney,
                $this->info)
        ];
    }
}
