<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;

class RevenueBistoTwoExport implements FromCollection, WithHeadings,
    ShouldAutoSize, WithEvents, WithCustomStartCell, WithColumnFormatting
{
    private $data;
    private $info;

    function __construct($data, $info)
    {
        $this->data = $data;
        $this->info = $info;
    }

    public function collection()
    {
        return collect($this->data);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $fontFamily = 'Times New Roman';
                $event->sheet->getDelegate()->mergeCells('A2:F2');
                $event->sheet->getDelegate()->setCellValue('A2', 'DOANH THU BÁN QUẢNG CÁO BANNER, THƯƠNG HIỆU, QUẢNG CÁO SẢN PHẨM');
                $event->sheet->getDelegate()->getStyle('A2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('A2')->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle('A2')->getFont()->setName($fontFamily);

                $event->sheet->getDelegate()->mergeCells('A3:F3');
                $event->sheet->getDelegate()->setCellValue('A3', $this->info->SubTitle);
                $event->sheet->getDelegate()->getStyle('A3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('A3')->getFont()->setItalic(true);
                $event->sheet->getDelegate()->getStyle('A3')->getFont()->setName($fontFamily);

                $headerStyles = 'A5:F5';
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setSize(11);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setName($fontFamily);

                $count = count($this->data) + 6;
                $styleTable = 'A5:F' . $count;
                $event->sheet->getDelegate()->getStyle($styleTable)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
                $event->sheet->getDelegate()->getStyle($styleTable)->getFont()->setName($fontFamily);

                $cellLast = 'A' . $count . ':E' . $count;
                $event->sheet->getDelegate()->mergeCells($cellLast);
                $event->sheet->getDelegate()->setCellValue('A' . $count, 'CỘNG');
                $event->sheet->getDelegate()->getStyle('A' . $count)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('A' . $count)->getFont()->setBold(true);

                $colTotal = 'F6:F' . ($count - 1);
                $event->sheet->getDelegate()->setCellValue('F' . $count, '=SUM(' . $colTotal . ')');
            },
        ];
    }

    public function startCell(): string
    {
        return 'A5';
    }

    public function columnFormats(): array
    {
        return [
            'F' => '#,###'
        ];
    }

    public function headings(): array
    {
        return [
            'Thời gian', 'Shop', 'Loại quảng cáo', 'Bắt đầu', 'Kết thúc', 'Số tiền'
        ];
    }
}
