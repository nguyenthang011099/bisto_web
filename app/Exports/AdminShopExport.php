<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AdminShopExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    public function collection()
    {
        return collect(User::getAdminShops());
    }

    public function headings(): array
    {
        return [
            'Họ và tên',
            'Email',
            'Số điện thoại',
            'Địa chỉ'
        ];
    }
}
