<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;

class PaymentExport implements FromCollection, WithHeadings,
    ShouldAutoSize, WithEvents, WithCustomStartCell, WithColumnFormatting
{
    private $data;

    private $info;

    function __construct($data, $info)
    {
        $this->data = $data;
        $this->info = $info;
    }

    public function collection()
    {
        $payment = $this->data->map(function ($item) {
            return $item->only(['number', 'created_at', 'code',
                'shop_name', 'buyer_name', 'status',
                'payment_method_name', 'total_money',
                'fee_service', 'note']);
        });
        return collect($payment);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $fontFamily = 'Times New Roman';
                $event->sheet->getDelegate()->mergeCells('B1:K1');
                $event->sheet->getDelegate()->setCellValue('B1', 'THANH TOÁN');
                $event->sheet->getDelegate()->getStyle('B1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('B1')->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle('B1')->getFont()->setName($fontFamily);

                $event->sheet->getDelegate()->mergeCells('B2:K2');
                $event->sheet->getDelegate()->setCellValue('B2', $this->info->SubTitle);
                $event->sheet->getDelegate()->getStyle('B2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('B2')->getFont()->setItalic(true);
                $event->sheet->getDelegate()->getStyle('B2')->getFont()->setName($fontFamily);

                $headerStyles = 'B4:K4';
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setSize(11);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setName($fontFamily);

                $count = count($this->data) + 6;
                $styleTable = 'B4:K' . $count;
                $event->sheet->getDelegate()->getStyle($styleTable)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
                $event->sheet->getDelegate()->getStyle($styleTable)->getFont()->setName($fontFamily);

                $cellLast = 'B' . $count . ':H' . $count;
                $event->sheet->getDelegate()->mergeCells($cellLast);
                $event->sheet->getDelegate()->setCellValue('B' . $count, 'CỘNG');
                $event->sheet->getDelegate()->getStyle('B' . $count)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('B' . $count)->getFont()->setBold(true);

                $colTotal = 'I5:I' . ($count - 1);
                $event->sheet->getDelegate()->setCellValue('I' . $count, '=SUM(' . $colTotal . ')');
                $colFee = 'J5:J' . ($count - 1);
                $event->sheet->getDelegate()->setCellValue('J' . $count, '=SUM(' . $colFee . ')');
            },
        ];
    }

    public function startCell(): string
    {
        return 'B4';
    }

    public function columnFormats(): array
    {
        return [
            'I' => '#,###',
            'J' => '#,###'
        ];
    }

    public function headings(): array
    {
        return [
            'STT', 'Thời gian', 'Mã đơn', 'Shop', 'Người nhận', 'Trạng thái', 'Phương thức TT',
            'Giá trị đơn hàng', 'Phí thanh toán', 'Ghi chú'
        ];
    }
}
