<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;

class RevenueBistoOneExport implements FromCollection, WithHeadings,
    ShouldAutoSize, WithEvents, WithCustomStartCell, WithColumnFormatting
{
    private $data;
    private $info;

    function __construct($data, $info)
    {
        $this->data = $data;
        $this->info = $info;
    }

    public function collection()
    {
        return collect($this->data);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $fontFamily = 'Times New Roman';
                $event->sheet->getDelegate()->mergeCells('B2:J2');
                $event->sheet->getDelegate()->setCellValue('B2', 'DOANH THU TRÊN HỆ THỐNG BISTO');
                $event->sheet->getDelegate()->getStyle('B2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('B2')->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle('B2')->getFont()->setName($fontFamily);

                $event->sheet->getDelegate()->mergeCells('B3:J3');
                $event->sheet->getDelegate()->setCellValue('B3', $this->info->SubTitle);
                $event->sheet->getDelegate()->getStyle('B3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('B3')->getFont()->setItalic(true);
                $event->sheet->getDelegate()->getStyle('B3')->getFont()->setName($fontFamily);

                $headerStyles = 'A5:U5';
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setSize(11);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setName($fontFamily);

                $count = count($this->data) + 6;
                $styleTable = 'A5:U' . $count;
                $event->sheet->getDelegate()->getStyle($styleTable)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
                $event->sheet->getDelegate()->getStyle($styleTable)->getFont()->setName($fontFamily);

                $cellLast = 'B' . $count . ':I' . $count;
                $event->sheet->getDelegate()->mergeCells($cellLast);
                $event->sheet->getDelegate()->setCellValue('B' . $count, 'CỘNG');
                $event->sheet->getDelegate()->getStyle('B' . $count)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('B' . $count)->getFont()->setBold(true);

                $rowLast = $count - 1;
                $event->sheet->getDelegate()->setCellValue('J' . $count, '=SUM(' . $this->convertCell('J', $rowLast) . ')');
                $event->sheet->getDelegate()->setCellValue('K' . $count, '=SUM(' . $this->convertCell('K', $rowLast) . ')');
                $event->sheet->getDelegate()->setCellValue('L' . $count, '=SUM(' . $this->convertCell('L', $rowLast) . ')');
                $event->sheet->getDelegate()->setCellValue('M' . $count, '=SUM(' . $this->convertCell('M', $rowLast) . ')');
                $event->sheet->getDelegate()->setCellValue('N' . $count, '=SUM(' . $this->convertCell('N', $rowLast) . ')');
                $event->sheet->getDelegate()->setCellValue('O' . $count, '=SUM(' . $this->convertCell('O', $rowLast) . ')');
                $event->sheet->getDelegate()->setCellValue('P' . $count, '=SUM(' . $this->convertCell('P', $rowLast) . ')');
                $event->sheet->getDelegate()->setCellValue('Q' . $count, '=SUM(' . $this->convertCell('Q', $rowLast) . ')');
                $event->sheet->getDelegate()->setCellValue('R' . $count, '=SUM(' . $this->convertCell('R', $rowLast) . ')');
                $event->sheet->getDelegate()->setCellValue('S' . $count, '=SUM(' . $this->convertCell('S', $rowLast) . ')');
            },
        ];
    }

    public function startCell(): string
    {
        return 'A5';
    }

    private function convertCell($col, $rowLast)
    {
        return $col . '6:' . $col . $rowLast;
    }

    public function columnFormats(): array
    {
        return [
            'J' => '#,###',
            'K' => '#,###',
            'L' => '#,###',
            'M' => '#,###',
            'N' => '#,###',
            'O' => '#,###',
            'P' => '#,###',
            'Q' => '#,###',
            'R' => '#,###',
            'S' => '#,###'
        ];
    }

    public function headings(): array
    {
        return [
            'STT', 'Mã đơn', 'Ngày tháng', 'Shop', 'Người mua', 'Hình thức TT',
            'Đ/v TT', 'Đ/v V/C', 'Trạng thái', 'Giá đề xuất', 'Bisto trợ giá',
            'Shop giảm giá', 'Giá gốc (Đã giảm)', 'Phí dịch vụ thu hộ', 'Phí vận chuyển',
            'Tổng giá trị đơn hàng', 'Phí cố định', 'Phí thanh toán', 'Hoàn tiền',
            'Chiết khấu', 'Ghi chú'
        ];
    }
}
