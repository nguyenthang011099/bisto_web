<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;

class SubsidyBistoExport implements FromCollection, WithHeadings,
    ShouldAutoSize, WithEvents, WithCustomStartCell, WithColumnFormatting
{
    private $data;
    private $info;

    function __construct($data, $info)
    {
        $this->data = $data;
        $this->info = $info;
    }

    public function collection()
    {
        return collect($this->data);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $fontFamily = 'Times New Roman';
                $event->sheet->getDelegate()->mergeCells('B2:L2');
                $event->sheet->getDelegate()->setCellValue('B2', 'BISTO TRỢ GIÁ HỖ TRỢ THƯƠNG HIỆU XÚC TIẾN THƯƠNG MẠI');
                $event->sheet->getDelegate()->getStyle('B2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('B2')->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle('B2')->getFont()->setName($fontFamily);

                $event->sheet->getDelegate()->mergeCells('B3:L3');
                $event->sheet->getDelegate()->setCellValue('B3', $this->info->SubTitle);
                $event->sheet->getDelegate()->getStyle('B3')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('B3')->getFont()->setItalic(true);
                $event->sheet->getDelegate()->getStyle('B3')->getFont()->setName($fontFamily);

                $headerStyles = 'B5:L5';
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setSize(11);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($headerStyles)->getFont()->setName($fontFamily);

                $count = count($this->data) + 6;
                $styleTable = 'B5:L' . $count;
                $event->sheet->getDelegate()->getStyle($styleTable)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
                $event->sheet->getDelegate()->getStyle($styleTable)->getFont()->setName($fontFamily);

                $cellLast = 'C' . $count . ':H' . $count;
                $event->sheet->getDelegate()->mergeCells($cellLast);
                $event->sheet->getDelegate()->setCellValue('C' . $count, 'CỘNG');
                $event->sheet->getDelegate()->getStyle('C' . $count)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('C' . $count)->getFont()->setBold(true);

                $colTotalOrigin = 'I6:I' . ($count - 1);
                $event->sheet->getDelegate()->setCellValue('I' . $count, '=SUM(' . $colTotalOrigin . ')');
                $event->sheet->getDelegate()->getStyle('I' . $count)->getFont()->setBold(true);

                $colSubsidy = 'J6:J' . ($count - 1);
                $event->sheet->getDelegate()->setCellValue('J' . $count, '=SUM(' . $colSubsidy . ')');
                $event->sheet->getDelegate()->getStyle('J' . $count)->getFont()->setBold(true);

                $colReal = 'K6:K' . ($count - 1);
                $event->sheet->getDelegate()->setCellValue('K' . $count, '=SUM(' . $colReal . ')');
                $event->sheet->getDelegate()->getStyle('K' . $count)->getFont()->setBold(true);
            },
        ];
    }

    public function startCell(): string
    {
        return 'B5';
    }

    public function columnFormats(): array
    {
        return [
            'I' => '#,###',
            'J' => '#,###',
            'K' => '#,###'
        ];
    }

    public function headings(): array
    {
        return [
            'STT', 'Mã trợ giá', 'Mã đơn hàng', 'Thời gian', 'Shop', 'Người mua',
            'Trạng thái', 'Giá gốc', 'Trợ giá', 'Thực thu', 'Ghi chú'
        ];
    }
}
