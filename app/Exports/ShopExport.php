<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ShopExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    private $is_without_branch;

    public function __construct($is_without_branch)
    {
        $this->is_without_branch = $is_without_branch;
    }

    public function collection(): \Illuminate\Support\Collection
    {
        return collect(User::getShopsWithOption($this->is_without_branch));
    }

    public function headings(): array
    {
        if ($this->is_without_branch == 0)
            return [
                'Họ và tên',
                'Email',
                'Số điện thoại',
                'Địa chỉ'
            ];
        else if ($this->is_without_branch == 1) return [
            'Họ và tên',
            'Email',
            'Số điện thoại',
            'CMND',
            'Mã số thuế',
            'Địa chỉ',
        ]; else return [
            'Họ và tên',
            'Email',
            'Số điện thoại',
            'Địa chỉ'
        ];
    }
}
