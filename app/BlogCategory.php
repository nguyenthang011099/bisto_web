<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BlogCategory extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên
     * user_id: người tạo
     * deleted_at: ngày xóa
     */
    protected $fillable = [
        'name', 'created_at', 'updated_at', "slug"
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_blog_categories';
}
