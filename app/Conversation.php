<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    public $timestamps = false; //set time to false

    /**
     * user_1: người tạo cuộc trò chuyện
     * deleted_at: thời điểm xóa tin nhắn
     * name: tên người tạo trò chuyện
     */
    protected $fillable = [
        'user_1', 'deleted_at', 'name'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_conversations';
}
