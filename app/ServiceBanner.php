<?php

namespace App;

use App\Exceptions\CustomException;
use Illuminate\Database\Eloquent\Model;

class ServiceBanner extends Model
{
    public $timestamps = false; //set time to false
    /**
     */
    protected $fillable = [
        'day', 'name', 'price', 'user_id'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_service_banners';

    /**
     * @throws CustomException
     */
    public static function insertServiceBanner($day, $name, $price, $user_id)
    {
        $data = new ServiceBanner();
        if (!$day || !$name || !$price) {
            throw  CustomException::makeBadRequest('Dữ liệu không hợp lệ');
        }
        $data->day = $day;
        $data->name = $name;
        $data->price = $price;
        $data->user_id = $user_id;
        $result = $data->save();

        if ($result) return true;
        return false;
    }

    /**
     * @throws CustomException
     */
    public static function updateServiceBanner($id, $day, $name, $price, $user_id)
    {
        $data = ServiceBanner::where('id', $id)->where('user_id', $user_id)->first();
        if (is_null($data)) {
            throw  CustomException::makeBadRequest('Không tìm thấy dịch vụ nào');
        }
        if (!$day || !$name || !$price) {
            throw  CustomException::makeBadRequest('Dữ liệu không hợp lệ');
        }
        $data->day = $day;
        $data->name = $name;
        $data->price = $price;
        $result = $data->save();
        if ($result) return true;
        return false;
    }

    /**
     * @throws CustomException
     */
    public static function deleteServiceBanner($id, $user_id)
    {
        $data = ServiceBanner::where('id', $id)->where('user_id', $user_id)->first();
        if (is_null($data)) {
            throw  CustomException::makeBadRequest('Không tìm thấy dịch vụ nào');
        }
        $data->delete();
        return;
    }
}
