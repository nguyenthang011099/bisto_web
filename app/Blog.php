<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Blog extends Model
{
    public $timestamps = false; //set time to false

    const TYPE_NORMAL = 'normal';
    const TYPE_HOT = 'hot';
    const PER_PAGE = 6;
    const SLUG_HOT_PAGE = "tin-tuc-noi-bat";
    /**
     * name: tên
     * image_id: id ảnh
     * user_id: người tạo
     * deleted_at: ngày xóa
     */
    protected $fillable = [
        'title', 'content', 'type', 'description', 'slug', 'image_id', 'user_id', 'blog_category_id', 'created_at', 'updated_at', 'deleted_at'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_blogs';
}
