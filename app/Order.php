<?php

namespace App;

class Order extends BaseOrder
{
    public $timestamps = false; //set time to false

    /**
     * code: mã code của order
     * user_id: id của người mua
     * seller_id: id của chủ shop (id trong bảng user)
     * status: 1-người mua đang mua hàng, 2-người mua tạo đơn, 3-người bán xác nhận đơn,
     *         4-đơn hàng đang giao, 5-người mua đã nhận được hàng, 6-người mua hủy hàng,
     *         7-người mua trả hàng, 8-người mua đổi hàng, 9-đơn thành công
     * note: note của người mua
     * payment_method: thanh toán tiền mặt hoặc online
     * coupon_id: mã voucher
     */
    const NGUOI_MUA_DANG_MUA_HANG = 1;
    const NGUOI_MUA_DA_TAO_DON = 2;
    const NGUOI_BAN_XAC_NHAN_DON = 3;
    const NGUOI_BAN_HUY_DON = 10;
    const DON_HANG_DANG_GIAO = 4;
    const NGUOI_MUA_DA_NHAN_HANG = 5;
    const NGUOI_MUA_HUY_DON = 6;
    const NGUOI_MUA_TRA_HANG = 7;
    const NGUOI_MUA_DOI_HANG = 8;
    const DON_THANH_CONG = 9;
    const DA_THANH_TOAN = 1;
    const CHUA_THANH_TOAN = 0;
    protected $fillable = [
        'code', 'user_id', 'seller_id', 'status',
        'note', 'payment_method', 'coupon_id', 'total', 'payment_status'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_orders';

    public function convertOrder($order, $item, $index)
    {
        parent::checkVoucher($order);
        $item->number = $index;
        unset($item->created_at);
        $item->created_at = date("H:m d/m/Y", strtotime($order->created_at));
        $item->code = $order->code;
        $item->buyer_name = User::getFullName($order->user_id);
        if ($order->payment_status) {
            $item->payment_status = 'CK';
        } else {
            $item->payment_status = 'TM';
        }

        $item->provider_name = '';
        if ($order->shipping_fee) {
            $item->provider_name = $order->provider_name;
        }

        $item->payment_method_name = parent::showPaymentMethod($order->payment_method);
        $item->status = $order->status;
        $item->total_money = $order->total_money;

        $item->bisto_voucher = $order->bisto_voucher;
        $item->shop_voucher = $order->shop_voucher;

        $item->real_money = ($order->total_money + 2000);
        $item->discount = 5;
        $item->fee_service = parent::getFeeCollection();
        $item->fixed_charge = parent::getFeeCharge($item->total_money);
        $item->pay = 3000;
        $item->refund = 0;
        $item->shipping_fee = 0;
        if ($order->shipping_fee) {
            $item->shipping_fee = $order->shipping_fee;
        }
        $item->color = $order->color;
        $item->status_code = $order->status_code;
        return $item;
    }

    public function getOrderById($order)
    {
        list($item, $products) = parent::getDetailOrder($order->id);
        $this->convertOrder($item, $order, 1);

        $shipping_buyer = ShippingInfo::where('user_id', $order->user_id)
            ->where('is_default', 1)
            ->select('city_id', 'district_id', 'ward_id', 'address')
            ->first();
        $shipping_buyer->full_name = User::where('id', $order->user_id)->value('fullname');

        $shipping_shop = ShippingInfo::where('user_id', $order->seller_id)
            ->where('is_default', 1)
            ->select('city_id', 'district_id', 'ward_id', 'address')
            ->first();
        $shipping_shop->full_name = User::where('id', $order->seller_id)->value('fullname');

        $shipping_buyer->address = User::getAddress($shipping_buyer);
        $shipping_shop->address = User::getAddress($shipping_shop);

        $order_return = OrderReturn::showOrderReturn($order->id);
        $order_log = null;

        return response()->json(['shipping_buyer' => $shipping_buyer,
            'shipping_shop' => $shipping_shop,
            'order' => $order, 'products' => $products,
            'order_return' => $order_return,
            'order_log' => $order_log]);
    }

    public function customOrdersExcel($orders)
    {
        $hasReceivedMoney = array();
        $summaryHasReceivedMoney = (object)array(
            'total_money' => 0,
            'bisto_voucher' => 0,
            'shop_voucher' => 0,
            'real_money' => 0,
            'discount' => 0,
            'fee_service' => 0,
            'fixed_charge' => 0,
            'pay' => 0,
            'refund' => 0,
            'shipping_fee' => 0
        );
        $noReceivedMoney = array();
        $summaryNoReceivedMoney = clone $summaryHasReceivedMoney;
        $summary = array();

        foreach ($orders as $item) {
            if ($item->provider_name == ShippingOrder::AHAMOVE && $item->payment_status == 'TM') {
                array_push($hasReceivedMoney, $item);
                $this->calculateFieldMoney($summaryHasReceivedMoney, $item);
            } else {
                array_push($noReceivedMoney, $item);
                $this->calculateFieldMoney($summaryNoReceivedMoney, $item);
            }
        }
        $summary = $this->calcalateSummary($summary, $summaryHasReceivedMoney, $summaryNoReceivedMoney);
        return array($hasReceivedMoney, $noReceivedMoney, $summaryHasReceivedMoney, $summaryNoReceivedMoney, $summary);
    }

    private function calculateFieldMoney($summaryMoney, $item)
    {
        $summaryMoney->total_money += $item->total_money;
        $summaryMoney->bisto_voucher += $item->bisto_voucher;
        $summaryMoney->shop_voucher += $item->shop_voucher;
        $summaryMoney->real_money += $item->real_money;
        $summaryMoney->discount += $item->discount;
        $summaryMoney->fee_service += $item->fee_service;
        $summaryMoney->fixed_charge += $item->fixed_charge;
        $summaryMoney->pay += $item->pay;
        $summaryMoney->refund += $item->refund;
        $summaryMoney->shipping_fee += $item->shipping_fee;
    }

    private function calcalateSummary($summary, $summaryHasReceivedMoney, $summaryNoReceivedMoney)
    {
        $summary[0] = $summaryHasReceivedMoney->total_money + $summaryNoReceivedMoney->total_money;
        $summary[3] = $summaryHasReceivedMoney->real_money;
        $summary[4] = $summaryNoReceivedMoney->real_money;
        $summary[5] = $summaryHasReceivedMoney->bisto_voucher + $summaryNoReceivedMoney->bisto_voucher;
        $summary[6] = $summaryHasReceivedMoney->refund + $summaryNoReceivedMoney->refund;
        $summary[7] = $summaryHasReceivedMoney->shop_voucher + $summaryNoReceivedMoney->shop_voucher;
        $summary[8] = $summaryHasReceivedMoney->fixed_charge + $summaryNoReceivedMoney->fixed_charge;
        $summary[9] = $summaryHasReceivedMoney->fee_service + $summaryNoReceivedMoney->fee_service;
        $summary[10] = $summaryHasReceivedMoney->pay + $summaryNoReceivedMoney->pay;
        $summary[11] = 0;
        $summary[12] = $summaryHasReceivedMoney->shipping_fee + $summaryNoReceivedMoney->shipping_fee;
        $summary[13] = 0;
        $summary[1] = $summary[6] + $summary[7] + $summary[8] + $summary[10] + $summary[11] + $summary[12];
        $summary[2] = $summary[0] - $summary[1];
        $summary[14] = $summary[2] - $summary[3] + $summary[13];
        return $summary;
    }
}
