<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RelUserPostCommentLike extends Model
{
    public $timestamps = false; //set time to false

    /**
     */
    protected $fillable = [
        'user_id', 'comment_id', 'post_id', 'created_at', 'updated_at'
    ];

    protected $primaryKey = 'id';
    protected $table = 'rel_user_post_comment_like';
}
