<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên size
     */
    protected $fillable = [
        'name'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_sizes';
}
