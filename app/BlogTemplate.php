<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BlogTemplate extends Model
{
    public $timestamps = false; //set time to false

    /**
     * content:
     * user_id: người tạo
     * deleted_at: ngày xóa
     */
    protected $fillable = [
        'content', 'created_at', 'updated_at'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_blog_templates';
}
