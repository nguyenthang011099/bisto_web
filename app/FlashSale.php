<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlashSale extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên chương trình flash sale
     * description: mô tả chương trình
     * start_time: thời điểm bắt đầu
     * end_time: thời điểm kết thúc
     * type: giảm theo tiền hoặc phần trăm
     * amount: giá giảm còn...
     * user_id: người tạo ra flash sale
     * image_id: id hình ảnh
     * status: 0-Ẩn, 1-Hiển thị
     */
    protected $fillable = [
        'name', 'description', 'start_time', 'end_time',
        'status', 'amount', 'user_id', 'image_id', 'type'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_flash_sales';
}
