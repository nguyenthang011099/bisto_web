<?php

namespace App;

class Subsidy extends BaseOrder
{
    public function convertOrder($order, $item, $index) {
        parent::checkVoucher($order);
        unset($item->created_at);
        $item->number = $index;
        $item->coupon_code = Coupon::getCouponCode($order->coupon_id);
        $item->code = $order->code;

        $item->created_at = date("H:m d/m/Y", strtotime($order->created_at));
        $item->shop_name = Shop::getShopName($order->seller_id);
        $item->buyer_name = User::getFullName($order->user_id);

        $item->status = $order->status;

        $item->origin_total = ($order->bisto_voucher + $order->total_money);
        $item->bisto_voucher = $order->bisto_voucher;
        $item->total_money = $order->total_money;
        $item->note = $order->note;

        unset($item->id);
        return $item;
    }
}
