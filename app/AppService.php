<?php

namespace App;

use App\Exceptions\CustomException;
use Illuminate\Database\Eloquent\Model;

class AppService extends Model
{
    public $timestamps = false; //set time to false

    /**
     * minimum_order_amount: số tiền tối thiểu trên đơn hàng
     * amount_service: tiền phí dịch vụ
     * type: 1-số tiền, 2-phần trăm
     */
    protected $fillable = [
        'amount', 'type'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_service_shop';

    public static function insertAppServices($service_amount, $type)
    {
        $data = new AppService();
        $data->amount = $service_amount;
        $data->type = $type;
        return $data->save();
    }

    /**
     * @throws CustomException
     */
    public static function updateAppServices($service_amount, $type)
    {
        $data = AppService::first();
        if (is_null($data)) throw new CustomException('Không tìm thấy bản ghi phí dịch vụ', 400);
        $data->amount = $service_amount;
        $data->type = $type;
        return $data->save();
    }

    public static function getAppServices($money)
    {
        $data = AppService::select('minimum_order_amount', 'service_amount')->first();
        return $money + $data->service_amount;
    }
}
