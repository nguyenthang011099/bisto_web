<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RelTag extends Model
{
    public $timestamps = false; //set time to false

    /**
     */
    protected $fillable = [
        'blog_id', 'tag_id', 'created_at', 'updated_at'
    ];

    protected $primaryKey = 'id';
    protected $table = 'rel_tag';
}
