<?php

namespace App\Exceptions;

use Exception;

class CustomException extends Exception
{

    public $message = 'Có lỗi xảy ra';
    public $statusCode = 500;

    function __construct($message, $statusCode)
    {
        parent::__construct();
        $this->message = $message;
        $this->statusCode = $statusCode;
    }

    public static function makeServerError($message): CustomException
    {
        return new CustomException($message, 500);
    }

    public static function makeBadRequest($message = 'Yêu cầu không hợp lệ'): CustomException
    {
        return new CustomException($message, 400);
    }

    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {
    }

    public function render($request): \Illuminate\Http\JsonResponse
    {
        return response()->json(['success' => 0, 'message' => $this->message], $this->statusCode);
    }
}
