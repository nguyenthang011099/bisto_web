<?php

namespace App;

use App\Exceptions\CustomException;
use Illuminate\Database\Eloquent\Model;

class PaymentService extends Model
{
    public $timestamps = false; //set time to false

    /**
     * unit_payment: đơn vị thanh toán
     * plus_percent: phần trăm theo đơn hàng
     * plus_amount: tiền thêm
     */
    protected $fillable = [
        'unit_payment', 'plus_percent', 'plus_amount'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_payment_services';

    public static function insertPaymentServices($unit_payment, $plus_percent, $plus_amount)
    {
        $data = new PaymentService();
        $data->unit_payment = $unit_payment;
        $data->plus_percent = $plus_percent;
        $data->plus_amount = $plus_amount;
        $result = $data->save();
        if ($result) return true;
        return false;
    }

    /**
     * @throws CustomException
     */
    public static function updatePaymentServices($id, $unit_payment, $plus_percent, $plus_amount)
    {
        $data = PaymentService::where('id', $id)->first();
        if (is_null($data)) throw new CustomException('Không tìm thấy bản ghi phí thanh toán', 400);

        $data->unit_payment = $unit_payment;
        $data->plus_percent = $plus_percent;
        $data->plus_amount = $plus_amount;
        $result = $data->save();

        if ($result) return true;
        return false;
    }

    public static function getFeePayment($money, $paymentMethod)
    {
        $payment = PaymentService::where('id', $paymentMethod)->first();
        if (!$payment) return array($money, 0, 0);
        $percent = $payment->plus_percent ?? 0;
        $amount = $payment->plus_amount ?? 0;

        $feeService = ($money * $percent) / 100 + $amount;
        $note = ($percent != 0) ? ($percent . '%') : '';
        if ($amount != 0) {
            $note = $note . ' + ' . $amount . 'đ';
        }
        return array($money + $feeService, $feeService, $note);
    }
}
