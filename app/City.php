<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên tỉnh, thành phố
     * type: Tỉnh, Thành phố
     */
    protected $fillable = [
        'name', 'type'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_cities';
}
