<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserConversation extends Model
{
    public $timestamps = false; //set time to false

    /**
     * user_id: những người đc chat
     * conversation_id: cuộc trò chuyện ...
     * deleted_at: thời điểm xóa cuộc trò chuyện
     */
    protected $fillable = [
        'user_id', 'conversation_id', 'deleted_at'
    ];

    protected $primaryKey = 'id';
    protected $table = 'rel_user_conversations';
}
