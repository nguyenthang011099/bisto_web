<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên của mã giảm
     * code: mã code
     * number_voucher: số lượng mã giảm được áp dụng
     * type: 0-giảm theo phần trăm, 1-giảm theo tiền
     * price: phần trăm giảm hoặc số tiền giảm
     * start_at: thời điểm bắt đầu
     * end_at: thời điểm kết thúc
     * user_id: người tạo mã giảm (id trong bảng user)
     * required_minimum_price: tiền tối thiểu
     * max_reduce_price: tiền giảm tối đa
     */
    protected $fillable = [
        'name', 'code', 'number_voucher', 'type', 'price',
        'start_at', 'end_at', 'user_id', 'required_minimum_price', 'max_reduce_price'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_coupons';

    public static function getCouponCode($couponId)
    {
        $coupon = Coupon::where('id', $couponId)->first();
        if ($coupon) {
            return $coupon->code;
        }
        return '';
    }
}
