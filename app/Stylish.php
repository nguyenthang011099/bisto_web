<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stylish extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên phong cách
     * parent_id: phong cách lớn: Nam hoặc Nữ
     */
    protected $fillable = [
        'name', 'type', 'icon_id'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_stylists';
}
