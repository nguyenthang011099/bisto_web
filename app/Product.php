<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên sản phẩm
     * price: giá cả
     * description: mô tả sản phẩm
     * discount: giảm giá sản phẩm
     * self_status: 1-kích hoat, -1-ẩn
     * admin_status: -1 admin block , 1 admin unblock
     * user_id: id của người tạo sản phẩm
     * image_ids: id của ảnh
     * stylish_id: id của stylish
     */
    protected $fillable = [
        'name', 'price', 'description', 'discount', 'self_status', 'admin_status', 'user_id',
        'start_at', 'end_at', 'created_at', 'updated_at', 'stylish_ids', 'image_ids', 'category_id'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_products';

    public static function getDetailProduct($productId)
    {
        $product = Product::where('id', $productId)
            ->select('name', 'image_ids')
            ->first();
        if (is_null($product)) return;

        $imageIds = explode(",", $product->image_ids);
        $index = rand(0, count($imageIds) - 1);
        $sourceUrl = File::where('id', $imageIds[$index])->value('url');
        $product->url = $sourceUrl;
        return $product;
    }
}
