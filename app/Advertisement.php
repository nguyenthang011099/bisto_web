<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Advertisement extends Model
{
    public $timestamps = false; //set time to false
    /**
     * image_id: id ảnh
     * deleted_at: ngày xóa
     */
    protected $fillable = [
        'image_id', 'url', 'created_at', 'updated_at', 'deleted_at'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_advertisements';
}
