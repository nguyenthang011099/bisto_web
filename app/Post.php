<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    public $timestamps = false; //set time to false

    const STATUS_ACCEPT = 1;
    const STATUS_REJECT = -1;
    const STATUS_PENDING = 0;
    /**
     */
    protected $fillable = [
        'user_id', 'content', 'title', 'description', 'created_at', 'updated_at', 'admin_status', 'self_status'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_posts';
}
