<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopRate extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên size
     */
    protected $fillable = [
        'rate', 'user_id', 'shop_id'
    ];

    protected $primaryKey = 'id';
    protected $table = 'rel_user_shop_rates';

    public static function getAvgRates($id)
    {
        $rates = ShopRate::where('shop_id', $id)
            ->where('rate', '<>', null)
            ->select('shop_id', 'rate', 'user_id')
            ->get();
        $avg_rates = 0;
        foreach ($rates as $rate) {
            $avg_rates += $rate->rate;
        }

        $number_rates = count($rates);
        if ($number_rates) {
            $avg_rates /= $number_rates;
        }
        return $avg_rates;
    }
}
