<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Brand extends Model
{
    public $timestamps = false; //set time to false

    /**
     * name: tên thương hiệu
     * image_id: id ảnh
     * user_id: người tạo
     * deleted_at: ngày xóa
     */
    protected $fillable = [
        'name', 'image_id', 'user_id', 'created_at', 'updated_at', 'deleted_at'
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_brands';

    /* Lấy ra brand nửa đầu tháng */
    public static function getBrandsFirstMonth($data)
    {
        $date1 = date($data['year'] . '-' . $data['month'] . '-01');
        $date2 = date($data['year'] . '-' . $data['month'] . '-15');
        return self::getBrandsByDay($date1, $date2);
    }

    /* Lấy ra brand nửa cuối tháng */
    public static function getBrandsLastMonth($data)
    {
        $date1 = date($data['year'] . '-' . $data['month'] . '-15');
        $date2 = date($data['year'] . '-' . $data['month'] . '-t');
        return self::getBrandsByDay($date1, $date2);
    }

    /* Lấy ra brand của cả tháng */
    public static function getBrandsOfMonth($data)
    {
        $date1 = date($data['year'] . '-' . $data['month'] . '-01');
        $date2 = date($data['year'] . '-' . $data['month'] . '-t');
        return self::getBrandsByDay($date1, $date2);
    }

    /* Lấy ra brand nửa đầu quý */
    public static function getBrandsFirstPrecious($data)
    {
        $precious = $data['precious'];
        $lastMonthOfPrecious = 3 * $precious;

        $date1 = date($data['year'] . '-' . ($lastMonthOfPrecious - 2) . '-01');
        $date2 = date($data['year'] . '-' . ($lastMonthOfPrecious - 1) . '-15');
        return self::getBrandsByDay($date1, $date2);
    }

    /* Lấy ra brand nửa cuối quý */
    public static function getBrandsLastPrecious($data)
    {
        $precious = $data['precious'];
        $lastMonthOfPrecious = 3 * $precious;

        $date1 = date($data['year'] . '-' . ($lastMonthOfPrecious - 1) . '-15');
        $date2 = date($data['year'] . '-' . $lastMonthOfPrecious . '-t');
        return self::getBrandsByDay($date1, $date2);
    }

    /* Lấy ra brand của cả quý */
    public static function getBrandsOfPrecious($data)
    {
        $precious = $data['precious'];
        $lastMonthOfPrecious = 3 * $precious;

        $date1 = date($data['year'] . '-' . ($lastMonthOfPrecious - 2) . '-01');
        $date2 = date($data['year'] . '-' . $lastMonthOfPrecious . '-t');
        return self::getBrandsByDay($date1, $date2);
    }

    /* Lấy ra brand nửa đầu năm */
    public static function getBrandsFirstYear($data)
    {
        $startMonthOfYear = 1;
        $endMonthOfYear = 6;

        $date1 = date($data['year'] . '-' . $startMonthOfYear . '-01');
        $date2 = date($data['year'] . '-' . $endMonthOfYear . '-t');
        return self::getBrandsByDay($date1, $date2);
    }

    /* Lấy ra brand nửa cuối năm */
    public static function getBrandsLastYear($data)
    {
        $startMonthOfYear = 7;
        $endMonthOfYear = 12;

        $date1 = date($data['year'] . '-' . $startMonthOfYear . '-01');
        $date2 = date($data['year'] . '-' . $endMonthOfYear . '-t');
        return self::getBrandsByDay($date1, $date2);
    }

    /* Lấy ra brand của cả năm */
    public static function getBrandsOfYear($data)
    {
        $startMonthOfYear = 1;
        $endMonthOfYear = 12;

        $date1 = date($data['year'] . '-' . $startMonthOfYear . '-01');
        $date2 = date($data['year'] . '-' . $endMonthOfYear . '-t');
        return self::getBrandsByDay($date1, $date2);
    }

    /* Lấy ra các brands theo điều kiện ngày */
    private static function getBrandsByDay($date1, $date2)
    {
        $brands = Brand::whereBetween('created_at', [$date1, $date2])
            ->select('created_at', 'start_at', 'end_at', 'user_id')
            ->get();

        if (!count($brands)) return array();

        foreach ($brands as $item) {
            self::convertBrand($item);
        }
        return $brands;
    }

    private static function convertBrand($item)
    {
        $item->created_at = date("H:m d/m/Y", strtotime($item->created_at));
        $item->shop = User::where('id', $item->user_id)->value('fullname');
        $item->type_advertise = 'Thương hiệu';

        $start_at = $item->start_at;
        $end_at = $item->start_at;

        unset($item->start_at);
        unset($item->end_at);

        $item->start_at = date("H:m d/m/Y", strtotime($start_at));
        $item->end_at = date("H:m d/m/Y", strtotime($end_at));

        $diff = abs(strtotime($start_at) - strtotime($end_at));
        $item->money = 0;
        $money = DB::table('app_service_brands')
            ->where('day', $diff)->value('price');
        if ($money) {
            $item->money = $money;
        }

        unset($item->user_id);
        return $item;
    }
}
