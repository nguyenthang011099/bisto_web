<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GiaoHangNhanh extends Model
{
    public $timestamps = false; //set time to false

    protected $fillable = [
        'shop_id', 'giao_hang_nhanh_shop_id',
    ];

    protected $primaryKey = 'id';
    protected $table = 'app_giaohangnhanh';
}
